procedure Peterson is

   type Id is range 1..2;
   type Tab_Candidate is array (Id) of Boolean;

   Priority  : Id := 1;
   Candidate : Tab_Candidate := (others => False);

   -- procedure to call before entering a critical region
   procedure Enter
     (X : in Id) is
      Other : Id;
   begin
      Other        := (X mod 2) + 1;
      Candidate(X) := True;
      Priority     := Other;
      while (Candidate (Other)) and (Priority = Other) loop
         null;
      end loop;
   end Enter;

   -- procedure to call when leaving a critical region
   procedure Quit
     (X : in Id) is
   begin
      Candidate (X) := False;
   end Quit;

   procedure Critical_Region
     (X : in Id) is
   begin
      null;
   end;

   task T_One;
   task T_Two;

   task body T_One is
      My_Id : Id := 1;
   begin
      loop
         Peterson.Enter(My_Id);
         Critical_Region(My_Id);
         Peterson.Quit(My_Id);
      end loop;
   end T_One;

   task body T_Two is
      My_Id : Id := 2;
   begin
      loop
         Peterson.Enter(My_Id);
         Critical_Region(My_Id);
         Peterson.Quit(My_Id);
      end loop;
   end T_Two;

begin
   null;
end Peterson;

