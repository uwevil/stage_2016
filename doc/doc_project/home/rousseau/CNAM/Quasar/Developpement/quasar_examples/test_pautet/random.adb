with Ada.Numerics.Discrete_Random;

package body Random is

   package Instance is new Ada.Numerics.Discrete_Random (Natural);

   G : Instance.Generator;

   function Value return Natural is
   begin
      return Instance.Random (G);
   end Value;

begin
   Instance.Reset (G);
end Random;
