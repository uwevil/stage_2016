procedure Test_Requeue is
 
   type Id is mod 10;
   protected type P is
      entry With_Index(Id)(I : in Integer ; B : in Boolean);
      entry Without_Index(I : in Integer ; B : in Boolean);
   end P;
   task type T is
      entry Without_Index(I : in Integer ; B : in Boolean);
      entry With_Index(Id)(I : in Integer ; B : in Boolean);
   end T;
   type T_Array is array(1..10) of T;
   type P_Array is array(1..10) of P;
 
   Many_Tasks   : T_Array;
   One_Task     : T;
   Many_Objects : P_Array;
   One_Object   : P;
 
 
 
   protected body P is
      entry With_Index(for Mon_Id in Id)(I : in Integer ; B : in Boolean) when True is
      begin
         null;
         ----- REQUEUE INTERNE -----
         -- REQUEUE SUR UNE ENTREE AVEC INDEX
         requeue Without_Index;
         -- REQUEUE SUR UNE ENTREE AVEC INDEX
         requeue With_Index(1);
         ---------------------------
      end;
      entry Without_Index(I : in Integer ; B : in Boolean) when True is
      begin
         null;
         ----- REQUEUE EXTERNE VERS UN OBJET -----
         -- REQUEUE SUR UNE ENTREE SANS INDEX
--         requeue One_Object.Without_Index;
         requeue Many_Objects(3).Without_Index;
         -- REQUEUE SUR UNE ENTREE AVEC INDEX
--         requeue One_Object.With_Index(5);
         requeue Many_Objects(3).With_Index(2);
         -----------------------------------------
 
         ----- REQUEUE EXTERNE VERS UNE TACHE -----
         -- REQUEUE SUR UNE ENTREE SANS INDEX
         requeue One_Task.Without_Index;
         requeue Many_Tasks(3).Without_Index;
         -- REQUEUE SUR UNE ENTREE AVEC INDEX
         requeue One_Task.With_Index(1);
         requeue Many_Tasks(3).With_Index(2);
         ------------------------------------------
 
      end;
   end P;
 
   task body T is
   begin
      accept With_Index(2)(I : in Integer ; B : in Boolean) do
         null;
         ----- REQUEUE INTERNE -----
         -- REQUEUE SUR UNE ENTREE AVEC INDEX
         requeue Without_Index;
         -- REQUEUE SUR UNE ENTREE AVEC INDEX
         requeue With_Index(1);
         ---------------------------
 
         ----- REQUEUE EXTERNE VERS UNE TACHE -----
         -- REQUEUE SUR UNE ENTREE SANS INDEX
         requeue One_Task.Without_Index;
         requeue Many_Tasks(3).Without_Index;
         -- REQUEUE SUR UNE ENTREE AVEC INDEX
         requeue One_Task.With_Index(1);
         requeue Many_Tasks(3).With_Index(2);
         ------------------------------------------
 
         ----- REQUEUE EXTERNE VERS UN OBJET -----
         -- REQUEUE SUR UNE ENTREE SANS INDEX
         requeue One_Object.Without_Index;
         requeue Many_Objects(3).Without_Index;
         -- REQUEUE SUR UNE ENTREE AVEC INDEX
         requeue One_Object.With_Index(4);
         requeue Many_Objects(3).With_Index(2);
         -----------------------------------------
      end With_Index;
      accept Without_Index(I : in Integer ; B : in Boolean);
   end;
begin
     null;
end;

