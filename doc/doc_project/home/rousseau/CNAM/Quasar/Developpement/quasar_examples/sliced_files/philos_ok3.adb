procedure Philos_Ok3 is
   type Id is mod 5;
   type Ready_Table is array (Id) of Boolean;

   protected type T_Sticks is
      entry Get_Pair(Id);
      procedure Release_Pair(C : in Id);
   private
      Ready : Ready_Table := (others => true);
   end;
   protected body T_Sticks is
      entry Get_Pair(for C in Id) when Ready(C) and Ready(C + 1) is
      begin
         Ready(C)     := False;
         Ready(C + 1) := False;
      end;
      procedure Release_Pair(C : in Id) is
      begin
         Ready(C)   := True;
         Ready(C+1) := True;
      end;
   end;
   Sticks : T_Sticks;
   task type Philosopher is
      entry Init(N : in Id);
   end;
   task body Philosopher is
      Self : Id;
   begin
      accept Init(N : in Id) do
         Self := N;
      end;
      loop
         Sticks.Get_Pair(Self);
         Sticks.Release_Pair(Self);
      end loop;
   end;
   type Tab is array (Id) of Philosopher;
   Philosophers : Tab;
begin
   for I in Id loop
      Philosophers(I).Init(I);
   end loop;
end;
