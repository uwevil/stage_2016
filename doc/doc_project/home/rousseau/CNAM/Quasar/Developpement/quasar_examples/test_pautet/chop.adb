with Types;               use Types;
with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

package body Chop is

   protected body Sticks is
      entry Get_Left  (C : Id) when not Updating is
      begin
         if Ready (C) then
            Ready (C) := False;
            requeue Get_Right;
         else
            requeue Get_Left_Q;
         end if;
      end Get_Left;

      entry Get_Left_Q (C : Id) when Updating is
      begin
         Updating := (Get_Left_Q'Count > 0 or Get_Right_Q'Count > 0);
         requeue Get_Left;
      end Get_Left_Q;

      entry Get_Right  (C : Id) when not Updating is
      begin
         if Ready (C + 1) then
            Ready (C + 1) := False;
         else
            requeue Get_Right_Q;
         end if;
      end Get_Right;

      entry Get_Right_Q  (C : Id) when Updating is
      begin
         Updating := (Get_Left_Q'Count > 0 or Get_Right_Q'Count > 0);
         requeue Get_Right;
      end Get_Right_Q;

      procedure Free   (C : Id) is
      begin
         Ready (C)     := True;
         Ready (C + 1) := True;
         Updating := (Get_Left_Q'Count > 0 or Get_Right_Q'Count > 0);
      end Free;

   end Sticks;

end Chop;
