with Chop;               use Chop;
with Random;
with Output;
with Types;               use Types;
with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
 
procedure Diner is
 
   type Status is (Eating, Thinking, Hungry);
 
   Tick_Delay  : constant Duration := 1.0;
   Max_Tick    : constant := 5;
 
   S : Sticks;
   
   task type Philosopher is
      entry Init (N : Id);
   end Philosopher;
 
   task body Philosopher is
      Self   : Id;
      N_Tick : Natural;
      Max_Times   : constant := 1_000;
   begin
      accept Init (N : Id) do
         Self  := N;
      end Init;
 
      for Times in 1 .. Max_Times loop
         S.Get_Left (Self);
 
         N_Tick := Random.Value mod Max_Tick;
 
         Output.Log (Self, Eating, Times);
         delay Tick_Delay * N_Tick;
 
         S.Free(Self);
 
         Output.Log (Self, Thinking, Times);
         delay (Max_Tick - N_Tick) * Tick_Delay;
 
         Output.Log (Self, Hungry, Times);
      end loop;
   end Philosopher;
 
   Philosophers : array (Id) of Philosopher;
 
begin
   for P in Philosophers'Range loop
      Philosophers (P).Init (P);
   end loop;
end Diner;


