procedure Philos_Ok5 is

   type Id is mod 5;
   type Booleans_Array is array(Id) of Boolean;

   ----- PROTECTED TYPE FORK SPECIFICATION -----
   protected Forks is
      entry Take_Left(Id);
      entry Take_Right(Id);
      procedure Release(I : in Id);
   private
      Free : Booleans_Array := (others => True);
   end Forks;
   ---------------------------------------------

   ----- TASK TYPE PHILOSOPHER SPECIFICATION -----
   task type Philosopher is
      entry Get_Id(New_Id : in Id);
   end Philosopher;
   type T_Philosophers is array(Id) of Philosopher;
   -----------------------------------------------

   ----- THE PHILOSOPHERS -----
   The_Philosophers : T_Philosophers;
   ----------------------------



   ----- PROTECTED TYPE FORK BODY -----
   protected body Forks is
      entry Take_Left(for I in Id) when Free(I) is
      begin
         Free(I) := False;
         requeue Take_Right(I);
      end;
      entry Take_Right(for I in Id) when Free(I + 1) is
      begin
         Free(I + 1) := False;
      end;
      procedure Release(I : in Id) is
      begin
         Free(I) := True;
         Free(I+ 1) := True;
      end;
   end Forks;
   ------------------------------------

   ----- TASK TYPE PHILOSOPHER BODY -----
   task body Philosopher is
      My_Id : Id;
   begin
      accept Get_Id(New_Id : in Id) do
         My_Id := New_Id;
      end Get_Id;
      loop
         Forks.Take_Left(My_Id);
         Forks.Release(My_Id);
      end loop;
   end;
   --------------------------------------

begin
   for I in Id loop
      The_Philosophers(I).Get_Id(I);
   end loop;
end Philos_Ok5;

