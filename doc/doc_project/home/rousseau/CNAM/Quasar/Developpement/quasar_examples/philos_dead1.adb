procedure Philos_Dead1 is
   type Id is mod 5;
   -- PROTECTED TYPE FORK SPECIFICATION --
   protected type Fork is
      entry Take;
      procedure Release;
   private
      Free : Boolean := True;
   end Fork;
   type T_Forks is array(Id) of Fork;
   The_Forks : T_Forks;
   --===================================--
   -- TASK TYPE PHILOSOPHER SPECIFICATION --
   task type Philosopher is
      entry Get_Id(New_Id : in Id);
   end Philosopher;
   type T_Philosophers is array(Id) of Philosopher;
   The_Philosophers : T_Philosophers;
   --=====================================--
   -- PROTECTED TYPE FORK BODY --
   protected body Fork is
      entry Take when Free is
      begin
         Free := False;
      end;
      procedure Release is
      begin
         Free := True;
      end;
   end Fork;
   --==========================--
   -- TASK TYPE PHILOSOPHER BODY --
   task body Philosopher is
      My_Id : Id;
   begin
      accept Get_Id(New_Id : in Id) do
         My_Id := New_Id;
      end Get_Id;
      loop
         The_Forks(My_Id).Take;
         The_Forks(My_Id + 1).Take;
         The_Forks(My_Id).Release;
         The_Forks(My_Id + 1).Release;
      end loop;
   end;
   --============================--
begin
   for I in Id loop
      The_Philosophers(I).Get_Id(I);
   end loop;
end Philos_Dead1;
