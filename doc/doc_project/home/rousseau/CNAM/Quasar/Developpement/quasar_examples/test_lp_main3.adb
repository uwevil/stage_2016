with Ada.Text_IO; use Ada.Text_IO;
 
procedure Test_Lp_Main3 is
   type Id is mod 4;
   type Ready_Table is array (Id) of Boolean;
 
--   Tick_Delay  : constant Duration := 1.0;
--   Max_Tick    : constant := 5;
   
   protected type T_Sticks is
      entry Get_Left (C : in Id);
      entry Get_Right (C : in Id);
      procedure Free (C : in Id);
      entry Get_Left_Q (C : in Id);
      entry Get_Right_Q (C : in Id);
   private
      Ready : Ready_Table := (others => True);
      Updating : Boolean := False;
   end T_Sticks;
 
   protected body T_Sticks is
      entry Get_Left (C : in Id) when not Updating is
      begin
         if Ready(C) then
            Ready(C) := False;
            requeue Get_Right;
         else
            requeue Get_Left_Q;
         end if;
      end;
      entry Get_Left_Q (C : in Id)  when Updating is
      begin
         Updating := Get_Left'count > 0 or Get_Right'count > 0;
         requeue Get_Left;
      end;
      entry Get_Right (C : in Id) when not Updating is
      begin
         if Ready(C+1) then
            Ready(C+1) := False;
         else
            requeue Get_Right_Q;
         end if;
      end;
      entry Get_Right_Q (C : in Id) when Updating is
      begin
         Updating := Get_Left'count > 0 or Get_Right'count > 0;
         requeue Get_Right;
      end;
      procedure Free (C : in Id) is
      begin
         Ready(C) := True;
         Ready(C+1) := True;
         Updating := Get_Left_Q'count > 0 or Get_Right_Q'count > 0;
      end;
   end T_Sticks;
   Sticks : T_Sticks;
   task type Philosopher is
      entry Init (N : in Id);
   end Philosopher;
   task body Philosopher  is
      Self : Id;
   begin
      accept Init(N : in Id) do
         Self := N;
      end Init;
      loop	 
         Sticks.Get_Left(Self);
         Sticks.Free(Self);
      end loop;
   end Philosopher;
   type Tab is array (Id) of Philosopher;
   Philosophers : Tab;
begin
   Philosophers(0).Init(0);
   Philosophers(1).Init(1);
   Philosophers(2).Init(2);
   Philosophers(3).Init(3);
end;

