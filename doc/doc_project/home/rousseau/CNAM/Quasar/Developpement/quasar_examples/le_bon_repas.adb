procedure Le_Bon_Repas is
   N : constant integer := 5;
   type Id is mod N;
   type Tab_B is array(Id) of Boolean;
   type Inot is (Init, NoChopStick, OneChopStick, TwoChopSticks);
   type Tab_Inot is array(Id) of Inot;
 
   protected Repas is 
      entry Demander(X : in Id);
      procedure Conclure(X : in Id);
   private
      Chaise   : Integer  := N - 1;
      Baguette : Tab_B    := (others => true);
      Etape    : Tab_Inot := (others => Init);
      entry B
         (Id)(X : in Id);
   end Repas;
 
   protected Numero is
      procedure Unique(Resultat : out Id);
   private
      Compte : Id := 0;
   end Numero;
 
   procedure Mange(X : in Id) is
   begin
      null;
   end Mange;
    
   task type Philo;
   Philosophe : array(Id) of Philo;
 
   task body Philo is
      X : Integer;
      Ego : Id;
   begin
      X := X + 1;
      Numero.Unique(Ego);
      loop
         Repas.Demander(Ego);
         Mange(Ego);
         Repas.Conclure(Ego);
      end loop;
   end Philo;
 
   protected body Repas is
      entry Demander(X : in Id) when Chaise > 0 is
      begin
         Chaise := Chaise - 1;
         if Baguette(X) and Baguette(X+1) then
            Baguette(X)   := False;
            Baguette(X+1) := False;
            Etape(X)      := TwoChopSticks;
         elsif Baguette(X) then
            Baguette(X)   := False;
            Etape(X)      := OneChopStick;
            requeue B(X+1);
         elsif Baguette(X+1) then
            Baguette(X+1) := False;
            Etape(X)      := OneChopStick;
            requeue B(X);
         else
            Etape(X)      := NoChopStick;
            requeue B(X);
         end if;
      end Demander;
 
      procedure Conclure(X : in Id) is
      begin
         Chaise        := Chaise + 1;
         Baguette(X)   := True;
         Baguette(X+1) := True;
         Etape(X)      := Init;
      end Conclure;
 
      entry B(for I in Id)(X : in Id) when Baguette(I) is
      begin
         Baguette(I) := False;
         if Etape(X) = NoChopStick then
            Etape(X) := OneChopStick;
            requeue B(X+1);
         else
            Etape(X) := TwoChopSticks;
         end if;
      end B;
   end Repas;
 
   protected body Numero is 
      procedure Unique(Resultat : out Id) is
      begin
         Resultat := Compte;
         Compte   := Compte + 1;
      end Unique;
   end Numero;
 
begin
   null;
end Le_Bon_Repas;
