with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

package body Output is

   type Info is record
      S : Status;
      T : Natural;
   end record;

   Info_Table : array (Id) of Info := (others => (Hungry, 0));

   protected type Mutex is
      entry P;
      procedure V;
   private
      Free : Boolean := True;
   end Mutex;

   protected body Mutex is
      entry P when Free is
      begin
         Free := False;
      end P;

      procedure V is
      begin
         Free := True;
      end V;
   end Mutex;

   M : Mutex;
   
   procedure Log (I : Id; S : Status; T : Natural) is
   begin
      Info_Table (I) := (S, T);

      M.P;
      for P in Info_Table'Range loop
         case Info_Table (P).S is
            when Eating =>
               Put ("   eating");
            when Thinking =>
               Put (" thinking");
            when Hungry =>
               Put ("   hungry");
         end case;
         Put (Info_Table (P).T, Width => 4);
      end loop;
      New_Line;
      M.V;
   end Log;

end Output;
