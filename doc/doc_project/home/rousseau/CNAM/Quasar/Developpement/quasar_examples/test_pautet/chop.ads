with Types;              use Types;

package Chop is

   type Ready_Table is array (Id) of Boolean;

   protected type Sticks is
      entry Get_Left  (C : Id);
      entry Get_Right (C : Id);
      procedure Free  (C : Id);
   private
      entry Get_Left_Q  (C : Id);
      entry Get_Right_Q (C : Id);
      Ready    : Ready_Table := (others => True);
      Updating : Boolean := False;
   end Sticks;

end Chop;
