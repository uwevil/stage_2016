procedure Philos_Ok6 is

   type Id is mod 5;
   type Tab_B is array(Id) of Boolean;
   type Inot is (Init, NoChopStick, OneChopStick, TwoChopSticks);
   type Tab_Inot is array(Id) of Inot;

   protected Repas is
      entry Demander(X : in Id);
      procedure Conclure(X : in Id);
   private
--      Chaise   : Integer  := 5;
      Baguette : Tab_B    := (others => true);
      Etape    : Tab_Inot := (others => Init);
      entry B(Id)(X : in Id);
   end Repas;

   task type Philo is
      entry Get_My_Id(My_Id : in Id);
   end Philo;
   Philosophe : array(Id) of Philo;

   task body Philo is
      Ego : Id;
   begin
      accept Get_My_Id(My_Id : in Id) do
         Ego := My_Id;
      end Get_My_Id;
      loop
         Repas.Demander(Ego);
         Repas.Conclure(Ego);
      end loop;
   end Philo;

   protected body Repas is
--      entry Demander(X : in Id) when Chaise > 0 is
      entry Demander(X : in Id) when true is
      begin
--         Chaise := Chaise - 1;
         if Baguette(X) and Baguette(X+1) then
            Baguette(X)   := False;
            Baguette(X+1) := False;
            Etape(X)      := TwoChopSticks;
         elsif Baguette(X) then
            Baguette(X)   := False;
            Etape(X)      := OneChopStick;
            requeue B(X+1);
         elsif Baguette(X+1) then
            Baguette(X+1) := False;
            Etape(X)      := OneChopStick;
            requeue B(X);
         else
            Etape(X)      := NoChopStick;
            requeue B(X);
         end if;
      end Demander;

      procedure Conclure(X : in Id) is
      begin
--         Chaise        := Chaise + 1;
         Baguette(X)   := True;
         Baguette(X+1) := True;
         Etape(X)      := Init;
      end Conclure;

      entry B(for I in Id)(X : in Id) when Baguette(I) is
      begin
         Baguette(I) := False;
         if Etape(X) = NoChopStick then
            Etape(X) := OneChopStick;
            requeue B(X+1);
         else
            Etape(X) := TwoChopSticks;
         end if;
      end B;
   end Repas;
begin
   for I in Id loop
      Philosophe(I).Get_My_Id(I);
   end loop;
end;





