procedure Philos_Dead3 is

   type Id is mod 4;

   task type Philos is
      entry Get_Id(I : in Id);
   end;
   P : array(Id) of Philos;
   task type Sticks is
      entry Take;
      entry Release;
   end;
   S : array(Id) of Sticks;

   task body Philos is
      My_Id : Id;
   begin
      accept Get_Id(I : in Id) do
         My_Id := I;
      end;
      loop
         S(My_Id).Take;
         S(My_Id+1).Take;
         S(My_Id).Release;
         S(My_Id+1).Release;
      end loop;
   end;
   task body Sticks is
   begin
      loop
         accept Take;
         accept Release;
      end loop;
   end;

begin
   for I in Id loop
      P(I).Get_Id(I);
   end loop;
end;
