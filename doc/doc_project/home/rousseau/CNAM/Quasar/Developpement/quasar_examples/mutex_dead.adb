procedure Mutex_Dead is
   protected type Mutex is
      entry P;
      procedure V;
   private
      Free : Boolean := True;
   end;
   protected body Mutex is
      entry P when Free is
      begin
         Free := False;
      end;
      procedure V is
      begin
         Free := True;
      end;
   end;
   M1: Mutex;
   M2: Mutex;
   task T1 is
   end;
   task T2 is
   end;
   task body T1 is
   begin
      loop
         M2.P;
         M1.P;
         M1.V;
         M2.V;
      end loop;
   end;
   task body T2 is
   begin
      loop
null;--         M1.P;
--         M2.P;
--         M2.V;
--         M1.V;
      end loop;
   end;
   I : Integer;
begin
   I := 0;
   null;
end;





