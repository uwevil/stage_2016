procedure Philos_Ok1 is

   type Id is mod 5;
   type Ready_Table is array (Id) of Boolean;


   protected Sticks is
      entry Get_Left(C : in Id);
      entry Get_Right(C : in Id);
      procedure Free(C : in Id);
      entry Get_Left_Q(C : in Id);
      entry Get_Right_Q(C : in Id);
   private
      Ready : Ready_Table := (others => true);
      Updating : Boolean := False;
   end;
   protected body Sticks is
      entry Get_Left(C : in Id) when not Updating is
      begin
         if Ready(C) then
            Ready(C) := False;
            requeue Get_Right;
         else
            requeue Get_Left_Q;
         end if;
      end;
      entry Get_Left_Q(C : in Id) when Updating is
      begin
         Updating := Get_Left_Q'count > 0 or Get_Right_Q'count > 0;
         requeue Get_Left;
      end;
      entry Get_Right(C : in Id) when not Updating is
      begin
         if Ready(C+1) then
            Ready(C+1) := False;
         else
            requeue Get_Right_Q;
         end if;
      end;
      entry Get_Right_Q(C : in Id) when Updating is
      begin
         Updating := Get_Left_Q'count > 0 or Get_Right_Q'count > 0;
         requeue Get_Right;
      end;
      procedure Free(C : in Id) is
      begin
         Ready(C) := True;
         Ready(C+1) := True;
         Updating := Get_Left_Q'count > 0 or Get_Right_Q'count > 0;
      end;
   end;



   task type Philosopher is
      entry Init(N : in Id);
   end;
   task body Philosopher is
      Self : Id;
   begin
      accept Init(N : in Id) do
         Self := N;
      end;
      loop
         Sticks.Get_Left(Self);
         Sticks.Free(Self);
      end loop;
   end;
   type Tab is array (Id) of Philosopher;
   Philosophers : Tab;

begin
   for I in Id loop
      Philosophers(I).Init(I);
   end loop;
end;
