procedure Philos_Dead2 is

   type Id is mod 6;


   protected type Fork is
      entry Take_Left(Fork_Id : in Id);
      entry Take_Right(Fork_Id : in Id);
      procedure Release;
   private
      Free : Boolean := True;
   end;
   type T_Forks is array(Id) of Fork;



   task type Philosopher is
      entry Get_Id(New_Id : in Id);
   end;
   type T_Philosophers is array(Id) of Philosopher;



   The_Forks : T_Forks;



   The_Philosophers : T_Philosophers;



   protected body Fork is
      entry Take_Left(Fork_Id : in Id) when Free is
      begin
         Free := False;
         requeue The_Forks(Fork_Id + 1).Take_Right;
      end;
      entry Take_Right(Fork_Id : in Id) when Free is
      begin
         Free := False;
      end;
      procedure Release is
      begin
         Free := True;
      end;
   end;



   task body Philosopher is
      My_Id : Id;
   begin
      accept Get_Id(New_Id : in Id) do
         My_Id := New_Id;
      end;
      loop
         The_Forks(My_Id).Take_Left(My_Id);
         The_Forks(My_Id).Release;
         The_Forks(My_Id + 1).Release;
      end loop;
   end;


begin
   for I in Id loop
      The_Philosophers(I).Get_Id(I);
   end loop;
end;
