package Types is

   --N_Philosophers : constant := 5;

   type Id is mod 5;--N_Philosophers;
   --  Values between 0 .. 4. Successor of 4 is 0.

   type Status is (Eating, Thinking, Hungry);

end Types;

