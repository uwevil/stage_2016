procedure Philos_Ok7 is

   type Id is mod 5;

   -- TASK SPECIFICATIONS --
   task type Philos is
      entry Get_Id(I : in Id);
   end Philos;
   P : array(Id) of Philos;
   task type Sticks is
      entry Take;
      entry Release;
   end Sticks;
   S : array(Id) of Sticks;
   task Chairs is
      entry Sit;
      entry Get_Up;
   end Chairs;
   --=====================--

   -- TASK BODIES --
   task body Philos is
      My_Id : Id;
   begin
      accept Get_Id(I : in Id) do
         My_Id := I;
      end Get_Id;
      loop
         Chairs.Sit;
         S(My_Id).Take;
         S(My_Id+1).Take;
         S(My_Id).Release;
         S(My_Id+1).Release;
         Chairs.Get_Up;
      end loop;
   end Philos;
   task body Sticks is
   begin
      loop
         accept Take;
         accept Release;
      end loop;
   end;
   task body Chairs is
      Free : Integer := 4;
   begin
      loop
         select
            when Free > 0 =>
               accept Sit;
               Free := Free - 1;
         or
            when Free < 4 =>
               accept Get_Up;
               Free := Free + 1;
         end select;
      end loop;
   end;
   --=============--

begin
   for I in Id loop
      P(I).Get_Id(I);
   end loop;
end;

