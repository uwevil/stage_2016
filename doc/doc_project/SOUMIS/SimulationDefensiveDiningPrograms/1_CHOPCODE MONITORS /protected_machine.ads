generic
   type Result is private;
   type Internal_State is limited private;
   with procedure Reset (V: in Internal_State);
   with function Next_Value (V: Internal_State) return Result;

package Protected_Machine is
    function Next_Protected_Value return Result;
end Protected_Machine;
