-------					THIS PROCEDURE IS THE MAIN PROGRAM OF THE CHAMENEOS GAME 
	
with Text_IO, P_Id_Chameneos, P_Colour, Mall, Generic_Lock ;
use Text_IO,  P_Id_Chameneos, P_Colour, Mall;

procedure  P_Chameneos is

	-- Nb_Requests is the number of loops in a chameneos task
	Nb_Requests: Integer renames P_Id_Chameneos.Nb_Requests;

	--------------        Chameneos task specification  ------------------
   task type T_Chameneos is
      entry Start(Id: in Id_Chameneos);		-- initialization
	  -- entries for transaction with a peer 
	  entry Call(Emitter : in Id_Chameneos; C_Emitter : Colour; 
					Receptor : in Id_Chameneos; C_Receptor : out Colour);
	  entry Finish(Emitter : in Id_Chameneos; C_Emitter : Colour; 
					Receptor : in Id_Chameneos; C_Receptor : out Colour; 
					WellDone : out Boolean);
   end T_Chameneos;

   ----------------------------------------------------------------------
   Chameneos : array(Id_Chameneos) of T_Chameneos;  -- set of mutating chameneos
   
   Last_Chameneos : Id_Chameneos ;   -- name of the last chameneos finishing 
   Rescued_Chameneos : Id_Chameneos ; -- used for correct termination

   package Display_Lock is new Generic_Lock ;   -- printing mutex
   package World_Lock is new Generic_Lock ;		-- P_Colour_World mutex
   
  ------------------    Termination protected object      -----------------
   type Boolean_Chameneos is array(Id_Chameneos) of Boolean ;
   protected Termination is 
		entry Wait_Penultimate(X : out Id_Chameneos);
		entry Wait_Ultimate ;
		procedure Signal(X : in Id_Chameneos); -- task X ends
		function Is_terminated(X : in Id_Chameneos) return Boolean;
   private
		Remaining : Integer := P_Id_Chameneos.Nb_Chameneos;
		Terminated : Boolean_Chameneos := (others => False) ;
   end Termination;
   
   protected body Termination is 
		entry Wait_Penultimate(X : out Id_Chameneos) when Remaining <= 1 is 
		begin 
			for I in Id_Chameneos loop
				X := I;
				exit when not Terminated(I);
			end loop;
		end Wait_Penultimate ;
		entry Wait_Ultimate when Remaining = 0 is
		begin null; end Wait_Ultimate; 
		procedure Signal(X : in Id_Chameneos) is
		begin 
			Remaining := Remaining - 1;
			Terminated(X) := True;
		end Signal; 
		function Is_Terminated(X : in Id_Chameneos) return Boolean is
		begin return Terminated(X); end Is_Terminated;
   end Termination;
   ----------------------   task body Chameneos   ---------------------
   task body T_Chameneos is
   
      My_Id          : Id_Chameneos;  -- name of Chameneos
	  My_Peer	     : Id_Chameneos;  -- name of current peer if any
      My_Colour      : Colour;
      My_Peer_Colour : Colour;
	  Committed		 : Boolean;		  -- end of transaction commit 
	  Stroke		 : Integer := 0;  -- current stroke number

----  messages displayed by a chameneos indicating its progression  ****
	  
	  procedure Message(Mess : in String) is
      begin
		 Display_Lock.Acquire;
		 Put("< " & Id_Chameneos'Image(My_Id) &  " :: stroke " & Integer'Image(Stroke));
         Put_Line(" :: " & Colour'Image(My_Colour) & " >  " & Mess);
		 Display_Lock.Release;
      end Message;

	  procedure Starting is
      begin
         Message("is starting");
      end Starting;

      procedure Eating_Honey_Suckle_And_Training is
      begin
         Message("is eating honeysuckle");
      end Eating_Honey_Suckle_And_Training;

      procedure Going_To_The_Mall is
      begin
         Message("is going to the mall");
      end Going_To_The_Mall;

      procedure Peering is
      begin
         Message("has got a peer named < " &  Id_Chameneos'Image(My_Peer) & " >");
      end Peering;
	  
	  procedure Peering_With_Colour is
      begin
         Message("has got a peer named < " &  Id_Chameneos'Image(My_Peer) 
					& " :: " &  Colour'Image(My_Peer_Colour) & " >");
      end Peering_With_Colour;
	  
	  procedure Mutating is
      begin
         Message("has performed a mutation");
      end Mutating;
	  
	procedure Quitting is
      begin
         Put_Line("                                                    < " 
			& Id_Chameneos'Image(My_Id) &  " :: " & Colour'Image(My_Colour) 
			& " > is quitting" );
      end Quitting;
  
	----------------------------------------------------------------------
	begin
	
	      -----       initialization of Chameneos attributes    ----------
      accept Start(Id: in Id_Chameneos) do
         My_Id := Id; My_Colour := P_Colour.Get_Colour;  
		 P_Colour.Update_World(My_Id, My_Colour); Starting;      
	  end Start;

	  ---------------- each loop cycle is a new peer to peer communication ----
      for I in 1.. Nb_Requests loop
		 
		 Stroke := I; -- chameneos current evolution 
		 
		 Eating_Honey_Suckle_And_Training;
		 
		 Going_To_The_Mall;
		 My_Peer := Mall.Get_A_Peer(My_Id);
		 exit when My_Peer = My_Id;  -- used for rescueing an incorrect termination
		 Peering;
		 
		 -- Perform_Transaction(My_Id, My_Colour, My_Peer, My_Peer_Colour) ;
		 -- to get the colour of its peer
		 if My_Id < My_Peer then 
			Chameneos(My_Peer).Call(My_Id, My_Colour, My_Peer, My_Peer_Colour);
		 else
			accept Call(Emitter : in Id_Chameneos; C_Emitter : Colour; 
						Receptor : in Id_Chameneos; C_Receptor : out Colour) do 
				C_Receptor := My_Colour; My_Peer_Colour := C_Emitter;
			end Call;
		 end if;
		 Peering_With_Colour ;
		 
		 -- each chameneos performs a mutation
		 My_Colour := P_Colour.Complementary_Colour( My_Colour, My_Peer_Colour);
		 Mutating;
		 
		 -- Commit_Transaction(My_Id, My_Colour, My_Peer, My_Peer_Colour, Committed)
		 -- both chameneos update their colour in P_Colour.World during the commit
		if My_Id < My_Peer then 
			World_Lock.Acquire;
			Chameneos(My_Peer).Finish(My_Id, My_Colour, 
									My_Peer, My_Peer_Colour, Committed);
			P_Colour.Update_World(My_Id, My_Colour);
			World_Lock.Release;
		 else
			accept Finish(Emitter : in Id_Chameneos; C_Emitter : Colour;
					Receptor : in Id_Chameneos; C_Receptor : out Colour; 
				WellDone : out Boolean) do
				C_Receptor := My_Colour; WellDone := (C_Emitter = My_Colour);
				-- the World_Lock has been acquired by the calling partner
				-- and it will also be released by the calling partner
				P_Colour.Update_World(My_Id, My_Colour);
			end Finish ;
		end if;
 
		-------   end of each stroke : a peer name acquired and a transaction done
						 
      end loop;
		  
	  Quitting ;
	  Termination.Signal(My_Id);  -- registers its termination

	  
   end T_Chameneos;
   
begin
	
	---------------  displaying initial --------------
	Put_Line("....................     NEW GAME STARTING    ....................");
	
	-- record initial colour set
	-- declare
	--	World : P_Colour.World_Array;
	--	B, R, Y : Integer := 0;
--	begin
	--	World := Display_World;
	--	for I in Id_Chameneos loop
	--		Put_Line(P_Colour.Colour'Image(World(I)));
	--	end loop;
	--	P_Colour.Get_Situation(B, R, Y) ; 
	--	Put_Line("Game starting with (BLEU, RED, YELLOW) = (" & Integer'Image(B) 
	--			& ", " & Integer'Image(R) & ", " & Integer'Image(Y) & ")");
--	end;
	
	----------------------------	start all chameneos successively
	for I in Id_Chameneos loop
			Chameneos(I).Start(Id_Chameneos(I));
	end loop; 
		
	----------------------------- control of the correct termination
	Termination.Wait_Penultimate(Last_Chameneos);
	
	-- wait until all finished whether properly or not
	-- if the last chameneos waits lonely in the Mall, it must be aborted
	-- the last chameneos must be allowed to finish if not blocked in Mall
	
	delay(1.0); -- this gives it some delay to finish or to call the Mall
	if not Termination.Is_Terminated(Last_Chameneos) then
		Put_Line("Rescue sent to" & Id_Chameneos'Image(Last_Chameneos)); 
		Rescued_Chameneos := Mall.Get_A_Peer(Last_Chameneos);
		Put_Line("Rescued is " & Id_Chameneos'Image(Rescued_Chameneos)); 
	end if;
	
	Termination.Wait_Ultimate; -- waits a possible rescue
	
	Put_Line("....................Correct termination....................");
	
	-----------------------		record the final colour set 
	declare
		World : P_Colour.World_Array;
		B, R, Y : Integer := 0;
	begin
		P_Colour.Get_Situation(B, R, Y) ; 
		Put_Line("Game ending with (BLEU, RED, YELLOW) = (" & Integer'Image(B) 
				& ", " & Integer'Image(R) & ", " & Integer'Image(Y) & ")");
		World := Display_World;
		for I in Id_Chameneos loop
			Put_Line(P_Colour.Colour'Image(World(I)));
		end loop;
	end;
	
	------------------------------   display the end of simulation -----
	Put_Line("..............................        THE END        ....................");
			
end P_Chameneos;

-- commentary : Without the rescue action the following may occur:
-- Nb_Chameneos -1 have quitted and the last one is in the mall waiting for a peer
-- this is a deadlock situation
-- Hence the necessity of knowing the last chameneos name 
-- and guessing whether it is waiting in Mall. If so, rescueing it. 

--------------       END OF THE CHAMENEOS GAME MAIN PROGRAMME    ******

------------------------------------		ADDITIONAL COMMENTS
-- in Ada the enclosing body of accept must be a task
-- thus a procedure cannot include an accept statement 
-- thus the following procedures (moreover a library package) were refused
	 -- procedure Perform_Transaction(X : in Id_Chameneos; C_X : in Colour; 
	 --								  Y : in Id_Chameneos;  C_Y : out Colour);
			
	 -- procedure Commit_Transaction(X : in Id_Chameneos; C_X : in Colour; 
	 --						Y : in Id_Chameneos; C_Y : out Colour; 
	 --						OK : out Boolean) ;
	