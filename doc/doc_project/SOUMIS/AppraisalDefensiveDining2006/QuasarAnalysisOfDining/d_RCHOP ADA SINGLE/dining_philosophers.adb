procedure Dining_Philosophers is

   type Id is range 0..4;

   type SticState is array(Id) of Boolean;
   type Cardinal is range 0.. 2 ;
   type SticScore is array(Id) of Cardinal;

   protected type Sticks_Type is
      procedure Init;
      entry Get_LR
        (C : in Id);
      procedure Release
        (C : in Id);
   private
      entry Wait
        (C : in Id);
      Available     : SticState;-- := (others => True);
      Score         : SticScore;-- := (others => 0);
      StillToNotify : Integer := 0;
      NotifyAll     : Boolean := False;
   end Sticks_Type;
   protected body Sticks_Type is
      procedure Init is
      begin
         for I in Id loop
            Available(I) := True;
            Score(I) := 0;
         end loop;
      end;
      entry Get_LR
        (C : in Id) when not NotifyAll is
      begin
         case Score(C) is
            when 0 =>
               if Available(C) then
                  Available(C) := False;
                  Score(C) := 1;
                  if Available((C + 1) mod 5) then
                     Available((C + 1) mod 5) := False;
                     Score(C) := 2;
                  end if;
               end if;
            when 1 =>
               if Available((C + 1) mod 5) then
                  Available((C + 1) mod 5) := False;
                  Score(C) := 2;
               end if;
            when 2 =>
               null;
         end case;
         if Score(C) /= 2 then
            requeue Wait;
         end if;
      end Get_LR;
      entry Wait
        (C : in Id) when NotifyAll is
      begin
         StillToNotify:= StillToNotify - 1;
         NotifyAll := (StillToNotify > 0);
         requeue Get_LR;
      end Wait;
      procedure Release
        (C : in Id) is
      begin
         Available(C) := True;
         Available((C + 1) mod 5) := True;
         Score(C) := 0;
         StillToNotify:= Wait'Count;
         NotifyAll := (StillToNotify > 0);
      end Release;
   end Sticks_Type;
   Sticks : Sticks_Type;


   task type Philosopher is
      entry GetId
        (Get_Unique : in Id);
   end Philosopher;
   task body Philosopher is
      Self : Id;
   begin
      accept GetId
        (Get_Unique : in Id) do
         Self := Get_Unique;
      end GetId;
      loop
         Sticks.Get_LR(Self);
         Sticks.Release(Self);
      end loop;
   end Philosopher;
   --type Philosophers_Array is array(Id) of Philosopher;
   --Philosophers : Philosophers_Array;
   P0 : Philosopher;
   P1 : Philosopher;
   P2 : Philosopher;
   P3 : Philosopher;
   P4 : Philosopher;

begin
   --for I in Id loop
   --   Philosophers(I).GetId(I);
   --end loop;
   Sticks.Init;
   P0.Getid(0);
   P1.Getid(1);
   P2.Getid(2);
   P3.Getid(3);
   P4.Getid(4);
end;
