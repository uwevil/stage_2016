procedure Dining_Philosophers is

   type Id is range 0..4;

   type SticState is array(Id) of Boolean;

   protected type Sticks_Type is
      procedure Init;
      entry Get_LR(Id);
      procedure Release
        (C : in Id);
   private
      entry Get_R(Id);
      Available : SticState;-- := (others => True);
   end Sticks_Type;
   protected body Sticks_Type is
      procedure Init is
      begin
         for I in Id loop
            Available(I) := True;
         end loop;
      end;
      entry Get_LR(for C in Id) when Available (C) is
      begin
         Available(C) := False;
         requeue Get_R((C + 1) mod 5);
      end Get_LR;
      entry Get_R(for C in Id) when Available (C) is
      begin
         Available(C) := False;
      end Get_R;
      procedure Release
        (C : in Id) is
      begin
         Available(C) := True;
         Available((C + 1) mod 5) := True;
      end Release;
   end Sticks_Type;
   Sticks : Sticks_Type;


   task type Philosopher is
      entry GetId(Get_Unique : in Id);
   end Philosopher;
   task body Philosopher is
      Self : Id ;
   begin
      accept GetId
        (Get_Unique : in Id) do
         Self := Get_Unique;
      end GetId;
      loop
         Sticks.Get_LR(Self);
         Sticks.Release(Self);
      end loop;
   end Philosopher;
   P0 : Philosopher;
   P1 : Philosopher;
   P2 : Philosopher;
   P3 : Philosopher;
   P4 : Philosopher;

begin
   Sticks.Init;
   P0.Getid(0);
   P1.Getid(1);
   P2.Getid(2);
   P3.Getid(3);
   P4.Getid(4);

   --type Philosophers_Array is array(Id) of Philosopher;
   --Philosophers : Philosophers_Array;

   --begin
   --for I in Id loop
   --   Philosophers(I).GetId(I);
   --end loop;

end;
