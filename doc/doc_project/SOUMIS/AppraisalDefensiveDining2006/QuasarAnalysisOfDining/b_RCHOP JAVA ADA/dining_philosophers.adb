procedure Dining_Philosophers is

   type Id is range 0..4;

   type SticState is array(Id) of Boolean;
   type Cardinal is range 0.. 2 ;

   protected type Sticks_Type is
      procedure Init;
      entry Get_LR
        (C     : in     Id;
         Score : in out Cardinal);
      procedure Release
        (C : in Id);
   private
      entry Wait
        (C     : in     Id;
         Score : in out Cardinal);
      Available : SticState;-- := (others => True);
      Booked    : SticState;-- := (others => False);
      NotifyAll : Boolean := False;
   end Sticks_Type;
   Sticks : Sticks_Type;

   protected body Sticks_Type is
      procedure Init is
      begin
         for I in Id loop
            Available(I) := True;
            Booked(I) := False;
         end loop;
      end;
      entry  Get_LR
        (C     : in     Id;
         Score : in out Cardinal) when True is
      begin
         case Score is
            when 0 =>
               if not Booked(C) and Available (C) then
                  Available(C) := False;
                  Score:= 1;
                  if Available((C + 1) mod 5) then
                     Available((C + 1) mod 5) := False;
                     Score := 2;
                  else
                     Booked((C + 1) mod 5) := True;
                  end if;
               end if;
            when 1 =>
               if Available((C + 1) mod 5) then
                  Available((C + 1) mod 5) := False;
                  Score := 2;
                  Booked((C + 1) mod 5) := False;
               end if;
            when others =>
               null;
         end case;
         if Score /= 2 then
            NotifyAll := False; requeue Wait;
         end if;
      end Get_LR;
      entry Wait
        (C     : in     Id;
         Score : in out Cardinal) when NotifyAll is
      begin
         null;
      end Wait;
      procedure Release
        (C : in Id) is
      begin
         Available(C) := True;
         Available((C + 1) mod 5) := True;
         NotifyAll := (Wait'Count > 0);
      end Release;
   end Sticks_Type;

   procedure Get_LR
     (C : in Id) is
      Score :  Cardinal := 0;
   begin
      while Score /= 2 loop
         Sticks.Get_LR(C, Score);
      end loop;
   end Get_LR;
   procedure Release
     (C : in Id) is
   begin
      Sticks.Release(C);
   end Release;

   task type Philosopher is
      entry GetId
        (Get_Unique : in Id);
   end Philosopher;
   task body Philosopher is
      Self : Id ;
   begin
      accept GetId
        (Get_Unique : in Id) do
         Self := Get_Unique;
      end GetId;
      loop
         Get_LR(Self);
         Release(Self);
      end loop;
   end Philosopher;
   --type Philosophers_Array is array(Id) of Philosopher;
   --Philosophers : Philosophers_Array;
   P0 : Philosopher;
   P1 : Philosopher;
   P2 : Philosopher;
   P3 : Philosopher;
   P4 : Philosopher;

begin
   --for I in Id loop
   --   Philosophers(I).GetId(I);
   --end loop;
   Sticks.Init;
   P0.Getid(0);
   P1.Getid(1);
   P2.Getid(2);
   P3.Getid(3);
   P4.Getid(4);
end;
