procedure Dining_Philosophers is
   type Id is range 0..4;
     task type Philosopher is
      entry GetId(Get_Unique : in Id);
   end Philosopher;

        procedure Get_LR(C : Id) is
        begin
                null;
        end Get_LR;

        procedure Release(C : Id) is
        begin
                null ;
        end Release;

   task body Philosopher is
          Self : Id ;
   begin
          accept GetId(Get_Unique : in Id)
                do Self := Get_Unique; end GetId;
      loop
         -- Thinking;
         Get_LR(Self);
         Release(Self);
         -- Quitting;
      end loop;
   end Philosopher;


   P0 : Philosopher;
   P1 : Philosopher;
   P2 : Philosopher;
   P3 : Philosopher;
   P4 : Philosopher;

begin
   P0.Getid(0);
   P1.Getid(1);
   P2.Getid(2);
   P3.Getid(3);
   P4.Getid(4);
end Dining_Philosophers;

