public final class Chop {

	private int N ;
	private boolean available [ ] ;
	private boolean bookedRight[ ] ;
	private boolean bookedLeft[ ] ;

	Chop (int N) {
		this.N = N ;
		this.available = new boolean[N] ;
		this. bookedRight = new boolean[N] ;
		this. bookedLeft = new boolean[N] ;
		for (int i =0 ; i < N ; i++) {
			available[i] = true ; // non allocated stick
			bookedRight[i] = false;
			bookedLeft[i] = false;
		}
	}

	public synchronized void get_LR (int me) {
		while ( !available[me] || bookedRight[me]) {
			try { wait() ; } catch (InterruptedException e) {}
		}
		available[me] = false ; // left stick allocated
		bookedRight[(me + 1)% N] = true; // right stick booked

		// don�t release mutual exclusion lock and immediately requests second stick
		while ( !available[(me + 1)% N]) {
			try { wait() ; } catch (InterruptedException e) {}
		}
		available[(me + 1)% N] = false ; // both sticks allocated
		bookedRight[(me + 1)% N] = false; // no more reason for booking right stick
	}
	
	public synchronized void get_LR2(int me) {
		while ( !available[me] || bookedRight[me]) { 
			try { bookedLeft[me] = true ; wait() ; } catch (InterruptedException e) {} // left stick booked
		}
		available[me] = false ; // left stick allocated
		bookedLeft[me] = false; // no more reason for booking left stick 
		bookedRight[(me + 1)% N] = true; // right stick booked

		// don�t release mutual exclusion lock and immediately requests second stick
		while ( !available[(me + 1)% N] || bookedLeft[(me + 1)% N] ) {
			try { wait() ; } catch (InterruptedException e) {}
		}
		available[(me + 1)% N] = false ; // both sticks allocated
		bookedRight[(me + 1)% N] = false; // no more reason for booking right stick
	}


	public synchronized void release (int me) {
		available[me] = true ; available[(me + 1)% N] = true ;
		notifyAll();
	}
}
