public final class Chop {

	private int N ;
	private boolean available [ ] ;

	Chop (int N) {
		this.N = N ;
		this.available = new boolean[N] ;
		for (int i =0 ; i < N ; i++) {
			available[i] = true ; // non allocated stick
		}
	}

	public synchronized void get_LR (int me) {
		while ( !available[me]) {
			try { wait() ; } catch (InterruptedException e) {}
		}
		available[me] = false ; // left stick allocated

		// don�t release mutual exclusion lock and immediately requests second stick
		while ( !available[(me + 1)% N]) {
			try { wait() ; } catch (InterruptedException e) {}
		}
		available[(me + 1)% N] = false ; // both sticks allocated
	}

	public synchronized void release (int me) {
		available[me] = true ; available[(me + 1)% N] = true ;
		notifyAll() ;
	}
}
