import java.util.Random;

class Philosopher extends Thread {
    private static Random rand = new Random();
    private static int counter = 0;
    private int number = counter++;
    private Chop chop;
    private int sleepingDurationFactor = 0;
    private long maxTimes = 0;

    public Philosopher(Chop chop, int sleepingDurationFactor, long maxTimes) {
	this.chop = chop;
	this.sleepingDurationFactor = sleepingDurationFactor;
	this.maxTimes = maxTimes;
	start();
    }
    public void thinking() {
	System.out.println(this + " is thinking");
	if(sleepingDurationFactor > 0)
	    try {
		sleep(rand.nextInt(sleepingDurationFactor));
	    } catch(InterruptedException e) {
		throw new RuntimeException(e);
	    }
	System.out.println(this + " is hungry");
    }
    public void eating() {
	System.out.println(this + " is eating");
	if(sleepingDurationFactor > 0)
	    try {
		sleep(rand.nextInt(sleepingDurationFactor));
	    } catch(InterruptedException e) {
		throw new RuntimeException(e);
	    }
	System.out.println(this + " has finished eating");
    }
    public void quitting() {
	System.out.println(this + " has released its chopsticks");
	if(sleepingDurationFactor > 0)
	    try {
		sleep(rand.nextInt(sleepingDurationFactor));
	    } catch(InterruptedException e) {
		throw new RuntimeException(e);
	    }
    }
    public String toString() {
	return "Philosopher " + number;
    }
    public void run() {
	long i = 0;
	while(i++ < maxTimes) {
	    thinking();
	    try {
			chop.get_LR(number);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    eating();
	    try {
			chop.release(number);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    quitting();
	}
    }
}

public class Diner {
    public static void main(String[] args) {
	if(args.length < 1) {
	    System.err.println("usage:\n" +
			       "java Diner numberOfPhilosophers sleepingDurationFactor maxTimes\n" +
			       "A nonzero sleepingDurationFactor will generate a random sleep time during thinking() and eating()\n" +
			       "maxTimes is the maximum number of meals for each philosopher.\n"
			       );
	    System.exit(1);
	}
	int numberOfPhilosophers = 5;
	if ( args.length > 0 ) numberOfPhilosophers = Integer.parseInt(args[0]);
	Philosopher[] philosopher =
	    new Philosopher[numberOfPhilosophers];
	int sleepingDurationFactor = 0;
	if ( args.length > 1 ) sleepingDurationFactor = Integer.parseInt(args[1]);
	Chop chop = new Chop(numberOfPhilosophers);
	long maxTimes = 100;
	if ( args.length > 2 ) maxTimes = Long.parseLong(args[2]);
	int i = 0;
	while(i < philosopher.length) {
	    philosopher[i++] =
		new Philosopher(chop, sleepingDurationFactor, maxTimes);
	}
    }
}
