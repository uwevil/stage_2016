import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class Chop {
	private int N ;
	private boolean available[ ] ;
	private boolean bookedRight[ ] ;
	final Lock lock = new ReentrantLock();
	final Condition LeftAllocated[ ] ; 
	final Condition RightAllocated[ ] ;

	Chop (int N) {
		this.N = N ;
		this.available = new boolean[N] ;
		this. bookedRight = new boolean[N] ;
		LeftAllocated = new Condition[N];
		RightAllocated = new Condition[N];
		for (int i =0 ; i < N ; i++) {
			available[i] = true ; // non allocated stick
			bookedRight[i] = false;
			LeftAllocated[i] = lock.newCondition();
			RightAllocated[i] = lock.newCondition();
		}
	}

	public void get_LR(int me) throws InterruptedException {
		lock.lock();
		try {
			while ( !available[me] || bookedRight[me] ) 
				LeftAllocated[me].await();
			available[me] = false ;                        // left stick allocated
			bookedRight[(me + 1)% N] = true; // right stick booked

			// don�t release mutual exclusion lock and immediately requests second stick
			while ( !available[(me + 1)% N] )
 				RightAllocated[(me + 1)% N].await();
			available[(me + 1)% N] = false ;        // both sticks allocated
			bookedRight[(me + 1)% N] = false; // no more reason for booking right stick
		} finally {
 			lock.unlock();
		}
	}

	public synchronized void release (int me) throws InterruptedException {
		lock.lock();
		available[me] = true ; available[(me + 1)% N] = true ;
		if (bookedRight[me]) RightAllocated[me].signal();  // booked by the left neighbour
		LeftAllocated[(me + 1)% N].signal();
		lock.unlock();
	}
 }

