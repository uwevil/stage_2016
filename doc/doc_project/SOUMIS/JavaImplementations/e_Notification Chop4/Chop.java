
public final class Chop {

	private int N ;
	private boolean available[ ] ;
	private boolean requesting[ ] ; // 
	private Object Allocated[ ] ; // similar role as private semaphores
	private int score [ ] ; // number of allocated sticks to each philosopher

	Chop (int N) {
		this.N = N ;
		this.available = new boolean[N] ;
		this. requesting = new boolean[N] ;
		this. Allocated = new Object[N];
		this.score = new int[N] ;
		for (int i =0 ; i < N ; i++) {
			available[i] = true ; // non allocated stick
			requesting[i] = false;
			Allocated[i] = new Object() ;
			score[i] = 0 ;
		}
	}
	public void get_LR(int me) {
		synchronized (Allocated[me]) {
			if (successFirstTime(me)) return;
			else 
				while (requesting[me]) {
					try { Allocated[me].wait() ; } catch (InterruptedException e) {}
					// when Allocated[me].notified is posted, requesting[me] is
					// false
				}
			}
		}

	private synchronized boolean successFirstTime (int me) {
		// successFirstTime is true when both sticks are granted,
		// score and requesting provide more about philosopher state
		score[me] = 0 ; requesting[me] = true ;
		if ( available[me] && available[(me + 1)% N] ){
			score[me] = 2 ; requesting[me] = false ;
			available[me] = false ; available[(me + 1)% N] = false ;
		}
		else if (available[me]){
			score[me] = 1 ; available[me] = false ; 
		}
		return score[me] == 2 ;
	}

	public synchronized void release (int me) {
		available[me] = true ; available[(me + 1)% N] = true ; score[me] = 0 ;
		// now waiting philosopher are served preferentially and during this
		// synchronized section
		if ( requesting[(N + me - 1)% N] && score[(N + me - 1)% N] == 1 ) { 
			// the left neighbour has already its left stick
			available [me] = false ; score[(N + me - 1)% N] = 2 ;
			requesting[(N + me - 1)% N] = false ; 
			synchronized (Allocated[(N + me - 1)% N]) {
				Allocated[(N + me - 1)% N].notify(); 
			}
		}
		if ( requesting[(me +1)% N] ) {
			// the right neighbour is waiting its first stick
			available[(me + 1)% N] = false ; score[(me + 1)% N] = 1 ;
			if (available[(me + 2)% N]) {
				available [(me + 2)% N] = false ; score[(me + 1)% N] = 2 ;
				requesting[(me + 1)% N] = false ; 
				synchronized (Allocated[(me + 1)% N]) {
					Allocated[(me + 1)% N].notify(); 
				}
			}
		}
	}
}

