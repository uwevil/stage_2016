
package body Mall is

   protected Cooperation_Synchro is
      entry Cooperate(X: in Id_Chameneos; C: in Colour; C_Other: out Colour);
   private
      entry Waiting(X: in Id_Chameneos; C: in Colour; C_Other: out Colour);
      First_Call : Boolean := True;
      A_Colour   : Colour;
      B_Colour   : Colour;
   end Cooperation_Synchro;

   protected body Cooperation_Synchro is
      entry Cooperate(X: in Id_Chameneos; C: in Colour; C_Other: out Colour) when True is
      begin
         if (First_Call) then
            A_Colour := C;
            First_Call := False;
            requeue Waiting;
         else
            B_Colour := C;
            C_Other := A_Colour;
            First_Call := True;
         end if;
      end Cooperate;

      entry Waiting(X: in Id_Chameneos; C: in Colour; C_Other: out Colour) when First_Call is
      begin
         C_Other := B_Colour;
      end;
   end Cooperation_Synchro;

   function Cooperation(Xa: Id_Chameneos; Ca: Colour) return Colour is
      Other_Colourb : Colour;
   begin
      Cooperation_Synchro.Cooperate(Xa, Ca, Other_Colourb);
      return Other_Colourb;
   end Cooperation;
end Mall;
