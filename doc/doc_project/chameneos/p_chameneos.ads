with P_Id_Chameneos; use P_Id_Chameneos;
with P_Colour; use P_Colour;
with Mall; use Mall;
with Text_IO; use Text_IO;
package P_Chameneos is
   task type Chameneos is
      entry Start(Id2: in Id_Chameneos; C2: in Colour);
   end Chameneos;
end P_Chameneos;
