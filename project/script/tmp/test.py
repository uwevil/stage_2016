import matplotlib.pyplot as plt
#plt.ylabel('some numbers')
#plt.xlabel("test")

#fig, ax = plt.subplots()
#plt.plot([3,4,5, 7], [187.480,176.048, 175.952, 165.935], 'b-', label='Minisat 2.2.1')
#plt.plot([3,4,5, 7], [248.480,232.287, 200.807, 244.742], 'g:', label='Glucose 4.0')
#plt.plot([12,5,8,21], [1,4,9,16], 'k--', label='Yices 2.4.1')

#plt.plot([3,4, 3, 4], [1,4,9,16], 'bo', label='OK')
#plt.plot([3,4,5,7,3,4,5,7], [187.480,176.048, 175.952, 165.935,248.480,232.287, 200.807, 244.742], 'ro', label='KO')

#plt.plot([1,3,0,11], [7,12,4,18], 'k<', label='Unknown')
#plt.plot([1,10,20,5], [9,2,1,13], 'k>', label='TimeOut')

#plt.axis([0, 20, 0, 20])
#plt.savefig("test", dpi=None, facecolor='w', edgecolor='w',
 #       orientation='portrait', papertype=None, format=None,
  #      transparent=False, bbox_inches=None, pad_inches=0.1,
   #     frameon=None)

#plt.figure()
#plt.ylabel('some numbers')
#plt.xlabel("test")
#plt.close()

#fig=plt.figure("test")

#plt.plotfile("test.txt", cols=(0,1), plotfuncs=None, comments='#', skiprows=0, checkrows=0, delimiter=';', names=None, subplots=True, newfig=False)
#plt.xlabel("abscisse")
#plt.ylabel("ordonnee")
#plt.axis('tight')
#(xmin, xmax) = plt.xlim()
#plt.xlim(0, xmax+1)

#(ymin, ymax) = plt.ylim()
#plt.ylim(ymin, ymax)
#ax.set_xticklabels([0,'','',3,4,5,'','N'])

#plt.grid(True)

f, axarr = plt.subplots(3, sharex=True)

#axarr[0].title('')

axarr[0].plot([4,5,6,7,8,9,10], [29129,34958,40787,46616,52445,58274,64103], 'b-s', label='Current')
axarr[0].plot([4,5,6,7,8,9,10], [17399,20868,24337,27806,31275,34744,38213], 'g-.s', label='Potential')

axarr[0].grid(True)

axarr[0].set_ylabel('Number of steps')

axarr[0].axis('tight')

axarr[0].set_xlim(0, 11)
axarr[0].set_ylim(0, 70000)

axarr[0].legend(loc='upper left', title='Number of steps', shadow=False, fontsize='small')


axarr[01].plot([4,5,6,7,8,9,10], [422536,509729,596994,684333,771704,859075,946446], 'b-o', label='Current')
axarr[01].plot([4,5,6,7,8,9,10], [115523,140098,164745,189466,214219,238972,263725], 'g-.o', label='Potential')

axarr[01].grid(True)
axarr[01].set_ylabel('Number of variables')

axarr[01].axis('tight')
axarr[01].set_ylim(0, 1000000)
axarr[01].legend(loc='upper left', title='Number of variables', shadow=False, fontsize='small')


axarr[2].plot([4,5,6,7,8,9,10], [1622333,1982332,2349879,2724982,3107459,3497184,3894157], 'b-^', label='Current')
axarr[2].plot([4,5,6,7,8,9,10], [489287,609696,735737,867418,1004585,1147112,1294999], 'g-.^', label='Potential')

axarr[2].grid(True)

axarr[2].set_ylabel('Number of clauses')
axarr[2].set_xlabel('context switch')

axarr[2].axis('tight')

axarr[2].set_xlim(0, 11)
axarr[2].set_ylim(0, 4500000)

axarr[2].legend(loc='upper left', title='Number of clauses', shadow=False, fontsize='small')




#legend = plt.legend(loc='upper left', title='', shadow=False, fontsize='small')
plt.savefig("test1.png", dpi=400, facecolor='w', edgecolor='w',orientation='portrait', papertype=None, format=None,transparent=False, bbox_inches=None, pad_inches=0.1,frameon=None)
#plt.show()

