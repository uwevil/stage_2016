import glob
import multiprocessing
import os
import sys
import subprocess
import signal

import multiprocessing as mp
import time

MAXCPU = multiprocessing.cpu_count()
TEST = False

def getFileName(fileIn, extension):
    return fileIn[(fileIn.rfind("/")+1):fileIn.rfind(extension)]

def isCreatedAt(time, name):
    p = re.compile(time+'*')
    return p.match(name)

def getLoops(line):
    start=len("Not unwinding loop main.0 iteration ")
    end=line.find(" (")
    return int(line[start:end])

def getSteps(line):
    start=len("size of program expression: ")
    end=line.find(" steps")
    return int(line[start:end])

def getSolverSAT(line):
    start=len("Solving with ")
    end=line.find(" with simplifier")
    return line[start:end].replace(" ", "-")

def getSolverSMT(line):
    start=len("Passing problem to SMT2 QF_AUFBV using ")
    s = line[start:]
    return s.strip()

def getVarCla(line):
    var = int(line[:line.find(" variables,")])
    cla = int(line[line.find(", ")+2:line.find(" clauses")])
    return (var, cla)

def getTime(line):
    start=len("real	")
    end=line.rfind("m")
    t=0
    if end != -1:
        t = t + int(line[start:end])*60
        start=line.find("m")+1
    end=line.find(".")
    t = t + int(line[start:end])
    return str(t)+line[end:len(line)-2]

def isInLine(s, line):
    return s in line

def parseResult(lines):
    name=""
    loops = 0
    steps = 0
    variables = 0
    clauses = 0
    util = "no"
    violated = False
    isGood = "unknown"
    isOk = False
    isTimeOut=False
    time = ""
    for line in lines:
        if isInLine("Parsing", line):
            name=getFileName(line, ".c")
        if isInLine("Not unwinding loop main.0 iteration ", line):
            loops = getLoops(line)-1
            continue
        if isInLine("size of program expression: ",line):
            steps = getSteps(line)
            continue
        if isInLine("Solving with ",line):
            util = getSolverSAT(line)
            continue
        if isInLine("Passing problem to SMT2 QF_AUFBV using ",line):
            util = getSolverSMT(line)
            continue
        if isInLine("variables", line) and isInLine("clauses",line):
            (variables, clauses) = getVarCla(line)
            continue
        if isInLine("Violated property:",line):
            violated=True
            continue
        if isInLine(" OK",line) and violated:
            isOk=True
            tmp=line.strip()
            if len(tmp)>3:
                isGood="KO"
            else:
                isGood="OK"
            continue
        if isInLine("VERIFICATION SUCCESSFUL",line):
            isOk=True
            isGood="OK"
            continue
        if isInLine("Alarm clock",line):
            isTimeOut=True
            continue
        if isInLine("real",line):
            if isOk:
                time = getTime(line)
                break
            elif not isOk and not isTimeOut:
                isGood="unknown"
                time = getTime(line)
                break
            elif not isOk and isTimeOut:
                isGood="timeOut"
                time = getTime(line)
                break
    if util == "no":
        return ""
    return name+";"+util+";"+str(loops)+";"+isGood+";"+time+";"+str(steps)+";"+str(variables)+";"+str(clauses)

def do_work(r):
    curproc = multiprocessing.current_process()
    print curproc, "Started Process, args={}".format(r)
    #cmd = ["time", "cbmc", "p_ko.c", "--unwind", str(r), "--function", "main"]
    cmd = ["make", "-f", "./maketest", "-i", "test", "UNWIND="+str(r)]
    #cmd = ["ls", "-a"]
    try:
        return subprocess.check_output(cmd, shell=False,stderr=subprocess.STDOUT)
    finally:
        print curproc, "Ended Process"

def do_work2(r):
    curproc = multiprocessing.current_process()
    print curproc, "Started Process, args={}".format(r)
    #cmd = ["time", "cbmc", "p_ko.c", "--unwind", str(r), "--function", "main"]
    cmd = ["make", "-f", "./maketest", "-i", "test2", "UNWIND="+str(r)]
    #cmd = ["ls", "-a"]
    try:
        return subprocess.check_output(cmd, shell=False,stderr=subprocess.STDOUT)
    finally:
        print curproc, "Ended Process"

def log_result(result):
    pid = os.getpid()
    print(str(pid))
    
    lines = result.split("\n")
    
    s = parseResult(lines)
    
    with open("test2.txt", 'a') as fo:
        fo.write(s+"\n")
        fo.write(result)
    print("Event")

    if not "OK" in s:
        return
    print("Event.set")
    event.set()
    pool.terminate()

def apply_async_with_callback(pool, event):
    for i in range(3,4):
        tmp = pool.apply_async(do_work, args = (i,), callback = log_result)
        print(tmp)
        tmp = pool.apply_async(do_work2, args = (i,), callback = log_result)
        print(tmp)
    pool.close()
    event.wait()
    pool.terminate()
    print("Pool.terminate")
 #   pool.join()

if __name__ == '__main__':
    pool = mp.Pool(4)
    manager = mp.Manager()
    event = manager.Event()
    
    if not os.path.exists("test2.txt"):
        os.open("test2.txt", os.O_CREAT)
    else:
        os.open("test2.txt", os.O_TRUNC)
    if not os.path.exists("test22.txt"):
        os.open("test22.txt", os.O_CREAT)
    else:
        os.open("test22.txt", os.O_TRUNC)

    apply_async_with_callback(pool, event)

    #if TEST:
     #   cp = MAXCPU
#    list_pdb = glob.glob('t*.py')
  #  else:
   #     cp = int(raw_input("Enter Number of processes to use (%d CPUs) = " % MAXCPU))
#    list_pdb = glob.glob('*.pdb') # Input PDB files
# assert cp <= MAXCPU

#print '{} files, {} procs'.format(len(list_pdb), cp)
#assert len(list_pdb) != 0

  #  pool = multiprocessing.Pool(cp)
#print pool.map(do_work, [(".", cp), ("../", cp)],)

   # pool.close()
   # pool.join()







