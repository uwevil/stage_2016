import os
import re
import sys
import fileinput

def parser(fileName) :
    """
        Parse 'fileName'.
        
        Return : 
            List of lines.
    """
    if not os.path.isfile(fileName) or ".DS" in fileName:
        return
    lines = list()
    with open(fileName, 'r') as fileIn:
        lines = fileIn.readlines()
    return lines

def isBegin(line):
    """
        Test if 'line' is the marker '@begin'
        
        Return :
            Bool
    """
    return "//@begin" in line

def isEnd(line):
    """
        Test if 'line' is the marker '@end'
        
        Return :
            Bool
    """
    return "//@end" in line

def isControlBegin(line):
    """
        Test if 'line' is the marker '@controlBegin'. This marker is used at the begin of condition statement for verification, it is NOT in the input program.
        
        Return :
            Bool
    """
    return "//@controlBegin" in line

def isControlEnd(line):
    """
        Test if 'line' is the marker '@controlEnd'. This marker is used at the end of condition statement for verification, it is NOT in the input program.
        
        Return :
            Bool
    """
    return "//@controlEnd" in line

def isControlLabel(line):
    """
        Test if 'line' is the marker '@controlLabel'. This marker is used for retain the current label , it is NOT in the input program.
        
        Return :
            Bool
    """
    return "//@controlLabel" in line

def isControlAssume(line):
    """
        Test if 'line' is the marker '@controlAssume'. This marker is intended for add automatically a step control after each statement, it is NOT in the input program.
        
        Return :
            Bool
    """
    return "//@controlAssume" in line

def isNoControlBegin(line):
    return "//@noControlBegin" in line

def isNoControlEnd(line):
    return "//@noControlEnd" in line

def isValidLine(line):
    """
        Test if 'line' is a valid line, which contains at least an valid statement.
        
        Return :
            Bool
    """
    p = re.compile('\W*\w+') #'   a=1;'
    p1 = re.compile('\W*//.*') #'  //'
    p2 = re.compile('\W*/\*.*\*/') #' /*...*/'
    p3 = re.compile('.*}el.*(.*){') # '   }else if (...) {'
    return p.match(line) and not p1.match(line) and not p2.match(line) and not p3.match(line);

def generator(fileNameIn, fileNameOut):
    lines = parser(fileNameIn)
    if (len(lines) <= 0):
        exit()
    
    currentLabel = 0
    
    controlLabel = -1
    
    begin = False
    controlBegin = False
    noControl = False
    writable = True
    
    with open(fileNameOut, 'w') as fileOut:
        for line in lines:
            if isBegin(line):
                begin = True
                currentLabel = 0
                continue
            if isEnd(line):
                begin = False
                #fileOut.write("l"+str(currentLabel)+":\n return;\n")
                fileOut.write("end:\n return;\n")
                continue

            if begin:
                writable = True;
                if isControlBegin(line):
                    controlBegin = True
                    fileOut.write("l" + str(currentLabel) + ": if (pc[ct]>" + str(currentLabel) + "||" + str(currentLabel) + ">=cs) goto l" + str(currentLabel+1) + ";\n")
                    currentLabel = currentLabel + 1
                    writable = False
                elif isControlEnd(line):
                    controlBegin = False
                    writable = False
                elif isControlAssume(line):
                    fileOut.write("__CPROVER_assume(cs>="+str(currentLabel)+");\n\n")
                    writable = False
                elif isNoControlBegin(line):
                    noControl = True
                    writable = False
                elif isNoControlEnd(line):
                    noControl = False
                    writable = False
                elif isControlLabel(line):
                    controlLabel = currentLabel
                    writable = False
                elif controlBegin:
                    if "cs = ?;" in line:
                        if controlLabel != -1:
                            fileOut.write(line.replace("?", str(controlLabel), 1));
                         #   controlLabel = -1
                        else:
                            fileOut.write(line.replace("?", str(currentLabel-1), 1));
                        writable = False
                elif isValidLine(line) and not controlBegin and not noControl:
                    fileOut.write("l" + str(currentLabel) + ": if (pc[ct]>" + str(currentLabel) + "||" + str(currentLabel) + ">=cs) goto l" + str(currentLabel+1) + ";\n")
                    fileOut.write(line)
                    currentLabel = currentLabel + 1
                    writable = False
                if writable:
                    tmp=line.strip()
                    if len(tmp)<1:
                        continue
                    fileOut.write(line)
            else:
                fileOut.write(line)

    lines = parser(fileNameOut)
    if (len(lines) <= 0):
        exit()
    
    s = "l" + str(currentLabel-1) + ": if (pc[ct]>" + str(currentLabel-1) + "||" + str(currentLabel-1) + ">=cs) goto l" + str(currentLabel) + ";\n"
    s2 = "l"+str(currentLabel)

    for line in fileinput.input(fileNameOut, inplace=True):
        if s in line:
            line = line.replace(s2, "end")
        sys.stdout.write(line)



if __name__ == "__main__" :
    if len(sys.argv) < 2:
        print("Entrez le chemin du fichier source et du fichier sorti\n")
        sys.exit(1)
    
    generator(str(sys.argv[1]), str(sys.argv[2]))

