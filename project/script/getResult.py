import os
import re
import sys
import fileinput
import math

import matplotlib.pyplot as plt

from datetime import datetime
from parser2 import *

def getFileName(fileIn, extension):
    """
        Get the file name without 'extension' from a path 'fileIn'
        
        Return :
            String
    """
    return fileIn[(fileIn.rfind("/")+1):fileIn.rfind(extension)]

def isCreatedAt(time, name):
    """
        Test if a string 'name' contains a time 'time'.
        
        Return:
            Bool
    """
    p = re.compile(time+'*')
    return p.match(name)

def getLoops(line):
    """
        Get the number of loops in a string 'line'
        
        Return:
            Integer
    """
    start=len("Not unwinding loop main.0 iteration ")
    end=line.find(" (")
    return int(line[start:end])

def getSteps(line):
    """
        Get the number of steps in a string 'line'
        
        Return:
            Integer
    """

    start=len("size of program expression: ")
    end=line.find(" steps")
    return int(line[start:end])

def getSolverSAT(line):
    """
        Get the SAT solver name in a string 'line'
        
        Return:
            String
    """

    start=len("Solving with ")
    end=line.find(" with simplifier")
    return line[start:end].replace(" ", "-")

def getSolverSMT(line):
    """
        Get the SMT solver name in a string 'line'
        
        Return:
            String
    """
    start=len("Passing problem to SMT2 QF_AUFBV using ")
    s = line[start:]
    return s.strip()

def getVarCla(line):
    """
        Get a tuple (var, cla) for the number of variables and clauses respectively in a string 'line'
        
        Return:
            Tuple
    """
    var = int(line[:line.find(" variables,")])
    cla = int(line[line.find(", ")+2:line.find(" clauses")])
    return (var, cla)

def getTime(line):
    """
        Get the real total time from the begin to the end of vefication in a string 'line'
        
        Return:
            Integer
    """
    start=len("real	")
    end=line.rfind("m")
    t=0
    if end != -1:
        t = t + int(line[start:end])*60
        start=line.find("m")+1
    end=line.find(".")
    t = t + int(line[start:end])
    return str(t)+line[end:len(line)-2]

def isInLine(s, line):
    """
        Test if a string 's' is in a string 'line'
        
        Return:
            Bool
    """
    return s in line

def generateFileResult(f, fileOut, complete):
    """
        Generate a file 'fileOut' that contains the result after parsing from the output file 'f' of the verification tool with the type of procedure used 'complete'.
        Return:
            NULL
    """
    if not os.path.isfile(f) or ".DS" in f:
        return
    lines = parser(f)
    if (len(lines) <= 0):
        exit()
    s = parseResult(lines, complete)
    if len(s)<2:
        return
    with open(fileOut, 'a') as fo:
        fo.write(s+"\n")

def parseResult(lines, complete):
    """
        Read the list of lines 'lines' with the procedure used 'complete' to get the information.
        
        Return:
            String
    """
    name=""
    loops = 0
    steps = 0
    variables = 0
    clauses = 0
    util = "no"
    violated = False
    isGood = "unknown"
    isOk = False
    isTimeOut=False
    time = ""
    for line in lines:
        if isInLine("Parsing", line):
            name=getFileName(line, ".c")
        if isInLine("Not unwinding loop main.0 iteration ", line):
            loops = getLoops(line)-1
            continue
        if isInLine("size of program expression: ",line):
            steps = getSteps(line)
            continue
        if isInLine("Solving with ",line):
            util = getSolverSAT(line)
            continue
        if isInLine("Passing problem to SMT2 QF_AUFBV using ",line):
            util = getSolverSMT(line)
            continue
        if isInLine("variables", line) and isInLine("clauses",line):
            (variables, clauses) = getVarCla(line)
            continue
        if isInLine("Violated property:",line):
            violated=True
            continue
        if isInLine(" OK",line) and violated:
            isOk=True
            tmp=line.strip()
            if len(tmp)>3:
                isGood="KO"
            else:
                isGood="OK"
            continue
        if isInLine("VERIFICATION SUCCESSFUL",line) and complete:
            isOk=True
            isGood="OK"
            continue
        if isInLine("Alarm clock",line) or isInLine("Minuterie", line):
            isTimeOut=True
            continue
        if isInLine("real",line):
            if isOk:
                time = getTime(line)
                break
            elif not isOk and not isTimeOut:
                isGood="Unknown"
                time = getTime(line)
                break
            elif not isOk and isTimeOut:
                isGood="TimeOut"
                time = getTime(line)
                break
    if util == "no":
        return ""
    return name+";"+util+";"+str(loops)+";"+isGood+";"+time+";"+str(steps)+";"+str(variables)+";"+str(clauses)

def getAllResult(dirIn, fileOut, complete):
    """
        Read all files in 'dirIn' to generate a file 'fileOut' with procedure used 'complete'.
        
        Return:
            NULL;
    """
    if len(dirIn)<1:
        dirIn="."
    listDir = os.listdir(dirIn)
    for d in listDir:
        d = os.path.join(dirIn, d)
        if len(d)<1:
            d="."
        if os.path.isdir(d):
            getAllResult(d, fileOut, complete)
            continue
        print(d)
        generateFileResult(d, fileOut, complete)

def isFileWithExtension(f, extension):
    """
        Test if a file 'f' contains 'extension'
        
        Return:
            Bool
    """
    return extension in f

def txtToPng(dirIn, f):
    """
        Generate an image '.png' from a file 'f' generated by 'generateFileResult' function.
        
        Return:
            NULL
    """
    if len(dirIn)<1:
        dirIn="."
    fname = f
    f = os.path.join(dirIn, f)
    if not os.path.isfile(f):
        return
    if not isFileWithExtension(fname, ".txt"):
        return
    fname = getFileName(fname, ".txt")
    print(fname)
    lines = parser(f)
    if (len(lines) <= 0):
        return
    
    mainList = list()
    listTrait = ['g--', 'b-', 'k-.', 'r:']
    solverList = list()
    
    stateSymbol = ['bo', 'ro', 'k<', 'k>']
    stateList = ['OK', 'KO', 'Unknown', 'TimeOut']
    plotList = [list(),list(),list(),list()]

    currentSolver = ""
    currentIndex = 0

    currentState = ""
    currentStateIndex = 0
    
    plt.figure(fname)
    fig, ax = plt.subplots()
    
    plt.ylabel('temps(s)')
    plt.xlabel('CC')
        
    for line in lines:
        #Glucose-Syrup;3;KO;84.386;31278;754124;3005944
        l = line.split(";")
        print(l)
        if len(l) <= 2 :
            continue
        if l[1] == "no":
            continue

        try:
            currentIndex = solverList.index(l[1])
            currentSolver = l[1]
            mainList[currentIndex].append((int(l[2]), l[4]))
        except:
            solverList.append(l[1])
            currentIndex = solverList.index(l[1])
            currentSolver = l[1]
            mainList.append(list())
            mainList[currentIndex].append((int(l[2]), l[4]))

        try:
            currentStateIndex = stateList.index(l[3])
            currentState = l[3]
            plotList[currentStateIndex].append((int(l[2]), l[4]))
        except:
            print("stateList error " + l[3])
            sys.exit(1)

    for currentSolver in solverList:
        currentIndex = solverList.index(currentSolver)
        listTmp = mainList[currentIndex]
        listTmp.sort(key=lambda tup: tup[0])
        if len(listTmp)>0:
            x = list()
            y = list()
            for (k, t) in listTmp:
                x.append(int(k))
                y.append(float(t))
            plt.plot(x, y, listTrait[currentIndex], label=currentSolver)

    for currentState in stateList:
        currentStateIndex = stateList.index(currentState)
        listTmp = plotList[currentStateIndex]
        if len(listTmp)>0:
            x = list()
            y = list()
            for (k, t) in listTmp:
                x.append(int(k))
                y.append(float(t))
            plt.plot(x, y, stateSymbol[currentStateIndex], label=currentState)


    plt.axis('tight')
    (xmin, xmax) = plt.xlim()
    plt.xlim(0, xmax+1)

    (ymin, ymax) = plt.ylim()
    if ymax<1:
        ymax=ymax+1
    elif ymax<5:
        ymax=5
    elif (ymax<50):
        ymax=ymax+10
    elif ymax<100:
        ymax=ymax+10;
    elif ymax<1000:
        ymax=ymax+100
    elif ymax<5000:
        ymax=ymax+1000
    elif ymax>5000:
        ymax=ymax+1000

    if ymin<1:
        ymin=0
    elif ymin<5:
        ymin=0
    elif (ymin<10):
        ymin=ymin-1
    elif ymin<100:
        ymin=ymin-10;
    elif ymin<1000:
        ymin=ymin-100
    elif ymin>1000:
        ymin=ymin-1000

    plt.ylim(ymin, ymax)
    #fig.suptitle(l[0], fontsize=12)
    plt.grid(True)

    #legend = plt.legend(loc='upper left',title=l[0], shadow=False, fontsize='small', fancybox=True, framealpha=0.5)
    legend = plt.legend(loc='best',title=l[0], shadow=False, fontsize='small', fancybox=True)

    #plt.show()
    plt.savefig(os.path.join(dirIn, fname+".png"), dpi=400, facecolor='w', edgecolor='w',orientation='portrait', papertype=None, format=None,transparent=True, bbox_inches='tight', pad_inches=0.1,frameon=None)
    plt.close()

def allResult(dirIn):
    if len(dirIn)<1:
        dirIn="."
    if not os.path.isdir(dirIn):
        return
    print(dirIn)
    listF = os.listdir(dirIn)
    for f in listF:
        txtToPng(dirIn, f)


if __name__ == "__main__" :
    if len(sys.argv) < 2:
        print("Entrez le chemin du repertoire source, de fichier sorti et de type de procedure\n")
        sys.exit(1)
    
    dirIn = str(sys.argv[1])
    fileOut = str(sys.argv[2])
    complete = False

    try:
        if str(sys.argv[3])=="yes":
            complete = True
    except:
        complete = False

    if not os.path.isdir(dirIn):
        print("Entrez le repertoire source correctement : "+dirIn)
        sys.exit(1)

    (head, tail) = os.path.split(fileOut)
    if len(head)>0 and not os.path.isdir(head):
        os.makedirs(head)

    if not os.path.exists(fileOut):
        os.open(fileOut, os.O_CREAT)
    else:
        os.open(fileOut, os.O_TRUNC)

    getAllResult(dirIn, fileOut, complete)
    #generateFileResult(dirIn, fileOut)

    #allResult(head)
    txtToPng(head, tail)





