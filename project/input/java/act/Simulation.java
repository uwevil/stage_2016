public class Simulation{
    public static void main (String args[]){
        Monitor m = new Monitor();
        int NB_TASK = 3;
        ActThread[] actThreadArray = new ActThread[NB_TASK];
                
        for (int i = 0; i<NB_TASK; i++){
            actThreadArray[i] = new ActThread(i, m);
        }
        
        for (int i = 0; i<NB_TASK; i++){
            actThreadArray[i].start();
        }
        
        for (int i = 0; i<NB_TASK; i++){
            try{
                actThreadArray[i].join();
            }catch(InterruptedException e){}
        }
    }
}