public class Mall{
    private Colour AColour, BColour;
    private boolean FirstCall = true;
    private boolean MustWait = false;
    private IdChameneos a, b;
    private Combo combo;
    
    public synchronized Combo Cooperation(IdChameneos x, Colour C){
        Colour result;
        
        while(MustWait){
            try{wait();} catch(InterruptedException e){}
        }
        if (FirstCall){
            AColour = C;
            a = x;
            FirstCall=false;
            while(!FirstCall){
                try{wait();}catch(InterruptedException e){}
            }
            MustWait = false;
            result = BColour;
            combo = new Combo(b.getId(), result);
            notifyAll();
        }else{
            BColour = C;
            b = x;
            result = AColour;
            FirstCall = true;
            MustWait = true;
            combo = new Combo(a.getId(), result);

            notifyAll();
        }
        return combo;
    }
}