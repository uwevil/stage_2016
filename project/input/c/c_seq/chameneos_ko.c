#include <stdio.h>
#include <assert.h>

#define TRUE 1
#define FALSE 0
#define FREE -1

#define NB_TASK 6 //without main task
#define NB_ENTRY 2
#define NB_PROCEDURE 0
#define NB_FUNCTION 0

/*-------------------------------------------------------------*/
int active[NB_TASK+1] = {TRUE, FALSE, FALSE};
int cs, ct;
int pc[NB_TASK+1];
int size[NB_TASK+1];
int dead = 0;

void create(int t, int id){
    active[id] = TRUE;
    t = id;
}
/*-------------------------------------------------------------*/
int tabStatus[NB_TASK+1] = {TRUE, TRUE, TRUE, TRUE};
int tabWaiting[NB_TASK+1] = {0, 0, 0, 0};
int tabBarrier[NB_ENTRY+1] = {FALSE, FALSE, FALSE};

int currentTask=1;

int tabEntryCount[NB_ENTRY+1] = {0,0,0};
int taskInWait = 0;

int externalLock = FREE;

void lock(int id){
    __CPROVER_assume(externalLock==FREE);
    externalLock=id;
}

void transfert(int id, int newId){
    __CPROVER_assert(externalLock==id, "OKexternalLock!=id");
    externalLock=newId;
}

void unlock(int id){
    __CPROVER_assert(externalLock==id, "OKexternalLock!=id");
    externalLock=FREE;
}
/*---------------------------------------------------------------------------*/
#define BLUE 1
#define RED 2
#define YELLOW 3

typedef int colour;

int firstCall = TRUE;
colour aColour;
colour bColour;

int x1[NB_TASK+1];
colour c1[NB_TASK+1];
colour cOther1[NB_TASK+1];

colour complementary_colour(colour c1, colour c2){
    if (c1==c2){
        return c1;
    }else{
        return RED; //3-c1-c2;
    }
}

colour myColour[NB_TASK+1];
colour otherColour[NB_TASK+1];

/*---------------------------------------------------------------------------*/

void evaluate_barrier(int index){
    if (index == 1){ //Cooperate
        tabBarrier[index] = TRUE;
    }else if (index == 2){ //Waiting
        tabBarrier[index] = firstCall;
    }
}

void reevaluate_barrier(){
    tabBarrier[1] = TRUE; //Cooperate
    tabBarrier[2] = firstCall; //Waiting
}

void setTabStatusOn(int taskId){
    tabStatus[taskId] = TRUE;
    taskInWait = taskInWait - 1;
}

void setTabStatusOff(int taskId){
    tabStatus[taskId] = FALSE;
    taskInWait = taskInWait + 1;
    __CPROVER_assert(taskInWait<NB_TASK, "OKdeadlock");
}


void add_entry_queue(int taskId, int index){
    tabEntryCount[index] = tabEntryCount[index]+1;
    tabWaiting[taskId] = index;
    setTabStatusOff(taskId);
}

void remove_entry_queue(int taskId, int index){
    tabEntryCount[index] = tabEntryCount[index]-1;
    tabWaiting[taskId] = 0;
}

int current_task_eligible(){ //return taskId
    int j;
    for (j=1; j<=NB_TASK;j++){
        if (tabWaiting[currentTask]!=0&&tabBarrier[tabWaiting[currentTask]]){
            return currentTask;
        }
        currentTask = (currentTask % NB_TASK)+1;
    }
    return 0;
}

void run_entry_body(int taskId, int index);

void requeue(int taskId, int index){ //requeue to another or same entry queue
    evaluate_barrier(index);
    
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        remove_entry_queue(taskId, index);
    }
    run_entry_body(taskId,index);
}

void run_entry_body(int taskId, int index){
    if (index == 1) { //Request
        if (firstCall){
            aColour=c1[taskId];
            firstCall=FALSE;
            requeue(taskId, 2); //requeue Assign
        }else{
            bColour=c1[taskId];
            cOther1[taskId]=aColour;
            firstCall=TRUE;
        }
    }else if (index == 2){ //Assign
        cOther1[taskId]=bColour;
    }
}

int check_and_run_any_entry(){
    reevaluate_barrier();
    int taskIdEligible = current_task_eligible();
    if (taskIdEligible==0){
        return FREE;
    }
    setTabStatusOn(taskIdEligible);
    return taskIdEligible;
}

void run_function_call(taskId, index){
}

void run_procedure_call(taskId, index){
    int taskIdEligible;

    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
}

void run_entry_call(int taskId, int index){
    int taskIdEligible;
    evaluate_barrier(index);
    
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        remove_entry_queue(taskId, index);
    }
    run_entry_body(taskId,index);
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
}

void protected_object_call(int taskId, int appel, int index){
    lock(taskId);
    
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        run_entry_call(taskId, index);
    }
}


//yellow, blue, red, blue, yellow, blue

void t_one(){
    static int taskId=1;
    static int taskIdEligible;
    static int index;
l0: if (pc[ct]>0||0>=cs) goto l1;
    myColour[taskId]=YELLOW;
l1: if (pc[ct]>1||1>=cs) goto l2;
    otherColour[taskId]=0;
l2: if (pc[ct]>2||2>=cs) goto l3;
    lock(taskId);
l3: if (pc[ct]>3||3>=cs) goto l4;
    index=1;
    evaluate_barrier(index);
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
    l4: if (pc[ct]>4||4>=cs) goto l5;
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
    l5: if (pc[ct]>5||5>=cs) goto l6;
        remove_entry_queue(taskId, index);
    }
    __CPROVER_assume(cs>=6);
    
l6: if (pc[ct]>6||6>=cs) goto l7;
    //run_entry_body(taskId,index);
    index=1;
    c1[taskId]=myColour[taskId];
    if (index == 1) { //Request
        if (firstCall){
            aColour=c1[taskId];
            firstCall=FALSE;
            
            //requeue(taskId, 2); //requeue Assign
            index = 2;
            evaluate_barrier(index);
            if (tabBarrier[index]==FALSE){
                add_entry_queue(taskId, index);
                unlock(taskId);
            l7: if (pc[ct]>7||7>=cs) goto l8;
                __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
            l8: if (pc[ct]>8||8>=cs) goto l9;
                remove_entry_queue(taskId, index);
            }
            __CPROVER_assume(cs>=9);
            
            //run_entry_body(taskId,index);
        l9: if (pc[ct]>9||9>=cs) goto l10;
            cOther1[taskId]=bColour;
        }else{
            __CPROVER_assume(cs>=10);
            
        l10: if (pc[ct]>10||10>=cs) goto l11;
            bColour=c1[taskId];
            cOther1[taskId]=aColour;
            firstCall=TRUE;
            //@controlend
        }
        __CPROVER_assume(cs>=11);
        
    }
    __CPROVER_assume(cs>=11);
    
l11: if (pc[ct]>11||11>=cs) goto l12;
    //taskIdEligible = check_and_run_any_entry();
    taskIdEligible=FREE;
    transfert(taskId, taskIdEligible);
l12: if (pc[ct]>12||12>=cs) goto l13;
    otherColour[taskId]=cOther1[taskId];
    
l13: if (pc[ct]>13||13>=cs) goto l14;
    myColour[taskId]=complementary_colour(myColour[taskId], otherColour[taskId]);
l14:
    return;
}

//yellow, blue, red, blue, yellow, blue

void t_two(){
    static int taskId=2;
    static int taskIdEligible;
    static int index;
l0: if (pc[ct]>0||0>=cs) goto l1;
    myColour[taskId]=YELLOW;
l1: if (pc[ct]>1||1>=cs) goto l2;
    otherColour[taskId]=0;
l2: if (pc[ct]>2||2>=cs) goto l3;
    lock(taskId);
l3: if (pc[ct]>3||3>=cs) goto l4;
    index=1;
    evaluate_barrier(index);
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
    l4: if (pc[ct]>4||4>=cs) goto l5;
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
    l5: if (pc[ct]>5||5>=cs) goto l6;
        remove_entry_queue(taskId, index);
    }
    __CPROVER_assume(cs>=6);
    
l6: if (pc[ct]>6||6>=cs) goto l7;
    //run_entry_body(taskId,index);
    index=1;
    c1[taskId]=myColour[taskId];
    if (index == 1) { //Request
        if (firstCall){
            aColour=c1[taskId];
            firstCall=FALSE;
            
            //requeue(taskId, 2); //requeue Assign
            index = 2;
            evaluate_barrier(index);
            if (tabBarrier[index]==FALSE){
                add_entry_queue(taskId, index);
                unlock(taskId);
            l7: if (pc[ct]>7||7>=cs) goto l8;
                __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
            l8: if (pc[ct]>8||8>=cs) goto l9;
                remove_entry_queue(taskId, index);
            }
            __CPROVER_assume(cs>=9);
            
            //run_entry_body(taskId,index);
        l9: if (pc[ct]>9||9>=cs) goto l10;
            cOther1[taskId]=bColour;
        }else{
            __CPROVER_assume(cs>=10);
            
        l10: if (pc[ct]>10||10>=cs) goto l11;
            bColour=c1[taskId];
            cOther1[taskId]=aColour;
            firstCall=TRUE;
            //@controlend
        }
        __CPROVER_assume(cs>=11);
        
    }
    __CPROVER_assume(cs>=11);
    
l11: if (pc[ct]>11||11>=cs) goto l12;
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
l12: if (pc[ct]>12||12>=cs) goto l13;
    otherColour[taskId]=cOther1[taskId];
    
l13: if (pc[ct]>13||13>=cs) goto l14;
    myColour[taskId]=complementary_colour(myColour[taskId], otherColour[taskId]);
l14:
    return;
}

//yellow, blue, red, blue, yellow, blue

void t_three(){
    static int taskId=3;
    static int taskIdEligible;
    static int index;
l0: if (pc[ct]>0||0>=cs) goto l1;
    myColour[taskId]=YELLOW;
l1: if (pc[ct]>1||1>=cs) goto l2;
    otherColour[taskId]=0;
l2: if (pc[ct]>2||2>=cs) goto l3;
    lock(taskId);
l3: if (pc[ct]>3||3>=cs) goto l4;
    index=1;
    evaluate_barrier(index);
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
    l4: if (pc[ct]>4||4>=cs) goto l5;
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
    l5: if (pc[ct]>5||5>=cs) goto l6;
        remove_entry_queue(taskId, index);
    }
    __CPROVER_assume(cs>=6);
    
l6: if (pc[ct]>6||6>=cs) goto l7;
    //run_entry_body(taskId,index);
    index=1;
    c1[taskId]=myColour[taskId];
    if (index == 1) { //Request
        if (firstCall){
            aColour=c1[taskId];
            firstCall=FALSE;
            
            //requeue(taskId, 2); //requeue Assign
            index = 2;
            evaluate_barrier(index);
            if (tabBarrier[index]==FALSE){
                add_entry_queue(taskId, index);
                unlock(taskId);
            l7: if (pc[ct]>7||7>=cs) goto l8;
                __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
            l8: if (pc[ct]>8||8>=cs) goto l9;
                remove_entry_queue(taskId, index);
            }
            __CPROVER_assume(cs>=9);
            
            //run_entry_body(taskId,index);
        l9: if (pc[ct]>9||9>=cs) goto l10;
            cOther1[taskId]=bColour;
        }else{
            __CPROVER_assume(cs>=10);
            
        l10: if (pc[ct]>10||10>=cs) goto l11;
            bColour=c1[taskId];
            cOther1[taskId]=aColour;
            firstCall=TRUE;
            //@controlend
        }
        __CPROVER_assume(cs>=11);
        
    }
    __CPROVER_assume(cs>=11);
    
l11: if (pc[ct]>11||11>=cs) goto l12;
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
l12: if (pc[ct]>12||12>=cs) goto l13;
    otherColour[taskId]=cOther1[taskId];
    
l13: if (pc[ct]>13||13>=cs) goto l14;
    myColour[taskId]=complementary_colour(myColour[taskId], otherColour[taskId]);
l14:
    return;
}
//yellow, blue, red, blue, yellow, blue

void t_four(){
    static int taskId=4;
    static int taskIdEligible;
    static int index;
l0: if (pc[ct]>0||0>=cs) goto l1;
    myColour[taskId]=YELLOW;
l1: if (pc[ct]>1||1>=cs) goto l2;
    otherColour[taskId]=0;
l2: if (pc[ct]>2||2>=cs) goto l3;
    lock(taskId);
l3: if (pc[ct]>3||3>=cs) goto l4;
    index=1;
    evaluate_barrier(index);
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
    l4: if (pc[ct]>4||4>=cs) goto l5;
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
    l5: if (pc[ct]>5||5>=cs) goto l6;
        remove_entry_queue(taskId, index);
    }
    __CPROVER_assume(cs>=6);
    
l6: if (pc[ct]>6||6>=cs) goto l7;
    //run_entry_body(taskId,index);
    index=1;
    c1[taskId]=myColour[taskId];
    if (index == 1) { //Request
        if (firstCall){
            aColour=c1[taskId];
            firstCall=FALSE;
            
            //requeue(taskId, 2); //requeue Assign
            index = 2;
            evaluate_barrier(index);
            if (tabBarrier[index]==FALSE){
                add_entry_queue(taskId, index);
                unlock(taskId);
            l7: if (pc[ct]>7||7>=cs) goto l8;
                __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
            l8: if (pc[ct]>8||8>=cs) goto l9;
                remove_entry_queue(taskId, index);
            }
            __CPROVER_assume(cs>=9);
            
            //run_entry_body(taskId,index);
        l9: if (pc[ct]>9||9>=cs) goto l10;
            cOther1[taskId]=bColour;
        }else{
            __CPROVER_assume(cs>=10);
            
        l10: if (pc[ct]>10||10>=cs) goto l11;
            bColour=c1[taskId];
            cOther1[taskId]=aColour;
            firstCall=TRUE;
            //@controlend
        }
        __CPROVER_assume(cs>=11);
        
    }
    __CPROVER_assume(cs>=11);
    
l11: if (pc[ct]>11||11>=cs) goto l12;
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
l12: if (pc[ct]>12||12>=cs) goto l13;
    otherColour[taskId]=cOther1[taskId];
    
l13: if (pc[ct]>13||13>=cs) goto l14;
    myColour[taskId]=complementary_colour(myColour[taskId], otherColour[taskId]);
l14:
    return;
}
//yellow, blue, red, blue, yellow, blue

void t_five(){
    static int taskId=5;
    static int taskIdEligible;
    static int index;
l0: if (pc[ct]>0||0>=cs) goto l1;
    myColour[taskId]=YELLOW;
l1: if (pc[ct]>1||1>=cs) goto l2;
    otherColour[taskId]=0;
l2: if (pc[ct]>2||2>=cs) goto l3;
    lock(taskId);
l3: if (pc[ct]>3||3>=cs) goto l4;
    index=1;
    evaluate_barrier(index);
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
    l4: if (pc[ct]>4||4>=cs) goto l5;
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
    l5: if (pc[ct]>5||5>=cs) goto l6;
        remove_entry_queue(taskId, index);
    }
    __CPROVER_assume(cs>=6);
    
l6: if (pc[ct]>6||6>=cs) goto l7;
    //run_entry_body(taskId,index);
    index=1;
    c1[taskId]=myColour[taskId];
    if (index == 1) { //Request
        if (firstCall){
            aColour=c1[taskId];
            firstCall=FALSE;
            
            //requeue(taskId, 2); //requeue Assign
            index = 2;
            evaluate_barrier(index);
            if (tabBarrier[index]==FALSE){
                add_entry_queue(taskId, index);
                unlock(taskId);
            l7: if (pc[ct]>7||7>=cs) goto l8;
                __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
            l8: if (pc[ct]>8||8>=cs) goto l9;
                remove_entry_queue(taskId, index);
            }
            __CPROVER_assume(cs>=9);
            
            //run_entry_body(taskId,index);
        l9: if (pc[ct]>9||9>=cs) goto l10;
            cOther1[taskId]=bColour;
        }else{
            __CPROVER_assume(cs>=10);
            
        l10: if (pc[ct]>10||10>=cs) goto l11;
            bColour=c1[taskId];
            cOther1[taskId]=aColour;
            firstCall=TRUE;
            //@controlend
        }
        __CPROVER_assume(cs>=11);
        
    }
    __CPROVER_assume(cs>=11);
    
l11: if (pc[ct]>11||11>=cs) goto l12;
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
l12: if (pc[ct]>12||12>=cs) goto l13;
    otherColour[taskId]=cOther1[taskId];
    
l13: if (pc[ct]>13||13>=cs) goto l14;
    myColour[taskId]=complementary_colour(myColour[taskId], otherColour[taskId]);
l14:
    return;
}

//yellow, blue, red, blue, yellow, blue

void t_six(){
    static int taskId=6;
    static int taskIdEligible;
    static int index;
l0: if (pc[ct]>0||0>=cs) goto l1;
    myColour[taskId]=YELLOW;
l1: if (pc[ct]>1||1>=cs) goto l2;
    otherColour[taskId]=0;
l2: if (pc[ct]>2||2>=cs) goto l3;
    lock(taskId);
l3: if (pc[ct]>3||3>=cs) goto l4;
    index=1;
    evaluate_barrier(index);
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
    l4: if (pc[ct]>4||4>=cs) goto l5;
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
    l5: if (pc[ct]>5||5>=cs) goto l6;
        remove_entry_queue(taskId, index);
    }
    __CPROVER_assume(cs>=6);
    
l6: if (pc[ct]>6||6>=cs) goto l7;
    //run_entry_body(taskId,index);
    index=1;
    c1[taskId]=myColour[taskId];
    if (index == 1) { //Request
        if (firstCall){
            aColour=c1[taskId];
            firstCall=FALSE;
            
            //requeue(taskId, 2); //requeue Assign
            index = 2;
            evaluate_barrier(index);
            if (tabBarrier[index]==FALSE){
                add_entry_queue(taskId, index);
                unlock(taskId);
            l7: if (pc[ct]>7||7>=cs) goto l8;
                __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
            l8: if (pc[ct]>8||8>=cs) goto l9;
                remove_entry_queue(taskId, index);
            }
            __CPROVER_assume(cs>=9);
            
            //run_entry_body(taskId,index);
        l9: if (pc[ct]>9||9>=cs) goto l10;
            cOther1[taskId]=bColour;
        }else{
            __CPROVER_assume(cs>=10);
            
        l10: if (pc[ct]>10||10>=cs) goto l11;
            bColour=c1[taskId];
            cOther1[taskId]=aColour;
            firstCall=TRUE;
            //@controlend
        }
        __CPROVER_assume(cs>=11);
        
    }
    __CPROVER_assume(cs>=11);
    
l11: if (pc[ct]>11||11>=cs) goto l12;
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
l12: if (pc[ct]>12||12>=cs) goto l13;
    otherColour[taskId]=cOther1[taskId];
    
l13: if (pc[ct]>13||13>=cs) goto l14;
    myColour[taskId]=complementary_colour(myColour[taskId], otherColour[taskId]);
l14:
    return;
}

void init(){
    pc[0] = 0;
    size[0] = 0;
    
    pc[1] = 0;
    pc[2] = 0;
    pc[3] = 0;
    pc[4] = 0;
    pc[5] = 0;
    pc[6] = 0;
    size[1] = 14;
    size[2] = 14;
    size[3] = 14;
    size[4] = 14;
    size[5] = 14;
    size[6] = 14;
    
    int t1, t2, t3, t4, t5, t6;
    
    create(t1, 1);
    create(t2, 2);
    create(t3, 3);
    create(t1, 4);
    create(t2, 5);
    create(t3, 6);
}

int main(){
    init();
    
    for (;;) {
        ct = 1;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_one();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        ct = 2;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_two();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        ct = 3;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_three();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        ct = 4;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_four();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        ct = 5;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_five();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        ct = 6;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_six();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        __CPROVER_assert(taskInWait<2, "OK taskInWait==2");
        
        if (dead>0 && dead<NB_TASK){
            __CPROVER_assert(dead+taskInWait<NB_TASK, "OK dead+taskInWait>=NB_TASK");
        }
        
        if (pc[1] == size[1] && pc[2] == size[2] && pc[3] == size[3] && pc[4] == size[4] && pc[5] == size[5] && pc[6] == size[6]){
            __CPROVER_assert(FALSE, "OK");
        }
    }
    
    __CPROVER_assert(FALSE, "KO");
}