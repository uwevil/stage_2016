#include <stdio.h>
#include <assert.h>

#define TRUE 1
#define FALSE 0
#define FREE -1

#define NB_TASK 3 //without main task
#define NB_ENTRY 2
#define NB_PROCEDURE 1
#define NB_FUNCTION 0

/*-------------------------------------------------------------*/
int active[NB_TASK+1] = {TRUE, FALSE, FALSE, FALSE};
int cs, ct;
int pc[NB_TASK+1];
int size[NB_TASK+1];
int dead = 0;

void create(int t, int id){
    active[id] = TRUE;
    t = id;
}
/*-------------------------------------------------------------*/
int stock = 22;
int assignCount = 0;
int taskInWait = 0;

/*
public synchronized void request(int id, int y) {
        System.out.println(id + " IN Monitor: current stock = "+stock);
        try{
            while (assignCount != 0)
                wait();
            stock -= y;
            System.out.println(id + " IN Monitor: stock after resquest of " + id +" = "+stock);
            if (stock<0)
                assign(id, y);
        }catch(InterruptedException e){
        }finally{
        }
    }

    public synchronized void assign(int id, int y){
        assignCount++;
        System.out.println(id + " IN Monitor: " + id + " is waiting (assignCount = "+assignCount+")");
        try{
            assert assignCount<3 : "assignCount == 3";
            while (stock < 0)
                wait();
        }catch(InterruptedException e){
        }finally{
            assignCount--;
            System.out.println(id + " IN Monitor: " + id + " is getting out (assignCount = "+assignCount+")");
            notifyAll();
        }
    }

    public synchronized void release(int id, int y) {
        stock += y;
        System.out.println(id + " IN Monitor: " + id + " release "+ y + " unit (current stock = "+stock+")");
        notifyAll();
    }
*/


void t_one(){
    static int taskId=1;
    
    static int i = 0;
    static int MyClaim = 0;
    static int FullAllocation = 0;
    static int y;

   // for (i = 1; i<5; i++){
l0: if (pc[ct]>0||0>=cs) goto l1;
   i = 1;
l1: if (pc[ct]>1||1>=cs) goto l2;
           MyClaim = (5 - i) % 5; //deadlock
l2: if (pc[ct]>2||2>=cs) goto l3;
        //m.request(id, MyClaim);
        y = MyClaim;
        //atomic
        if (assignCount!=0){
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l3: if (pc[ct]>3||3>=cs) goto l4;
            __CPROVER_assume(!(assignCount != 0));
l4: if (pc[ct]>4||4>=cs) goto l5;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=5);

        stock -= y;
        if (stock<0){
            //assign(id, y);
            assignCount++;
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l5: if (pc[ct]>5||5>=cs) goto l6;
            __CPROVER_assume(!(stock < 0));
l6: if (pc[ct]>6||6>=cs) goto l7;
            assignCount--;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=7);

        //atomic
l7: if (pc[ct]>7||7>=cs) goto l8;
        FullAllocation = FullAllocation + MyClaim;
l8: if (pc[ct]>8||8>=cs) goto l9;
    i = 2;
l9: if (pc[ct]>9||9>=cs) goto l10;
           MyClaim = (5 - i) % 5; //deadlock
l10: if (pc[ct]>10||10>=cs) goto l11;
        //m.request(id, MyClaim);
        y = MyClaim;
         //atomic
        if (assignCount!=0){
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l11: if (pc[ct]>11||11>=cs) goto l12;
            __CPROVER_assume(!(assignCount != 0));
l12: if (pc[ct]>12||12>=cs) goto l13;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=13);

        stock -= y;
        if (stock<0){
            //assign(id, y);
            assignCount++;
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l13: if (pc[ct]>13||13>=cs) goto l14;
            __CPROVER_assume(!(stock < 0));
l14: if (pc[ct]>14||14>=cs) goto l15;
            assignCount--;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=15);

        //atomic
l15: if (pc[ct]>15||15>=cs) goto l16;
        FullAllocation = FullAllocation + MyClaim;
l16: if (pc[ct]>16||16>=cs) goto l17;
    i = 3;
l17: if (pc[ct]>17||17>=cs) goto l18;
           MyClaim = (5 - i) % 5; //deadlock
l18: if (pc[ct]>18||18>=cs) goto l19;
        //m.request(id, MyClaim);
        y = MyClaim;
         //atomic
        if (assignCount!=0){
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l19: if (pc[ct]>19||19>=cs) goto l20;
            __CPROVER_assume(!(assignCount != 0));
l20: if (pc[ct]>20||20>=cs) goto l21;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=21);

        stock -= y;
        if (stock<0){
            //assign(id, y);
            assignCount++;
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l21: if (pc[ct]>21||21>=cs) goto l22;
            __CPROVER_assume(!(stock < 0));
l22: if (pc[ct]>22||22>=cs) goto l23;
            assignCount--;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=23);

        //atomic
l23: if (pc[ct]>23||23>=cs) goto l24;
        FullAllocation = FullAllocation + MyClaim;
l24: if (pc[ct]>24||24>=cs) goto l25;
    i = 4;
l25: if (pc[ct]>25||25>=cs) goto l26;
           MyClaim = (5 - i) % 5; //deadlock
l26: if (pc[ct]>26||26>=cs) goto l27;
        //m.request(id, MyClaim);
        y = MyClaim;
        //atomic
        if (assignCount!=0){
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l27: if (pc[ct]>27||27>=cs) goto l28;
            __CPROVER_assume(!(assignCount != 0));
l28: if (pc[ct]>28||28>=cs) goto l29;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=29);

        stock -= y;
        if (stock<0){
            //assign(id, y);
            assignCount++;
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l29: if (pc[ct]>29||29>=cs) goto l30;
            __CPROVER_assume(!(stock < 0));
l30: if (pc[ct]>30||30>=cs) goto l31;
            assignCount--;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=31);

        //atomic
l31: if (pc[ct]>31||31>=cs) goto l32;
        FullAllocation = FullAllocation + MyClaim;
   // }
   // m.release(id, FullAllocation);
l32: if (pc[ct]>32||32>=cs) goto end;
   //atomic
    y=FullAllocation;
    stock += y;
   //atomic
end:
 return;
}





void t_two(){
    static int taskId=2;
    
    static int i = 0;
    static int MyClaim = 0;
    static int FullAllocation = 0;
    static int y;

  // for (i = 1; i<5; i++){
l0: if (pc[ct]>0||0>=cs) goto l1;
   i = 1;
l1: if (pc[ct]>1||1>=cs) goto l2;
           MyClaim = (5 - i) % 5; //deadlock
l2: if (pc[ct]>2||2>=cs) goto l3;
        //m.request(id, MyClaim);
        y = MyClaim;
        //atomic
        if (assignCount!=0){
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l3: if (pc[ct]>3||3>=cs) goto l4;
            __CPROVER_assume(!(assignCount != 0));
l4: if (pc[ct]>4||4>=cs) goto l5;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=5);

        stock -= y;
        if (stock<0){
            //assign(id, y);
            assignCount++;
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l5: if (pc[ct]>5||5>=cs) goto l6;
            __CPROVER_assume(!(stock < 0));
l6: if (pc[ct]>6||6>=cs) goto l7;
            assignCount--;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=7);

        //atomic
l7: if (pc[ct]>7||7>=cs) goto l8;
        FullAllocation = FullAllocation + MyClaim;
l8: if (pc[ct]>8||8>=cs) goto l9;
    i = 2;
l9: if (pc[ct]>9||9>=cs) goto l10;
           MyClaim = (5 - i) % 5; //deadlock
l10: if (pc[ct]>10||10>=cs) goto l11;
        //m.request(id, MyClaim);
        y = MyClaim;
         //atomic
        if (assignCount!=0){
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l11: if (pc[ct]>11||11>=cs) goto l12;
            __CPROVER_assume(!(assignCount != 0));
l12: if (pc[ct]>12||12>=cs) goto l13;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=13);

        stock -= y;
        if (stock<0){
            //assign(id, y);
            assignCount++;
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l13: if (pc[ct]>13||13>=cs) goto l14;
            __CPROVER_assume(!(stock < 0));
l14: if (pc[ct]>14||14>=cs) goto l15;
            assignCount--;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=15);

        //atomic
l15: if (pc[ct]>15||15>=cs) goto l16;
        FullAllocation = FullAllocation + MyClaim;
l16: if (pc[ct]>16||16>=cs) goto l17;
    i = 3;
l17: if (pc[ct]>17||17>=cs) goto l18;
           MyClaim = (5 - i) % 5; //deadlock
l18: if (pc[ct]>18||18>=cs) goto l19;
        //m.request(id, MyClaim);
        y = MyClaim;
         //atomic
        if (assignCount!=0){
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l19: if (pc[ct]>19||19>=cs) goto l20;
            __CPROVER_assume(!(assignCount != 0));
l20: if (pc[ct]>20||20>=cs) goto l21;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=21);

        stock -= y;
        if (stock<0){
            //assign(id, y);
            assignCount++;
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l21: if (pc[ct]>21||21>=cs) goto l22;
            __CPROVER_assume(!(stock < 0));
l22: if (pc[ct]>22||22>=cs) goto l23;
            assignCount--;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=23);

        //atomic
l23: if (pc[ct]>23||23>=cs) goto l24;
        FullAllocation = FullAllocation + MyClaim;
l24: if (pc[ct]>24||24>=cs) goto l25;
    i = 4;
l25: if (pc[ct]>25||25>=cs) goto l26;
           MyClaim = (5 - i) % 5; //deadlock
l26: if (pc[ct]>26||26>=cs) goto l27;
        //m.request(id, MyClaim);
        y = MyClaim;
        //atomic
        if (assignCount!=0){
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l27: if (pc[ct]>27||27>=cs) goto l28;
            __CPROVER_assume(!(assignCount != 0));
l28: if (pc[ct]>28||28>=cs) goto l29;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=29);

        stock -= y;
        if (stock<0){
            //assign(id, y);
            assignCount++;
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l29: if (pc[ct]>29||29>=cs) goto l30;
            __CPROVER_assume(!(stock < 0));
l30: if (pc[ct]>30||30>=cs) goto l31;
            assignCount--;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=31);

        //atomic
l31: if (pc[ct]>31||31>=cs) goto l32;
        FullAllocation = FullAllocation + MyClaim;
   // }
   // m.release(id, FullAllocation);
l32: if (pc[ct]>32||32>=cs) goto end;
   //atomic
    y=FullAllocation;
    stock += y;
   //atomic
end:
 return;
}





void t_three(){
    static int taskId=3;
    static int i = 0;
    static int MyClaim = 0;
    static int FullAllocation = 0;
    static int y;

   // for (i = 1; i<5; i++){
l0: if (pc[ct]>0||0>=cs) goto l1;
   i = 1;
l1: if (pc[ct]>1||1>=cs) goto l2;
           MyClaim = (5 - i) % 5; //deadlock
l2: if (pc[ct]>2||2>=cs) goto l3;
        //m.request(id, MyClaim);
        y = MyClaim;
        //atomic
        if (assignCount!=0){
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l3: if (pc[ct]>3||3>=cs) goto l4;
            __CPROVER_assume(!(assignCount != 0));
l4: if (pc[ct]>4||4>=cs) goto l5;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=5);

        stock -= y;
        if (stock<0){
            //assign(id, y);
            assignCount++;
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l5: if (pc[ct]>5||5>=cs) goto l6;
            __CPROVER_assume(!(stock < 0));
l6: if (pc[ct]>6||6>=cs) goto l7;
            assignCount--;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=7);

        //atomic
l7: if (pc[ct]>7||7>=cs) goto l8;
        FullAllocation = FullAllocation + MyClaim;
l8: if (pc[ct]>8||8>=cs) goto l9;
    i = 2;
l9: if (pc[ct]>9||9>=cs) goto l10;
           MyClaim = (5 - i) % 5; //deadlock
l10: if (pc[ct]>10||10>=cs) goto l11;
        //m.request(id, MyClaim);
        y = MyClaim;
         //atomic
        if (assignCount!=0){
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l11: if (pc[ct]>11||11>=cs) goto l12;
            __CPROVER_assume(!(assignCount != 0));
l12: if (pc[ct]>12||12>=cs) goto l13;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=13);

        stock -= y;
        if (stock<0){
            //assign(id, y);
            assignCount++;
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l13: if (pc[ct]>13||13>=cs) goto l14;
            __CPROVER_assume(!(stock < 0));
l14: if (pc[ct]>14||14>=cs) goto l15;
            assignCount--;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=15);

        //atomic
l15: if (pc[ct]>15||15>=cs) goto l16;
        FullAllocation = FullAllocation + MyClaim;
l16: if (pc[ct]>16||16>=cs) goto l17;
    i = 3;
l17: if (pc[ct]>17||17>=cs) goto l18;
           MyClaim = (5 - i) % 5; //deadlock
l18: if (pc[ct]>18||18>=cs) goto l19;
        //m.request(id, MyClaim);
        y = MyClaim;
         //atomic
        if (assignCount!=0){
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l19: if (pc[ct]>19||19>=cs) goto l20;
            __CPROVER_assume(!(assignCount != 0));
l20: if (pc[ct]>20||20>=cs) goto l21;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=21);

        stock -= y;
        if (stock<0){
            //assign(id, y);
            assignCount++;
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l21: if (pc[ct]>21||21>=cs) goto l22;
            __CPROVER_assume(!(stock < 0));
l22: if (pc[ct]>22||22>=cs) goto l23;
            assignCount--;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=23);

        //atomic
l23: if (pc[ct]>23||23>=cs) goto l24;
        FullAllocation = FullAllocation + MyClaim;
l24: if (pc[ct]>24||24>=cs) goto l25;
    i = 4;
l25: if (pc[ct]>25||25>=cs) goto l26;
           MyClaim = (5 - i) % 5; //deadlock
l26: if (pc[ct]>26||26>=cs) goto l27;
        //m.request(id, MyClaim);
        y = MyClaim;
        //atomic
        if (assignCount!=0){
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l27: if (pc[ct]>27||27>=cs) goto l28;
            __CPROVER_assume(!(assignCount != 0));
l28: if (pc[ct]>28||28>=cs) goto l29;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=29);

        stock -= y;
        if (stock<0){
            //assign(id, y);
            assignCount++;
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
l29: if (pc[ct]>29||29>=cs) goto l30;
            __CPROVER_assume(!(stock < 0));
l30: if (pc[ct]>30||30>=cs) goto l31;
            assignCount--;
            taskInWait = taskInWait - 1;
        }
__CPROVER_assume(cs>=31);

        //atomic
l31: if (pc[ct]>31||31>=cs) goto l32;
        FullAllocation = FullAllocation + MyClaim;
   // }
   // m.release(id, FullAllocation);
l32: if (pc[ct]>32||32>=cs) goto end;
   //atomic
    y=FullAllocation;
    stock += y;
   //atomic
end:
 return;
}






void init(){
    pc[0] = 0;
    size[0] = 0;
    
    pc[1] = 0;
    pc[2] = 0;
    pc[3] = 0;
    int ssize = 33;
    size[1] = ssize;//117;
    size[2] = ssize;
    size[3] =ssize;
    
    int t1, t2, t3;
    
    create(t1, 1);
    create(t2, 2);
    create(t3, 3);
}

int main(){
    init();
    
    for (;;) {
        ct = 1;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_one();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        ct = 2;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_two();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        ct = 3;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_three();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        if (pc[1] == size[1] && pc[2] == size[2] && pc[3] == size[3]){
            __CPROVER_assert(FALSE, "OK");
        }
    }
    
    __CPROVER_assert(FALSE, "KO");
}
