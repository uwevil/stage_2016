#include <stdio.h>
#include <assert.h>

#define TRUE 1
#define FALSE 0
#define FREE -1

#define NB_TASK 3 //without main task
#define NB_ENTRY 2
#define NB_PROCEDURE 1
#define NB_FUNCTION 0

/*-------------------------------------------------------------*/
int active[NB_TASK+1] = {TRUE, FALSE, FALSE};
int cs, ct;
int pc[NB_TASK+1];
int size[NB_TASK+1];
int dead = 0;

void create(int t, int id){
    active[id] = TRUE;
    t = id;
}
/*-------------------------------------------------------------*/
int tabStatus[NB_TASK+1] = {TRUE, TRUE, TRUE, TRUE};
int tabWaiting[NB_TASK+1] = {0, 0, 0, 0};
int tabBarrier[NB_ENTRY+1] = {FALSE, FALSE, FALSE};

int currentTask=1;

int y[NB_TASK+1] = {0,0,0,0};

int tabEntryCount[NB_ENTRY+1] = {0,0,0};
int taskInWait = 0;

int stock = 22;

int externalLock = FREE;

void lock(int id){
    __CPROVER_assume(externalLock==FREE);
    externalLock=id;
}

void transfert(int id, int newId){
    __CPROVER_assert(externalLock==id, "OKexternalLock!=id");
    externalLock=newId;
}

void unlock(int id){
    __CPROVER_assert(externalLock==id, "OKexternalLock!=id");
    externalLock=FREE;
}
/*---------------------------------------------------------------------------*/

void evaluate_barrier(int index){
    if (index == 1){ //Request
        tabBarrier[index] = tabEntryCount[2]==0;
    }else if (index == 2){ //Assign
        tabBarrier[index] = stock>=0;
    }
}

void reevaluate_barrier(){
    tabBarrier[1] = tabEntryCount[2]==0; //request
    tabBarrier[2] = stock>=0; //assign
}

void setTabStatusOn(int taskId){
    tabStatus[taskId] = TRUE;
    taskInWait = taskInWait - 1;
}

void setTabStatusOff(int taskId){
    tabStatus[taskId] = FALSE;
    taskInWait = taskInWait + 1;
    __CPROVER_assert(taskInWait<NB_TASK, "OKdeadlock");
}


void add_entry_queue(int taskId, int index){
    tabEntryCount[index] = tabEntryCount[index]+1;
    tabWaiting[taskId] = index;
    setTabStatusOff(taskId);
}

void remove_entry_queue(int taskId, int index){
    tabEntryCount[index] = tabEntryCount[index]-1;
    tabWaiting[taskId] = 0;
}

int current_task_eligible(){ //return taskId
    int j;
    for (j=1; j<=NB_TASK;j++){
        if (tabWaiting[currentTask]!=0&&tabBarrier[tabWaiting[currentTask]]){
            return currentTask;
        }
        currentTask = (currentTask % NB_TASK)+1;
    }
    return 0;
}

void run_entry_body(int taskId, int index);

void requeue(int taskId, int index){ //requeue to another or same entry queue
    evaluate_barrier(index);
    
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        remove_entry_queue(taskId, index);
    }
    run_entry_body(taskId,index);
}

void run_entry_body(int taskId, int index){
    if (index == 1) { //Request
        stock = stock - y[taskId];
        if (stock < 0){
            requeue(taskId, 2); //requeue Assign
        }
    }else if (index == 2){ //Assign
    }
}

int check_and_run_any_entry(){
    reevaluate_barrier();
    int taskIdEligible = current_task_eligible();
    if (taskIdEligible==0){
        return FREE;
    }
    setTabStatusOn(taskIdEligible);
    return taskIdEligible;
}

void run_function_call(taskId, index){
}

void run_procedure_call(taskId, index){
    int taskIdEligible;
    if (index == 1){
        stock = stock + y[taskId];
    }
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
}

void t_one(){
    static int taskId = 1;
    static int appel = 1;
    static int index = 1;
    static int taskIdEligible;
    static int taskIdTmp;
    static int i = 0;
    static int myClaim = 0;
    static int fullAllocation = 0;
l0: if (pc[ct]>0||0>=cs) goto l1;
    i = 1;
l1: if (pc[ct]>1||1>=cs) goto l2;
    myClaim = (5 + i) % 5;
    //protected_object_call(taskId, 2, 1);
l2: if (pc[ct]>2||2>=cs) goto l3;
    y[taskId] = myClaim;
l3: if (pc[ct]>3||3>=cs) goto l4;
    appel = 2;
l4: if (pc[ct]>4||4>=cs) goto l5;
    index = 1;
    
l5: if (pc[ct]>5||5>=cs) goto l6;
    lock(taskId);
    
l6: if (pc[ct]>6||6>=cs) goto l7;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=7);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=7);
        
        //run_entry_call(taskId, index);
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
        l7: if (pc[ct]>7||7>=cs) goto l8;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        l8: if (pc[ct]>8||8>=cs) goto l9;
            remove_entry_queue(taskId, index);
        }
        __CPROVER_assume(cs>=9);
        
        //run_entry_body(taskId,index);
    l9: if (pc[ct]>9||9>=cs) goto l10;
        if (index == 1) { //Request
            stock = stock - y[taskId];
            if (stock < 0){
                //requeue(taskId, 2); //requeue Assign
                index=2;
                evaluate_barrier(index);
                
                if (tabBarrier[index]==FALSE){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l10: if (pc[ct]>10||10>=cs) goto l11;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l11: if (pc[ct]>11||11>=cs) goto l12;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=12);
                
            }
            __CPROVER_assume(cs>=12);
            
        }
        __CPROVER_assume(cs>=12);
        
    l12: if (pc[ct]>12||12>=cs) goto l13;
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    __CPROVER_assume(cs>=13);
    
l13: if (pc[ct]>13||13>=cs) goto l14;
    fullAllocation = fullAllocation + myClaim;
    
l14: if (pc[ct]>14||14>=cs) goto l15;
    i = 2;
l15: if (pc[ct]>15||15>=cs) goto l16;
    myClaim = (5 + i) % 5;
    //protected_object_call(taskId, 2, 1);
l16: if (pc[ct]>16||16>=cs) goto l17;
    y[taskId] = myClaim;
l17: if (pc[ct]>17||17>=cs) goto l18;
    appel = 2;
l18: if (pc[ct]>18||18>=cs) goto l19;
    index = 1;
    
l19: if (pc[ct]>19||19>=cs) goto l20;
    lock(taskId);
    
l20: if (pc[ct]>20||20>=cs) goto l21;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=21);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=21);
        
        //run_entry_call(taskId, index);
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
        l21: if (pc[ct]>21||21>=cs) goto l22;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        l22: if (pc[ct]>22||22>=cs) goto l23;
            remove_entry_queue(taskId, index);
        }
        __CPROVER_assume(cs>=23);
        
        //run_entry_body(taskId,index);
    l23: if (pc[ct]>23||23>=cs) goto l24;
        if (index == 1) { //Request
            stock = stock - y[taskId];
            if (stock < 0){
                //requeue(taskId, 2); //requeue Assign
                index=2;
                evaluate_barrier(index);
                
                if (tabBarrier[index]==FALSE){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l24: if (pc[ct]>24||24>=cs) goto l25;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l25: if (pc[ct]>25||25>=cs) goto l26;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=26);
                
            }
            __CPROVER_assume(cs>=26);
            
        }
        __CPROVER_assume(cs>=26);
        
    l26: if (pc[ct]>26||26>=cs) goto l27;
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    __CPROVER_assume(cs>=27);
    
l27: if (pc[ct]>27||27>=cs) goto l28;
    fullAllocation = fullAllocation + myClaim;
    
l28: if (pc[ct]>28||28>=cs) goto l29;
    i = 3;
l29: if (pc[ct]>29||29>=cs) goto l30;
    myClaim = (5 + i) % 5;
    //protected_object_call(taskId, 2, 1);
l30: if (pc[ct]>30||30>=cs) goto l31;
    y[taskId] = myClaim;
l31: if (pc[ct]>31||31>=cs) goto l32;
    appel = 2;
l32: if (pc[ct]>32||32>=cs) goto l33;
    index = 1;
    
l33: if (pc[ct]>33||33>=cs) goto l34;
    lock(taskId);
    
l34: if (pc[ct]>34||34>=cs) goto l35;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=35);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=35);
        
        //run_entry_call(taskId, index);
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
        l35: if (pc[ct]>35||35>=cs) goto l36;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        l36: if (pc[ct]>36||36>=cs) goto l37;
            remove_entry_queue(taskId, index);
        }
        __CPROVER_assume(cs>=37);
        
        //run_entry_body(taskId,index);
    l37: if (pc[ct]>37||37>=cs) goto l38;
        if (index == 1) { //Request
            stock = stock - y[taskId];
            if (stock < 0){
                //requeue(taskId, 2); //requeue Assign
                index=2;
                evaluate_barrier(index);
                
                if (tabBarrier[index]==FALSE){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l38: if (pc[ct]>38||38>=cs) goto l39;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l39: if (pc[ct]>39||39>=cs) goto l40;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=40);
                
            }
            __CPROVER_assume(cs>=40);
            
        }
        __CPROVER_assume(cs>=40);
        
    l40: if (pc[ct]>40||40>=cs) goto l41;
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    __CPROVER_assume(cs>=41);
    
l41: if (pc[ct]>41||41>=cs) goto l42;
    fullAllocation = fullAllocation + myClaim;
    
l42: if (pc[ct]>42||42>=cs) goto l43;
    i = 4;
l43: if (pc[ct]>43||43>=cs) goto l44;
    myClaim = (5 + i) % 5;
    //protected_object_call(taskId, 2, 1);
l44: if (pc[ct]>44||44>=cs) goto l45;
    y[taskId] = myClaim;
l45: if (pc[ct]>45||45>=cs) goto l46;
    appel = 2;
l46: if (pc[ct]>46||46>=cs) goto l47;
    index = 1;
    
l47: if (pc[ct]>47||47>=cs) goto l48;
    lock(taskId);
    
l48: if (pc[ct]>48||48>=cs) goto l49;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=49);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=49);
        
        //run_entry_call(taskId, index);
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
        l49: if (pc[ct]>49||49>=cs) goto l50;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        l50: if (pc[ct]>50||50>=cs) goto l51;
            remove_entry_queue(taskId, index);
        }
        __CPROVER_assume(cs>=51);
        
        //run_entry_body(taskId,index);
    l51: if (pc[ct]>51||51>=cs) goto l52;
        if (index == 1) { //Request
            stock = stock - y[taskId];
            if (stock < 0){
                //requeue(taskId, 2); //requeue Assign
                index=2;
                evaluate_barrier(index);
                
                if (tabBarrier[index]==FALSE){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l52: if (pc[ct]>52||52>=cs) goto l53;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l53: if (pc[ct]>53||53>=cs) goto l54;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=54);
                
            }
            __CPROVER_assume(cs>=54);
            
        }
        __CPROVER_assume(cs>=54);
        
    l54: if (pc[ct]>54||54>=cs) goto l55;
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    __CPROVER_assume(cs>=55);
    
l55: if (pc[ct]>55||55>=cs) goto l56;
    fullAllocation = fullAllocation + myClaim;
    
l56: if (pc[ct]>56||56>=cs) goto l57;
    myClaim = fullAllocation;
    //protected_object_call(taskId, 1, 1);
l57: if (pc[ct]>57||57>=cs) goto l58;
    y[taskId] = myClaim;
l58: if (pc[ct]>58||58>=cs) goto l59;
    appel = 1;
l59: if (pc[ct]>59||59>=cs) goto l60;
    index = 1;
    
l60: if (pc[ct]>60||60>=cs) goto l61;
    lock(taskId);
    
l61: if (pc[ct]>61||61>=cs) goto l62;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=62);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=62);
        
        //run_entry_call(taskId, index);
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
        l62: if (pc[ct]>62||62>=cs) goto l63;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        l63: if (pc[ct]>63||63>=cs) goto l64;
            remove_entry_queue(taskId, index);
        }
        __CPROVER_assume(cs>=64);
        
        //run_entry_body(taskId,index);
    l64: if (pc[ct]>64||64>=cs) goto l65;
        if (index == 1) { //Request
            stock = stock - y[taskId];
            if (stock < 0){
                //requeue(taskId, 2); //requeue Assign
                index=2;
                evaluate_barrier(index);
                
                if (tabBarrier[index]==FALSE){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l65: if (pc[ct]>65||65>=cs) goto l66;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l66: if (pc[ct]>66||66>=cs) goto l67;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=67);
                
            }
            __CPROVER_assume(cs>=67);
            
        }
        __CPROVER_assume(cs>=67);
        
    l67: if (pc[ct]>67||67>=cs) goto l68;
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    __CPROVER_assume(cs>=68);
    
l68: if (pc[ct]>68||68>=cs) goto l69;
    fullAllocation = 0;
l69:
    return;
}

void t_two(){
    static int taskId = 2;
    static int appel = 1;
    static int index = 1;
    static int taskIdEligible;
    static int taskIdTmp;
    static int i = 0;
    static int myClaim = 0;
    static int fullAllocation = 0;
l0: if (pc[ct]>0||0>=cs) goto l1;
    i = 1;
l1: if (pc[ct]>1||1>=cs) goto l2;
    myClaim = (5 + i) % 5;
    //protected_object_call(taskId, 2, 1);
l2: if (pc[ct]>2||2>=cs) goto l3;
    y[taskId] = myClaim;
l3: if (pc[ct]>3||3>=cs) goto l4;
    appel = 2;
l4: if (pc[ct]>4||4>=cs) goto l5;
    index = 1;
    
l5: if (pc[ct]>5||5>=cs) goto l6;
    lock(taskId);
    
l6: if (pc[ct]>6||6>=cs) goto l7;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=7);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=7);
        
        //run_entry_call(taskId, index);
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
        l7: if (pc[ct]>7||7>=cs) goto l8;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        l8: if (pc[ct]>8||8>=cs) goto l9;
            remove_entry_queue(taskId, index);
        }
        __CPROVER_assume(cs>=9);
        
        //run_entry_body(taskId,index);
    l9: if (pc[ct]>9||9>=cs) goto l10;
        if (index == 1) { //Request
            stock = stock - y[taskId];
            if (stock < 0){
                //requeue(taskId, 2); //requeue Assign
                index=2;
                evaluate_barrier(index);
                
                if (tabBarrier[index]==FALSE){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l10: if (pc[ct]>10||10>=cs) goto l11;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l11: if (pc[ct]>11||11>=cs) goto l12;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=12);
                
            }
            __CPROVER_assume(cs>=12);
            
        }
        __CPROVER_assume(cs>=12);
        
    l12: if (pc[ct]>12||12>=cs) goto l13;
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    __CPROVER_assume(cs>=13);
    
l13: if (pc[ct]>13||13>=cs) goto l14;
    fullAllocation = fullAllocation + myClaim;
    
l14: if (pc[ct]>14||14>=cs) goto l15;
    i = 2;
l15: if (pc[ct]>15||15>=cs) goto l16;
    myClaim = (5 + i) % 5;
    //protected_object_call(taskId, 2, 1);
l16: if (pc[ct]>16||16>=cs) goto l17;
    y[taskId] = myClaim;
l17: if (pc[ct]>17||17>=cs) goto l18;
    appel = 2;
l18: if (pc[ct]>18||18>=cs) goto l19;
    index = 1;
    
l19: if (pc[ct]>19||19>=cs) goto l20;
    lock(taskId);
    
l20: if (pc[ct]>20||20>=cs) goto l21;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=21);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=21);
        
        //run_entry_call(taskId, index);
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
        l21: if (pc[ct]>21||21>=cs) goto l22;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        l22: if (pc[ct]>22||22>=cs) goto l23;
            remove_entry_queue(taskId, index);
        }
        __CPROVER_assume(cs>=23);
        
        //run_entry_body(taskId,index);
    l23: if (pc[ct]>23||23>=cs) goto l24;
        if (index == 1) { //Request
            stock = stock - y[taskId];
            if (stock < 0){
                //requeue(taskId, 2); //requeue Assign
                index=2;
                evaluate_barrier(index);
                
                if (tabBarrier[index]==FALSE){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l24: if (pc[ct]>24||24>=cs) goto l25;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l25: if (pc[ct]>25||25>=cs) goto l26;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=26);
                
            }
            __CPROVER_assume(cs>=26);
            
        }
        __CPROVER_assume(cs>=26);
        
    l26: if (pc[ct]>26||26>=cs) goto l27;
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    __CPROVER_assume(cs>=27);
    
l27: if (pc[ct]>27||27>=cs) goto l28;
    fullAllocation = fullAllocation + myClaim;
    
l28: if (pc[ct]>28||28>=cs) goto l29;
    i = 3;
l29: if (pc[ct]>29||29>=cs) goto l30;
    myClaim = (5 + i) % 5;
    //protected_object_call(taskId, 2, 1);
l30: if (pc[ct]>30||30>=cs) goto l31;
    y[taskId] = myClaim;
l31: if (pc[ct]>31||31>=cs) goto l32;
    appel = 2;
l32: if (pc[ct]>32||32>=cs) goto l33;
    index = 1;
    
l33: if (pc[ct]>33||33>=cs) goto l34;
    lock(taskId);
    
l34: if (pc[ct]>34||34>=cs) goto l35;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=35);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=35);
        
        //run_entry_call(taskId, index);
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
        l35: if (pc[ct]>35||35>=cs) goto l36;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        l36: if (pc[ct]>36||36>=cs) goto l37;
            remove_entry_queue(taskId, index);
        }
        __CPROVER_assume(cs>=37);
        
        //run_entry_body(taskId,index);
    l37: if (pc[ct]>37||37>=cs) goto l38;
        if (index == 1) { //Request
            stock = stock - y[taskId];
            if (stock < 0){
                //requeue(taskId, 2); //requeue Assign
                index=2;
                evaluate_barrier(index);
                
                if (tabBarrier[index]==FALSE){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l38: if (pc[ct]>38||38>=cs) goto l39;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l39: if (pc[ct]>39||39>=cs) goto l40;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=40);
                
            }
            __CPROVER_assume(cs>=40);
            
        }
        __CPROVER_assume(cs>=40);
        
    l40: if (pc[ct]>40||40>=cs) goto l41;
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    __CPROVER_assume(cs>=41);
    
l41: if (pc[ct]>41||41>=cs) goto l42;
    fullAllocation = fullAllocation + myClaim;
    
l42: if (pc[ct]>42||42>=cs) goto l43;
    i = 4;
l43: if (pc[ct]>43||43>=cs) goto l44;
    myClaim = (5 + i) % 5;
    //protected_object_call(taskId, 2, 1);
l44: if (pc[ct]>44||44>=cs) goto l45;
    y[taskId] = myClaim;
l45: if (pc[ct]>45||45>=cs) goto l46;
    appel = 2;
l46: if (pc[ct]>46||46>=cs) goto l47;
    index = 1;
    
l47: if (pc[ct]>47||47>=cs) goto l48;
    lock(taskId);
    
l48: if (pc[ct]>48||48>=cs) goto l49;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=49);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=49);
        
        //run_entry_call(taskId, index);
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
        l49: if (pc[ct]>49||49>=cs) goto l50;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        l50: if (pc[ct]>50||50>=cs) goto l51;
            remove_entry_queue(taskId, index);
        }
        __CPROVER_assume(cs>=51);
        
        //run_entry_body(taskId,index);
    l51: if (pc[ct]>51||51>=cs) goto l52;
        if (index == 1) { //Request
            stock = stock - y[taskId];
            if (stock < 0){
                //requeue(taskId, 2); //requeue Assign
                index=2;
                evaluate_barrier(index);
                
                if (tabBarrier[index]==FALSE){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l52: if (pc[ct]>52||52>=cs) goto l53;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l53: if (pc[ct]>53||53>=cs) goto l54;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=54);
                
            }
            __CPROVER_assume(cs>=54);
            
        }
        __CPROVER_assume(cs>=54);
        
    l54: if (pc[ct]>54||54>=cs) goto l55;
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    __CPROVER_assume(cs>=55);
    
l55: if (pc[ct]>55||55>=cs) goto l56;
    fullAllocation = fullAllocation + myClaim;
    
l56: if (pc[ct]>56||56>=cs) goto l57;
    myClaim = fullAllocation;
    //protected_object_call(taskId, 1, 1);
l57: if (pc[ct]>57||57>=cs) goto l58;
    y[taskId] = myClaim;
l58: if (pc[ct]>58||58>=cs) goto l59;
    appel = 1;
l59: if (pc[ct]>59||59>=cs) goto l60;
    index = 1;
    
l60: if (pc[ct]>60||60>=cs) goto l61;
    lock(taskId);
    
l61: if (pc[ct]>61||61>=cs) goto l62;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=62);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=62);
        
        //run_entry_call(taskId, index);
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
        l62: if (pc[ct]>62||62>=cs) goto l63;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        l63: if (pc[ct]>63||63>=cs) goto l64;
            remove_entry_queue(taskId, index);
        }
        __CPROVER_assume(cs>=64);
        
        //run_entry_body(taskId,index);
    l64: if (pc[ct]>64||64>=cs) goto l65;
        if (index == 1) { //Request
            stock = stock - y[taskId];
            if (stock < 0){
                //requeue(taskId, 2); //requeue Assign
                index=2;
                evaluate_barrier(index);
                
                if (tabBarrier[index]==FALSE){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l65: if (pc[ct]>65||65>=cs) goto l66;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l66: if (pc[ct]>66||66>=cs) goto l67;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=67);
                
            }
            __CPROVER_assume(cs>=67);
            
        }
        __CPROVER_assume(cs>=67);
        
    l67: if (pc[ct]>67||67>=cs) goto l68;
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    __CPROVER_assume(cs>=68);
    
l68: if (pc[ct]>68||68>=cs) goto l69;
    fullAllocation = 0;
l69:
    return;
}

void t_three(){
    static int taskId = 3;
    static int appel = 1;
    static int index = 1;
    static int taskIdEligible;
    static int taskIdTmp;
    static int i = 0;
    static int myClaim = 0;
    static int fullAllocation = 0;
l0: if (pc[ct]>0||0>=cs) goto l1;
    i = 1;
l1: if (pc[ct]>1||1>=cs) goto l2;
    myClaim = (5 + i) % 5;
    //protected_object_call(taskId, 2, 1);
l2: if (pc[ct]>2||2>=cs) goto l3;
    y[taskId] = myClaim;
l3: if (pc[ct]>3||3>=cs) goto l4;
    appel = 2;
l4: if (pc[ct]>4||4>=cs) goto l5;
    index = 1;
    
l5: if (pc[ct]>5||5>=cs) goto l6;
    lock(taskId);
    
l6: if (pc[ct]>6||6>=cs) goto l7;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=7);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=7);
        
        //run_entry_call(taskId, index);
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
        l7: if (pc[ct]>7||7>=cs) goto l8;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        l8: if (pc[ct]>8||8>=cs) goto l9;
            remove_entry_queue(taskId, index);
        }
        __CPROVER_assume(cs>=9);
        
        //run_entry_body(taskId,index);
    l9: if (pc[ct]>9||9>=cs) goto l10;
        if (index == 1) { //Request
            stock = stock - y[taskId];
            if (stock < 0){
                //requeue(taskId, 2); //requeue Assign
                index=2;
                evaluate_barrier(index);
                
                if (tabBarrier[index]==FALSE){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l10: if (pc[ct]>10||10>=cs) goto l11;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l11: if (pc[ct]>11||11>=cs) goto l12;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=12);
                
            }
            __CPROVER_assume(cs>=12);
            
        }
        __CPROVER_assume(cs>=12);
        
    l12: if (pc[ct]>12||12>=cs) goto l13;
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    __CPROVER_assume(cs>=13);
    
l13: if (pc[ct]>13||13>=cs) goto l14;
    fullAllocation = fullAllocation + myClaim;
    
l14: if (pc[ct]>14||14>=cs) goto l15;
    i = 2;
l15: if (pc[ct]>15||15>=cs) goto l16;
    myClaim = (5 + i) % 5;
    //protected_object_call(taskId, 2, 1);
l16: if (pc[ct]>16||16>=cs) goto l17;
    y[taskId] = myClaim;
l17: if (pc[ct]>17||17>=cs) goto l18;
    appel = 2;
l18: if (pc[ct]>18||18>=cs) goto l19;
    index = 1;
    
l19: if (pc[ct]>19||19>=cs) goto l20;
    lock(taskId);
    
l20: if (pc[ct]>20||20>=cs) goto l21;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=21);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=21);
        
        //run_entry_call(taskId, index);
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
        l21: if (pc[ct]>21||21>=cs) goto l22;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        l22: if (pc[ct]>22||22>=cs) goto l23;
            remove_entry_queue(taskId, index);
        }
        __CPROVER_assume(cs>=23);
        
        //run_entry_body(taskId,index);
    l23: if (pc[ct]>23||23>=cs) goto l24;
        if (index == 1) { //Request
            stock = stock - y[taskId];
            if (stock < 0){
                //requeue(taskId, 2); //requeue Assign
                index=2;
                evaluate_barrier(index);
                
                if (tabBarrier[index]==FALSE){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l24: if (pc[ct]>24||24>=cs) goto l25;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l25: if (pc[ct]>25||25>=cs) goto l26;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=26);
                
            }
            __CPROVER_assume(cs>=26);
            
        }
        __CPROVER_assume(cs>=26);
        
    l26: if (pc[ct]>26||26>=cs) goto l27;
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    __CPROVER_assume(cs>=27);
    
l27: if (pc[ct]>27||27>=cs) goto l28;
    fullAllocation = fullAllocation + myClaim;
    
l28: if (pc[ct]>28||28>=cs) goto l29;
    i = 3;
l29: if (pc[ct]>29||29>=cs) goto l30;
    myClaim = (5 + i) % 5;
    //protected_object_call(taskId, 2, 1);
l30: if (pc[ct]>30||30>=cs) goto l31;
    y[taskId] = myClaim;
l31: if (pc[ct]>31||31>=cs) goto l32;
    appel = 2;
l32: if (pc[ct]>32||32>=cs) goto l33;
    index = 1;
    
l33: if (pc[ct]>33||33>=cs) goto l34;
    lock(taskId);
    
l34: if (pc[ct]>34||34>=cs) goto l35;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=35);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=35);
        
        //run_entry_call(taskId, index);
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
        l35: if (pc[ct]>35||35>=cs) goto l36;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        l36: if (pc[ct]>36||36>=cs) goto l37;
            remove_entry_queue(taskId, index);
        }
        __CPROVER_assume(cs>=37);
        
        //run_entry_body(taskId,index);
    l37: if (pc[ct]>37||37>=cs) goto l38;
        if (index == 1) { //Request
            stock = stock - y[taskId];
            if (stock < 0){
                //requeue(taskId, 2); //requeue Assign
                index=2;
                evaluate_barrier(index);
                
                if (tabBarrier[index]==FALSE){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l38: if (pc[ct]>38||38>=cs) goto l39;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l39: if (pc[ct]>39||39>=cs) goto l40;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=40);
                
            }
            __CPROVER_assume(cs>=40);
            
        }
        __CPROVER_assume(cs>=40);
        
    l40: if (pc[ct]>40||40>=cs) goto l41;
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    __CPROVER_assume(cs>=41);
    
l41: if (pc[ct]>41||41>=cs) goto l42;
    fullAllocation = fullAllocation + myClaim;
    
l42: if (pc[ct]>42||42>=cs) goto l43;
    i = 4;
l43: if (pc[ct]>43||43>=cs) goto l44;
    myClaim = (5 + i) % 5;
    //protected_object_call(taskId, 2, 1);
l44: if (pc[ct]>44||44>=cs) goto l45;
    y[taskId] = myClaim;
l45: if (pc[ct]>45||45>=cs) goto l46;
    appel = 2;
l46: if (pc[ct]>46||46>=cs) goto l47;
    index = 1;
    
l47: if (pc[ct]>47||47>=cs) goto l48;
    lock(taskId);
    
l48: if (pc[ct]>48||48>=cs) goto l49;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=49);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=49);
        
        //run_entry_call(taskId, index);
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
        l49: if (pc[ct]>49||49>=cs) goto l50;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        l50: if (pc[ct]>50||50>=cs) goto l51;
            remove_entry_queue(taskId, index);
        }
        __CPROVER_assume(cs>=51);
        
        //run_entry_body(taskId,index);
    l51: if (pc[ct]>51||51>=cs) goto l52;
        if (index == 1) { //Request
            stock = stock - y[taskId];
            if (stock < 0){
                //requeue(taskId, 2); //requeue Assign
                index=2;
                evaluate_barrier(index);
                
                if (tabBarrier[index]==FALSE){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l52: if (pc[ct]>52||52>=cs) goto l53;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l53: if (pc[ct]>53||53>=cs) goto l54;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=54);
                
            }
            __CPROVER_assume(cs>=54);
            
        }
        __CPROVER_assume(cs>=54);
        
    l54: if (pc[ct]>54||54>=cs) goto l55;
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    __CPROVER_assume(cs>=55);
    
l55: if (pc[ct]>55||55>=cs) goto l56;
    fullAllocation = fullAllocation + myClaim;
    
l56: if (pc[ct]>56||56>=cs) goto l57;
    myClaim = fullAllocation;
    //protected_object_call(taskId, 1, 1);
l57: if (pc[ct]>57||57>=cs) goto l58;
    y[taskId] = myClaim;
l58: if (pc[ct]>58||58>=cs) goto l59;
    appel = 1;
l59: if (pc[ct]>59||59>=cs) goto l60;
    index = 1;
    
l60: if (pc[ct]>60||60>=cs) goto l61;
    lock(taskId);
    
l61: if (pc[ct]>61||61>=cs) goto l62;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=62);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=62);
        
        //run_entry_call(taskId, index);
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
        l62: if (pc[ct]>62||62>=cs) goto l63;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        l63: if (pc[ct]>63||63>=cs) goto l64;
            remove_entry_queue(taskId, index);
        }
        __CPROVER_assume(cs>=64);
        
        //run_entry_body(taskId,index);
    l64: if (pc[ct]>64||64>=cs) goto l65;
        if (index == 1) { //Request
            stock = stock - y[taskId];
            if (stock < 0){
                //requeue(taskId, 2); //requeue Assign
                index=2;
                evaluate_barrier(index);
                
                if (tabBarrier[index]==FALSE){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l65: if (pc[ct]>65||65>=cs) goto l66;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l66: if (pc[ct]>66||66>=cs) goto l67;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=67);
                
            }
            __CPROVER_assume(cs>=67);
            
        }
        __CPROVER_assume(cs>=67);
        
    l67: if (pc[ct]>67||67>=cs) goto l68;
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    __CPROVER_assume(cs>=68);
    
l68: if (pc[ct]>68||68>=cs) goto l69;
    fullAllocation = 0;
l69:
    return;
}

void init(){
    pc[0] = 0;
    size[0] = 0;
    
    pc[1] = 0;
    pc[2] = 0;
    pc[3] = 0;
    size[1] = 69;//117;
    size[2] = 69;
    size[3] =69;
    
    int t1, t2, t3;
    
    create(t1, 1);
    create(t2, 2);
    create(t3, 3);
}

int main(){
    init();
    
    for (;;) {
        ct = 1;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_one();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        ct = 2;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_two();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        ct = 3;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_three();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        if (dead>0 && dead<NB_TASK){
            __CPROVER_assert(dead+taskInWait<NB_TASK, "OK dead+taskInWait>=NB_TASK");
        }
    }
}
