void t_one(){
    static int taskId=1;
    static int taskIdEligible;
    static int appel = 2;
    static int index = 1;
    static int with_abort;
//@begin
    //protected_object_call(taskId, appel, index)
//@controlBegin
    lock(taskId);
    appel=2;
    index=1;
    
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
//@controlAssume
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
//@controlAssume
        //begin :run_entry_call(taskId, index);
        index=1;
        evaluate_barrier(index);
    
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
//@controlEnd
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
//@controlBegin
            remove_entry_queue(taskId, index);
        }
//@controlAssume
        //begin : run_entry_body(taskId,index);
        index = 1;
        if (index == 1) { //get_pair
            if (available[eid1[taskId]] && available[(eid1[taskId] +1) % NB_TASK]){
                available[eid1[taskId]] = FALSE;
                available[(eid1[taskId] + 1) % NB_TASK] = FALSE;
            }else{
//@controlAssume
                eid2[taskId] = eid1[taskId];
                //begin : requeue(taskId, 2, TRUE);
                index=2;
                with_abort=TRUE;
//@controlEnd
//@controlLabel
//@controlBegin
                evaluate_barrier(index);
    
                if (tabBarrier[index]==FALSE && !with_abort){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
//@controlEnd
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
//@controlBegin
                    remove_entry_queue(taskId, index);
                }
//@controlAssume
    
                if (tabBarrier[index]==TRUE){
                    //begin : run_entry_body(taskId,index);
                   /* if (index == 1) { //get_pair
                        if (available[eid1[taskId]] && available[(eid1[taskId] +1) % NB_TASK]){
                            available[eid1[taskId]] = FALSE;
                            available[(eid1[taskId] + 1) % NB_TASK] = FALSE;
                        }else{
                            eid2[taskId] = eid1[taskId];
                            requeue(taskId, 2, TRUE);
                        }
                    }else */if (index == 2){ //please_get_pair
                        flush_count = flush_count-1;
                        if (available[eid2[taskId]] && available[(eid2[taskId]+1)%NB_TASK]){
                            available[eid2[taskId]] = FALSE;
                            available[(eid2[taskId]+1)%NB_TASK] = FALSE;
                        }else{
//@controlAssume
                            eid2[taskId] = eid2[taskId];
                            index = 2;
                            with_abort=TRUE;
                           //begin : requeue(taskId, 2, TRUE);
                            cs = ?;
                            goto end;
                           //end : requeue
                        }
//@controlAssume
                    }
//@controlAssume
                    //end : run_entry_body
                }
//@controlAssume
                //end : requeue
            }
//@controlAssume
        }/*else if (index == 2){ //please_get_pair
//@controlAssume
            flush_count = flush_count-1;
            if (available[eid2[taskId]] && available[(eid2[taskId]+1)%NB_TASK]){
                available[eid2[taskId]] = FALSE;
                available[(eid2[taskId]+1)%NB_TASK] = FALSE;
            }else{
                eid2[taskId] = eid2[taskId];
                requeue(taskId, 2, TRUE);
            }
        }*/

        //end : run_entry_body
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
        //end : run_entry_call
    }
//@controlAssume
//@controlEnd
//@controlBegin
    lock(taskId);
    
    appel=1;
    index=1;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
//@controlAssume
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
//@controlAssume
        run_entry_call(taskId, index);
    }
//@controlAssume
//@controlEnd
//@end
}
