void t_one(){
    static int taskId=1;
    static int taskIdEligible;
    static int appel = 2;
    static int index = 1;
    static int with_abort;
    //protected_object_call(taskId, appel, index)
l0: if (pc[ct]>0||0>=cs) goto l1;
    lock(taskId);
    appel=2;
    index=1;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
__CPROVER_assume(cs>=1);

        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
__CPROVER_assume(cs>=1);

        //begin :run_entry_call(taskId, index);
        index=1;
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
l1: if (pc[ct]>1||1>=cs) goto l2;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
l2: if (pc[ct]>2||2>=cs) goto l3;
            remove_entry_queue(taskId, index);
        }
__CPROVER_assume(cs>=3);

        //begin : run_entry_body(taskId,index);
        index = 1;
        if (index == 1) { //get_pair
            if (available[eid1[taskId]] && available[(eid1[taskId] +1) % NB_TASK]){
                available[eid1[taskId]] = FALSE;
                available[(eid1[taskId] + 1) % NB_TASK] = FALSE;
            }else{
__CPROVER_assume(cs>=3);

                eid2[taskId] = eid1[taskId];
                //begin : requeue(taskId, 2, TRUE);
                index=2;
                with_abort=TRUE;
l3: if (pc[ct]>3||3>=cs) goto l4;
                evaluate_barrier(index);
                if (tabBarrier[index]==FALSE && !with_abort){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
l4: if (pc[ct]>4||4>=cs) goto l5;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
l5: if (pc[ct]>5||5>=cs) goto l6;
                    remove_entry_queue(taskId, index);
                }
__CPROVER_assume(cs>=6);

                if (tabBarrier[index]==TRUE){
                    //begin : run_entry_body(taskId,index);
                   /* if (index == 1) { //get_pair
                        if (available[eid1[taskId]] && available[(eid1[taskId] +1) % NB_TASK]){
                            available[eid1[taskId]] = FALSE;
                            available[(eid1[taskId] + 1) % NB_TASK] = FALSE;
                        }else{
                            eid2[taskId] = eid1[taskId];
                            requeue(taskId, 2, TRUE);
                        }
                    }else */if (index == 2){ //please_get_pair
                        flush_count = flush_count-1;
                        if (available[eid2[taskId]] && available[(eid2[taskId]+1)%NB_TASK]){
                            available[eid2[taskId]] = FALSE;
                            available[(eid2[taskId]+1)%NB_TASK] = FALSE;
                        }else{
__CPROVER_assume(cs>=6);

                            eid2[taskId] = eid2[taskId];
                            index = 2;
                            with_abort=TRUE;
                           //begin : requeue(taskId, 2, TRUE);
                            cs = 3;
                            goto end;
                           //end : requeue
                        }
__CPROVER_assume(cs>=6);

                    }
__CPROVER_assume(cs>=6);

                    //end : run_entry_body
                }
__CPROVER_assume(cs>=6);

                //end : requeue
            }
__CPROVER_assume(cs>=6);

        }/*else if (index == 2){ //please_get_pair
__CPROVER_assume(cs>=6);

            flush_count = flush_count-1;
            if (available[eid2[taskId]] && available[(eid2[taskId]+1)%NB_TASK]){
                available[eid2[taskId]] = FALSE;
                available[(eid2[taskId]+1)%NB_TASK] = FALSE;
            }else{
                eid2[taskId] = eid2[taskId];
                requeue(taskId, 2, TRUE);
            }
        }*/
        //end : run_entry_body
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
        //end : run_entry_call
    }
__CPROVER_assume(cs>=6);

l6: if (pc[ct]>6||6>=cs) goto end;
    lock(taskId);
    appel=1;
    index=1;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
__CPROVER_assume(cs>=7);

        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
__CPROVER_assume(cs>=7);

        run_entry_call(taskId, index);
    }
__CPROVER_assume(cs>=7);

end:
 return;
}
