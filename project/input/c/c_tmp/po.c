void t_one(){
    static int taskId = 1;
    static int appel = 2;
    static int index = 1;
    static int taskIdEligible;
    static int ax, bx;
    
    //@begin
    lock(taskId);
    
    //@controlBegin
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        //@controlAssume
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        //@controlAssume
        //run_entry_call(taskId, index);
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
            //@controlEnd
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
            remove_entry_queue(taskId, index);
        }
        //@controlAssume
        //run_entry_body(taskId,index);
        if (index == 1) { //Request
            ax = x;
            ax = ax + 1;
            x=ax;
        }else if (index == 2){ //Assign
            //@controlAssume
            bx=x;
            bx = bx - 1;
            x=bx;
        }
        //@controlAssume
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    //@controlAssume
    //@end
}

void t_two(){
    static int taskId = 2;
    static int appel = 2;
    static int index = 2;
    static int taskIdEligible;
    static int ax, bx;
        
    //@begin
    lock(taskId);
    
    //@controlBegin
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        //@controlAssume
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        //@controlAssume
        //run_entry_call(taskId, index);
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
            //@controlEnd
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
            remove_entry_queue(taskId, index);
        }
        //@controlAssume
        //run_entry_body(taskId,index);
        if (index == 1) { //Request
            ax = x;
            ax = ax + 1;
            x=ax;
        }else if (index == 2){ //Assign
            //@controlAssume
            bx=x;
            bx = bx - 1;
            x=bx;
        }
        //@controlAssume
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    //@controlAssume
    //@end
}