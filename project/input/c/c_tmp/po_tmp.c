void t_one(){
    static int taskId = 1;
    static int appel = 2;
    static int index = 1;
    static int taskIdEligible;
    static int ax, bx;
    
l0: if (pc[ct]>0||0>=cs) goto l1;
    lock(taskId);
l1: if (pc[ct]>1||1>=cs) goto l2;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
__CPROVER_assume(cs>=2);

        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
__CPROVER_assume(cs>=2);

        //run_entry_call(taskId, index);
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
l2: if (pc[ct]>2||2>=cs) goto l3;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
l3: if (pc[ct]>3||3>=cs) goto l4;
            remove_entry_queue(taskId, index);
        }
__CPROVER_assume(cs>=4);

        //run_entry_body(taskId,index);
l4: if (pc[ct]>4||4>=cs) goto l5;
        if (index == 1) { //Request
l5: if (pc[ct]>5||5>=cs) goto l6;
            ax = x;
l6: if (pc[ct]>6||6>=cs) goto l7;
            ax = ax + 1;
l7: if (pc[ct]>7||7>=cs) goto l8;
            x=ax;
        }else if (index == 2){ //Assign
__CPROVER_assume(cs>=8);

l8: if (pc[ct]>8||8>=cs) goto l9;
            bx=x;
l9: if (pc[ct]>9||9>=cs) goto l10;
            bx = bx - 1;
l10: if (pc[ct]>10||10>=cs) goto l11;
            x=bx;
        }
__CPROVER_assume(cs>=11);

l11: if (pc[ct]>11||11>=cs) goto l12;
        taskIdEligible = check_and_run_any_entry();
l12: if (pc[ct]>12||12>=cs) goto end;
        transfert(taskId, taskIdEligible);
    }
__CPROVER_assume(cs>=13);

end:
 return;
}

void t_two(){
    static int taskId = 2;
    static int appel = 2;
    static int index = 2;
    static int taskIdEligible;
    static int ax, bx;
        
l0: if (pc[ct]>0||0>=cs) goto l1;
    lock(taskId);
l1: if (pc[ct]>1||1>=cs) goto l2;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
__CPROVER_assume(cs>=2);

        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
__CPROVER_assume(cs>=2);

        //run_entry_call(taskId, index);
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
l2: if (pc[ct]>2||2>=cs) goto l3;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
l3: if (pc[ct]>3||3>=cs) goto l4;
            remove_entry_queue(taskId, index);
        }
__CPROVER_assume(cs>=4);

        //run_entry_body(taskId,index);
l4: if (pc[ct]>4||4>=cs) goto l5;
        if (index == 1) { //Request
l5: if (pc[ct]>5||5>=cs) goto l6;
            ax = x;
l6: if (pc[ct]>6||6>=cs) goto l7;
            ax = ax + 1;
l7: if (pc[ct]>7||7>=cs) goto l8;
            x=ax;
        }else if (index == 2){ //Assign
__CPROVER_assume(cs>=8);

l8: if (pc[ct]>8||8>=cs) goto l9;
            bx=x;
l9: if (pc[ct]>9||9>=cs) goto l10;
            bx = bx - 1;
l10: if (pc[ct]>10||10>=cs) goto l11;
            x=bx;
        }
__CPROVER_assume(cs>=11);

l11: if (pc[ct]>11||11>=cs) goto l12;
        taskIdEligible = check_and_run_any_entry();
l12: if (pc[ct]>12||12>=cs) goto end;
        transfert(taskId, taskIdEligible);
    }
__CPROVER_assume(cs>=13);

end:
 return;
}