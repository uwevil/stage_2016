void t_one(){
    static int taskId=1;
    static int taskIdEligible;
    static int appel = 2;
    static int index = 1;
    
    //protected_object_call(taskId, appel, index)
l0: if (pc[ct]>0||0>=cs) goto l1;
    lock(taskId);
    appel=2;
    index=1;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
__CPROVER_assume(cs>=1);

        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
__CPROVER_assume(cs>=1);

        //run_entry_call(taskId, index);
        static int taskIdEligible;
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
l1: if (pc[ct]>1||1>=cs) goto l2;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
l2: if (pc[ct]>2||2>=cs) goto l3;
            remove_entry_queue(taskId, index);
        }
__CPROVER_assume(cs>=3);

l3: if (pc[ct]>3||3>=cs) goto l4;
        //run_entry_body(taskId,index);
        if (index == 1) { //get_pair
            if (available[eid1[taskId]] && available[(eid1[taskId] +1) % NB_TASK]){
                available[eid1[taskId]] = FALSE;
                available[(eid1[taskId] + 1) % NB_TASK] = FALSE;
            }else{
__CPROVER_assume(cs>=4);

                //requeue(taskId, 2);
                evaluate_barrier(index);
                if (tabBarrier[index]==FALSE){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
l4: if (pc[ct]>4||4>=cs) goto l5;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
l5: if (pc[ct]>5||5>=cs) goto l6;
                    remove_entry_queue(taskId, index);
                }
__CPROVER_assume(cs>=6);

l6: if (pc[ct]>6||6>=cs) goto l7;
                //run_entry_body(taskId,index);
                index = 2;
                cs = 3;
                goto end;
            }
__CPROVER_assume(cs>=7);

        }else if (index == 2){ //please_get_pair
__CPROVER_assume(cs>=7);

            flush_count = flush_count-1;
            if (available[eid2[taskId]] and available[(eid2[taskId]+1)%NB_TASK]){
                available[eid2[taskId]] = FALSE;
                available[(eid2[taskId]+1)%NB_TASK] = FALSE;
            }else{
__CPROVER_assume(cs>=7);

                index = 2;
                //requeue(taskId, 2);
                evaluate_barrier(index);
                if (tabBarrier[index]==FALSE){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
l7: if (pc[ct]>7||7>=cs) goto l8;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
l8: if (pc[ct]>8||8>=cs) goto l9;
                    remove_entry_queue(taskId, index);
                }
__CPROVER_assume(cs>=9);

l9: if (pc[ct]>9||9>=cs) goto l10;
                //run_entry_body(taskId,index);
                index = 2;
                cs = 3;
                goto end;
            }
__CPROVER_assume(cs>=10);

        }
__CPROVER_assume(cs>=10);

        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
__CPROVER_assume(cs>=10);

l10: if (pc[ct]>10||10>=cs) goto end;
    lock(taskId);
    appel=1;
    index=1;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
__CPROVER_assume(cs>=11);

        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
__CPROVER_assume(cs>=11);

        run_entry_call(taskId, index);
    }
__CPROVER_assume(cs>=11);

end:
 return;
}
