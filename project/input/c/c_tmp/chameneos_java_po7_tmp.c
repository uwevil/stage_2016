void t_one(){
    static int taskId=1;
    static int taskIdEligible;
    static int my_peer = 0;
    static int i;
    static int score;
    static int appel;
    static int index;
    static int taskIdEligible2;
    
l0: if (pc[ct]>0||0>=cs) goto l1;
    i = 1;
    // for (i = 1; i<=nb_request; i++){
l1: if (pc[ct]>1||1>=cs) goto l2;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
l2: if (pc[ct]>2||2>=cs) goto l3;
    score=0;
    //  while (score!=2) {
l3: if (pc[ct]>3||3>=cs) goto l4;
    x1[taskId]=x3[taskId];
l4: if (pc[ct]>4||4>=cs) goto l5;
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
l5: if (pc[ct]>5||5>=cs) goto l6;
    appel = 2;
l6: if (pc[ct]>6||6>=cs) goto l7;
    index = 1;
l7: if (pc[ct]>7||7>=cs) goto l8;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
__CPROVER_assume(cs>=8);

        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
__CPROVER_assume(cs>=8);

        //run_entry_call2(taskId, index);
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
l8: if (pc[ct]>8||8>=cs) goto l9;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
l9: if (pc[ct]>9||9>=cs) goto l10;
            remove_entry_queue2(taskId, index);
        }
__CPROVER_assume(cs>=10);

        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
l10: if (pc[ct]>10||10>=cs) goto l11;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
l11: if (pc[ct]>11||11>=cs) goto l12;
                    remove_entry_queue2(taskId, index);
                }
__CPROVER_assume(cs>=12);

                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && First_Call){
__CPROVER_assume(cs>=12);

                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
l12: if (pc[ct]>12||12>=cs) goto l13;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
l13: if (pc[ct]>13||13>=cs) goto l14;
                    remove_entry_queue2(taskId, index);
                }
__CPROVER_assume(cs>=14);

                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && !First_Call){
__CPROVER_assume(cs>=14);

                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }else if (score1[taskId]=1 && First_Call){
__CPROVER_assume(cs>=14);

                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
__CPROVER_assume(cs>=14);

        }else if (index == 2){ //Wait
__CPROVER_assume(cs>=14);

        }
__CPROVER_assume(cs>=14);

        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
__CPROVER_assume(cs>=14);

    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 3;
        goto end;
    }
__CPROVER_assume(cs>=14);

    //  }
l14: if (pc[ct]>14||14>=cs) goto l15;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l15: if (pc[ct]>15||15>=cs) goto l16;
    my_peer=return3[taskId];
l16: if (pc[ct]>16||16>=cs) goto l17;
    if (my_peer==taskId){
l17: if (pc[ct]>17||17>=cs) goto l18;
        cs = size[taskId];
l18: if (pc[ct]>18||18>=cs) goto l19;
        goto end;
    }
__CPROVER_assume(cs>=19);

l19: if (pc[ct]>19||19>=cs) goto l20;
    test[taskId]=my_peer;
    //--------------------------
l20: if (pc[ct]>20||20>=cs) goto l21;
    i = 2;
l21: if (pc[ct]>21||21>=cs) goto l22;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
l22: if (pc[ct]>22||22>=cs) goto l23;
    score=0;
    //  while (score!=2) {
l23: if (pc[ct]>23||23>=cs) goto l24;
    x1[taskId]=x3[taskId];
l24: if (pc[ct]>24||24>=cs) goto l25;
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
l25: if (pc[ct]>25||25>=cs) goto l26;
    appel = 2;
l26: if (pc[ct]>26||26>=cs) goto l27;
    index = 1;
l27: if (pc[ct]>27||27>=cs) goto l28;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
__CPROVER_assume(cs>=28);

        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
__CPROVER_assume(cs>=28);

        //run_entry_call2(taskId, index);
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
l28: if (pc[ct]>28||28>=cs) goto l29;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
l29: if (pc[ct]>29||29>=cs) goto l30;
            remove_entry_queue2(taskId, index);
        }
__CPROVER_assume(cs>=30);

        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
l30: if (pc[ct]>30||30>=cs) goto l31;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
l31: if (pc[ct]>31||31>=cs) goto l32;
                    remove_entry_queue2(taskId, index);
                }
__CPROVER_assume(cs>=32);

                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && First_Call){
__CPROVER_assume(cs>=32);

                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
l32: if (pc[ct]>32||32>=cs) goto l33;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
l33: if (pc[ct]>33||33>=cs) goto l34;
                    remove_entry_queue2(taskId, index);
                }
__CPROVER_assume(cs>=34);

                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && !First_Call){
__CPROVER_assume(cs>=34);

                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }else if (score1[taskId]=1 && First_Call){
__CPROVER_assume(cs>=34);

                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
__CPROVER_assume(cs>=34);

        }else if (index == 2){ //Wait
__CPROVER_assume(cs>=34);

        }
__CPROVER_assume(cs>=34);

        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
__CPROVER_assume(cs>=34);

    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 23;
        goto end;
    }
__CPROVER_assume(cs>=34);

    //  }
l34: if (pc[ct]>34||34>=cs) goto l35;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l35: if (pc[ct]>35||35>=cs) goto l36;
    my_peer=return3[taskId];
l36: if (pc[ct]>36||36>=cs) goto l37;
    if (my_peer==taskId){
l37: if (pc[ct]>37||37>=cs) goto l38;
        cs = size[taskId];
l38: if (pc[ct]>38||38>=cs) goto l39;
        goto end;
    }
__CPROVER_assume(cs>=39);

l39: if (pc[ct]>39||39>=cs) goto l40;
    test[taskId]=my_peer;
    //--------------------------
l40: if (pc[ct]>40||40>=cs) goto l41;
    i = 3;
l41: if (pc[ct]>41||41>=cs) goto l42;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
l42: if (pc[ct]>42||42>=cs) goto l43;
    score=0;
    //  while (score!=2) {
l43: if (pc[ct]>43||43>=cs) goto l44;
    x1[taskId]=x3[taskId];
l44: if (pc[ct]>44||44>=cs) goto l45;
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
l45: if (pc[ct]>45||45>=cs) goto l46;
    appel = 2;
l46: if (pc[ct]>46||46>=cs) goto l47;
    index = 1;
l47: if (pc[ct]>47||47>=cs) goto l48;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
__CPROVER_assume(cs>=48);

        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
__CPROVER_assume(cs>=48);

        //run_entry_call2(taskId, index);
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
l48: if (pc[ct]>48||48>=cs) goto l49;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
l49: if (pc[ct]>49||49>=cs) goto l50;
            remove_entry_queue2(taskId, index);
        }
__CPROVER_assume(cs>=50);

        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
l50: if (pc[ct]>50||50>=cs) goto l51;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
l51: if (pc[ct]>51||51>=cs) goto l52;
                    remove_entry_queue2(taskId, index);
                }
__CPROVER_assume(cs>=52);

                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && First_Call){
__CPROVER_assume(cs>=52);

                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
l52: if (pc[ct]>52||52>=cs) goto l53;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
l53: if (pc[ct]>53||53>=cs) goto l54;
                    remove_entry_queue2(taskId, index);
                }
__CPROVER_assume(cs>=54);

                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && !First_Call){
__CPROVER_assume(cs>=54);

                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }else if (score1[taskId]=1 && First_Call){
__CPROVER_assume(cs>=54);

                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
__CPROVER_assume(cs>=54);

        }else if (index == 2){ //Wait
__CPROVER_assume(cs>=54);

        }
__CPROVER_assume(cs>=54);

        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
__CPROVER_assume(cs>=54);

    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 43;
        goto end;
    }
__CPROVER_assume(cs>=54);

    //  }
l54: if (pc[ct]>54||54>=cs) goto l55;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l55: if (pc[ct]>55||55>=cs) goto l56;
    my_peer=return3[taskId];
l56: if (pc[ct]>56||56>=cs) goto l57;
    if (my_peer==taskId){
l57: if (pc[ct]>57||57>=cs) goto l58;
        cs = size[taskId];
l58: if (pc[ct]>58||58>=cs) goto l59;
        goto end;
    }
__CPROVER_assume(cs>=59);

l59: if (pc[ct]>59||59>=cs) goto end;
    test[taskId]=my_peer;
    //--------------------------
    //  }
end:
 return;
}