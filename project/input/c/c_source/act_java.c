#include <stdio.h>
#include <assert.h>

#define TRUE 1
#define FALSE 0
#define FREE -1

#define NB_TASK 3 //without main task

/*-------------------------------------------------------------*/
int active[NB_TASK+1] = {TRUE, FALSE, FALSE, FALSE};//, FALSE, FALSE, FALSE, FALSE, FALSE};
int cs, ct;
int pc[NB_TASK+1];
int size[NB_TASK+1];
int dead = 0;

void create(int t, int id){
    active[id] = TRUE;
    t = id;
}
/*-------------------------------------------------------------*/
int stock = 22;
int assignCount = 0;
int taskInWait = 0;

/*
public synchronized void request(int id, int y) {
        System.out.println(id + " IN Monitor: current stock = "+stock);
        try{
            while (assignCount != 0)
                wait();
            stock -= y;
            System.out.println(id + " IN Monitor: stock after resquest of " + id +" = "+stock);
            if (stock<0)
                assign(id, y);
        }catch(InterruptedException e){
        }finally{
        }
    }

    public synchronized void assign(int id, int y){
        assignCount++;
        System.out.println(id + " IN Monitor: " + id + " is waiting (assignCount = "+assignCount+")");
        try{
            assert assignCount<3 : "assignCount == 3";
            while (stock < 0)
                wait();
        }catch(InterruptedException e){
        }finally{
            assignCount--;
            System.out.println(id + " IN Monitor: " + id + " is getting out (assignCount = "+assignCount+")");
            notifyAll();
        }
    }

    public synchronized void release(int id, int y) {
        stock += y;
        System.out.println(id + " IN Monitor: " + id + " release "+ y + " unit (current stock = "+stock+")");
        notifyAll();
    }
*/

void t_one(){
    static int taskId=1;
    
    static int i = 0;
    static int MyClaim = 0;
    static int FullAllocation = 0;
    static int y;
   
   // for (i = 1; i<5; i++){
   i = 1;
        MyClaim = (5 + i) % 5;
        //   MyClaim = (5 - i) % 5; //deadlock
        //m.request(id, MyClaim);
        y = MyClaim;
        //atomic
        if (assignCount != 0){
            taskInWait = taskInWait+1;
        }
        __CPROVER_assume(!(assignCount != 0));
        taskInWait = taskInWait-1;
        stock -= y;
        if (stock<0){
            //assign(id, y);
            assignCount++;
            if (stock<0){
                taskInWait = taskInWait+1;
            }
            __CPROVER_assume(!(stock < 0));
            taskInWait = taskInWait-1;
            assignCount--;
        }
        //atomic
        FullAllocation += MyClaim;
    
    i = 2;
        MyClaim = (5 + i) % 5;
        //   MyClaim = (5 - i) % 5; //deadlock
        //m.request(id, MyClaim);
        y = MyClaim;
        //atomic
        if (assignCount != 0){
            taskInWait = taskInWait+1;
        }
        __CPROVER_assume(!(assignCount != 0));
        taskInWait = taskInWait-1;
        stock -= y;
        if (stock<0){
            //assign(id, y);
            assignCount++;
            if (stock<0){
                taskInWait = taskInWait+1;
            }
            __CPROVER_assume(!(stock < 0));
            taskInWait = taskInWait-1;
            assignCount--;
        }
        //atomic
        FullAllocation += MyClaim;
    
    i = 3;
        MyClaim = (5 + i) % 5;
        //   MyClaim = (5 - i) % 5; //deadlock
        //m.request(id, MyClaim);
        y = MyClaim;
         //atomic
        if (assignCount != 0){
            taskInWait = taskInWait+1;
        }
        __CPROVER_assume(!(assignCount != 0));
        taskInWait = taskInWait-1;
        stock -= y;
        if (stock<0){
            //assign(id, y);
            assignCount++;
            if (stock<0){
                taskInWait = taskInWait+1;
            }
            __CPROVER_assume(!(stock < 0));
            taskInWait = taskInWait-1;
            assignCount--;
        }
        //atomic
        FullAllocation += MyClaim;
    
    i = 4;
        MyClaim = (5 + i) % 5;
        //   MyClaim = (5 - i) % 5; //deadlock
        //m.request(id, MyClaim);
        y = MyClaim;
         //atomic
        if (assignCount != 0){
            taskInWait = taskInWait+1;
        }
        __CPROVER_assume(!(assignCount != 0));
        taskInWait = taskInWait-1;
        stock -= y;
        if (stock<0){
            //assign(id, y);
            assignCount++;
            if (stock<0){
                taskInWait = taskInWait+1;
            }
            __CPROVER_assume(!(stock < 0));
            taskInWait = taskInWait-1;
            assignCount--;
        }
        //atomic
        FullAllocation += MyClaim;
   // }
   // m.release(id, FullAllocation);
   //atomic
    y=FullAllocation;
    stock += y;
   //atomic
}











