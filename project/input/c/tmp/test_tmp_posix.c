#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

#define NB_TASK 4 //without task main
#define NB_ENTRY 2
#define NB_PROCEDURE 0
#define NB_FUNCTION 0

#define TRUE 1
#define FALSE 0

#define FREE -1
#define DESTROY -2

static volatile int flag[NB_TASK+1] = {-1, -1, -1, -1, -1};
static volatile int turn[NB_TASK] = {-1, -1, -1, -1};

int x = 0;

void t_one(int *id){
    int taskId = *id;
    int i, j, wait, x_tmp;
    
    printf("%d init with x = %d\n", taskId, x);

    for(i = 1; i < NB_TASK; i++){
        flag[taskId] = i;
        turn[i] = taskId;
        do {
            sleep(1);
            wait = FALSE;
            for (j = 1; j < NB_TASK+1 && j != taskId; j++){
                wait = wait || flag[j] >= i;
            }
        }while(wait && turn[i] == taskId);
    }
    
    printf("%d after wait with x = %d\n", taskId, x);
    
    if (taskId % 2 != 0){
        x_tmp = x;
       // sleep(1);
        x_tmp = x_tmp + 1;
       // sleep(1);
        x = x_tmp;
    }else{
        x_tmp = x;
       // sleep(1);
        x_tmp = x_tmp - 1;
       // sleep(1);
        x = x_tmp;
    }
    printf("tmp %d = %d \n", taskId, x_tmp);
    
    flag[taskId] = -1;
}

int main(){
    pthread_t t1, t2, t3, t4;
    int arg[NB_TASK+1] = {0, 1, 2,3,4};

    //printf("init x = %d\n", x);
    pthread_create(&t1, NULL, (void *(*)(void *))t_one, &arg[1]);
    //printf("init x = %d\n", x);
    pthread_create(&t2, NULL, (void *(*)(void *))t_one, &arg[2]);
    //printf("init x = %d\n", x);
    pthread_create(&t3, NULL, (void *(*)(void *))t_one, &arg[3]);
    //printf("init x = %d\n", x);
    pthread_create(&t4, NULL, (void *(*)(void *))t_one, &arg[4]);
    //printf("init x = %d\n", x);

    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    pthread_join(t3, NULL);
    pthread_join(t4, NULL);
    
    printf("x = %d\n", x);

    return 0;
}













