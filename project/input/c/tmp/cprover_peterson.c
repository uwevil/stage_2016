#include <stdio.h>
#include <pthread.h>
#include <assert.h>


#define NB_TASK 2 //without task main
#define NB_ENTRY 2
#define NB_PROCEDURE 0
#define NB_FUNCTION 0

#define TRUE 1
#define FALSE 0

#define FREE -1
#define DESTROY -2

int active[NB_TASK+1] = {1, 0, 0};
int cs, ct;
int pc[NB_TASK+1];
int size[NB_TASK+1];

int arg[NB_TASK+1];

#define G(L) __CPROVER_assume(cs>=L)
#define J(A,B) if(pc[ct]>A||A>=cs){goto B;}

typedef int pthread;
typedef int pthread_mutex;

////////////////////////////////////////////////////////////////
static int turn = 1;
static int flag[NB_TASK+1] = {FALSE, FALSE, FALSE};

static int x = 0;

void *t_one(void *arg){
    static int taskId = 1;
    static int other = 0;
    __CPROVER_atomic_begin();
    other = (taskId % NB_TASK) + 1;
    flag[taskId] = TRUE;
    turn = other;
    __CPROVER_atomic_end();
    
    __CPROVER_assume(!(flag[other] && turn == other));
    
    static ax;
    ax = x;
    ax = ax + 1;
    x = ax;
    
    flag[taskId] = FALSE;
}

void *t_two(void *arg){
    static int taskId = 2;
    static int other = 0;
    __CPROVER_atomic_begin();
    other = (taskId % NB_TASK) + 1;
    flag[taskId] = TRUE;
    turn = other;
    __CPROVER_atomic_end();
    
    __CPROVER_assume(!(flag[other] && turn == other));

    static int bx;
    bx = x;
    bx = bx - 1;
    x = bx;
    
    flag[taskId] = FALSE;
}

int main(){
    pthread_t t1, t2;
    pthread_create(&t1, NULL, t_one, NULL);
    pthread_create(&t2, NULL, t_two, NULL);
    
    pthread_join(t1, 0);
    pthread_join(t2, 0);
    
    if(flag[1]==flag[2]&&flag[1]==FALSE){
        __CPROVER_assert(x == 0, "OK x != 0");
        __CPROVER_assert(FALSE, "OK");
    }else{
        __CPROVER_assert(FALSE, "KO");
    }

}




