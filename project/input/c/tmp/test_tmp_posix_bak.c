#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

#define NB_TASK 4 //without task main
#define NB_ENTRY 2
#define NB_PROCEDURE 0
#define NB_FUNCTION 0

#define TRUE 1
#define FALSE 0

#define FREE -1
#define DESTROY -2

static volatile int flag[NB_TASK+1] = {-1, -1, -1, -1, -1};
static volatile int turn[NB_TASK] = {-1, -1, -1, -1};

static int x = 0;

void t_one(void){
    printf("zzzz\n");

    static int taskId = 1;
    static int i, j, wait;
/*
    for(i = 1; i <= NB_TASK; i++){
        flag[taskId] = i;
        turn[i] = taskId;
        wait = TRUE;
        for (j = 1; j <= NB_TASK+1 && j != taskId; j++){
            wait = wait || flag[j] >= i;
        }
        while(wait && turn[i] == taskId){
        }
    }
    int tmp;
    for(i = 0; i < 10; i++){
        tmp = x;
        sleep(1);
        tmp = tmp + 1;
        sleep(1);
        x = tmp;
        sleep(1);
    }
        printf("tmp = %d\n", x);
        */

}

void t_two(void){
 /*   static int taskId = 2;
    static int i, j, wait;
    for(i = 1; i <= NB_TASK; i++){
        flag[taskId] = i;
        turn[i] = taskId;
        wait = TRUE;
        for (j = 1; j <= NB_TASK+1 && j != taskId; j++){
            wait = wait || flag[j] >= i;
        }
        while(wait && turn[i] == taskId){
        }
    }
    
    int tmp;
    for(i = 0; i < 10; i++){
        tmp = x;
        sleep(1);
        tmp = tmp + 1;
        sleep(1);
        x = tmp;
        sleep(1);
    }
        printf("tmp = %d\n", x);
*/
}

void t_three(void){
 /*   static int taskId = 3;
    static int i, j, wait;
    for(i = 1; i <= NB_TASK; i++){
        flag[taskId] = i;
        turn[i] = taskId;
        wait = TRUE;
        for (j = 1; j <= NB_TASK+1 && j != taskId; j++){
            wait = wait || flag[j] >= i;
        }
        while(wait && turn[i] == taskId){
        }
    }
    
    int tmp;
    for(i = 0; i < 10; i++){
        tmp = x;
        sleep(1);
        tmp = tmp + 1;
        sleep(1);
        x = tmp;
        sleep(1);
    }
        printf("tmp = %d\n", x);
*/
}

void t_four(void){
    printf("zzzz\n");
 /*   static int taskId = 4;
    static int i, j, wait;
    for(i = 1; i <= NB_TASK; i++){
        flag[taskId] = i;
        turn[i] = taskId;
        wait = TRUE;
        for (j = 1; j <= NB_TASK+1 && j != taskId; j++){
            wait = wait || flag[j] >= i;
        }
        while(wait && turn[i] == taskId){
        }
    }
    
   int tmp;
    for(i = 0; i < 10; i++){
        tmp = x;
        sleep(1);
        tmp = tmp + 1;
        sleep(1);
        x = tmp;
        sleep(1);
    }
    printf("tmp = %d\n", x);

   */
}


int main(void){
    pthread_t t1, t2, t3, t4;
    
    pthread_create(&t1, NULL, (void *(*)(void *))t_one, NULL);
    pthread_create(&t2, NULL, (void *(*)(void *))t_two, NULL);
    pthread_create(&t3, NULL, (void *(*)(void *))t_three, NULL);
    pthread_create(&t4, NULL, (void *(*)(void *))t_four, NULL);

    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    pthread_join(t3, NULL);
    pthread_join(t4, NULL);
    
    printf("x = %d\n", x);

    return 0;
}













