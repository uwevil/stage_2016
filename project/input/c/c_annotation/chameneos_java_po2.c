#include <stdio.h>
#include <assert.h>

#define TRUE 1
#define FALSE 0
#define FREE -1

#define NB_TASK 4 //without main task

int const nb_request = 6;

/*-------------------------------------------------------------*/
int active[NB_TASK+1] = {TRUE, FALSE, FALSE, FALSE, FALSE};//, FALSE, FALSE, FALSE, FALSE, FALSE};
int cs, ct;
int pc[NB_TASK+1];
int size[NB_TASK+1];
int dead = 0;

void create(int t, int id){
    active[id] = TRUE;
    t = id;
}
/*-------------------------------------------------------------*/
#define NB_ENTRY1 1
#define NB_PROCEDURE1 1
#define NB_FUNCTION1 0

int tabStatus1[NB_TASK+1] = {TRUE, TRUE, TRUE, TRUE, TRUE}; //, TRUE, TRUE, TRUE, TRUE, TRUE};
int tabWaiting1[NB_TASK+1] = {0, 0, 0, 0,0}; //, 0, 0, 0, 0, 0};
int tabBarrier1[NB_ENTRY1+1] = {FALSE, FALSE};

int currentTask1=1;

int tabEntryCount1[NB_ENTRY1+1] = {0,0};
int taskInWait = 0;

int externalLock1 = FREE;

void lock1(int id){
    __CPROVER_assume(externalLock1==FREE);
    externalLock1=id;
}

void transfert1(int id, int newId){
    __CPROVER_assert(externalLock1==id, "OKexternalLock1!=id");
    externalLock1=newId;
}

void unlock1(int id){
    __CPROVER_assert(externalLock1==id, "OKexternalLock1!=id");
    externalLock1=FREE;
}
/*---------------------------------------------------------------------------*/

int free1 = TRUE;

void evaluate_barrier1(int index){
    if (index == 1){ //Acquire
        tabBarrier1[index] = free1;
    }
}

void reevaluate_barrier1(){
    tabBarrier1[1] = free1; //Acquire
}

void setTabStatusOn1(int taskId){
    tabStatus1[taskId] = TRUE;
    taskInWait = taskInWait - 1;
}

void setTabStatusOff1(int taskId){
    tabStatus1[taskId] = FALSE;
    taskInWait = taskInWait + 1;
    __CPROVER_assert(taskInWait<NB_TASK, "OKdeadlock1");
}


void add_entry_queue1(int taskId, int index){
    tabEntryCount1[index] = tabEntryCount1[index]+1;
    tabWaiting1[taskId] = index;
    setTabStatusOff1(taskId);
}

void remove_entry_queue1(int taskId, int index){
    tabEntryCount1[index] = tabEntryCount1[index]-1;
    tabWaiting1[taskId] = 0;
}

int current_task_eligible1(){ //return taskId
    int j;
    for (j=1; j<=NB_TASK;j++){
        if (tabWaiting1[currentTask1]!=0&&tabBarrier1[tabWaiting1[currentTask1]]){
            return currentTask1;
        }
        currentTask1 = (currentTask1 % NB_TASK)+1;
    }
    return 0;
}

void run_entry_body1(int taskId, int index);

void requeue1(int taskId, int index){ //requeue to another or same entry queue
    evaluate_barrier1(index);
    
    if (tabBarrier1[index]==FALSE){
        add_entry_queue1(taskId, index);
        unlock1(taskId);
        __CPROVER_assume(tabStatus1[taskId]&&externalLock1==taskId);
        remove_entry_queue1(taskId, index);
    }
    run_entry_body1(taskId,index);
}

void run_entry_body1(int taskId, int index){
    if (index == 1) { //Acquire
        free1 = FALSE;
    }
}

int check_and_run_any_entry1(){
    reevaluate_barrier1();
    int taskIdEligible1 = current_task_eligible1();
    if (taskIdEligible1==0){
        return FREE;
    }
    setTabStatusOn1(taskIdEligible1);
    return taskIdEligible1;
}

void run_function_call1(taskId, index){
}

void run_procedure_call1(taskId, index){
    int taskIdEligible1;
    if (index == 1){
        free1 = TRUE;
    }
    taskIdEligible1 = check_and_run_any_entry1();
    transfert1(taskId, taskIdEligible1);
}

void run_entry_call1(int taskId, int index){
    int taskIdEligible1;
    evaluate_barrier1(index);
    
    if (tabBarrier1[index]==FALSE){
        add_entry_queue1(taskId, index);
        unlock1(taskId);
        __CPROVER_assume(tabStatus1[taskId]&&externalLock1==taskId);
        remove_entry_queue1(taskId, index);
    }
    run_entry_body1(taskId,index);
    taskIdEligible1 = check_and_run_any_entry1();
    transfert1(taskId, taskIdEligible1);
}

void protected_object_call1(int taskId, int appel, int index){
    lock1(taskId);
    
    if (appel == 0){ //function
        run_function_call1(taskId, index);
    }else if (appel == 1){ //procedure
        run_procedure_call1(taskId, index);
    }else if (appel == 2){ //entry
        run_entry_call1(taskId, index);
    }
}

/*---------------------------------------------------------------------*/

/*-------------------------------------------------------------*/
#define NB_ENTRY2 2
#define NB_PROCEDURE2 0
#define NB_FUNCTION2 0

int tabStatus2[NB_TASK+1] = {TRUE, TRUE, TRUE, TRUE, TRUE}; //, TRUE, TRUE, TRUE, TRUE, TRUE};
int tabWaiting2[NB_TASK+1] = {0, 0, 0, 0, 0}; //, 0, 0, 0, 0, 0};
int tabBarrier2[NB_ENTRY2+1] = {FALSE, FALSE, FALSE};

int currentTask2=1;

int tabEntryCount2[NB_ENTRY2+1] = {0,0,0};
//int taskInWait = 0;

int externalLock2 = FREE;

void lock2(int id){
    __CPROVER_assume(externalLock2==FREE);
    externalLock2=id;
}

void transfert2(int id, int newId){
    __CPROVER_assert(externalLock2==id, "OKexternalLock2!=id");
    externalLock2=newId;
}

void unlock2(int id){
    __CPROVER_assert(externalLock2==id, "OKexternalLock2!=id");
    externalLock2=FREE;
}
/*---------------------------------------------------------------------------*/

#define BLUE 1
#define RED 2
#define YELLOW 3

int First_Call = TRUE;
int MustWait = FALSE;
int a_chameneos, b_chameneos;
int NotifyAll = FALSE;

int firstCall = TRUE;

int test[NB_TASK+1];

int x1[NB_TASK+1];
int x_other1[NB_TASK+1];
int score1[NB_TASK+1];

int x2[NB_TASK+1];
int x_other2[NB_TASK+1];
int score2[NB_TASK+1];

int x3[NB_TASK+1];
int return3[NB_TASK+1];

void protected_object_call2(int taskId, int appel, int index);

void get_a_peer3(int taskId){
    int score=0;
    while (score!=2) {
        x1[taskId]=x3[taskId];
        protected_object_call2(taskId, 2, 1);
        score=score1[taskId];
    }
    return3[taskId]=x_other1[taskId];
}

/*---------------------------------------------------------------------------*/

void evaluate_barrier2(int index){
    if (index == 1){ //Cooperate
        tabBarrier2[index] = TRUE;
    }else if (index == 2){ //Wait
        tabBarrier2[index] = NotifyAll;
    }
}

void reevaluate_barrier2(){
    tabBarrier2[1] = TRUE; //Cooperate
    tabBarrier2[2] = NotifyAll; //Wait
}

void setTabStatusOn2(int taskId){
    tabStatus2[taskId] = TRUE;
    taskInWait = taskInWait - 1;
}

void setTabStatusOff2(int taskId){
    tabStatus2[taskId] = FALSE;
    taskInWait = taskInWait + 1;
    __CPROVER_assert(taskInWait<NB_TASK, "OKdeadlock2");
}


void add_entry_queue2(int taskId, int index){
    tabEntryCount2[index] = tabEntryCount2[index]+1;
    tabWaiting2[taskId] = index;
    setTabStatusOff2(taskId);
}

void remove_entry_queue2(int taskId, int index){
    tabEntryCount2[index] = tabEntryCount2[index]-1;
    tabWaiting2[taskId] = 0;
}

int current_task_eligible2(){ //return taskId
    int j;
    for (j=1; j<=NB_TASK;j++){
        if (tabWaiting2[currentTask2]!=0&&tabBarrier2[tabWaiting2[currentTask2]]){
            return currentTask2;
        }
        currentTask2 = (currentTask2 % NB_TASK)+1;
    }
    return 0;
}

void run_entry_body2(int taskId, int index);

void requeue2(int taskId, int index){ //requeue to another or same entry queue
    evaluate_barrier2(index);
    
    if (tabBarrier2[index]==FALSE){
        add_entry_queue2(taskId, index);
        unlock2(taskId);
        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        remove_entry_queue2(taskId, index);
    }
    run_entry_body2(taskId,index);
}

void run_entry_body2(int taskId, int index){
    if (index == 1) { //Cooperate
        if (score1[taskId]==0 && MustWait){
            NotifyAll = FALSE;
            requeue2(taskId, 2);
        }
        if (score1[taskId]==0 && First_Call){
            a_chameneos = x1[taskId];
            First_Call = FALSE;
            score1[taskId]=1;
            NotifyAll = FALSE;
            requeue2(taskId, 2);
        }
        if (score1[taskId]==0 && !First_Call){
            b_chameneos=x1[taskId];
            x_other1[taskId]=a_chameneos;
            First_Call=TRUE;
            MustWait=TRUE;
            score1[taskId]=2;
            NotifyAll=TRUE;
        }
        if (score1[taskId]=1 && First_Call){
            x_other1[taskId]=b_chameneos;
            MustWait=FALSE;
            score1[taskId]=2;
            NotifyAll=TRUE;
        }
    }else if (index == 2){ //Wait
    }
}

int check_and_run_any_entry2(){
    reevaluate_barrier2();
    int taskIdEligible2 = current_task_eligible2();
    if (taskIdEligible2==0){
        return FREE;
    }
    setTabStatusOn2(taskIdEligible2);
    return taskIdEligible2;
}

void run_function_call2(taskId, index){
}

void run_procedure_call2(taskId, index){
    /*  int taskIdEligible;
     if (index == 1){
     stock = stock + y[taskId];
     }
     taskIdEligible = check_and_run_any_entry();
     transfert(taskId, taskIdEligible);
     */
}

void run_entry_call2(int taskId, int index){
    int taskIdEligible2;
    evaluate_barrier2(index);
    
    if (tabBarrier2[index]==FALSE){
        add_entry_queue2(taskId, index);
        unlock2(taskId);
        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        remove_entry_queue2(taskId, index);
    }
    run_entry_body2(taskId,index);
    taskIdEligible2 = check_and_run_any_entry2();
    transfert2(taskId, taskIdEligible2);
}

void protected_object_call2(int taskId, int appel, int index){
    lock2(taskId);
    
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        run_entry_call2(taskId, index);
    }
}

/*---------------------------------------------------------------------*/

/*---------------------------------------------------------------------*/


void t_one(){
    static int taskId=1;
    static int taskIdEligible;
    static int my_peer = 0;
    static int i;
    static int score;
    static int appel;
    static int index;
    static int taskIdEligible2;
    
//@begin
    i = 1;
   // for (i = 1; i<=nb_request; i++){
        x3[taskId]=taskId;
        //get_a_peer3(taskId);
        score=0;
      //  while (score!=2) {
//@controlLabel
            x1[taskId]=x3[taskId];
            score1[taskId]=score;
            //protected_object_call2(taskId, 2, 1);
            appel = 2;
            index = 1;
//@controlBegin
            lock2(taskId);
            
            if (appel == 0){ //function
                run_function_call2(taskId, index);
            }else if (appel == 1){ //procedure
//@controlAssume
                run_procedure_call2(taskId, index);
            }else if (appel == 2){ //entry
//@controlAssume
                //run_entry_call2(taskId, index);
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
//@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
//@controlBegin
                    remove_entry_queue2(taskId, index);
                }
//@controlAssume
                //run_entry_body2(taskId,index);
                if (index == 1) { //Cooperate
                    if (score1[taskId]==0 && MustWait){
                        NotifyAll = FALSE;
                        //requeue2(taskId, 2);
                        index = 2;
                        evaluate_barrier2(index);
                        
                        if (tabBarrier2[index]==FALSE){
                            add_entry_queue2(taskId, index);
                            unlock2(taskId);
//@controlEnd
                            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
//@controlBegin
                            remove_entry_queue2(taskId, index);
                        }
//@controlAssume
                        //run_entry_body2(taskId,index);
                        goto while1;
                        //--------------------------
                        //--------------------------
                    }
//@controlAssume
                    if (score1[taskId]==0 && First_Call){
                        a_chameneos = x1[taskId];
                        First_Call = FALSE;
                        score1[taskId]=1;
                        NotifyAll = FALSE;
                        //requeue2(taskId, 2);
                        index = 2;
                        evaluate_barrier2(index);
                        
                        if (tabBarrier2[index]==FALSE){
                            add_entry_queue2(taskId, index);
                            unlock2(taskId);
//@controlEnd
                            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
//@controlBegin
                            remove_entry_queue2(taskId, index);
                        }
//@controlAssume
                        //run_entry_body2(taskId,index);
                        goto while1;
                        //--------------------------
                        //--------------------------
                    }
//@controlAssume
                    if (score1[taskId]==0 && !First_Call){
                        b_chameneos=x1[taskId];
                        x_other1[taskId]=a_chameneos;
                        First_Call=TRUE;
                        MustWait=TRUE;
                        score1[taskId]=2;
                        NotifyAll=TRUE;
                    }
//@controlAssume
                    if (score1[taskId]=1 && First_Call){
                        x_other1[taskId]=b_chameneos;
                        MustWait=FALSE;
                        score1[taskId]=2;
                        NotifyAll=TRUE;
                    }
//@controlAssume
                }else if (index == 2){ //Wait
//@controlAssume
                }
//@controlAssume
                //--------------------------
                taskIdEligible2 = check_and_run_any_entry2();
                transfert2(taskId, taskIdEligible2);
            }
//@controlAssume
            //--------------------------
    while1:
            score=score1[taskId];
   // __CPROVER_assume(score==2);
    if (score!=2){
        cs = ?;
        goto end;
    }
//@controlAssume
//@controlEnd
    
      //  }
    
        return3[taskId]=x_other1[taskId];
        //--------------------------
        my_peer=return3[taskId];
        if (my_peer==taskId){
            cs = size[taskId];
            goto end;
        }
//@controlAssume
     test[my_peer]=taskId;
    //--------------------------
    
    i = 2;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
    score=0;
    //  while (score!=2) {
    //@controlLabel
    x1[taskId]=x3[taskId];
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
    appel = 2;
    index = 1;
    //@controlBegin
    lock2(taskId);
    
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        //@controlAssume
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        //@controlAssume
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
            //@controlEnd
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            //@controlBegin
            remove_entry_queue2(taskId, index);
        }
        //@controlAssume
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    //@controlBegin
                    remove_entry_queue2(taskId, index);
                }
                //@controlAssume
                //run_entry_body2(taskId,index);
                goto while2;
                //--------------------------
                //--------------------------
            }
            //@controlAssume
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    //@controlBegin
                    remove_entry_queue2(taskId, index);
                }
                //@controlAssume
                //run_entry_body2(taskId,index);
                goto while2;
                //--------------------------
                //--------------------------
            }
            //@controlAssume
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
//@controlAssume
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
//@controlAssume
        }else if (index == 2){ //Wait
            //@controlAssume
        }
        //@controlAssume
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    //@controlAssume
    //--------------------------
    while2:
    score=score1[taskId];
    
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = ?;
        goto end;
    }
    //@controlAssume
    //@controlEnd
    
    //  }
    
    return3[taskId]=x_other1[taskId];
    //--------------------------
    my_peer=return3[taskId];
    if (my_peer==taskId){
        cs = size[taskId];
        goto end;
    }
//@controlAssume
     test[my_peer]=taskId;
    //--------------------------
    
    i=3;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
    score=0;
    //  while (score!=2) {
    //@controlLabel
    x1[taskId]=x3[taskId];
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
    appel = 2;
    index = 1;
    //@controlBegin
    lock2(taskId);
    
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        //@controlAssume
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        //@controlAssume
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
            //@controlEnd
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            //@controlBegin
            remove_entry_queue2(taskId, index);
        }
        //@controlAssume
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    //@controlBegin
                    remove_entry_queue2(taskId, index);
                }
                //@controlAssume
                //run_entry_body2(taskId,index);
                goto while3;
                //--------------------------
                //--------------------------
            }
            //@controlAssume
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    //@controlBegin
                    remove_entry_queue2(taskId, index);
                }
                //@controlAssume
                //run_entry_body2(taskId,index);
                goto while3;
                //--------------------------
                //--------------------------
            }
            //@controlAssume
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
//@controlAssume
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
//@controlAssume
        }else if (index == 2){ //Wait
            //@controlAssume
        }
        //@controlAssume
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    //@controlAssume
    //--------------------------
    while3:
    score=score1[taskId];
    
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = ?;
        goto end;
    }
    //@controlAssume
    //@controlEnd
    
    //  }
    
    return3[taskId]=x_other1[taskId];
    //--------------------------
    my_peer=return3[taskId];
    if (my_peer==taskId){
        cs = size[taskId];
        goto end;
    }
//@controlAssume
     test[my_peer]=taskId;
    //--------------------------
    
    i=4;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
    score=0;
    //  while (score!=2) {
    //@controlLabel
    x1[taskId]=x3[taskId];
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
    appel = 2;
    index = 1;
    //@controlBegin
    lock2(taskId);
    
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        //@controlAssume
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        //@controlAssume
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
            //@controlEnd
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            //@controlBegin
            remove_entry_queue2(taskId, index);
        }
        //@controlAssume
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    //@controlBegin
                    remove_entry_queue2(taskId, index);
                }
                //@controlAssume
                //run_entry_body2(taskId,index);
                goto while4;
                //--------------------------
                //--------------------------
            }
            //@controlAssume
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    //@controlBegin
                    remove_entry_queue2(taskId, index);
                }
                //@controlAssume
                //run_entry_body2(taskId,index);
                goto while4;
                //--------------------------
                //--------------------------
            }
            //@controlAssume
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
//@controlAssume
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
//@controlAssume
        }else if (index == 2){ //Wait
            //@controlAssume
        }
        //@controlAssume
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    //@controlAssume
    //--------------------------
    while4:
    score=score1[taskId];
    
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = ?;
        goto end;
    }
    //@controlAssume
    //@controlEnd
    
    //  }
    
    return3[taskId]=x_other1[taskId];
    //--------------------------
    my_peer=return3[taskId];
    if (my_peer==taskId){
        cs = size[taskId];
        goto end;
    }
//@controlAssume
     test[my_peer]=taskId;
    //--------------------------
    
    i=5;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
    score=0;
    //  while (score!=2) {
    //@controlLabel
    x1[taskId]=x3[taskId];
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
    appel = 2;
    index = 1;
    //@controlBegin
    lock2(taskId);
    
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        //@controlAssume
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        //@controlAssume
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
            //@controlEnd
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            //@controlBegin
            remove_entry_queue2(taskId, index);
        }
        //@controlAssume
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    //@controlBegin
                    remove_entry_queue2(taskId, index);
                }
                //@controlAssume
                //run_entry_body2(taskId,index);
                goto while5;
                //--------------------------
                //--------------------------
            }
            //@controlAssume
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    //@controlBegin
                    remove_entry_queue2(taskId, index);
                }
                //@controlAssume
                //run_entry_body2(taskId,index);
                goto while5;
                //--------------------------
                //--------------------------
            }
            //@controlAssume
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
//@controlAssume
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
//@controlAssume
        }else if (index == 2){ //Wait
            //@controlAssume
        }
        //@controlAssume
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    //@controlAssume
    //--------------------------
    while5:
    score=score1[taskId];
    
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = ?;
        goto end;
    }
    //@controlAssume
    //@controlEnd
    
    //  }
    
    return3[taskId]=x_other1[taskId];
    //--------------------------
    my_peer=return3[taskId];
    if (my_peer==taskId){
        cs = size[taskId];
        goto end;
    }
//@controlAssume
     test[my_peer]=taskId;
    //--------------------------
    
    i=6;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
    score=0;
    //  while (score!=2) {
    //@controlLabel
    x1[taskId]=x3[taskId];
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
    appel = 2;
    index = 1;
    //@controlBegin
    lock2(taskId);
    
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        //@controlAssume
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        //@controlAssume
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
            //@controlEnd
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            //@controlBegin
            remove_entry_queue2(taskId, index);
        }
        //@controlAssume
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    //@controlBegin
                    remove_entry_queue2(taskId, index);
                }
                //@controlAssume
                //run_entry_body2(taskId,index);
                goto while6;
                //--------------------------
                //--------------------------
            }
            //@controlAssume
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    //@controlBegin
                    remove_entry_queue2(taskId, index);
                }
                //@controlAssume
                //run_entry_body2(taskId,index);
                goto while6;
                //--------------------------
                //--------------------------
            }
            //@controlAssume
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
//@controlAssume
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
//@controlAssume
        }else if (index == 2){ //Wait
            //@controlAssume
        }
        //@controlAssume
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    //@controlAssume
    //--------------------------
    while6:
    score=score1[taskId];
    
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = ?;
        goto end;
    }
//@controlAssume
    //@controlEnd
    
    //  }
    
    return3[taskId]=x_other1[taskId];
    //--------------------------
    my_peer=return3[taskId];
    if (my_peer==taskId){
        cs = size[taskId];
        goto end;
    }
//@controlAssume
     test[my_peer]=taskId;
    //--------------------------
  //  }
//@end
}











