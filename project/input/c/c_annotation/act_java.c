void t_one(){
    static int taskId=1;
    
    static int i = 0;
    static int MyClaim = 0;
    static int FullAllocation = 0;
    static int y;

//@begin
   // for (i = 1; i<5; i++){
   i = 1;
           MyClaim = (5 - i) % 5; //deadlock
//@controlBegin
        //m.request(id, MyClaim);
        y = MyClaim;
        //atomic
        if (assignCount!=0){
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
//@controlEnd
            __CPROVER_assume(!(assignCount != 0));
//@controlBegin
            taskInWait = taskInWait - 1;
        }
//@controlAssume
        stock -= y;
        if (stock<0){
            //assign(id, y);
            assignCount++;
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
//@controlEnd
            __CPROVER_assume(!(stock < 0));
//@controlBegin
            assignCount--;
            taskInWait = taskInWait - 1;
        }
//@controlAssume
//@controlEnd
        //atomic
        FullAllocation = FullAllocation + MyClaim;
    
    i = 2;
           MyClaim = (5 - i) % 5; //deadlock
//@controlBegin
        //m.request(id, MyClaim);
        y = MyClaim;
         //atomic
        if (assignCount!=0){
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
//@controlEnd
            __CPROVER_assume(!(assignCount != 0));
//@controlBegin
            taskInWait = taskInWait - 1;
        }
//@controlAssume
        stock -= y;
        if (stock<0){
            //assign(id, y);
            assignCount++;
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
//@controlEnd
            __CPROVER_assume(!(stock < 0));
//@controlBegin
            assignCount--;
            taskInWait = taskInWait - 1;
        }
//@controlAssume
//@controlEnd
        //atomic
        FullAllocation = FullAllocation + MyClaim;
    
    i = 3;
           MyClaim = (5 - i) % 5; //deadlock
//@controlBegin
        //m.request(id, MyClaim);
        y = MyClaim;
         //atomic
        if (assignCount!=0){
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
//@controlEnd
            __CPROVER_assume(!(assignCount != 0));
//@controlBegin
            taskInWait = taskInWait - 1;
        }
//@controlAssume
        stock -= y;
        if (stock<0){
            //assign(id, y);
            assignCount++;
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
//@controlEnd
            __CPROVER_assume(!(stock < 0));
//@controlBegin
            assignCount--;
            taskInWait = taskInWait - 1;
        }
//@controlAssume
//@controlEnd
        //atomic
        FullAllocation = FullAllocation + MyClaim;
    
    i = 4;
           MyClaim = (5 - i) % 5; //deadlock
//@controlBegin
        //m.request(id, MyClaim);
        y = MyClaim;
        //atomic
        if (assignCount!=0){
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
//@controlEnd
            __CPROVER_assume(!(assignCount != 0));
//@controlBegin
            taskInWait = taskInWait - 1;
        }
//@controlAssume
        stock -= y;
        if (stock<0){
            //assign(id, y);
            assignCount++;
            taskInWait = taskInWait+1;
            __CPROVER_assert(taskInWait<3, "OK taskInWait==3");
//@controlEnd
            __CPROVER_assume(!(stock < 0));
//@controlBegin
            assignCount--;
            taskInWait = taskInWait - 1;
        }
//@controlAssume
//@controlEnd
        //atomic
        FullAllocation = FullAllocation + MyClaim;
   // }
   // m.release(id, FullAllocation);
//@controlBegin
   //atomic
    y=FullAllocation;
    stock += y;
   //atomic
//@controlEnd
//@end
}











