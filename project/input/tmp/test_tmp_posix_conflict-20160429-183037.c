#include <stdio.h>
#include <pthread.h>

#define NB_TASK 4 //without task main
#define NB_ENTRY 2
#define NB_PROCEDURE 0
#define NB_FUNCTION 0

#define TRUE 1
#define FALSE 0

#define FREE -1
#define DESTROY -2

static volatile int flag[NB_TASK+1] = {-1, -1, -1, -1, -1};
static volatile int turn[NB_TASK] = {-1, -1, -1, -1};

static int x = 0;

void * t_one(){
    static int taskId = 1;
    static int i, j, wait;
    for(i = 1; i <= NB_TASK; i++){
        flag[taskId] = i;
        turn[i] = taskId;
        wait = TRUE;
        for (j = 1; j <= NB_TASK+1 && j != taskId; j++){
            wait = wait || flag[j] >= i;
        }
        while(cond && turn[i] == taskId){
        }
    }
    int tmp;
    for(i = 0; i < 10; i++){
        tmp = x;
        sleep(1);
        tmp = tmp + 1;
        sleep(1);
        x = tmp;
        sleep(1);
    }
}

void * t_two(){
    static int taskId = 2;
    static int i, j, wait;
    for(i = 1; i <= NB_TASK; i++){
        flag[taskId] = i;
        turn[i] = taskId;
        wait = TRUE;
        for (j = 1; j <= NB_TASK+1 && j != taskId; j++){
            wait = wait || flag[j] >= i;
        }
        while(cond && turn[i] == taskId){
        }
    }
    
    int tmp;
    for(i = 0; i < 10; i++){
        tmp = x;
        sleep(1)
        tmp = tmp + 1;
        sleep(1);
        x = tmp;
        sleep(1);
    }
}

void * t_three(){
    static int taskId = 3;
    static int i, j, wait;
    for(i = 1; i <= NB_TASK; i++){
        flag[taskId] = i;
        turn[i] = taskId;
        wait = TRUE;
        for (j = 1; j <= NB_TASK+1 && j != taskId; j++){
            wait = wait || flag[j] >= i;
        }
        while(cond && turn[i] == taskId){
        }
    }
    
    int tmp;
    for(i = 0; i < 10; i++){
        tmp = x;
        sleep(1)
        tmp = tmp + 1;
        sleep(1);
        x = tmp;
        sleep(1);
    }
}

void * t_four(){
    static int taskId = 4;
    static int i, j, wait;
    for(i = 1; i <= NB_TASK; i++){
        flag[taskId] = i;
        turn[i] = taskId;
        wait = TRUE;
        for (j = 1; j <= NB_TASK+1 && j != taskId; j++){
            wait = wait || flag[j] >= i;
        }
        while(cond && turn[i] == taskId){
        }
    }
    
   int tmp;
    for(i = 0; i < 10; i++){
        tmp = x;
        sleep(1)
        tmp = tmp + 1;
        sleep(1);
        x = tmp;
        sleep(1);
    }
}


int main(){
    pthread_t t1, t2, t3, t4;
    pthread_create(&t1, t_one, NULL, NULL);
    pthread_create(&t2, t_two, NULL, NULL);
    pthread_create(&t3, t_three, NULL, NULL);
    pthread_create(&t4, t_four, NULL, NULL);

    pthread_join(&t1, NULL);
    pthread_join(&t2, NULL);
    pthread_join(&t3, NULL);
    pthread_join(&t4, NULL);

    return 0;
}













