#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>

#define TRUE 1
#define FALSE 0
#define FREE -1

#define NB_TASK 3 //without main task
#define NB_ENTRY 2
#define NB_PROCEDURE 1
#define NB_FUNCTION 0

/*-------------------------------------------------------------*/
/*int active[NB_TASK+1] = {TRUE, FALSE, FALSE};
 int cs, ct;
 int pc[NB_TASK+1];
 int size[NB_TASK+1];
 
 void create(int t, int id){
 active[id] = TRUE;
 t = id;
 }*/
/*-------------------------------------------------------------*/

int tabStatus[NB_TASK+1] = {TRUE, TRUE, TRUE, TRUE};
int tabWaiting[NB_TASK+1] = {0, 0, 0, 0};
int tabBarrier[NB_ENTRY+1] = {FALSE, FALSE, FALSE};

int currentTask=1;

int y[NB_TASK+1] = {0,0,0,0};

int tabEntryCount[NB_ENTRY+1] = {0,0,0};
int taskInWait = 0;

int stock = 22;

int externalLock = FREE;

sem_t semaphore;

void assume(int a){
    if (!a){
        printf("assume error\n");
        exit(2);
    }
}
void assert(int a, int b){
    if (!a){
        printf("assert error %d\n", b);
        exit(2);
    }
}

void lock(int id){
    sem_wait(&semaphore);
    while(!(externalLock==FREE)){
        sem_post(&semaphore);
        sleep(1);
        sem_wait(&semaphore);
    }
    externalLock=id;
}

void transfert(int id, int newId){
    printf("externalLock = %d, id=%d, newId=%d\n", externalLock, id, newId);
    assert(externalLock==id, 2);
    externalLock=newId;
    sem_post(&semaphore);
}

void unlock(int id){
    assert(externalLock==id, 3);
    externalLock=FREE;
    sem_post(&semaphore);
}

/*---------------------------------------------------------------------------*/

void evaluate_barrier(int index){
    if (index == 1){ //Request
        tabBarrier[index] = tabEntryCount[2]==0;
    }else if (index == 2){ //Assign
        tabBarrier[index] = stock>=0;
    }
}

void reevaluate_barrier(){
    tabBarrier[1] = tabEntryCount[2]==0; //request
    tabBarrier[2] = stock>=0; //assign
}

void add_entry_queue(int taskId, int index){
    tabEntryCount[index] = tabEntryCount[index]+1;
    taskInWait = taskInWait + 1;
    assert(taskInWait<NB_TASK, 4);
    
    tabWaiting[taskId] = index;
    tabStatus[taskId] = FALSE;
}

void remove_entry_queue(int taskId, int index){
    tabEntryCount[index] = tabEntryCount[index]-1;
    taskInWait = taskInWait - 1;
    
    tabWaiting[taskId] = 0;
}

int current_task_eligible(){ //return taskId
    int j;
    for (j=1; j<=NB_TASK;j++){
        if (tabWaiting[currentTask]!=0&&tabBarrier[tabWaiting[currentTask]]){
            return currentTask;
        }
        currentTask = (currentTask % NB_TASK)+1;
    }
    return 0;
}

void run_entry_body(int taskId, int index);

void requeue(int taskId, int index){ //requeue to another or same entry queue
    evaluate_barrier(index);
    
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
        while(!(tabStatus[taskId]&&externalLock==taskId)){
            sem_post(&semaphore);
            sleep(1);
            sem_wait(&semaphore);
        }
        //    assume(tabStatus[taskId]&&externalLock==taskId);
        
        remove_entry_queue(taskId, index);
    }
    run_entry_body(taskId,index);
}

void run_entry_body(int taskId, int index){
    if (index == 1) { //Request
        stock = stock - y[taskId];
        if (stock < 0){
            requeue(taskId, 2); //requeue Assign
        }
    }else if (index == 2){ //Assign
    }
}

int check_and_run_any_entry(){
    reevaluate_barrier();
    int taskIdEligible = current_task_eligible();
    if (taskIdEligible==0){
        return FREE;
    }
    tabStatus[taskIdEligible] = TRUE;
    return taskIdEligible;
}

void run_function_call(taskId, index){
}

void run_procedure_call(taskId, index){
    int taskIdEligible;
    if (index == 1){
        stock = stock + y[taskId];
    }
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
}

void run_entry_call(int taskId, int index){
    int taskIdEligible;
    evaluate_barrier(index);
    
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
        while(!(tabStatus[taskId]&&externalLock==taskId)){
            sem_post(&semaphore);
            sleep(1);
            sem_wait(&semaphore);
        }
        //    assume(tabStatus[taskId]&&externalLock==taskId);
        remove_entry_queue(taskId, index);
    }
    run_entry_body(taskId,index);
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
}

void protected_object_call(int taskId, int appel, int index){
    lock(taskId);
    
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        run_entry_call(taskId, index);
    }
}


//const int NB_TASK = 2;

void t_one(){ //void *a){
    /*static int taskId = 1;
     static int appel = 2;
     static int index = 1;
     */
    int taskId = 1;
    int appel = 2;
    int index = 1;
    int i = 0;
    int myClaim = 0;
    int fullAllocation = 0;
    
    for (i=1; i<=4; i++){
        myClaim = (5-i)%5;
        printf(" --test1 begin myClaim = %d\n", myClaim);
        protected_object_call(taskId, 2, 1);
        printf(" --test1 end stock = %d\n", myClaim);
        sleep(1);
        fullAllocation = fullAllocation+myClaim;
        printf(" --test1 fullAllocation = %d\n", myClaim);
    }
    
    myClaim = fullAllocation;
    protected_object_call(taskId, 1, 1);
    fullAllocation = 0;
}

void t_two(){//void *a){
    /* static int taskId = 1;
     static int appel = 2;
     static int index = 2;*/
    int taskId = 2;
    int appel = 2;
    int index = 2;
    int i = 0;
    int myClaim = 0;
    int fullAllocation = 0;
    
    for (i=1; i<=4; i++){
        myClaim = (5-i)%5;
        printf("  **test2 begin myClaim = %d\n", myClaim);
        protected_object_call(taskId, 2, 1);
        printf("  **test2 end stock = %d\n", myClaim);
        sleep(1);
        fullAllocation = fullAllocation+myClaim;
        printf("  **test2 fullAllocation = %d\n", myClaim);
    }
    
    myClaim = fullAllocation;
    protected_object_call(taskId, 1, 1);
    fullAllocation = 0;
}

void t_three(){//void *a){
    /* static int taskId = 1;
     static int appel = 2;
     static int index = 2;*/
    int taskId = 3;
    int appel = 2;
    int index = 2;
    int i = 0;
    int myClaim = 0;
    int fullAllocation = 0;
    
    for (i=1; i<=4; i++){
        myClaim = (5-i)%5;
        printf("   $$test3 begin myClaim = %d\n", myClaim);
        protected_object_call(taskId, 2, 1);
        printf("   $$test3 end stock = %d\n", myClaim);
        sleep(1);
        fullAllocation = fullAllocation+myClaim;
        printf("   $$test3 fullAllocation = %d\n", myClaim);
    }
    
    myClaim = fullAllocation;
    protected_object_call(taskId, 1, 1);
    fullAllocation = 0;
}


int main(){
    pthread_t t1, t2, t3;
    int arg[NB_TASK+1] = {0, 1,2};
    sem_init(&semaphore, 0, 1);
    pthread_create(&t1, NULL, (void *(*)(void *))t_one, &arg[0]);
    pthread_create(&t2, NULL, (void *(*)(void *))t_two, &arg[0]);
    pthread_create(&t3, NULL, (void *(*)(void *))t_three, &arg[0]);
    
    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    pthread_join(t3, NULL);
    
    printf("test stock = %d\n", stock);
    
    assert(stock==0, 5);
    
    
    return 0;
}
