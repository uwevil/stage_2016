------------------------	Application main program ----------------
with Ada.Text_IO;
use Ada.Text_IO;
procedure application211c3 is
   type Client_Id is mod 3;
   -- Max_Client;
   Stock : Integer := 22; -- Resource_Number; -- number of currently available resources
   function Next_Hazard return Float is	begin
      return 0.1;
   end Next_Hazard;
   ---------------------- resource allocator     --------------
   -- resources are allocated according to the most simple FIFO algorithm
   -- that is sometimes names Best Effort
   protected type Control_Type is
      entry Request(Y : in Integer; Client_X : in  Client_Id);
      procedure Release(Y : in Integer; Client_X : in  Client_Id);
   private
      entry Assign(Y : in Integer; Client_X : in  Client_Id);
   end Control_Type;

   protected body Control_Type is
      entry Request(Y : in Integer; Client_X : in  Client_Id) when Assign'Count = 0 is
      begin
         Stock := Stock - Y;
         if Stock < 0 then requeue Assign; end if;
      end Request;
      entry Assign(Y : in Integer; Client_X : in  Client_Id) when Stock >= 0 is
      begin
         null;
      end Assign;
      procedure Release(Y : in Integer; Client_X : in  Client_Id) is
      begin
         Stock := Stock + Y;
      end Release;
   end Control_Type;

   resource : Control_Type;
   ----------------------------------------------------------
   -- Display_Lock provides mutual exclusion for text displaying on console
   -- allows to print lines of text correctly while concurrent requests are posted
   ----------------------------------------------------------
   protected type Server is
      entry Acquire;
      procedure Release;
   private
      Free : Boolean := True;
   end Server;
   protected body Server is
      entry Acquire when Free is
      begin
         Free := False;
      end Acquire ;
      procedure Release is
      begin
         Free := True;
      end Release;
   end Server;
   Display_Lock : Server;
   ----------------------------------------------------------
   --  displaying Application state
   ----------------------------------------------------------
   type Client_State is (Requesting, Using, Releasing, Quiescent);
   type Client_State_Array is array(Client_Id) of Client_State;
   State : Client_State_Array;  --  := (others => Quiescent);
   type Client_Id_Array is array(Client_Id) of Integer ;
   Claim : Client_Id_Array; -- := (others => 0);
   Full_Claim : Client_Id_Array; -- := (others => 0);
   Full_Allocation : Client_Id_Array; -- := (others => 0);
   procedure Display_States is
   begin
      Display_Lock.Acquire;
      for I in Client_Id loop
         Put_Line("**  **  **  Client " & Client_Id'Image(I) &  " "
                  & "State " & Client_State'Image(State(I))
                  & " Claim " & Integer'Image(Claim(I))
                  &" Full_Claim " & Integer'Image(Full_Claim(I))
                  & " Full_Allocation " & Integer'Image(Full_Allocation(I)));
      end loop;
      Put_Line("**  **  **  Stock value is " & Integer'Image(Stock));
      Display_Lock.Release;
   end Display_States;
   -----------------------------------
   -- simulating some action
   -- Doing_Some_Work was replaced by data creation and filling a global array
   procedure Doing_Some_Work(t : in Client_Id) is
   begin
      --delay(duration(Next_Hazard));
      case t is
         when 1 => delay(duration(0));
         when 2 => delay(Duration(3));
         when others => delay(Duration(6));
      end case;
   end Doing_Some_Work;
   -- Doing_Some_Long_Work was replaced by sorting the global array with a Bubble sort procedure
   -- followed by saving in a file the sorted array
   procedure Doing_Some_Long_Work(t : in Client_Id) is
   begin
      --delay (duration(10.0 * Next_Hazard));
      --delay(duration(10.0*Integer(t)));
      case t is
         when 1 => delay(duration(9));
         when 2 => delay(Duration(12));
         when others => delay(Duration(15));
      end case;
   end Doing_Some_Long_Work;
   -----------------------------------
   task type Client is
      entry Get_Id (Name : in Client_Id);
   end Client	;
   task body Client is
      My_Id : Client_Id := 0;   -- client unique name
      My_Claim : Integer := 0;   -- client current request value
      Job_Number : Integer := 0;
      ----------------------------  displayed messages
      procedure Message(Mess : in String) is
      begin
         Display_Lock.Acquire;
         Put_Line("Client" & Client_Id'Image(My_Id) &  "  " & Mess);
         Display_Lock.Release;
      end Message;
      procedure Asking(Y : in Integer) is
      begin
         Message("is requesting " & Integer'Image(Y) & " resource");
         State(My_Id) := Requesting;
         Claim(My_Id) := Y; Full_Claim(My_Id) := Full_Claim(My_Id) + Y;
      end Asking;
      procedure Utilizing(Y : in Integer) is
      begin
         Message("is using " & Integer'Image(Y) & " resource");
         State(My_Id) := Using;
         Claim(My_Id) := 0;
         Full_Allocation(My_Id) := Full_Allocation(My_Id) + Y ;
         Display_States; -- imprime les ?tats de tous les  processus
      end Utilizing;
      procedure Liberating(Y : in Integer) is
      begin
         Message("is releasing " & Integer'Image(Y) & " resource");
         State(My_Id) := Quiescent;
         Claim(My_Id) := 0;
         Full_Allocation(My_Id) := 0 ;
         Full_Claim(My_Id) := 0;
         Display_States; -- imprime les ?tats de tous les  processus
      end Liberating;
      ------------------------------------------------
   begin
      -- starts acquiring a unique name
      accept Get_Id (Name : in Client_Id) do
         My_Id := Name;
      end Get_Id;
      --
      loop
         Job_Number := Job_Number + 1;
         Message("is starting job number " & Integer'Image(Job_Number));
         -- the job requires additional resources four times
         for I in 1..4 loop
            Doing_Some_Work(My_Id);
            My_Claim :=(5 - I) mod 5;
            Asking(My_Claim);
            resource.Request(My_Claim, My_Id); -- first request
            -- client is allowed to acquire (in mutual exclusion) requested resources and use them
            Utilizing(My_Claim);
            --  Doing_Some_Work(My_Id);
         end loop;
         --  Doing_Some_Long_Work(My_Id);
         -- the job releases all its acquired resources
         resource.Release(Full_Allocation(My_Id) , My_Id);
         Liberating(Full_Allocation(My_Id) );
         --  Doing_Some_Work(My_Id);
         Doing_Some_Long_Work(My_Id);
         Message("has finished job number " & Integer'Image(Job_Number));
      end loop;
   end Client;
   type Client_Array is array(Client_Id) of Client;
   The_Clients : Client_Array;
begin
   for I in  Client_Id loop
      State(I) := Quiescent;
      Claim(I) := 0;
      Full_Claim(I) := 0;
      Full_Allocation(I) := 0;
   end loop;
   for I in  Client_Id loop
      The_Clients(I).Get_Id (I); -- allocates unique names to Client processes
   end loop;
end application211c3;
