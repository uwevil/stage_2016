

package P_Colour is
   type Colour is (Blue, Red, Yellow);
   function Complementary_Colour(C1, C2: Colour) return Colour;
end P_Colour;
