with P_Id_Chameneos; use P_Id_Chameneos;
with P_Colour; use P_Colour;
package Mall is
   function Cooperation(Xa: Id_Chameneos; Ca: Colour) return Colour;
end Mall;
