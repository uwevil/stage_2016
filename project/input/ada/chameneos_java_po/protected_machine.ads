generic
   type Result is private;
   type Internal_State is limited private;
   with procedure Reset (V: in Internal_State);
   with function Next_Value (V: Internal_State) return Result;

package Protected_Machine is
    function Next_Protected_Value return Result;
end Protected_Machine;

-- Usable for encapsulating a sequence generator 
-- (for example a random generator, or a unique name generator)
-- and allowing mutual exclusion when calling one of the operations
-- Initialization ( named  Reset)
-- Getting once and only once current value (named Next_Value)