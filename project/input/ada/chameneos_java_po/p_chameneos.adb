-------					THIS PROCEDURE IS THE MAIN PROGRAM OF THE CHAMENEOS GAME 
	
with Text_IO, P_Id_Chameneos, P_Colour, Mall, Generic_Lock ;
use Text_IO,  P_Id_Chameneos, P_Colour, Mall;

procedure  P_Chameneos is

   Nb_Requests: Integer renames P_Id_Chameneos.Nb_Requests;
   
   test : array(Id_Chameneos) of Id_Chameneos := (others => 0);

   --------------        Chameneos task specification  ------------------
   task type T_Chameneos is
      entry Start(Id: in Id_Chameneos);		-- initialization
      entry Call(Emitter : in Id_Chameneos; C_Emitter : Colour; 
                 Receptor : in Id_Chameneos; C_Receptor : out Colour);
      entry Finish(Emitter : in Id_Chameneos; C_Emitter : Colour; 
                   Receptor : in Id_Chameneos; C_Receptor : out Colour; 
                   WellDone : out Boolean);
   end T_Chameneos;

   ----------------------------------------------------------------------
   Chameneos : array(Id_Chameneos) of T_Chameneos;  -- set of mutating chameneos
   
   Last_Chameneos : Id_Chameneos ;   -- name of the last chameneos finishing 
   Rescued_Chameneos : Id_Chameneos ; -- used for correct termination

   package World_Lock is new Generic_Lock ;		-- P_Colour_World mutex
   
   ----------------------   task body Chameneos   ---------------------
   task body T_Chameneos is
   
      My_Id          : Id_Chameneos;  -- name of Chameneos
      My_Peer	     : Id_Chameneos;  -- name of current peer if any
      My_Colour      : Colour;
      My_Peer_Colour : Colour;
      Committed		 : Boolean;		  -- end of transaction commit 
      Stroke		 : Integer := 0;  -- current stroke number

   begin
	
      -----       initialization of Chameneos attributes    ----------
      accept Start(Id: in Id_Chameneos) do
         My_Id := Id; 
         My_Colour := P_Colour.Get_Colour;  
         P_Colour.Update_World(My_Id, My_Colour);
      end Start;

      ---------------- each loop cycle is a new peer to peer communication ----
      for I in 1..Nb_Requests loop
		 
         Stroke := I; -- chameneos current evolution 
		 		 
         My_Peer := Mall.Get_A_Peer(My_Id);
         exit when My_Peer = My_Id;  -- used for rescueing an incorrect termination

         test(My_Peer) := My_Id;
         
         if My_Id < My_Peer then 
            Chameneos(My_Peer).Call(My_Id, My_Colour, My_Peer, My_Peer_Colour);
         else
            accept Call(Emitter : in Id_Chameneos; C_Emitter : Colour; 
                        Receptor : in Id_Chameneos; C_Receptor : out Colour) do 
               C_Receptor := My_Colour; 
               My_Peer_Colour := C_Emitter;
            end Call;
         end if;
		 
         My_Colour := P_Colour.Complementary_Colour( My_Colour, My_Peer_Colour);
		 
         if My_Id < My_Peer then 
            World_Lock.Acquire;
            Chameneos(My_Peer).Finish(My_Id, My_Colour, 
                                      My_Peer, My_Peer_Colour, Committed);
            P_Colour.Update_World(My_Id, My_Colour);
            World_Lock.Release;
         else
            accept Finish(Emitter : in Id_Chameneos; C_Emitter : Colour;
                          Receptor : in Id_Chameneos; C_Receptor : out Colour; 
                          WellDone : out Boolean) do
               C_Receptor := My_Colour;
               WellDone := (C_Emitter = My_Colour);
            
               P_Colour.Update_World(My_Id, My_Colour);
            end Finish ;
         end if;
         
         pragma Assert(test(My_Id)=My_Peer, "erreur assert pragma");
      end loop;	
   end T_Chameneos;
   
begin
   for I in Id_Chameneos loop
      Chameneos(I).Start(Id_Chameneos(I));
   end loop; 
				
end P_Chameneos;
