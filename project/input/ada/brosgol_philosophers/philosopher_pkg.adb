--with Chopstick_Pkg;
--with Chopstick_Pkg_requeue_without_abort;
with Chopstick_Pkg_requeue_with_abort;

with Ada.Text_IO;
use Ada.Text_IO;

package body Philosopher_Pkg is
   task Terminator is
      entry Start_Shutdown; -- Called from user task
      entry Stop_Me; -- Called from philosophers
   end Terminator;
   procedure Shutdown is
   begin
      Terminator.Start_Shutdown;
   end Shutdown;
   task body Terminator is
   begin
      accept Start_Shutdown;
      loop
         select
            accept Stop_Me;
         or
            terminate;
         end select;
      end loop;
   end Terminator;
   procedure Eat( Id : in Phil_Range ) is
   begin
      Put_Line(Phil_Range'Image(Id) & " eating" );
      delay 2.0;
   end Eat;
   procedure Think( Id : in Phil_Range ) is
   begin
      Put_Line(Phil_Range'Image(Id) & " thinking" );
      delay 2.0;
   end Think;
   task body Philosopher is
      Id : Phil_Range;
      --  use Chopstick_Pkg;
      --use Chopstick_Pkg_requeue_without_abort;
      use Chopstick_Pkg_requeue_with_abort;

   begin
      accept Identify( Id : in Phil_Range ) do
         Philosopher.Id := Id;
      end Identify;
      select
         Terminator.Stop_Me;
         Put_Line( "Philosopher" & Phil_Range'Image(Id)
                   & " starting to shut down...");
      then abort
         loop
            Chopstick_Sentry.Get_Pair( Id );
            Eat ( Id ) ;
            Chopstick_Sentry.Release_Pair( Id );
            Think( Id );
         end loop;
      end select;
   end Philosopher;
end Philosopher_Pkg;
