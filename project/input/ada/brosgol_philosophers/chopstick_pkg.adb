package body Chopstick_Pkg is
   protected body Chopstick_Sentry is
      entry Get Pair( for J in Phil Range )
      when Available(J) and Available(J+1) is
      begin
         Available(J) := False;
         Available(J+1) := False;
      end Get_Pair;
      procedure Release_Pair( Id : in Phil_Range ) is
      begin
         Available(Id) := True;
         Available(Id+l) := True;
      end Release Pair;
   end Chopstick_Sentry;
end Chopstick_Pkg;
