
package body Protected_Machine is

   --====================================================================
   protected Internal_Machine is
      procedure Internal_Reset;
      function Next_Internal_Value return Result;
   private
      State : Internal_State;
   end Internal_Machine;

   protected body Internal_Machine is
      function Next_Internal_Value return Result is
      begin
         return Next_Value(State);
      end Next_Internal_Value;
      procedure Internal_Reset is
      begin
         Reset (State);
      end Internal_Reset;
   end Internal_Machine;

   --====================================================================
   function Next_Protected_Value return Result is
   begin
      return Internal_Machine.Next_Internal_Value;
   end Next_Protected_Value;

   --====================================================================
begin
   Internal_Machine.Internal_Reset;
end Protected_Machine;
