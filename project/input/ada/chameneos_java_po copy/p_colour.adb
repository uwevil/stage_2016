with Protected_Machine; with Ada.Numerics.Discrete_Random;
package body P_Colour is

   function Complementary_Colour(C1, C2: Colour) return Colour is
   begin
      if (C1 = C2) then
         return C1;
      else
         return Colour'Val(3 - Colour'Pos(C1) - Colour'Pos(C2));
      end if;
   end Complementary_Colour;
   
   Nb_Blue, Nb_Red : Integer := 0;
   
   package Colour_Random is new
     Ada.Numerics.Discrete_Random (Colour);

   package Protected_Colour_Random is new Protected_Machine
     ( Result			=> Colour,
       Internal_State   => Colour_Random.Generator,
       Reset            => Colour_Random.Reset,
       Next_Value		=> Colour_Random.Random);

   function Get_Colour return Colour is
		C : Colour; N1, N2 : Integer;
	begin
		loop
			C := Protected_Colour_Random.Next_Protected_Value;
			if C = Blue		then N1 := Nb_Blue + 1; N2 := Nb_Red;
			elsif C = Red   then N2 := Nb_Red + 1 ; N1 := Nb_Blue;
			end if;
			-- C is accepted if the set of colours allows permanent evolution
			-- more details in the READ_ME CHAMENEOS.html documentation
			exit when  Nb_Chameneos mod 3 /=  0 or (N1 - N2) mod 3 /= 0 ;
		end loop;
		if C = Blue		then Nb_Blue := Nb_Blue + 1; 
		elsif C = Red   then Nb_Red := Nb_Red + 1 ; 
		end if;
		return C;
	end Get_Colour;
	
	World : World_Array := (others => Yellow);
	
	procedure Update_World(X : in Id_Chameneos; C : in Colour) is
	begin World(X) := C ; end Update_World;
	
	function Display_World return World_Array is
	begin return World ; end Display_World;
	
	procedure Get_Situation (N_Blue, N_Red, N_Yellow : out Integer) is 
		N1, N2, N3 : Integer := 0;
	begin
		for I in Id_Chameneos loop
			if World(I) = Blue		then N1 := N1 + 1; 
			elsif World(I) = Red	then N2 := N2 + 1 ; 
									else N3 := N3 + 1; 
			end if;
		end loop;
		N_Blue := N1; N_Red := N2; N_Yellow := N3;
	end Get_Situation;
	
end P_Colour;
