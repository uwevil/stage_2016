
with P_Id_Chameneos; use P_Id_Chameneos;
package P_Colour is
   type Colour is (Blue, Red, Yellow);
   function Complementary_Colour(C1, C2: Colour) return Colour;
   function Get_Colour return Colour;
   type World_Array is array(Id_Chameneos) of Colour;
   procedure Update_World(X : in Id_Chameneos; C : in Colour);
   function Display_World return World_Array;
   procedure Get_Situation (N_Blue, N_Red, N_Yellow : out Integer);
end P_Colour;


-- C3 = Complementary_Colour(C1, C2)  ::  if C1 = C2 then C3 = C1
-- C3 = Complementary_Colour(C1, C2)  ::  if C1 /= C2 then C3 /= C1 and C3 /= C2

-- Get_Colour returns a colour such that the future evolution
--     of all the chameneos remains always possible
-- Get_Colour can be used only at simulation initialisation 
-- All subprograms are supposed to have a sequential execution 
-- use by concurrent tasks must be protected by a mutual exclusion lock 