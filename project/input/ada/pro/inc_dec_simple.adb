with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;

procedure inc_dec_simple is

   x : Integer := 0;

   task dec;
   task inc;

   task body inc is
      ax : Integer := 0;
   begin
      for i in 1..10_000 loop
         ax := x;
         ax := ax + 1;
         x := ax;
      end loop;
   end inc;

   task body dec is
      bx : Integer := 0;
   begin
      for i in 1..10_000 loop
         bx := x;
         bx := bx - 1;
         x := bx;
      end loop;
   end dec;

begin
   null;
   Put(x);
end inc_dec_simple;
