with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Calendar;
use Ada.Calendar;

with Ada.Numerics.Discrete_Random;

procedure inc_dec is

   subtype t_character is Character range 'A'..'z';
   type t_array is array(1..10) of t_character;

   package p_random is new Ada.Numerics.Discrete_Random(t_character);
   use p_random;

   protected type t_produit is
      entry inc(id : Integer; c : t_character);
      entry dec(id : Integer);
   private
      current_pile_size : Integer := 0;
      --pile : t_array;
   end t_produit;

   protected body t_produit is
      entry inc(id : Integer; c : t_character) when current_pile_size < 10 is
      begin
         current_pile_size := current_pile_size + 1;
        -- pile(current_pile_size) := c;
         Put_Line(Integer'Image(id) & " inc " & Integer'Image(current_pile_size));--Character'Image(c));
      end inc;

      entry dec(id : Integer) when current_pile_size > 0 is
      begin
      --   Put_Line(Integer'Image(id) & " dec " & Integer'Image(current_pile_size));-- Character'Image(pile(1)));
     --    for i in 1..(current_pile_size-1) loop
       --     pile(i) := pile(i+1);
     --    end loop;
         current_pile_size := current_pile_size - 1;
         Put_Line(Integer'Image(id) & " dec " & Integer'Image(current_pile_size));
      end dec;
   end t_produit;

   produit : t_produit;

   task type t_inc(id : Integer);

   task body t_inc is
      g : Generator;
   begin
      Reset(g);
      loop
         produit.inc(id, Random(g));
         delay 1*Duration(1);
      end loop;
   end t_inc;

   task type t_dec(id : Integer);

   task body t_dec is
   begin
      loop
         produit.dec(id);
         delay 1*Duration(1);
      end loop;
   end t_dec;

   inc1 : t_inc(1);

   dec1 : t_dec(11);

begin
   null;
end inc_dec;
