package Protected_Object is
   -------Variables depend on protected object-------
   x : Integer := 0;
   ax : Integer := 0;
   bx : Integer := 0;

   id1 : Integer;

   id2 : Integer;

   --------------------------------------------------


   nbTask : Constant Natural := 2;
   nbEntry : Constant Natural := 2;

   type tQueue is array(1..nbTask) of Natural;
   subtype tPositionQueue is Natural range 1..nbTask;
   type tBarrier is array(1..nbEntry) of Boolean;

   tabStatus : tQueue := (others => 1);

   tabExternalQueue : tQueue := (others => 0);
   currentPositionExternalQueue : tPositionQueue := tPositionQueue'First;

   type tEntryQueue is array(1..nbEntry) of tQueue;
   subtype tPositionEntry is Natural range 1..nbEntry;

   tabEntryQueue : tEntryQueue := (others => (others => 0));

   type tCurrentPositionEntryQueue is array(1..nbEntry) of tPositionQueue;
   tabCurrentPositionEntryQueue : tCurrentPositionEntryQueue := (others => tPositionQueue'First);

   tabBarrier : tBarrier := (others => False);

   isLock : Boolean := False;

   pragma Atomic(currentPositionExternalQueue);
   pragma Atomic(tabCurrentPositionEntryQueue);

   function is_lock return Boolean;
   procedure add_external_queue(taskId : in Natural);
   procedure remove_external_queue;
   procedure wait_external_queue(taskId : in Natural);

   procedure evaluate_barrier(entryId : in Natural);
   function is_open(entryId : in Natural) return Boolean;
   procedure add_entry_queue(taskId : in Natural; entryId : in Natural);
   procedure remove_entry_queue(entryId : in Natural);
   procedure wait_entry_queue(taskId :in Natural; entryId : in Natural);

   procedure run_entry_body(entryId : in Natural);
   procedure reevaluate_barrier;

   currentEligible : Natural := 0;

   function current_eligible return Natural;
   procedure check_and_run_all_entry;

   procedure entry_call(taskId : in Natural; entryId : in Natural);



end Protected_Object;
