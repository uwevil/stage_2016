with Ada.Text_IO;
use Ada.Text_IO;

with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;

procedure test is
   -------------------------------DECLARATION-------------------------
   subtype modulo is Natural range 1..10;
   m : modulo := 3;

   phil_count : constant := 5;
   type phil_range is mod phil_count;

   type Cardinal is new Integer range 0..2;
   score :  Cardinal := 0;

   s : String := "sazere";
   -------------------------------MAIN-------------------------
begin

   for i in 1..(10 - 1) loop
      m := (m) mod 10 + 1;
      -- Put(Natural'Image(m mod 10 + i));
      -- Put(" ");
      Put(Natural'Image(m));

      -- Put(Natural'Image(m mod 10 + i));
      -- Put(" ");
      New_Line;
      for j in phil_range loop
         Put(phil_range'Image(j));
      end loop;

      New_Line;

      pragma Assert(m=1, "assert lolll");
      -- score /= 2;
      -- Put(Cardinal'Image(score));

      -- while Score /= 2 loop
      --  Put(Cardinal'Image(score));
      -- New_Line;
      -- end loop;
   end loop;

   Put(Character'Pos('a'));
   New_Line;
   Put(s'First);
   New_Line;

end test;
