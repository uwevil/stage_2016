Répertoire:
	.       : contient les fichiers Makefile, README.
	make/   : contient des fichiers pour lancer les tests appelés par le Makefile dans .
	input/  : contient les fichiers sources de langage Ada, C, Java
	output/ : contient des fichiers sorties de la vérification
	script/ : contient les fichiers scripts Python pour parser et générer les fichiers et les images de résultat.
	tmp/    : contient des fichiers temporaires.

Après installer des outils précisés dans README.md dans la racine, on peut normalement lancer des tests par Makefile facilement.

Afin d'ajouter un nouveau programme Ada pour tester avec notre approche, les étapes à réaliser sont suivantes:
	- créer un répertoire pour stocker le programme Ada dans le répertoire input/ada/ (par ex. input/ada/Test/)
	- tester et lancer le programme Ada par gnatmake.
	- créer un répertoire qui a le même nom avec celui dans le répertoire input/ada/ dans le répertoire input/c/c_source/ et créer un fichier .c.
	- pour traduire de Ada vers C :
		# comprendre le papier 'Lazy-sequentialization'
		# créer le corps en C correspondant avec la version en Ada
		# copy l'objet protégé dans le fichier input/c/c_source/include/protected_object.c dans le fichier .c
		# s'il contient plusieurs objets protégés, ajouter un numéro unique pour chaque objet protégé dans les variables, les fonctions, (tous) qu'on a copié depuis include/
		# voir les autres exemples dans le répertoire courant pour continuer
	- créer le répertoire correspondant dans input/c/c_annotation/ pour ajouter des annotations, ajouter des annotations (voir les autres exemples)
	- créer un fichier .c dans input/c/c_tmp/ en copiant le fichier crée dans input/c/c_annotation/répertoire_vient_de_crée/
	- changer dans Makefile qui se trouver dans project/ la ligne 'parse' sous forme : python script/parser2.py fichierAnnotationIn fichierOut (ou python3.5)
	- run 'make parse'
	- copier le fichier crée dans le répertoire /input/c/c_seq/ (c_seq_complete si vous utilisez la procédure complète).
	- run test en utilisant make/GenericMake sous forme :
		(on se place dans le répertoire project/)
		make -f make/GenericMake -i FOLDERIN=input/c/c_seq/ FOLDEROUT=output/benchmark/Test FILEIN=filename.c COMPLETE=no TIMEOUT=700 MIN=3 MAX=11
		ou modifier la ligne après la ligne generic dans le fichier Makefile et lancer 'make generic'

		pour la procédure complète, changer juste FOLDERIN=input/c/c_seq_complete/ et COMPLETE=yes; changer le timeout si vous voulez








