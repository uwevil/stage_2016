CBMC version 5.4 64-bit x86_64 macos
Parsing input/c/c_seq/p_ko.c
Converting
Type-checking p_ko
file input/c/c_seq/p_ko.c line 119 function main: function `nondet_uint' is not declared
Generating GOTO Program
Adding CPROVER library (x86_64)
Function Pointer Removal
Partial Inlining
Generic Property Instrumentation
Starting Bounded Model Checking
Unwinding loop main.0 iteration 1 (11 max) file input/c/c_seq/p_ko.c line 116 function main thread 0
Unwinding loop main.0 iteration 2 (11 max) file input/c/c_seq/p_ko.c line 116 function main thread 0
Unwinding loop main.0 iteration 3 (11 max) file input/c/c_seq/p_ko.c line 116 function main thread 0
Unwinding loop main.0 iteration 4 (11 max) file input/c/c_seq/p_ko.c line 116 function main thread 0
Unwinding loop main.0 iteration 5 (11 max) file input/c/c_seq/p_ko.c line 116 function main thread 0
Unwinding loop main.0 iteration 6 (11 max) file input/c/c_seq/p_ko.c line 116 function main thread 0
Unwinding loop main.0 iteration 7 (11 max) file input/c/c_seq/p_ko.c line 116 function main thread 0
Unwinding loop main.0 iteration 8 (11 max) file input/c/c_seq/p_ko.c line 116 function main thread 0
Unwinding loop main.0 iteration 9 (11 max) file input/c/c_seq/p_ko.c line 116 function main thread 0
Unwinding loop main.0 iteration 10 (11 max) file input/c/c_seq/p_ko.c line 116 function main thread 0
Not unwinding loop main.0 iteration 11 (11 max) file input/c/c_seq/p_ko.c line 116 function main thread 0
size of program expression: 1990 steps
simple slicing removed 3 assignments
Generated 23 VCC(s), 23 remaining after simplification
Passing problem to SMT2 QF_AUFBV using Yices
converting SSA
Running SMT2 QF_AUFBV using Yices
Runtime decision procedure: 0.584s
Building error trace

Counterexample:

State 35 file input/c/c_seq/p_ko.c line 99 function init thread 0
----------------------------------------------------
  pc[(long int)0]=(assignment removed)

State 36 file input/c/c_seq/p_ko.c line 100 function init thread 0
----------------------------------------------------
  size[(long int)0]=(assignment removed)

State 37 file input/c/c_seq/p_ko.c line 102 function init thread 0
----------------------------------------------------
  pc[(long int)1]=(assignment removed)

State 38 file input/c/c_seq/p_ko.c line 103 function init thread 0
----------------------------------------------------
  pc[(long int)2]=(assignment removed)

State 39 file input/c/c_seq/p_ko.c line 104 function init thread 0
----------------------------------------------------
  size[(long int)1]=(assignment removed)

State 40 file input/c/c_seq/p_ko.c line 105 function init thread 0
----------------------------------------------------
  size[(long int)2]=(assignment removed)

State 41 file input/c/c_seq/p_ko.c line 107 function init thread 0
----------------------------------------------------
  t1=0 (00000000000000000000000000000000)

State 42 file input/c/c_seq/p_ko.c line 107 function init thread 0
----------------------------------------------------
  t2=0 (00000000000000000000000000000000)

State 43 file input/c/c_seq/p_ko.c line 107 function init thread 0
----------------------------------------------------
  t3=0 (00000000000000000000000000000000)

State 44 file input/c/c_seq/p_ko.c line 107 function init thread 0
----------------------------------------------------
  t4=0 (00000000000000000000000000000000)

State 45 file input/c/c_seq/p_ko.c line 107 function init thread 0
----------------------------------------------------
  t5=0 (00000000000000000000000000000000)

State 46 file input/c/c_seq/p_ko.c line 107 function init thread 0
----------------------------------------------------
  t6=0 (00000000000000000000000000000000)

State 49 file input/c/c_seq/p_ko.c line 109 function init thread 0
----------------------------------------------------
  t=0 (00000000000000000000000000000000)

State 50 file input/c/c_seq/p_ko.c line 109 function init thread 0
----------------------------------------------------
  id=1 (00000000000000000000000000000001)

State 51 file input/c/c_seq/p_ko.c line 20 function create thread 0
----------------------------------------------------
  active[(long int)id!0@1]=(assignment removed)

State 52 file input/c/c_seq/p_ko.c line 21 function create thread 0
----------------------------------------------------
  t=1 (00000000000000000000000000000001)

State 56 file input/c/c_seq/p_ko.c line 110 function init thread 0
----------------------------------------------------
  t=0 (00000000000000000000000000000000)

State 57 file input/c/c_seq/p_ko.c line 110 function init thread 0
----------------------------------------------------
  id=2 (00000000000000000000000000000010)

State 58 file input/c/c_seq/p_ko.c line 20 function create thread 0
----------------------------------------------------
  active[(long int)id!0@2]=(assignment removed)

State 59 file input/c/c_seq/p_ko.c line 21 function create thread 0
----------------------------------------------------
  t=2 (00000000000000000000000000000010)

State 62 file input/c/c_seq/p_ko.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 64 file input/c/c_seq/p_ko.c line 119 function main thread 0
----------------------------------------------------
  cs=7 (00000000000000000000000000000111)

State 75 file input/c/c_seq/p_ko.c line 38 function t_one thread 0
----------------------------------------------------
  other=2 (00000000000000000000000000000010)

State 77 file input/c/c_seq/p_ko.c line 40 function t_one thread 0
----------------------------------------------------
  flag[(long int)taskId]=(assignment removed)

State 79 file input/c/c_seq/p_ko.c line 43 function t_one thread 0
----------------------------------------------------
  turn=2 (00000000000000000000000000000010)

State 83 file input/c/c_seq/p_ko.c line 49 function t_one thread 0
----------------------------------------------------
  inSC=1 (00000000000000000000000000000001)

State 90 file input/c/c_seq/p_ko.c line 122 function main thread 0
----------------------------------------------------
  pc[(long int)ct]=(assignment removed)

State 91 file input/c/c_seq/p_ko.c line 123 function main thread 0
----------------------------------------------------
  active[(long int)ct]=(assignment removed)

State 92 file input/c/c_seq/p_ko.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 94 file input/c/c_seq/p_ko.c line 128 function main thread 0
----------------------------------------------------
  cs=9 (00000000000000000000000000001001)

State 105 file input/c/c_seq/p_ko.c line 71 function t_two thread 0
----------------------------------------------------
  other=1 (00000000000000000000000000000001)

State 107 file input/c/c_seq/p_ko.c line 73 function t_two thread 0
----------------------------------------------------
  flag[(long int)taskId]=(assignment removed)

State 109 file input/c/c_seq/p_ko.c line 76 function t_two thread 0
----------------------------------------------------
  turn=1 (00000000000000000000000000000001)

State 113 file input/c/c_seq/p_ko.c line 82 function t_two thread 0
----------------------------------------------------
  inSC=2 (00000000000000000000000000000010)

State 115 file input/c/c_seq/p_ko.c line 85 function t_two thread 0
----------------------------------------------------
  bx=0 (00000000000000000000000000000000)

State 117 file input/c/c_seq/p_ko.c line 87 function t_two thread 0
----------------------------------------------------
  bx=-1 (11111111111111111111111111111111)

State 122 file input/c/c_seq/p_ko.c line 131 function main thread 0
----------------------------------------------------
  pc[(long int)ct]=(assignment removed)

State 123 file input/c/c_seq/p_ko.c line 132 function main thread 0
----------------------------------------------------
  active[(long int)ct]=(assignment removed)

Violated property:
  file input/c/c_seq/p_ko.c line 135 function main
  OK inSC < 2
  inSC < 2

VERIFICATION FAILED

real	0m0.861s
user	0m0.771s
sys	0m0.079s
