CBMC version 5.4 64-bit x86_64 macos
Parsing input/c/c_seq/p_ko.c
Converting
Type-checking p_ko
file input/c/c_seq/p_ko.c line 119 function main: function `nondet_uint' is not declared
Generating GOTO Program
Adding CPROVER library (x86_64)
Function Pointer Removal
Partial Inlining
Generic Property Instrumentation
Starting Bounded Model Checking
Unwinding loop main.0 iteration 1 (9 max) file input/c/c_seq/p_ko.c line 116 function main thread 0
Unwinding loop main.0 iteration 2 (9 max) file input/c/c_seq/p_ko.c line 116 function main thread 0
Unwinding loop main.0 iteration 3 (9 max) file input/c/c_seq/p_ko.c line 116 function main thread 0
Unwinding loop main.0 iteration 4 (9 max) file input/c/c_seq/p_ko.c line 116 function main thread 0
Unwinding loop main.0 iteration 5 (9 max) file input/c/c_seq/p_ko.c line 116 function main thread 0
Unwinding loop main.0 iteration 6 (9 max) file input/c/c_seq/p_ko.c line 116 function main thread 0
Unwinding loop main.0 iteration 7 (9 max) file input/c/c_seq/p_ko.c line 116 function main thread 0
Unwinding loop main.0 iteration 8 (9 max) file input/c/c_seq/p_ko.c line 116 function main thread 0
Not unwinding loop main.0 iteration 9 (9 max) file input/c/c_seq/p_ko.c line 116 function main thread 0
size of program expression: 1644 steps
simple slicing removed 3 assignments
Generated 19 VCC(s), 19 remaining after simplification
Passing problem to propositional reduction
converting SSA
Running propositional reduction
Post-processing
Solving with MiniSAT 2.2.1 with simplifier
25734 variables, 97661 clauses
SAT checker: instance is SATISFIABLE
Runtime decision procedure: 0.182s
Building error trace

Counterexample:

State 35 file input/c/c_seq/p_ko.c line 99 function init thread 0
----------------------------------------------------
  pc[0]=0 (00000000000000000000000000000000)

State 36 file input/c/c_seq/p_ko.c line 100 function init thread 0
----------------------------------------------------
  size[0]=0 (00000000000000000000000000000000)

State 37 file input/c/c_seq/p_ko.c line 102 function init thread 0
----------------------------------------------------
  pc[1]=0 (00000000000000000000000000000000)

State 38 file input/c/c_seq/p_ko.c line 103 function init thread 0
----------------------------------------------------
  pc[2]=0 (00000000000000000000000000000000)

State 39 file input/c/c_seq/p_ko.c line 104 function init thread 0
----------------------------------------------------
  size[1]=11 (00000000000000000000000000001011)

State 40 file input/c/c_seq/p_ko.c line 105 function init thread 0
----------------------------------------------------
  size[2]=11 (00000000000000000000000000001011)

State 41 file input/c/c_seq/p_ko.c line 107 function init thread 0
----------------------------------------------------
  t1=0 (00000000000000000000000000000000)

State 42 file input/c/c_seq/p_ko.c line 107 function init thread 0
----------------------------------------------------
  t2=0 (00000000000000000000000000000000)

State 43 file input/c/c_seq/p_ko.c line 107 function init thread 0
----------------------------------------------------
  t3=0 (00000000000000000000000000000000)

State 44 file input/c/c_seq/p_ko.c line 107 function init thread 0
----------------------------------------------------
  t4=0 (00000000000000000000000000000000)

State 45 file input/c/c_seq/p_ko.c line 107 function init thread 0
----------------------------------------------------
  t5=0 (00000000000000000000000000000000)

State 46 file input/c/c_seq/p_ko.c line 107 function init thread 0
----------------------------------------------------
  t6=0 (00000000000000000000000000000000)

State 49 file input/c/c_seq/p_ko.c line 109 function init thread 0
----------------------------------------------------
  t=0 (00000000000000000000000000000000)

State 50 file input/c/c_seq/p_ko.c line 109 function init thread 0
----------------------------------------------------
  id=1 (00000000000000000000000000000001)

State 51 file input/c/c_seq/p_ko.c line 20 function create thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 52 file input/c/c_seq/p_ko.c line 21 function create thread 0
----------------------------------------------------
  t=1 (00000000000000000000000000000001)

State 56 file input/c/c_seq/p_ko.c line 110 function init thread 0
----------------------------------------------------
  t=0 (00000000000000000000000000000000)

State 57 file input/c/c_seq/p_ko.c line 110 function init thread 0
----------------------------------------------------
  id=2 (00000000000000000000000000000010)

State 58 file input/c/c_seq/p_ko.c line 20 function create thread 0
----------------------------------------------------
  active[2]=1 (00000000000000000000000000000001)

State 59 file input/c/c_seq/p_ko.c line 21 function create thread 0
----------------------------------------------------
  t=2 (00000000000000000000000000000010)

State 62 file input/c/c_seq/p_ko.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 64 file input/c/c_seq/p_ko.c line 119 function main thread 0
----------------------------------------------------
  cs=3 (00000000000000000000000000000011)

State 75 file input/c/c_seq/p_ko.c line 38 function t_one thread 0
----------------------------------------------------
  other=2 (00000000000000000000000000000010)

State 86 file input/c/c_seq/p_ko.c line 122 function main thread 0
----------------------------------------------------
  pc[1]=3 (00000000000000000000000000000011)

State 87 file input/c/c_seq/p_ko.c line 123 function main thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 88 file input/c/c_seq/p_ko.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 90 file input/c/c_seq/p_ko.c line 128 function main thread 0
----------------------------------------------------
  cs=6 (00000000000000000000000000000110)

State 101 file input/c/c_seq/p_ko.c line 71 function t_two thread 0
----------------------------------------------------
  other=1 (00000000000000000000000000000001)

State 103 file input/c/c_seq/p_ko.c line 73 function t_two thread 0
----------------------------------------------------
  flag[2]=1 (00000000000000000000000000000001)

State 105 file input/c/c_seq/p_ko.c line 76 function t_two thread 0
----------------------------------------------------
  turn=1 (00000000000000000000000000000001)

State 115 file input/c/c_seq/p_ko.c line 131 function main thread 0
----------------------------------------------------
  pc[2]=6 (00000000000000000000000000000110)

State 116 file input/c/c_seq/p_ko.c line 132 function main thread 0
----------------------------------------------------
  active[2]=1 (00000000000000000000000000000001)

State 120 file input/c/c_seq/p_ko.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 122 file input/c/c_seq/p_ko.c line 119 function main thread 0
----------------------------------------------------
  cs=10 (00000000000000000000000000001010)

State 134 file input/c/c_seq/p_ko.c line 40 function t_one thread 0
----------------------------------------------------
  flag[1]=1 (00000000000000000000000000000001)

State 136 file input/c/c_seq/p_ko.c line 43 function t_one thread 0
----------------------------------------------------
  turn=2 (00000000000000000000000000000010)

State 140 file input/c/c_seq/p_ko.c line 49 function t_one thread 0
----------------------------------------------------
  inSC=1 (00000000000000000000000000000001)

State 142 file input/c/c_seq/p_ko.c line 52 function t_one thread 0
----------------------------------------------------
  ax=0 (00000000000000000000000000000000)

State 144 file input/c/c_seq/p_ko.c line 54 function t_one thread 0
----------------------------------------------------
  ax=1 (00000000000000000000000000000001)

State 146 file input/c/c_seq/p_ko.c line 56 function t_one thread 0
----------------------------------------------------
  x=1 (00000000000000000000000000000001)

State 150 file input/c/c_seq/p_ko.c line 122 function main thread 0
----------------------------------------------------
  pc[1]=10 (00000000000000000000000000001010)

State 151 file input/c/c_seq/p_ko.c line 123 function main thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 152 file input/c/c_seq/p_ko.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 154 file input/c/c_seq/p_ko.c line 128 function main thread 0
----------------------------------------------------
  cs=11 (00000000000000000000000000001011)

State 169 file input/c/c_seq/p_ko.c line 82 function t_two thread 0
----------------------------------------------------
  inSC=2 (00000000000000000000000000000010)

State 171 file input/c/c_seq/p_ko.c line 85 function t_two thread 0
----------------------------------------------------
  bx=1 (00000000000000000000000000000001)

State 173 file input/c/c_seq/p_ko.c line 87 function t_two thread 0
----------------------------------------------------
  bx=0 (00000000000000000000000000000000)

State 175 file input/c/c_seq/p_ko.c line 89 function t_two thread 0
----------------------------------------------------
  x=0 (00000000000000000000000000000000)

State 177 file input/c/c_seq/p_ko.c line 92 function t_two thread 0
----------------------------------------------------
  inSC=1 (00000000000000000000000000000001)

State 178 file input/c/c_seq/p_ko.c line 93 function t_two thread 0
----------------------------------------------------
  flag[2]=0 (00000000000000000000000000000000)

State 181 file input/c/c_seq/p_ko.c line 131 function main thread 0
----------------------------------------------------
  pc[2]=11 (00000000000000000000000000001011)

State 182 file input/c/c_seq/p_ko.c line 132 function main thread 0
----------------------------------------------------
  active[2]=0 (00000000000000000000000000000000)

State 186 file input/c/c_seq/p_ko.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 188 file input/c/c_seq/p_ko.c line 119 function main thread 0
----------------------------------------------------
  cs=10 (00000000000000000000000000001010)

State 209 file input/c/c_seq/p_ko.c line 122 function main thread 0
----------------------------------------------------
  pc[1]=10 (00000000000000000000000000001010)

State 210 file input/c/c_seq/p_ko.c line 123 function main thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 211 file input/c/c_seq/p_ko.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 216 file input/c/c_seq/p_ko.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 218 file input/c/c_seq/p_ko.c line 119 function main thread 0
----------------------------------------------------
  cs=10 (00000000000000000000000000001010)

State 239 file input/c/c_seq/p_ko.c line 122 function main thread 0
----------------------------------------------------
  pc[1]=10 (00000000000000000000000000001010)

State 240 file input/c/c_seq/p_ko.c line 123 function main thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 241 file input/c/c_seq/p_ko.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 246 file input/c/c_seq/p_ko.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 248 file input/c/c_seq/p_ko.c line 119 function main thread 0
----------------------------------------------------
  cs=10 (00000000000000000000000000001010)

State 269 file input/c/c_seq/p_ko.c line 122 function main thread 0
----------------------------------------------------
  pc[1]=10 (00000000000000000000000000001010)

State 270 file input/c/c_seq/p_ko.c line 123 function main thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 271 file input/c/c_seq/p_ko.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 276 file input/c/c_seq/p_ko.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 278 file input/c/c_seq/p_ko.c line 119 function main thread 0
----------------------------------------------------
  cs=10 (00000000000000000000000000001010)

State 299 file input/c/c_seq/p_ko.c line 122 function main thread 0
----------------------------------------------------
  pc[1]=10 (00000000000000000000000000001010)

State 300 file input/c/c_seq/p_ko.c line 123 function main thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 301 file input/c/c_seq/p_ko.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 306 file input/c/c_seq/p_ko.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 308 file input/c/c_seq/p_ko.c line 119 function main thread 0
----------------------------------------------------
  cs=11 (00000000000000000000000000001011)

State 327 file input/c/c_seq/p_ko.c line 59 function t_one thread 0
----------------------------------------------------
  inSC=0 (00000000000000000000000000000000)

State 328 file input/c/c_seq/p_ko.c line 60 function t_one thread 0
----------------------------------------------------
  flag[1]=0 (00000000000000000000000000000000)

State 331 file input/c/c_seq/p_ko.c line 122 function main thread 0
----------------------------------------------------
  pc[1]=11 (00000000000000000000000000001011)

State 332 file input/c/c_seq/p_ko.c line 123 function main thread 0
----------------------------------------------------
  active[1]=0 (00000000000000000000000000000000)

State 333 file input/c/c_seq/p_ko.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

Violated property:
  file input/c/c_seq/p_ko.c line 138 function main
  OK
  0 != 0

VERIFICATION FAILED

real	0m0.462s
user	0m0.420s
sys	0m0.035s
