CBMC version 5.4 64-bit x86_64 macos
Parsing input/c/c_seq_complete/act_java.c
Converting
Type-checking act_java
file input/c/c_seq_complete/act_java.c line 584 function main: function `nondet_uint' is not declared
Generating GOTO Program
Adding CPROVER library (x86_64)
Function Pointer Removal
Partial Inlining
Generic Property Instrumentation
Starting Bounded Model Checking
Unwinding loop main.0 iteration 1 (7 max) file input/c/c_seq_complete/act_java.c line 581 function main thread 0
Unwinding loop main.0 iteration 2 (7 max) file input/c/c_seq_complete/act_java.c line 581 function main thread 0
Unwinding loop main.0 iteration 3 (7 max) file input/c/c_seq_complete/act_java.c line 581 function main thread 0
Unwinding loop main.0 iteration 4 (7 max) file input/c/c_seq_complete/act_java.c line 581 function main thread 0
Unwinding loop main.0 iteration 5 (7 max) file input/c/c_seq_complete/act_java.c line 581 function main thread 0
Unwinding loop main.0 iteration 6 (7 max) file input/c/c_seq_complete/act_java.c line 581 function main thread 0
Not unwinding loop main.0 iteration 7 (7 max) file input/c/c_seq_complete/act_java.c line 581 function main thread 0
size of program expression: 6681 steps
simple slicing removed 60 assignments
Generated 167 VCC(s), 167 remaining after simplification
Passing problem to propositional reduction
converting SSA
Running propositional reduction
Post-processing
Solving with MiniSAT 2.2.1 with simplifier
208219 variables, 860895 clauses
SAT checker: instance is UNSATISFIABLE
Runtime decision procedure: 8272.36s
VERIFICATION SUCCESSFUL

real	137m55.260s
user	137m36.511s
sys	0m16.882s
