CBMC version 5.4 64-bit x86_64 macos
Parsing input/c/c_seq/p.c
Converting
Type-checking p
file input/c/c_seq/p.c line 119 function main: function `nondet_uint' is not declared
Generating GOTO Program
Adding CPROVER library (x86_64)
Function Pointer Removal
Partial Inlining
Generic Property Instrumentation
Starting Bounded Model Checking
Unwinding loop main.0 iteration 1 (10 max) file input/c/c_seq/p.c line 116 function main thread 0
Unwinding loop main.0 iteration 2 (10 max) file input/c/c_seq/p.c line 116 function main thread 0
Unwinding loop main.0 iteration 3 (10 max) file input/c/c_seq/p.c line 116 function main thread 0
Unwinding loop main.0 iteration 4 (10 max) file input/c/c_seq/p.c line 116 function main thread 0
Unwinding loop main.0 iteration 5 (10 max) file input/c/c_seq/p.c line 116 function main thread 0
Unwinding loop main.0 iteration 6 (10 max) file input/c/c_seq/p.c line 116 function main thread 0
Unwinding loop main.0 iteration 7 (10 max) file input/c/c_seq/p.c line 116 function main thread 0
Unwinding loop main.0 iteration 8 (10 max) file input/c/c_seq/p.c line 116 function main thread 0
Unwinding loop main.0 iteration 9 (10 max) file input/c/c_seq/p.c line 116 function main thread 0
Not unwinding loop main.0 iteration 10 (10 max) file input/c/c_seq/p.c line 116 function main thread 0
size of program expression: 1817 steps
simple slicing removed 3 assignments
Generated 21 VCC(s), 21 remaining after simplification
Passing problem to SMT2 QF_AUFBV using Yices
converting SSA
Running SMT2 QF_AUFBV using Yices
Runtime decision procedure: 0.456s
Building error trace

Counterexample:

State 35 file input/c/c_seq/p.c line 99 function init thread 0
----------------------------------------------------
  pc[(long int)0]=(assignment removed)

State 36 file input/c/c_seq/p.c line 100 function init thread 0
----------------------------------------------------
  size[(long int)0]=(assignment removed)

State 37 file input/c/c_seq/p.c line 102 function init thread 0
----------------------------------------------------
  pc[(long int)1]=(assignment removed)

State 38 file input/c/c_seq/p.c line 103 function init thread 0
----------------------------------------------------
  pc[(long int)2]=(assignment removed)

State 39 file input/c/c_seq/p.c line 104 function init thread 0
----------------------------------------------------
  size[(long int)1]=(assignment removed)

State 40 file input/c/c_seq/p.c line 105 function init thread 0
----------------------------------------------------
  size[(long int)2]=(assignment removed)

State 41 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t1=0 (00000000000000000000000000000000)

State 42 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t2=0 (00000000000000000000000000000000)

State 43 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t3=0 (00000000000000000000000000000000)

State 44 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t4=0 (00000000000000000000000000000000)

State 45 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t5=0 (00000000000000000000000000000000)

State 46 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t6=0 (00000000000000000000000000000000)

State 49 file input/c/c_seq/p.c line 109 function init thread 0
----------------------------------------------------
  t=0 (00000000000000000000000000000000)

State 50 file input/c/c_seq/p.c line 109 function init thread 0
----------------------------------------------------
  id=1 (00000000000000000000000000000001)

State 51 file input/c/c_seq/p.c line 20 function create thread 0
----------------------------------------------------
  active[(long int)id!0@1]=(assignment removed)

State 52 file input/c/c_seq/p.c line 21 function create thread 0
----------------------------------------------------
  t=1 (00000000000000000000000000000001)

State 56 file input/c/c_seq/p.c line 110 function init thread 0
----------------------------------------------------
  t=0 (00000000000000000000000000000000)

State 57 file input/c/c_seq/p.c line 110 function init thread 0
----------------------------------------------------
  id=2 (00000000000000000000000000000010)

State 58 file input/c/c_seq/p.c line 20 function create thread 0
----------------------------------------------------
  active[(long int)id!0@2]=(assignment removed)

State 59 file input/c/c_seq/p.c line 21 function create thread 0
----------------------------------------------------
  t=2 (00000000000000000000000000000010)

State 62 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 64 file input/c/c_seq/p.c line 119 function main thread 0
----------------------------------------------------
  cs=0 (00000000000000000000000000000000)

State 85 file input/c/c_seq/p.c line 122 function main thread 0
----------------------------------------------------
  pc[(long int)ct]=(assignment removed)

State 86 file input/c/c_seq/p.c line 123 function main thread 0
----------------------------------------------------
  active[(long int)ct]=(assignment removed)

State 87 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 89 file input/c/c_seq/p.c line 128 function main thread 0
----------------------------------------------------
  cs=0 (00000000000000000000000000000000)

State 110 file input/c/c_seq/p.c line 131 function main thread 0
----------------------------------------------------
  pc[(long int)ct]=(assignment removed)

State 111 file input/c/c_seq/p.c line 132 function main thread 0
----------------------------------------------------
  active[(long int)ct]=(assignment removed)

State 115 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 117 file input/c/c_seq/p.c line 119 function main thread 0
----------------------------------------------------
  cs=4 (00000000000000000000000000000100)

State 128 file input/c/c_seq/p.c line 38 function t_one thread 0
----------------------------------------------------
  other=2 (00000000000000000000000000000010)

State 130 file input/c/c_seq/p.c line 40 function t_one thread 0
----------------------------------------------------
  flag[(long int)taskId]=(assignment removed)

State 140 file input/c/c_seq/p.c line 122 function main thread 0
----------------------------------------------------
  pc[(long int)ct]=(assignment removed)

State 141 file input/c/c_seq/p.c line 123 function main thread 0
----------------------------------------------------
  active[(long int)ct]=(assignment removed)

State 142 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 144 file input/c/c_seq/p.c line 128 function main thread 0
----------------------------------------------------
  cs=3 (00000000000000000000000000000011)

State 155 file input/c/c_seq/p.c line 71 function t_two thread 0
----------------------------------------------------
  other=1 (00000000000000000000000000000001)

State 166 file input/c/c_seq/p.c line 131 function main thread 0
----------------------------------------------------
  pc[(long int)ct]=(assignment removed)

State 167 file input/c/c_seq/p.c line 132 function main thread 0
----------------------------------------------------
  active[(long int)ct]=(assignment removed)

State 171 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 173 file input/c/c_seq/p.c line 119 function main thread 0
----------------------------------------------------
  cs=6 (00000000000000000000000000000110)

State 186 file input/c/c_seq/p.c line 43 function t_one thread 0
----------------------------------------------------
  turn=2 (00000000000000000000000000000010)

State 196 file input/c/c_seq/p.c line 122 function main thread 0
----------------------------------------------------
  pc[(long int)ct]=(assignment removed)

State 197 file input/c/c_seq/p.c line 123 function main thread 0
----------------------------------------------------
  active[(long int)ct]=(assignment removed)

State 198 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 200 file input/c/c_seq/p.c line 128 function main thread 0
----------------------------------------------------
  cs=3 (00000000000000000000000000000011)

State 221 file input/c/c_seq/p.c line 131 function main thread 0
----------------------------------------------------
  pc[(long int)ct]=(assignment removed)

State 222 file input/c/c_seq/p.c line 132 function main thread 0
----------------------------------------------------
  active[(long int)ct]=(assignment removed)

State 226 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 228 file input/c/c_seq/p.c line 119 function main thread 0
----------------------------------------------------
  cs=10 (00000000000000000000000000001010)

State 243 file input/c/c_seq/p.c line 49 function t_one thread 0
----------------------------------------------------
  inSC=1 (00000000000000000000000000000001)

State 245 file input/c/c_seq/p.c line 52 function t_one thread 0
----------------------------------------------------
  ax=0 (00000000000000000000000000000000)

State 247 file input/c/c_seq/p.c line 54 function t_one thread 0
----------------------------------------------------
  ax=1 (00000000000000000000000000000001)

State 249 file input/c/c_seq/p.c line 56 function t_one thread 0
----------------------------------------------------
  x=1 (00000000000000000000000000000001)

State 253 file input/c/c_seq/p.c line 122 function main thread 0
----------------------------------------------------
  pc[(long int)ct]=(assignment removed)

State 254 file input/c/c_seq/p.c line 123 function main thread 0
----------------------------------------------------
  active[(long int)ct]=(assignment removed)

State 255 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 257 file input/c/c_seq/p.c line 128 function main thread 0
----------------------------------------------------
  cs=3 (00000000000000000000000000000011)

State 278 file input/c/c_seq/p.c line 131 function main thread 0
----------------------------------------------------
  pc[(long int)ct]=(assignment removed)

State 279 file input/c/c_seq/p.c line 132 function main thread 0
----------------------------------------------------
  active[(long int)ct]=(assignment removed)

State 283 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 285 file input/c/c_seq/p.c line 119 function main thread 0
----------------------------------------------------
  cs=10 (00000000000000000000000000001010)

State 306 file input/c/c_seq/p.c line 122 function main thread 0
----------------------------------------------------
  pc[(long int)ct]=(assignment removed)

State 307 file input/c/c_seq/p.c line 123 function main thread 0
----------------------------------------------------
  active[(long int)ct]=(assignment removed)

State 308 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 310 file input/c/c_seq/p.c line 128 function main thread 0
----------------------------------------------------
  cs=3 (00000000000000000000000000000011)

State 331 file input/c/c_seq/p.c line 131 function main thread 0
----------------------------------------------------
  pc[(long int)ct]=(assignment removed)

State 332 file input/c/c_seq/p.c line 132 function main thread 0
----------------------------------------------------
  active[(long int)ct]=(assignment removed)

State 336 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 338 file input/c/c_seq/p.c line 119 function main thread 0
----------------------------------------------------
  cs=10 (00000000000000000000000000001010)

State 359 file input/c/c_seq/p.c line 122 function main thread 0
----------------------------------------------------
  pc[(long int)ct]=(assignment removed)

State 360 file input/c/c_seq/p.c line 123 function main thread 0
----------------------------------------------------
  active[(long int)ct]=(assignment removed)

State 361 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 363 file input/c/c_seq/p.c line 128 function main thread 0
----------------------------------------------------
  cs=3 (00000000000000000000000000000011)

State 384 file input/c/c_seq/p.c line 131 function main thread 0
----------------------------------------------------
  pc[(long int)ct]=(assignment removed)

State 385 file input/c/c_seq/p.c line 132 function main thread 0
----------------------------------------------------
  active[(long int)ct]=(assignment removed)

State 389 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 391 file input/c/c_seq/p.c line 119 function main thread 0
----------------------------------------------------
  cs=10 (00000000000000000000000000001010)

State 412 file input/c/c_seq/p.c line 122 function main thread 0
----------------------------------------------------
  pc[(long int)ct]=(assignment removed)

State 413 file input/c/c_seq/p.c line 123 function main thread 0
----------------------------------------------------
  active[(long int)ct]=(assignment removed)

State 414 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 416 file input/c/c_seq/p.c line 128 function main thread 0
----------------------------------------------------
  cs=3 (00000000000000000000000000000011)

State 437 file input/c/c_seq/p.c line 131 function main thread 0
----------------------------------------------------
  pc[(long int)ct]=(assignment removed)

State 438 file input/c/c_seq/p.c line 132 function main thread 0
----------------------------------------------------
  active[(long int)ct]=(assignment removed)

State 442 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 444 file input/c/c_seq/p.c line 119 function main thread 0
----------------------------------------------------
  cs=10 (00000000000000000000000000001010)

State 465 file input/c/c_seq/p.c line 122 function main thread 0
----------------------------------------------------
  pc[(long int)ct]=(assignment removed)

State 466 file input/c/c_seq/p.c line 123 function main thread 0
----------------------------------------------------
  active[(long int)ct]=(assignment removed)

State 467 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 469 file input/c/c_seq/p.c line 128 function main thread 0
----------------------------------------------------
  cs=3 (00000000000000000000000000000011)

State 490 file input/c/c_seq/p.c line 131 function main thread 0
----------------------------------------------------
  pc[(long int)ct]=(assignment removed)

State 491 file input/c/c_seq/p.c line 132 function main thread 0
----------------------------------------------------
  active[(long int)ct]=(assignment removed)

State 495 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 497 file input/c/c_seq/p.c line 119 function main thread 0
----------------------------------------------------
  cs=10 (00000000000000000000000000001010)

State 518 file input/c/c_seq/p.c line 122 function main thread 0
----------------------------------------------------
  pc[(long int)ct]=(assignment removed)

State 519 file input/c/c_seq/p.c line 123 function main thread 0
----------------------------------------------------
  active[(long int)ct]=(assignment removed)

State 520 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 522 file input/c/c_seq/p.c line 128 function main thread 0
----------------------------------------------------
  cs=3 (00000000000000000000000000000011)

State 543 file input/c/c_seq/p.c line 131 function main thread 0
----------------------------------------------------
  pc[(long int)ct]=(assignment removed)

State 544 file input/c/c_seq/p.c line 132 function main thread 0
----------------------------------------------------
  active[(long int)ct]=(assignment removed)

State 548 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 550 file input/c/c_seq/p.c line 119 function main thread 0
----------------------------------------------------
  cs=11 (00000000000000000000000000001011)

State 569 file input/c/c_seq/p.c line 59 function t_one thread 0
----------------------------------------------------
  inSC=0 (00000000000000000000000000000000)

State 570 file input/c/c_seq/p.c line 60 function t_one thread 0
----------------------------------------------------
  flag[(long int)taskId]=(assignment removed)

State 573 file input/c/c_seq/p.c line 122 function main thread 0
----------------------------------------------------
  pc[(long int)ct]=(assignment removed)

State 574 file input/c/c_seq/p.c line 123 function main thread 0
----------------------------------------------------
  active[(long int)ct]=(assignment removed)

State 575 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 577 file input/c/c_seq/p.c line 128 function main thread 0
----------------------------------------------------
  cs=11 (00000000000000000000000000001011)

State 589 file input/c/c_seq/p.c line 73 function t_two thread 0
----------------------------------------------------
  flag[(long int)taskId]=(assignment removed)

State 591 file input/c/c_seq/p.c line 76 function t_two thread 0
----------------------------------------------------
  turn=1 (00000000000000000000000000000001)

State 595 file input/c/c_seq/p.c line 82 function t_two thread 0
----------------------------------------------------
  inSC=1 (00000000000000000000000000000001)

State 597 file input/c/c_seq/p.c line 85 function t_two thread 0
----------------------------------------------------
  bx=1 (00000000000000000000000000000001)

State 599 file input/c/c_seq/p.c line 87 function t_two thread 0
----------------------------------------------------
  bx=0 (00000000000000000000000000000000)

State 601 file input/c/c_seq/p.c line 89 function t_two thread 0
----------------------------------------------------
  x=0 (00000000000000000000000000000000)

State 603 file input/c/c_seq/p.c line 92 function t_two thread 0
----------------------------------------------------
  inSC=0 (00000000000000000000000000000000)

State 604 file input/c/c_seq/p.c line 93 function t_two thread 0
----------------------------------------------------
  flag[(long int)taskId]=(assignment removed)

State 607 file input/c/c_seq/p.c line 131 function main thread 0
----------------------------------------------------
  pc[(long int)ct]=(assignment removed)

State 608 file input/c/c_seq/p.c line 132 function main thread 0
----------------------------------------------------
  active[(long int)ct]=(assignment removed)

Violated property:
  file input/c/c_seq/p.c line 138 function main
  OK
  0 != 0

VERIFICATION FAILED

real	0m0.725s
user	0m0.649s
sys	0m0.069s
