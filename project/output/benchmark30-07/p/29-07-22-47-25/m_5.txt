CBMC version 5.4 64-bit x86_64 macos
Parsing input/c/c_seq/p.c
Converting
Type-checking p
file input/c/c_seq/p.c line 119 function main: function `nondet_uint' is not declared
Generating GOTO Program
Adding CPROVER library (x86_64)
Function Pointer Removal
Partial Inlining
Generic Property Instrumentation
Starting Bounded Model Checking
Unwinding loop main.0 iteration 1 (5 max) file input/c/c_seq/p.c line 116 function main thread 0
Unwinding loop main.0 iteration 2 (5 max) file input/c/c_seq/p.c line 116 function main thread 0
Unwinding loop main.0 iteration 3 (5 max) file input/c/c_seq/p.c line 116 function main thread 0
Unwinding loop main.0 iteration 4 (5 max) file input/c/c_seq/p.c line 116 function main thread 0
Not unwinding loop main.0 iteration 5 (5 max) file input/c/c_seq/p.c line 116 function main thread 0
size of program expression: 952 steps
simple slicing removed 3 assignments
Generated 11 VCC(s), 11 remaining after simplification
Passing problem to propositional reduction
converting SSA
Running propositional reduction
Post-processing
Solving with MiniSAT 2.2.1 with simplifier
13758 variables, 51505 clauses
SAT checker: instance is SATISFIABLE
Runtime decision procedure: 0.119s
Building error trace

Counterexample:

State 35 file input/c/c_seq/p.c line 99 function init thread 0
----------------------------------------------------
  pc[0]=0 (00000000000000000000000000000000)

State 36 file input/c/c_seq/p.c line 100 function init thread 0
----------------------------------------------------
  size[0]=0 (00000000000000000000000000000000)

State 37 file input/c/c_seq/p.c line 102 function init thread 0
----------------------------------------------------
  pc[1]=0 (00000000000000000000000000000000)

State 38 file input/c/c_seq/p.c line 103 function init thread 0
----------------------------------------------------
  pc[2]=0 (00000000000000000000000000000000)

State 39 file input/c/c_seq/p.c line 104 function init thread 0
----------------------------------------------------
  size[1]=11 (00000000000000000000000000001011)

State 40 file input/c/c_seq/p.c line 105 function init thread 0
----------------------------------------------------
  size[2]=11 (00000000000000000000000000001011)

State 41 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t1=0 (00000000000000000000000000000000)

State 42 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t2=0 (00000000000000000000000000000000)

State 43 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t3=0 (00000000000000000000000000000000)

State 44 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t4=0 (00000000000000000000000000000000)

State 45 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t5=0 (00000000000000000000000000000000)

State 46 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t6=0 (00000000000000000000000000000000)

State 49 file input/c/c_seq/p.c line 109 function init thread 0
----------------------------------------------------
  t=0 (00000000000000000000000000000000)

State 50 file input/c/c_seq/p.c line 109 function init thread 0
----------------------------------------------------
  id=1 (00000000000000000000000000000001)

State 51 file input/c/c_seq/p.c line 20 function create thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 52 file input/c/c_seq/p.c line 21 function create thread 0
----------------------------------------------------
  t=1 (00000000000000000000000000000001)

State 56 file input/c/c_seq/p.c line 110 function init thread 0
----------------------------------------------------
  t=0 (00000000000000000000000000000000)

State 57 file input/c/c_seq/p.c line 110 function init thread 0
----------------------------------------------------
  id=2 (00000000000000000000000000000010)

State 58 file input/c/c_seq/p.c line 20 function create thread 0
----------------------------------------------------
  active[2]=1 (00000000000000000000000000000001)

State 59 file input/c/c_seq/p.c line 21 function create thread 0
----------------------------------------------------
  t=2 (00000000000000000000000000000010)

State 62 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 64 file input/c/c_seq/p.c line 119 function main thread 0
----------------------------------------------------
  cs=0 (00000000000000000000000000000000)

State 85 file input/c/c_seq/p.c line 122 function main thread 0
----------------------------------------------------
  pc[1]=0 (00000000000000000000000000000000)

State 86 file input/c/c_seq/p.c line 123 function main thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 87 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 89 file input/c/c_seq/p.c line 128 function main thread 0
----------------------------------------------------
  cs=11 (00000000000000000000000000001011)

State 100 file input/c/c_seq/p.c line 71 function t_two thread 0
----------------------------------------------------
  other=1 (00000000000000000000000000000001)

State 102 file input/c/c_seq/p.c line 73 function t_two thread 0
----------------------------------------------------
  flag[2]=1 (00000000000000000000000000000001)

State 104 file input/c/c_seq/p.c line 76 function t_two thread 0
----------------------------------------------------
  turn=1 (00000000000000000000000000000001)

State 108 file input/c/c_seq/p.c line 82 function t_two thread 0
----------------------------------------------------
  inSC=1 (00000000000000000000000000000001)

State 110 file input/c/c_seq/p.c line 85 function t_two thread 0
----------------------------------------------------
  bx=0 (00000000000000000000000000000000)

State 112 file input/c/c_seq/p.c line 87 function t_two thread 0
----------------------------------------------------
  bx=-1 (11111111111111111111111111111111)

State 114 file input/c/c_seq/p.c line 89 function t_two thread 0
----------------------------------------------------
  x=-1 (11111111111111111111111111111111)

State 116 file input/c/c_seq/p.c line 92 function t_two thread 0
----------------------------------------------------
  inSC=0 (00000000000000000000000000000000)

State 117 file input/c/c_seq/p.c line 93 function t_two thread 0
----------------------------------------------------
  flag[2]=0 (00000000000000000000000000000000)

State 120 file input/c/c_seq/p.c line 131 function main thread 0
----------------------------------------------------
  pc[2]=11 (00000000000000000000000000001011)

State 121 file input/c/c_seq/p.c line 132 function main thread 0
----------------------------------------------------
  active[2]=0 (00000000000000000000000000000000)

State 125 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 127 file input/c/c_seq/p.c line 119 function main thread 0
----------------------------------------------------
  cs=6 (00000000000000000000000000000110)

State 138 file input/c/c_seq/p.c line 38 function t_one thread 0
----------------------------------------------------
  other=2 (00000000000000000000000000000010)

State 140 file input/c/c_seq/p.c line 40 function t_one thread 0
----------------------------------------------------
  flag[1]=1 (00000000000000000000000000000001)

State 142 file input/c/c_seq/p.c line 43 function t_one thread 0
----------------------------------------------------
  turn=2 (00000000000000000000000000000010)

State 152 file input/c/c_seq/p.c line 122 function main thread 0
----------------------------------------------------
  pc[1]=6 (00000000000000000000000000000110)

State 153 file input/c/c_seq/p.c line 123 function main thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 154 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 159 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 161 file input/c/c_seq/p.c line 119 function main thread 0
----------------------------------------------------
  cs=6 (00000000000000000000000000000110)

State 182 file input/c/c_seq/p.c line 122 function main thread 0
----------------------------------------------------
  pc[1]=6 (00000000000000000000000000000110)

State 183 file input/c/c_seq/p.c line 123 function main thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 184 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 189 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 191 file input/c/c_seq/p.c line 119 function main thread 0
----------------------------------------------------
  cs=11 (00000000000000000000000000001011)

State 206 file input/c/c_seq/p.c line 49 function t_one thread 0
----------------------------------------------------
  inSC=1 (00000000000000000000000000000001)

State 208 file input/c/c_seq/p.c line 52 function t_one thread 0
----------------------------------------------------
  ax=-1 (11111111111111111111111111111111)

State 210 file input/c/c_seq/p.c line 54 function t_one thread 0
----------------------------------------------------
  ax=0 (00000000000000000000000000000000)

State 212 file input/c/c_seq/p.c line 56 function t_one thread 0
----------------------------------------------------
  x=0 (00000000000000000000000000000000)

State 214 file input/c/c_seq/p.c line 59 function t_one thread 0
----------------------------------------------------
  inSC=0 (00000000000000000000000000000000)

State 215 file input/c/c_seq/p.c line 60 function t_one thread 0
----------------------------------------------------
  flag[1]=0 (00000000000000000000000000000000)

State 218 file input/c/c_seq/p.c line 122 function main thread 0
----------------------------------------------------
  pc[1]=11 (00000000000000000000000000001011)

State 219 file input/c/c_seq/p.c line 123 function main thread 0
----------------------------------------------------
  active[1]=0 (00000000000000000000000000000000)

State 220 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

Violated property:
  file input/c/c_seq/p.c line 138 function main
  OK
  0 != 0

VERIFICATION FAILED

real	0m0.338s
user	0m0.307s
sys	0m0.024s
