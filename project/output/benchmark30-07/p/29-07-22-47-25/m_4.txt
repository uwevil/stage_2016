CBMC version 5.4 64-bit x86_64 macos
Parsing input/c/c_seq/p.c
Converting
Type-checking p
file input/c/c_seq/p.c line 119 function main: function `nondet_uint' is not declared
Generating GOTO Program
Adding CPROVER library (x86_64)
Function Pointer Removal
Partial Inlining
Generic Property Instrumentation
Starting Bounded Model Checking
Unwinding loop main.0 iteration 1 (4 max) file input/c/c_seq/p.c line 116 function main thread 0
Unwinding loop main.0 iteration 2 (4 max) file input/c/c_seq/p.c line 116 function main thread 0
Unwinding loop main.0 iteration 3 (4 max) file input/c/c_seq/p.c line 116 function main thread 0
Not unwinding loop main.0 iteration 4 (4 max) file input/c/c_seq/p.c line 116 function main thread 0
size of program expression: 779 steps
simple slicing removed 3 assignments
Generated 9 VCC(s), 9 remaining after simplification
Passing problem to propositional reduction
converting SSA
Running propositional reduction
Post-processing
Solving with MiniSAT 2.2.1 with simplifier
10764 variables, 39976 clauses
SAT checker: instance is SATISFIABLE
Runtime decision procedure: 0.084s
Building error trace

Counterexample:

State 35 file input/c/c_seq/p.c line 99 function init thread 0
----------------------------------------------------
  pc[0]=0 (00000000000000000000000000000000)

State 36 file input/c/c_seq/p.c line 100 function init thread 0
----------------------------------------------------
  size[0]=0 (00000000000000000000000000000000)

State 37 file input/c/c_seq/p.c line 102 function init thread 0
----------------------------------------------------
  pc[1]=0 (00000000000000000000000000000000)

State 38 file input/c/c_seq/p.c line 103 function init thread 0
----------------------------------------------------
  pc[2]=0 (00000000000000000000000000000000)

State 39 file input/c/c_seq/p.c line 104 function init thread 0
----------------------------------------------------
  size[1]=11 (00000000000000000000000000001011)

State 40 file input/c/c_seq/p.c line 105 function init thread 0
----------------------------------------------------
  size[2]=11 (00000000000000000000000000001011)

State 41 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t1=0 (00000000000000000000000000000000)

State 42 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t2=0 (00000000000000000000000000000000)

State 43 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t3=0 (00000000000000000000000000000000)

State 44 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t4=0 (00000000000000000000000000000000)

State 45 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t5=0 (00000000000000000000000000000000)

State 46 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t6=0 (00000000000000000000000000000000)

State 49 file input/c/c_seq/p.c line 109 function init thread 0
----------------------------------------------------
  t=0 (00000000000000000000000000000000)

State 50 file input/c/c_seq/p.c line 109 function init thread 0
----------------------------------------------------
  id=1 (00000000000000000000000000000001)

State 51 file input/c/c_seq/p.c line 20 function create thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 52 file input/c/c_seq/p.c line 21 function create thread 0
----------------------------------------------------
  t=1 (00000000000000000000000000000001)

State 56 file input/c/c_seq/p.c line 110 function init thread 0
----------------------------------------------------
  t=0 (00000000000000000000000000000000)

State 57 file input/c/c_seq/p.c line 110 function init thread 0
----------------------------------------------------
  id=2 (00000000000000000000000000000010)

State 58 file input/c/c_seq/p.c line 20 function create thread 0
----------------------------------------------------
  active[2]=1 (00000000000000000000000000000001)

State 59 file input/c/c_seq/p.c line 21 function create thread 0
----------------------------------------------------
  t=2 (00000000000000000000000000000010)

State 62 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 64 file input/c/c_seq/p.c line 119 function main thread 0
----------------------------------------------------
  cs=4 (00000000000000000000000000000100)

State 75 file input/c/c_seq/p.c line 38 function t_one thread 0
----------------------------------------------------
  other=2 (00000000000000000000000000000010)

State 77 file input/c/c_seq/p.c line 40 function t_one thread 0
----------------------------------------------------
  flag[1]=1 (00000000000000000000000000000001)

State 87 file input/c/c_seq/p.c line 122 function main thread 0
----------------------------------------------------
  pc[1]=4 (00000000000000000000000000000100)

State 88 file input/c/c_seq/p.c line 123 function main thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 89 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 91 file input/c/c_seq/p.c line 128 function main thread 0
----------------------------------------------------
  cs=3 (00000000000000000000000000000011)

State 102 file input/c/c_seq/p.c line 71 function t_two thread 0
----------------------------------------------------
  other=1 (00000000000000000000000000000001)

State 113 file input/c/c_seq/p.c line 131 function main thread 0
----------------------------------------------------
  pc[2]=3 (00000000000000000000000000000011)

State 114 file input/c/c_seq/p.c line 132 function main thread 0
----------------------------------------------------
  active[2]=1 (00000000000000000000000000000001)

State 118 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 120 file input/c/c_seq/p.c line 119 function main thread 0
----------------------------------------------------
  cs=7 (00000000000000000000000000000111)

State 133 file input/c/c_seq/p.c line 43 function t_one thread 0
----------------------------------------------------
  turn=2 (00000000000000000000000000000010)

State 137 file input/c/c_seq/p.c line 49 function t_one thread 0
----------------------------------------------------
  inSC=1 (00000000000000000000000000000001)

State 144 file input/c/c_seq/p.c line 122 function main thread 0
----------------------------------------------------
  pc[1]=7 (00000000000000000000000000000111)

State 145 file input/c/c_seq/p.c line 123 function main thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 146 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 148 file input/c/c_seq/p.c line 128 function main thread 0
----------------------------------------------------
  cs=4 (00000000000000000000000000000100)

State 160 file input/c/c_seq/p.c line 73 function t_two thread 0
----------------------------------------------------
  flag[2]=1 (00000000000000000000000000000001)

State 170 file input/c/c_seq/p.c line 131 function main thread 0
----------------------------------------------------
  pc[2]=4 (00000000000000000000000000000100)

State 171 file input/c/c_seq/p.c line 132 function main thread 0
----------------------------------------------------
  active[2]=1 (00000000000000000000000000000001)

State 175 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 177 file input/c/c_seq/p.c line 119 function main thread 0
----------------------------------------------------
  cs=11 (00000000000000000000000000001011)

State 193 file input/c/c_seq/p.c line 52 function t_one thread 0
----------------------------------------------------
  ax=0 (00000000000000000000000000000000)

State 195 file input/c/c_seq/p.c line 54 function t_one thread 0
----------------------------------------------------
  ax=1 (00000000000000000000000000000001)

State 197 file input/c/c_seq/p.c line 56 function t_one thread 0
----------------------------------------------------
  x=1 (00000000000000000000000000000001)

State 199 file input/c/c_seq/p.c line 59 function t_one thread 0
----------------------------------------------------
  inSC=0 (00000000000000000000000000000000)

State 200 file input/c/c_seq/p.c line 60 function t_one thread 0
----------------------------------------------------
  flag[1]=0 (00000000000000000000000000000000)

State 203 file input/c/c_seq/p.c line 122 function main thread 0
----------------------------------------------------
  pc[1]=11 (00000000000000000000000000001011)

State 204 file input/c/c_seq/p.c line 123 function main thread 0
----------------------------------------------------
  active[1]=0 (00000000000000000000000000000000)

State 205 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 207 file input/c/c_seq/p.c line 128 function main thread 0
----------------------------------------------------
  cs=8 (00000000000000000000000000001000)

State 220 file input/c/c_seq/p.c line 76 function t_two thread 0
----------------------------------------------------
  turn=1 (00000000000000000000000000000001)

State 224 file input/c/c_seq/p.c line 82 function t_two thread 0
----------------------------------------------------
  inSC=1 (00000000000000000000000000000001)

State 226 file input/c/c_seq/p.c line 85 function t_two thread 0
----------------------------------------------------
  bx=1 (00000000000000000000000000000001)

State 232 file input/c/c_seq/p.c line 131 function main thread 0
----------------------------------------------------
  pc[2]=8 (00000000000000000000000000001000)

State 233 file input/c/c_seq/p.c line 132 function main thread 0
----------------------------------------------------
  active[2]=1 (00000000000000000000000000000001)

State 238 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 240 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 242 file input/c/c_seq/p.c line 128 function main thread 0
----------------------------------------------------
  cs=11 (00000000000000000000000000001011)

State 259 file input/c/c_seq/p.c line 87 function t_two thread 0
----------------------------------------------------
  bx=0 (00000000000000000000000000000000)

State 261 file input/c/c_seq/p.c line 89 function t_two thread 0
----------------------------------------------------
  x=0 (00000000000000000000000000000000)

State 263 file input/c/c_seq/p.c line 92 function t_two thread 0
----------------------------------------------------
  inSC=0 (00000000000000000000000000000000)

State 264 file input/c/c_seq/p.c line 93 function t_two thread 0
----------------------------------------------------
  flag[2]=0 (00000000000000000000000000000000)

State 267 file input/c/c_seq/p.c line 131 function main thread 0
----------------------------------------------------
  pc[2]=11 (00000000000000000000000000001011)

State 268 file input/c/c_seq/p.c line 132 function main thread 0
----------------------------------------------------
  active[2]=0 (00000000000000000000000000000000)

Violated property:
  file input/c/c_seq/p.c line 138 function main
  OK
  0 != 0

VERIFICATION FAILED

real	0m0.495s
user	0m0.260s
sys	0m0.128s
