CBMC version 5.4 64-bit x86_64 macos
Parsing input/c/c_seq/p.c
Converting
Type-checking p
file input/c/c_seq/p.c line 119 function main: function `nondet_uint' is not declared
Generating GOTO Program
Adding CPROVER library (x86_64)
Function Pointer Removal
Partial Inlining
Generic Property Instrumentation
Starting Bounded Model Checking
Unwinding loop main.0 iteration 1 (10 max) file input/c/c_seq/p.c line 116 function main thread 0
Unwinding loop main.0 iteration 2 (10 max) file input/c/c_seq/p.c line 116 function main thread 0
Unwinding loop main.0 iteration 3 (10 max) file input/c/c_seq/p.c line 116 function main thread 0
Unwinding loop main.0 iteration 4 (10 max) file input/c/c_seq/p.c line 116 function main thread 0
Unwinding loop main.0 iteration 5 (10 max) file input/c/c_seq/p.c line 116 function main thread 0
Unwinding loop main.0 iteration 6 (10 max) file input/c/c_seq/p.c line 116 function main thread 0
Unwinding loop main.0 iteration 7 (10 max) file input/c/c_seq/p.c line 116 function main thread 0
Unwinding loop main.0 iteration 8 (10 max) file input/c/c_seq/p.c line 116 function main thread 0
Unwinding loop main.0 iteration 9 (10 max) file input/c/c_seq/p.c line 116 function main thread 0
Not unwinding loop main.0 iteration 10 (10 max) file input/c/c_seq/p.c line 116 function main thread 0
size of program expression: 1817 steps
simple slicing removed 3 assignments
Generated 21 VCC(s), 21 remaining after simplification
Passing problem to propositional reduction
converting SSA
Running propositional reduction
Post-processing
Solving with Glucose Syrup with simplifier
28728 variables, 109210 clauses
SAT checker: instance is SATISFIABLE
Runtime decision procedure: 0.24s
Building error trace

Counterexample:

State 35 file input/c/c_seq/p.c line 99 function init thread 0
----------------------------------------------------
  pc[0]=0 (00000000000000000000000000000000)

State 36 file input/c/c_seq/p.c line 100 function init thread 0
----------------------------------------------------
  size[0]=0 (00000000000000000000000000000000)

State 37 file input/c/c_seq/p.c line 102 function init thread 0
----------------------------------------------------
  pc[1]=0 (00000000000000000000000000000000)

State 38 file input/c/c_seq/p.c line 103 function init thread 0
----------------------------------------------------
  pc[2]=0 (00000000000000000000000000000000)

State 39 file input/c/c_seq/p.c line 104 function init thread 0
----------------------------------------------------
  size[1]=11 (00000000000000000000000000001011)

State 40 file input/c/c_seq/p.c line 105 function init thread 0
----------------------------------------------------
  size[2]=11 (00000000000000000000000000001011)

State 41 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t1=0 (00000000000000000000000000000000)

State 42 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t2=0 (00000000000000000000000000000000)

State 43 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t3=0 (00000000000000000000000000000000)

State 44 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t4=0 (00000000000000000000000000000000)

State 45 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t5=0 (00000000000000000000000000000000)

State 46 file input/c/c_seq/p.c line 107 function init thread 0
----------------------------------------------------
  t6=0 (00000000000000000000000000000000)

State 49 file input/c/c_seq/p.c line 109 function init thread 0
----------------------------------------------------
  t=0 (00000000000000000000000000000000)

State 50 file input/c/c_seq/p.c line 109 function init thread 0
----------------------------------------------------
  id=1 (00000000000000000000000000000001)

State 51 file input/c/c_seq/p.c line 20 function create thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 52 file input/c/c_seq/p.c line 21 function create thread 0
----------------------------------------------------
  t=1 (00000000000000000000000000000001)

State 56 file input/c/c_seq/p.c line 110 function init thread 0
----------------------------------------------------
  t=0 (00000000000000000000000000000000)

State 57 file input/c/c_seq/p.c line 110 function init thread 0
----------------------------------------------------
  id=2 (00000000000000000000000000000010)

State 58 file input/c/c_seq/p.c line 20 function create thread 0
----------------------------------------------------
  active[2]=1 (00000000000000000000000000000001)

State 59 file input/c/c_seq/p.c line 21 function create thread 0
----------------------------------------------------
  t=2 (00000000000000000000000000000010)

State 62 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 64 file input/c/c_seq/p.c line 119 function main thread 0
----------------------------------------------------
  cs=11 (00000000000000000000000000001011)

State 75 file input/c/c_seq/p.c line 38 function t_one thread 0
----------------------------------------------------
  other=2 (00000000000000000000000000000010)

State 77 file input/c/c_seq/p.c line 40 function t_one thread 0
----------------------------------------------------
  flag[1]=1 (00000000000000000000000000000001)

State 79 file input/c/c_seq/p.c line 43 function t_one thread 0
----------------------------------------------------
  turn=2 (00000000000000000000000000000010)

State 83 file input/c/c_seq/p.c line 49 function t_one thread 0
----------------------------------------------------
  inSC=1 (00000000000000000000000000000001)

State 85 file input/c/c_seq/p.c line 52 function t_one thread 0
----------------------------------------------------
  ax=0 (00000000000000000000000000000000)

State 87 file input/c/c_seq/p.c line 54 function t_one thread 0
----------------------------------------------------
  ax=1 (00000000000000000000000000000001)

State 89 file input/c/c_seq/p.c line 56 function t_one thread 0
----------------------------------------------------
  x=1 (00000000000000000000000000000001)

State 91 file input/c/c_seq/p.c line 59 function t_one thread 0
----------------------------------------------------
  inSC=0 (00000000000000000000000000000000)

State 92 file input/c/c_seq/p.c line 60 function t_one thread 0
----------------------------------------------------
  flag[1]=0 (00000000000000000000000000000000)

State 95 file input/c/c_seq/p.c line 122 function main thread 0
----------------------------------------------------
  pc[1]=11 (00000000000000000000000000001011)

State 96 file input/c/c_seq/p.c line 123 function main thread 0
----------------------------------------------------
  active[1]=0 (00000000000000000000000000000000)

State 97 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 99 file input/c/c_seq/p.c line 128 function main thread 0
----------------------------------------------------
  cs=5 (00000000000000000000000000000101)

State 110 file input/c/c_seq/p.c line 71 function t_two thread 0
----------------------------------------------------
  other=1 (00000000000000000000000000000001)

State 112 file input/c/c_seq/p.c line 73 function t_two thread 0
----------------------------------------------------
  flag[2]=1 (00000000000000000000000000000001)

State 114 file input/c/c_seq/p.c line 76 function t_two thread 0
----------------------------------------------------
  turn=1 (00000000000000000000000000000001)

State 123 file input/c/c_seq/p.c line 131 function main thread 0
----------------------------------------------------
  pc[2]=5 (00000000000000000000000000000101)

State 124 file input/c/c_seq/p.c line 132 function main thread 0
----------------------------------------------------
  active[2]=1 (00000000000000000000000000000001)

State 129 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 131 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 133 file input/c/c_seq/p.c line 128 function main thread 0
----------------------------------------------------
  cs=7 (00000000000000000000000000000111)

State 149 file input/c/c_seq/p.c line 82 function t_two thread 0
----------------------------------------------------
  inSC=1 (00000000000000000000000000000001)

State 156 file input/c/c_seq/p.c line 131 function main thread 0
----------------------------------------------------
  pc[2]=7 (00000000000000000000000000000111)

State 157 file input/c/c_seq/p.c line 132 function main thread 0
----------------------------------------------------
  active[2]=1 (00000000000000000000000000000001)

State 162 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 164 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 166 file input/c/c_seq/p.c line 128 function main thread 0
----------------------------------------------------
  cs=11 (00000000000000000000000000001011)

State 182 file input/c/c_seq/p.c line 85 function t_two thread 0
----------------------------------------------------
  bx=1 (00000000000000000000000000000001)

State 184 file input/c/c_seq/p.c line 87 function t_two thread 0
----------------------------------------------------
  bx=0 (00000000000000000000000000000000)

State 186 file input/c/c_seq/p.c line 89 function t_two thread 0
----------------------------------------------------
  x=0 (00000000000000000000000000000000)

State 188 file input/c/c_seq/p.c line 92 function t_two thread 0
----------------------------------------------------
  inSC=0 (00000000000000000000000000000000)

State 189 file input/c/c_seq/p.c line 93 function t_two thread 0
----------------------------------------------------
  flag[2]=0 (00000000000000000000000000000000)

State 192 file input/c/c_seq/p.c line 131 function main thread 0
----------------------------------------------------
  pc[2]=11 (00000000000000000000000000001011)

State 193 file input/c/c_seq/p.c line 132 function main thread 0
----------------------------------------------------
  active[2]=0 (00000000000000000000000000000000)

Violated property:
  file input/c/c_seq/p.c line 138 function main
  OK
  0 != 0

VERIFICATION FAILED

real	0m0.525s
user	0m0.485s
sys	0m0.033s
