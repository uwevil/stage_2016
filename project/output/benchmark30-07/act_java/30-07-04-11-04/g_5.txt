CBMC version 5.4 64-bit x86_64 macos
Parsing input/c/c_seq/act_java.c
Converting
Type-checking act_java
file input/c/c_seq/act_java.c line 584 function main: function `nondet_uint' is not declared
Generating GOTO Program
Adding CPROVER library (x86_64)
Function Pointer Removal
Partial Inlining
Generic Property Instrumentation
Starting Bounded Model Checking
Unwinding loop main.0 iteration 1 (5 max) file input/c/c_seq/act_java.c line 581 function main thread 0
Unwinding loop main.0 iteration 2 (5 max) file input/c/c_seq/act_java.c line 581 function main thread 0
Unwinding loop main.0 iteration 3 (5 max) file input/c/c_seq/act_java.c line 581 function main thread 0
Unwinding loop main.0 iteration 4 (5 max) file input/c/c_seq/act_java.c line 581 function main thread 0
Not unwinding loop main.0 iteration 5 (5 max) file input/c/c_seq/act_java.c line 581 function main thread 0
size of program expression: 4837 steps
simple slicing removed 3 assignments
Generated 125 VCC(s), 125 remaining after simplification
Passing problem to propositional reduction
converting SSA
Running propositional reduction
Post-processing
Solving with Glucose Syrup with simplifier
148339 variables, 604489 clauses
SAT checker: instance is SATISFIABLE
Runtime decision procedure: 3.68s
Building error trace

Counterexample:

State 41 file input/c/c_seq/act_java.c line 560 function init thread 0
----------------------------------------------------
  pc[0]=0 (00000000000000000000000000000000)

State 42 file input/c/c_seq/act_java.c line 561 function init thread 0
----------------------------------------------------
  size[0]=0 (00000000000000000000000000000000)

State 43 file input/c/c_seq/act_java.c line 563 function init thread 0
----------------------------------------------------
  pc[1]=0 (00000000000000000000000000000000)

State 44 file input/c/c_seq/act_java.c line 564 function init thread 0
----------------------------------------------------
  pc[2]=0 (00000000000000000000000000000000)

State 45 file input/c/c_seq/act_java.c line 565 function init thread 0
----------------------------------------------------
  pc[3]=0 (00000000000000000000000000000000)

State 46 file input/c/c_seq/act_java.c line 566 function init thread 0
----------------------------------------------------
  ssize=0 (00000000000000000000000000000000)

State 47 file input/c/c_seq/act_java.c line 566 function init thread 0
----------------------------------------------------
  ssize=33 (00000000000000000000000000100001)

State 48 file input/c/c_seq/act_java.c line 567 function init thread 0
----------------------------------------------------
  size[1]=33 (00000000000000000000000000100001)

State 49 file input/c/c_seq/act_java.c line 568 function init thread 0
----------------------------------------------------
  size[2]=33 (00000000000000000000000000100001)

State 50 file input/c/c_seq/act_java.c line 569 function init thread 0
----------------------------------------------------
  size[3]=33 (00000000000000000000000000100001)

State 51 file input/c/c_seq/act_java.c line 571 function init thread 0
----------------------------------------------------
  t1=0 (00000000000000000000000000000000)

State 52 file input/c/c_seq/act_java.c line 571 function init thread 0
----------------------------------------------------
  t2=0 (00000000000000000000000000000000)

State 53 file input/c/c_seq/act_java.c line 571 function init thread 0
----------------------------------------------------
  t3=0 (00000000000000000000000000000000)

State 56 file input/c/c_seq/act_java.c line 573 function init thread 0
----------------------------------------------------
  t=0 (00000000000000000000000000000000)

State 57 file input/c/c_seq/act_java.c line 573 function init thread 0
----------------------------------------------------
  id=1 (00000000000000000000000000000001)

State 58 file input/c/c_seq/act_java.c line 21 function create thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 59 file input/c/c_seq/act_java.c line 22 function create thread 0
----------------------------------------------------
  t=1 (00000000000000000000000000000001)

State 63 file input/c/c_seq/act_java.c line 574 function init thread 0
----------------------------------------------------
  t=0 (00000000000000000000000000000000)

State 64 file input/c/c_seq/act_java.c line 574 function init thread 0
----------------------------------------------------
  id=2 (00000000000000000000000000000010)

State 65 file input/c/c_seq/act_java.c line 21 function create thread 0
----------------------------------------------------
  active[2]=1 (00000000000000000000000000000001)

State 66 file input/c/c_seq/act_java.c line 22 function create thread 0
----------------------------------------------------
  t=2 (00000000000000000000000000000010)

State 70 file input/c/c_seq/act_java.c line 575 function init thread 0
----------------------------------------------------
  t=0 (00000000000000000000000000000000)

State 71 file input/c/c_seq/act_java.c line 575 function init thread 0
----------------------------------------------------
  id=3 (00000000000000000000000000000011)

State 72 file input/c/c_seq/act_java.c line 21 function create thread 0
----------------------------------------------------
  active[3]=1 (00000000000000000000000000000001)

State 73 file input/c/c_seq/act_java.c line 22 function create thread 0
----------------------------------------------------
  t=3 (00000000000000000000000000000011)

State 76 file input/c/c_seq/act_java.c line 582 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 78 file input/c/c_seq/act_java.c line 584 function main thread 0
----------------------------------------------------
  cs=31 (00000000000000000000000000011111)

State 87 file input/c/c_seq/act_java.c line 77 function t_one thread 0
----------------------------------------------------
  i=1 (00000000000000000000000000000001)

State 89 file input/c/c_seq/act_java.c line 79 function t_one thread 0
----------------------------------------------------
  MyClaim=1 (00000000000000000000000000000001)

State 91 file input/c/c_seq/act_java.c line 82 function t_one thread 0
----------------------------------------------------
  y=1 (00000000000000000000000000000001)

State 94 file input/c/c_seq/act_java.c line 94 function t_one thread 0
----------------------------------------------------
  stock=21 (00000000000000000000000000010101)

State 98 file input/c/c_seq/act_java.c line 110 function t_one thread 0
----------------------------------------------------
  FullAllocation=1 (00000000000000000000000000000001)

State 100 file input/c/c_seq/act_java.c line 112 function t_one thread 0
----------------------------------------------------
  i=2 (00000000000000000000000000000010)

State 102 file input/c/c_seq/act_java.c line 114 function t_one thread 0
----------------------------------------------------
  MyClaim=2 (00000000000000000000000000000010)

State 104 file input/c/c_seq/act_java.c line 117 function t_one thread 0
----------------------------------------------------
  y=2 (00000000000000000000000000000010)

State 107 file input/c/c_seq/act_java.c line 129 function t_one thread 0
----------------------------------------------------
  stock=19 (00000000000000000000000000010011)

State 111 file input/c/c_seq/act_java.c line 145 function t_one thread 0
----------------------------------------------------
  FullAllocation=3 (00000000000000000000000000000011)

State 113 file input/c/c_seq/act_java.c line 147 function t_one thread 0
----------------------------------------------------
  i=3 (00000000000000000000000000000011)

State 115 file input/c/c_seq/act_java.c line 149 function t_one thread 0
----------------------------------------------------
  MyClaim=3 (00000000000000000000000000000011)

State 117 file input/c/c_seq/act_java.c line 152 function t_one thread 0
----------------------------------------------------
  y=3 (00000000000000000000000000000011)

State 120 file input/c/c_seq/act_java.c line 164 function t_one thread 0
----------------------------------------------------
  stock=16 (00000000000000000000000000010000)

State 124 file input/c/c_seq/act_java.c line 180 function t_one thread 0
----------------------------------------------------
  FullAllocation=6 (00000000000000000000000000000110)

State 126 file input/c/c_seq/act_java.c line 182 function t_one thread 0
----------------------------------------------------
  i=4 (00000000000000000000000000000100)

State 128 file input/c/c_seq/act_java.c line 184 function t_one thread 0
----------------------------------------------------
  MyClaim=4 (00000000000000000000000000000100)

State 130 file input/c/c_seq/act_java.c line 187 function t_one thread 0
----------------------------------------------------
  y=4 (00000000000000000000000000000100)

State 133 file input/c/c_seq/act_java.c line 199 function t_one thread 0
----------------------------------------------------
  stock=12 (00000000000000000000000000001100)

State 140 file input/c/c_seq/act_java.c line 587 function main thread 0
----------------------------------------------------
  pc[1]=31 (00000000000000000000000000011111)

State 141 file input/c/c_seq/act_java.c line 588 function main thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 142 file input/c/c_seq/act_java.c line 589 function main thread 0
----------------------------------------------------
  dead=0 (00000000000000000000000000000000)

State 143 file input/c/c_seq/act_java.c line 592 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 145 file input/c/c_seq/act_java.c line 594 function main thread 0
----------------------------------------------------
  cs=31 (00000000000000000000000000011111)

State 154 file input/c/c_seq/act_java.c line 241 function t_two thread 0
----------------------------------------------------
  i=1 (00000000000000000000000000000001)

State 156 file input/c/c_seq/act_java.c line 243 function t_two thread 0
----------------------------------------------------
  MyClaim=1 (00000000000000000000000000000001)

State 158 file input/c/c_seq/act_java.c line 246 function t_two thread 0
----------------------------------------------------
  y=1 (00000000000000000000000000000001)

State 161 file input/c/c_seq/act_java.c line 258 function t_two thread 0
----------------------------------------------------
  stock=11 (00000000000000000000000000001011)

State 165 file input/c/c_seq/act_java.c line 274 function t_two thread 0
----------------------------------------------------
  FullAllocation=1 (00000000000000000000000000000001)

State 167 file input/c/c_seq/act_java.c line 276 function t_two thread 0
----------------------------------------------------
  i=2 (00000000000000000000000000000010)

State 169 file input/c/c_seq/act_java.c line 278 function t_two thread 0
----------------------------------------------------
  MyClaim=2 (00000000000000000000000000000010)

State 171 file input/c/c_seq/act_java.c line 281 function t_two thread 0
----------------------------------------------------
  y=2 (00000000000000000000000000000010)

State 174 file input/c/c_seq/act_java.c line 293 function t_two thread 0
----------------------------------------------------
  stock=9 (00000000000000000000000000001001)

State 178 file input/c/c_seq/act_java.c line 309 function t_two thread 0
----------------------------------------------------
  FullAllocation=3 (00000000000000000000000000000011)

State 180 file input/c/c_seq/act_java.c line 311 function t_two thread 0
----------------------------------------------------
  i=3 (00000000000000000000000000000011)

State 182 file input/c/c_seq/act_java.c line 313 function t_two thread 0
----------------------------------------------------
  MyClaim=3 (00000000000000000000000000000011)

State 184 file input/c/c_seq/act_java.c line 316 function t_two thread 0
----------------------------------------------------
  y=3 (00000000000000000000000000000011)

State 187 file input/c/c_seq/act_java.c line 328 function t_two thread 0
----------------------------------------------------
  stock=6 (00000000000000000000000000000110)

State 191 file input/c/c_seq/act_java.c line 344 function t_two thread 0
----------------------------------------------------
  FullAllocation=6 (00000000000000000000000000000110)

State 193 file input/c/c_seq/act_java.c line 346 function t_two thread 0
----------------------------------------------------
  i=4 (00000000000000000000000000000100)

State 195 file input/c/c_seq/act_java.c line 348 function t_two thread 0
----------------------------------------------------
  MyClaim=4 (00000000000000000000000000000100)

State 197 file input/c/c_seq/act_java.c line 351 function t_two thread 0
----------------------------------------------------
  y=4 (00000000000000000000000000000100)

State 200 file input/c/c_seq/act_java.c line 363 function t_two thread 0
----------------------------------------------------
  stock=2 (00000000000000000000000000000010)

State 207 file input/c/c_seq/act_java.c line 597 function main thread 0
----------------------------------------------------
  pc[2]=31 (00000000000000000000000000011111)

State 208 file input/c/c_seq/act_java.c line 598 function main thread 0
----------------------------------------------------
  active[2]=1 (00000000000000000000000000000001)

State 209 file input/c/c_seq/act_java.c line 599 function main thread 0
----------------------------------------------------
  dead=0 (00000000000000000000000000000000)

State 210 file input/c/c_seq/act_java.c line 602 function main thread 0
----------------------------------------------------
  ct=3 (00000000000000000000000000000011)

State 212 file input/c/c_seq/act_java.c line 604 function main thread 0
----------------------------------------------------
  cs=13 (00000000000000000000000000001101)

State 221 file input/c/c_seq/act_java.c line 404 function t_three thread 0
----------------------------------------------------
  i=1 (00000000000000000000000000000001)

State 223 file input/c/c_seq/act_java.c line 406 function t_three thread 0
----------------------------------------------------
  MyClaim=1 (00000000000000000000000000000001)

State 225 file input/c/c_seq/act_java.c line 409 function t_three thread 0
----------------------------------------------------
  y=1 (00000000000000000000000000000001)

State 228 file input/c/c_seq/act_java.c line 421 function t_three thread 0
----------------------------------------------------
  stock=1 (00000000000000000000000000000001)

State 232 file input/c/c_seq/act_java.c line 437 function t_three thread 0
----------------------------------------------------
  FullAllocation=1 (00000000000000000000000000000001)

State 234 file input/c/c_seq/act_java.c line 439 function t_three thread 0
----------------------------------------------------
  i=2 (00000000000000000000000000000010)

State 236 file input/c/c_seq/act_java.c line 441 function t_three thread 0
----------------------------------------------------
  MyClaim=2 (00000000000000000000000000000010)

State 238 file input/c/c_seq/act_java.c line 444 function t_three thread 0
----------------------------------------------------
  y=2 (00000000000000000000000000000010)

State 241 file input/c/c_seq/act_java.c line 456 function t_three thread 0
----------------------------------------------------
  stock=-1 (11111111111111111111111111111111)

State 243 file input/c/c_seq/act_java.c line 459 function t_three thread 0
----------------------------------------------------
  assignCount=1 (00000000000000000000000000000001)

State 244 file input/c/c_seq/act_java.c line 460 function t_three thread 0
----------------------------------------------------
  taskInWait=1 (00000000000000000000000000000001)

State 268 file input/c/c_seq/act_java.c line 607 function main thread 0
----------------------------------------------------
  pc[3]=13 (00000000000000000000000000001101)

State 269 file input/c/c_seq/act_java.c line 608 function main thread 0
----------------------------------------------------
  active[3]=1 (00000000000000000000000000000001)

State 270 file input/c/c_seq/act_java.c line 609 function main thread 0
----------------------------------------------------
  dead=0 (00000000000000000000000000000000)

State 273 file input/c/c_seq/act_java.c line 582 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 275 file input/c/c_seq/act_java.c line 584 function main thread 0
----------------------------------------------------
  cs=32 (00000000000000000000000000100000)

State 315 file input/c/c_seq/act_java.c line 215 function t_one thread 0
----------------------------------------------------
  FullAllocation=10 (00000000000000000000000000001010)

State 319 file input/c/c_seq/act_java.c line 587 function main thread 0
----------------------------------------------------
  pc[1]=32 (00000000000000000000000000100000)

State 320 file input/c/c_seq/act_java.c line 588 function main thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 321 file input/c/c_seq/act_java.c line 589 function main thread 0
----------------------------------------------------
  dead=0 (00000000000000000000000000000000)

State 322 file input/c/c_seq/act_java.c line 592 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 324 file input/c/c_seq/act_java.c line 594 function main thread 0
----------------------------------------------------
  cs=31 (00000000000000000000000000011111)

State 367 file input/c/c_seq/act_java.c line 597 function main thread 0
----------------------------------------------------
  pc[2]=31 (00000000000000000000000000011111)

State 368 file input/c/c_seq/act_java.c line 598 function main thread 0
----------------------------------------------------
  active[2]=1 (00000000000000000000000000000001)

State 369 file input/c/c_seq/act_java.c line 599 function main thread 0
----------------------------------------------------
  dead=0 (00000000000000000000000000000000)

State 370 file input/c/c_seq/act_java.c line 602 function main thread 0
----------------------------------------------------
  ct=3 (00000000000000000000000000000011)

State 372 file input/c/c_seq/act_java.c line 604 function main thread 0
----------------------------------------------------
  cs=13 (00000000000000000000000000001101)

State 415 file input/c/c_seq/act_java.c line 607 function main thread 0
----------------------------------------------------
  pc[3]=13 (00000000000000000000000000001101)

State 416 file input/c/c_seq/act_java.c line 608 function main thread 0
----------------------------------------------------
  active[3]=1 (00000000000000000000000000000001)

State 417 file input/c/c_seq/act_java.c line 609 function main thread 0
----------------------------------------------------
  dead=0 (00000000000000000000000000000000)

State 420 file input/c/c_seq/act_java.c line 582 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 422 file input/c/c_seq/act_java.c line 584 function main thread 0
----------------------------------------------------
  cs=33 (00000000000000000000000000100001)

State 463 file input/c/c_seq/act_java.c line 220 function t_one thread 0
----------------------------------------------------
  y=10 (00000000000000000000000000001010)

State 464 file input/c/c_seq/act_java.c line 221 function t_one thread 0
----------------------------------------------------
  stock=9 (00000000000000000000000000001001)

State 467 file input/c/c_seq/act_java.c line 587 function main thread 0
----------------------------------------------------
  pc[1]=33 (00000000000000000000000000100001)

State 468 file input/c/c_seq/act_java.c line 588 function main thread 0
----------------------------------------------------
  active[1]=0 (00000000000000000000000000000000)

State 469 file input/c/c_seq/act_java.c line 589 function main thread 0
----------------------------------------------------
  dead=1 (00000000000000000000000000000001)

State 470 file input/c/c_seq/act_java.c line 592 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 472 file input/c/c_seq/act_java.c line 594 function main thread 0
----------------------------------------------------
  cs=32 (00000000000000000000000000100000)

State 512 file input/c/c_seq/act_java.c line 379 function t_two thread 0
----------------------------------------------------
  FullAllocation=10 (00000000000000000000000000001010)

State 516 file input/c/c_seq/act_java.c line 597 function main thread 0
----------------------------------------------------
  pc[2]=32 (00000000000000000000000000100000)

State 517 file input/c/c_seq/act_java.c line 598 function main thread 0
----------------------------------------------------
  active[2]=1 (00000000000000000000000000000001)

State 518 file input/c/c_seq/act_java.c line 599 function main thread 0
----------------------------------------------------
  dead=1 (00000000000000000000000000000001)

State 519 file input/c/c_seq/act_java.c line 602 function main thread 0
----------------------------------------------------
  ct=3 (00000000000000000000000000000011)

State 521 file input/c/c_seq/act_java.c line 604 function main thread 0
----------------------------------------------------
  cs=26 (00000000000000000000000000011010)

State 545 file input/c/c_seq/act_java.c line 465 function t_three thread 0
----------------------------------------------------
  assignCount=0 (00000000000000000000000000000000)

State 546 file input/c/c_seq/act_java.c line 466 function t_three thread 0
----------------------------------------------------
  taskInWait=0 (00000000000000000000000000000000)

State 549 file input/c/c_seq/act_java.c line 472 function t_three thread 0
----------------------------------------------------
  FullAllocation=3 (00000000000000000000000000000011)

State 551 file input/c/c_seq/act_java.c line 474 function t_three thread 0
----------------------------------------------------
  i=3 (00000000000000000000000000000011)

State 553 file input/c/c_seq/act_java.c line 476 function t_three thread 0
----------------------------------------------------
  MyClaim=3 (00000000000000000000000000000011)

State 555 file input/c/c_seq/act_java.c line 479 function t_three thread 0
----------------------------------------------------
  y=3 (00000000000000000000000000000011)

State 558 file input/c/c_seq/act_java.c line 491 function t_three thread 0
----------------------------------------------------
  stock=6 (00000000000000000000000000000110)

State 562 file input/c/c_seq/act_java.c line 507 function t_three thread 0
----------------------------------------------------
  FullAllocation=6 (00000000000000000000000000000110)

State 564 file input/c/c_seq/act_java.c line 509 function t_three thread 0
----------------------------------------------------
  i=4 (00000000000000000000000000000100)

State 566 file input/c/c_seq/act_java.c line 511 function t_three thread 0
----------------------------------------------------
  MyClaim=4 (00000000000000000000000000000100)

State 576 file input/c/c_seq/act_java.c line 607 function main thread 0
----------------------------------------------------
  pc[3]=26 (00000000000000000000000000011010)

State 577 file input/c/c_seq/act_java.c line 608 function main thread 0
----------------------------------------------------
  active[3]=1 (00000000000000000000000000000001)

State 578 file input/c/c_seq/act_java.c line 609 function main thread 0
----------------------------------------------------
  dead=1 (00000000000000000000000000000001)

State 582 file input/c/c_seq/act_java.c line 582 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 584 file input/c/c_seq/act_java.c line 592 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 586 file input/c/c_seq/act_java.c line 594 function main thread 0
----------------------------------------------------
  cs=32 (00000000000000000000000000100000)

State 629 file input/c/c_seq/act_java.c line 597 function main thread 0
----------------------------------------------------
  pc[2]=32 (00000000000000000000000000100000)

State 630 file input/c/c_seq/act_java.c line 598 function main thread 0
----------------------------------------------------
  active[2]=1 (00000000000000000000000000000001)

State 631 file input/c/c_seq/act_java.c line 599 function main thread 0
----------------------------------------------------
  dead=1 (00000000000000000000000000000001)

State 632 file input/c/c_seq/act_java.c line 602 function main thread 0
----------------------------------------------------
  ct=3 (00000000000000000000000000000011)

State 634 file input/c/c_seq/act_java.c line 604 function main thread 0
----------------------------------------------------
  cs=31 (00000000000000000000000000011111)

State 669 file input/c/c_seq/act_java.c line 514 function t_three thread 0
----------------------------------------------------
  y=4 (00000000000000000000000000000100)

State 672 file input/c/c_seq/act_java.c line 526 function t_three thread 0
----------------------------------------------------
  stock=2 (00000000000000000000000000000010)

State 679 file input/c/c_seq/act_java.c line 607 function main thread 0
----------------------------------------------------
  pc[3]=31 (00000000000000000000000000011111)

State 680 file input/c/c_seq/act_java.c line 608 function main thread 0
----------------------------------------------------
  active[3]=1 (00000000000000000000000000000001)

State 681 file input/c/c_seq/act_java.c line 609 function main thread 0
----------------------------------------------------
  dead=1 (00000000000000000000000000000001)

State 685 file input/c/c_seq/act_java.c line 582 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 687 file input/c/c_seq/act_java.c line 592 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 689 file input/c/c_seq/act_java.c line 594 function main thread 0
----------------------------------------------------
  cs=33 (00000000000000000000000000100001)

State 730 file input/c/c_seq/act_java.c line 384 function t_two thread 0
----------------------------------------------------
  y=10 (00000000000000000000000000001010)

State 731 file input/c/c_seq/act_java.c line 385 function t_two thread 0
----------------------------------------------------
  stock=12 (00000000000000000000000000001100)

State 734 file input/c/c_seq/act_java.c line 597 function main thread 0
----------------------------------------------------
  pc[2]=33 (00000000000000000000000000100001)

State 735 file input/c/c_seq/act_java.c line 598 function main thread 0
----------------------------------------------------
  active[2]=0 (00000000000000000000000000000000)

State 736 file input/c/c_seq/act_java.c line 599 function main thread 0
----------------------------------------------------
  dead=2 (00000000000000000000000000000010)

State 737 file input/c/c_seq/act_java.c line 602 function main thread 0
----------------------------------------------------
  ct=3 (00000000000000000000000000000011)

State 739 file input/c/c_seq/act_java.c line 604 function main thread 0
----------------------------------------------------
  cs=33 (00000000000000000000000000100001)

State 779 file input/c/c_seq/act_java.c line 542 function t_three thread 0
----------------------------------------------------
  FullAllocation=10 (00000000000000000000000000001010)

State 781 file input/c/c_seq/act_java.c line 547 function t_three thread 0
----------------------------------------------------
  y=10 (00000000000000000000000000001010)

State 782 file input/c/c_seq/act_java.c line 548 function t_three thread 0
----------------------------------------------------
  stock=22 (00000000000000000000000000010110)

State 785 file input/c/c_seq/act_java.c line 607 function main thread 0
----------------------------------------------------
  pc[3]=33 (00000000000000000000000000100001)

State 786 file input/c/c_seq/act_java.c line 608 function main thread 0
----------------------------------------------------
  active[3]=0 (00000000000000000000000000000000)

State 787 file input/c/c_seq/act_java.c line 609 function main thread 0
----------------------------------------------------
  dead=3 (00000000000000000000000000000011)

Violated property:
  file input/c/c_seq/act_java.c line 613 function main
  OK
  0 != 0

VERIFICATION FAILED

real	0m5.215s
user	0m5.069s
sys	0m0.128s
