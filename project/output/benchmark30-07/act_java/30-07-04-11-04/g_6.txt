CBMC version 5.4 64-bit x86_64 macos
Parsing input/c/c_seq/act_java.c
Converting
Type-checking act_java
file input/c/c_seq/act_java.c line 584 function main: function `nondet_uint' is not declared
Generating GOTO Program
Adding CPROVER library (x86_64)
Function Pointer Removal
Partial Inlining
Generic Property Instrumentation
Starting Bounded Model Checking
Unwinding loop main.0 iteration 1 (6 max) file input/c/c_seq/act_java.c line 581 function main thread 0
Unwinding loop main.0 iteration 2 (6 max) file input/c/c_seq/act_java.c line 581 function main thread 0
Unwinding loop main.0 iteration 3 (6 max) file input/c/c_seq/act_java.c line 581 function main thread 0
Unwinding loop main.0 iteration 4 (6 max) file input/c/c_seq/act_java.c line 581 function main thread 0
Unwinding loop main.0 iteration 5 (6 max) file input/c/c_seq/act_java.c line 581 function main thread 0
Not unwinding loop main.0 iteration 6 (6 max) file input/c/c_seq/act_java.c line 581 function main thread 0
size of program expression: 5784 steps
simple slicing removed 3 assignments
Generated 150 VCC(s), 150 remaining after simplification
Passing problem to propositional reduction
converting SSA
Running propositional reduction
Post-processing
Solving with Glucose Syrup with simplifier
179016 variables, 736197 clauses
SAT checker: instance is SATISFIABLE
Runtime decision procedure: 6.153s
Building error trace

Counterexample:

State 41 file input/c/c_seq/act_java.c line 560 function init thread 0
----------------------------------------------------
  pc[0]=0 (00000000000000000000000000000000)

State 42 file input/c/c_seq/act_java.c line 561 function init thread 0
----------------------------------------------------
  size[0]=0 (00000000000000000000000000000000)

State 43 file input/c/c_seq/act_java.c line 563 function init thread 0
----------------------------------------------------
  pc[1]=0 (00000000000000000000000000000000)

State 44 file input/c/c_seq/act_java.c line 564 function init thread 0
----------------------------------------------------
  pc[2]=0 (00000000000000000000000000000000)

State 45 file input/c/c_seq/act_java.c line 565 function init thread 0
----------------------------------------------------
  pc[3]=0 (00000000000000000000000000000000)

State 46 file input/c/c_seq/act_java.c line 566 function init thread 0
----------------------------------------------------
  ssize=0 (00000000000000000000000000000000)

State 47 file input/c/c_seq/act_java.c line 566 function init thread 0
----------------------------------------------------
  ssize=33 (00000000000000000000000000100001)

State 48 file input/c/c_seq/act_java.c line 567 function init thread 0
----------------------------------------------------
  size[1]=33 (00000000000000000000000000100001)

State 49 file input/c/c_seq/act_java.c line 568 function init thread 0
----------------------------------------------------
  size[2]=33 (00000000000000000000000000100001)

State 50 file input/c/c_seq/act_java.c line 569 function init thread 0
----------------------------------------------------
  size[3]=33 (00000000000000000000000000100001)

State 51 file input/c/c_seq/act_java.c line 571 function init thread 0
----------------------------------------------------
  t1=0 (00000000000000000000000000000000)

State 52 file input/c/c_seq/act_java.c line 571 function init thread 0
----------------------------------------------------
  t2=0 (00000000000000000000000000000000)

State 53 file input/c/c_seq/act_java.c line 571 function init thread 0
----------------------------------------------------
  t3=0 (00000000000000000000000000000000)

State 56 file input/c/c_seq/act_java.c line 573 function init thread 0
----------------------------------------------------
  t=0 (00000000000000000000000000000000)

State 57 file input/c/c_seq/act_java.c line 573 function init thread 0
----------------------------------------------------
  id=1 (00000000000000000000000000000001)

State 58 file input/c/c_seq/act_java.c line 21 function create thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 59 file input/c/c_seq/act_java.c line 22 function create thread 0
----------------------------------------------------
  t=1 (00000000000000000000000000000001)

State 63 file input/c/c_seq/act_java.c line 574 function init thread 0
----------------------------------------------------
  t=0 (00000000000000000000000000000000)

State 64 file input/c/c_seq/act_java.c line 574 function init thread 0
----------------------------------------------------
  id=2 (00000000000000000000000000000010)

State 65 file input/c/c_seq/act_java.c line 21 function create thread 0
----------------------------------------------------
  active[2]=1 (00000000000000000000000000000001)

State 66 file input/c/c_seq/act_java.c line 22 function create thread 0
----------------------------------------------------
  t=2 (00000000000000000000000000000010)

State 70 file input/c/c_seq/act_java.c line 575 function init thread 0
----------------------------------------------------
  t=0 (00000000000000000000000000000000)

State 71 file input/c/c_seq/act_java.c line 575 function init thread 0
----------------------------------------------------
  id=3 (00000000000000000000000000000011)

State 72 file input/c/c_seq/act_java.c line 21 function create thread 0
----------------------------------------------------
  active[3]=1 (00000000000000000000000000000001)

State 73 file input/c/c_seq/act_java.c line 22 function create thread 0
----------------------------------------------------
  t=3 (00000000000000000000000000000011)

State 76 file input/c/c_seq/act_java.c line 582 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 78 file input/c/c_seq/act_java.c line 584 function main thread 0
----------------------------------------------------
  cs=10 (00000000000000000000000000001010)

State 87 file input/c/c_seq/act_java.c line 77 function t_one thread 0
----------------------------------------------------
  i=1 (00000000000000000000000000000001)

State 89 file input/c/c_seq/act_java.c line 79 function t_one thread 0
----------------------------------------------------
  MyClaim=1 (00000000000000000000000000000001)

State 91 file input/c/c_seq/act_java.c line 82 function t_one thread 0
----------------------------------------------------
  y=1 (00000000000000000000000000000001)

State 94 file input/c/c_seq/act_java.c line 94 function t_one thread 0
----------------------------------------------------
  stock=21 (00000000000000000000000000010101)

State 98 file input/c/c_seq/act_java.c line 110 function t_one thread 0
----------------------------------------------------
  FullAllocation=1 (00000000000000000000000000000001)

State 100 file input/c/c_seq/act_java.c line 112 function t_one thread 0
----------------------------------------------------
  i=2 (00000000000000000000000000000010)

State 102 file input/c/c_seq/act_java.c line 114 function t_one thread 0
----------------------------------------------------
  MyClaim=2 (00000000000000000000000000000010)

State 128 file input/c/c_seq/act_java.c line 587 function main thread 0
----------------------------------------------------
  pc[1]=10 (00000000000000000000000000001010)

State 129 file input/c/c_seq/act_java.c line 588 function main thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 130 file input/c/c_seq/act_java.c line 589 function main thread 0
----------------------------------------------------
  dead=0 (00000000000000000000000000000000)

State 131 file input/c/c_seq/act_java.c line 592 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 133 file input/c/c_seq/act_java.c line 594 function main thread 0
----------------------------------------------------
  cs=31 (00000000000000000000000000011111)

State 142 file input/c/c_seq/act_java.c line 241 function t_two thread 0
----------------------------------------------------
  i=1 (00000000000000000000000000000001)

State 144 file input/c/c_seq/act_java.c line 243 function t_two thread 0
----------------------------------------------------
  MyClaim=1 (00000000000000000000000000000001)

State 146 file input/c/c_seq/act_java.c line 246 function t_two thread 0
----------------------------------------------------
  y=1 (00000000000000000000000000000001)

State 149 file input/c/c_seq/act_java.c line 258 function t_two thread 0
----------------------------------------------------
  stock=20 (00000000000000000000000000010100)

State 153 file input/c/c_seq/act_java.c line 274 function t_two thread 0
----------------------------------------------------
  FullAllocation=1 (00000000000000000000000000000001)

State 155 file input/c/c_seq/act_java.c line 276 function t_two thread 0
----------------------------------------------------
  i=2 (00000000000000000000000000000010)

State 157 file input/c/c_seq/act_java.c line 278 function t_two thread 0
----------------------------------------------------
  MyClaim=2 (00000000000000000000000000000010)

State 159 file input/c/c_seq/act_java.c line 281 function t_two thread 0
----------------------------------------------------
  y=2 (00000000000000000000000000000010)

State 162 file input/c/c_seq/act_java.c line 293 function t_two thread 0
----------------------------------------------------
  stock=18 (00000000000000000000000000010010)

State 166 file input/c/c_seq/act_java.c line 309 function t_two thread 0
----------------------------------------------------
  FullAllocation=3 (00000000000000000000000000000011)

State 168 file input/c/c_seq/act_java.c line 311 function t_two thread 0
----------------------------------------------------
  i=3 (00000000000000000000000000000011)

State 170 file input/c/c_seq/act_java.c line 313 function t_two thread 0
----------------------------------------------------
  MyClaim=3 (00000000000000000000000000000011)

State 172 file input/c/c_seq/act_java.c line 316 function t_two thread 0
----------------------------------------------------
  y=3 (00000000000000000000000000000011)

State 175 file input/c/c_seq/act_java.c line 328 function t_two thread 0
----------------------------------------------------
  stock=15 (00000000000000000000000000001111)

State 179 file input/c/c_seq/act_java.c line 344 function t_two thread 0
----------------------------------------------------
  FullAllocation=6 (00000000000000000000000000000110)

State 181 file input/c/c_seq/act_java.c line 346 function t_two thread 0
----------------------------------------------------
  i=4 (00000000000000000000000000000100)

State 183 file input/c/c_seq/act_java.c line 348 function t_two thread 0
----------------------------------------------------
  MyClaim=4 (00000000000000000000000000000100)

State 185 file input/c/c_seq/act_java.c line 351 function t_two thread 0
----------------------------------------------------
  y=4 (00000000000000000000000000000100)

State 188 file input/c/c_seq/act_java.c line 363 function t_two thread 0
----------------------------------------------------
  stock=11 (00000000000000000000000000001011)

State 195 file input/c/c_seq/act_java.c line 597 function main thread 0
----------------------------------------------------
  pc[2]=31 (00000000000000000000000000011111)

State 196 file input/c/c_seq/act_java.c line 598 function main thread 0
----------------------------------------------------
  active[2]=1 (00000000000000000000000000000001)

State 197 file input/c/c_seq/act_java.c line 599 function main thread 0
----------------------------------------------------
  dead=0 (00000000000000000000000000000000)

State 198 file input/c/c_seq/act_java.c line 602 function main thread 0
----------------------------------------------------
  ct=3 (00000000000000000000000000000011)

State 200 file input/c/c_seq/act_java.c line 604 function main thread 0
----------------------------------------------------
  cs=10 (00000000000000000000000000001010)

State 209 file input/c/c_seq/act_java.c line 404 function t_three thread 0
----------------------------------------------------
  i=1 (00000000000000000000000000000001)

State 211 file input/c/c_seq/act_java.c line 406 function t_three thread 0
----------------------------------------------------
  MyClaim=1 (00000000000000000000000000000001)

State 213 file input/c/c_seq/act_java.c line 409 function t_three thread 0
----------------------------------------------------
  y=1 (00000000000000000000000000000001)

State 216 file input/c/c_seq/act_java.c line 421 function t_three thread 0
----------------------------------------------------
  stock=10 (00000000000000000000000000001010)

State 220 file input/c/c_seq/act_java.c line 437 function t_three thread 0
----------------------------------------------------
  FullAllocation=1 (00000000000000000000000000000001)

State 222 file input/c/c_seq/act_java.c line 439 function t_three thread 0
----------------------------------------------------
  i=2 (00000000000000000000000000000010)

State 224 file input/c/c_seq/act_java.c line 441 function t_three thread 0
----------------------------------------------------
  MyClaim=2 (00000000000000000000000000000010)

State 250 file input/c/c_seq/act_java.c line 607 function main thread 0
----------------------------------------------------
  pc[3]=10 (00000000000000000000000000001010)

State 251 file input/c/c_seq/act_java.c line 608 function main thread 0
----------------------------------------------------
  active[3]=1 (00000000000000000000000000000001)

State 252 file input/c/c_seq/act_java.c line 609 function main thread 0
----------------------------------------------------
  dead=0 (00000000000000000000000000000000)

State 255 file input/c/c_seq/act_java.c line 582 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 257 file input/c/c_seq/act_java.c line 584 function main thread 0
----------------------------------------------------
  cs=33 (00000000000000000000000000100001)

State 276 file input/c/c_seq/act_java.c line 117 function t_one thread 0
----------------------------------------------------
  y=2 (00000000000000000000000000000010)

State 279 file input/c/c_seq/act_java.c line 129 function t_one thread 0
----------------------------------------------------
  stock=8 (00000000000000000000000000001000)

State 283 file input/c/c_seq/act_java.c line 145 function t_one thread 0
----------------------------------------------------
  FullAllocation=3 (00000000000000000000000000000011)

State 285 file input/c/c_seq/act_java.c line 147 function t_one thread 0
----------------------------------------------------
  i=3 (00000000000000000000000000000011)

State 287 file input/c/c_seq/act_java.c line 149 function t_one thread 0
----------------------------------------------------
  MyClaim=3 (00000000000000000000000000000011)

State 289 file input/c/c_seq/act_java.c line 152 function t_one thread 0
----------------------------------------------------
  y=3 (00000000000000000000000000000011)

State 292 file input/c/c_seq/act_java.c line 164 function t_one thread 0
----------------------------------------------------
  stock=5 (00000000000000000000000000000101)

State 296 file input/c/c_seq/act_java.c line 180 function t_one thread 0
----------------------------------------------------
  FullAllocation=6 (00000000000000000000000000000110)

State 298 file input/c/c_seq/act_java.c line 182 function t_one thread 0
----------------------------------------------------
  i=4 (00000000000000000000000000000100)

State 300 file input/c/c_seq/act_java.c line 184 function t_one thread 0
----------------------------------------------------
  MyClaim=4 (00000000000000000000000000000100)

State 302 file input/c/c_seq/act_java.c line 187 function t_one thread 0
----------------------------------------------------
  y=4 (00000000000000000000000000000100)

State 305 file input/c/c_seq/act_java.c line 199 function t_one thread 0
----------------------------------------------------
  stock=1 (00000000000000000000000000000001)

State 309 file input/c/c_seq/act_java.c line 215 function t_one thread 0
----------------------------------------------------
  FullAllocation=10 (00000000000000000000000000001010)

State 311 file input/c/c_seq/act_java.c line 220 function t_one thread 0
----------------------------------------------------
  y=10 (00000000000000000000000000001010)

State 312 file input/c/c_seq/act_java.c line 221 function t_one thread 0
----------------------------------------------------
  stock=11 (00000000000000000000000000001011)

State 315 file input/c/c_seq/act_java.c line 587 function main thread 0
----------------------------------------------------
  pc[1]=33 (00000000000000000000000000100001)

State 316 file input/c/c_seq/act_java.c line 588 function main thread 0
----------------------------------------------------
  active[1]=0 (00000000000000000000000000000000)

State 317 file input/c/c_seq/act_java.c line 589 function main thread 0
----------------------------------------------------
  dead=1 (00000000000000000000000000000001)

State 318 file input/c/c_seq/act_java.c line 592 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 320 file input/c/c_seq/act_java.c line 594 function main thread 0
----------------------------------------------------
  cs=33 (00000000000000000000000000100001)

State 360 file input/c/c_seq/act_java.c line 379 function t_two thread 0
----------------------------------------------------
  FullAllocation=10 (00000000000000000000000000001010)

State 362 file input/c/c_seq/act_java.c line 384 function t_two thread 0
----------------------------------------------------
  y=10 (00000000000000000000000000001010)

State 363 file input/c/c_seq/act_java.c line 385 function t_two thread 0
----------------------------------------------------
  stock=21 (00000000000000000000000000010101)

State 366 file input/c/c_seq/act_java.c line 597 function main thread 0
----------------------------------------------------
  pc[2]=33 (00000000000000000000000000100001)

State 367 file input/c/c_seq/act_java.c line 598 function main thread 0
----------------------------------------------------
  active[2]=0 (00000000000000000000000000000000)

State 368 file input/c/c_seq/act_java.c line 599 function main thread 0
----------------------------------------------------
  dead=2 (00000000000000000000000000000010)

State 369 file input/c/c_seq/act_java.c line 602 function main thread 0
----------------------------------------------------
  ct=3 (00000000000000000000000000000011)

State 371 file input/c/c_seq/act_java.c line 604 function main thread 0
----------------------------------------------------
  cs=33 (00000000000000000000000000100001)

State 390 file input/c/c_seq/act_java.c line 444 function t_three thread 0
----------------------------------------------------
  y=2 (00000000000000000000000000000010)

State 393 file input/c/c_seq/act_java.c line 456 function t_three thread 0
----------------------------------------------------
  stock=19 (00000000000000000000000000010011)

State 397 file input/c/c_seq/act_java.c line 472 function t_three thread 0
----------------------------------------------------
  FullAllocation=3 (00000000000000000000000000000011)

State 399 file input/c/c_seq/act_java.c line 474 function t_three thread 0
----------------------------------------------------
  i=3 (00000000000000000000000000000011)

State 401 file input/c/c_seq/act_java.c line 476 function t_three thread 0
----------------------------------------------------
  MyClaim=3 (00000000000000000000000000000011)

State 403 file input/c/c_seq/act_java.c line 479 function t_three thread 0
----------------------------------------------------
  y=3 (00000000000000000000000000000011)

State 406 file input/c/c_seq/act_java.c line 491 function t_three thread 0
----------------------------------------------------
  stock=16 (00000000000000000000000000010000)

State 410 file input/c/c_seq/act_java.c line 507 function t_three thread 0
----------------------------------------------------
  FullAllocation=6 (00000000000000000000000000000110)

State 412 file input/c/c_seq/act_java.c line 509 function t_three thread 0
----------------------------------------------------
  i=4 (00000000000000000000000000000100)

State 414 file input/c/c_seq/act_java.c line 511 function t_three thread 0
----------------------------------------------------
  MyClaim=4 (00000000000000000000000000000100)

State 416 file input/c/c_seq/act_java.c line 514 function t_three thread 0
----------------------------------------------------
  y=4 (00000000000000000000000000000100)

State 419 file input/c/c_seq/act_java.c line 526 function t_three thread 0
----------------------------------------------------
  stock=12 (00000000000000000000000000001100)

State 423 file input/c/c_seq/act_java.c line 542 function t_three thread 0
----------------------------------------------------
  FullAllocation=10 (00000000000000000000000000001010)

State 425 file input/c/c_seq/act_java.c line 547 function t_three thread 0
----------------------------------------------------
  y=10 (00000000000000000000000000001010)

State 426 file input/c/c_seq/act_java.c line 548 function t_three thread 0
----------------------------------------------------
  stock=22 (00000000000000000000000000010110)

State 429 file input/c/c_seq/act_java.c line 607 function main thread 0
----------------------------------------------------
  pc[3]=33 (00000000000000000000000000100001)

State 430 file input/c/c_seq/act_java.c line 608 function main thread 0
----------------------------------------------------
  active[3]=0 (00000000000000000000000000000000)

State 431 file input/c/c_seq/act_java.c line 609 function main thread 0
----------------------------------------------------
  dead=3 (00000000000000000000000000000011)

Violated property:
  file input/c/c_seq/act_java.c line 613 function main
  OK
  0 != 0

VERIFICATION FAILED

real	0m8.227s
user	0m8.087s
sys	0m0.122s
