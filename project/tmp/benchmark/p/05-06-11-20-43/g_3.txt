CBMC version 5.4 64-bit x86_64 linux
Parsing input/c/c_seq/p.c
Converting
Type-checking p
file input/c/c_seq/p.c line 114 function main: function `nondet_uint' is not declared
Generating GOTO Program
Adding CPROVER library (x86_64)
Function Pointer Removal
Partial Inlining
Generic Property Instrumentation
Starting Bounded Model Checking
Unwinding loop main.0 iteration 1 (4 max) file input/c/c_seq/p.c line 111 function main thread 0
Unwinding loop main.0 iteration 2 (4 max) file input/c/c_seq/p.c line 111 function main thread 0
Unwinding loop main.0 iteration 3 (4 max) file input/c/c_seq/p.c line 111 function main thread 0
Not unwinding loop main.0 iteration 4 (4 max) file input/c/c_seq/p.c line 111 function main thread 0
size of program expression: 765 steps
simple slicing removed 3 assignments
Generated 3 VCC(s), 3 remaining after simplification
Passing problem to propositional reduction
converting SSA
Running propositional reduction
Post-processing
Solving with Glucose Syrup with simplifier
9672 variables, 36322 clauses
SAT checker: instance is SATISFIABLE
Runtime decision procedure: 0.102s
Building error trace

Counterexample:

State 34 file input/c/c_seq/p.c line 94 function init thread 0
----------------------------------------------------
  pc[0]=0 (00000000000000000000000000000000)

State 35 file input/c/c_seq/p.c line 95 function init thread 0
----------------------------------------------------
  size[0]=0 (00000000000000000000000000000000)

State 36 file input/c/c_seq/p.c line 97 function init thread 0
----------------------------------------------------
  pc[1]=0 (00000000000000000000000000000000)

State 37 file input/c/c_seq/p.c line 98 function init thread 0
----------------------------------------------------
  pc[2]=0 (00000000000000000000000000000000)

State 38 file input/c/c_seq/p.c line 99 function init thread 0
----------------------------------------------------
  size[1]=11 (00000000000000000000000000001011)

State 39 file input/c/c_seq/p.c line 100 function init thread 0
----------------------------------------------------
  size[2]=11 (00000000000000000000000000001011)

State 40 file input/c/c_seq/p.c line 102 function init thread 0
----------------------------------------------------
  t1=0 (00000000000000000000000000000000)

State 41 file input/c/c_seq/p.c line 102 function init thread 0
----------------------------------------------------
  t2=0 (00000000000000000000000000000000)

State 42 file input/c/c_seq/p.c line 102 function init thread 0
----------------------------------------------------
  t3=0 (00000000000000000000000000000000)

State 43 file input/c/c_seq/p.c line 102 function init thread 0
----------------------------------------------------
  t4=0 (00000000000000000000000000000000)

State 44 file input/c/c_seq/p.c line 102 function init thread 0
----------------------------------------------------
  t5=0 (00000000000000000000000000000000)

State 45 file input/c/c_seq/p.c line 102 function init thread 0
----------------------------------------------------
  t6=0 (00000000000000000000000000000000)

State 48 file input/c/c_seq/p.c line 104 function init thread 0
----------------------------------------------------
  t=0 (00000000000000000000000000000000)

State 49 file input/c/c_seq/p.c line 104 function init thread 0
----------------------------------------------------
  id=1 (00000000000000000000000000000001)

State 50 file input/c/c_seq/p.c line 20 function create thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 51 file input/c/c_seq/p.c line 21 function create thread 0
----------------------------------------------------
  t=1 (00000000000000000000000000000001)

State 55 file input/c/c_seq/p.c line 105 function init thread 0
----------------------------------------------------
  t=0 (00000000000000000000000000000000)

State 56 file input/c/c_seq/p.c line 105 function init thread 0
----------------------------------------------------
  id=2 (00000000000000000000000000000010)

State 57 file input/c/c_seq/p.c line 20 function create thread 0
----------------------------------------------------
  active[2]=1 (00000000000000000000000000000001)

State 58 file input/c/c_seq/p.c line 21 function create thread 0
----------------------------------------------------
  t=2 (00000000000000000000000000000010)

State 61 file input/c/c_seq/p.c line 112 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 63 file input/c/c_seq/p.c line 114 function main thread 0
----------------------------------------------------
  cs=4 (00000000000000000000000000000100)

State 74 file input/c/c_seq/p.c line 37 function t_one thread 0
----------------------------------------------------
  other=2 (00000000000000000000000000000010)

State 76 file input/c/c_seq/p.c line 39 function t_one thread 0
----------------------------------------------------
  flag[1]=1 (00000000000000000000000000000001)

State 86 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  pc[1]=4 (00000000000000000000000000000100)

State 87 file input/c/c_seq/p.c line 118 function main thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 88 file input/c/c_seq/p.c line 121 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 90 file input/c/c_seq/p.c line 123 function main thread 0
----------------------------------------------------
  cs=2 (00000000000000000000000000000010)

State 111 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  pc[2]=2 (00000000000000000000000000000010)

State 112 file input/c/c_seq/p.c line 127 function main thread 0
----------------------------------------------------
  active[2]=1 (00000000000000000000000000000001)

State 115 file input/c/c_seq/p.c line 112 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 117 file input/c/c_seq/p.c line 114 function main thread 0
----------------------------------------------------
  cs=7 (00000000000000000000000000000111)

State 130 file input/c/c_seq/p.c line 42 function t_one thread 0
----------------------------------------------------
  turn=2 (00000000000000000000000000000010)

State 140 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  pc[1]=7 (00000000000000000000000000000111)

State 141 file input/c/c_seq/p.c line 118 function main thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 142 file input/c/c_seq/p.c line 121 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 144 file input/c/c_seq/p.c line 123 function main thread 0
----------------------------------------------------
  cs=2 (00000000000000000000000000000010)

State 165 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  pc[2]=2 (00000000000000000000000000000010)

State 166 file input/c/c_seq/p.c line 127 function main thread 0
----------------------------------------------------
  active[2]=1 (00000000000000000000000000000001)

State 169 file input/c/c_seq/p.c line 112 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 171 file input/c/c_seq/p.c line 114 function main thread 0
----------------------------------------------------
  cs=11 (00000000000000000000000000001011)

State 187 file input/c/c_seq/p.c line 50 function t_one thread 0
----------------------------------------------------
  ax=0 (00000000000000000000000000000000)

State 189 file input/c/c_seq/p.c line 52 function t_one thread 0
----------------------------------------------------
  ax=1 (00000000000000000000000000000001)

State 191 file input/c/c_seq/p.c line 54 function t_one thread 0
----------------------------------------------------
  x=1 (00000000000000000000000000000001)

State 193 file input/c/c_seq/p.c line 57 function t_one thread 0
----------------------------------------------------
  flag[1]=0 (00000000000000000000000000000000)

State 196 file input/c/c_seq/p.c line 117 function main thread 0
----------------------------------------------------
  pc[1]=11 (00000000000000000000000000001011)

State 197 file input/c/c_seq/p.c line 118 function main thread 0
----------------------------------------------------
  active[1]=0 (00000000000000000000000000000000)

State 198 file input/c/c_seq/p.c line 121 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 200 file input/c/c_seq/p.c line 123 function main thread 0
----------------------------------------------------
  cs=2 (00000000000000000000000000000010)

State 221 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  pc[2]=2 (00000000000000000000000000000010)

State 222 file input/c/c_seq/p.c line 127 function main thread 0
----------------------------------------------------
  active[2]=1 (00000000000000000000000000000001)

State 226 file input/c/c_seq/p.c line 112 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 228 file input/c/c_seq/p.c line 121 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 230 file input/c/c_seq/p.c line 123 function main thread 0
----------------------------------------------------
  cs=11 (00000000000000000000000000001011)

State 241 file input/c/c_seq/p.c line 68 function t_two thread 0
----------------------------------------------------
  other=1 (00000000000000000000000000000001)

State 243 file input/c/c_seq/p.c line 70 function t_two thread 0
----------------------------------------------------
  flag[2]=1 (00000000000000000000000000000001)

State 245 file input/c/c_seq/p.c line 73 function t_two thread 0
----------------------------------------------------
  turn=1 (00000000000000000000000000000001)

State 250 file input/c/c_seq/p.c line 81 function t_two thread 0
----------------------------------------------------
  bx=1 (00000000000000000000000000000001)

State 252 file input/c/c_seq/p.c line 83 function t_two thread 0
----------------------------------------------------
  bx=0 (00000000000000000000000000000000)

State 254 file input/c/c_seq/p.c line 85 function t_two thread 0
----------------------------------------------------
  x=0 (00000000000000000000000000000000)

State 256 file input/c/c_seq/p.c line 88 function t_two thread 0
----------------------------------------------------
  flag[2]=0 (00000000000000000000000000000000)

State 259 file input/c/c_seq/p.c line 126 function main thread 0
----------------------------------------------------
  pc[2]=11 (00000000000000000000000000001011)

State 260 file input/c/c_seq/p.c line 127 function main thread 0
----------------------------------------------------
  active[2]=0 (00000000000000000000000000000000)

Violated property:
  file input/c/c_seq/p.c line 137 function main
  OK
  0 != 0

VERIFICATION FAILED

real	0m0.324s
user	0m0.270s
sys	0m0.030s
