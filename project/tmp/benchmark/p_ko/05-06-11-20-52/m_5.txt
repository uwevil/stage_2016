CBMC version 5.4 64-bit x86_64 linux
Parsing input/c/c_seq/p_ko.c
Converting
Type-checking p_ko
file input/c/c_seq/p_ko.c line 114 function main: function `nondet_uint' is not declared
Generating GOTO Program
Adding CPROVER library (x86_64)
Function Pointer Removal
Partial Inlining
Generic Property Instrumentation
Starting Bounded Model Checking
Unwinding loop main.0 iteration 1 (6 max) file input/c/c_seq/p_ko.c line 111 function main thread 0
Unwinding loop main.0 iteration 2 (6 max) file input/c/c_seq/p_ko.c line 111 function main thread 0
Unwinding loop main.0 iteration 3 (6 max) file input/c/c_seq/p_ko.c line 111 function main thread 0
Unwinding loop main.0 iteration 4 (6 max) file input/c/c_seq/p_ko.c line 111 function main thread 0
Unwinding loop main.0 iteration 5 (6 max) file input/c/c_seq/p_ko.c line 111 function main thread 0
Not unwinding loop main.0 iteration 6 (6 max) file input/c/c_seq/p_ko.c line 111 function main thread 0
size of program expression: 1105 steps
simple slicing removed 3 assignments
Generated 3 VCC(s), 3 remaining after simplification
Passing problem to propositional reduction
converting SSA
Running propositional reduction
Post-processing
Solving with MiniSAT 2.2.1 with simplifier
15274 variables, 58319 clauses
SAT checker: instance is SATISFIABLE
Runtime decision procedure: 0.152s
Building error trace

Counterexample:

State 34 file input/c/c_seq/p_ko.c line 94 function init thread 0
----------------------------------------------------
  pc[0]=0 (00000000000000000000000000000000)

State 35 file input/c/c_seq/p_ko.c line 95 function init thread 0
----------------------------------------------------
  size[0]=0 (00000000000000000000000000000000)

State 36 file input/c/c_seq/p_ko.c line 97 function init thread 0
----------------------------------------------------
  pc[1]=0 (00000000000000000000000000000000)

State 37 file input/c/c_seq/p_ko.c line 98 function init thread 0
----------------------------------------------------
  pc[2]=0 (00000000000000000000000000000000)

State 38 file input/c/c_seq/p_ko.c line 99 function init thread 0
----------------------------------------------------
  size[1]=11 (00000000000000000000000000001011)

State 39 file input/c/c_seq/p_ko.c line 100 function init thread 0
----------------------------------------------------
  size[2]=11 (00000000000000000000000000001011)

State 40 file input/c/c_seq/p_ko.c line 102 function init thread 0
----------------------------------------------------
  t1=0 (00000000000000000000000000000000)

State 41 file input/c/c_seq/p_ko.c line 102 function init thread 0
----------------------------------------------------
  t2=0 (00000000000000000000000000000000)

State 42 file input/c/c_seq/p_ko.c line 102 function init thread 0
----------------------------------------------------
  t3=0 (00000000000000000000000000000000)

State 43 file input/c/c_seq/p_ko.c line 102 function init thread 0
----------------------------------------------------
  t4=0 (00000000000000000000000000000000)

State 44 file input/c/c_seq/p_ko.c line 102 function init thread 0
----------------------------------------------------
  t5=0 (00000000000000000000000000000000)

State 45 file input/c/c_seq/p_ko.c line 102 function init thread 0
----------------------------------------------------
  t6=0 (00000000000000000000000000000000)

State 48 file input/c/c_seq/p_ko.c line 104 function init thread 0
----------------------------------------------------
  t=0 (00000000000000000000000000000000)

State 49 file input/c/c_seq/p_ko.c line 104 function init thread 0
----------------------------------------------------
  id=1 (00000000000000000000000000000001)

State 50 file input/c/c_seq/p_ko.c line 20 function create thread 0
----------------------------------------------------
  active[1]=1 (00000000000000000000000000000001)

State 51 file input/c/c_seq/p_ko.c line 21 function create thread 0
----------------------------------------------------
  t=1 (00000000000000000000000000000001)

State 55 file input/c/c_seq/p_ko.c line 105 function init thread 0
----------------------------------------------------
  t=0 (00000000000000000000000000000000)

State 56 file input/c/c_seq/p_ko.c line 105 function init thread 0
----------------------------------------------------
  id=2 (00000000000000000000000000000010)

State 57 file input/c/c_seq/p_ko.c line 20 function create thread 0
----------------------------------------------------
  active[2]=1 (00000000000000000000000000000001)

State 58 file input/c/c_seq/p_ko.c line 21 function create thread 0
----------------------------------------------------
  t=2 (00000000000000000000000000000010)

State 61 file input/c/c_seq/p_ko.c line 112 function main thread 0
----------------------------------------------------
  ct=1 (00000000000000000000000000000001)

State 63 file input/c/c_seq/p_ko.c line 114 function main thread 0
----------------------------------------------------
  cs=11 (00000000000000000000000000001011)

State 74 file input/c/c_seq/p_ko.c line 37 function t_one thread 0
----------------------------------------------------
  other=2 (00000000000000000000000000000010)

State 76 file input/c/c_seq/p_ko.c line 39 function t_one thread 0
----------------------------------------------------
  flag[1]=1 (00000000000000000000000000000001)

State 78 file input/c/c_seq/p_ko.c line 42 function t_one thread 0
----------------------------------------------------
  turn=2 (00000000000000000000000000000010)

State 83 file input/c/c_seq/p_ko.c line 50 function t_one thread 0
----------------------------------------------------
  ax=-1 (11111111111111111111111111111111)

State 85 file input/c/c_seq/p_ko.c line 52 function t_one thread 0
----------------------------------------------------
  ax=0 (00000000000000000000000000000000)

State 87 file input/c/c_seq/p_ko.c line 54 function t_one thread 0
----------------------------------------------------
  x=0 (00000000000000000000000000000000)

State 89 file input/c/c_seq/p_ko.c line 57 function t_one thread 0
----------------------------------------------------
  flag[1]=0 (00000000000000000000000000000000)

State 92 file input/c/c_seq/p_ko.c line 117 function main thread 0
----------------------------------------------------
  pc[1]=11 (00000000000000000000000000001011)

State 93 file input/c/c_seq/p_ko.c line 118 function main thread 0
----------------------------------------------------
  active[1]=0 (00000000000000000000000000000000)

State 94 file input/c/c_seq/p_ko.c line 121 function main thread 0
----------------------------------------------------
  ct=2 (00000000000000000000000000000010)

State 96 file input/c/c_seq/p_ko.c line 123 function main thread 0
----------------------------------------------------
  cs=11 (00000000000000000000000000001011)

State 107 file input/c/c_seq/p_ko.c line 68 function t_two thread 0
----------------------------------------------------
  other=1 (00000000000000000000000000000001)

State 109 file input/c/c_seq/p_ko.c line 70 function t_two thread 0
----------------------------------------------------
  flag[2]=1 (00000000000000000000000000000001)

State 111 file input/c/c_seq/p_ko.c line 73 function t_two thread 0
----------------------------------------------------
  turn=1 (00000000000000000000000000000001)

State 116 file input/c/c_seq/p_ko.c line 81 function t_two thread 0
----------------------------------------------------
  bx=0 (00000000000000000000000000000000)

State 118 file input/c/c_seq/p_ko.c line 83 function t_two thread 0
----------------------------------------------------
  bx=-1 (11111111111111111111111111111111)

State 120 file input/c/c_seq/p_ko.c line 85 function t_two thread 0
----------------------------------------------------
  x=-1 (11111111111111111111111111111111)

State 122 file input/c/c_seq/p_ko.c line 88 function t_two thread 0
----------------------------------------------------
  flag[2]=0 (00000000000000000000000000000000)

State 125 file input/c/c_seq/p_ko.c line 126 function main thread 0
----------------------------------------------------
  pc[2]=11 (00000000000000000000000000001011)

State 126 file input/c/c_seq/p_ko.c line 127 function main thread 0
----------------------------------------------------
  active[2]=0 (00000000000000000000000000000000)

Violated property:
  file input/c/c_seq/p_ko.c line 136 function main
  OK x != 0
  x == 0

VERIFICATION FAILED

real	0m0.388s
user	0m0.370s
sys	0m0.000s
