/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.3"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0

/* Substitute the variable and function names.  */
#define yyparse yyansi_cparse
#define yylex   yyansi_clex
#define yyerror yyansi_cerror
#define yylval  yyansi_clval
#define yychar  yyansi_cchar
#define yydebug yyansi_cdebug
#define yynerrs yyansi_cnerrs


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     TOK_AUTO = 258,
     TOK_BOOL = 259,
     TOK_COMPLEX = 260,
     TOK_BREAK = 261,
     TOK_CASE = 262,
     TOK_CHAR = 263,
     TOK_CONST = 264,
     TOK_CONTINUE = 265,
     TOK_DEFAULT = 266,
     TOK_DO = 267,
     TOK_DOUBLE = 268,
     TOK_ELSE = 269,
     TOK_ENUM = 270,
     TOK_EXTERN = 271,
     TOK_FLOAT = 272,
     TOK_FOR = 273,
     TOK_GOTO = 274,
     TOK_IF = 275,
     TOK_INLINE = 276,
     TOK_INT = 277,
     TOK_LONG = 278,
     TOK_REGISTER = 279,
     TOK_RESTRICT = 280,
     TOK_RETURN = 281,
     TOK_SHORT = 282,
     TOK_SIGNED = 283,
     TOK_SIZEOF = 284,
     TOK_STATIC = 285,
     TOK_STRUCT = 286,
     TOK_SWITCH = 287,
     TOK_TYPEDEF = 288,
     TOK_UNION = 289,
     TOK_UNSIGNED = 290,
     TOK_VOID = 291,
     TOK_VOLATILE = 292,
     TOK_WCHAR_T = 293,
     TOK_WHILE = 294,
     TOK_ARROW = 295,
     TOK_INCR = 296,
     TOK_DECR = 297,
     TOK_SHIFTLEFT = 298,
     TOK_SHIFTRIGHT = 299,
     TOK_LE = 300,
     TOK_GE = 301,
     TOK_EQ = 302,
     TOK_NE = 303,
     TOK_ANDAND = 304,
     TOK_OROR = 305,
     TOK_ELLIPSIS = 306,
     TOK_MULTASSIGN = 307,
     TOK_DIVASSIGN = 308,
     TOK_MODASSIGN = 309,
     TOK_PLUSASSIGN = 310,
     TOK_MINUSASSIGN = 311,
     TOK_SHLASSIGN = 312,
     TOK_SHRASSIGN = 313,
     TOK_ANDASSIGN = 314,
     TOK_XORASSIGN = 315,
     TOK_ORASSIGN = 316,
     TOK_IDENTIFIER = 317,
     TOK_TYPEDEFNAME = 318,
     TOK_INTEGER = 319,
     TOK_FLOATING = 320,
     TOK_CHARACTER = 321,
     TOK_STRING = 322,
     TOK_ASM_STRING = 323,
     TOK_INT8 = 324,
     TOK_INT16 = 325,
     TOK_INT32 = 326,
     TOK_INT64 = 327,
     TOK_PTR32 = 328,
     TOK_PTR64 = 329,
     TOK_TYPEOF = 330,
     TOK_GCC_AUTO_TYPE = 331,
     TOK_GCC_FLOAT80 = 332,
     TOK_GCC_FLOAT128 = 333,
     TOK_GCC_INT128 = 334,
     TOK_GCC_DECIMAL32 = 335,
     TOK_GCC_DECIMAL64 = 336,
     TOK_GCC_DECIMAL128 = 337,
     TOK_GCC_ASM = 338,
     TOK_GCC_ASM_PAREN = 339,
     TOK_GCC_ATTRIBUTE = 340,
     TOK_GCC_ATTRIBUTE_ALIGNED = 341,
     TOK_GCC_ATTRIBUTE_TRANSPARENT_UNION = 342,
     TOK_GCC_ATTRIBUTE_PACKED = 343,
     TOK_GCC_ATTRIBUTE_VECTOR_SIZE = 344,
     TOK_GCC_ATTRIBUTE_MODE = 345,
     TOK_GCC_ATTRIBUTE_GNU_INLINE = 346,
     TOK_GCC_ATTRIBUTE_END = 347,
     TOK_GCC_LABEL = 348,
     TOK_MSC_ASM = 349,
     TOK_MSC_BASED = 350,
     TOK_CW_VAR_ARG_TYPEOF = 351,
     TOK_BUILTIN_VA_ARG = 352,
     TOK_GCC_BUILTIN_TYPES_COMPATIBLE_P = 353,
     TOK_OFFSETOF = 354,
     TOK_ALIGNOF = 355,
     TOK_MSC_TRY = 356,
     TOK_MSC_FINALLY = 357,
     TOK_MSC_EXCEPT = 358,
     TOK_MSC_LEAVE = 359,
     TOK_MSC_DECLSPEC = 360,
     TOK_INTERFACE = 361,
     TOK_CDECL = 362,
     TOK_STDCALL = 363,
     TOK_FASTCALL = 364,
     TOK_CLRCALL = 365,
     TOK_FORALL = 366,
     TOK_EXISTS = 367,
     TOK_ACSL_FORALL = 368,
     TOK_ACSL_EXISTS = 369,
     TOK_ARRAY_OF = 370,
     TOK_CPROVER_BITVECTOR = 371,
     TOK_CPROVER_FLOATBV = 372,
     TOK_CPROVER_FIXEDBV = 373,
     TOK_CPROVER_ATOMIC = 374,
     TOK_CPROVER_BOOL = 375,
     TOK_CPROVER_THROW = 376,
     TOK_CPROVER_CATCH = 377,
     TOK_CPROVER_TRY = 378,
     TOK_CPROVER_FINALLY = 379,
     TOK_CPROVER_ID = 380,
     TOK_IMPLIES = 381,
     TOK_EQUIVALENT = 382,
     TOK_TRUE = 383,
     TOK_FALSE = 384,
     TOK_REAL = 385,
     TOK_IMAG = 386,
     TOK_ALIGNAS = 387,
     TOK_ATOMIC_TYPE_QUALIFIER = 388,
     TOK_ATOMIC_TYPE_SPECIFIER = 389,
     TOK_GENERIC = 390,
     TOK_IMAGINARY = 391,
     TOK_NORETURN = 392,
     TOK_STATIC_ASSERT = 393,
     TOK_THREAD_LOCAL = 394,
     TOK_NULLPTR = 395,
     TOK_CONSTEXPR = 396,
     TOK_SCANNER_ERROR = 397,
     TOK_SCANNER_EOF = 398,
     TOK_CATCH = 399,
     TOK_CHAR16_T = 400,
     TOK_CHAR32_T = 401,
     TOK_CLASS = 402,
     TOK_DELETE = 403,
     TOK_DECLTYPE = 404,
     TOK_EXPLICIT = 405,
     TOK_FRIEND = 406,
     TOK_MUTABLE = 407,
     TOK_NAMESPACE = 408,
     TOK_NEW = 409,
     TOK_NOEXCEPT = 410,
     TOK_OPERATOR = 411,
     TOK_PRIVATE = 412,
     TOK_PROTECTED = 413,
     TOK_PUBLIC = 414,
     TOK_TEMPLATE = 415,
     TOK_THIS = 416,
     TOK_THROW = 417,
     TOK_TYPEID = 418,
     TOK_TYPENAME = 419,
     TOK_TRY = 420,
     TOK_USING = 421,
     TOK_VIRTUAL = 422,
     TOK_SCOPE = 423,
     TOK_DOTPM = 424,
     TOK_ARROWPM = 425,
     TOK_UNARY_TYPE_PREDICATE = 426,
     TOK_BINARY_TYPE_PREDICATE = 427,
     TOK_MSC_UUIDOF = 428,
     TOK_MSC_IF_EXISTS = 429,
     TOK_MSC_IF_NOT_EXISTS = 430,
     TOK_UNDERLYING_TYPE = 431
   };
#endif
/* Tokens.  */
#define TOK_AUTO 258
#define TOK_BOOL 259
#define TOK_COMPLEX 260
#define TOK_BREAK 261
#define TOK_CASE 262
#define TOK_CHAR 263
#define TOK_CONST 264
#define TOK_CONTINUE 265
#define TOK_DEFAULT 266
#define TOK_DO 267
#define TOK_DOUBLE 268
#define TOK_ELSE 269
#define TOK_ENUM 270
#define TOK_EXTERN 271
#define TOK_FLOAT 272
#define TOK_FOR 273
#define TOK_GOTO 274
#define TOK_IF 275
#define TOK_INLINE 276
#define TOK_INT 277
#define TOK_LONG 278
#define TOK_REGISTER 279
#define TOK_RESTRICT 280
#define TOK_RETURN 281
#define TOK_SHORT 282
#define TOK_SIGNED 283
#define TOK_SIZEOF 284
#define TOK_STATIC 285
#define TOK_STRUCT 286
#define TOK_SWITCH 287
#define TOK_TYPEDEF 288
#define TOK_UNION 289
#define TOK_UNSIGNED 290
#define TOK_VOID 291
#define TOK_VOLATILE 292
#define TOK_WCHAR_T 293
#define TOK_WHILE 294
#define TOK_ARROW 295
#define TOK_INCR 296
#define TOK_DECR 297
#define TOK_SHIFTLEFT 298
#define TOK_SHIFTRIGHT 299
#define TOK_LE 300
#define TOK_GE 301
#define TOK_EQ 302
#define TOK_NE 303
#define TOK_ANDAND 304
#define TOK_OROR 305
#define TOK_ELLIPSIS 306
#define TOK_MULTASSIGN 307
#define TOK_DIVASSIGN 308
#define TOK_MODASSIGN 309
#define TOK_PLUSASSIGN 310
#define TOK_MINUSASSIGN 311
#define TOK_SHLASSIGN 312
#define TOK_SHRASSIGN 313
#define TOK_ANDASSIGN 314
#define TOK_XORASSIGN 315
#define TOK_ORASSIGN 316
#define TOK_IDENTIFIER 317
#define TOK_TYPEDEFNAME 318
#define TOK_INTEGER 319
#define TOK_FLOATING 320
#define TOK_CHARACTER 321
#define TOK_STRING 322
#define TOK_ASM_STRING 323
#define TOK_INT8 324
#define TOK_INT16 325
#define TOK_INT32 326
#define TOK_INT64 327
#define TOK_PTR32 328
#define TOK_PTR64 329
#define TOK_TYPEOF 330
#define TOK_GCC_AUTO_TYPE 331
#define TOK_GCC_FLOAT80 332
#define TOK_GCC_FLOAT128 333
#define TOK_GCC_INT128 334
#define TOK_GCC_DECIMAL32 335
#define TOK_GCC_DECIMAL64 336
#define TOK_GCC_DECIMAL128 337
#define TOK_GCC_ASM 338
#define TOK_GCC_ASM_PAREN 339
#define TOK_GCC_ATTRIBUTE 340
#define TOK_GCC_ATTRIBUTE_ALIGNED 341
#define TOK_GCC_ATTRIBUTE_TRANSPARENT_UNION 342
#define TOK_GCC_ATTRIBUTE_PACKED 343
#define TOK_GCC_ATTRIBUTE_VECTOR_SIZE 344
#define TOK_GCC_ATTRIBUTE_MODE 345
#define TOK_GCC_ATTRIBUTE_GNU_INLINE 346
#define TOK_GCC_ATTRIBUTE_END 347
#define TOK_GCC_LABEL 348
#define TOK_MSC_ASM 349
#define TOK_MSC_BASED 350
#define TOK_CW_VAR_ARG_TYPEOF 351
#define TOK_BUILTIN_VA_ARG 352
#define TOK_GCC_BUILTIN_TYPES_COMPATIBLE_P 353
#define TOK_OFFSETOF 354
#define TOK_ALIGNOF 355
#define TOK_MSC_TRY 356
#define TOK_MSC_FINALLY 357
#define TOK_MSC_EXCEPT 358
#define TOK_MSC_LEAVE 359
#define TOK_MSC_DECLSPEC 360
#define TOK_INTERFACE 361
#define TOK_CDECL 362
#define TOK_STDCALL 363
#define TOK_FASTCALL 364
#define TOK_CLRCALL 365
#define TOK_FORALL 366
#define TOK_EXISTS 367
#define TOK_ACSL_FORALL 368
#define TOK_ACSL_EXISTS 369
#define TOK_ARRAY_OF 370
#define TOK_CPROVER_BITVECTOR 371
#define TOK_CPROVER_FLOATBV 372
#define TOK_CPROVER_FIXEDBV 373
#define TOK_CPROVER_ATOMIC 374
#define TOK_CPROVER_BOOL 375
#define TOK_CPROVER_THROW 376
#define TOK_CPROVER_CATCH 377
#define TOK_CPROVER_TRY 378
#define TOK_CPROVER_FINALLY 379
#define TOK_CPROVER_ID 380
#define TOK_IMPLIES 381
#define TOK_EQUIVALENT 382
#define TOK_TRUE 383
#define TOK_FALSE 384
#define TOK_REAL 385
#define TOK_IMAG 386
#define TOK_ALIGNAS 387
#define TOK_ATOMIC_TYPE_QUALIFIER 388
#define TOK_ATOMIC_TYPE_SPECIFIER 389
#define TOK_GENERIC 390
#define TOK_IMAGINARY 391
#define TOK_NORETURN 392
#define TOK_STATIC_ASSERT 393
#define TOK_THREAD_LOCAL 394
#define TOK_NULLPTR 395
#define TOK_CONSTEXPR 396
#define TOK_SCANNER_ERROR 397
#define TOK_SCANNER_EOF 398
#define TOK_CATCH 399
#define TOK_CHAR16_T 400
#define TOK_CHAR32_T 401
#define TOK_CLASS 402
#define TOK_DELETE 403
#define TOK_DECLTYPE 404
#define TOK_EXPLICIT 405
#define TOK_FRIEND 406
#define TOK_MUTABLE 407
#define TOK_NAMESPACE 408
#define TOK_NEW 409
#define TOK_NOEXCEPT 410
#define TOK_OPERATOR 411
#define TOK_PRIVATE 412
#define TOK_PROTECTED 413
#define TOK_PUBLIC 414
#define TOK_TEMPLATE 415
#define TOK_THIS 416
#define TOK_THROW 417
#define TOK_TYPEID 418
#define TOK_TYPENAME 419
#define TOK_TRY 420
#define TOK_USING 421
#define TOK_VIRTUAL 422
#define TOK_SCOPE 423
#define TOK_DOTPM 424
#define TOK_ARROWPM 425
#define TOK_UNARY_TYPE_PREDICATE 426
#define TOK_BINARY_TYPE_PREDICATE 427
#define TOK_MSC_UUIDOF 428
#define TOK_MSC_IF_EXISTS 429
#define TOK_MSC_IF_NOT_EXISTS 430
#define TOK_UNDERLYING_TYPE 431




/* Copy the first part of user declarations.  */
#line 1 "parser.y"


/*
 * This parser is based on:
 *
 * c5.y, a ANSI-C grammar written by James A. Roskind.
 * "Portions Copyright (c) 1989, 1990 James A. Roskind".
 * (http://www.idiom.com/free-compilers/,
 * ftp://ftp.infoseek.com/ftp/pub/c++grammar/,
 * ftp://ftp.sra.co.jp/.a/pub/cmd/c++grammar2.0.tar.gz)
 */

#define PARSER ansi_c_parser

#include "ansi_c_parser.h"

int yyansi_clex();
extern char *yyansi_ctext;

#include "parser_static.inc"

#include "ansi_c_y.tab.h"

// statements have right recursion, deep nesting of statements thus
// requires more stack space
#define YYMAXDEPTH 25600

/*** token declaration **************************************************/
#line 234 "parser.y"

/************************************************************************/
/*** rules **************************************************************/
/************************************************************************/


/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef int YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 216 of yacc.c.  */
#line 501 "ansi_c_y.tab.cpp"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int i)
#else
static int
YYID (i)
    int i;
#endif
{
  return i;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  154
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   6508

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  201
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  203
/* YYNRULES -- Number of rules.  */
#define YYNRULES  588
/* YYNRULES -- Number of states.  */
#define YYNSTATES  1055

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   431

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   191,     2,     2,     2,   193,   186,     2,
     177,   178,   187,   188,   179,   189,   181,   192,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,   180,   200,
     194,   199,   195,   198,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,   182,     2,   183,   196,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   184,   197,   185,   190,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,   171,   172,   173,   174,
     175,   176
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     5,     7,    10,    12,    14,    16,    18,
      20,    22,    24,    26,    28,    30,    32,    36,    38,    40,
      42,    44,    46,    48,    55,    57,    61,    65,    69,    76,
      83,    88,    95,    97,   101,   106,   113,   118,   125,   130,
     134,   136,   141,   145,   150,   154,   158,   161,   164,   171,
     179,   181,   183,   185,   189,   191,   194,   197,   200,   203,
     206,   209,   212,   215,   218,   221,   226,   229,   234,   236,
     241,   244,   247,   249,   253,   257,   261,   263,   267,   271,
     273,   277,   281,   283,   287,   291,   295,   299,   301,   305,
     309,   311,   315,   317,   321,   323,   327,   329,   333,   335,
     339,   341,   345,   347,   351,   353,   359,   364,   366,   370,
     374,   378,   382,   386,   390,   394,   398,   402,   406,   410,
     412,   416,   418,   419,   421,   424,   427,   430,   433,   436,
     443,   444,   449,   450,   455,   456,   462,   468,   470,   473,
     475,   476,   478,   479,   485,   486,   492,   498,   499,   506,
     508,   510,   512,   514,   516,   518,   520,   522,   524,   526,
     528,   531,   533,   536,   539,   541,   544,   547,   549,   552,
     554,   556,   558,   560,   562,   564,   566,   568,   570,   575,
     577,   582,   587,   589,   591,   593,   595,   597,   599,   602,
     606,   610,   614,   618,   621,   625,   628,   632,   635,   638,
     641,   643,   646,   649,   653,   657,   661,   665,   669,   673,
     677,   681,   685,   688,   692,   696,   701,   706,   708,   711,
     715,   718,   723,   725,   728,   732,   735,   737,   739,   741,
     743,   748,   753,   760,   771,   773,   775,   778,   783,   787,
     788,   790,   792,   794,   796,   798,   800,   802,   804,   806,
     808,   810,   812,   814,   816,   818,   820,   822,   824,   826,
     828,   830,   832,   834,   836,   838,   840,   842,   844,   846,
     848,   850,   855,   863,   871,   873,   875,   877,   879,   884,
     885,   886,   896,   897,   908,   909,   916,   918,   920,   922,
     926,   927,   929,   930,   932,   934,   939,   941,   945,   952,
     953,   955,   957,   960,   963,   966,   972,   975,   981,   987,
     990,   992,   994,   995,   997,   999,  1002,  1005,  1008,  1010,
    1013,  1017,  1021,  1025,  1029,  1033,  1034,  1037,  1041,  1044,
    1045,  1047,  1050,  1051,  1059,  1060,  1069,  1074,  1076,  1077,
    1079,  1081,  1085,  1088,  1092,  1093,  1096,  1098,  1102,  1104,
    1108,  1110,  1112,  1116,  1118,  1121,  1125,  1128,  1130,  1133,
    1137,  1139,  1142,  1146,  1149,  1151,  1154,  1158,  1160,  1162,
    1165,  1169,  1172,  1176,  1177,  1180,  1182,  1186,  1191,  1193,
    1197,  1199,  1200,  1202,  1206,  1209,  1213,  1216,  1220,  1226,
    1231,  1238,  1242,  1244,  1246,  1248,  1250,  1252,  1254,  1256,
    1258,  1260,  1262,  1264,  1266,  1268,  1272,  1277,  1284,  1288,
    1292,  1297,  1302,  1303,  1305,  1308,  1311,  1317,  1325,  1331,
    1333,  1335,  1341,  1349,  1350,  1360,  1364,  1368,  1371,  1374,
    1377,  1381,  1385,  1387,  1391,  1393,  1400,  1406,  1411,  1414,
    1422,  1427,  1429,  1432,  1437,  1442,  1443,  1445,  1447,  1450,
    1453,  1455,  1458,  1462,  1467,  1473,  1475,  1478,  1480,  1485,
    1493,  1495,  1499,  1502,  1504,  1509,  1517,  1519,  1523,  1526,
    1528,  1530,  1532,  1536,  1539,  1541,  1543,  1547,  1549,  1550,
    1552,  1554,  1557,  1559,  1561,  1563,  1565,  1571,  1574,  1576,
    1577,  1579,  1581,  1584,  1587,  1589,  1592,  1595,  1599,  1603,
    1607,  1611,  1615,  1619,  1623,  1628,  1633,  1635,  1637,  1639,
    1642,  1645,  1649,  1651,  1654,  1657,  1660,  1663,  1665,  1667,
    1669,  1671,  1673,  1676,  1678,  1680,  1683,  1687,  1691,  1696,
    1698,  1703,  1709,  1712,  1716,  1720,  1725,  1730,  1732,  1736,
    1738,  1740,  1742,  1745,  1748,  1752,  1755,  1759,  1764,  1766,
    1770,  1772,  1774,  1776,  1778,  1780,  1782,  1786,  1787,  1793,
    1795,  1798,  1799,  1804,  1807,  1811,  1815,  1819,  1824,  1829,
    1834,  1836,  1839,  1842,  1846,  1848,  1850,  1853,  1856,  1860,
    1862,  1866,  1870,  1874,  1879,  1884,  1888,  1892,  1894
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int16 yyrhs[] =
{
     202,     0,    -1,   365,    -1,    62,    -1,   125,    67,    -1,
      63,    -1,    64,    -1,    65,    -1,    66,    -1,    67,    -1,
     205,    -1,   206,    -1,   207,    -1,   208,    -1,   203,    -1,
     209,    -1,   177,   239,   178,    -1,   219,    -1,   214,    -1,
     215,    -1,   216,    -1,   218,    -1,   211,    -1,   135,   177,
     238,   179,   212,   178,    -1,   213,    -1,   212,   179,   213,
      -1,   324,   180,   238,    -1,    11,   180,   238,    -1,    97,
     177,   238,   179,   324,   178,    -1,    98,   177,   324,   179,
     324,   178,    -1,    96,   177,   324,   178,    -1,    99,   177,
     324,   179,   217,   178,    -1,   221,    -1,   217,   181,   221,
      -1,   217,   182,   239,   183,    -1,   111,   335,   184,   242,
     239,   185,    -1,   113,   335,   242,   210,    -1,   112,   335,
     184,   242,   239,   185,    -1,   114,   335,   242,   210,    -1,
     177,   334,   178,    -1,   210,    -1,   220,   182,   239,   183,
      -1,   220,   177,   178,    -1,   220,   177,   222,   178,    -1,
     220,   181,   221,    -1,   220,    40,   221,    -1,   220,    41,
      -1,   220,    42,    -1,   177,   324,   178,   184,   328,   185,
      -1,   177,   324,   178,   184,   327,   179,   185,    -1,   203,
      -1,   204,    -1,   238,    -1,   222,   179,   238,    -1,   220,
      -1,    41,   223,    -1,    42,   223,    -1,   186,   224,    -1,
      49,   345,    -1,   187,   224,    -1,   188,   224,    -1,   189,
     224,    -1,   190,   224,    -1,   191,   224,    -1,    29,   223,
      -1,    29,   177,   324,   178,    -1,   100,   223,    -1,   100,
     177,   324,   178,    -1,   223,    -1,   177,   324,   178,   224,
      -1,   130,   224,    -1,   131,   224,    -1,   224,    -1,   225,
     187,   224,    -1,   225,   192,   224,    -1,   225,   193,   224,
      -1,   225,    -1,   226,   188,   225,    -1,   226,   189,   225,
      -1,   226,    -1,   227,    43,   226,    -1,   227,    44,   226,
      -1,   227,    -1,   228,   194,   227,    -1,   228,   195,   227,
      -1,   228,    45,   227,    -1,   228,    46,   227,    -1,   228,
      -1,   229,    47,   228,    -1,   229,    48,   228,    -1,   229,
      -1,   230,   186,   229,    -1,   230,    -1,   231,   196,   230,
      -1,   231,    -1,   232,   197,   231,    -1,   232,    -1,   233,
      49,   232,    -1,   233,    -1,   234,    50,   233,    -1,   234,
      -1,   234,   126,   235,    -1,   235,    -1,   236,   127,   235,
      -1,   236,    -1,   236,   198,   239,   180,   237,    -1,   236,
     198,   180,   237,    -1,   237,    -1,   224,   199,   238,    -1,
     224,    52,   238,    -1,   224,    53,   238,    -1,   224,    54,
     238,    -1,   224,    55,   238,    -1,   224,    56,   238,    -1,
     224,    57,   238,    -1,   224,    58,   238,    -1,   224,    59,
     238,    -1,   224,    60,   238,    -1,   224,    61,   238,    -1,
     238,    -1,   239,   179,   238,    -1,   238,    -1,    -1,   239,
      -1,   255,   200,    -1,   256,   200,    -1,   243,   200,    -1,
     251,   200,    -1,   244,   200,    -1,   138,   177,   238,   179,
     238,   178,    -1,    -1,   257,   389,   245,   325,    -1,    -1,
     258,   389,   246,   325,    -1,    -1,   244,   179,   389,   247,
     325,    -1,    84,   350,   177,   351,   178,    -1,   300,    -1,
     249,   248,    -1,   248,    -1,    -1,   249,    -1,    -1,   255,
     381,   250,   252,   325,    -1,    -1,   256,   381,   250,   253,
     325,    -1,    76,   381,   250,   199,   326,    -1,    -1,   251,
     179,   381,   250,   254,   325,    -1,   266,    -1,   268,    -1,
     270,    -1,   271,    -1,   272,    -1,   267,    -1,   269,    -1,
     273,    -1,   275,    -1,   277,    -1,   283,    -1,   258,   283,
      -1,   300,    -1,   257,   300,    -1,   257,   260,    -1,   261,
      -1,   258,   261,    -1,   258,   300,    -1,   263,    -1,   258,
     263,    -1,   283,    -1,   261,    -1,   133,    -1,     9,    -1,
      25,    -1,    37,    -1,   119,    -1,    73,    -1,    74,    -1,
      95,   177,   239,   178,    -1,   262,    -1,   132,   177,   239,
     178,    -1,   132,   177,   324,   178,    -1,   261,    -1,   300,
      -1,   261,    -1,   300,    -1,   283,    -1,   264,    -1,   265,
     264,    -1,   257,   284,   298,    -1,   267,   283,   298,    -1,
     266,   260,   298,    -1,   266,   284,   298,    -1,   284,   298,
      -1,   258,   284,   298,    -1,   267,   261,    -1,   267,   284,
     298,    -1,   257,   285,    -1,   269,   283,    -1,   268,   260,
      -1,   285,    -1,   258,   285,    -1,   269,   261,    -1,   273,
     283,   298,    -1,   257,   204,   298,    -1,   270,   260,   298,
      -1,   275,   283,   298,    -1,   257,   274,   298,    -1,   271,
     260,   298,    -1,   277,   283,   298,    -1,   257,   276,   298,
      -1,   272,   260,   298,    -1,   204,   298,    -1,   258,   204,
     298,    -1,   273,   261,   298,    -1,    75,   177,   239,   178,
      -1,    75,   177,   324,   178,    -1,   274,    -1,   258,   274,
      -1,   258,   274,   258,    -1,   274,   258,    -1,   134,   177,
     324,   178,    -1,   276,    -1,   258,   276,    -1,   258,   276,
     258,    -1,   276,   258,    -1,    62,    -1,    63,    -1,    25,
      -1,   278,    -1,   278,   177,    67,   178,    -1,   278,   177,
      64,   178,    -1,   278,   177,   278,   199,   278,   178,    -1,
     278,   177,   278,   199,   278,   179,   278,   199,   278,   178,
      -1,   179,    -1,   279,    -1,   280,   279,    -1,   105,   177,
     280,   178,    -1,   105,   177,   178,    -1,    -1,   281,    -1,
      33,    -1,    16,    -1,    30,    -1,     3,    -1,    24,    -1,
      21,    -1,   139,    -1,    83,    -1,   281,    -1,    22,    -1,
      69,    -1,    70,    -1,    71,    -1,    72,    -1,     8,    -1,
      27,    -1,    23,    -1,    17,    -1,    77,    -1,    78,    -1,
      79,    -1,    80,    -1,    81,    -1,    82,    -1,    13,    -1,
      28,    -1,    35,    -1,    36,    -1,     4,    -1,     5,    -1,
     116,   182,   239,   183,    -1,   117,   182,   239,   183,   182,
     239,   183,    -1,   118,   182,   239,   183,   182,   239,   183,
      -1,   120,    -1,   288,    -1,   310,    -1,   286,    -1,   115,
     194,   324,   195,    -1,    -1,    -1,   292,   298,   282,   289,
     184,   301,   185,   298,   287,    -1,    -1,   292,   298,   282,
     323,   290,   184,   301,   185,   298,   287,    -1,    -1,   292,
     298,   282,   323,   291,   298,    -1,    31,    -1,    34,    -1,
     238,    -1,   293,   179,   238,    -1,    -1,   293,    -1,    -1,
       9,    -1,   203,    -1,   203,   177,   294,   178,    -1,   295,
      -1,   296,   179,   295,    -1,    85,   177,   177,   296,   178,
     178,    -1,    -1,   299,    -1,   300,    -1,   299,   300,    -1,
      88,    92,    -1,    87,    92,    -1,    89,   177,   239,   178,
      92,    -1,    86,    92,    -1,    86,   177,   239,   178,    92,
      -1,    90,   177,   203,   178,    92,    -1,    91,    92,    -1,
     137,    -1,   297,    -1,    -1,   302,    -1,   303,    -1,   302,
     303,    -1,   305,   200,    -1,   304,   200,    -1,   200,    -1,
     243,   200,    -1,   298,   258,   307,    -1,   304,   179,   307,
      -1,   298,   256,   306,    -1,   305,   179,   306,    -1,   381,
     308,   298,    -1,    -1,   309,   298,    -1,   389,   308,   298,
      -1,   309,   298,    -1,    -1,   309,    -1,   180,   240,    -1,
      -1,   313,   298,   311,   184,   314,   185,   298,    -1,    -1,
     313,   298,   323,   312,   184,   314,   185,   298,    -1,   313,
     298,   323,   298,    -1,    15,    -1,    -1,   315,    -1,   316,
      -1,   315,   179,   316,    -1,   315,   179,    -1,   323,   298,
     317,    -1,    -1,   199,   240,    -1,   321,    -1,   321,   179,
      51,    -1,   320,    -1,   319,   179,   320,    -1,   203,    -1,
     322,    -1,   321,   179,   322,    -1,   255,    -1,   255,   394,
      -1,   255,   389,   298,    -1,   255,   383,    -1,   257,    -1,
     257,   394,    -1,   257,   389,   298,    -1,   256,    -1,   256,
     394,    -1,   256,   389,   298,    -1,   256,   383,    -1,   258,
      -1,   258,   394,    -1,   258,   389,   298,    -1,   203,    -1,
     204,    -1,   298,   256,    -1,   298,   256,   393,    -1,   298,
     258,    -1,   298,   258,   393,    -1,    -1,   199,   326,    -1,
     240,    -1,   184,   328,   185,    -1,   184,   327,   179,   185,
      -1,   329,    -1,   327,   179,   329,    -1,   327,    -1,    -1,
     326,    -1,   330,   199,   326,    -1,   330,   326,    -1,   221,
     180,   326,    -1,   181,   221,    -1,   182,   239,   183,    -1,
     182,   239,    51,   239,   183,    -1,   330,   182,   239,   183,
      -1,   330,   182,   239,    51,   239,   183,    -1,   330,   181,
     221,    -1,   333,    -1,   334,    -1,   332,    -1,   337,    -1,
     338,    -1,   340,    -1,   342,    -1,   346,    -1,   343,    -1,
     347,    -1,   348,    -1,   349,    -1,   242,    -1,   323,   180,
     331,    -1,     7,   240,   180,   331,    -1,     7,   240,    51,
     240,   180,   331,    -1,    11,   180,   331,    -1,   335,   184,
     185,    -1,   335,   184,   336,   185,    -1,   335,   184,    68,
     185,    -1,    -1,   331,    -1,   336,   331,    -1,   241,   200,
      -1,    20,   177,   239,   178,   331,    -1,    20,   177,   239,
     178,   331,    14,   331,    -1,    32,   177,   239,   178,   331,
      -1,   332,    -1,   337,    -1,    39,   177,   241,   178,   331,
      -1,    12,   331,    39,   177,   239,   178,   200,    -1,    -1,
      18,   341,   177,   339,   241,   200,   241,   178,   331,    -1,
      19,   239,   200,    -1,    19,   204,   200,    -1,    10,   200,
      -1,     6,   200,    -1,    26,   200,    -1,    26,   239,   200,
      -1,    93,   344,   200,    -1,   345,    -1,   344,   179,   345,
      -1,   323,    -1,    84,   350,   177,   351,   178,   200,    -1,
      84,   350,   184,    68,   185,    -1,    94,   184,    68,   185,
      -1,    94,    68,    -1,   101,   334,   103,   177,   239,   178,
     334,    -1,   101,   334,   102,   334,    -1,   104,    -1,   121,
     200,    -1,   123,   334,   122,   334,    -1,   123,   334,   124,
     334,    -1,    -1,    37,    -1,    19,    -1,    19,    37,    -1,
      37,    19,    -1,   352,    -1,   352,   353,    -1,   352,   353,
     356,    -1,   352,   353,   356,   359,    -1,   352,   353,   356,
     359,   362,    -1,   208,    -1,   180,   355,    -1,   180,    -1,
     208,   177,   239,   178,    -1,   182,   323,   183,   208,   177,
     239,   178,    -1,   354,    -1,   355,   179,   354,    -1,   180,
     358,    -1,   180,    -1,   208,   177,   239,   178,    -1,   182,
     323,   183,   208,   177,   239,   178,    -1,   357,    -1,   358,
     179,   357,    -1,   180,   361,    -1,   180,    -1,   208,    -1,
     360,    -1,   361,   179,   360,    -1,   180,   363,    -1,   180,
      -1,   364,    -1,   363,   179,   364,    -1,   345,    -1,    -1,
     366,    -1,   367,    -1,   366,   367,    -1,   369,    -1,   242,
      -1,   368,    -1,   200,    -1,    84,   177,   208,   178,   200,
      -1,   380,   370,    -1,   334,    -1,    -1,   372,    -1,   373,
      -1,   372,   373,    -1,   379,   200,    -1,   283,    -1,   261,
     283,    -1,   374,   260,    -1,   374,   284,   298,    -1,   267,
     283,   298,    -1,   375,   260,   298,    -1,   375,   284,   298,
      -1,   273,   283,   298,    -1,   374,   204,   298,    -1,   376,
     260,   298,    -1,   374,   292,   323,   298,    -1,   374,   313,
     323,   298,    -1,   375,    -1,   376,    -1,   377,    -1,   378,
     381,    -1,   256,   381,    -1,   379,   179,   381,    -1,   389,
      -1,   255,   381,    -1,   256,   381,    -1,   257,   389,    -1,
     258,   389,    -1,   389,    -1,   382,    -1,   386,    -1,   383,
      -1,   204,    -1,   204,   395,    -1,   384,    -1,   385,    -1,
     187,   383,    -1,   187,   259,   383,    -1,   177,   384,   178,
      -1,   177,   384,   178,   395,    -1,   387,    -1,   187,   177,
     388,   178,    -1,   187,   259,   177,   388,   178,    -1,   187,
     386,    -1,   187,   259,   386,    -1,   177,   386,   178,    -1,
     177,   388,   395,   178,    -1,   177,   386,   178,   395,    -1,
     204,    -1,   177,   388,   178,    -1,   390,    -1,   392,    -1,
     391,    -1,   187,   389,    -1,   196,   389,    -1,   187,   259,
     389,    -1,   392,   395,    -1,   177,   390,   178,    -1,   177,
     390,   178,   395,    -1,   203,    -1,   177,   392,   178,    -1,
     400,    -1,   402,    -1,   395,    -1,   401,    -1,   403,    -1,
     397,    -1,   177,   178,   372,    -1,    -1,   177,   396,   319,
     178,   371,    -1,   399,    -1,   177,   178,    -1,    -1,   177,
     398,   318,   178,    -1,   182,   183,    -1,   182,   265,   183,
      -1,   182,   187,   183,    -1,   182,   240,   183,    -1,   182,
     265,   240,   183,    -1,   399,   182,   240,   183,    -1,   399,
     182,   187,   183,    -1,   187,    -1,   187,   259,    -1,   187,
     393,    -1,   187,   259,   393,    -1,   196,    -1,   187,    -1,
     187,   259,    -1,   187,   394,    -1,   187,   259,   394,    -1,
     196,    -1,   177,   400,   178,    -1,   177,   402,   178,    -1,
     177,   395,   178,    -1,   177,   402,   178,   395,    -1,   177,
     400,   178,   395,    -1,   177,   401,   178,    -1,   177,   403,
     178,    -1,   397,    -1,   177,   401,   178,   397,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   242,   242,   248,   249,   261,   265,   269,   273,   277,
     282,   283,   284,   285,   291,   292,   293,   295,   296,   297,
     298,   299,   300,   304,   314,   318,   325,   332,   342,   349,
     363,   372,   382,   390,   397,   407,   415,   425,   433,   445,
     455,   456,   458,   467,   476,   482,   488,   494,   501,   511,
     525,   526,   530,   535,   543,   544,   550,   556,   561,   571,
     576,   581,   586,   591,   596,   601,   606,   612,   621,   622,
     629,   634,   642,   643,   645,   647,   652,   653,   655,   660,
     661,   663,   668,   669,   671,   673,   675,   680,   681,   683,
     688,   689,   694,   695,   700,   701,   706,   707,   712,   713,
     721,   722,   730,   731,   736,   737,   744,   754,   755,   757,
     759,   761,   763,   765,   767,   769,   771,   773,   775,   780,
     781,   786,   791,   792,   798,   804,   810,   811,   812,   816,
     828,   827,   840,   839,   852,   851,   867,   874,   878,   882,
     887,   890,   896,   894,   912,   910,   926,   944,   942,   958,
     959,   960,   961,   962,   966,   967,   968,   969,   970,   974,
     975,   979,   980,   984,   991,   992,   999,  1006,  1007,  1014,
    1015,  1019,  1020,  1021,  1022,  1023,  1024,  1025,  1026,  1027,
    1031,  1036,  1044,  1045,  1049,  1050,  1051,  1055,  1056,  1063,
    1067,  1071,  1075,  1082,  1086,  1090,  1094,  1102,  1106,  1110,
    1118,  1119,  1123,  1130,  1134,  1138,  1145,  1149,  1153,  1160,
    1164,  1168,  1175,  1179,  1183,  1190,  1195,  1203,  1204,  1208,
    1212,  1219,  1228,  1229,  1233,  1237,  1244,  1248,  1252,  1259,
    1260,  1264,  1268,  1272,  1276,  1280,  1284,  1291,  1296,  1304,
    1307,  1311,  1312,  1313,  1314,  1315,  1316,  1317,  1318,  1319,
    1323,  1324,  1325,  1326,  1327,  1328,  1329,  1330,  1331,  1332,
    1333,  1334,  1335,  1336,  1337,  1338,  1339,  1340,  1341,  1342,
    1343,  1344,  1350,  1357,  1364,  1368,  1369,  1370,  1374,  1379,
    1391,  1388,  1409,  1405,  1429,  1425,  1442,  1444,  1449,  1454,
    1463,  1466,  1471,  1474,  1480,  1485,  1494,  1495,  1502,  1508,
    1511,  1515,  1516,  1523,  1525,  1527,  1529,  1531,  1533,  1535,
    1537,  1539,  1544,  1547,  1551,  1556,  1564,  1565,  1566,  1570,
    1577,  1588,  1596,  1611,  1619,  1630,  1633,  1644,  1653,  1665,
    1668,  1672,  1684,  1682,  1696,  1693,  1706,  1717,  1726,  1729,
    1733,  1738,  1743,  1750,  1761,  1765,  1772,  1773,  1782,  1787,
    1794,  1803,  1808,  1816,  1824,  1831,  1839,  1848,  1856,  1863,
    1871,  1879,  1886,  1894,  1902,  1910,  1917,  1928,  1929,  1933,
    1937,  1942,  1946,  1955,  1959,  1969,  1970,  1976,  1985,  1993,
    2001,  2003,  2011,  2012,  2020,  2026,  2041,  2048,  2055,  2063,
    2070,  2078,  2090,  2091,  2092,  2093,  2094,  2095,  2096,  2097,
    2098,  2099,  2100,  2101,  2105,  2114,  2122,  2129,  2138,  2149,
    2156,  2164,  2176,  2183,  2188,  2195,  2210,  2219,  2228,  2239,
    2240,  2244,  2252,  2261,  2260,  2288,  2305,  2312,  2314,  2316,
    2318,  2323,  2344,  2349,  2356,  2360,  2366,  2377,  2383,  2392,
    2401,  2409,  2417,  2422,  2430,  2440,  2442,  2443,  2444,  2445,
    2457,  2463,  2470,  2478,  2487,  2499,  2503,  2507,  2511,  2517,
    2527,  2532,  2540,  2544,  2548,  2554,  2564,  2569,  2577,  2581,
    2585,  2593,  2598,  2606,  2610,  2614,  2619,  2627,  2635,  2637,
    2641,  2642,  2646,  2651,  2655,  2656,  2660,  2667,  2688,  2693,
    2696,  2700,  2705,  2713,  2718,  2719,  2723,  2730,  2734,  2738,
    2742,  2750,  2754,  2758,  2766,  2771,  2781,  2782,  2783,  2787,
    2793,  2799,  2807,  2815,  2822,  2829,  2836,  2846,  2847,  2851,
    2852,  2856,  2857,  2862,  2866,  2867,  2872,  2880,  2882,  2892,
    2893,  2898,  2904,  2909,  2917,  2919,  2924,  2934,  2935,  2940,
    2941,  2945,  2946,  2951,  2958,  2971,  2977,  2979,  2989,  2996,
    3001,  3002,  3003,  3007,  3008,  3012,  3014,  3025,  3024,  3047,
    3048,  3056,  3055,  3075,  3082,  3092,  3100,  3107,  3116,  3125,
    3138,  3144,  3152,  3157,  3167,  3178,  3184,  3192,  3197,  3207,
    3218,  3220,  3222,  3224,  3230,  3239,  3241,  3243,  3244
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "\"auto\"", "\"bool\"", "\"complex\"",
  "\"break\"", "\"case\"", "\"char\"", "\"const\"", "\"continue\"",
  "\"default\"", "\"do\"", "\"double\"", "\"else\"", "\"enum\"",
  "\"extern\"", "\"float\"", "\"for\"", "\"goto\"", "\"if\"", "\"inline\"",
  "\"int\"", "\"long\"", "\"register\"", "\"restrict\"", "\"return\"",
  "\"short\"", "\"signed\"", "\"sizeof\"", "\"static\"", "\"struct\"",
  "\"switch\"", "\"typedef\"", "\"union\"", "\"unsigned\"", "\"void\"",
  "\"volatile\"", "\"wchar_t\"", "\"while\"", "\"->\"", "\"++\"", "\"--\"",
  "\"<<\"", "\">>\"", "\"<=\"", "\">=\"", "\"==\"", "\"!=\"", "\"&&\"",
  "\"||\"", "\"...\"", "\"*=\"", "\"/=\"", "\"%=\"", "\"+=\"", "\"-=\"",
  "\"<<=\"", "\">>=\"", "\"&=\"", "\"^=\"", "\"|=\"", "TOK_IDENTIFIER",
  "TOK_TYPEDEFNAME", "TOK_INTEGER", "TOK_FLOATING", "TOK_CHARACTER",
  "TOK_STRING", "TOK_ASM_STRING", "\"__int8\"", "\"__int16\"",
  "\"__int32\"", "\"__int64\"", "\"__ptr32\"", "\"__ptr64\"", "\"typeof\"",
  "\"__auto_type\"", "\"__float80\"", "\"__float128\"", "\"__int128\"",
  "\"_Decimal32\"", "\"_Decimal64\"", "\"_Decimal128\"", "\"__asm__\"",
  "\"__asm__ (with parentheses)\"", "\"__attribute__\"", "\"aligned\"",
  "\"transparent_union\"", "\"packed\"", "\"vector_size\"", "\"mode\"",
  "\"__gnu_inline__\"", "\")\"", "\"__label__\"", "\"__asm\"",
  "\"__based\"", "\"_var_arg_typeof\"", "\"__builtin_va_arg\"",
  "\"__builtin_types_compatible_p\"", "\"__offsetof\"", "\"__alignof__\"",
  "\"__try\"", "\"__finally\"", "\"__except\"", "\"__leave\"",
  "\"__declspec\"", "\"__interface\"", "\"__cdecl\"", "\"__stdcall\"",
  "\"__fastcall\"", "\"__clrcall\"", "\"forall\"", "\"exists\"",
  "\"\\\\forall\"", "\"\\\\exists\"", "\"array_of\"",
  "\"__CPROVER_bitvector\"", "\"__CPROVER_floatbv\"",
  "\"__CPROVER_fixedbv\"", "\"__CPROVER_atomic\"", "\"__CPROVER_bool\"",
  "\"__CPROVER_throw\"", "\"__CPROVER_catch\"", "\"__CPROVER_try\"",
  "\"__CPROVER_finally\"", "\"__CPROVER_ID\"", "\"==>\"", "\"<==>\"",
  "\"TRUE\"", "\"FALSE\"", "\"__real__\"", "\"__imag__\"", "\"_Alignas\"",
  "\"_Atomic\"", "\"_Atomic()\"", "\"_Generic\"", "\"_Imaginary\"",
  "\"_Noreturn\"", "\"_Static_assert\"", "\"_Thread_local\"",
  "\"nullptr\"", "\"constexpr\"", "TOK_SCANNER_ERROR", "TOK_SCANNER_EOF",
  "\"catch\"", "\"char16_t\"", "\"char32_t\"", "\"class\"", "\"delete\"",
  "\"decltype\"", "\"explicit\"", "\"friend\"", "\"mutable\"",
  "\"namespace\"", "\"new\"", "\"noexcept\"", "\"operator\"",
  "\"private\"", "\"protected\"", "\"public\"", "\"template\"", "\"this\"",
  "\"throw\"", "\"typeid\"", "\"typename\"", "\"try\"", "\"using\"",
  "\"virtual\"", "\"::\"", "\".*\"", "\"->*\"", "TOK_UNARY_TYPE_PREDICATE",
  "TOK_BINARY_TYPE_PREDICATE", "\"__uuidof\"", "\"__if_exists\"",
  "\"__if_not_exists\"", "\"__underlying_type\"", "'('", "')'", "','",
  "':'", "'.'", "'['", "']'", "'{'", "'}'", "'&'", "'*'", "'+'", "'-'",
  "'~'", "'!'", "'/'", "'%'", "'<'", "'>'", "'^'", "'|'", "'?'", "'='",
  "';'", "$accept", "grammar", "identifier", "typedef_name", "integer",
  "floating", "character", "string", "constant", "primary_expression",
  "generic_selection", "generic_assoc_list", "generic_association",
  "gcc_builtin_expressions", "cw_builtin_expressions", "offsetof",
  "offsetof_member_designator", "quantifier_expression",
  "statement_expression", "postfix_expression", "member_name",
  "argument_expression_list", "unary_expression", "cast_expression",
  "multiplicative_expression", "additive_expression", "shift_expression",
  "relational_expression", "equality_expression", "and_expression",
  "exclusive_or_expression", "inclusive_or_expression",
  "logical_and_expression", "logical_or_expression",
  "logical_implication_expression", "logical_equivalence_expression",
  "conditional_expression", "assignment_expression", "comma_expression",
  "constant_expression", "comma_expression_opt", "declaration",
  "static_assert_declaration", "default_declaring_list", "@1", "@2", "@3",
  "post_declarator_attribute", "post_declarator_attributes",
  "post_declarator_attributes_opt", "declaring_list", "@4", "@5", "@6",
  "declaration_specifier", "type_specifier", "declaration_qualifier_list",
  "type_qualifier_list", "attribute_type_qualifier_list",
  "declaration_qualifier", "type_qualifier", "alignas_specifier",
  "attribute_or_type_qualifier",
  "attribute_or_type_qualifier_or_storage_class",
  "attribute_type_qualifier_storage_class_list",
  "basic_declaration_specifier", "basic_type_specifier",
  "sue_declaration_specifier", "sue_type_specifier",
  "typedef_declaration_specifier", "typeof_declaration_specifier",
  "atomic_declaration_specifier", "typedef_type_specifier",
  "typeof_specifier", "typeof_type_specifier", "atomic_specifier",
  "atomic_type_specifier", "msc_decl_identifier", "msc_decl_modifier",
  "msc_declspec_seq", "msc_declspec", "msc_declspec_opt", "storage_class",
  "basic_type_name", "elaborated_type_name", "array_of_construct",
  "pragma_packed", "aggregate_name", "@7", "@8", "@9", "aggregate_key",
  "gcc_attribute_expression_list", "gcc_attribute_expression_list_opt",
  "gcc_attribute", "gcc_attribute_list", "gcc_attribute_specifier",
  "gcc_type_attribute_opt", "gcc_type_attribute_list",
  "gcc_type_attribute", "member_declaration_list_opt",
  "member_declaration_list", "member_declaration",
  "member_default_declaring_list", "member_declaring_list",
  "member_declarator", "member_identifier_declarator",
  "bit_field_size_opt", "bit_field_size", "enum_name", "@10", "@11",
  "enum_key", "enumerator_list_opt", "enumerator_list",
  "enumerator_declaration", "enumerator_value_opt", "parameter_type_list",
  "KnR_parameter_list", "KnR_parameter", "parameter_list",
  "parameter_declaration", "identifier_or_typedef_name", "type_name",
  "initializer_opt", "initializer", "initializer_list",
  "initializer_list_opt", "designated_initializer", "designator",
  "statement", "declaration_statement", "labeled_statement",
  "compound_statement", "compound_scope", "statement_list",
  "expression_statement", "selection_statement",
  "declaration_or_expression_statement", "iteration_statement", "@12",
  "jump_statement", "gcc_local_label_statement", "gcc_local_label_list",
  "gcc_local_label", "gcc_asm_statement", "msc_asm_statement",
  "msc_seh_statement", "cprover_exception_statement",
  "volatile_or_goto_opt", "gcc_asm_commands", "gcc_asm_assembler_template",
  "gcc_asm_outputs", "gcc_asm_output", "gcc_asm_output_list",
  "gcc_asm_inputs", "gcc_asm_input", "gcc_asm_input_list",
  "gcc_asm_clobbered_registers", "gcc_asm_clobbered_register",
  "gcc_asm_clobbered_registers_list", "gcc_asm_labels",
  "gcc_asm_labels_list", "gcc_asm_label", "translation_unit",
  "external_definition_list", "external_definition", "asm_definition",
  "function_definition", "function_body", "KnR_parameter_header_opt",
  "KnR_parameter_header", "KnR_parameter_declaration",
  "KnR_declaration_qualifier_list", "KnR_basic_declaration_specifier",
  "KnR_typedef_declaration_specifier", "KnR_sue_declaration_specifier",
  "KnR_declaration_specifier", "KnR_parameter_declaring_list",
  "function_head", "declarator", "typedef_declarator",
  "parameter_typedef_declarator", "clean_typedef_declarator",
  "clean_postfix_typedef_declarator", "paren_typedef_declarator",
  "paren_postfix_typedef_declarator", "simple_paren_typedef_declarator",
  "identifier_declarator", "unary_identifier_declarator",
  "postfix_identifier_declarator", "paren_identifier_declarator",
  "abstract_declarator", "parameter_abstract_declarator",
  "postfixing_abstract_declarator", "@13",
  "parameter_postfixing_abstract_declarator", "@14",
  "array_abstract_declarator", "unary_abstract_declarator",
  "parameter_unary_abstract_declarator", "postfix_abstract_declarator",
  "parameter_postfix_abstract_declarator", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,   389,   390,   391,   392,   393,   394,
     395,   396,   397,   398,   399,   400,   401,   402,   403,   404,
     405,   406,   407,   408,   409,   410,   411,   412,   413,   414,
     415,   416,   417,   418,   419,   420,   421,   422,   423,   424,
     425,   426,   427,   428,   429,   430,   431,    40,    41,    44,
      58,    46,    91,    93,   123,   125,    38,    42,    43,    45,
     126,    33,    47,    37,    60,    62,    94,   124,    63,    61,
      59
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint16 yyr1[] =
{
       0,   201,   202,   203,   203,   204,   205,   206,   207,   208,
     209,   209,   209,   209,   210,   210,   210,   210,   210,   210,
     210,   210,   210,   211,   212,   212,   213,   213,   214,   214,
     215,   216,   217,   217,   217,   218,   218,   218,   218,   219,
     220,   220,   220,   220,   220,   220,   220,   220,   220,   220,
     221,   221,   222,   222,   223,   223,   223,   223,   223,   223,
     223,   223,   223,   223,   223,   223,   223,   223,   224,   224,
     224,   224,   225,   225,   225,   225,   226,   226,   226,   227,
     227,   227,   228,   228,   228,   228,   228,   229,   229,   229,
     230,   230,   231,   231,   232,   232,   233,   233,   234,   234,
     235,   235,   236,   236,   237,   237,   237,   238,   238,   238,
     238,   238,   238,   238,   238,   238,   238,   238,   238,   239,
     239,   240,   241,   241,   242,   242,   242,   242,   242,   243,
     245,   244,   246,   244,   247,   244,   248,   248,   249,   249,
     250,   250,   252,   251,   253,   251,   251,   254,   251,   255,
     255,   255,   255,   255,   256,   256,   256,   256,   256,   257,
     257,   257,   257,   257,   258,   258,   258,   259,   259,   260,
     260,   261,   261,   261,   261,   261,   261,   261,   261,   261,
     262,   262,   263,   263,   264,   264,   264,   265,   265,   266,
     266,   266,   266,   267,   267,   267,   267,   268,   268,   268,
     269,   269,   269,   270,   270,   270,   271,   271,   271,   272,
     272,   272,   273,   273,   273,   274,   274,   275,   275,   275,
     275,   276,   277,   277,   277,   277,   278,   278,   278,   279,
     279,   279,   279,   279,   279,   280,   280,   281,   281,   282,
     282,   283,   283,   283,   283,   283,   283,   283,   283,   283,
     284,   284,   284,   284,   284,   284,   284,   284,   284,   284,
     284,   284,   284,   284,   284,   284,   284,   284,   284,   284,
     284,   284,   284,   284,   284,   285,   285,   285,   286,   287,
     289,   288,   290,   288,   291,   288,   292,   292,   293,   293,
     294,   294,   295,   295,   295,   295,   296,   296,   297,   298,
     298,   299,   299,   300,   300,   300,   300,   300,   300,   300,
     300,   300,   301,   301,   302,   302,   303,   303,   303,   303,
     304,   304,   305,   305,   306,   306,   306,   307,   307,   308,
     308,   309,   311,   310,   312,   310,   310,   313,   314,   314,
     315,   315,   315,   316,   317,   317,   318,   318,   319,   319,
     320,   321,   321,   322,   322,   322,   322,   322,   322,   322,
     322,   322,   322,   322,   322,   322,   322,   323,   323,   324,
     324,   324,   324,   325,   325,   326,   326,   326,   327,   327,
     328,   328,   329,   329,   329,   329,   330,   330,   330,   330,
     330,   330,   331,   331,   331,   331,   331,   331,   331,   331,
     331,   331,   331,   331,   332,   333,   333,   333,   333,   334,
     334,   334,   335,   336,   336,   337,   338,   338,   338,   339,
     339,   340,   340,   341,   340,   342,   342,   342,   342,   342,
     342,   343,   344,   344,   345,   346,   346,   347,   347,   348,
     348,   348,   349,   349,   349,   350,   350,   350,   350,   350,
     351,   351,   351,   351,   351,   352,   353,   353,   354,   354,
     355,   355,   356,   356,   357,   357,   358,   358,   359,   359,
     360,   361,   361,   362,   362,   363,   363,   364,   365,   365,
     366,   366,   367,   367,   367,   367,   368,   369,   370,   371,
     371,   372,   372,   373,   374,   374,   374,   375,   375,   375,
     375,   376,   376,   376,   377,   377,   378,   378,   378,   379,
     379,   379,   380,   380,   380,   380,   380,   381,   381,   382,
     382,   383,   383,   383,   384,   384,   384,   385,   385,   386,
     386,   386,   386,   386,   387,   387,   387,   388,   388,   389,
     389,   390,   390,   390,   390,   391,   391,   391,   392,   392,
     393,   393,   393,   394,   394,   395,   395,   396,   395,   397,
     397,   398,   397,   399,   399,   399,   399,   399,   399,   399,
     400,   400,   400,   400,   400,   401,   401,   401,   401,   401,
     402,   402,   402,   402,   402,   403,   403,   403,   403
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     1,     2,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     3,     1,     1,     1,
       1,     1,     1,     6,     1,     3,     3,     3,     6,     6,
       4,     6,     1,     3,     4,     6,     4,     6,     4,     3,
       1,     4,     3,     4,     3,     3,     2,     2,     6,     7,
       1,     1,     1,     3,     1,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     4,     2,     4,     1,     4,
       2,     2,     1,     3,     3,     3,     1,     3,     3,     1,
       3,     3,     1,     3,     3,     3,     3,     1,     3,     3,
       1,     3,     1,     3,     1,     3,     1,     3,     1,     3,
       1,     3,     1,     3,     1,     5,     4,     1,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     1,
       3,     1,     0,     1,     2,     2,     2,     2,     2,     6,
       0,     4,     0,     4,     0,     5,     5,     1,     2,     1,
       0,     1,     0,     5,     0,     5,     5,     0,     6,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       2,     1,     2,     2,     1,     2,     2,     1,     2,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     4,     1,
       4,     4,     1,     1,     1,     1,     1,     1,     2,     3,
       3,     3,     3,     2,     3,     2,     3,     2,     2,     2,
       1,     2,     2,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     2,     3,     3,     4,     4,     1,     2,     3,
       2,     4,     1,     2,     3,     2,     1,     1,     1,     1,
       4,     4,     6,    10,     1,     1,     2,     4,     3,     0,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     4,     7,     7,     1,     1,     1,     1,     4,     0,
       0,     9,     0,    10,     0,     6,     1,     1,     1,     3,
       0,     1,     0,     1,     1,     4,     1,     3,     6,     0,
       1,     1,     2,     2,     2,     5,     2,     5,     5,     2,
       1,     1,     0,     1,     1,     2,     2,     2,     1,     2,
       3,     3,     3,     3,     3,     0,     2,     3,     2,     0,
       1,     2,     0,     7,     0,     8,     4,     1,     0,     1,
       1,     3,     2,     3,     0,     2,     1,     3,     1,     3,
       1,     1,     3,     1,     2,     3,     2,     1,     2,     3,
       1,     2,     3,     2,     1,     2,     3,     1,     1,     2,
       3,     2,     3,     0,     2,     1,     3,     4,     1,     3,
       1,     0,     1,     3,     2,     3,     2,     3,     5,     4,
       6,     3,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     3,     4,     6,     3,     3,
       4,     4,     0,     1,     2,     2,     5,     7,     5,     1,
       1,     5,     7,     0,     9,     3,     3,     2,     2,     2,
       3,     3,     1,     3,     1,     6,     5,     4,     2,     7,
       4,     1,     2,     4,     4,     0,     1,     1,     2,     2,
       1,     2,     3,     4,     5,     1,     2,     1,     4,     7,
       1,     3,     2,     1,     4,     7,     1,     3,     2,     1,
       1,     1,     3,     2,     1,     1,     3,     1,     0,     1,
       1,     2,     1,     1,     1,     1,     5,     2,     1,     0,
       1,     1,     2,     2,     1,     2,     2,     3,     3,     3,
       3,     3,     3,     3,     4,     4,     1,     1,     1,     2,
       2,     3,     1,     2,     2,     2,     2,     1,     1,     1,
       1,     1,     2,     1,     1,     2,     3,     3,     4,     1,
       4,     5,     2,     3,     3,     4,     4,     1,     3,     1,
       1,     1,     2,     2,     3,     2,     3,     4,     1,     3,
       1,     1,     1,     1,     1,     1,     3,     0,     5,     1,
       2,     0,     4,     2,     3,     3,     3,     4,     4,     4,
       1,     2,     2,     3,     1,     1,     2,     2,     3,     1,
       3,     3,     3,     4,     4,     3,     3,     1,     4
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
     478,   244,   269,   270,   255,   172,   265,   337,   242,   258,
     246,   250,   257,   245,   173,   256,   266,   243,   286,   241,
     287,   267,   268,   174,     3,     5,   251,   252,   253,   254,
     176,   177,     0,     0,   259,   260,   261,   262,   263,   264,
     248,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   175,   274,     0,     0,   171,
       0,   310,     0,   247,     0,     0,     0,   485,     0,   548,
     299,   483,     0,     0,     0,     0,     0,     0,     0,   164,
     179,   149,   154,   150,   155,   151,   152,   153,   156,   217,
     157,   222,   158,   249,   159,   299,   200,   277,   275,   299,
     311,   161,   276,   299,     2,   479,   480,   484,   482,   412,
     512,   539,   541,   540,   299,     0,     0,   521,   140,   518,
     520,   523,   524,   519,   529,   517,     0,     0,   306,     0,
     304,   303,     0,     0,   309,     0,     0,   299,     0,     0,
       0,     4,   299,   299,     0,     0,     0,     0,     0,   164,
     167,   183,   542,   543,     1,   212,   300,   301,   126,     0,
     128,     0,   127,   124,   140,   125,   140,   299,   163,   170,
     299,   299,   169,   299,   197,   162,   130,   299,   165,   218,
     223,   160,   299,   201,   166,   132,   299,   299,   195,   299,
     299,   199,   202,   198,   299,   299,   299,   299,   299,   220,
     299,   225,   299,   193,   239,   332,   481,   488,     0,   487,
     561,     0,   545,   555,   559,     0,     0,     0,     0,     6,
       7,     8,     9,     0,     0,     0,     0,     0,   412,   412,
     412,   412,     0,     0,     0,   299,     0,     0,     0,     0,
       0,     0,    14,    10,    11,    12,    13,    15,    40,    22,
      18,    19,    20,    21,    17,    54,    68,    72,    76,    79,
      82,    87,    90,    92,    94,    96,    98,   100,   102,   104,
     107,   119,     0,     0,     0,     0,   537,     0,     0,     0,
       0,     0,   525,   532,   522,   445,   139,   141,     0,   137,
       0,   292,     0,     0,     0,     0,   228,   226,   227,   238,
     234,   229,   235,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   546,   549,   165,   168,   166,   544,   302,   134,
     140,   142,   144,   204,   207,   210,   189,   373,   213,   219,
     224,   194,   373,   191,   192,   190,   196,   205,   208,   211,
     214,   203,   206,   209,   240,   280,   367,   368,     0,   299,
     122,   560,     0,     0,   563,     0,   121,     0,   184,   187,
       0,   186,   185,     0,   299,    64,   299,    55,    56,   434,
      58,   299,     0,   299,   299,   299,    66,     0,     0,     0,
       0,    70,    71,     0,     0,     0,     0,    57,    59,    60,
      61,    62,    63,     0,    46,    47,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   215,     0,   369,   371,   154,   155,   156,   157,   158,
     216,     0,   527,   534,     0,     0,     0,   526,   533,   447,
     446,     0,   138,     0,     0,   293,   294,   296,     0,     0,
       0,     0,   178,     0,   237,   236,   278,   271,     0,     0,
     180,   181,   221,     0,   547,   373,   147,   373,   373,     0,
     131,   133,     0,   284,   338,   336,     0,     0,     0,     0,
       0,   122,   423,     0,     0,     0,     0,     0,     0,   445,
       0,     0,   412,   441,     0,   412,   409,    14,   299,   123,
       0,   404,     0,     0,     0,     0,     0,   413,   394,   392,
     393,   122,   395,   396,   397,   398,   400,   399,   401,   402,
     403,     0,     0,   164,   154,   156,   494,   556,   491,     0,
     506,   507,   508,     0,     0,   350,     0,   348,   353,   360,
     357,   364,     0,   346,   351,   565,   566,   564,     0,   188,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    16,     0,    39,    50,    51,    45,
      42,     0,    52,    44,     0,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   108,    73,    74,    75,    72,
      77,    78,    80,    81,    85,    86,    83,    84,    88,    89,
      91,    93,    95,    97,    99,   101,   103,     0,     0,   120,
     561,   570,   574,   370,   552,   550,   551,   372,   538,   528,
     536,   535,   530,     0,   448,   449,     0,   381,   375,   146,
     486,   290,     0,   292,   307,   305,   308,     0,     0,     0,
       0,     0,     0,   135,   373,   143,   145,   374,   299,     0,
     299,     0,   339,   340,   299,   338,   428,     0,   427,   122,
       0,     0,     0,     0,     0,   429,     0,     0,   122,   411,
       0,     0,   432,   438,     0,     0,   442,     0,   415,   140,
     140,   130,   132,   122,   410,   414,   510,   495,   299,   299,
     492,   299,   496,   299,     0,     0,   299,   299,   299,   509,
       0,   493,   489,     0,   561,   575,   579,   356,   299,   354,
     587,   553,   554,   363,   299,   361,   561,   575,   299,   358,
     299,   365,   562,     0,   567,   569,   568,    65,     0,    30,
     299,   299,     0,    67,     0,     0,   412,    36,    38,   299,
     381,    69,    43,     0,    41,   106,     0,     0,     0,     0,
     571,   572,   531,   455,     0,   450,     0,     0,    14,     0,
     382,   380,     0,   378,     0,   288,   291,     0,   298,   297,
     231,   230,     0,     0,     0,   129,   148,   318,     0,     0,
       0,   299,   314,     0,     0,   299,   285,   299,   342,   344,
       0,     0,   122,   408,     0,   122,   426,   425,     0,   430,
       0,     0,     0,     0,     0,   431,     0,   412,     0,   412,
     412,   405,   498,   501,   502,   497,   299,   299,   499,   500,
     503,   511,   558,   490,   349,   560,     0,     0,   576,   577,
     355,   362,   576,   359,   366,   347,   352,     0,     0,     0,
      32,     0,     0,     0,     0,    24,     0,   380,     0,    53,
     105,   582,   580,   581,   573,   136,   457,   451,   386,     0,
       0,     0,   376,     0,     0,     0,   384,     0,   295,     0,
     272,   273,   319,   325,     0,   299,   315,     0,   317,   325,
     316,     0,   333,   341,     0,   343,   299,     0,   406,     0,
     419,   420,   122,   122,   122,   122,     0,     0,   433,   437,
     440,     0,   443,   444,   504,   505,   585,   586,   578,    28,
      29,    31,     0,     0,    35,    37,     0,    23,   299,     0,
       0,    48,   584,   583,     0,     0,   460,   456,   463,   452,
       0,   387,   385,   377,   379,   391,     0,   383,   289,   232,
       0,     0,   322,   299,   329,   320,   299,   329,   279,   321,
     323,   299,   345,   335,   122,     0,     0,   416,   418,   421,
       0,   436,     0,   561,   588,    33,     0,    27,    25,    26,
      49,     0,     0,     0,     0,     0,   466,   462,   469,   453,
       0,     0,   389,     0,   331,   326,   299,   330,   328,   299,
     281,   279,   407,     0,   122,   122,   435,   412,    34,     0,
       0,   461,     0,     0,     0,   470,   471,   468,   474,   454,
     388,     0,     0,   324,   327,   283,   422,     0,   417,   439,
       0,   458,     0,     0,   467,     0,   477,   473,   475,   390,
       0,   122,     0,     0,   464,   472,     0,   233,   424,     0,
       0,   476,   459,     0,   465
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    68,   242,   117,   243,   244,   245,   246,   247,   248,
     249,   854,   855,   250,   251,   252,   849,   253,   254,   255,
     769,   581,   256,   257,   258,   259,   260,   261,   262,   263,
     264,   265,   266,   267,   268,   269,   270,   271,   509,   638,
     510,   511,    72,    73,   327,   332,   475,   286,   287,   321,
      74,   477,   478,   654,   512,   513,   514,   515,   148,   168,
      79,    80,   150,   359,   360,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,   301,   302,   303,
      93,   345,    94,    95,    96,    97,  1000,    98,   482,   659,
     660,    99,   776,   777,   457,   458,   100,   273,   156,   157,
     790,   791,   792,   793,   794,   952,   955,   996,   953,   102,
     348,   486,   103,   661,   662,   663,   895,   552,   546,   547,
     553,   554,   516,   856,   480,   770,   771,   772,   773,   774,
     517,   518,   519,   520,   208,   521,   522,   523,   902,   524,
     671,   525,   526,   681,  1036,   527,   528,   529,   530,   451,
     764,   765,   867,   936,   937,   939,   986,   987,   989,  1016,
    1017,  1019,  1037,  1038,   104,   105,   106,   107,   108,   209,
     832,   537,   538,   539,   540,   541,   542,   543,   544,   109,
     954,   119,   120,   121,   122,   123,   124,   279,   125,   111,
     112,   113,   623,   839,   444,   352,   213,   353,   214,   625,
     721,   626,   722
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -830
static const yytype_int16 yypact[] =
{
    1898,  -830,  -830,  -830,  -830,  -830,  -830,  -830,  -830,  -830,
    -830,  -830,  -830,  -830,  -830,  -830,  -830,  -830,  -830,  -830,
    -830,  -830,  -830,  -830,  -830,  -830,  -830,  -830,  -830,  -830,
    -830,  -830,  -129,   324,  -830,  -830,  -830,  -830,  -830,  -830,
    -830,   -67,   -43,   -52,    48,    93,    54,    68,   156,    80,
      87,    83,   108,   111,   118,  -830,  -830,   293,   167,  -830,
     193,  -830,   204,  -830,   257,  3548,   257,  -830,   309,  -830,
    1360,  -830,   189,   -97,   -86,   112,   242,  2174,  2174,  -830,
    -830,  6005,  6005,  1653,  1653,  1653,  1653,  1653,  1653,  1097,
     332,  1097,   332,  -830,  -830,  1360,  -830,  -830,  -830,  1360,
    -830,  -830,  -830,  1360,  -830,  1898,  -830,  -830,  -830,  -830,
    -830,  -830,  -830,    16,  3945,   823,  1643,    16,  1352,  -830,
    -830,  -830,  -830,  -830,  -830,  -830,   317,   214,  -830,  5012,
    -830,  -830,  5012,    12,  -830,  5012,    21,  1360,  5012,  5012,
    5012,  -830,  3945,  1360,  5012,   218,   125,  3869,   257,   995,
    -830,  -830,  -830,  -830,  -830,  -830,  1360,  -830,  -830,   257,
    -830,   324,  -830,  -830,  1344,  -830,  1426,  1360,  -830,  -830,
    1360,  1360,  -830,  1360,  -830,  -830,   249,  1360,  -830,  1097,
    1097,  -830,  1360,  -830,  -830,   251,  1360,  1360,  -830,  1360,
    1360,  -830,  -830,  -830,  1360,  1360,  1360,  1360,  1360,  3869,
    1360,  3869,  1360,  -830,   306,    59,  -830,  -830,   266,  -830,
      58,  3278,  -830,  -830,   236,  1289,  5206,  5206,    59,  -830,
    -830,  -830,  -830,   299,   308,   313,   328,  5212,  -830,  -830,
    -830,  -830,  5012,  5012,   331,  3838,  5012,  5012,  5012,  5012,
    5012,  5012,  -830,  -830,  -830,  -830,  -830,  -830,  -830,  -830,
    -830,  -830,  -830,  -830,  -830,    29,  -830,   169,    66,   -94,
     279,     9,   310,   340,   322,   333,   488,     6,  -830,   -47,
    -830,  -830,   269,  6257,   376,   823,  -830,   381,   386,    16,
     823,   873,  -830,  -830,  -830,   117,  -830,  1352,   369,  -830,
     403,    38,   277,   288,   412,   295,  -830,  -830,  -830,  -830,
    -830,   418,  -830,    36,   405,    77,   132,   196,   300,   424,
     433,   444,    16,  -830,   995,  -830,  1162,  -830,  -830,  -830,
    1352,  -830,  -830,  -830,  -830,  -830,  -830,   437,  -830,  3869,
    3869,  -830,   437,  -830,  -830,  -830,  -830,  -830,  -830,  -830,
    -830,  -830,  -830,  -830,  -830,    59,  -830,  -830,   445,   891,
    2578,  5752,    12,  5615,  -830,  4333,  -830,   458,  -830,  -830,
    3409,  -830,  -830,  5109,  3838,  -830,  3838,  -830,  -830,  -830,
    -830,  1360,  5012,  1360,  1360,  3838,  -830,   464,   473,  5341,
    5341,  -830,  -830,  5012,   360,   472,   482,  -830,  -830,  -830,
    -830,  -830,  -830,    59,  -830,  -830,  4430,    59,  5012,  5012,
    5012,  5012,  5012,  5012,  5012,  5012,  5012,  5012,  5012,  5012,
    5012,  5012,  5012,  5012,  5012,  5012,  5012,  5012,  5012,  5012,
    5012,  5012,  5012,  5012,  5012,  5012,  5012,  5012,  5012,  5012,
    4527,  -830,  5012,   110,  2441,  6375,  1097,  1097,  -830,  -830,
    -830,   225,    16,    16,   486,   227,   823,  -830,  -830,   630,
     667,   519,  -830,  4624,   503,  -830,   529,  -830,   367,   620,
     628,   636,  -830,   508,  -830,  -830,  -830,  -830,   551,   554,
    -830,  -830,  -830,  5012,  -830,   437,  -830,   437,   437,  4624,
    -830,  -830,   557,   559,    59,  -830,   571,   565,  5012,   567,
     601,  2956,  -830,  4721,   596,  3634,   613,   615,   616,   117,
      59,   -45,  -830,  -830,   600,  -830,  -830,   623,  1076,   627,
     609,  -830,   112,   242,  2174,  2174,   635,  -830,  -830,  -830,
    -830,  2767,  -830,  -830,  -830,  -830,  -830,  -830,  -830,  -830,
    -830,   324,  6126,   332,  6005,  1653,  -830,  5752,  -830,  5884,
    6005,  1653,  -830,   324,   -73,  -830,   374,  -830,   773,   773,
    2037,  2037,   640,   642,  -830,  -830,  -830,  -830,   639,  -830,
    4818,   646,   647,   654,   655,   660,   661,   662,   656,  5341,
    5341,  1395,  1395,   664,  -830,  4915,  -830,  -830,  -830,  -830,
    -830,   378,  -830,  -830,   234,  -830,  -830,  -830,  -830,  -830,
    -830,  -830,  -830,  -830,  -830,  -830,  -830,  -830,  -830,  -830,
      66,    66,   -94,   -94,   279,   279,   279,   279,     9,     9,
     310,   340,   322,   333,   488,  -830,  -830,  5012,   383,  -830,
     682,  3617,  -830,  -830,  -830,  -830,  -830,  -830,  -830,  -830,
    -830,  -830,  -830,   246,  -830,  -830,   317,  4236,  -830,  -830,
    -830,  5012,   670,    38,  -830,  -830,  -830,   673,   674,   650,
    5012,  5012,   675,  -830,   437,  -830,  -830,  -830,  1117,   677,
    1360,   669,   666,  -830,  1360,    59,  -830,   -24,  -830,  2956,
     816,   685,   663,   -35,  5012,  -830,   -27,  5012,  5012,  -830,
      24,   -13,  -830,  -830,   799,   477,  -830,   212,  -830,  1352,
    1352,  -830,  -830,  2956,  -830,  -830,  -830,  -830,  1360,  1360,
    -830,  1360,  -830,  1360,    59,    59,  1360,  1360,  1360,  -830,
     324,  -830,  5752,    12,   775,  1560,   257,  -830,  1360,  -830,
    -830,  -830,  -830,  -830,  1360,  -830,   848,  3492,  1360,  -830,
    1360,  -830,  -830,  5478,  -830,  -830,  -830,   684,   684,  -830,
    1360,  1360,    59,   684,  5012,  5012,  5012,  -830,  -830,   671,
    4236,  -830,  -830,  5012,  -830,  -830,  5012,   694,   697,   698,
     110,  -830,  -830,  -830,   699,   700,    59,  5012,   701,   702,
    -830,   709,   705,  -830,  3731,  -830,   713,   715,  -830,  -830,
    -830,  -830,    67,   237,   253,  -830,  -830,  -830,   679,  6257,
     710,  1161,  -830,    34,    40,  1117,  -830,  1360,    59,   695,
     712,  5012,  2956,  -830,   722,  3145,  -830,  -830,   414,  -830,
     430,   723,   317,   838,    59,  -830,   727,  -830,   731,  -830,
    -830,  -830,  -830,  -830,  -830,  -830,  1360,  1360,  -830,  -830,
    -830,  -830,  -830,  5752,  -830,  -830,   735,   736,   773,  -830,
    -830,  -830,   515,  -830,  -830,  -830,  -830,   737,   738,   165,
    -830,    62,   109,   741,   436,  -830,   744,   739,   740,  -830,
    -830,  -830,    16,    16,  -830,  -830,   -41,   746,  -830,   -15,
    4624,  4042,  -830,    59,  5012,  4624,  -830,  5012,  -830,   443,
    -830,  -830,  -830,   407,  2310,  1360,  -830,    55,  -830,   407,
    -830,   747,  -830,  -830,  5012,  -830,  1360,   749,  -830,  5012,
    -830,  -830,  5012,  2956,  2956,  2956,   753,   752,  -830,  -830,
    -830,  5012,  -830,  -830,  -830,  -830,    96,  -830,  -830,  -830,
    -830,  -830,    59,  5012,  -830,  -830,  5012,  -830,   671,  5012,
    4139,  -830,  -830,  -830,    59,   767,  -830,   766,    -4,   769,
    5012,  -830,  -830,  -830,  -830,  -830,    11,  -830,  -830,  -830,
      67,  5012,  -830,  1360,   776,  -830,  1360,   776,  -830,  -830,
    -830,  1360,  -830,  -830,  2956,   449,   761,   949,  -830,  -830,
     768,  -830,   453,   789,  -830,  -830,   262,  -830,  -830,  -830,
    -830,   787,  5012,   -41,    59,   795,  -830,   811,   317,   794,
     282,  5012,  -830,   793,  -830,  -830,  1360,  -830,  -830,  1360,
    -830,  -830,  -830,   797,  5012,  2956,  -830,  -830,  -830,   317,
     460,  -830,   810,  5012,    -4,  -830,  -830,   815,    59,  -830,
    -830,   285,    67,  -830,  -830,  -830,  -830,   817,  -830,  -830,
     822,  -830,   317,   466,  -830,   317,  -830,   824,  -830,  -830,
     828,  2956,  5012,   825,  -830,  -830,    59,  -830,  -830,   530,
    5012,  -830,  -830,   593,  -830
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -830,  -830,     0,   254,  -830,  -830,  -830,   -81,  -830,    14,
    -830,  -830,    73,  -830,  -830,  -830,  -830,  -830,  -830,  -830,
    -312,  -830,   122,  1127,   375,   379,   359,   391,   584,   585,
     588,   589,   587,  -830,   368,  -830,  -611,   920,   545,  -203,
    -664,    19,  -599,  -830,  -830,  -830,  -830,   734,  -830,  -108,
    -830,  -830,  -830,  -830,    13,    41,    18,    37,   -85,    26,
    1067,  -830,   875,   672,  -830,  -830,  -220,  -830,  -216,  -830,
    -830,  -830,  -213,   -34,  -165,   -28,  -154,  -431,   721,  -830,
     827,  -830,   184,   -57,   -26,  -830,    32,  -830,  -830,  -830,
    -830,   495,  -830,  -830,   384,  -830,  -830,   913,  -830,   296,
     241,  -830,   248,  -830,  -830,   148,   155,    88,  -641,  -830,
    -830,  -830,   509,   382,  -830,   255,  -830,  -830,  -830,   336,
    -830,   318,  -202,   -46,  -328,  -444,   304,   305,  -829,  -830,
    -242,   256,  -830,   -80,   442,  -830,   258,  -830,  -830,  -830,
    -830,  -830,  -830,  -830,  -217,  -830,  -830,  -830,  -830,   563,
     259,  -830,  -830,    82,  -830,  -830,    52,  -830,  -830,    35,
    -830,  -830,  -830,    27,  -830,  -830,   962,  -830,  -830,  -830,
    -830,   362,  -532,  -830,  -830,  -830,  -830,  -830,  -830,  -830,
      -3,  -830,  -109,  -113,  -830,  -104,  -830,  -258,   104,   -36,
    -830,   -25,  -419,  -462,   -79,  -830,   390,  -830,  -830,   452,
    -535,   456,  -519
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -558
static const yytype_int16 yytable[] =
{
      69,   370,   277,   349,   481,   700,   755,   282,   357,   639,
     288,   278,   283,    75,   811,   627,   369,   441,    77,    71,
     173,   182,   445,   683,   187,   190,   222,   801,   145,   207,
     118,   281,   649,    69,   212,   657,   940,    78,   284,   146,
     128,    76,   944,   170,   179,   290,   296,   455,   114,   171,
     180,   174,   183,   435,   417,   418,   427,   436,   322,   788,
     437,   296,   991,   222,    69,    69,    69,   212,   274,   393,
     394,   395,   164,   166,    24,    69,    69,    69,    69,   145,
     429,   579,   159,   297,   298,   583,   719,   725,   729,   731,
     146,   304,   296,   161,   413,   414,   309,   310,   297,   298,
      24,   944,   147,   160,   110,    69,   710,   186,   438,   191,
     126,   194,   195,   196,   162,    69,    69,    24,    75,   439,
    -557,    24,    25,    77,    71,   129,   199,   711,   201,   297,
     298,   534,   428,   294,   127,   436,   449,    57,   535,   684,
     130,   934,    78,   483,   432,   860,    76,   653,    69,   655,
     656,   430,   432,   147,   450,   386,   802,   558,   320,    69,
     561,    69,   277,    57,   432,   807,   814,   277,   941,   152,
     153,   278,   447,   809,    24,    25,   278,   448,   984,   836,
      57,   176,   185,  -557,    57,   131,   438,   815,   633,   385,
     432,   836,   788,   210,   992,   837,   788,   439,   211,   299,
     300,   812,   761,   419,   420,   346,   396,   837,   813,   110,
     397,   398,   476,   887,   464,   300,   329,   330,   346,   889,
     152,   399,   400,   401,   402,   403,   404,   405,   406,   407,
     408,   132,    64,   474,   888,   951,   351,    57,   966,   145,
     890,   432,    65,   956,   145,   133,   956,   924,   134,   670,
     146,    66,   317,   410,    70,   146,   432,   135,   411,   412,
     467,   172,   181,   319,   136,   172,   189,   172,   193,   172,
     172,   172,   198,   973,   200,    69,   202,   137,   211,   695,
      69,    69,   664,   682,   386,   667,   386,   620,   432,   115,
     138,   456,   211,   139,   925,   386,   101,   621,   369,   116,
     140,   700,   210,   313,    24,    25,   622,   211,    66,   154,
     434,   432,   163,   997,   433,   468,   997,   534,   562,    24,
     563,   436,   415,   416,   535,   564,   786,   566,   567,   568,
     876,   167,   177,   277,   819,     1,   820,   365,   367,   368,
    1027,   864,   278,   921,   142,   346,   922,   923,     8,   376,
     507,   879,   545,    10,   624,   624,    13,   421,   422,    70,
     141,   151,    17,   629,   630,    19,   548,    57,   409,   276,
     143,   550,   438,   175,   184,   432,   918,   182,   190,   469,
     918,   144,    57,   439,   222,   317,    24,    25,   532,   158,
     551,   291,   531,   577,   549,   361,   312,   577,   571,   572,
     179,   101,   210,   628,   210,   632,   180,   211,   183,   211,
     145,    50,   151,   432,   289,    40,   432,   754,   363,   115,
     880,   146,   685,   210,   762,   687,   942,   803,   211,   116,
     850,   947,   432,  -515,    64,  -516,   881,    50,    66,   717,
     723,   432,   165,   316,    65,  1008,    69,   431,   432,    57,
     350,   821,   318,    66,   868,   459,   432,   173,   182,   347,
     289,   432,   289,   664,   432,  1020,   460,   432,  1039,    24,
      25,    63,   347,   462,   432,   182,   371,   190,   470,   432,
     170,   179,   703,   707,   346,   372,   171,   180,   174,   183,
     373,   507,   534,   173,   182,   184,   436,   184,   179,   535,
     346,   115,   826,   827,   180,   374,   183,   362,   383,   689,
     690,   116,    69,    69,    69,    69,   170,   179,   424,   993,
      66,   507,   171,   180,   174,   183,   423,    70,   696,   276,
     425,    69,    57,   296,   276,   536,   760,   426,   574,   432,
     709,   757,   624,    69,   361,   642,   643,   438,    69,    69,
      69,    69,   712,   713,   440,   763,   752,   753,   439,   442,
     898,   945,   432,   756,   443,   702,   706,   708,   453,   435,
     297,   298,   647,   436,   532,   648,   437,    24,   531,   817,
     818,   454,   322,   289,   115,   747,   748,   951,   744,   745,
     461,  1040,   903,   432,   116,   463,   664,   908,   897,   347,
     466,   277,   471,    66,   508,    70,   282,    70,   904,   432,
     975,   472,   369,   534,   927,   928,   289,   436,   691,   692,
     535,   949,   950,   473,   438,   184,   184,  1003,   432,   484,
     838,  1007,   432,    70,    70,   439,   479,   768,  1031,   432,
      57,   556,   842,   456,  1044,   432,   101,   578,   569,   101,
     575,   578,   718,   724,   728,   730,   362,   570,   147,   272,
     576,   967,   968,   969,   631,   346,   386,   634,   438,   507,
     377,   378,   379,   380,   292,   101,   101,   293,   145,   439,
     295,   624,   853,   305,   306,   307,   635,   308,   177,   146,
     145,   962,   726,   507,   847,   848,   636,   211,   172,   181,
     276,   146,   727,   640,   346,   346,   641,   831,  1052,   432,
      69,   716,   644,   545,    69,    69,    69,   697,   698,   699,
     645,   536,  1002,   172,   172,   172,    69,    69,   646,   447,
     184,   763,   981,   650,   172,   181,   651,   910,   347,   912,
     913,   658,   577,  -282,  -557,   508,   548,   672,   994,   532,
     768,   550,   147,   531,   347,   665,    42,    43,    44,    45,
      46,    47,    48,  1028,   147,   666,   577,   668,   167,   177,
     551,  1054,   432,   674,   549,   508,   604,   605,   606,   607,
     384,   669,  1012,   932,   933,   935,   177,   101,   600,   601,
     677,    70,   678,   701,   602,   603,   615,   616,   346,  1048,
     686,   679,   507,  -367,   167,   177,   432,  -557,    61,   688,
     175,   184,   608,   609,   346,   693,   369,   101,   732,   152,
     153,   733,   734,    70,    70,   737,   884,   182,   184,   736,
     883,   152,   738,   739,   743,    24,    25,    24,    69,   740,
     741,   742,    69,   749,   369,   798,   175,   184,   778,   782,
     179,   780,   781,   785,   797,   804,   180,   985,   183,   620,
     351,   795,   805,   806,   211,   101,   101,   816,   750,   621,
     532,   768,   861,   577,   531,   862,   863,   865,   622,   882,
     866,   -50,   870,    69,    69,    24,    25,    69,   871,    69,
     872,   578,   877,   878,   894,   885,   536,   896,    57,   899,
      57,   905,   935,   507,   507,   507,   907,  1015,   911,   384,
      24,   384,   909,   916,   917,   919,   920,   151,   930,   347,
     384,   926,   577,   508,   929,   931,   938,  1029,  1030,   964,
     768,   970,   961,   985,   346,    24,    25,   971,   720,   720,
     720,   720,   317,   584,   982,   983,   317,   508,    57,   988,
     714,  1043,   714,   835,  1015,   211,   951,   211,   347,   347,
     715,  1004,   715,  1005,   507,   101,    70,   835,  1006,   716,
    1009,   716,  1013,    57,  1018,   618,    42,    43,    44,    45,
      46,    47,    48,   155,   346,   289,   289,    70,   957,   101,
    1014,   957,  1022,  1032,  1035,  1041,   578,  1026,    57,  1042,
     275,   978,  1050,  1046,   578,   507,  1047,   610,   203,   611,
     116,   151,   204,   612,   614,   613,   205,   536,   346,    66,
     578,   452,   315,   151,   465,   726,   835,   779,    61,   101,
     211,   344,   559,  1025,   704,   727,   891,   960,   673,   886,
     676,   507,   959,    70,   716,   999,   346,   800,   705,   834,
     446,   846,   347,   893,   857,   858,   508,  -182,  -182,    70,
     116,   900,   680,   901,   311,  1011,  1034,   206,   347,    66,
    1045,   906,   758,  1051,   833,  -334,   759,     0,     0,     0,
     323,     0,     0,   324,   325,     0,   326,    70,     0,     0,
     328,     0,     0,     0,     0,   331,     0,     0,   101,   333,
     334,   101,   335,   336,   720,   720,     5,   337,   338,   339,
     340,   341,     0,   342,     0,   343,   720,   720,     0,     0,
    -182,     0,    14,     0,     0,   578,     0,   578,     0,     0,
       0,   356,   149,     0,    23,     0,     0,     0,   177,     0,
       0,     0,     0,     0,   169,   178,     0,     0,   169,   188,
     169,   192,   169,   169,   169,   197,     0,   508,   508,   508,
       0,    42,    43,    44,    45,    46,    47,    48,     0,     0,
      30,    31,  -182,  -182,  -182,  -182,   578,  -182,     0,     0,
     184,     0,  -182,   149,   578,     0,     0,     0,   347,     0,
    -182,  -182,    49,     0,     0,   783,   784,     0,     0,   101,
     101,   101,    42,    43,    44,    45,    46,    47,    48,     0,
       0,     0,     0,    61,   314,     0,    55,     0,   508,   808,
       0,     0,   810,     0,  -183,  -183,     0,     0,   720,    58,
      59,     0,   720,     0,     0,     0,     0,     0,   347,     0,
       0,     0,     0,     0,     0,     0,    42,    43,    44,    45,
      46,    47,    48,     0,    61,    62,  -368,     0,     0,   508,
     101,     0,   485,     0,     0,     0,   178,     0,   178,     0,
       0,     0,   347,     0,     0,     0,     0,     0,   358,     0,
     356,     0,     0,   356,     0,     0,     0,  -183,     0,   851,
     852,   384,   565,     0,     0,   508,     0,     0,    61,    62,
     347,   101,  -312,   573,     0,     0,   974,     0,     0,     0,
       0,     0,   869,     0,     0,     0,   582,   787,   215,   585,
     586,   587,   588,   589,   590,   591,   592,   593,   594,   595,
     216,   217,     0,     0,     0,     0,     0,   101,   218,  -183,
    -183,  -183,  -183,     0,  -183,     0,  -313,     0,     0,  -183,
       0,    24,   619,   219,   220,   221,   222,  -183,  -183,   381,
     382,   787,     0,   387,   388,   389,   390,   391,   392,     0,
       0,     0,     0,   356,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   223,   224,   225,   226,   227,
       0,     0,     0,   652,     0,     0,   178,   178,     0,   356,
     228,   229,   230,   231,     0,     0,     0,     0,   356,     0,
       0,     0,     0,     0,    57,     0,     0,     0,   533,   946,
       0,   155,     0,     0,   234,     0,     0,   358,   285,    42,
      43,    44,    45,    46,    47,    48,   285,    42,    43,    44,
      45,    46,    47,    48,   965,    42,    43,    44,    45,    46,
      47,    48,     0,     0,     0,     0,   972,    24,     0,   219,
     220,   221,   222,     0,     0,     0,   364,     0,   976,     0,
       0,     0,     0,     0,     0,   236,   237,   238,   239,   240,
     241,    61,   388,     0,     0,   990,     0,     0,     0,    61,
       0,   223,   224,   225,   226,     0,     0,    61,     0,     0,
       0,   178,   188,   192,   197,     0,   228,   229,   230,   231,
     285,    42,    43,    44,    45,    46,    47,    48,     0,     0,
      57,     0,     0,     0,     0,     0,     0,  1010,  -513,     0,
     234,     0,     0,     0,     0,     0,  1021,   596,   597,   598,
     599,   599,   599,   599,   599,   599,   599,   599,   599,   599,
     599,   599,   599,   599,   599,   599,   599,   356,  1033,     0,
       0,   775,     0,    61,     0,     0,     0,     0,     0,     5,
       0,   789,   746,   796,     0,     0,     0,   799,     0,     0,
       0,   169,   178,     0,     0,    14,     0,  1049,     0,     0,
       0,     0,     0,     0,     0,  1053,     0,    23,     0,   178,
       0,   188,   197,     0,   533,     0,   169,   169,   169,     0,
    -514,   822,   823,     0,   824,     0,   825,   169,   178,   828,
     829,   830,    24,    25,     0,     0,     0,     0,     0,     0,
       0,   840,     0,    30,    31,     0,     0,   841,     0,     0,
       0,   843,     0,   844,     0,    42,    43,    44,    45,    46,
      47,    48,     5,     0,     0,    49,     1,     0,     0,     0,
       0,     0,     5,     0,     0,     0,     0,     0,    14,     8,
     356,     0,     0,   859,    10,     0,     0,    13,    14,    55,
      23,     0,     0,    17,     0,    57,    19,   388,   149,     0,
      23,     0,    58,    59,   356,     0,     0,    61,     0,     0,
       0,     0,   751,     0,   789,    24,    25,     0,   789,     0,
     892,     0,     0,     0,     0,     0,    30,    31,     0,     0,
       0,   356,     0,     0,     0,     0,    30,    31,    42,    43,
      44,    45,    46,    47,    48,     0,    40,   714,    49,   914,
     915,     0,   211,     0,   599,     0,     0,   715,    49,     0,
       0,     0,     0,     0,     0,     0,   716,     0,    50,     0,
       0,     0,    55,     0,     0,     0,     0,     0,    57,     0,
       0,     0,    55,     0,     0,    58,    59,     0,     0,   533,
      61,     0,   149,     0,     0,    58,    59,     0,     0,     0,
     356,   356,    63,     0,   149,   356,     0,   948,   958,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   963,
       0,     0,     0,     0,   356,     0,     0,     0,     0,     0,
     280,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     116,     0,     0,     0,     0,     0,     0,     0,     0,    66,
       0,     0,     0,     0,     0,     0,   977,     0,     0,   979,
     356,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   995,     0,     0,   998,
       0,   356,     0,     0,  1001,     0,     0,     0,     0,     0,
       0,     0,     0,   599,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     533,     1,     2,     3,     0,     0,     4,     5,     0,  1023,
       0,     6,  1024,     7,     8,     9,     0,     0,     0,    10,
      11,    12,    13,    14,     0,    15,    16,     0,    17,    18,
       0,    19,    20,    21,    22,    23,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   178,     0,     0,     0,     0,     0,     0,     0,     0,
      24,    25,     0,     0,     0,     0,     0,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
       0,     0,     0,    49,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    50,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    51,    52,    53,    54,    55,    56,     0,
       0,     0,     0,    57,     0,     0,     0,     0,     0,     0,
      58,    59,    60,     0,     0,    61,    62,    63,     0,     0,
       1,     2,     3,     0,     0,     4,     5,     0,     0,     0,
       6,     0,     7,     8,     9,     0,     0,     0,    10,    11,
      12,    13,    14,     0,    15,    16,     0,    17,    18,     0,
      19,    20,    21,    22,    23,    64,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    65,     0,     0,     0,     0,
       0,     0,     0,     0,    66,     0,     0,     0,    67,    24,
      25,     0,     0,     0,     0,     0,    26,    27,    28,    29,
      30,    31,    32,     0,    34,    35,    36,    37,    38,    39,
      40,     0,    42,    43,    44,    45,    46,    47,    48,     0,
       0,     0,    49,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    50,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    51,    52,    53,    54,    55,    56,     0,     0,
       0,     0,    57,     0,     0,     0,     0,     0,     0,    58,
      59,    60,     0,     0,    61,     0,    63,     1,     2,     3,
       0,     0,     4,     5,     0,     0,     0,     6,     0,     7,
       8,     9,     0,     0,     0,    10,    11,    12,    13,    14,
       0,    15,    16,     0,    17,    18,     0,    19,    20,    21,
      22,    23,     0,     0,   726,     0,     0,     0,     0,   211,
       0,     0,     0,     0,   727,     0,     0,     0,     0,     0,
       0,     0,     0,   716,     0,     0,    24,    25,     0,     0,
       0,     0,     0,    26,    27,    28,    29,    30,    31,    32,
       0,    34,    35,    36,    37,    38,    39,    40,     0,    42,
      43,    44,    45,    46,    47,    48,     0,     0,     0,    49,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    50,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    51,
      52,    53,    54,    55,    56,     0,     0,     0,     0,    57,
       0,     0,     0,     0,     0,     0,    58,    59,    60,     0,
       0,    61,     0,    63,     2,     3,     0,     0,     4,     5,
       0,     0,     0,     6,     0,     7,     0,     9,     0,     0,
       0,     0,    11,    12,     0,    14,     0,    15,    16,     0,
       0,    18,     0,     0,    20,    21,    22,    23,     0,     0,
       0,    64,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    65,     0,     0,     0,     0,     0,     0,     0,     0,
      66,     0,    24,    25,     0,     0,     0,     0,     0,    26,
      27,    28,    29,    30,    31,    32,     0,    34,    35,    36,
      37,    38,    39,     0,     0,    42,    43,    44,    45,    46,
      47,    48,     0,     0,     0,    49,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    51,    52,    53,    54,    55,
      56,     0,     0,     0,     0,    57,     0,     0,     0,     0,
       0,     0,    58,    59,    60,     2,     3,    61,     0,     4,
       5,     0,     0,     0,     6,     0,     7,     0,     9,     0,
       0,     0,     0,    11,    12,     0,    14,     0,    15,    16,
       0,     0,    18,     0,     0,    20,    21,    22,    23,     0,
       0,     0,     0,     0,     0,     0,     0,    64,     0,     0,
     951,     0,     0,     0,     0,     0,     0,    65,     0,     0,
       0,     0,     0,     0,    25,     0,    66,     0,     0,     0,
      26,    27,    28,    29,    30,    31,    32,     0,    34,    35,
      36,    37,    38,    39,     0,     0,    42,    43,    44,    45,
      46,    47,    48,     0,     0,     0,    49,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    51,    52,    53,    54,
      55,    56,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    58,    59,    60,     0,     0,    61,     0,
       0,     1,     2,     3,   487,   488,     4,     5,   489,   490,
     491,     6,     0,     7,     8,     9,   492,   493,   494,    10,
      11,    12,    13,    14,   495,    15,    16,   215,    17,    18,
     496,    19,    20,    21,    22,    23,     0,   497,   620,   216,
     217,     0,     0,   211,     0,     0,     0,   218,   621,     0,
       0,     0,     0,     0,     0,     0,     0,   622,     0,     0,
      24,    25,   219,   220,   221,   222,   498,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,   499,    42,    43,    44,    45,    46,    47,    48,
       0,   500,   501,    49,   223,   224,   225,   226,   227,   502,
       0,     0,   503,    50,     0,     0,     0,     0,     0,   228,
     229,   230,   231,    51,    52,    53,    54,    55,    56,   504,
       0,   505,     0,    57,     0,     0,     0,     0,   232,   233,
      58,    59,    60,   234,     0,    61,    62,    63,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   235,     0,     0,     0,     0,
       0,     0,  -412,   506,   236,   237,   238,   239,   240,   241,
       1,     2,     3,   487,   488,     4,     5,   489,   490,   491,
       6,     0,     7,     8,     9,   492,   493,   494,    10,    11,
      12,    13,    14,   495,    15,    16,   215,    17,    18,   496,
      19,    20,    21,    22,    23,     0,   497,     0,   216,   217,
       0,     0,     0,     0,     0,     0,   218,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    24,
      25,   219,   220,   221,   222,     0,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,   499,    42,    43,    44,    45,    46,    47,    48,     0,
     500,   501,    49,   223,   224,   225,   226,   227,   502,     0,
       0,   503,    50,     0,     0,     0,     0,     0,   228,   229,
     230,   231,    51,    52,    53,    54,    55,    56,   504,     0,
     505,     0,    57,     0,     0,     0,     0,   232,   233,    58,
      59,    60,   234,     0,    61,    62,    63,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   235,     0,     0,     0,     0,     0,
       0,  -412,   694,   236,   237,   238,   239,   240,   241,     1,
       2,     3,   487,   488,     4,     5,   489,   490,   491,     6,
       0,     7,     8,     9,   492,   493,   494,    10,    11,    12,
      13,    14,   495,    15,    16,   215,    17,    18,   496,    19,
      20,    21,    22,    23,     0,   497,     0,   216,   217,     0,
       0,     0,     0,     0,     0,   218,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    24,    25,
     219,   220,   221,   222,     0,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
     499,    42,    43,    44,    45,    46,    47,    48,     0,   500,
     501,    49,   223,   224,   225,   226,   227,   502,     0,     0,
     503,    50,     0,     0,     0,     0,     0,   228,   229,   230,
     231,    51,    52,    53,    54,    55,    56,   504,     0,   505,
       0,    57,     0,     0,     0,     0,   232,   233,    58,    59,
      60,   234,     0,    61,    62,    63,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   235,     0,     0,     0,     0,     0,     0,
    -412,     0,   236,   237,   238,   239,   240,   241,     1,     2,
       3,     0,     0,     4,     5,     0,     0,     0,     6,     0,
       7,     8,     9,     0,     0,     0,    10,    11,    12,    13,
      14,     0,    15,    16,   215,    17,    18,     0,    19,    20,
      21,    22,    23,     0,     0,     0,   216,   217,     0,     0,
       0,     0,     0,     0,   218,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    24,    25,   219,
     220,   221,   222,     0,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,     0,
      42,    43,    44,    45,    46,    47,    48,     0,     0,     0,
      49,   223,   224,   225,   226,   227,     0,     0,     0,     0,
      50,     0,     0,     0,     0,     0,   228,   229,   230,   231,
      51,    52,    53,    54,    55,    56,     0,     0,     0,     0,
      57,     0,     0,     0,     0,   232,   233,    58,    59,    60,
     234,     1,    61,    62,    63,     0,     0,     5,     0,     0,
       0,     0,     0,     0,     8,     0,     0,     0,     0,    10,
       0,     0,    13,    14,     0,     0,     0,   215,    17,     0,
       0,    19,     0,     0,     0,    23,     0,     0,     0,   216,
     217,     0,   235,     0,     0,     0,     0,   218,     0,     0,
       0,   236,   237,   238,   239,   240,   241,     0,     0,     0,
      24,     0,   219,   220,   221,   222,     0,     0,     0,     0,
       0,    30,    31,     0,     0,     0,     0,     0,     0,     0,
       0,    40,     0,    42,    43,    44,    45,    46,    47,    48,
       0,     0,     0,    49,   223,   224,   225,   226,   227,     0,
       0,     0,     0,    50,     0,     0,     0,     0,     0,   228,
     229,   230,   231,     0,     0,     0,     0,    55,     0,     0,
       0,     0,     0,    57,     0,     0,     0,     0,   232,   233,
      58,    59,     1,   234,     0,    61,     0,    63,     5,     0,
       0,     0,     0,     0,     0,     8,     0,     0,     0,     0,
      10,     0,     0,    13,    14,     0,     0,     0,   215,    17,
       0,     0,    19,     0,     0,     0,    23,     0,     0,     0,
     216,   217,     0,     0,     0,   235,     0,     0,   218,     0,
       0,   354,     0,     0,   236,   355,   238,   239,   240,   241,
       0,    24,     0,   219,   220,   221,   222,     0,     0,     0,
       0,     0,    30,    31,     0,     0,     0,     0,     0,     0,
       0,     0,    40,     0,    42,    43,    44,    45,    46,    47,
      48,     5,     0,     0,    49,   223,   224,   225,   226,   227,
       0,     0,     0,     0,    50,     0,     0,    14,     0,     0,
     228,   229,   230,   231,     0,     0,     0,     0,    55,    23,
       0,     0,     0,     0,    57,     0,     0,     0,     0,   232,
     233,    58,    59,     0,   234,     0,    61,     0,    63,     0,
       0,     0,     0,     0,    24,     0,     0,     5,     0,     0,
       0,     0,     0,     0,     0,    30,    31,     0,     0,     0,
       0,     0,     0,    14,     0,     0,     0,    42,    43,    44,
      45,    46,    47,    48,     0,    23,   235,    49,     0,     0,
       0,     0,   557,     0,     0,   236,   237,   238,   239,   240,
     241,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      24,    55,     0,     0,     0,     0,     0,    57,     0,     0,
       0,    30,    31,     0,    58,    59,     5,     0,     0,    61,
       0,     0,     0,    42,    43,    44,    45,    46,    47,    48,
       0,     0,    14,    49,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    23,     0,     0,     0,     0,     0,
       0,     0,     0,   215,     0,     0,     0,    55,     0,   726,
       0,     0,     0,    57,   211,   216,   217,     0,     0,   727,
      58,    59,     0,   218,     0,    61,     0,     0,   716,     0,
      30,    31,     0,     0,     0,     0,    24,     0,   219,   220,
     221,   222,    42,    43,    44,    45,    46,    47,    48,     0,
       0,     0,    49,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    64,     0,     0,     0,     0,
     223,   224,   225,   226,   227,    65,    55,     0,     0,     0,
       0,     0,     0,     0,    66,   228,   229,   230,   231,    58,
      59,     0,     0,     0,    61,     0,     0,     0,     0,    57,
     215,     0,     0,     0,   232,   233,     0,     0,     0,   234,
       0,     0,   216,   217,     0,     0,     0,     0,     0,     0,
     218,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    24,   620,   219,   220,   221,   222,   211,
       0,     0,     0,     0,   621,     0,     0,     0,     0,     0,
       0,   235,     0,   622,     0,     0,     0,     0,     0,     0,
     236,   237,   238,   239,   240,   241,     0,   223,   224,   225,
     226,   227,     0,     0,   675,     0,     0,     0,     0,     0,
       0,     0,   228,   229,   230,   231,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    57,     0,     0,     0,
       0,   232,   233,     0,     0,     0,   234,   215,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     5,   216,
     217,     0,     0,     0,     0,     0,     0,   218,     0,     0,
       0,     0,     0,     0,    14,     0,     0,     0,     0,     0,
      24,     0,   219,   220,   221,   222,    23,     0,   235,     0,
       0,     0,   873,   874,     0,   637,     0,   236,   237,   238,
     239,   240,   241,    42,    43,    44,    45,    46,    47,    48,
     875,     0,     0,     0,   223,   224,   225,   226,   227,     0,
       0,     0,    30,    31,     0,     0,     0,     0,     0,   228,
     229,   230,   231,     0,    42,    43,    44,    45,    46,    47,
      48,     0,     0,    57,    49,     0,     0,     0,   232,   233,
       0,     0,     0,   234,   215,    61,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   216,   217,    55,     0,
       0,     0,     0,     0,   218,     0,     0,     0,     0,     0,
       0,    58,    59,     0,     0,     0,    61,    24,     0,   219,
     220,   221,   222,     0,     0,   235,     0,     0,     0,     0,
       0,     0,  -412,     0,   236,   237,   238,   239,   240,   241,
      42,    43,    44,    45,    46,    47,    48,     0,     0,     0,
       0,   223,   224,   225,   226,   227,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   228,   229,   230,   231,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      57,   215,     0,     0,     0,   232,   233,     0,     0,     0,
     234,     0,    61,   216,   217,     0,     0,     0,     0,     0,
       0,   218,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    24,    25,   219,   220,   221,   222,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   235,     0,     0,     0,     0,     0,     0,     0,
       0,   236,   237,   238,   239,   240,   241,     0,   223,   224,
     225,   226,   227,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   228,   229,   230,   231,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    57,   215,     0,
       0,     0,   232,   233,     0,     0,     0,   234,     0,     0,
     216,   217,     0,     0,     0,     0,     0,     0,   218,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    24,    25,   219,   220,   221,   222,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   235,
       0,     0,     0,   766,   767,     0,   637,   943,   236,   237,
     238,   239,   240,   241,     0,   223,   224,   225,   226,   227,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     228,   229,   230,   231,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    57,   215,     0,     0,     0,   232,
     233,     0,     0,     0,   234,     0,     0,   216,   217,     0,
       0,     0,     0,     0,     0,   218,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    24,    25,
     219,   220,   221,   222,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   235,     0,     0,     0,
     766,   767,     0,   637,   980,   236,   237,   238,   239,   240,
     241,     0,   223,   224,   225,   226,   227,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   228,   229,   230,
     231,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    57,   215,     0,     0,     0,   232,   233,     0,     0,
       0,   234,     0,     0,   216,   217,     0,     0,     0,     0,
       0,     0,   218,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    24,     0,   219,   220,   221,
     222,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   235,     0,     0,     0,   766,   767,     0,
     637,     0,   236,   237,   238,   239,   240,   241,     0,   223,
     224,   225,   226,   227,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   228,   229,   230,   231,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    57,   215,
       0,     0,     0,   232,   233,     0,     0,     0,   234,     0,
       0,   216,   217,     0,     0,     0,     0,     0,     0,   218,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    24,     0,   219,   220,   221,   222,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     235,     0,     0,     0,     0,     0,   555,     0,     0,   236,
     237,   238,   239,   240,   241,     0,   223,   224,   225,   226,
     227,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   228,   229,   230,   231,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    57,   215,     0,     0,     0,
     232,   233,     0,     0,     0,   234,     0,     0,   216,   217,
       0,     0,     0,     0,     0,     0,   218,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    24,
       0,   219,   220,   221,   222,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   235,   580,     0,
       0,     0,     0,     0,     0,     0,   236,   237,   238,   239,
     240,   241,     0,   223,   224,   225,   226,   227,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   228,   229,
     230,   231,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    57,   215,     0,     0,     0,   232,   233,     0,
       0,     0,   234,     0,     0,   216,   217,     0,     0,     0,
       0,     0,     0,   218,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    24,     0,   219,   220,
     221,   222,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   235,     0,     0,   617,     0,     0,
       0,     0,     0,   236,   237,   238,   239,   240,   241,     0,
     223,   224,   225,   226,   227,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   228,   229,   230,   231,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    57,
     215,     0,     0,     0,   232,   233,     0,     0,     0,   234,
       0,     0,   216,   217,     0,     0,     0,     0,     0,     0,
     218,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    24,    25,   219,   220,   221,   222,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   235,     0,     0,     0,     0,     0,     0,   637,     0,
     236,   237,   238,   239,   240,   241,     0,   223,   224,   225,
     226,   227,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   228,   229,   230,   231,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    57,   215,     0,     0,
       0,   232,   233,     0,     0,     0,   234,     0,     0,   216,
     217,     0,     0,     0,     0,     0,     0,   218,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      24,     0,   219,   220,   221,   222,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   235,     0,
       0,     0,     0,     0,     0,     0,     0,   236,   237,   238,
     239,   240,   241,     0,   223,   224,   225,   226,   227,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   228,
     229,   230,   231,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    57,   215,     0,     0,     0,   232,   233,
       0,     0,     0,   234,     0,     0,   216,   217,     0,     0,
       0,     0,     0,     0,   218,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    24,     0,   219,
     220,   221,   222,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   235,     0,     0,     0,     0,
       0,   735,     0,     0,   236,   237,   238,   239,   240,   241,
       0,   223,   224,   225,   226,   227,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   228,   229,   230,   231,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      57,   215,     0,     0,     0,   232,   233,     0,     0,     0,
     234,     0,     0,   216,   217,     0,     0,     0,     0,     0,
       0,   218,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    24,     0,   219,   220,   221,   222,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   235,     0,     0,     0,     0,     0,     0,   750,
       0,   236,   237,   238,   239,   240,   241,     0,   223,   224,
     225,   226,   227,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   228,   229,   230,   231,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    57,   215,     0,
       0,     0,   232,   233,     0,     0,     0,   234,     0,     0,
     216,   217,     0,     0,     0,     0,     0,     0,   218,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    24,     0,   219,   220,   221,   222,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   235,
       0,     0,     0,     0,     0,     0,     0,     0,   236,   237,
     238,   239,   240,   241,     0,   223,   224,   225,   226,   227,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     228,   229,   230,   231,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    57,   215,     0,     0,     0,   232,
     233,   215,     0,     0,   234,     0,     0,   216,   217,     0,
       0,     0,     0,   216,   217,   218,     0,     0,     0,     0,
       0,   218,     0,     0,     0,     0,     0,     0,    24,     0,
     219,   220,   221,   222,    24,     0,   219,   220,   221,   222,
       0,     0,     0,     0,     0,     0,   235,     0,     0,     0,
       0,     0,     0,     0,     0,   236,   560,   238,   239,   240,
     241,     0,   223,   224,   225,   226,   227,     0,   223,   224,
     225,   226,   227,     0,     0,     0,     0,   228,   229,   230,
     231,     0,     0,   228,   229,   230,   231,     0,     0,     0,
       0,    57,     0,     0,     0,     0,     0,    57,     0,     0,
       0,   234,     0,     0,     1,     2,     3,   234,     0,     4,
       5,     0,     0,     0,     6,     0,     7,     8,     9,     0,
       0,     0,    10,    11,    12,    13,    14,     0,    15,    16,
       0,    17,    18,     0,    19,    20,    21,    22,    23,     0,
       0,     0,     0,   366,     0,     0,     0,     0,     0,   375,
       0,     0,   236,   237,   238,   239,   240,   241,   236,   237,
     238,   239,   240,   241,    25,     0,     0,     0,     0,     0,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,     0,    42,    43,    44,    45,
      46,    47,    48,     0,     0,     0,    49,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    50,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    51,    52,    53,    54,
      55,    56,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    58,    59,    60,     0,     0,    61,    62,
      63,     1,     2,     3,     0,     0,     4,     5,     0,     0,
       0,     6,     0,     7,     8,     9,     0,     0,     0,    10,
      11,    12,    13,    14,     0,    15,    16,     0,    17,    18,
       0,    19,    20,    21,    22,    23,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   845,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    25,     0,     0,     0,     0,     0,    26,    27,    28,
      29,    30,    31,    32,     0,    34,    35,    36,    37,    38,
      39,    40,     0,    42,    43,    44,    45,    46,    47,    48,
       0,     0,     0,    49,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    50,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    51,    52,    53,    54,    55,    56,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      58,    59,    60,     0,     0,    61,     0,    63,     1,     2,
       3,     0,     0,     4,     5,     0,     0,     0,     6,     0,
       7,     8,     9,     0,     0,     0,    10,    11,    12,    13,
      14,     0,    15,    16,     0,    17,    18,     0,    19,    20,
      21,    22,    23,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    25,     0,
       0,     0,     0,     0,    26,    27,    28,    29,    30,    31,
      32,     0,    34,    35,    36,    37,    38,    39,    40,     0,
      42,    43,    44,    45,    46,    47,    48,     0,     0,     0,
      49,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      50,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      51,    52,    53,    54,    55,    56,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    58,    59,    60,
       0,     0,    61,     0,    63,     1,     2,     3,     0,     0,
       4,     5,     0,     0,     0,     6,     0,     7,     8,     9,
       0,     0,     0,    10,    11,    12,    13,    14,     0,    15,
      16,     0,    17,    18,     0,    19,    20,    21,    22,    23,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    25,     0,     0,     0,     0,
       0,    26,    27,    28,    29,    30,    31,    32,     0,    34,
      35,    36,    37,    38,    39,    40,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    49,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    50,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    51,    52,    53,
      54,    55,    56,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    58,    59,    60,     1,     2,     3,
       0,    63,     4,     5,     0,     0,     0,     6,     0,     7,
       8,     9,     0,     0,     0,    10,    11,    12,    13,    14,
       0,    15,    16,     0,    17,    18,     0,    19,    20,    21,
      22,    23,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    25,     0,     0,
       0,     0,     0,    26,    27,    28,    29,    30,    31,     0,
       0,    34,    35,    36,    37,    38,    39,    40,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    49,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    50,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      52,    53,    54,    55,    56,     0,     0,     0,     1,     2,
       3,     0,     0,     4,     5,     0,    58,    59,     6,     0,
       0,     8,     9,    63,     0,     0,    10,    11,    12,    13,
      14,     0,    15,    16,     0,    17,     0,     0,    19,     0,
      21,    22,    23,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    26,    27,    28,    29,    30,    31,
       0,     0,    34,    35,    36,    37,    38,    39,    40,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      49,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      50,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    52,    53,    54,    55,    56,     0,     0,     0,     0,
       2,     3,     0,     0,     4,     5,     0,    58,    59,     6,
       0,     7,     0,     9,    63,     0,     0,     0,    11,    12,
       0,    14,     0,    15,    16,     0,     0,    18,     0,     0,
      20,    21,    22,    23,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    25,
       0,     0,     0,     0,     0,    26,    27,    28,    29,    30,
      31,    32,     0,    34,    35,    36,    37,    38,    39,     0,
       0,    42,    43,    44,    45,    46,    47,    48,     0,     0,
       0,    49,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    51,    52,    53,    54,    55,    56,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    58,    59,
      60,     2,     3,    61,     0,     4,     5,     0,     0,     0,
       6,     0,     7,     0,     9,     0,     0,     0,     0,    11,
      12,     0,    14,     0,    15,    16,     0,     0,    18,     0,
       0,    20,    21,    22,    23,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      25,     0,     0,     0,     0,     0,    26,    27,    28,    29,
      30,    31,    32,     0,    34,    35,    36,    37,    38,    39,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    49,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    51,    52,    53,    54,    55,    56,     0,     2,
       3,     0,     0,     4,     5,     0,     0,     0,     6,    58,
      59,    60,     9,     0,     0,     0,     0,    11,    12,     0,
      14,     0,    15,    16,     0,     0,     0,     0,     0,     0,
      21,    22,    23,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    26,    27,    28,    29,    30,    31,
       0,     0,    34,    35,    36,    37,    38,    39,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      49,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    52,    53,    54,    55,    56,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    58,    59
};

static const yytype_int16 yycheck[] =
{
       0,   218,   115,   205,   332,   537,   617,   116,   211,   453,
     118,   115,   116,     0,   678,   434,   218,   275,     0,     0,
      77,    78,   280,    68,    81,    82,    67,    51,    64,   109,
      33,   116,   463,    33,   113,   479,    51,     0,   117,    64,
      92,     0,   871,    77,    78,   126,    25,     9,   177,    77,
      78,    77,    78,   273,    45,    46,    50,   273,   166,   658,
     273,    25,    51,    67,    64,    65,    66,   146,   114,    40,
      41,    42,    75,    76,    62,    75,    76,    77,    78,   115,
     127,   393,   179,    62,    63,   397,   548,   549,   550,   551,
     115,   137,    25,   179,   188,   189,   142,   143,    62,    63,
      62,   930,    65,   200,     0,   105,   179,    81,   273,    83,
     177,    85,    86,    87,   200,   115,   116,    62,   105,   273,
      62,    62,    63,   105,   105,   177,    89,   200,    91,    62,
      63,   351,   126,   133,   177,   351,    19,   125,   351,   184,
      92,   182,   105,   345,   179,   756,   105,   475,   148,   477,
     478,   198,   179,   116,    37,   235,   180,   360,   161,   159,
     363,   161,   275,   125,   179,   200,   179,   280,   183,    65,
      66,   275,   281,   200,    62,    63,   280,   281,   182,   714,
     125,    77,    78,   125,   125,    92,   351,   200,   446,   235,
     179,   726,   791,   177,   183,   714,   795,   351,   182,   178,
     179,   177,   621,   194,   195,   205,   177,   726,   184,   105,
     181,   182,   320,   179,   178,   179,   179,   180,   218,   179,
     116,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,   177,   177,   312,   200,   180,   178,   125,   902,   275,
     200,   179,   187,   884,   280,   177,   887,   185,    92,   491,
     275,   196,   148,   187,     0,   280,   179,   177,   192,   193,
     183,    77,    78,   159,   177,    81,    82,    83,    84,    85,
      86,    87,    88,   177,    90,   275,    92,   194,   182,   521,
     280,   281,   484,   500,   364,   488,   366,   177,   179,   177,
     182,   291,   182,   182,   185,   375,     0,   187,   500,   187,
     182,   833,   177,   178,    62,    63,   196,   182,   196,     0,
     273,   179,   200,   954,   273,   183,   957,   537,   364,    62,
     366,   537,    43,    44,   537,   371,   654,   373,   374,   375,
     774,    77,    78,   446,   122,     3,   124,   215,   216,   217,
    1004,   760,   446,   178,   177,   345,   181,   182,    16,   227,
     350,   782,   352,    21,   433,   434,    24,    47,    48,   105,
      67,    65,    30,   442,   443,    33,   353,   125,   199,   115,
     177,   353,   537,    77,    78,   179,   838,   434,   435,   183,
     842,   177,   125,   537,    67,   281,    62,    63,   351,   200,
     353,   177,   351,   393,   353,   211,   178,   397,   379,   380,
     434,   105,   177,   178,   177,   178,   434,   182,   434,   182,
     446,   105,   116,   179,   118,    83,   179,   183,   182,   177,
     183,   446,   502,   177,   178,   505,   870,   669,   182,   187,
     742,   875,   179,   184,   177,   184,   183,   105,   196,   548,
     549,   179,   200,   147,   187,   183,   446,   178,   179,   125,
     184,   693,   156,   196,   766,   178,   179,   514,   515,   205,
     164,   179,   166,   665,   179,   183,   178,   179,   183,    62,
      63,   139,   218,   178,   179,   532,   177,   534,   178,   179,
     514,   515,   539,   540,   484,   177,   514,   515,   514,   515,
     177,   491,   712,   550,   551,   199,   712,   201,   532,   712,
     500,   177,   704,   705,   532,   177,   532,   211,   177,   512,
     513,   187,   512,   513,   514,   515,   550,   551,   196,   950,
     196,   521,   550,   551,   550,   551,   186,   273,   531,   275,
     197,   531,   125,    25,   280,   351,   621,    49,   178,   179,
     543,   620,   621,   543,   360,   178,   179,   712,   548,   549,
     550,   551,   178,   179,   178,   636,   178,   179,   712,   178,
     802,   873,   179,   180,   178,   539,   540,   541,   199,   789,
      62,    63,    64,   789,   537,    67,   789,    62,   537,   102,
     103,   178,   690,   287,   177,   571,   572,   180,   569,   570,
     178,  1022,   178,   179,   187,   177,   798,   814,   801,   345,
     195,   714,   178,   196,   350,   351,   715,   353,   178,   179,
     922,   178,   814,   833,   178,   179,   320,   833,   514,   515,
     833,   178,   179,   179,   789,   329,   330,   178,   179,   184,
     715,   178,   179,   379,   380,   789,   199,   637,   178,   179,
     125,   183,   727,   643,   178,   179,   350,   393,   184,   353,
     178,   397,   548,   549,   550,   551,   360,   184,   621,   114,
     178,   903,   904,   905,   178,   665,   746,    37,   833,   669,
     228,   229,   230,   231,   129,   379,   380,   132,   714,   833,
     135,   760,    11,   138,   139,   140,    19,   142,   434,   714,
     726,   894,   177,   693,   740,   741,   177,   182,   514,   515,
     446,   726,   187,   200,   704,   705,   177,   710,   178,   179,
     710,   196,    92,   713,   714,   715,   716,   533,   534,   535,
      92,   537,   964,   539,   540,   541,   726,   727,    92,   838,
     434,   812,   934,   182,   550,   551,   182,   817,   484,   819,
     820,   184,   742,   184,    62,   491,   733,   493,   951,   712,
     750,   733,   715,   712,   500,   184,    85,    86,    87,    88,
      89,    90,    91,  1005,   727,   200,   766,   200,   514,   515,
     733,   178,   179,   177,   733,   521,   417,   418,   419,   420,
     235,   180,   984,   862,   863,   866,   532,   491,   413,   414,
     177,   537,   177,   539,   415,   416,   428,   429,   798,  1041,
     200,   185,   802,   180,   550,   551,   179,   125,   137,   200,
     514,   515,   421,   422,   814,   180,  1018,   521,   178,   715,
     716,   179,   183,   569,   570,   178,   789,   884,   532,   183,
     789,   727,   178,   178,   178,    62,    63,    62,   838,   179,
     179,   179,   842,   179,  1046,   179,   550,   551,   178,   199,
     884,   178,   178,   178,   185,    39,   884,   938,   884,   177,
     178,   184,   177,   200,   182,   569,   570,    68,   184,   187,
     833,   871,   178,   873,   833,   178,   178,   178,   196,   200,
     180,   180,   180,   883,   884,    62,    63,   887,   179,   889,
     185,   637,   179,   178,   199,   185,   712,   185,   125,   177,
     125,   178,   983,   903,   904,   905,    68,   988,   177,   364,
      62,   366,   185,   178,   178,   178,   178,   621,   179,   665,
     375,   180,   922,   669,   180,   185,   180,  1007,  1009,   180,
     930,   178,   185,  1014,   934,    62,    63,   185,   548,   549,
     550,   551,   838,   398,   177,   179,   842,   693,   125,   180,
     177,  1032,   177,   178,  1035,   182,   180,   182,   704,   705,
     187,   200,   187,    14,   964,   669,   712,   178,   200,   196,
     183,   196,   177,   125,   180,   430,    85,    86,    87,    88,
      89,    90,    91,    70,   984,   689,   690,   733,   884,   693,
     179,   887,   199,   183,   179,   178,   742,   200,   125,   177,
     177,   928,   177,   179,   750,  1005,   178,   423,    95,   424,
     187,   715,    99,   425,   427,   426,   103,   833,  1018,   196,
     766,   287,   147,   727,   303,   177,   178,   643,   137,   733,
     182,   204,   360,  1001,   539,   187,   795,   889,   493,   791,
     495,  1041,   887,   789,   196,   957,  1046,   665,   539,   713,
     177,   733,   798,   798,   750,   750,   802,    62,    63,   805,
     187,   805,   499,   805,   144,   983,  1014,   105,   814,   196,
    1035,   812,   620,  1046,   712,   184,   620,    -1,    -1,    -1,
     167,    -1,    -1,   170,   171,    -1,   173,   833,    -1,    -1,
     177,    -1,    -1,    -1,    -1,   182,    -1,    -1,   802,   186,
     187,   805,   189,   190,   714,   715,     9,   194,   195,   196,
     197,   198,    -1,   200,    -1,   202,   726,   727,    -1,    -1,
     125,    -1,    25,    -1,    -1,   871,    -1,   873,    -1,    -1,
      -1,   211,    65,    -1,    37,    -1,    -1,    -1,   884,    -1,
      -1,    -1,    -1,    -1,    77,    78,    -1,    -1,    81,    82,
      83,    84,    85,    86,    87,    88,    -1,   903,   904,   905,
      -1,    85,    86,    87,    88,    89,    90,    91,    -1,    -1,
      73,    74,   177,   178,   179,   180,   922,   182,    -1,    -1,
     884,    -1,   187,   116,   930,    -1,    -1,    -1,   934,    -1,
     195,   196,    95,    -1,    -1,   650,   651,    -1,    -1,   903,
     904,   905,    85,    86,    87,    88,    89,    90,    91,    -1,
      -1,    -1,    -1,   137,   147,    -1,   119,    -1,   964,   674,
      -1,    -1,   677,    -1,    62,    63,    -1,    -1,   838,   132,
     133,    -1,   842,    -1,    -1,    -1,    -1,    -1,   984,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    85,    86,    87,    88,
      89,    90,    91,    -1,   137,   138,   180,    -1,    -1,  1005,
     964,    -1,   349,    -1,    -1,    -1,   199,    -1,   201,    -1,
      -1,    -1,  1018,    -1,    -1,    -1,    -1,    -1,   211,    -1,
     360,    -1,    -1,   363,    -1,    -1,    -1,   125,    -1,   744,
     745,   746,   372,    -1,    -1,  1041,    -1,    -1,   137,   138,
    1046,  1005,   185,   383,    -1,    -1,   916,    -1,    -1,    -1,
      -1,    -1,   767,    -1,    -1,    -1,   396,   200,    29,   399,
     400,   401,   402,   403,   404,   405,   406,   407,   408,   409,
      41,    42,    -1,    -1,    -1,    -1,    -1,  1041,    49,   177,
     178,   179,   180,    -1,   182,    -1,   185,    -1,    -1,   187,
      -1,    62,   432,    64,    65,    66,    67,   195,   196,   232,
     233,   200,    -1,   236,   237,   238,   239,   240,   241,    -1,
      -1,    -1,    -1,   453,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    96,    97,    98,    99,   100,
      -1,    -1,    -1,   473,    -1,    -1,   329,   330,    -1,   479,
     111,   112,   113,   114,    -1,    -1,    -1,    -1,   488,    -1,
      -1,    -1,    -1,    -1,   125,    -1,    -1,    -1,   351,   874,
      -1,   508,    -1,    -1,   135,    -1,    -1,   360,    84,    85,
      86,    87,    88,    89,    90,    91,    84,    85,    86,    87,
      88,    89,    90,    91,   899,    85,    86,    87,    88,    89,
      90,    91,    -1,    -1,    -1,    -1,   911,    62,    -1,    64,
      65,    66,    67,    -1,    -1,    -1,   177,    -1,   923,    -1,
      -1,    -1,    -1,    -1,    -1,   186,   187,   188,   189,   190,
     191,   137,   355,    -1,    -1,   940,    -1,    -1,    -1,   137,
      -1,    96,    97,    98,    99,    -1,    -1,   137,    -1,    -1,
      -1,   434,   435,   436,   437,    -1,   111,   112,   113,   114,
      84,    85,    86,    87,    88,    89,    90,    91,    -1,    -1,
     125,    -1,    -1,    -1,    -1,    -1,    -1,   982,   184,    -1,
     135,    -1,    -1,    -1,    -1,    -1,   991,   410,   411,   412,
     413,   414,   415,   416,   417,   418,   419,   420,   421,   422,
     423,   424,   425,   426,   427,   428,   429,   637,  1013,    -1,
      -1,   641,    -1,   137,    -1,    -1,    -1,    -1,    -1,     9,
      -1,   658,   177,   660,    -1,    -1,    -1,   664,    -1,    -1,
      -1,   514,   515,    -1,    -1,    25,    -1,  1042,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,  1050,    -1,    37,    -1,   532,
      -1,   534,   535,    -1,   537,    -1,   539,   540,   541,    -1,
     184,   698,   699,    -1,   701,    -1,   703,   550,   551,   706,
     707,   708,    62,    63,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   718,    -1,    73,    74,    -1,    -1,   724,    -1,    -1,
      -1,   728,    -1,   730,    -1,    85,    86,    87,    88,    89,
      90,    91,     9,    -1,    -1,    95,     3,    -1,    -1,    -1,
      -1,    -1,     9,    -1,    -1,    -1,    -1,    -1,    25,    16,
     750,    -1,    -1,   753,    21,    -1,    -1,    24,    25,   119,
      37,    -1,    -1,    30,    -1,   125,    33,   560,   621,    -1,
      37,    -1,   132,   133,   774,    -1,    -1,   137,    -1,    -1,
      -1,    -1,   575,    -1,   791,    62,    63,    -1,   795,    -1,
     797,    -1,    -1,    -1,    -1,    -1,    73,    74,    -1,    -1,
      -1,   801,    -1,    -1,    -1,    -1,    73,    74,    85,    86,
      87,    88,    89,    90,    91,    -1,    83,   177,    95,   826,
     827,    -1,   182,    -1,   617,    -1,    -1,   187,    95,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   196,    -1,   105,    -1,
      -1,    -1,   119,    -1,    -1,    -1,    -1,    -1,   125,    -1,
      -1,    -1,   119,    -1,    -1,   132,   133,    -1,    -1,   712,
     137,    -1,   715,    -1,    -1,   132,   133,    -1,    -1,    -1,
     870,   871,   139,    -1,   727,   875,    -1,   877,   885,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   896,
      -1,    -1,    -1,    -1,   894,    -1,    -1,    -1,    -1,    -1,
     177,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     187,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   196,
      -1,    -1,    -1,    -1,    -1,    -1,   926,    -1,    -1,   929,
     930,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   953,    -1,    -1,   956,
      -1,   951,    -1,    -1,   961,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   756,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     833,     3,     4,     5,    -1,    -1,     8,     9,    -1,   996,
      -1,    13,   999,    15,    16,    17,    -1,    -1,    -1,    21,
      22,    23,    24,    25,    -1,    27,    28,    -1,    30,    31,
      -1,    33,    34,    35,    36,    37,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   884,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      62,    63,    -1,    -1,    -1,    -1,    -1,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      -1,    -1,    -1,    95,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   105,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   115,   116,   117,   118,   119,   120,    -1,
      -1,    -1,    -1,   125,    -1,    -1,    -1,    -1,    -1,    -1,
     132,   133,   134,    -1,    -1,   137,   138,   139,    -1,    -1,
       3,     4,     5,    -1,    -1,     8,     9,    -1,    -1,    -1,
      13,    -1,    15,    16,    17,    -1,    -1,    -1,    21,    22,
      23,    24,    25,    -1,    27,    28,    -1,    30,    31,    -1,
      33,    34,    35,    36,    37,   177,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   187,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   196,    -1,    -1,    -1,   200,    62,
      63,    -1,    -1,    -1,    -1,    -1,    69,    70,    71,    72,
      73,    74,    75,    -1,    77,    78,    79,    80,    81,    82,
      83,    -1,    85,    86,    87,    88,    89,    90,    91,    -1,
      -1,    -1,    95,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   105,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   115,   116,   117,   118,   119,   120,    -1,    -1,
      -1,    -1,   125,    -1,    -1,    -1,    -1,    -1,    -1,   132,
     133,   134,    -1,    -1,   137,    -1,   139,     3,     4,     5,
      -1,    -1,     8,     9,    -1,    -1,    -1,    13,    -1,    15,
      16,    17,    -1,    -1,    -1,    21,    22,    23,    24,    25,
      -1,    27,    28,    -1,    30,    31,    -1,    33,    34,    35,
      36,    37,    -1,    -1,   177,    -1,    -1,    -1,    -1,   182,
      -1,    -1,    -1,    -1,   187,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   196,    -1,    -1,    62,    63,    -1,    -1,
      -1,    -1,    -1,    69,    70,    71,    72,    73,    74,    75,
      -1,    77,    78,    79,    80,    81,    82,    83,    -1,    85,
      86,    87,    88,    89,    90,    91,    -1,    -1,    -1,    95,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   105,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   115,
     116,   117,   118,   119,   120,    -1,    -1,    -1,    -1,   125,
      -1,    -1,    -1,    -1,    -1,    -1,   132,   133,   134,    -1,
      -1,   137,    -1,   139,     4,     5,    -1,    -1,     8,     9,
      -1,    -1,    -1,    13,    -1,    15,    -1,    17,    -1,    -1,
      -1,    -1,    22,    23,    -1,    25,    -1,    27,    28,    -1,
      -1,    31,    -1,    -1,    34,    35,    36,    37,    -1,    -1,
      -1,   177,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   187,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     196,    -1,    62,    63,    -1,    -1,    -1,    -1,    -1,    69,
      70,    71,    72,    73,    74,    75,    -1,    77,    78,    79,
      80,    81,    82,    -1,    -1,    85,    86,    87,    88,    89,
      90,    91,    -1,    -1,    -1,    95,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   115,   116,   117,   118,   119,
     120,    -1,    -1,    -1,    -1,   125,    -1,    -1,    -1,    -1,
      -1,    -1,   132,   133,   134,     4,     5,   137,    -1,     8,
       9,    -1,    -1,    -1,    13,    -1,    15,    -1,    17,    -1,
      -1,    -1,    -1,    22,    23,    -1,    25,    -1,    27,    28,
      -1,    -1,    31,    -1,    -1,    34,    35,    36,    37,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,    -1,
     180,    -1,    -1,    -1,    -1,    -1,    -1,   187,    -1,    -1,
      -1,    -1,    -1,    -1,    63,    -1,   196,    -1,    -1,    -1,
      69,    70,    71,    72,    73,    74,    75,    -1,    77,    78,
      79,    80,    81,    82,    -1,    -1,    85,    86,    87,    88,
      89,    90,    91,    -1,    -1,    -1,    95,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   115,   116,   117,   118,
     119,   120,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   132,   133,   134,    -1,    -1,   137,    -1,
      -1,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    13,    -1,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    -1,    39,   177,    41,
      42,    -1,    -1,   182,    -1,    -1,    -1,    49,   187,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   196,    -1,    -1,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      -1,    93,    94,    95,    96,    97,    98,    99,   100,   101,
      -1,    -1,   104,   105,    -1,    -1,    -1,    -1,    -1,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
      -1,   123,    -1,   125,    -1,    -1,    -1,    -1,   130,   131,
     132,   133,   134,   135,    -1,   137,   138,   139,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   177,    -1,    -1,    -1,    -1,
      -1,    -1,   184,   185,   186,   187,   188,   189,   190,   191,
       3,     4,     5,     6,     7,     8,     9,    10,    11,    12,
      13,    -1,    15,    16,    17,    18,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    -1,    39,    -1,    41,    42,
      -1,    -1,    -1,    -1,    -1,    -1,    49,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    62,
      63,    64,    65,    66,    67,    -1,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    -1,
      93,    94,    95,    96,    97,    98,    99,   100,   101,    -1,
      -1,   104,   105,    -1,    -1,    -1,    -1,    -1,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,    -1,
     123,    -1,   125,    -1,    -1,    -1,    -1,   130,   131,   132,
     133,   134,   135,    -1,   137,   138,   139,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   177,    -1,    -1,    -1,    -1,    -1,
      -1,   184,   185,   186,   187,   188,   189,   190,   191,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    13,
      -1,    15,    16,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    -1,    39,    -1,    41,    42,    -1,
      -1,    -1,    -1,    -1,    -1,    49,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    62,    63,
      64,    65,    66,    67,    -1,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    -1,    93,
      94,    95,    96,    97,    98,    99,   100,   101,    -1,    -1,
     104,   105,    -1,    -1,    -1,    -1,    -1,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,    -1,   123,
      -1,   125,    -1,    -1,    -1,    -1,   130,   131,   132,   133,
     134,   135,    -1,   137,   138,   139,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   177,    -1,    -1,    -1,    -1,    -1,    -1,
     184,    -1,   186,   187,   188,   189,   190,   191,     3,     4,
       5,    -1,    -1,     8,     9,    -1,    -1,    -1,    13,    -1,
      15,    16,    17,    -1,    -1,    -1,    21,    22,    23,    24,
      25,    -1,    27,    28,    29,    30,    31,    -1,    33,    34,
      35,    36,    37,    -1,    -1,    -1,    41,    42,    -1,    -1,
      -1,    -1,    -1,    -1,    49,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    62,    63,    64,
      65,    66,    67,    -1,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    -1,
      85,    86,    87,    88,    89,    90,    91,    -1,    -1,    -1,
      95,    96,    97,    98,    99,   100,    -1,    -1,    -1,    -1,
     105,    -1,    -1,    -1,    -1,    -1,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,    -1,    -1,    -1,    -1,
     125,    -1,    -1,    -1,    -1,   130,   131,   132,   133,   134,
     135,     3,   137,   138,   139,    -1,    -1,     9,    -1,    -1,
      -1,    -1,    -1,    -1,    16,    -1,    -1,    -1,    -1,    21,
      -1,    -1,    24,    25,    -1,    -1,    -1,    29,    30,    -1,
      -1,    33,    -1,    -1,    -1,    37,    -1,    -1,    -1,    41,
      42,    -1,   177,    -1,    -1,    -1,    -1,    49,    -1,    -1,
      -1,   186,   187,   188,   189,   190,   191,    -1,    -1,    -1,
      62,    -1,    64,    65,    66,    67,    -1,    -1,    -1,    -1,
      -1,    73,    74,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    83,    -1,    85,    86,    87,    88,    89,    90,    91,
      -1,    -1,    -1,    95,    96,    97,    98,    99,   100,    -1,
      -1,    -1,    -1,   105,    -1,    -1,    -1,    -1,    -1,   111,
     112,   113,   114,    -1,    -1,    -1,    -1,   119,    -1,    -1,
      -1,    -1,    -1,   125,    -1,    -1,    -1,    -1,   130,   131,
     132,   133,     3,   135,    -1,   137,    -1,   139,     9,    -1,
      -1,    -1,    -1,    -1,    -1,    16,    -1,    -1,    -1,    -1,
      21,    -1,    -1,    24,    25,    -1,    -1,    -1,    29,    30,
      -1,    -1,    33,    -1,    -1,    -1,    37,    -1,    -1,    -1,
      41,    42,    -1,    -1,    -1,   177,    -1,    -1,    49,    -1,
      -1,   183,    -1,    -1,   186,   187,   188,   189,   190,   191,
      -1,    62,    -1,    64,    65,    66,    67,    -1,    -1,    -1,
      -1,    -1,    73,    74,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    83,    -1,    85,    86,    87,    88,    89,    90,
      91,     9,    -1,    -1,    95,    96,    97,    98,    99,   100,
      -1,    -1,    -1,    -1,   105,    -1,    -1,    25,    -1,    -1,
     111,   112,   113,   114,    -1,    -1,    -1,    -1,   119,    37,
      -1,    -1,    -1,    -1,   125,    -1,    -1,    -1,    -1,   130,
     131,   132,   133,    -1,   135,    -1,   137,    -1,   139,    -1,
      -1,    -1,    -1,    -1,    62,    -1,    -1,     9,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    73,    74,    -1,    -1,    -1,
      -1,    -1,    -1,    25,    -1,    -1,    -1,    85,    86,    87,
      88,    89,    90,    91,    -1,    37,   177,    95,    -1,    -1,
      -1,    -1,   183,    -1,    -1,   186,   187,   188,   189,   190,
     191,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      62,   119,    -1,    -1,    -1,    -1,    -1,   125,    -1,    -1,
      -1,    73,    74,    -1,   132,   133,     9,    -1,    -1,   137,
      -1,    -1,    -1,    85,    86,    87,    88,    89,    90,    91,
      -1,    -1,    25,    95,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    37,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    29,    -1,    -1,    -1,   119,    -1,   177,
      -1,    -1,    -1,   125,   182,    41,    42,    -1,    -1,   187,
     132,   133,    -1,    49,    -1,   137,    -1,    -1,   196,    -1,
      73,    74,    -1,    -1,    -1,    -1,    62,    -1,    64,    65,
      66,    67,    85,    86,    87,    88,    89,    90,    91,    -1,
      -1,    -1,    95,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   177,    -1,    -1,    -1,    -1,
      96,    97,    98,    99,   100,   187,   119,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   196,   111,   112,   113,   114,   132,
     133,    -1,    -1,    -1,   137,    -1,    -1,    -1,    -1,   125,
      29,    -1,    -1,    -1,   130,   131,    -1,    -1,    -1,   135,
      -1,    -1,    41,    42,    -1,    -1,    -1,    -1,    -1,    -1,
      49,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    62,   177,    64,    65,    66,    67,   182,
      -1,    -1,    -1,    -1,   187,    -1,    -1,    -1,    -1,    -1,
      -1,   177,    -1,   196,    -1,    -1,    -1,    -1,    -1,    -1,
     186,   187,   188,   189,   190,   191,    -1,    96,    97,    98,
      99,   100,    -1,    -1,   200,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   111,   112,   113,   114,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   125,    -1,    -1,    -1,
      -1,   130,   131,    -1,    -1,    -1,   135,    29,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     9,    41,
      42,    -1,    -1,    -1,    -1,    -1,    -1,    49,    -1,    -1,
      -1,    -1,    -1,    -1,    25,    -1,    -1,    -1,    -1,    -1,
      62,    -1,    64,    65,    66,    67,    37,    -1,   177,    -1,
      -1,    -1,   181,   182,    -1,   184,    -1,   186,   187,   188,
     189,   190,   191,    85,    86,    87,    88,    89,    90,    91,
     199,    -1,    -1,    -1,    96,    97,    98,    99,   100,    -1,
      -1,    -1,    73,    74,    -1,    -1,    -1,    -1,    -1,   111,
     112,   113,   114,    -1,    85,    86,    87,    88,    89,    90,
      91,    -1,    -1,   125,    95,    -1,    -1,    -1,   130,   131,
      -1,    -1,    -1,   135,    29,   137,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    41,    42,   119,    -1,
      -1,    -1,    -1,    -1,    49,    -1,    -1,    -1,    -1,    -1,
      -1,   132,   133,    -1,    -1,    -1,   137,    62,    -1,    64,
      65,    66,    67,    -1,    -1,   177,    -1,    -1,    -1,    -1,
      -1,    -1,   184,    -1,   186,   187,   188,   189,   190,   191,
      85,    86,    87,    88,    89,    90,    91,    -1,    -1,    -1,
      -1,    96,    97,    98,    99,   100,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   111,   112,   113,   114,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     125,    29,    -1,    -1,    -1,   130,   131,    -1,    -1,    -1,
     135,    -1,   137,    41,    42,    -1,    -1,    -1,    -1,    -1,
      -1,    49,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    62,    63,    64,    65,    66,    67,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   177,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   186,   187,   188,   189,   190,   191,    -1,    96,    97,
      98,    99,   100,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   111,   112,   113,   114,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   125,    29,    -1,
      -1,    -1,   130,   131,    -1,    -1,    -1,   135,    -1,    -1,
      41,    42,    -1,    -1,    -1,    -1,    -1,    -1,    49,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    62,    63,    64,    65,    66,    67,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,
      -1,    -1,    -1,   181,   182,    -1,   184,   185,   186,   187,
     188,   189,   190,   191,    -1,    96,    97,    98,    99,   100,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     111,   112,   113,   114,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   125,    29,    -1,    -1,    -1,   130,
     131,    -1,    -1,    -1,   135,    -1,    -1,    41,    42,    -1,
      -1,    -1,    -1,    -1,    -1,    49,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    62,    63,
      64,    65,    66,    67,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,    -1,    -1,
     181,   182,    -1,   184,   185,   186,   187,   188,   189,   190,
     191,    -1,    96,    97,    98,    99,   100,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   111,   112,   113,
     114,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   125,    29,    -1,    -1,    -1,   130,   131,    -1,    -1,
      -1,   135,    -1,    -1,    41,    42,    -1,    -1,    -1,    -1,
      -1,    -1,    49,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    62,    -1,    64,    65,    66,
      67,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   177,    -1,    -1,    -1,   181,   182,    -1,
     184,    -1,   186,   187,   188,   189,   190,   191,    -1,    96,
      97,    98,    99,   100,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   111,   112,   113,   114,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   125,    29,
      -1,    -1,    -1,   130,   131,    -1,    -1,    -1,   135,    -1,
      -1,    41,    42,    -1,    -1,    -1,    -1,    -1,    -1,    49,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    62,    -1,    64,    65,    66,    67,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     177,    -1,    -1,    -1,    -1,    -1,   183,    -1,    -1,   186,
     187,   188,   189,   190,   191,    -1,    96,    97,    98,    99,
     100,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   111,   112,   113,   114,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   125,    29,    -1,    -1,    -1,
     130,   131,    -1,    -1,    -1,   135,    -1,    -1,    41,    42,
      -1,    -1,    -1,    -1,    -1,    -1,    49,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    62,
      -1,    64,    65,    66,    67,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,   178,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   186,   187,   188,   189,
     190,   191,    -1,    96,    97,    98,    99,   100,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   111,   112,
     113,   114,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   125,    29,    -1,    -1,    -1,   130,   131,    -1,
      -1,    -1,   135,    -1,    -1,    41,    42,    -1,    -1,    -1,
      -1,    -1,    -1,    49,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    62,    -1,    64,    65,
      66,    67,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   177,    -1,    -1,   180,    -1,    -1,
      -1,    -1,    -1,   186,   187,   188,   189,   190,   191,    -1,
      96,    97,    98,    99,   100,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   111,   112,   113,   114,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   125,
      29,    -1,    -1,    -1,   130,   131,    -1,    -1,    -1,   135,
      -1,    -1,    41,    42,    -1,    -1,    -1,    -1,    -1,    -1,
      49,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    62,    63,    64,    65,    66,    67,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   177,    -1,    -1,    -1,    -1,    -1,    -1,   184,    -1,
     186,   187,   188,   189,   190,   191,    -1,    96,    97,    98,
      99,   100,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   111,   112,   113,   114,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   125,    29,    -1,    -1,
      -1,   130,   131,    -1,    -1,    -1,   135,    -1,    -1,    41,
      42,    -1,    -1,    -1,    -1,    -1,    -1,    49,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      62,    -1,    64,    65,    66,    67,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   186,   187,   188,
     189,   190,   191,    -1,    96,    97,    98,    99,   100,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   111,
     112,   113,   114,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   125,    29,    -1,    -1,    -1,   130,   131,
      -1,    -1,    -1,   135,    -1,    -1,    41,    42,    -1,    -1,
      -1,    -1,    -1,    -1,    49,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    62,    -1,    64,
      65,    66,    67,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   177,    -1,    -1,    -1,    -1,
      -1,   183,    -1,    -1,   186,   187,   188,   189,   190,   191,
      -1,    96,    97,    98,    99,   100,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   111,   112,   113,   114,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     125,    29,    -1,    -1,    -1,   130,   131,    -1,    -1,    -1,
     135,    -1,    -1,    41,    42,    -1,    -1,    -1,    -1,    -1,
      -1,    49,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    62,    -1,    64,    65,    66,    67,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   177,    -1,    -1,    -1,    -1,    -1,    -1,   184,
      -1,   186,   187,   188,   189,   190,   191,    -1,    96,    97,
      98,    99,   100,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   111,   112,   113,   114,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   125,    29,    -1,
      -1,    -1,   130,   131,    -1,    -1,    -1,   135,    -1,    -1,
      41,    42,    -1,    -1,    -1,    -1,    -1,    -1,    49,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    62,    -1,    64,    65,    66,    67,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   177,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   186,   187,
     188,   189,   190,   191,    -1,    96,    97,    98,    99,   100,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     111,   112,   113,   114,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   125,    29,    -1,    -1,    -1,   130,
     131,    29,    -1,    -1,   135,    -1,    -1,    41,    42,    -1,
      -1,    -1,    -1,    41,    42,    49,    -1,    -1,    -1,    -1,
      -1,    49,    -1,    -1,    -1,    -1,    -1,    -1,    62,    -1,
      64,    65,    66,    67,    62,    -1,    64,    65,    66,    67,
      -1,    -1,    -1,    -1,    -1,    -1,   177,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   186,   187,   188,   189,   190,
     191,    -1,    96,    97,    98,    99,   100,    -1,    96,    97,
      98,    99,   100,    -1,    -1,    -1,    -1,   111,   112,   113,
     114,    -1,    -1,   111,   112,   113,   114,    -1,    -1,    -1,
      -1,   125,    -1,    -1,    -1,    -1,    -1,   125,    -1,    -1,
      -1,   135,    -1,    -1,     3,     4,     5,   135,    -1,     8,
       9,    -1,    -1,    -1,    13,    -1,    15,    16,    17,    -1,
      -1,    -1,    21,    22,    23,    24,    25,    -1,    27,    28,
      -1,    30,    31,    -1,    33,    34,    35,    36,    37,    -1,
      -1,    -1,    -1,   177,    -1,    -1,    -1,    -1,    -1,   177,
      -1,    -1,   186,   187,   188,   189,   190,   191,   186,   187,
     188,   189,   190,   191,    63,    -1,    -1,    -1,    -1,    -1,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    -1,    85,    86,    87,    88,
      89,    90,    91,    -1,    -1,    -1,    95,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   105,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   115,   116,   117,   118,
     119,   120,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   132,   133,   134,    -1,    -1,   137,   138,
     139,     3,     4,     5,    -1,    -1,     8,     9,    -1,    -1,
      -1,    13,    -1,    15,    16,    17,    -1,    -1,    -1,    21,
      22,    23,    24,    25,    -1,    27,    28,    -1,    30,    31,
      -1,    33,    34,    35,    36,    37,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    51,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    63,    -1,    -1,    -1,    -1,    -1,    69,    70,    71,
      72,    73,    74,    75,    -1,    77,    78,    79,    80,    81,
      82,    83,    -1,    85,    86,    87,    88,    89,    90,    91,
      -1,    -1,    -1,    95,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   105,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   115,   116,   117,   118,   119,   120,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     132,   133,   134,    -1,    -1,   137,    -1,   139,     3,     4,
       5,    -1,    -1,     8,     9,    -1,    -1,    -1,    13,    -1,
      15,    16,    17,    -1,    -1,    -1,    21,    22,    23,    24,
      25,    -1,    27,    28,    -1,    30,    31,    -1,    33,    34,
      35,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    63,    -1,
      -1,    -1,    -1,    -1,    69,    70,    71,    72,    73,    74,
      75,    -1,    77,    78,    79,    80,    81,    82,    83,    -1,
      85,    86,    87,    88,    89,    90,    91,    -1,    -1,    -1,
      95,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     105,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     115,   116,   117,   118,   119,   120,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   132,   133,   134,
      -1,    -1,   137,    -1,   139,     3,     4,     5,    -1,    -1,
       8,     9,    -1,    -1,    -1,    13,    -1,    15,    16,    17,
      -1,    -1,    -1,    21,    22,    23,    24,    25,    -1,    27,
      28,    -1,    30,    31,    -1,    33,    34,    35,    36,    37,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    63,    -1,    -1,    -1,    -1,
      -1,    69,    70,    71,    72,    73,    74,    75,    -1,    77,
      78,    79,    80,    81,    82,    83,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    95,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   105,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   115,   116,   117,
     118,   119,   120,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   132,   133,   134,     3,     4,     5,
      -1,   139,     8,     9,    -1,    -1,    -1,    13,    -1,    15,
      16,    17,    -1,    -1,    -1,    21,    22,    23,    24,    25,
      -1,    27,    28,    -1,    30,    31,    -1,    33,    34,    35,
      36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    63,    -1,    -1,
      -1,    -1,    -1,    69,    70,    71,    72,    73,    74,    -1,
      -1,    77,    78,    79,    80,    81,    82,    83,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    95,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   105,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     116,   117,   118,   119,   120,    -1,    -1,    -1,     3,     4,
       5,    -1,    -1,     8,     9,    -1,   132,   133,    13,    -1,
      -1,    16,    17,   139,    -1,    -1,    21,    22,    23,    24,
      25,    -1,    27,    28,    -1,    30,    -1,    -1,    33,    -1,
      35,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    69,    70,    71,    72,    73,    74,
      -1,    -1,    77,    78,    79,    80,    81,    82,    83,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      95,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     105,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   116,   117,   118,   119,   120,    -1,    -1,    -1,    -1,
       4,     5,    -1,    -1,     8,     9,    -1,   132,   133,    13,
      -1,    15,    -1,    17,   139,    -1,    -1,    -1,    22,    23,
      -1,    25,    -1,    27,    28,    -1,    -1,    31,    -1,    -1,
      34,    35,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    63,
      -1,    -1,    -1,    -1,    -1,    69,    70,    71,    72,    73,
      74,    75,    -1,    77,    78,    79,    80,    81,    82,    -1,
      -1,    85,    86,    87,    88,    89,    90,    91,    -1,    -1,
      -1,    95,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   115,   116,   117,   118,   119,   120,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   132,   133,
     134,     4,     5,   137,    -1,     8,     9,    -1,    -1,    -1,
      13,    -1,    15,    -1,    17,    -1,    -1,    -1,    -1,    22,
      23,    -1,    25,    -1,    27,    28,    -1,    -1,    31,    -1,
      -1,    34,    35,    36,    37,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      63,    -1,    -1,    -1,    -1,    -1,    69,    70,    71,    72,
      73,    74,    75,    -1,    77,    78,    79,    80,    81,    82,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    95,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   115,   116,   117,   118,   119,   120,    -1,     4,
       5,    -1,    -1,     8,     9,    -1,    -1,    -1,    13,   132,
     133,   134,    17,    -1,    -1,    -1,    -1,    22,    23,    -1,
      25,    -1,    27,    28,    -1,    -1,    -1,    -1,    -1,    -1,
      35,    36,    37,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    69,    70,    71,    72,    73,    74,
      -1,    -1,    77,    78,    79,    80,    81,    82,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      95,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   116,   117,   118,   119,   120,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   132,   133
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint16 yystos[] =
{
       0,     3,     4,     5,     8,     9,    13,    15,    16,    17,
      21,    22,    23,    24,    25,    27,    28,    30,    31,    33,
      34,    35,    36,    37,    62,    63,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    95,
     105,   115,   116,   117,   118,   119,   120,   125,   132,   133,
     134,   137,   138,   139,   177,   187,   196,   200,   202,   203,
     204,   242,   243,   244,   251,   255,   256,   257,   258,   261,
     262,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   281,   283,   284,   285,   286,   288,   292,
     297,   300,   310,   313,   365,   366,   367,   368,   369,   380,
     389,   390,   391,   392,   177,   177,   187,   204,   381,   382,
     383,   384,   385,   386,   387,   389,   177,   177,    92,   177,
      92,    92,   177,   177,    92,   177,   177,   194,   182,   182,
     182,    67,   177,   177,   177,   390,   392,   258,   259,   261,
     263,   300,   389,   389,     0,   298,   299,   300,   200,   179,
     200,   179,   200,   200,   381,   200,   381,   204,   260,   261,
     274,   276,   283,   284,   285,   300,   389,   204,   261,   274,
     276,   283,   284,   285,   300,   389,   260,   284,   261,   283,
     284,   260,   261,   283,   260,   260,   260,   261,   283,   258,
     283,   258,   283,   298,   298,   298,   367,   334,   335,   370,
     177,   182,   395,   397,   399,    29,    41,    42,    49,    64,
      65,    66,    67,    96,    97,    98,    99,   100,   111,   112,
     113,   114,   130,   131,   135,   177,   186,   187,   188,   189,
     190,   191,   203,   205,   206,   207,   208,   209,   210,   211,
     214,   215,   216,   218,   219,   220,   223,   224,   225,   226,
     227,   228,   229,   230,   231,   232,   233,   234,   235,   236,
     237,   238,   239,   298,   324,   177,   204,   384,   386,   388,
     177,   259,   383,   386,   395,    84,   248,   249,   250,   300,
     208,   177,   239,   239,   203,   239,    25,    62,    63,   178,
     179,   278,   279,   280,   324,   239,   239,   239,   239,   324,
     324,   238,   178,   178,   261,   263,   300,   389,   300,   389,
     381,   250,   250,   298,   298,   298,   298,   245,   298,   258,
     258,   298,   246,   298,   298,   298,   298,   298,   298,   298,
     298,   298,   298,   298,   281,   282,   203,   204,   311,   323,
     184,   178,   396,   398,   183,   187,   238,   240,   261,   264,
     265,   283,   300,   182,   177,   223,   177,   223,   223,   323,
     345,   177,   177,   177,   177,   177,   223,   335,   335,   335,
     335,   224,   224,   177,   239,   324,   334,   224,   224,   224,
     224,   224,   224,    40,    41,    42,   177,   181,   182,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    61,   199,
     187,   192,   193,   188,   189,    43,    44,    45,    46,   194,
     195,    47,    48,   186,   196,   197,    49,    50,   126,   127,
     198,   178,   179,   256,   258,   267,   269,   273,   275,   277,
     178,   388,   178,   178,   395,   388,   177,   383,   386,    19,
      37,   350,   248,   199,   178,     9,   203,   295,   296,   178,
     178,   178,   178,   177,   178,   279,   195,   183,   183,   183,
     178,   178,   178,   179,   395,   247,   250,   252,   253,   199,
     325,   325,   289,   323,   184,   298,   312,     6,     7,    10,
      11,    12,    18,    19,    20,    26,    32,    39,    68,    84,
      93,    94,   101,   104,   121,   123,   185,   203,   204,   239,
     241,   242,   255,   256,   257,   258,   323,   331,   332,   333,
     334,   336,   337,   338,   340,   342,   343,   346,   347,   348,
     349,   256,   258,   261,   267,   273,   283,   372,   373,   374,
     375,   376,   377,   378,   379,   203,   319,   320,   255,   256,
     257,   258,   318,   321,   322,   183,   183,   183,   240,   264,
     187,   240,   324,   324,   324,   238,   324,   324,   324,   184,
     184,   242,   242,   238,   178,   178,   178,   203,   204,   221,
     178,   222,   238,   221,   239,   238,   238,   238,   238,   238,
     238,   238,   238,   238,   238,   238,   224,   224,   224,   224,
     225,   225,   226,   226,   227,   227,   227,   227,   228,   228,
     229,   230,   231,   232,   233,   235,   235,   180,   239,   238,
     177,   187,   196,   393,   395,   400,   402,   393,   178,   395,
     395,   178,   178,   388,    37,    19,   177,   184,   240,   326,
     200,   177,   178,   179,    92,    92,    92,    64,    67,   278,
     182,   182,   238,   325,   254,   325,   325,   326,   184,   290,
     291,   314,   315,   316,   323,   184,   200,   240,   200,   180,
     331,   341,   204,   239,   177,   200,   239,   177,   177,   185,
     350,   344,   345,    68,   184,   334,   200,   334,   200,   381,
     381,   389,   389,   180,   185,   331,   381,   283,   283,   283,
     373,   204,   260,   284,   292,   313,   260,   284,   260,   381,
     179,   200,   178,   179,   177,   187,   196,   383,   389,   394,
     397,   401,   403,   383,   389,   394,   177,   187,   389,   394,
     389,   394,   178,   179,   183,   183,   183,   178,   178,   178,
     179,   179,   179,   178,   242,   242,   177,   210,   210,   179,
     184,   224,   178,   179,   183,   237,   180,   395,   400,   402,
     259,   393,   178,   208,   351,   352,   181,   182,   203,   221,
     326,   327,   328,   329,   330,   238,   293,   294,   178,   295,
     178,   178,   199,   239,   239,   178,   325,   200,   243,   298,
     301,   302,   303,   304,   305,   184,   298,   185,   179,   298,
     314,    51,   180,   331,    39,   177,   200,   200,   239,   200,
     239,   241,   177,   184,   179,   200,    68,   102,   103,   122,
     124,   331,   298,   298,   298,   298,   323,   323,   298,   298,
     298,   381,   371,   372,   320,   178,   401,   403,   259,   394,
     298,   298,   259,   298,   298,    51,   322,   324,   324,   217,
     221,   239,   239,    11,   212,   213,   324,   327,   328,   238,
     237,   178,   178,   178,   393,   178,   180,   353,   221,   239,
     180,   179,   185,   181,   182,   199,   326,   179,   178,   278,
     183,   183,   200,   256,   258,   185,   303,   179,   200,   179,
     200,   301,   298,   316,   199,   317,   185,   240,   331,   177,
     332,   337,   339,   178,   178,   178,   351,    68,   345,   185,
     334,   177,   334,   334,   298,   298,   178,   178,   394,   178,
     178,   178,   181,   182,   185,   185,   180,   178,   179,   180,
     179,   185,   395,   395,   182,   208,   354,   355,   180,   356,
      51,   183,   326,   185,   329,   221,   239,   326,   238,   178,
     179,   180,   306,   309,   381,   307,   309,   389,   298,   307,
     306,   185,   240,   298,   180,   239,   241,   331,   331,   331,
     178,   185,   239,   177,   397,   221,   239,   238,   213,   238,
     185,   323,   177,   179,   182,   208,   357,   358,   180,   359,
     239,    51,   183,   278,   240,   298,   308,   309,   298,   308,
     287,   298,   331,   178,   200,    14,   200,   178,   183,   183,
     239,   354,   323,   177,   179,   208,   360,   361,   180,   362,
     183,   239,   199,   298,   298,   287,   200,   241,   331,   334,
     208,   178,   183,   239,   357,   179,   345,   363,   364,   183,
     278,   178,   177,   208,   178,   360,   179,   178,   331,   239,
     177,   364,   178,   239,   178
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *bottom, yytype_int16 *top)
#else
static void
yy_stack_print (bottom, top)
    yytype_int16 *bottom;
    yytype_int16 *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      fprintf (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      fprintf (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
  
  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;
#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  yytype_int16 yyssa[YYINITDEPTH];
  yytype_int16 *yyss = yyssa;
  yytype_int16 *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     look-ahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to look-ahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 4:
#line 250 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (2)]);
          stack((yyval)).id(ID_symbol);
          irep_idt value=stack((yyvsp[(2) - (2)])).get(ID_value);
          stack((yyval)).set(ID_C_base_name, value);
          stack((yyval)).set(ID_identifier, value);
          stack((yyval)).set(ID_C_id_class, ANSI_C_SYMBOL);
        }
    break;

  case 16:
#line 294 "parser.y"
    { (yyval) = (yyvsp[(2) - (3)]); }
    break;

  case 23:
#line 305 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (6)]);
          set((yyval), ID_generic_selection);
          mto((yyval), (yyvsp[(3) - (6)]));
          stack((yyval)).add(ID_generic_associations).get_sub().swap((irept::subt&)stack((yyvsp[(5) - (6)])).operands());
        }
    break;

  case 24:
#line 315 "parser.y"
    {
          init((yyval)); mto((yyval), (yyvsp[(1) - (1)]));
        }
    break;

  case 25:
#line 319 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]); mto((yyval), (yyvsp[(3) - (3)]));
        }
    break;

  case 26:
#line 326 "parser.y"
    {
          (yyval)=(yyvsp[(2) - (3)]);
          stack((yyval)).id(ID_generic_association);
          stack((yyval)).set(ID_type_arg, stack((yyvsp[(1) - (3)])));
          stack((yyval)).set(ID_value, stack((yyvsp[(3) - (3)])));
        }
    break;

  case 27:
#line 333 "parser.y"
    {
          (yyval)=(yyvsp[(2) - (3)]);
          stack((yyval)).id(ID_generic_association);
          stack((yyval)).set(ID_type_arg, irept(ID_default));
          stack((yyval)).set(ID_value, stack((yyvsp[(3) - (3)])));
        }
    break;

  case 28:
#line 343 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (6)]);
          stack((yyval)).id(ID_gcc_builtin_va_arg);
          mto((yyval), (yyvsp[(3) - (6)]));
          stack((yyval)).type().swap(stack((yyvsp[(5) - (6)])));
        }
    break;

  case 29:
#line 351 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (6)]);
          stack((yyval)).id(ID_gcc_builtin_types_compatible_p);
          typet &type_arg=(typet &)(stack((yyval)).add(ID_type_arg));
          typet::subtypest &subtypes=type_arg.subtypes();
          subtypes.resize(2);
          subtypes[0].swap(stack((yyvsp[(3) - (6)])));
          subtypes[1].swap(stack((yyvsp[(5) - (6)])));
        }
    break;

  case 30:
#line 364 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (4)]);
          stack((yyval)).id(ID_cw_va_arg_typeof);
          stack((yyval)).add(ID_type_arg).swap(stack((yyvsp[(3) - (4)])));
        }
    break;

  case 31:
#line 373 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (6)]);
          stack((yyval)).id(ID_builtin_offsetof);
          stack((yyval)).add(ID_type_arg).swap(stack((yyvsp[(3) - (6)])));
          stack((yyval)).add(ID_designator).swap(stack((yyvsp[(5) - (6)])));
        }
    break;

  case 32:
#line 383 "parser.y"
    {
          init((yyval), ID_designated_initializer);
          stack((yyval)).operands().resize(1);
          stack((yyval)).op0().id(ID_member);
          stack((yyval)).op0().add_source_location()=stack((yyvsp[(1) - (1)])).source_location();
          stack((yyval)).op0().set(ID_component_name, stack((yyvsp[(1) - (1)])).get(ID_C_base_name));
        }
    break;

  case 33:
#line 391 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          set((yyvsp[(2) - (3)]), ID_member);
          stack((yyvsp[(2) - (3)])).set(ID_component_name, stack((yyvsp[(3) - (3)])).get(ID_C_base_name));
          mto((yyval), (yyvsp[(2) - (3)]));
        }
    break;

  case 34:
#line 398 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (4)]);
          set((yyvsp[(2) - (4)]), ID_index);
          mto((yyvsp[(2) - (4)]), (yyvsp[(3) - (4)]));
          mto((yyval), (yyvsp[(2) - (4)]));
        }
    break;

  case 35:
#line 408 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (6)]);
          set((yyval), ID_forall);
          mto((yyval), (yyvsp[(4) - (6)]));
          mto((yyval), (yyvsp[(5) - (6)]));
          PARSER.pop_scope();
        }
    break;

  case 36:
#line 416 "parser.y"
    {
          // The precedence of this operator is too high; it is meant
          // to bind only very weakly.
          (yyval)=(yyvsp[(1) - (4)]);
          set((yyval), ID_forall);
          mto((yyval), (yyvsp[(3) - (4)]));
          mto((yyval), (yyvsp[(4) - (4)]));
          PARSER.pop_scope();
        }
    break;

  case 37:
#line 426 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (6)]);
          set((yyval), ID_exists);
          mto((yyval), (yyvsp[(4) - (6)]));
          mto((yyval), (yyvsp[(5) - (6)]));
          PARSER.pop_scope();
        }
    break;

  case 38:
#line 434 "parser.y"
    {
          // The precedence of this operator is too high; it is meant
          // to bind only very weakly.
          (yyval)=(yyvsp[(1) - (4)]);
          set((yyval), ID_exists);
          mto((yyval), (yyvsp[(3) - (4)]));
          mto((yyval), (yyvsp[(4) - (4)]));
          PARSER.pop_scope();
        }
    break;

  case 39:
#line 446 "parser.y"
    { 
          (yyval)=(yyvsp[(1) - (3)]);
          set((yyval), ID_side_effect);
          stack((yyval)).set(ID_statement, ID_statement_expression);
          mto((yyval), (yyvsp[(2) - (3)]));
        }
    break;

  case 41:
#line 457 "parser.y"
    { binary((yyval), (yyvsp[(1) - (4)]), (yyvsp[(2) - (4)]), ID_index, (yyvsp[(3) - (4)])); }
    break;

  case 42:
#line 459 "parser.y"
    { (yyval)=(yyvsp[(2) - (3)]);
          set((yyval), ID_side_effect);
          stack((yyval)).set(ID_statement, ID_function_call);
          stack((yyval)).operands().resize(2);
          stack((yyval)).op0().swap(stack((yyvsp[(1) - (3)])));
          stack((yyval)).op1().clear();
          stack((yyval)).op1().id(ID_arguments);
        }
    break;

  case 43:
#line 468 "parser.y"
    { (yyval)=(yyvsp[(2) - (4)]);
          set((yyval), ID_side_effect);
          stack((yyval)).set(ID_statement, ID_function_call);
          stack((yyval)).operands().resize(2);
          stack((yyval)).op0().swap(stack((yyvsp[(1) - (4)])));
          stack((yyval)).op1().swap(stack((yyvsp[(3) - (4)])));
          stack((yyval)).op1().id(ID_arguments);
        }
    break;

  case 44:
#line 477 "parser.y"
    { (yyval)=(yyvsp[(2) - (3)]);
          set((yyval), ID_member);
          mto((yyval), (yyvsp[(1) - (3)]));
          stack((yyval)).set(ID_component_name, stack((yyvsp[(3) - (3)])).get(ID_C_base_name));
        }
    break;

  case 45:
#line 483 "parser.y"
    { (yyval)=(yyvsp[(2) - (3)]);
          set((yyval), ID_ptrmember);
          mto((yyval), (yyvsp[(1) - (3)]));
          stack((yyval)).set(ID_component_name, stack((yyvsp[(3) - (3)])).get(ID_C_base_name));
        }
    break;

  case 46:
#line 489 "parser.y"
    { (yyval)=(yyvsp[(2) - (2)]);
          set((yyval), ID_side_effect);
          stack((yyval)).set(ID_statement, ID_postincrement);
          mto((yyval), (yyvsp[(1) - (2)]));
        }
    break;

  case 47:
#line 495 "parser.y"
    { (yyval)=(yyvsp[(2) - (2)]);
          set((yyval), ID_side_effect);
          stack((yyval)).set(ID_statement, ID_postdecrement);
          mto((yyval), (yyvsp[(1) - (2)]));
        }
    break;

  case 48:
#line 502 "parser.y"
    {
          exprt tmp(ID_initializer_list);
          tmp.add_source_location()=stack((yyvsp[(4) - (6)])).source_location();
          tmp.operands().swap(stack((yyvsp[(5) - (6)])).operands());
          (yyval)=(yyvsp[(1) - (6)]);
          set((yyval), ID_typecast);
          stack((yyval)).move_to_operands(tmp);
          stack((yyval)).type().swap(stack((yyvsp[(2) - (6)])));
        }
    break;

  case 49:
#line 512 "parser.y"
    {
          // same as above
          exprt tmp(ID_initializer_list);
          tmp.add_source_location()=stack((yyvsp[(4) - (7)])).source_location();
          tmp.operands().swap(stack((yyvsp[(5) - (7)])).operands());
          (yyval)=(yyvsp[(1) - (7)]);
          set((yyval), ID_typecast);
          stack((yyval)).move_to_operands(tmp);
          stack((yyval)).type().swap(stack((yyvsp[(2) - (7)])));
        }
    break;

  case 52:
#line 531 "parser.y"
    {
          init((yyval), ID_expression_list);
          mto((yyval), (yyvsp[(1) - (1)]));
        }
    break;

  case 53:
#line 536 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          mto((yyval), (yyvsp[(3) - (3)]));
        }
    break;

  case 55:
#line 545 "parser.y"
    { (yyval)=(yyvsp[(1) - (2)]);
          set((yyval), ID_side_effect);
          stack((yyval)).set(ID_statement, ID_preincrement);
          mto((yyval), (yyvsp[(2) - (2)]));
        }
    break;

  case 56:
#line 551 "parser.y"
    { (yyval)=(yyvsp[(1) - (2)]);
          set((yyval), ID_side_effect);
          stack((yyval)).set(ID_statement, ID_predecrement);
          mto((yyval), (yyvsp[(2) - (2)]));
        }
    break;

  case 57:
#line 557 "parser.y"
    { (yyval)=(yyvsp[(1) - (2)]);
          set((yyval), ID_address_of);
          mto((yyval), (yyvsp[(2) - (2)]));
        }
    break;

  case 58:
#line 562 "parser.y"
    { // this takes the address of a label (a gcc extension)
          (yyval)=(yyvsp[(1) - (2)]);
          irep_idt identifier=PARSER.lookup_label(stack((yyvsp[(2) - (2)])).get(ID_C_base_name));
          set((yyval), ID_address_of);
          stack((yyval)).operands().resize(1);
          stack((yyval)).op0()=stack((yyvsp[(2) - (2)]));
          stack((yyval)).op0().id(ID_label);
          stack((yyval)).op0().set(ID_identifier, identifier);
        }
    break;

  case 59:
#line 572 "parser.y"
    { (yyval)=(yyvsp[(1) - (2)]);
          set((yyval), ID_dereference);
          mto((yyval), (yyvsp[(2) - (2)]));
        }
    break;

  case 60:
#line 577 "parser.y"
    { (yyval)=(yyvsp[(1) - (2)]);
          set((yyval), ID_unary_plus);
          mto((yyval), (yyvsp[(2) - (2)]));
        }
    break;

  case 61:
#line 582 "parser.y"
    { (yyval)=(yyvsp[(1) - (2)]);
          set((yyval), ID_unary_minus);
          mto((yyval), (yyvsp[(2) - (2)]));
        }
    break;

  case 62:
#line 587 "parser.y"
    { (yyval)=(yyvsp[(1) - (2)]);
          set((yyval), ID_bitnot);
          mto((yyval), (yyvsp[(2) - (2)]));
        }
    break;

  case 63:
#line 592 "parser.y"
    { (yyval)=(yyvsp[(1) - (2)]);
          set((yyval), ID_not);
          mto((yyval), (yyvsp[(2) - (2)]));
        }
    break;

  case 64:
#line 597 "parser.y"
    { (yyval)=(yyvsp[(1) - (2)]);
          set((yyval), ID_sizeof);
          mto((yyval), (yyvsp[(2) - (2)]));
        }
    break;

  case 65:
#line 602 "parser.y"
    { (yyval)=(yyvsp[(1) - (4)]);
          set((yyval), ID_sizeof);
          stack((yyval)).add(ID_type_arg).swap(stack((yyvsp[(3) - (4)])));
        }
    break;

  case 66:
#line 607 "parser.y"
    { // note no parentheses for expressions, just like sizeof
          (yyval)=(yyvsp[(1) - (2)]);
          set((yyval), ID_alignof);
          mto((yyval), (yyvsp[(2) - (2)]));
        }
    break;

  case 67:
#line 613 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (4)]);
          stack((yyval)).id(ID_alignof);
          stack((yyval)).add(ID_type_arg).swap(stack((yyvsp[(3) - (4)])));
        }
    break;

  case 69:
#line 623 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (4)]);
          set((yyval), ID_typecast);
          mto((yyval), (yyvsp[(4) - (4)]));
          stack((yyval)).type().swap(stack((yyvsp[(2) - (4)])));
        }
    break;

  case 70:
#line 630 "parser.y"
    { (yyval)=(yyvsp[(1) - (2)]);
          set((yyval), ID_complex_real);
          mto((yyval), (yyvsp[(2) - (2)]));
        }
    break;

  case 71:
#line 635 "parser.y"
    { (yyval)=(yyvsp[(1) - (2)]);
          set((yyval), ID_complex_imag);
          mto((yyval), (yyvsp[(2) - (2)]));
        }
    break;

  case 73:
#line 644 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_mult, (yyvsp[(3) - (3)])); }
    break;

  case 74:
#line 646 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_div, (yyvsp[(3) - (3)])); }
    break;

  case 75:
#line 648 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_mod, (yyvsp[(3) - (3)])); }
    break;

  case 77:
#line 654 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_plus, (yyvsp[(3) - (3)])); }
    break;

  case 78:
#line 656 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_minus, (yyvsp[(3) - (3)])); }
    break;

  case 80:
#line 662 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_shl, (yyvsp[(3) - (3)])); }
    break;

  case 81:
#line 664 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_shr, (yyvsp[(3) - (3)])); }
    break;

  case 83:
#line 670 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_lt, (yyvsp[(3) - (3)])); }
    break;

  case 84:
#line 672 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_gt, (yyvsp[(3) - (3)])); }
    break;

  case 85:
#line 674 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_le, (yyvsp[(3) - (3)])); }
    break;

  case 86:
#line 676 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_ge, (yyvsp[(3) - (3)])); }
    break;

  case 88:
#line 682 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_equal, (yyvsp[(3) - (3)])); }
    break;

  case 89:
#line 684 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_notequal, (yyvsp[(3) - (3)])); }
    break;

  case 91:
#line 690 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_bitand, (yyvsp[(3) - (3)])); }
    break;

  case 93:
#line 696 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_bitxor, (yyvsp[(3) - (3)])); }
    break;

  case 95:
#line 702 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_bitor, (yyvsp[(3) - (3)])); }
    break;

  case 97:
#line 708 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_and, (yyvsp[(3) - (3)])); }
    break;

  case 99:
#line 714 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_or, (yyvsp[(3) - (3)])); }
    break;

  case 101:
#line 723 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_implies, (yyvsp[(3) - (3)])); }
    break;

  case 103:
#line 732 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_equal, (yyvsp[(3) - (3)])); }
    break;

  case 105:
#line 738 "parser.y"
    { (yyval)=(yyvsp[(2) - (5)]);
          stack((yyval)).id(ID_if);
          mto((yyval), (yyvsp[(1) - (5)]));
          mto((yyval), (yyvsp[(3) - (5)]));
          mto((yyval), (yyvsp[(5) - (5)]));
        }
    break;

  case 106:
#line 745 "parser.y"
    { (yyval)=(yyvsp[(2) - (4)]);
          stack((yyval)).id(ID_side_effect);
          stack((yyval)).set(ID_statement, ID_gcc_conditional_expression);
          mto((yyval), (yyvsp[(1) - (4)]));
          mto((yyval), (yyvsp[(4) - (4)]));
        }
    break;

  case 108:
#line 756 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_side_effect, (yyvsp[(3) - (3)])); stack((yyval)).set(ID_statement, ID_assign); }
    break;

  case 109:
#line 758 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_side_effect, (yyvsp[(3) - (3)])); stack((yyval)).set(ID_statement, ID_assign_mult); }
    break;

  case 110:
#line 760 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_side_effect, (yyvsp[(3) - (3)])); stack((yyval)).set(ID_statement, ID_assign_div); }
    break;

  case 111:
#line 762 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_side_effect, (yyvsp[(3) - (3)])); stack((yyval)).set(ID_statement, ID_assign_mod); }
    break;

  case 112:
#line 764 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_side_effect, (yyvsp[(3) - (3)])); stack((yyval)).set(ID_statement, ID_assign_plus); }
    break;

  case 113:
#line 766 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_side_effect, (yyvsp[(3) - (3)])); stack((yyval)).set(ID_statement, ID_assign_minus); }
    break;

  case 114:
#line 768 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_side_effect, (yyvsp[(3) - (3)])); stack((yyval)).set(ID_statement, ID_assign_shl); }
    break;

  case 115:
#line 770 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_side_effect, (yyvsp[(3) - (3)])); stack((yyval)).set(ID_statement, ID_assign_shr); }
    break;

  case 116:
#line 772 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_side_effect, (yyvsp[(3) - (3)])); stack((yyval)).set(ID_statement, ID_assign_bitand); }
    break;

  case 117:
#line 774 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_side_effect, (yyvsp[(3) - (3)])); stack((yyval)).set(ID_statement, ID_assign_bitxor); }
    break;

  case 118:
#line 776 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_side_effect, (yyvsp[(3) - (3)])); stack((yyval)).set(ID_statement, ID_assign_bitor); }
    break;

  case 120:
#line 782 "parser.y"
    { binary((yyval), (yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]), ID_comma, (yyvsp[(3) - (3)])); }
    break;

  case 122:
#line 791 "parser.y"
    { init((yyval)); stack((yyval)).make_nil(); }
    break;

  case 124:
#line 799 "parser.y"
    {
          // type only, no declarator!
          init((yyval), ID_declaration);
          stack((yyval)).type().swap(stack((yyvsp[(1) - (2)])));
        }
    break;

  case 125:
#line 805 "parser.y"
    {
          // type only, no identifier!
          init((yyval), ID_declaration);
          stack((yyval)).type().swap(stack((yyvsp[(1) - (2)])));
        }
    break;

  case 129:
#line 817 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (6)]);
          set((yyval), ID_declaration);
          to_ansi_c_declaration(stack((yyval))).set_is_static_assert(true);
          mto((yyval), (yyvsp[(3) - (6)]));
          mto((yyval), (yyvsp[(5) - (6)]));
        }
    break;

  case 130:
#line 828 "parser.y"
    {
            init((yyval), ID_declaration);
            stack((yyval)).type().swap(stack((yyvsp[(1) - (2)])));
            PARSER.add_declarator(stack((yyval)), stack((yyvsp[(2) - (2)])));
          }
    break;

  case 131:
#line 834 "parser.y"
    {
          // patch on the initializer
          (yyval)=(yyvsp[(3) - (4)]);
          to_ansi_c_declaration(stack((yyval))).add_initializer(stack((yyvsp[(4) - (4)])));
        }
    break;

  case 132:
#line 840 "parser.y"
    {
            init((yyval), ID_declaration);
            stack((yyval)).type().swap(stack((yyvsp[(1) - (2)])));
            PARSER.add_declarator(stack((yyval)), stack((yyvsp[(2) - (2)])));
          }
    break;

  case 133:
#line 846 "parser.y"
    {
          // patch on the initializer
          (yyval)=(yyvsp[(3) - (4)]);
          to_ansi_c_declaration(stack((yyval))).add_initializer(stack((yyvsp[(4) - (4)])));
        }
    break;

  case 134:
#line 852 "parser.y"
    {
            // just add the declarator
            PARSER.add_declarator(stack((yyvsp[(1) - (3)])), stack((yyvsp[(3) - (3)])));
            // Needs to be done before initializer, as we want to see that identifier
            // already there!
          }
    break;

  case 135:
#line 859 "parser.y"
    {
          // patch on the initializer
          (yyval)=(yyvsp[(1) - (5)]);
          to_ansi_c_declaration(stack((yyval))).add_initializer(stack((yyvsp[(5) - (5)])));
        }
    break;

  case 136:
#line 868 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (5)]);
          stack((yyval)).id(ID_asm);
          stack((yyval)).set(ID_flavor, ID_gcc);
          stack((yyval)).operands().swap(stack((yyvsp[(4) - (5)])).operands());
        }
    break;

  case 138:
#line 879 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 140:
#line 887 "parser.y"
    {
          init((yyval));
        }
    break;

  case 142:
#line 896 "parser.y"
    {
            (yyvsp[(2) - (3)])=merge((yyvsp[(3) - (3)]), (yyvsp[(2) - (3)])); // type attribute
            
            // the symbol has to be visible during initialization
            init((yyval), ID_declaration);
            stack((yyval)).type().swap(stack((yyvsp[(1) - (3)])));
            PARSER.add_declarator(stack((yyval)), stack((yyvsp[(2) - (3)])));
          }
    break;

  case 143:
#line 905 "parser.y"
    {
          // add the initializer
          (yyval)=(yyvsp[(4) - (5)]);
          to_ansi_c_declaration(stack((yyval))).add_initializer(stack((yyvsp[(5) - (5)])));
        }
    break;

  case 144:
#line 912 "parser.y"
    {
            (yyvsp[(2) - (3)])=merge((yyvsp[(3) - (3)]), (yyvsp[(2) - (3)]));
            
            // the symbol has to be visible during initialization
            init((yyval), ID_declaration);
            stack((yyval)).type().swap(stack((yyvsp[(1) - (3)])));
            PARSER.add_declarator(stack((yyval)), stack((yyvsp[(2) - (3)])));
          }
    break;

  case 145:
#line 921 "parser.y"
    {
          // add the initializer
          (yyval)=(yyvsp[(4) - (5)]);
          to_ansi_c_declaration(stack((yyval))).add_initializer(stack((yyvsp[(5) - (5)])));
        }
    break;

  case 146:
#line 928 "parser.y"
    {
          // handled as typeof(initializer)
          stack((yyvsp[(1) - (5)])).id(ID_typeof);
          stack((yyvsp[(1) - (5)])).copy_to_operands(stack((yyvsp[(5) - (5)])));

          (yyvsp[(2) - (5)])=merge((yyvsp[(3) - (5)]), (yyvsp[(2) - (5)]));

          // the symbol has to be visible during initialization
          init((yyval), ID_declaration);
          stack((yyval)).type().swap(stack((yyvsp[(1) - (5)])));
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(2) - (5)])));
          // add the initializer
          to_ansi_c_declaration(stack((yyval))).add_initializer(stack((yyvsp[(5) - (5)])));
        }
    break;

  case 147:
#line 944 "parser.y"
    {
            // type attribute goes into declarator
            (yyvsp[(3) - (4)])=merge((yyvsp[(4) - (4)]), (yyvsp[(3) - (4)]));
            PARSER.add_declarator(stack((yyvsp[(1) - (4)])), stack((yyvsp[(3) - (4)])));
          }
    break;

  case 148:
#line 950 "parser.y"
    {
          // add in the initializer
          (yyval)=(yyvsp[(1) - (6)]);
          to_ansi_c_declaration(stack((yyval))).add_initializer(stack((yyvsp[(6) - (6)])));
        }
    break;

  case 160:
#line 976 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 162:
#line 981 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 163:
#line 985 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 165:
#line 993 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 166:
#line 1000 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 168:
#line 1008 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 171:
#line 1019 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_atomic); }
    break;

  case 172:
#line 1020 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_const); }
    break;

  case 173:
#line 1021 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_restrict); }
    break;

  case 174:
#line 1022 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_volatile); }
    break;

  case 175:
#line 1023 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_cprover_atomic); }
    break;

  case 176:
#line 1024 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_ptr32); }
    break;

  case 177:
#line 1025 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_ptr64); }
    break;

  case 178:
#line 1026 "parser.y"
    { (yyval)=(yyvsp[(1) - (4)]); set((yyval), ID_msc_based); mto((yyval), (yyvsp[(3) - (4)])); }
    break;

  case 180:
#line 1032 "parser.y"
    { (yyval) = (yyvsp[(1) - (4)]);
          stack((yyval)).id(ID_aligned);
          stack((yyval)).set(ID_size, stack((yyvsp[(3) - (4)])));
        }
    break;

  case 181:
#line 1037 "parser.y"
    { (yyval) = (yyvsp[(1) - (4)]);
          stack((yyval)).id(ID_aligned);
          stack((yyvsp[(3) - (4)])).set(ID_type_arg, stack((yyvsp[(3) - (4)])));
        }
    break;

  case 188:
#line 1057 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 189:
#line 1064 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 190:
#line 1068 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 191:
#line 1072 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 192:
#line 1076 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 193:
#line 1083 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)])); // type attribute
        }
    break;

  case 194:
#line 1087 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 195:
#line 1091 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 196:
#line 1095 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 197:
#line 1103 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 198:
#line 1107 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 199:
#line 1111 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 201:
#line 1120 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 202:
#line 1124 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 203:
#line 1131 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 204:
#line 1135 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 205:
#line 1139 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 206:
#line 1146 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 207:
#line 1150 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 208:
#line 1154 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 209:
#line 1161 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 210:
#line 1165 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 211:
#line 1169 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 212:
#line 1176 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 213:
#line 1180 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 214:
#line 1184 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 215:
#line 1191 "parser.y"
    { (yyval) = (yyvsp[(1) - (4)]);
          stack((yyval)).id(ID_typeof);
          mto((yyval), (yyvsp[(3) - (4)]));
        }
    break;

  case 216:
#line 1196 "parser.y"
    { (yyval) = (yyvsp[(1) - (4)]);
          stack((yyval)).id(ID_typeof);
          stack((yyval)).set(ID_type_arg, stack((yyvsp[(3) - (4)])));
        }
    break;

  case 218:
#line 1205 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 219:
#line 1209 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 220:
#line 1213 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 221:
#line 1220 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (4)]);
          stack((yyval)).id(ID_atomic_type_specifier);
          stack_type((yyval)).subtype()=stack_type((yyvsp[(3) - (4)]));
        }
    break;

  case 223:
#line 1230 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 224:
#line 1234 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 225:
#line 1238 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 226:
#line 1245 "parser.y"
    {
          stack((yyval)).id(stack((yyval)).get(ID_identifier));
        }
    break;

  case 227:
#line 1249 "parser.y"
    {
          stack((yyval)).id(stack((yyval)).get(ID_identifier));
        }
    break;

  case 228:
#line 1253 "parser.y"
    {
          stack((yyval)).id(ID_restrict);
        }
    break;

  case 230:
#line 1261 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (4)]); mto((yyval), (yyvsp[(3) - (4)]));
        }
    break;

  case 231:
#line 1265 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (4)]); mto((yyval), (yyvsp[(3) - (4)]));
        }
    break;

  case 232:
#line 1269 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (6)]); mto((yyval), (yyvsp[(3) - (6)])); mto((yyval), (yyvsp[(5) - (6)]));
        }
    break;

  case 233:
#line 1273 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (10)]); mto((yyval), (yyvsp[(3) - (10)])); mto((yyval), (yyvsp[(5) - (10)])); mto((yyval), (yyvsp[(7) - (10)])); mto((yyval), (yyvsp[(9) - (10)]));
        }
    break;

  case 234:
#line 1276 "parser.y"
    { init((yyval), ID_nil); }
    break;

  case 235:
#line 1281 "parser.y"
    {
          init((yyval)); mto((yyval), (yyvsp[(1) - (1)]));
        }
    break;

  case 236:
#line 1285 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (2)]); mto((yyval), (yyvsp[(2) - (2)]));
        }
    break;

  case 237:
#line 1292 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (4)]); set((yyval), ID_msc_declspec);
          stack((yyval)).operands().swap(stack((yyvsp[(3) - (4)])).operands());
        }
    break;

  case 238:
#line 1297 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]); set((yyval), ID_msc_declspec);
        }
    break;

  case 239:
#line 1304 "parser.y"
    {
          init((yyval), ID_nil);
        }
    break;

  case 241:
#line 1311 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_typedef); }
    break;

  case 242:
#line 1312 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_extern); }
    break;

  case 243:
#line 1313 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_static); }
    break;

  case 244:
#line 1314 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_auto); }
    break;

  case 245:
#line 1315 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_register); }
    break;

  case 246:
#line 1316 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_inline); }
    break;

  case 247:
#line 1317 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_thread_local); }
    break;

  case 248:
#line 1318 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_asm); }
    break;

  case 249:
#line 1319 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); }
    break;

  case 250:
#line 1323 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_int); }
    break;

  case 251:
#line 1324 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_int8); }
    break;

  case 252:
#line 1325 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_int16); }
    break;

  case 253:
#line 1326 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_int32); }
    break;

  case 254:
#line 1327 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_int64); }
    break;

  case 255:
#line 1328 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_char); }
    break;

  case 256:
#line 1329 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_short); }
    break;

  case 257:
#line 1330 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_long); }
    break;

  case 258:
#line 1331 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_float); }
    break;

  case 259:
#line 1332 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_gcc_float80); }
    break;

  case 260:
#line 1333 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_gcc_float128); }
    break;

  case 261:
#line 1334 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_gcc_int128); }
    break;

  case 262:
#line 1335 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_gcc_decimal32); }
    break;

  case 263:
#line 1336 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_gcc_decimal64); }
    break;

  case 264:
#line 1337 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_gcc_decimal128); }
    break;

  case 265:
#line 1338 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_double); }
    break;

  case 266:
#line 1339 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_signed); }
    break;

  case 267:
#line 1340 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_unsigned); }
    break;

  case 268:
#line 1341 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_void); }
    break;

  case 269:
#line 1342 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_c_bool); }
    break;

  case 270:
#line 1343 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_complex); }
    break;

  case 271:
#line 1345 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (4)]);
          set((yyval), ID_custom_bv);
          stack((yyval)).add(ID_size).swap(stack((yyvsp[(3) - (4)])));
        }
    break;

  case 272:
#line 1351 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (7)]);
          set((yyval), ID_custom_floatbv);
          stack((yyval)).add(ID_size).swap(stack((yyvsp[(3) - (7)])));
          stack((yyval)).add(ID_f).swap(stack((yyvsp[(6) - (7)])));
        }
    break;

  case 273:
#line 1358 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (7)]);
          set((yyval), ID_custom_fixedbv);
          stack((yyval)).add(ID_size).swap(stack((yyvsp[(3) - (7)])));
          stack((yyval)).add(ID_f).swap(stack((yyvsp[(6) - (7)])));
        }
    break;

  case 274:
#line 1364 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_proper_bool); }
    break;

  case 278:
#line 1375 "parser.y"
    { (yyval)=(yyvsp[(1) - (4)]); stack_type((yyval)).subtype().swap(stack((yyvsp[(2) - (4)]))); }
    break;

  case 279:
#line 1379 "parser.y"
    {
          init((yyval));
          if(!PARSER.pragma_pack.empty() &&
             PARSER.pragma_pack.back().is_one())
            set((yyval), ID_packed);
        }
    break;

  case 280:
#line 1391 "parser.y"
    {
            // an anon struct/union with body
          }
    break;

  case 281:
#line 1397 "parser.y"
    {
          // save the members
          stack((yyvsp[(1) - (9)])).add(ID_components).get_sub().swap(
            (irept::subt &)stack((yyvsp[(6) - (9)])).operands());

          // throw in the gcc attributes
          (yyval)=merge((yyvsp[(1) - (9)]), merge((yyvsp[(2) - (9)]), merge((yyvsp[(8) - (9)]), (yyvsp[(9) - (9)]))));
        }
    break;

  case 282:
#line 1409 "parser.y"
    {
            // A struct/union with tag and body.
            PARSER.add_tag_with_body(stack((yyvsp[(4) - (4)])));
            stack((yyvsp[(1) - (4)])).set(ID_tag, stack((yyvsp[(4) - (4)])));
          }
    break;

  case 283:
#line 1417 "parser.y"
    {
          // save the members
          stack((yyvsp[(1) - (10)])).add(ID_components).get_sub().swap(
            (irept::subt &)stack((yyvsp[(7) - (10)])).operands());

          // throw in the gcc attributes
          (yyval)=merge((yyvsp[(1) - (10)]), merge((yyvsp[(2) - (10)]), merge((yyvsp[(9) - (10)]), (yyvsp[(10) - (10)]))));
        }
    break;

  case 284:
#line 1429 "parser.y"
    {
            // a struct/union with tag but without body
            stack((yyvsp[(1) - (4)])).set(ID_tag, stack((yyvsp[(4) - (4)])));
          }
    break;

  case 285:
#line 1434 "parser.y"
    {
          stack((yyvsp[(1) - (6)])).set(ID_components, ID_nil);
          // type attributes
          (yyval)=merge((yyvsp[(1) - (6)]), merge((yyvsp[(2) - (6)]), (yyvsp[(6) - (6)])));
        }
    break;

  case 286:
#line 1443 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_struct); }
    break;

  case 287:
#line 1445 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_union); }
    break;

  case 288:
#line 1450 "parser.y"
    {
          init((yyval), ID_expression_list);
          mto((yyval), (yyvsp[(1) - (1)]));
        }
    break;

  case 289:
#line 1455 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          mto((yyval), (yyvsp[(3) - (3)]));
        }
    break;

  case 290:
#line 1463 "parser.y"
    {
          init((yyval), ID_expression_list);
        }
    break;

  case 292:
#line 1471 "parser.y"
    {
          init((yyval));
        }
    break;

  case 293:
#line 1475 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (1)]);
          stack((yyval)).id(ID_gcc_attribute);
          stack((yyval)).set(ID_identifier, ID_const);
        }
    break;

  case 294:
#line 1481 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (1)]);
          stack((yyval)).id(ID_gcc_attribute);
        }
    break;

  case 295:
#line 1486 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (4)]);
          stack((yyval)).id(ID_gcc_attribute);
          stack((yyval)).operands().swap(stack((yyvsp[(3) - (4)])).operands());
        }
    break;

  case 297:
#line 1496 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]));
        }
    break;

  case 298:
#line 1503 "parser.y"
    { (yyval)=(yyvsp[(4) - (6)]); }
    break;

  case 299:
#line 1508 "parser.y"
    {
          init((yyval));
        }
    break;

  case 302:
#line 1517 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 303:
#line 1524 "parser.y"
    { (yyval)=(yyvsp[(1) - (2)]); set((yyval), ID_packed); }
    break;

  case 304:
#line 1526 "parser.y"
    { (yyval)=(yyvsp[(1) - (2)]); set((yyval), ID_transparent_union); }
    break;

  case 305:
#line 1528 "parser.y"
    { (yyval)=(yyvsp[(1) - (5)]); set((yyval), ID_vector); stack((yyval)).add(ID_size)=stack((yyvsp[(3) - (5)])); }
    break;

  case 306:
#line 1530 "parser.y"
    { (yyval)=(yyvsp[(1) - (2)]); set((yyval), ID_aligned); }
    break;

  case 307:
#line 1532 "parser.y"
    { (yyval)=(yyvsp[(1) - (5)]); set((yyval), ID_aligned); stack((yyval)).set(ID_size, stack((yyvsp[(3) - (5)]))); }
    break;

  case 308:
#line 1534 "parser.y"
    { (yyval)=(yyvsp[(1) - (5)]); set((yyval), ID_gcc_attribute_mode); stack((yyval)).set(ID_size, stack((yyvsp[(3) - (5)])).get(ID_identifier)); }
    break;

  case 309:
#line 1536 "parser.y"
    { (yyval)=(yyvsp[(1) - (2)]); set((yyval), ID_static); }
    break;

  case 310:
#line 1538 "parser.y"
    { (yyval)=(yyvsp[(1) - (1)]); set((yyval), ID_noreturn); }
    break;

  case 312:
#line 1544 "parser.y"
    {
          init((yyval), ID_declaration_list);
        }
    break;

  case 314:
#line 1552 "parser.y"
    {
          init((yyval), ID_declaration_list);
          mto((yyval), (yyvsp[(1) - (1)]));
        }
    break;

  case 315:
#line 1557 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (2)]);
          mto((yyval), (yyvsp[(2) - (2)]));
        }
    break;

  case 318:
#line 1567 "parser.y"
    {
          init((yyval), ID_declaration);
        }
    break;

  case 320:
#line 1580 "parser.y"
    {
          (yyvsp[(2) - (3)])=merge((yyvsp[(2) - (3)]), (yyvsp[(1) - (3)]));

          init((yyval), ID_declaration);
          to_ansi_c_declaration(stack((yyval))).set_is_member(true);
          stack((yyval)).type().swap(stack((yyvsp[(2) - (3)])));
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(3) - (3)])));
        }
    break;

  case 321:
#line 1589 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(3) - (3)])));
        }
    break;

  case 322:
#line 1599 "parser.y"
    {
          if(!PARSER.pragma_pack.empty() &&
             !PARSER.pragma_pack.back().is_zero())
            stack((yyvsp[(2) - (3)])).set(ID_C_alignment, PARSER.pragma_pack.back());

          (yyvsp[(2) - (3)])=merge((yyvsp[(2) - (3)]), (yyvsp[(1) - (3)]));

          init((yyval), ID_declaration);
          to_ansi_c_declaration(stack((yyval))).set_is_member(true);
          stack((yyval)).type().swap(stack((yyvsp[(2) - (3)])));
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(3) - (3)])));
        }
    break;

  case 323:
#line 1612 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(3) - (3)])));
        }
    break;

  case 324:
#line 1620 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);

          if(stack((yyvsp[(2) - (3)])).is_not_nil())
            make_subtype((yyval), (yyvsp[(2) - (3)]));

          if(stack((yyvsp[(3) - (3)])).is_not_nil()) // type attribute
            (yyval)=merge((yyvsp[(3) - (3)]), (yyval));
        }
    break;

  case 325:
#line 1630 "parser.y"
    {
          init((yyval), ID_abstract);
        }
    break;

  case 326:
#line 1634 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (2)]);
          stack_type((yyval)).subtype()=typet(ID_abstract);

          if(stack((yyvsp[(2) - (2)])).is_not_nil()) // type attribute
            (yyval)=merge((yyvsp[(2) - (2)]), (yyval));
        }
    break;

  case 327:
#line 1645 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          if(stack((yyvsp[(2) - (3)])).is_not_nil())
            make_subtype((yyval), (yyvsp[(2) - (3)]));
          
          if(stack((yyvsp[(3) - (3)])).is_not_nil()) // type attribute
            (yyval)=merge((yyvsp[(3) - (3)]), (yyval));
        }
    break;

  case 328:
#line 1654 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (2)]);
          stack_type((yyval)).subtype()=typet(ID_abstract);

          if(stack((yyvsp[(2) - (2)])).is_not_nil()) // type attribute
            (yyval)=merge((yyvsp[(2) - (2)]), (yyval));
        }
    break;

  case 329:
#line 1665 "parser.y"
    {
          init((yyval), ID_nil);
        }
    break;

  case 331:
#line 1673 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (2)]);
          set((yyval), ID_c_bit_field);
          stack_type((yyval)).set(ID_size, stack((yyvsp[(2) - (2)])));
          stack_type((yyval)).subtype().id(ID_abstract);
        }
    break;

  case 332:
#line 1684 "parser.y"
    {
            // an anon enum
          }
    break;

  case 333:
#line 1689 "parser.y"
    {
          stack((yyvsp[(1) - (7)])).operands().swap(stack((yyvsp[(5) - (7)])).operands());
          (yyval)=merge((yyvsp[(1) - (7)]), merge((yyvsp[(2) - (7)]), (yyvsp[(7) - (7)]))); // throw in the gcc attributes
        }
    break;

  case 334:
#line 1696 "parser.y"
    {
            // an enum with tag
            stack((yyvsp[(1) - (3)])).set(ID_tag, stack((yyvsp[(3) - (3)])));
          }
    break;

  case 335:
#line 1702 "parser.y"
    {
          stack((yyvsp[(1) - (8)])).operands().swap(stack((yyvsp[(6) - (8)])).operands());
          (yyval)=merge((yyvsp[(1) - (8)]), merge((yyvsp[(2) - (8)]), (yyvsp[(8) - (8)]))); // throw in the gcc attributes
        }
    break;

  case 336:
#line 1710 "parser.y"
    {
          stack((yyvsp[(1) - (4)])).id(ID_c_enum_tag); // tag only
          stack((yyvsp[(1) - (4)])).set(ID_tag, stack((yyvsp[(3) - (4)])));
          (yyval)=merge((yyvsp[(1) - (4)]), merge((yyvsp[(2) - (4)]), (yyvsp[(4) - (4)]))); // throw in the gcc attributes
        }
    break;

  case 337:
#line 1718 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (1)]);
          set((yyval), ID_c_enum);
        }
    break;

  case 338:
#line 1726 "parser.y"
    {
          init((yyval), ID_declaration_list);
        }
    break;

  case 340:
#line 1734 "parser.y"
    {
          init((yyval), ID_declaration_list);
          mto((yyval), (yyvsp[(1) - (1)]));
        }
    break;

  case 341:
#line 1739 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          mto((yyval), (yyvsp[(3) - (3)]));
        }
    break;

  case 342:
#line 1744 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (2)]);
        }
    break;

  case 343:
#line 1751 "parser.y"
    {
          init((yyval), ID_declaration);
          to_ansi_c_declaration(stack((yyval))).set_is_enum_constant(true);
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(1) - (3)])));
          to_ansi_c_declaration(stack((yyval))).add_initializer(stack((yyvsp[(3) - (3)])));
        }
    break;

  case 344:
#line 1761 "parser.y"
    {
          init((yyval));
          stack((yyval)).make_nil();
        }
    break;

  case 345:
#line 1766 "parser.y"
    {
          (yyval)=(yyvsp[(2) - (2)]);
        }
    break;

  case 347:
#line 1774 "parser.y"
    {
          typet tmp(ID_ellipsis);
          (yyval)=(yyvsp[(1) - (3)]);
          stack_type((yyval)).move_to_subtypes(tmp);
        }
    break;

  case 348:
#line 1783 "parser.y"
    {
          init((yyval), ID_parameters);
          mts((yyval), (yyvsp[(1) - (1)]));
        }
    break;

  case 349:
#line 1788 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          mts((yyval), (yyvsp[(3) - (3)]));
        }
    break;

  case 350:
#line 1795 "parser.y"
    {
          init((yyval), ID_declaration);
          stack((yyval)).type()=typet(ID_KnR);
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(1) - (1)])));
        }
    break;

  case 351:
#line 1804 "parser.y"
    {
          init((yyval), ID_parameters);
          mts((yyval), (yyvsp[(1) - (1)]));
        }
    break;

  case 352:
#line 1809 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          mts((yyval), (yyvsp[(3) - (3)]));
        }
    break;

  case 353:
#line 1817 "parser.y"
    {
          init((yyval), ID_declaration);
          to_ansi_c_declaration(stack((yyval))).set_is_parameter(true);
          to_ansi_c_declaration(stack((yyval))).type().swap(stack((yyvsp[(1) - (1)])));
          exprt declarator=exprt(ID_abstract);
          PARSER.add_declarator(stack((yyval)), declarator);
        }
    break;

  case 354:
#line 1825 "parser.y"
    {
          init((yyval), ID_declaration);
          to_ansi_c_declaration(stack((yyval))).set_is_parameter(true);
          to_ansi_c_declaration(stack((yyval))).type().swap(stack((yyvsp[(1) - (2)])));
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(2) - (2)])));
        }
    break;

  case 355:
#line 1832 "parser.y"
    {
          (yyvsp[(2) - (3)])=merge((yyvsp[(3) - (3)]), (yyvsp[(2) - (3)])); // type attribute to go into declarator
          init((yyval), ID_declaration);
          to_ansi_c_declaration(stack((yyval))).set_is_parameter(true);
          to_ansi_c_declaration(stack((yyval))).type().swap(stack((yyvsp[(1) - (3)])));
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(2) - (3)])));
        }
    break;

  case 356:
#line 1840 "parser.y"
    {
          // the second tree is really the declarator -- not part
          // of the type!
          init((yyval), ID_declaration);
          to_ansi_c_declaration(stack((yyval))).set_is_parameter(true);
          to_ansi_c_declaration(stack((yyval))).type().swap(stack((yyvsp[(1) - (2)])));
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(2) - (2)])));
        }
    break;

  case 357:
#line 1849 "parser.y"
    {
          init((yyval), ID_declaration);
          to_ansi_c_declaration(stack((yyval))).set_is_parameter(true);
          to_ansi_c_declaration(stack((yyval))).type().swap(stack((yyvsp[(1) - (1)])));
          exprt declarator=exprt(ID_abstract);
          PARSER.add_declarator(stack((yyval)), declarator);
        }
    break;

  case 358:
#line 1857 "parser.y"
    {
          init((yyval), ID_declaration);
          to_ansi_c_declaration(stack((yyval))).set_is_parameter(true);
          to_ansi_c_declaration(stack((yyval))).type().swap(stack((yyvsp[(1) - (2)])));
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(2) - (2)])));
        }
    break;

  case 359:
#line 1864 "parser.y"
    {
          (yyvsp[(2) - (3)])=merge((yyvsp[(3) - (3)]), (yyvsp[(2) - (3)])); // type attribute to go into declarator
          init((yyval), ID_declaration);
          to_ansi_c_declaration(stack((yyval))).set_is_parameter(true);
          to_ansi_c_declaration(stack((yyval))).type().swap(stack((yyvsp[(1) - (3)])));
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(2) - (3)])));
        }
    break;

  case 360:
#line 1872 "parser.y"
    {
          init((yyval), ID_declaration);
          to_ansi_c_declaration(stack((yyval))).set_is_parameter(true);
          to_ansi_c_declaration(stack((yyval))).type().swap(stack((yyvsp[(1) - (1)])));
          exprt declarator=exprt(ID_abstract);
          PARSER.add_declarator(stack((yyval)), declarator);
        }
    break;

  case 361:
#line 1880 "parser.y"
    {
          init((yyval), ID_declaration);
          to_ansi_c_declaration(stack((yyval))).set_is_parameter(true);
          to_ansi_c_declaration(stack((yyval))).type().swap(stack((yyvsp[(1) - (2)])));
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(2) - (2)])));
        }
    break;

  case 362:
#line 1887 "parser.y"
    {
          (yyvsp[(2) - (3)])=merge((yyvsp[(3) - (3)]), (yyvsp[(2) - (3)])); // type attribute to go into declarator
          init((yyval), ID_declaration);
          to_ansi_c_declaration(stack((yyval))).set_is_parameter(true);
          to_ansi_c_declaration(stack((yyval))).type().swap(stack((yyvsp[(1) - (3)])));
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(2) - (3)])));
        }
    break;

  case 363:
#line 1895 "parser.y"
    {
          // the second tree is really the declarator -- not part of the type!
          init((yyval), ID_declaration);
          to_ansi_c_declaration(stack((yyval))).set_is_parameter(true);
          to_ansi_c_declaration(stack((yyval))).type().swap(stack((yyvsp[(1) - (2)])));
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(2) - (2)])));
        }
    break;

  case 364:
#line 1903 "parser.y"
    {
          init((yyval), ID_declaration);
          to_ansi_c_declaration(stack((yyval))).set_is_parameter(true);
          to_ansi_c_declaration(stack((yyval))).type().swap(stack((yyvsp[(1) - (1)])));
          exprt declarator=exprt(ID_abstract);
          PARSER.add_declarator(stack((yyval)), declarator);
        }
    break;

  case 365:
#line 1911 "parser.y"
    {
          init((yyval), ID_declaration);
          to_ansi_c_declaration(stack((yyval))).set_is_parameter(true);
          to_ansi_c_declaration(stack((yyval))).type().swap(stack((yyvsp[(1) - (2)])));
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(2) - (2)])));
        }
    break;

  case 366:
#line 1918 "parser.y"
    {
          (yyvsp[(2) - (3)])=merge((yyvsp[(3) - (3)]), (yyvsp[(2) - (3)])); // type attribute to go into declarator
          init((yyval), ID_declaration);
          to_ansi_c_declaration(stack((yyval))).set_is_parameter(true);
          to_ansi_c_declaration(stack((yyval))).type().swap(stack((yyvsp[(1) - (3)])));
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(2) - (3)])));
        }
    break;

  case 369:
#line 1934 "parser.y"
    {
          (yyval)=merge((yyvsp[(2) - (2)]), (yyvsp[(1) - (2)]));
        }
    break;

  case 370:
#line 1938 "parser.y"
    {
          (yyval)=merge((yyvsp[(2) - (3)]), (yyvsp[(1) - (3)]));
          make_subtype((yyval), (yyvsp[(3) - (3)]));
        }
    break;

  case 371:
#line 1943 "parser.y"
    {
          (yyval)=merge((yyvsp[(2) - (2)]), (yyvsp[(1) - (2)]));
        }
    break;

  case 372:
#line 1947 "parser.y"
    {
          (yyval)=merge((yyvsp[(2) - (3)]), (yyvsp[(1) - (3)]));
          make_subtype((yyval), (yyvsp[(3) - (3)]));
        }
    break;

  case 373:
#line 1955 "parser.y"
    {
          newstack((yyval));
          stack((yyval)).make_nil();
        }
    break;

  case 374:
#line 1960 "parser.y"
    { (yyval) = (yyvsp[(2) - (2)]); }
    break;

  case 376:
#line 1971 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          set((yyval), ID_initializer_list);
          stack((yyval)).operands().swap(stack((yyvsp[(2) - (3)])).operands());
        }
    break;

  case 377:
#line 1977 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (4)]);
          set((yyval), ID_initializer_list);
          stack((yyval)).operands().swap(stack((yyvsp[(2) - (4)])).operands());
        }
    break;

  case 378:
#line 1986 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (1)]);
          exprt tmp;
          tmp.swap(stack((yyval)));
          stack((yyval)).clear();
          stack((yyval)).move_to_operands(tmp);
        }
    break;

  case 379:
#line 1994 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          mto((yyval), (yyvsp[(3) - (3)]));
        }
    break;

  case 381:
#line 2003 "parser.y"
    {
          init((yyval));
          set((yyval), ID_initializer_list);
          stack((yyval)).operands().clear();
        }
    break;

  case 383:
#line 2013 "parser.y"
    {
          (yyval)=(yyvsp[(2) - (3)]);
          stack((yyval)).id(ID_designated_initializer);
          stack((yyval)).add(ID_designator).swap(stack((yyvsp[(1) - (3)])));
          mto((yyval), (yyvsp[(3) - (3)]));
        }
    break;

  case 384:
#line 2021 "parser.y"
    {
          init((yyval), ID_designated_initializer);
          stack((yyval)).add(ID_designator).swap(stack((yyvsp[(1) - (2)])));
          mto((yyval), (yyvsp[(2) - (2)]));
        }
    break;

  case 385:
#line 2027 "parser.y"
    {
          // yet another GCC speciality
          (yyval)=(yyvsp[(2) - (3)]);
          stack((yyval)).id(ID_designated_initializer);
          exprt designator;
          exprt member(ID_member);
          member.set(ID_component_name, stack((yyvsp[(1) - (3)])).get(ID_C_base_name));
          designator.move_to_operands(member);
          stack((yyval)).add(ID_designator).swap(designator);
          mto((yyval), (yyvsp[(3) - (3)]));
        }
    break;

  case 386:
#line 2042 "parser.y"
    {
          init((yyval));
          stack((yyvsp[(1) - (2)])).id(ID_member);
          stack((yyvsp[(1) - (2)])).set(ID_component_name, stack((yyvsp[(2) - (2)])).get(ID_C_base_name));
          mto((yyval), (yyvsp[(1) - (2)]));
        }
    break;

  case 387:
#line 2049 "parser.y"
    {
          init((yyval));
          stack((yyvsp[(1) - (3)])).id(ID_index);
          mto((yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]));
          mto((yyval), (yyvsp[(1) - (3)]));
        }
    break;

  case 388:
#line 2056 "parser.y"
    {
          // TODO
          init((yyval));
          stack((yyvsp[(1) - (5)])).id(ID_index);
          mto((yyvsp[(1) - (5)]), (yyvsp[(2) - (5)]));
          mto((yyval), (yyvsp[(1) - (5)]));
        }
    break;

  case 389:
#line 2064 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (4)]);
          stack((yyvsp[(2) - (4)])).id(ID_index);
          mto((yyvsp[(2) - (4)]), (yyvsp[(3) - (4)]));
          mto((yyval), (yyvsp[(2) - (4)]));
        }
    break;

  case 390:
#line 2071 "parser.y"
    {
          // TODO
          (yyval)=(yyvsp[(1) - (6)]);
          stack((yyvsp[(2) - (6)])).id(ID_index);
          mto((yyvsp[(2) - (6)]), (yyvsp[(3) - (6)]));
          mto((yyval), (yyvsp[(2) - (6)]));
        }
    break;

  case 391:
#line 2079 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          stack((yyvsp[(2) - (3)])).id(ID_member);
          stack((yyvsp[(2) - (3)])).set(ID_component_name, stack((yyvsp[(3) - (3)])).get(ID_C_base_name));
          mto((yyval), (yyvsp[(2) - (3)]));
        }
    break;

  case 404:
#line 2106 "parser.y"
    {
          init((yyval));
          statement((yyval), ID_decl);
          mto((yyval), (yyvsp[(1) - (1)]));
        }
    break;

  case 405:
#line 2115 "parser.y"
    {
          (yyval)=(yyvsp[(2) - (3)]);
          statement((yyval), ID_label);
          irep_idt identifier=PARSER.lookup_label(stack((yyvsp[(1) - (3)])).get(ID_C_base_name));
          stack((yyval)).set(ID_label, identifier);
          mto((yyval), (yyvsp[(3) - (3)]));
        }
    break;

  case 406:
#line 2123 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (4)]);
          statement((yyval), ID_switch_case);
          mto((yyval), (yyvsp[(2) - (4)]));
          mto((yyval), (yyvsp[(4) - (4)]));
        }
    break;

  case 407:
#line 2130 "parser.y"
    {
          // this is a GCC extension
          (yyval)=(yyvsp[(1) - (6)]);
          statement((yyval), ID_gcc_switch_case_range);
          mto((yyval), (yyvsp[(2) - (6)]));
          mto((yyval), (yyvsp[(4) - (6)]));
          mto((yyval), (yyvsp[(6) - (6)]));
        }
    break;

  case 408:
#line 2139 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          statement((yyval), ID_switch_case);
          stack((yyval)).operands().push_back(nil_exprt());
          mto((yyval), (yyvsp[(3) - (3)]));
          stack((yyval)).set(ID_default, true);
        }
    break;

  case 409:
#line 2150 "parser.y"
    {
          (yyval)=(yyvsp[(2) - (3)]);
          statement((yyval), ID_block);
          stack((yyval)).set(ID_C_end_location, stack((yyvsp[(3) - (3)])).source_location());
          PARSER.pop_scope();
        }
    break;

  case 410:
#line 2157 "parser.y"
    {
          (yyval)=(yyvsp[(2) - (4)]);
          statement((yyval), ID_block);
          stack((yyval)).set(ID_C_end_location, stack((yyvsp[(4) - (4)])).source_location());
          stack((yyval)).operands().swap(stack((yyvsp[(3) - (4)])).operands());
          PARSER.pop_scope();
        }
    break;

  case 411:
#line 2165 "parser.y"
    {
          (yyval)=(yyvsp[(2) - (4)]);
          statement((yyval), ID_asm);
          stack((yyval)).set(ID_C_end_location, stack((yyvsp[(4) - (4)])).source_location());
          mto((yyval), (yyvsp[(3) - (4)]));
          PARSER.pop_scope();
        }
    break;

  case 412:
#line 2176 "parser.y"
    {
          unsigned prefix=++PARSER.current_scope().compound_counter;
          PARSER.new_scope(i2string(prefix)+"::");
        }
    break;

  case 413:
#line 2184 "parser.y"
    {
          init((yyval));
          mto((yyval), (yyvsp[(1) - (1)]));
        }
    break;

  case 414:
#line 2189 "parser.y"
    {
          mto((yyval), (yyvsp[(2) - (2)]));
        }
    break;

  case 415:
#line 2196 "parser.y"
    {
          (yyval)=(yyvsp[(2) - (2)]);

          if(stack((yyvsp[(1) - (2)])).is_nil())
            statement((yyval), ID_skip);
          else
          {
            statement((yyval), ID_expression);
            mto((yyval), (yyvsp[(1) - (2)]));
          }
        }
    break;

  case 416:
#line 2211 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (5)]);
          statement((yyval), ID_ifthenelse);
          stack((yyval)).operands().reserve(3);
          mto((yyval), (yyvsp[(3) - (5)]));
          mto((yyval), (yyvsp[(5) - (5)]));
          stack((yyval)).copy_to_operands(nil_exprt());
        }
    break;

  case 417:
#line 2220 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (7)]);
          statement((yyval), ID_ifthenelse);
          stack((yyval)).operands().reserve(3);
          mto((yyval), (yyvsp[(3) - (7)]));
          mto((yyval), (yyvsp[(5) - (7)]));
          mto((yyval), (yyvsp[(7) - (7)]));
        }
    break;

  case 418:
#line 2229 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (5)]);
          statement((yyval), ID_switch);
          stack((yyval)).operands().reserve(2);
          mto((yyval), (yyvsp[(3) - (5)]));
          mto((yyval), (yyvsp[(5) - (5)]));
        }
    break;

  case 421:
#line 2245 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (5)]);
          statement((yyval), ID_while);
          stack((yyval)).operands().reserve(2);
          mto((yyval), (yyvsp[(3) - (5)]));
          mto((yyval), (yyvsp[(5) - (5)]));
        }
    break;

  case 422:
#line 2253 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (7)]);
          statement((yyval), ID_dowhile);
          stack((yyval)).operands().reserve(2);
          mto((yyval), (yyvsp[(5) - (7)]));
          mto((yyval), (yyvsp[(2) - (7)]));
        }
    break;

  case 423:
#line 2261 "parser.y"
    {
            // In C99 and upwards, for(;;) has a scope
            if(PARSER.for_has_scope)
            {
              unsigned prefix=++PARSER.current_scope().compound_counter;
              PARSER.new_scope(i2string(prefix)+"::");
            }
          }
    break;

  case 424:
#line 2273 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (9)]);
          statement((yyval), ID_for);
          stack((yyval)).operands().reserve(4);
          mto((yyval), (yyvsp[(4) - (9)]));
          mto((yyval), (yyvsp[(5) - (9)]));
          mto((yyval), (yyvsp[(7) - (9)]));
          mto((yyval), (yyvsp[(9) - (9)]));

          if(PARSER.for_has_scope)
            PARSER.pop_scope(); // remove the C99 for-scope
        }
    break;

  case 425:
#line 2289 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          if(stack((yyvsp[(2) - (3)])).id()==ID_symbol)
          {
            statement((yyval), ID_goto);
            irep_idt identifier=PARSER.lookup_label(stack((yyvsp[(2) - (3)])).get(ID_C_base_name));
            stack((yyval)).set(ID_destination, identifier);
          }
          else
          {
            // this is a gcc extension.
            // the original grammar uses identifier_or_typedef_name
            statement((yyval), ID_gcc_computed_goto);
            mto((yyval), (yyvsp[(2) - (3)]));
          }
        }
    break;

  case 426:
#line 2306 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          statement((yyval), ID_goto);
          irep_idt identifier=PARSER.lookup_label(stack((yyvsp[(2) - (3)])).get(ID_C_base_name));
          stack((yyval)).set(ID_destination, identifier);
        }
    break;

  case 427:
#line 2313 "parser.y"
    { (yyval)=(yyvsp[(1) - (2)]); statement((yyval), ID_continue); }
    break;

  case 428:
#line 2315 "parser.y"
    { (yyval)=(yyvsp[(1) - (2)]); statement((yyval), ID_break); }
    break;

  case 429:
#line 2317 "parser.y"
    { (yyval)=(yyvsp[(1) - (2)]); statement((yyval), ID_return); }
    break;

  case 430:
#line 2319 "parser.y"
    { (yyval)=(yyvsp[(1) - (3)]); statement((yyval), ID_return); mto((yyval), (yyvsp[(2) - (3)])); }
    break;

  case 431:
#line 2324 "parser.y"
    { 
          (yyval)=(yyvsp[(1) - (3)]);
          statement((yyval), ID_gcc_local_label);
          
          // put these into the scope
          forall_operands(it, stack((yyvsp[(2) - (3)])))
          {
            // labels have a separate name space
            irep_idt base_name=it->get(ID_identifier);
            irep_idt id="label-"+id2string(base_name);
            ansi_c_parsert::identifiert &i=PARSER.current_scope().name_map[id];
            i.id_class=ANSI_C_LOCAL_LABEL;
            i.base_name=base_name;
          }

          stack((yyval)).add(ID_label).get_sub().swap((irept::subt&)stack((yyvsp[(2) - (3)])).operands());
        }
    break;

  case 432:
#line 2345 "parser.y"
    {
          init((yyval));
          mto((yyval), (yyvsp[(1) - (1)]));
        }
    break;

  case 433:
#line 2350 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          mto((yyval), (yyvsp[(3) - (3)]));
        }
    break;

  case 435:
#line 2361 "parser.y"
    { (yyval)=(yyvsp[(1) - (6)]);
          statement((yyval), ID_asm);
          stack((yyval)).set(ID_flavor, ID_gcc);
          stack((yyval)).operands().swap(stack((yyvsp[(4) - (6)])).operands());
        }
    break;

  case 436:
#line 2367 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (5)]);
          statement((yyval), ID_asm);
          stack((yyval)).set(ID_flavor, ID_gcc);
          stack((yyval)).operands().resize(5);
          stack((yyval)).op0()=stack((yyvsp[(4) - (5)]));
        }
    break;

  case 437:
#line 2378 "parser.y"
    { (yyval)=(yyvsp[(1) - (4)]);
          statement((yyval), ID_asm);
          stack((yyval)).set(ID_flavor, ID_msc);
          mto((yyval), (yyvsp[(3) - (4)]));
        }
    break;

  case 438:
#line 2384 "parser.y"
    { (yyval)=(yyvsp[(1) - (2)]);
          statement((yyval), ID_asm);
          stack((yyval)).set(ID_flavor, ID_msc);
          mto((yyval), (yyvsp[(2) - (2)]));
        }
    break;

  case 439:
#line 2394 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (7)]);
          statement((yyval), ID_msc_try_except);
          mto((yyval), (yyvsp[(2) - (7)]));
          mto((yyval), (yyvsp[(5) - (7)]));
          mto((yyval), (yyvsp[(7) - (7)]));
        }
    break;

  case 440:
#line 2403 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (4)]);
          statement((yyval), ID_msc_try_finally);
          mto((yyval), (yyvsp[(2) - (4)]));
          mto((yyval), (yyvsp[(4) - (4)]));
        }
    break;

  case 441:
#line 2410 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (1)]);
          statement((yyval), ID_msc_leave);
        }
    break;

  case 442:
#line 2418 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (2)]);
          statement((yyval), ID_CPROVER_throw);
        }
    break;

  case 443:
#line 2424 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (4)]);
          statement((yyval), ID_CPROVER_try_catch);
          mto((yyval), (yyvsp[(2) - (4)]));
          mto((yyval), (yyvsp[(4) - (4)]));
        }
    break;

  case 444:
#line 2432 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (4)]);
          statement((yyval), ID_CPROVER_try_finally);
          mto((yyval), (yyvsp[(2) - (4)]));
          mto((yyval), (yyvsp[(4) - (4)]));
        }
    break;

  case 450:
#line 2458 "parser.y"
    {
          init((yyval));
          stack((yyval)).operands().resize(5);
          stack((yyval)).operands()[0]=stack((yyvsp[(1) - (1)]));
        }
    break;

  case 451:
#line 2464 "parser.y"
    {
          init((yyval));
          stack((yyval)).operands().resize(5);
          stack((yyval)).operands()[0]=stack((yyvsp[(1) - (2)]));
          stack((yyval)).operands()[1]=stack((yyvsp[(2) - (2)]));
        }
    break;

  case 452:
#line 2471 "parser.y"
    {
          init((yyval));
          stack((yyval)).operands().resize(5);
          stack((yyval)).operands()[0]=stack((yyvsp[(1) - (3)]));
          stack((yyval)).operands()[1]=stack((yyvsp[(2) - (3)]));
          stack((yyval)).operands()[2]=stack((yyvsp[(3) - (3)]));
        }
    break;

  case 453:
#line 2479 "parser.y"
    {
          init((yyval));
          stack((yyval)).operands().resize(5);
          stack((yyval)).operands()[0]=stack((yyvsp[(1) - (4)]));
          stack((yyval)).operands()[1]=stack((yyvsp[(2) - (4)]));
          stack((yyval)).operands()[2]=stack((yyvsp[(3) - (4)]));
          stack((yyval)).operands()[3]=stack((yyvsp[(4) - (4)]));
        }
    break;

  case 454:
#line 2488 "parser.y"
    {
          init((yyval));
          stack((yyval)).operands().resize(5);
          stack((yyval)).operands()[0]=stack((yyvsp[(1) - (5)]));
          stack((yyval)).operands()[1]=stack((yyvsp[(2) - (5)]));
          stack((yyval)).operands()[2]=stack((yyvsp[(3) - (5)]));
          stack((yyval)).operands()[3]=stack((yyvsp[(4) - (5)]));
          stack((yyval)).operands()[4]=stack((yyvsp[(5) - (5)]));
        }
    break;

  case 456:
#line 2504 "parser.y"
    {
          (yyval)=(yyvsp[(2) - (2)]);
        }
    break;

  case 458:
#line 2512 "parser.y"
    {
          (yyval)=(yyvsp[(2) - (4)]);
          stack((yyval)).id(ID_gcc_asm_output);
          stack((yyval)).move_to_operands(stack((yyvsp[(1) - (4)])), stack((yyvsp[(3) - (4)]))); 
        }
    break;

  case 459:
#line 2519 "parser.y"
    {
          (yyval)=(yyvsp[(5) - (7)]);
          stack((yyval)).id(ID_gcc_asm_output);
          stack((yyval)).move_to_operands(stack((yyvsp[(4) - (7)])), stack((yyvsp[(6) - (7)]))); 
        }
    break;

  case 460:
#line 2528 "parser.y"
    {
          init((yyval), irep_idt());
          mto((yyval), (yyvsp[(1) - (1)]));
        }
    break;

  case 461:
#line 2533 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          mto((yyval), (yyvsp[(3) - (3)]));
        }
    break;

  case 462:
#line 2541 "parser.y"
    {
          (yyval)=(yyvsp[(2) - (2)]);
        }
    break;

  case 464:
#line 2549 "parser.y"
    {
          (yyval)=(yyvsp[(2) - (4)]);
          stack((yyval)).id(ID_gcc_asm_input);
          stack((yyval)).move_to_operands(stack((yyvsp[(1) - (4)])), stack((yyvsp[(3) - (4)]))); 
        }
    break;

  case 465:
#line 2556 "parser.y"
    {
          (yyval)=(yyvsp[(5) - (7)]);
          stack((yyval)).id(ID_gcc_asm_input);
          stack((yyval)).move_to_operands(stack((yyvsp[(4) - (7)])), stack((yyvsp[(6) - (7)]))); 
        }
    break;

  case 466:
#line 2565 "parser.y"
    {
          init((yyval), irep_idt());
          mto((yyval), (yyvsp[(1) - (1)]));
        }
    break;

  case 467:
#line 2570 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          mto((yyval), (yyvsp[(3) - (3)]));
        }
    break;

  case 468:
#line 2578 "parser.y"
    {
          (yyval)=(yyvsp[(2) - (2)]);
        }
    break;

  case 470:
#line 2586 "parser.y"
    {
          init((yyval), ID_gcc_asm_clobbered_register);
          mto((yyval), (yyvsp[(1) - (1)]));
        }
    break;

  case 471:
#line 2594 "parser.y"
    {
          init((yyval), irep_idt());
          mto((yyval), (yyvsp[(1) - (1)]));
        }
    break;

  case 472:
#line 2599 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          mto((yyval), (yyvsp[(3) - (3)]));
        }
    break;

  case 473:
#line 2607 "parser.y"
    {
          (yyval)=(yyvsp[(2) - (2)]);
        }
    break;

  case 475:
#line 2615 "parser.y"
    {
          init((yyval));
          mto((yyval), (yyvsp[(1) - (1)]));
        }
    break;

  case 476:
#line 2620 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          mto((yyval), (yyvsp[(3) - (3)]));
        }
    break;

  case 477:
#line 2628 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (1)]);
          irep_idt identifier=PARSER.lookup_label(stack((yyval)).get(ID_C_base_name));
          stack((yyval)).id(ID_label);
          stack((yyval)).set(ID_identifier, identifier);
        }
    break;

  case 482:
#line 2647 "parser.y"
    {
          // put into global list of items
          PARSER.copy_item(to_ansi_c_declaration(stack((yyvsp[(1) - (1)]))));
        }
    break;

  case 483:
#line 2652 "parser.y"
    {
          PARSER.copy_item(to_ansi_c_declaration(stack((yyvsp[(1) - (1)]))));
        }
    break;

  case 486:
#line 2661 "parser.y"
    {
          // Not obvious what to do with this.
        }
    break;

  case 487:
#line 2669 "parser.y"
    {
          // The head is a declaration with one declarator,
          // and the body becomes the 'value'.
          (yyval)=(yyvsp[(1) - (2)]);
          ansi_c_declarationt &ansi_c_declaration=
            to_ansi_c_declaration(stack((yyval)));
            
          assert(ansi_c_declaration.declarators().size()==1);
          ansi_c_declaration.add_initializer(stack((yyvsp[(2) - (2)])));
          
          // Kill the scope that 'function_head' creates.
          PARSER.pop_scope();
          
          // We are no longer in any function.
          PARSER.set_function(irep_idt());
        }
    break;

  case 489:
#line 2693 "parser.y"
    {
          init((yyval));
        }
    break;

  case 491:
#line 2701 "parser.y"
    {
          init((yyval), ID_decl_block);
          mto((yyval), (yyvsp[(1) - (1)]));
        }
    break;

  case 492:
#line 2706 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (2)]);
          mto((yyval), (yyvsp[(2) - (2)]));
        }
    break;

  case 495:
#line 2720 "parser.y"
    {
          (yyval)=merge((yyvsp[(2) - (2)]), (yyvsp[(1) - (2)]));
        }
    break;

  case 496:
#line 2724 "parser.y"
    {
          (yyval)=merge((yyvsp[(2) - (2)]), (yyvsp[(1) - (2)]));
        }
    break;

  case 497:
#line 2731 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 498:
#line 2735 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 499:
#line 2739 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 500:
#line 2743 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 501:
#line 2751 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 502:
#line 2755 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 503:
#line 2759 "parser.y"
    {
          (yyval)=merge((yyvsp[(1) - (3)]), merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)])));
        }
    break;

  case 504:
#line 2767 "parser.y"
    {
          stack((yyvsp[(2) - (4)])).set(ID_tag, stack((yyvsp[(3) - (4)])));
          (yyval)=merge((yyvsp[(1) - (4)]), merge((yyvsp[(2) - (4)]), (yyvsp[(4) - (4)])));
        }
    break;

  case 505:
#line 2772 "parser.y"
    {
          stack((yyvsp[(2) - (4)])).id(ID_c_enum_tag);
          stack((yyvsp[(2) - (4)])).set(ID_tag, stack((yyvsp[(3) - (4)])));
          (yyval)=merge((yyvsp[(1) - (4)]), merge((yyvsp[(2) - (4)]), (yyvsp[(4) - (4)])));
        }
    break;

  case 509:
#line 2788 "parser.y"
    {
          init((yyval), ID_declaration);
          stack((yyval)).type().swap(stack((yyvsp[(1) - (2)])));
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(2) - (2)])));
        }
    break;

  case 510:
#line 2794 "parser.y"
    {
          init((yyval), ID_declaration);
          stack((yyval)).type().swap(stack((yyvsp[(1) - (2)])));
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(2) - (2)])));
        }
    break;

  case 511:
#line 2800 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(3) - (3)])));
        }
    break;

  case 512:
#line 2808 "parser.y"
    {
          init((yyval), ID_declaration);
          irept return_type(ID_int);
          stack((yyval)).type().swap(return_type);
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(1) - (1)])));
          create_function_scope((yyval));
        }
    break;

  case 513:
#line 2816 "parser.y"
    {
          init((yyval), ID_declaration);
          stack((yyval)).type().swap(stack((yyvsp[(1) - (2)])));
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(2) - (2)])));
          create_function_scope((yyval));
        }
    break;

  case 514:
#line 2823 "parser.y"
    {
          init((yyval), ID_declaration);
          stack((yyval)).type().swap(stack((yyvsp[(1) - (2)])));
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(2) - (2)])));
          create_function_scope((yyval));
        }
    break;

  case 515:
#line 2830 "parser.y"
    {
          init((yyval), ID_declaration);
          stack((yyval)).type().swap(stack((yyvsp[(1) - (2)])));
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(2) - (2)])));
          create_function_scope((yyval));
        }
    break;

  case 516:
#line 2837 "parser.y"
    {
          init((yyval), ID_declaration);
          stack((yyval)).type().swap(stack((yyvsp[(1) - (2)])));
          PARSER.add_declarator(stack((yyval)), stack((yyvsp[(2) - (2)])));
          create_function_scope((yyval));
        }
    break;

  case 522:
#line 2858 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (2)]);
          make_subtype((yyval), (yyvsp[(2) - (2)]));
        }
    break;

  case 525:
#line 2868 "parser.y"
    {
          (yyval)=(yyvsp[(2) - (2)]);
          do_pointer((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 526:
#line 2873 "parser.y"
    {
          (yyval)=merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)]));
          do_pointer((yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]));
        }
    break;

  case 527:
#line 2881 "parser.y"
    { (yyval) = (yyvsp[(2) - (3)]); }
    break;

  case 528:
#line 2883 "parser.y"
    {
          /* note: this is a pointer ($2) to a function ($4) */
          /* or an array ($4)! */
          (yyval)=(yyvsp[(2) - (4)]);
          make_subtype((yyval), (yyvsp[(4) - (4)]));
        }
    break;

  case 530:
#line 2894 "parser.y"
    {
          (yyval)=(yyvsp[(3) - (4)]);
          do_pointer((yyvsp[(1) - (4)]), (yyvsp[(3) - (4)]));
        }
    break;

  case 531:
#line 2899 "parser.y"
    {
          // not sure where the type qualifiers belong
          (yyval)=merge((yyvsp[(2) - (5)]), (yyvsp[(4) - (5)]));
          do_pointer((yyvsp[(1) - (5)]), (yyvsp[(2) - (5)]));
        }
    break;

  case 532:
#line 2905 "parser.y"
    {
          (yyval)=(yyvsp[(2) - (2)]);
          do_pointer((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 533:
#line 2910 "parser.y"
    {
          (yyval)=merge((yyvsp[(2) - (3)]), (yyvsp[(3) - (3)]));
          do_pointer((yyvsp[(1) - (3)]), (yyvsp[(2) - (3)]));
        }
    break;

  case 534:
#line 2918 "parser.y"
    { (yyval) = (yyvsp[(2) - (3)]); }
    break;

  case 535:
#line 2920 "parser.y"
    {        /* note: this is a function ($3) with a typedef name ($2) */
          (yyval)=(yyvsp[(2) - (4)]);
          make_subtype((yyval), (yyvsp[(3) - (4)]));
        }
    break;

  case 536:
#line 2925 "parser.y"
    {
          /* note: this is a pointer ($2) to a function ($4) */
          /* or an array ($4)! */
          (yyval)=(yyvsp[(2) - (4)]);
          make_subtype((yyval), (yyvsp[(4) - (4)]));
        }
    break;

  case 538:
#line 2936 "parser.y"
    { (yyval)=(yyvsp[(2) - (3)]); }
    break;

  case 542:
#line 2947 "parser.y"
    {
          (yyval)=(yyvsp[(2) - (2)]);
          do_pointer((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 543:
#line 2952 "parser.y"
    {
          // This is an Apple extension to C/C++/Objective C.
          // http://en.wikipedia.org/wiki/Blocks_(C_language_extension)
          (yyval)=(yyvsp[(2) - (2)]);
          do_pointer((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 544:
#line 2959 "parser.y"
    {
          // the type_qualifier_list is for the pointer,
          // and not the identifier_declarator
          stack_type((yyvsp[(1) - (3)])).id(ID_pointer);
          stack_type((yyvsp[(1) - (3)])).subtype()=typet(ID_abstract);
          (yyvsp[(2) - (3)])=merge((yyvsp[(2) - (3)]), (yyvsp[(1) - (3)])); // dest=$2
          make_subtype((yyvsp[(3) - (3)]), (yyvsp[(2) - (3)])); // dest=$3
          (yyval)=(yyvsp[(3) - (3)]);
        }
    break;

  case 545:
#line 2972 "parser.y"
    {
          /* note: this is a function or array ($2) with name ($1) */
          (yyval)=(yyvsp[(1) - (2)]);
          make_subtype((yyval), (yyvsp[(2) - (2)]));
        }
    break;

  case 546:
#line 2978 "parser.y"
    { (yyval) = (yyvsp[(2) - (3)]); }
    break;

  case 547:
#line 2980 "parser.y"
    {
          /* note: this is a pointer ($2) to a function ($4) */
          /* or an array ($4)! */
          (yyval)=(yyvsp[(2) - (4)]);
          make_subtype((yyval), (yyvsp[(4) - (4)]));
        }
    break;

  case 548:
#line 2990 "parser.y"
    {
          // We remember the last declarator for the benefit
          // of function argument scoping.
          PARSER.current_scope().last_declarator=
            stack((yyvsp[(1) - (1)])).get(ID_identifier);
        }
    break;

  case 549:
#line 2997 "parser.y"
    { (yyval)=(yyvsp[(2) - (3)]); }
    break;

  case 556:
#line 3017 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          set((yyval), ID_code);
          stack_type((yyval)).subtype()=typet(ID_abstract);
          stack_type((yyval)).add(ID_parameters);
          stack_type((yyval)).set(ID_C_KnR, true);
        }
    break;

  case 557:
#line 3025 "parser.y"
    {
            // Use last declarator (i.e., function name) to name
            // the scope.
            PARSER.new_scope(
              id2string(PARSER.current_scope().last_declarator)+"::");
          }
    break;

  case 558:
#line 3034 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (5)]);
          set((yyval), ID_code);
          stack_type((yyval)).subtype()=typet(ID_abstract);
          stack_type((yyval)).add(ID_parameters).get_sub().
            swap((irept::subt &)(stack_type((yyvsp[(3) - (5)])).subtypes()));
          PARSER.pop_scope();
          adjust_KnR_parameters(stack((yyval)).add(ID_parameters), stack((yyvsp[(5) - (5)])));
          stack((yyval)).set(ID_C_KnR, true);
        }
    break;

  case 560:
#line 3049 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (2)]);
          set((yyval), ID_code);
          stack_type((yyval)).add(ID_parameters);
          stack_type((yyval)).subtype()=typet(ID_abstract);
        }
    break;

  case 561:
#line 3056 "parser.y"
    {
            // Use last declarator (i.e., function name) to name
            // the scope.
            PARSER.new_scope(
              id2string(PARSER.current_scope().last_declarator)+"::");
          }
    break;

  case 562:
#line 3064 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (4)]);
          set((yyval), ID_code);
          stack_type((yyval)).subtype()=typet(ID_abstract);
          stack_type((yyval)).add(ID_parameters).get_sub().
            swap((irept::subt &)(stack_type((yyvsp[(3) - (4)])).subtypes()));
          PARSER.pop_scope();
        }
    break;

  case 563:
#line 3076 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (2)]);
          set((yyval), ID_array);
          stack_type((yyval)).subtype()=typet(ID_abstract);
          stack_type((yyval)).add(ID_size).make_nil();
        }
    break;

  case 564:
#line 3083 "parser.y"
    {
          // this is C99: e.g., restrict, const, etc
          // The type qualifier belongs to the array, not the
          // contents of the array, nor the size.
          set((yyvsp[(1) - (3)]), ID_array);
          stack_type((yyvsp[(1) - (3)])).subtype()=typet(ID_abstract);
          stack_type((yyvsp[(1) - (3)])).add(ID_size).make_nil();
          (yyval)=merge((yyvsp[(2) - (3)]), (yyvsp[(1) - (3)]));
        }
    break;

  case 565:
#line 3093 "parser.y"
    {
          // these should be allowed in prototypes only
          (yyval)=(yyvsp[(1) - (3)]);
          set((yyval), ID_array);
          stack_type((yyval)).subtype()=typet(ID_abstract);
          stack_type((yyval)).add(ID_size).make_nil();
        }
    break;

  case 566:
#line 3101 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (3)]);
          set((yyval), ID_array);
          stack_type((yyval)).add(ID_size).swap(stack((yyvsp[(2) - (3)])));
          stack_type((yyval)).subtype()=typet(ID_abstract);
        }
    break;

  case 567:
#line 3108 "parser.y"
    {
          // The type qualifier belongs to the array, not the
          // contents of the array, nor the size.
          set((yyvsp[(1) - (4)]), ID_array);
          stack_type((yyvsp[(1) - (4)])).add(ID_size).swap(stack((yyvsp[(3) - (4)])));
          stack_type((yyvsp[(1) - (4)])).subtype()=typet(ID_abstract);
          (yyval)=merge((yyvsp[(2) - (4)]), (yyvsp[(1) - (4)])); // dest=$2
        }
    break;

  case 568:
#line 3117 "parser.y"
    {
          // we need to push this down
          (yyval)=(yyvsp[(1) - (4)]);
          set((yyvsp[(2) - (4)]), ID_array);
          stack_type((yyvsp[(2) - (4)])).add(ID_size).swap(stack((yyvsp[(3) - (4)])));
          stack_type((yyvsp[(2) - (4)])).subtype()=typet(ID_abstract);
          make_subtype((yyvsp[(1) - (4)]), (yyvsp[(2) - (4)]));
        }
    break;

  case 569:
#line 3126 "parser.y"
    {
          // these should be allowed in prototypes only
          // we need to push this down
          (yyval)=(yyvsp[(1) - (4)]);
          set((yyvsp[(2) - (4)]), ID_array);
          stack_type((yyvsp[(2) - (4)])).add(ID_size).make_nil();
          stack_type((yyvsp[(2) - (4)])).subtype()=typet(ID_abstract);
          make_subtype((yyvsp[(1) - (4)]), (yyvsp[(2) - (4)]));
        }
    break;

  case 570:
#line 3139 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (1)]);
          set((yyval), ID_pointer);
          stack_type((yyval)).subtype()=typet(ID_abstract);
        }
    break;

  case 571:
#line 3145 "parser.y"
    {
          // The type_qualifier_list belongs to the pointer,
          // not to the (missing) abstract declarator.
          set((yyvsp[(1) - (2)]), ID_pointer);
          stack_type((yyvsp[(1) - (2)])).subtype()=typet(ID_abstract);
          (yyval)=merge((yyvsp[(2) - (2)]), (yyvsp[(1) - (2)]));
        }
    break;

  case 572:
#line 3153 "parser.y"
    {
          (yyval)=(yyvsp[(2) - (2)]);
          do_pointer((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 573:
#line 3158 "parser.y"
    {
          // The type_qualifier_list belongs to the pointer,
          // not to the abstract declarator.
          stack_type((yyvsp[(1) - (3)])).id(ID_pointer);
          stack_type((yyvsp[(1) - (3)])).subtype()=typet(ID_abstract);
          (yyvsp[(2) - (3)])=merge((yyvsp[(2) - (3)]), (yyvsp[(1) - (3)])); // dest=$2
          make_subtype((yyvsp[(3) - (3)]), (yyvsp[(2) - (3)])); // dest=$3
          (yyval)=(yyvsp[(3) - (3)]);
        }
    break;

  case 574:
#line 3168 "parser.y"
    {
          // This is an Apple extension to C/C++/Objective C.
          // http://en.wikipedia.org/wiki/Blocks_(C_language_extension)
          (yyval)=(yyvsp[(1) - (1)]);
          set((yyval), ID_block_pointer);
          stack_type((yyval)).subtype()=typet(ID_abstract);
        }
    break;

  case 575:
#line 3179 "parser.y"
    {
          (yyval)=(yyvsp[(1) - (1)]);
          set((yyval), ID_pointer);
          stack_type((yyval)).subtype()=typet(ID_abstract);
        }
    break;

  case 576:
#line 3185 "parser.y"
    {
          // The type_qualifier_list belongs to the pointer,
          // not to the (missing) abstract declarator.
          set((yyvsp[(1) - (2)]), ID_pointer);
          stack_type((yyvsp[(1) - (2)])).subtype()=typet(ID_abstract);
          (yyval)=merge((yyvsp[(2) - (2)]), (yyvsp[(1) - (2)]));
        }
    break;

  case 577:
#line 3193 "parser.y"
    {
          (yyval)=(yyvsp[(2) - (2)]);
          do_pointer((yyvsp[(1) - (2)]), (yyvsp[(2) - (2)]));
        }
    break;

  case 578:
#line 3198 "parser.y"
    {
          // The type_qualifier_list belongs to the pointer,
          // not to the (missing) abstract declarator.
          stack((yyvsp[(1) - (3)])).id(ID_pointer);
          stack_type((yyvsp[(1) - (3)])).subtype()=typet(ID_abstract);
          (yyvsp[(2) - (3)])=merge((yyvsp[(2) - (3)]), (yyvsp[(1) - (3)])); // dest=$2
          make_subtype((yyvsp[(3) - (3)]), (yyvsp[(2) - (3)])); // dest=$3
          (yyval)=(yyvsp[(3) - (3)]);
        }
    break;

  case 579:
#line 3208 "parser.y"
    {
          // This is an Apple extension to C/C++/Objective C.
          // http://en.wikipedia.org/wiki/Blocks_(C_language_extension)
          (yyval)=(yyvsp[(1) - (1)]);
          set((yyval), ID_block_pointer);
          stack_type((yyval)).subtype()=typet(ID_abstract);
        }
    break;

  case 580:
#line 3219 "parser.y"
    { (yyval) = (yyvsp[(2) - (3)]); }
    break;

  case 581:
#line 3221 "parser.y"
    { (yyval) = (yyvsp[(2) - (3)]); }
    break;

  case 582:
#line 3223 "parser.y"
    { (yyval) = (yyvsp[(2) - (3)]); }
    break;

  case 583:
#line 3225 "parser.y"
    {
          /* note: this is a pointer ($2) to a function or array ($4) */
          (yyval)=(yyvsp[(2) - (4)]);
          make_subtype((yyval), (yyvsp[(4) - (4)]));
        }
    break;

  case 584:
#line 3231 "parser.y"
    {
          /* note: this is a pointer ($2) to a function or array ($4) */
          (yyval)=(yyvsp[(2) - (4)]);
          make_subtype((yyval), (yyvsp[(4) - (4)]));
        }
    break;

  case 585:
#line 3240 "parser.y"
    { (yyval) = (yyvsp[(2) - (3)]); }
    break;

  case 586:
#line 3242 "parser.y"
    { (yyval) = (yyvsp[(2) - (3)]); }
    break;

  case 588:
#line 3245 "parser.y"
    {
          /* note: this is a pointer ($2) to a function ($4) */
          (yyval)=(yyvsp[(2) - (4)]);
          make_subtype((yyval), (yyvsp[(4) - (4)]));
        }
    break;


/* Line 1267 of yacc.c.  */
#line 7468 "ansi_c_y.tab.cpp"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}


#line 3252 "parser.y"


