/*  Generated 0.0.0.0.0.0 (-t2 -r1 -u3 -bcbmc) 2016-06-13 06:53:14  */
#include <stdio.h>
#include <stdlib.h>

#define THREADS 2
#define ROUNDS 1

#define STOP_VOID(A) return;
#define STOP_NONVOID(A) return 0;

#define IF(T,A,B)  if (pc[T] > A || (A >= pc_cs[T])) goto B;
#define GUARD(T,B)  assume( pc_cs[T] >= B );

#ifndef NULL
#define NULL 0
#endif

#define assume(x) __CPROVER_assume(x)
#define __VERIFIER_assume(x) __CPROVER_assume(x)

#define assert(x) assert(x)
#define __VERIFIER_assert(x) assert(x)

#define __VERIFIER_nondet_int() nondet_int()
int nondet_int();

#define __VERIFIER_nondet_uint() nondet_uint()
unsigned int nondet_uint();
unsigned int nondet_uint();

unsigned __CPROVER_bitvector[1] active_thread[THREADS+1]={1};                //unsigned char active_thread[THREADS+1]={1};
unsigned __CPROVER_bitvector[3] pc[THREADS+1];                                          //unsigned char active_thread[THREADS+1]={1};
unsigned __CPROVER_bitvector[4] pc_cs[THREADS+1];                                    //unsigned int pc[THREADS+1];
unsigned int thread_index;                                 //unsigned int thread_index;
unsigned __CPROVER_bitvector[3] thread_lines[] = {5, 7, 7}; //unsigned __CPROVER_bitvector[4] thread_lines[] = {3, 8, 8};

void * __cs_safe_malloc(size_t size) { void * ptr = malloc(size); assume(ptr); return ptr; }
void * __cs_unsafe_malloc(size_t size) { if(nondet_int()) return 0; return malloc(size); }
//#define __cs_unsafe_malloc malloc

void __cs_init_scalar(void *var, size_t size) {
	if (size == sizeof(int))
		*(int *)var = nondet_int();
	else {
		char * ptr = (char *)var;
		size_t j;

		//for (j=0; j<size; j++)
		//	ptr[j] = __cs_nondet_char();
	}
}

/* pthread API */
typedef int __cs_mutex_t;
typedef int __cs_cond_t;
typedef int __cs_t;

int __cs_mutex_init (__cs_mutex_t *m, int val) { *m = -1; return 0; }
int __cs_mutex_destroy(__cs_mutex_t *m) { return 0; }
int __cs_mutex_lock(__cs_mutex_t *m) { assume(*m == -1); *m = thread_index; return 0; } 
int __cs_mutex_unlock(__cs_mutex_t *m) { assert(*m == thread_index); *m = -1; return 0; }

void *threadargs[THREADS+1];

int __cs_create(__cs_t *id, void *attr, void *(*t)(void*), void *arg, int threadID) { if (threadID > THREADS) return 0;  *id = threadID; active_thread[threadID] = 1; threadargs[threadID] = arg; return 0; }
int __cs_join(__cs_t id, void **value_ptr) { assume(pc[id] == thread_lines[id]); return 0; }
int __cs_exit(void *value_ptr) { return 0; }   // only for parsing

int __cs_cond_wait(__cs_cond_t *cond, __cs_mutex_t *mutex) { __cs_mutex_unlock(mutex); assume(*cond != -1); __cs_mutex_lock(mutex); return 0; }
int __cs_cond_signal(__cs_cond_t *cond) { *cond = 1; return 0; }
int __cs_cond_init(__cs_cond_t *cond, void *attr) { *cond = -1; return 0; }



/* here */



                                                       static int turn = 1;
                                                       static int flag[3] = {0, 0, 0};
                                                       static int x = 0;
                                                       void t_one_0(void *__csLOCALPARAM_t_one___cs_unused)
                                                       {
                                                       	static int __csLOCAL_t_one_taskId = 1;
                                                       	static int __csLOCAL_t_one_other = 0;
IF(1,0,tt_one_0_1)                                     	__csLOCAL_t_one_other = (__csLOCAL_t_one_taskId % 2) + 1;
tt_one_0_1: IF(1,1,tt_one_0_2)                         	flag[__csLOCAL_t_one_taskId] = 1;
tt_one_0_2: IF(1,2,tt_one_0_3)                         	turn = __csLOCAL_t_one_other;
tt_one_0_3: IF(1,3,tt_one_0_4)                         	assume(!(flag[__csLOCAL_t_one_other] && (turn == __csLOCAL_t_one_other)));
                                                       	static int __csLOCAL_t_one_ax;
tt_one_0_4: IF(1,4,tt_one_0_5)                         	__csLOCAL_t_one_ax = x;
                                                       	__csLOCAL_t_one_ax = __csLOCAL_t_one_ax + 1;
tt_one_0_5: IF(1,5,tt_one_0_6)                         	x = __csLOCAL_t_one_ax;
tt_one_0_6: IF(1,6,tt_one_0_7)                         	flag[__csLOCAL_t_one_taskId] = 0;
                                                       	__exit_t_one: GUARD(1,7)
                                                       	;
tt_one_0_7:                                            	STOP_VOID(7);
                                                       }
                                                       
                                                       
                                                       
                                                       void t_two_0(void *__csLOCALPARAM_t_two___cs_unused)
                                                       {
                                                       	static int __csLOCAL_t_two_taskId = 2;
                                                       	static int __csLOCAL_t_two_other = 0;
IF(2,0,tt_two_0_1)                                     	__csLOCAL_t_two_other = (__csLOCAL_t_two_taskId % 2) + 1;
tt_two_0_1: IF(2,1,tt_two_0_2)                         	flag[__csLOCAL_t_two_taskId] = 1;
tt_two_0_2: IF(2,2,tt_two_0_3)                         	turn = __csLOCAL_t_two_other;
tt_two_0_3: IF(2,3,tt_two_0_4)                         	assume(!(flag[__csLOCAL_t_two_other] && (turn == __csLOCAL_t_two_other)));
                                                       	static int __csLOCAL_t_two_bx;
tt_two_0_4: IF(2,4,tt_two_0_5)                         	__csLOCAL_t_two_bx = x;
                                                       	__csLOCAL_t_two_bx = __csLOCAL_t_two_bx - 1;
tt_two_0_5: IF(2,5,tt_two_0_6)                         	x = __csLOCAL_t_two_bx;
tt_two_0_6: IF(2,6,tt_two_0_7)                         	flag[__csLOCAL_t_two_taskId] = 0;
                                                       	__exit_t_two: GUARD(2,7)
                                                       	;
tt_two_0_7:                                            	STOP_VOID(7);
                                                       }
                                                       
                                                       
                                                       
                                                       int main_thread(void)
                                                       {void;
IF(0,0,tmain_1)                                        	static __cs_t __csLOCAL_main_t1;
                                                       	__cs_init_scalar(&__csLOCAL_main_t1, sizeof(__cs_t));
                                                       	static __cs_t __csLOCAL_main_t2;
                                                       	__cs_init_scalar(&__csLOCAL_main_t2, sizeof(__cs_t));
                                                       	__cs_create(&__csLOCAL_main_t1, 0, t_one_0, 0, 1);
tmain_1: IF(0,1,tmain_2)                               	__cs_create(&__csLOCAL_main_t2, 0, t_two_0, 0, 2);
tmain_2: IF(0,2,tmain_3)                               	__cs_join(__csLOCAL_main_t1, 0);
tmain_3: IF(0,3,tmain_4)                               	__cs_join(__csLOCAL_main_t2, 0);
tmain_4: IF(0,4,tmain_5)                               	assert(x == 0);
                                                       	goto __exit_main;
                                                       	__exit_main: GUARD(0,5)
                                                       	;
tmain_5:                                               	STOP_NONVOID(5);
                                                       }
                                                       
                                                       
                                                       
                                                       int main(void) {
                                                                 unsigned __CPROVER_bitvector[3] tmp_t0_r0;
                                                                 unsigned __CPROVER_bitvector[3] tmp_t1_r0;
                                                                 unsigned __CPROVER_bitvector[3] tmp_t2_r0;
                                                                 unsigned __CPROVER_bitvector[3] tmp_t0_r1;
                                                       
                                                                 // round 0
                                                                 thread_index = 0;
                                                                 pc_cs[0] = pc[0] + tmp_t0_r0;
                                                                 assume(pc_cs[0] > 0);
                                                                 assume(pc_cs[0] <= 5);
                                                                 main_thread();
                                                                 pc[0] = pc_cs[0];
                                                       
                                                                 thread_index = 1;
                                                                 if (active_thread[thread_index] == 1) { // t_one_0
                                                                    pc_cs[1] = pc[1] + tmp_t1_r0;
                                                                    assume(pc_cs[1] <= 7);
                                                                    t_one_0(threadargs[1]);
                                                                    pc[1] = pc_cs[1];
                                                                 }
                                                       
                                                                 thread_index = 2;
                                                                 if (active_thread[thread_index] == 1) { // t_two_0
                                                                    pc_cs[2] = pc[2] + tmp_t2_r0;
                                                                    assume(pc_cs[2] <= 7);
                                                                    t_two_0(threadargs[2]);
                                                                    pc[2] = pc_cs[2];
                                                                 }
                                                       
                                                                 thread_index = 0;
                                                                 if (active_thread[0] == 1) {
                                                                    pc_cs[0] = pc[0] + tmp_t0_r1;
                                                                    assume(pc_cs[0] <= 5);
                                                                    main_thread();
                                                                 }
                                                       
                                                          return 0;
                                                       }
                                                       


