#include <stdio.h>
#include <assert.h>


#define NB_TASK 2 //without task main
#define NB_ENTRY 2
#define NB_PROCEDURE 0
#define NB_FUNCTION 0

#define TRUE 1
#define FALSE 0

#define FREE -1
#define DESTROY -2

////////////////////////////////////////////////////////////////
static int turn = 1;
static int flag[NB_TASK+1] = {FALSE, FALSE, FALSE};

static int x = 0;

void t_one(){
    static int taskId = 1;
    static int other = 0;
    other = (taskId % NB_TASK) + 1;
    flag[taskId] = TRUE;
    
    turn = other;
    while (flag[other] && turn == other)
    {
    }
    
    static int ax;
    ax = x;
    ax = ax + 1;
    x = ax;
    
    flag[taskId] = FALSE;
}

void t_two(){
    static int taskId = 2;
    static int other = 0;
    other = (taskId % NB_TASK) + 1;
    flag[taskId] = TRUE;
    
    turn = other;
    while (flag[other] && turn == other)
    {
    }

    static int bx;
    bx = x;
    bx = bx - 1;
    x = bx;
    
    flag[taskId] = FALSE;
}

int main(void){
    pthread_t t1, t2;
    
    pthread_create(&t1, NULL, t_one, NULL);
    pthread_create(&t2, NULL, t_two, NULL);
    
    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    
    assert(x==0);
    return 0;

}