compile and export to $PATH : 
    
    tools/z3-osx/bin/z3 //with z3 compiled for Mac
    tools/cbmc-cbmc-5.4/src/cbmc/cbmc //with Minisat compiled for Mac
    tools/yices-2.4.1/bin/ //export all files in directory, with Yices compiled for Mac
    tools/doalarm/doalarm

to compile and run CBMC with Glucose:
    
    in the src directory, run:
    make glucose-download

    Edit the config.inc file:

    #MINISAT2 = ../../minisat-2.2.1
    GLUCOSE = ../../glucose-syrup

    Edit the solvers/sat/satcheck.h file:

    //#define SATCHECK_MINISAT2
    #define SATCHECK_GLUCOSE

    make
    
    rename ./src/cbmc/cbmc to ./src/cbmc/cbmc2 and export cbmc2 to $PATH //CBMC using Glucose as default solver
    
to compile Glucose only on Mac:
    
    in Glucose-xxx/core/SolverTypes.h
    comment this line : 
        friend Lit mkLit(Var var, bool sign = false);
    to 
        //friend Lit mkLit(Var var, bool sign = false);
        
to compile Yices on Mac : 

    use version 2.4.1 instead of 2.4.2
    
to use script:

    install 'matplotlib' in tools/
    