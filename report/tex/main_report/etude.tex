\chapter{Séquentialisation d'un programme C}
\label{chapter/technique}
	Une étude a été menée l'an dernier \cite{rapport-Abdelali} pour trouver un outil efficace qui génère automatiquement un programme C à partir de Ada, mais le résultat obtenu ne nous convient pas. Dans ce cas, on a décidé de le faire manuellement de Ada concurrent vers C concurrent en utilisant les threads POSIX, ensuite, de C concurrent vers C séquentiel grâce à la technique \textit{Lazy sequentialization} \cite{lazy-seq}.

\begin{wrapfigure}[28]{l}{0.5\textwidth} 
	\vspace{-20pt}
	\begin{framed}
	\vspace{-10pt}
	{\footnotesize
		\begin{lstlisting}
pthread_mutex_t m; int c=0;
void INC(void *b){
 int tmp=(*b);
 pthread_mutex_lock(&m);
 if(c>0)
  c++;
 else{
  c=0;
  while(tmp>0){
   c++; tmp--;  
  } 
 }
 pthread_mutex_unlock(&m);
}
void DEC(){
 assume(c>0);
 c--;
 assert(c>=0);
}
int main(void){
 int x=1, y=5;
 pthread_t inc0, inc1, dec0, dec1;
 pthread_mutex_init(&m);
 pthread_create(&inc0,NULL,INC,&x);
 pthread_create(&inc1,NULL,INC,&y);
 pthread_create(&dec0,NULL,DEC,NULL);
 pthread_create(&dec1,NULL,DEC,NULL);
 pthread_join(inc0,NULL);
 .........
 return 0;
}
		\end{lstlisting}		
	}			
	\vspace{-10pt}				
	\end{framed}
\vspace{-10pt}
	\caption{$P$ : Exemple d'un programme C}
	\label{exemple/C}
	\end{wrapfigure}

	Le but pour lequel on séquentialise un programme concurrent est de contrôler son indéterminisme qui est dû à son exécution parallèle et de la simuler par les exécutions séquentielles qui sont plus faciles à observer.
	La séquentialisation doit faciliter la vérification mais en gardant les comportements fondamentaux d'un programme concurrent. L'idée principale de la méthode \textit{Lazy sequentialization} \cite{lazy-seq} est de mettre en œuvre des mécanismes permettant de contrôler l'avancement, d'observer et de sauvegarder l'état courant du système avant chaque changement de contexte (CC). Un ordonnaceur \textit{round-robin} est mis en place pour gérer l'entrelacement entre les tâches avec un nombre de CC maximum, chaque tâche gère localement son état en le sauvegardant après chaque CC, cette approche permet de ne pas recalculer son état courant chaque fois lors du changement de contexte, cela diminue la complexité du programme après la séquentialisation en réduisant le nombre d'états inaccessibles. Afin de garder l'indéterminisme du programme, nous utilisons la fonction \textbf{nondet\_uint()} offerte par \textbf{CBMC} qui incrémente de façon indéterministe le nombre de pas possibles lors du prochain CC.

	Le programme séquentialisé $P_{K}^{seq}$ avec $K>0$ nombre de CC maximum, $seq$ séquentiel contient au final une nouvelle fonction \texttt{main} et les fonctions $f_0^{seq}...f_{NB\_TASK}^{seq}$ pour chaque thread $f_{i, i \in [0,NB\_TASK]}$ (y compris l'ancien \textit{main} $f_0$) qui ont des appels directs de fonction et les boucles dépliées.
	
	Lors de chaque CC, le \texttt{main} appelle chacune des fonctions $f_i^{seq}$ qui simulent les threads, mais en reprenant l'exécution au niveau du dernier CC ; pour cela on réalise plusieurs sauts vers la position où on s'est arrêté et on continue l'exécution jusqu'à la position du prochain CC calculée par le \texttt{main}. Pour garder l'état cohérent de chaque tâche, toutes les variables locales sont mises en \texttt{static}. 
	En plus, tous les sauts sont vers avant (les boucles sont dépliées, voir chapitre suivant), chaque instruction est ainsi exécutée au plus une seule fois. Les paramètres des tâches sont aussi sauvegardés.
	
	La figure \ref{exemple/C} montre un programme C-POSIX $P$ qui crée quatre threads, deux qui réalisent la fonction \texttt{INC} et deux autres qui réalisent la fonction \texttt{DEC}. La fonction \texttt{INC} a pour but d'incrémenter la variable partagée \texttt{c} selon sa valeur au début. Si \texttt{c>0}, elle va incrémenter une, sinon elle l'initialise \texttt{c} à zéro et l'incrémente d'un nombre correspondant avec son paramètre d'entrée. Cette incrémentation est protégée par un mutex (un verrou dans C). La fonction \texttt{DEC} décrémente la variable partagée \texttt{c} et contient deux fonctions spéciales : \texttt{assume} et \texttt{assert}.

	\begin{description}
		\item[assume(\textit{cond})] est une sorte d'hypothèse sur l'état du programme à ce point d'exécution. Si la condition \textit{cond} est vraie, l'exécution continue, sinon le programme se termine avec une exception qui nous montre que l'hypothèse est fausse. 
		Dans la vérification, les "assumes" sont également utilisés afin de  limiter les choix non-déterministes : si la condition \textit{cond} est vraie, la vérification peut procéder les instructions suivantes, sinon les instructions après cette fonction sont inaccessibles mais la vérification continue en changeant à une autre exécution telle que  la condition est vraie.  
		\item[assert(\textit{cond})] est une propriété que l'on veut garantir. Si la condition \textit{cond} est fausse, l'exécution se termine avec une exception qui désigne la violation de propriété. Ce comportement est le même dans la vérification ; ainsi les "asserts" sont utilisés dans la vérification
		pour capter les propriétés à vérifier.
 	\end{description}
	
	Exemple :	\\
	
\begin{figure}[!htb]\centering
   \begin{minipage}{0.2\textwidth}
     \vspace{-20pt}
	\begin{framed}
	\vspace{-10pt}
	{\footnotesize
		\begin{lstlisting}
/*exemple 1*/
assert(x>0);
		\end{lstlisting}		
	}
	\vspace{-10pt}							
	\end{framed}
	\vspace{-20pt}
	%\caption{}
	\label{exemple/assertAssume}
   \end{minipage}
   \begin {minipage}{0.2\textwidth}
   \vspace{-20pt}
    \begin{framed}
    \vspace{-10pt}
	{\footnotesize
		\begin{lstlisting}
/*exemple 2*/
assume(x>0);
		\end{lstlisting}		
	}		
	\vspace{-10pt}					
	\end{framed}
	\vspace{-20pt}     
	%\caption{}
     \label{exemple/assumeAssert}
   \end{minipage}
\end{figure}

	Ces deux exemples se terminent toujours si la condition \texttt{x>0} est fausse. Toutefois, le comportement dans un outil de vérification est différent. Avec \texttt{x=0} au début, dans l'exemple 1, la vérification	 se termine avec une violation de propriété, par contre, dans l'exemple 2, la vérification continue sans interruption.\\
		
	{\color{red} \underline{\textbf{REMARQUE}}} {\itshape On utilise indiféremment les termes tâches (Ada) ou threads (C)}

%\newpage
\section{Structure de données}
\label{chapter/technique/structureDonnées}
	\begin{description}
		\item[bool active{[NB\_TASK+1]}] stocke l'état des threads, vivant ou non. Inititalement, \textit{active}[0] est \textit{TRUE}, les autres sont \textit{FALSE}.
		\item[void arg{[NB\_TASK+1]}] stocke les arguments utilisés pour la création d'une thread POSIX.
		\item[int size{[NB\_TASK+1]}] stocke le nombre d'instructions effectives de chaque thread.
		\item[int pc{[NB\_TASK+1]}] sauvegarde la dernière instruction de chaque thread avant le changement de contexte.
		\item[int ct] utilisée pour repérer le numéro de thread courante.
		\item[int cs] utilisée pour stocker le numéro d'instruction où le prochain changement de contexte aura lieu.
	\end{description}


\section{Fonction main}	
	La figure \ref{exemple/main} représente la nouvelle fonction \texttt{main} dans $P_K^{seq}$ qui joue le rôle un ordonnanceur avec laquelle on simule l'entrelacement. Chaque itération de boucle simule un CC de $P_K^{seq}$ avec le nombre de CC maximum $K$. Chaque thread est simulée par une fonction $f_i^{seq}$ avec argument \texttt{arg[ct]} qui est utilisé pour la création de cette thread. L'ordre de l'exécution des threads correspond à un ordonnanceur \textit{Round-Robin}, ici de $0$ à \textit{NB\_TASK}. Pour chaque thread vivante, \texttt{main} efffectue les étapes suivantes :
%l, r, c, i or o; this letters stand for left, right, centre, inner and outer (the last two intended for two-sided documents). The second parameter is the width of the figure, in the example is 0.25 the width of the text.
	\begin{wrapfigure}[13]{r}{0.46\textwidth} 
	\vspace{-20pt}
	\begin{framed}
	\vspace{-10pt}
	{\footnotesize
		\begin{lstlisting}
int main(){
 for(r=1; r<=K; r++){
  ct=0;
  if(active[ct]){
   cs=nondet_uint();
   assume(cs>=pc[ct]&&cs<=size[ct]);
   main0();
   pc[ct]=cs;
   active[ct]=pc[ct]!=size[ct];
  }
  ...........
  ct=NB_TASK;
  if(active[ct]){
   ..........  
  }}}
		\end{lstlisting}		
	}		
	\vspace{-10pt}					
	\end{framed}
\vspace{-10pt}
	\caption{$P_K^{seq}$ : \texttt{main}}
	\label{exemple/main}
	\end{wrapfigure}
	\begin{enumerate}
		\item stocke dans \texttt{cs} un nombre indéterministe pour le prochain changement de contexte.
		\item assure que cette valeur est appropriée.
		\item appelle la fonction $f_i^{seq}$ pour exécuter les instructions entre \texttt{pc[ct]} et \texttt{cs} exclu.
		\item sauvegarde \texttt{cs} dans \texttt{pc[ct]}.
		\item met à jour \texttt{active[ct]} si la thread a fini d'exécuter toutes ses instructions.
	\end{enumerate}
	
	Car tous les sauts sont en avant, on peut choisir la valeur de \texttt{cs} aléatoire entre celle stockée dans \texttt{pc[ct]} et \texttt{size[ct]} (la valeur maximum) grâce à la fonction \texttt{nondet\_uint} et \texttt{assume}. Ce choix est la seule source indéterministe de notre approche.
		
%\newpage		
\section{Traduction des threads}	
\label{chapter/technique/traduction}
	Chaque fonction $f_i$ qui représente une thread dans le programme $P$ (voir la figure \ref{exemple/C}) est transformée à la fonction $f_i^{seq}$ correspondante dans $P_K^{seq}$ par ces règles ci-dessous :
	
\subsection*{Boucles}	
	Puisque l'on ne peut séquentialiser une boucle, on la déplie. Pour les boucles qui ont une borne précise, on peut les déplier selon cette borne. Pour les boucles infinies, on doit les déplier avec une borne arbitraire, c'est à dire que l'on doit fixer le nombre maximum de dépliages pour ces boucles ou pour chacune de ces boucles. Bien sur, il nous faut de montrer que ce choix de borne est correct, cela dépend de chaque boucle, il n'y aura pas de règle général. Voici un exemple simple de dépliage d'une boucle avec une borne connue :\\

\begin{figure}[!htb]\centering
   \begin{minipage}{0.2\textwidth}
     \vspace{-20pt}
	\begin{framed}
	\vspace{-10pt}
	{\footnotesize
		\begin{lstlisting}
x=0;
while(x<2){
 x++;
}
		\end{lstlisting}		
	}
	\vspace{-10pt}							
	\end{framed}
	\vspace{-20pt}
	%\caption{}
	\label{exemple/avantDepliage}
   \end{minipage}
   \begin {minipage}{0.2\textwidth}
   \vspace{-20pt}
   \centering
   devient
	\vspace{-20pt}     
	%\caption{}
     %\label{result/exempleAssum}
   \end{minipage}
   \begin {minipage}{0.2\textwidth}
   \vspace{-20pt}
    \begin{framed}
    \vspace{-10pt}
	{\footnotesize
		\begin{lstlisting}
x=0;
if(x<2){
 x++;
 if(x<2){
  x++;
  if(x<2){
   x++;  
  }}}
assert(!(x<2));
		\end{lstlisting}		
	}		
	\vspace{-10pt}					
	\end{framed}
	\vspace{-20pt}     
	%\caption{}
     \label{exemple/apresDepliage}
   \end{minipage}
\end{figure}
	
	Dans l'exemple ci-dessus, la boucle \texttt{while} est dépliée avec 3 itérations. Même si après le dépliage, le nouveau code contient plus d'itérations que le code original (ici, une itération de plus). Evidemment, la dernière itération est inaccessible, mais cette itération de plus n'altère pas le programme. Par contre, si la boucle est dépliée avec un nombre insuffisant d'itérations (par exemple une seule itération), la simulation devient fausse. Afin de résoudre ce type de problème, l'insertion de \texttt{assert} à la fin de cette boucle après le dépliage est nécessaire pour lever l'exception une fois que l'erreur se produit. Dans CBMC, un message d'attention \texttt{unwinding assertion} sera affiché si le nombre de dépliage est insuffisant et on le laisse déplier automatiquement.

	Cette façon n'est pas la seule solution de dépliage mais elle est plus facile à faire. On peut aussi utiliser l'instruction \texttt{goto}. Dans la fonction \texttt{INC0} (\ref{exemple/traduction}), la boucle \texttt{while} dans l'ancienne fonction \texttt{INC} (voir \ref{exemple/C}) a été dépliée avec 2 itérations en utilisant \texttt{goto}, car la variable \texttt{tmp} dans \texttt{INCO} est à \texttt{1}. Idem, pour \texttt{INC1}, \texttt{tmp} est initialisée à \texttt{5}, dans ce cas, la boucle \texttt{while} sera dépliée avec par exemple, 6 itérations ou plus.
	
	Le choix du nombre d'itération doit être suffisant grand pour que la simulation soit correcte, mais ne doit pas être trop grand sinon la complexité de la simulation devient très importante.

\begin{wrapfigure}[9]{l}{0.47\textwidth} 
	\vspace{-20pt}
	\begin{framed}
	\vspace{-10pt}
	{\footnotesize
		\begin{lstlisting}
while(flag[other]&&turn==other){
 //wait
}
		\end{lstlisting}		
	}			
	\vspace{-10pt}				
	\end{framed}
	\vspace{-10pt}
	devient
	\vspace{-10pt}
	\begin{framed}
	\vspace{-10pt}
	{\footnotesize
		\begin{lstlisting}
assume(!(flag[other]&&turn==other));
		\end{lstlisting}		
	}	
	\vspace{-10pt}						
	\end{framed}
\vspace{-10pt}
	\caption{Exemple tiré dans l'algorithme de Peterson}
	\label{exemple/exTirePeterson}
	\end{wrapfigure}
	
	Pourtant, pour les boucles qui simulent une attente active, si on donne une borne, le résultat de vérification sera altéré car si un thread qui n'a pas droit d'accéder à la ressource protégée sort de la boucle d'attente et l'utilise. Finalement, la simulation des entrelacements devient fausse. Pour éviter ce problème, on transforme ces boucles en utilisant \texttt{assume} (voir \ref{exemple/exTirePeterson}, un exemple retiré dans l'algorithme de Peterson). Grâce à \texttt{assume}, on peut négliger des exécutions infaisables sans altérer le résultat final, ni alourdir la vérification. 
	
	L'utilisation de \texttt{assume} est un compromis. Dans C, si \texttt{assume} est fausse, le programme s'arrête et on connaît l'erreur soit dans l'hypothèse, soit dans la programmation, etc. Par contre, dans la vérification, elle considère que \texttt{assume} est une exécution infaisable bien que l'on faisse des vraies erreurs dans l'hypothèse et au final, on a sauté ces erreurs. 
	
	Bref, il ne faut pas mélanger les \texttt{assume} propres au programme avec celles à la vérification. En réalité, il faut transformer les \texttt{assume} propres au programme en des fonctions par exemple pour que la vérification lève des exceptions si ces hypothèses sont fausses.

\subsection*{Variable locale}

	La simulation d'une fonction $f_i$ dans $P$ est réalisée par la répétition des appels de fonction $f_i^{seq}$, chaque invocation exécute un fragment de code accordé avec où le changement de contexte qui est calculé de façon indéterministe par le \texttt{main} a lieu. Pour ne pas recalculer inutilement et stocker des variables locales des threads à chaque fois on appelle $f_i^{seq}$, on les transforme en \texttt{static} (car chaque thread est simulée par une fonction différente). Mais une variable locale peut contenir une valeur non-déterministe si elle n'est pas initialisée, tandis que une variable \texttt{static} sont toujours initialisée à \texttt{0}, on doit initialiser une variable avec \texttt{nondet\_uint()}. Pourtant, en Ada, lorsque l'on utilise une variable non initialisée, le compilateur lève l'erreur \texttt{"bounded error"} à partir de Ada 95. Le choix de l'initialisation d'une variable \texttt{static} dans $P_K^{seq}$ par les fonctions \texttt{nondet\_*} reste à discuter.
	
\subsection*{Positionnement et Retour d'une thread}

	Lorsque $f_{ct}^{seq}$ est appellée la première fois, elle commence l'exécution depuis la première instruction. Les appels suivants, elle doit négliger systématiquement les instructions exécutées dans les changements de contexte(CC) précédents afin de reprendre l'endroit où le dernier CC a lieu. Ensuite, si elle atteint le point de prochain CC, elle continue jusqu'à la fin de la fonction $f_{ct}^{seq}$ sans exécuter les instructions après ce point. En utilisant \texttt{goto} en C, il existe deux mécanismes possibles : soit utilise un multiplexeur \texttt{switch} au début de la fonction, soit ajoute l'instruction \texttt{if} tout simplement avant chaque instruction de la fonction. La première semble intéligente et efficace mais le mécanisme derrière \texttt{switch} reste compliqué et l'impact sur la performance du système est important, tandis que la deuxième rend le code C un peu difficile à comprendre mais le résultat est satisfaisant. 
	%{\color{blue} \textsc{Keep it simple (Rester simple).}}
	
	On a utilisé les entiers naturels comme labels qui commence à \texttt{0} pour la première instruction et incrémente à chaque instruction selon l'ordre dans la fonction (voir la figure \ref{exemple/traduction}). Pour réduire l'indéterminisme du système, on insère les labels devant la première instruction et la dernière, et seulement les instructions "\textit{visibles}". Notez que l'on ne s'intéresse que sur les violations des assertions et des propriétés de vivacité et de sûreté.
	
	Pour chaque label \texttt{l} (sauf le dernier), on introduit la condition associée avec \texttt{goto} sous forme \texttt{if(pc[ct]>l||l>=cs) goto l+1} devant les instructions. Notez que \texttt{l+1} est évaluée au moment de traduction et est différente à un entier. Quand $f_{ct}^{seq}$ tente d'exécuter les instructions avant le dernier CC, ou après le prochain CC, la condition devient \textbf{TRUE} et le contrôlleur saute vers le prochain label sans exécuter l'instruction associée. Potentiellement, plusieurs sauts peuvent être réalisés pour atteindre la position lors du dernier CC et après l'exécution des instructions possibles, sans interrompue par le contrôlleur. Quand l'on atteint la position du prochain CC, avec ce contrôlleur, on fait plusieurs sauts pour aller jusqu'au dernier label, conventionnellement, qui est associé avec l'instruction \texttt{return} qui est associée avec aucune condition. Dans la figure \ref{exemple/traduction}, le macro \texttt{J} représente le contrôlleur.
	
\begin{wrapfigure}[36]{l}{0.55\textwidth} 
\vspace{-20pt}
\begin{framed}
\vspace{-10pt}
{\footnotesize
\begin{lstlisting}
//T = NB_TASK+1(nombre de threads )
bool active[T]={1,0,0,0,0};
int cs,ct,pc[T],size[T]={5,8,8,2,2};
int arg[T];
#define G(L) assume(cs>=L);
#define J(A,B) if(pc[ct]>A||A>=cs) goto B; 
pthread_mutex_t m; int c=0;
void INC0(void *b) {
0:J(0,1) static int tmp=(*b); 
1:J(1,2) pthread_mutex_lock(&m); 
2:J(2,3) if(c>0)
3:J(3,4)  c++; 
         else { G(4)
4:J(4,5)  c=0;
         if(!(tmp>0)) goto l1;
5:J(5,6)  c++; tmp--; 
         if(!(tmp>0)) goto l1;
6:J(6,7)  c++;tmp--; 
          assert(!(tmp>0));
          l1: G(7); 
         } G(7)
7:J(7,8) pthread_mutex_unlock(&m); 
8:       return;
}
void INC1(void *b) {...}
void DEC0(){
0:J(0,1) assume(c>0);
1:J(1,2) c--;
         assert(c>=0);
2:       return;
}
void DEC1() {...}
int main0() {
         static int x=1,y=5;
         static pthread inc0,inc1,dec0,dec1;
0:J(0,1) pthread_mutex_init(&m); 
1:J(1,2) pthread_create(&inc0,INC0,&x,1); 
2:J(2,3) pthread_create(&inc1,INC1,&y,2);
3:J(3,4) pthread_create(&dec0,DEC0,NULL,3); 
4:J(4,5) pthread_create(&dec1,DEC1,NULL,4); 
5:       return 0;
}
int main() {...voir dessus...}
\end{lstlisting}		
}		
\vspace{-10pt}					
\end{framed}
\vspace{-10pt}
\caption{$P_K^{seq}$ : traduction des threads}
\label{exemple/traduction}
\end{wrapfigure}	
	
	Par exemple, on considère le programme dans la figure \ref{exemple/traduction}, \texttt{INC0} est appellée (i.e, \texttt{ct=1}) avec \texttt{pc[1]=2} et \texttt{cs=6}. Au label \texttt{0}, la condition dans le macro \texttt{J} devient \textbf{TRUE}, l'instruction \texttt{goto} est exécutée et saute au label \texttt{1} et ainsi de suite. Au label \texttt{2}, la condition devient \textbf{FALSE}, l'instruction associée est exécutée, les instructions entre \texttt{2} et \texttt{5} sont exécutées également. Au label \texttt{6}, la condition devient \textbf{TRUE}, l'instruction au label \texttt{6} n'est pas exécutée. A partir du label \texttt{6}, le control fait plusieurs sauts jusqu'au dernier label de l'instruction \texttt{return} sans exécuter d'autres instructions.

\subsection*{Gestion des branchements}
	
	On considère l'exemple dans la figure \ref{exemple/traduction}, l'instruction \texttt{if} dans \texttt{INC0} avec \texttt{pc[1]=2} et \texttt{cs=3}, la seule instruction faisable dans ce tour est celle au label \texttt{2} : \texttt{if(c>0)}. Pourtant, si \texttt{c$\leq$0} alors l'exécution saute directement au label \texttt{4}, ici, la condition dans le macro \texttt{J(4,5)} rend \textbf{TRUE} et l'exécution se termine par atteindre \texttt{return}. Retourne dans le \texttt{main}, \texttt{pc[1]=cs} et \texttt{cs=3}, lors du prochain tour, l'instruction au label \texttt{3} sera exécutée tandis que cette instruction est inaccessible.
	
	Car \texttt{cs} est indéterministe, il nous faut éliminer les chemins inaccessibles. Pour le faire, on introduit un simple macro \texttt{G(L) assume(cs>=L)} (avec \texttt{L} le prochain label) juste derrière l'instruction \texttt{else}, \texttt{if} et toutes les autres instructions de branchement qui sont propre au programme.
	
	Cette solution élimine les chemins inaccessibles, comme l'exemple ci-dessus, l'exécution est correctement abandonnée (mais la simulation continue) sans altérer le résultat final. Il existe une autre solution, on implémente un mécanisme d'affectation à la variable \texttt{pc[ct]} un choix non déterministe à chaque position possible, mais elle augmente la complexité du programme, alourdit la simulation et les outils de vérification (ici, CBMC). Finalement, on élimine juste les chemins inaccessibles. 
	%{\color{blue} \textsc{Keep it simple (Rester simple).}}

\subsection*{Simulation des routines de Pthreads}
	Pour que le programme $P_K^{seq}$ marche avec \texttt{Pthread}, on implémente une nouvelle version simple de cette routine en C (voir la figure \ref{exemple/pthread}).

\begin{wrapfigure}[23]{l}{0.57\textwidth} 
\vspace{-20pt}
\begin{framed}
\vspace{-10pt}
{\footnotesize
\begin{lstlisting}
typedef int pthread;
typedef int pthread_mutex;
void pthread_create(pthread *t, 
 void *(*f)(void*),void *argument,int id){
  active[id] = TRUE;
  arg[id]=argument;
  *t=id;
}
void pthread_join(pthread t)
 {assume(pc[t]==size[t]);}
void pthread_mutex_init(pthread_mutex *m)
 {*m=FREE;}
void pthread_mutex_destroy(pthread_mutex *m)
 {*m=DESTROY;}
void pthread_mutex_lock(pthread_mutex *m,
 int id){
 assert(*m!=DESTROY);
 assume(*m==FREE);
 *m=id;
}
void pthread_mutex_unlock(pthread_mutex *m,
 int id){
  assert(*m==id);
  *m=FREE;
}
\end{lstlisting}		
}			
\vspace{-10pt}				
\end{framed}
\vspace{-10pt}
\caption{$P_K^{seq}$ : nouvelle version de \texttt{pthread}}
\label{exemple/pthread}
\end{wrapfigure}		
	
	Dans la fonction \texttt{pthread\_create}, on met dans la variable \texttt{active[id]} à \textbf{TRUE} et stocke l'argument de cette thread dans la variable \texttt{arg[id]}. Notez que le \texttt{main} appelle les threads explicitement et \texttt{id} sert à identifier les threads.
	
	En réalité, quand la fonction \texttt{pthread\_join(t)} est appelée, l'appelant est bloqué jusqu'à la thread \texttt{t} passée en paramètre se termine son exécution. Dans la simulation, une thread termine son exécution lorsqu'il atteint sa dernière instruction \texttt{return}, puisque l'on connaît le nombre d'instructions dans une thread, en ajoutant un macro \texttt{assume(pc[t]==size[t])} (qui retourne vraie si la thread \texttt{t} a fini son exécution, fausse sinon), on peut se contenter d'abandonner les chemins d'exécutions infaisables.
	
	On tranforme \texttt{phtread\_mutex\_t} en un entier \texttt{pthread\_mutex} et on définie deux constants \texttt{FREE} et \texttt{DESTROY} qui ont deux valeurs distinguées avec les identifiants des threads. Lorsque l'on le verrouille, on assume qu'il est libre et met l'identifiant de thread appelante. Par contre, pour libérer, il faut utiliser \texttt{assert} pour assurer que seulement la thread qui le verrouille ne peut que le déverrouiller.
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	