\chapter{Introduction}
%\addcontentsline{toc}{section}{Introduction} % Ajout dans la table des matières
\label{chapter/introduction}
	Des erreurs lors de l'exécution de programmes concurrents sont notoirement difficiles à reproduire, à localiser et à corriger à cause de l'explosion de l'espace d'état du programme concurrent (contre celui séquentiel). Cela représente un grand défi pour la phase de test, de vérification et pour la création des outils de vérification automatique, qui sont tous nécessaires pour désambiguïser un vrai problème du programme.
	
	Une autre question est de détecter les erreurs possibles tôt, avec moins de ressources et dans un temps raisonnable à partir du programme concurrent intrinsèque ou contexte séquentiel quelque soit l'erreur dans le code du programme ou de la bibliothèque, etc.

	La vérification automatique de programmes est un domaine de recherche important et très actif. Il pose de nombreux défis scientifiques aussi bien sur le plan théorique que sur le plan pratique. Son objectif est de vérifier des propriétés, qui sont souvent écrites par un langage formel, de sûreté, de validité d'un programme,... de façon algorithmique sans savoir comment il fonctionne.
	
	Il existe des méthodes pour vérifier un programme concurrent :  soit on s'intéresse à la trace d'exécution, soit on séquentialise le programme concurrent pour que l'on puisse appliquer le \textit{model checking}. La première méthodes semble simple mais n'est pas efficace car on doit observer également le comportement du système qui peut altérer le résultat de la vérification. 
	
\section{Ada - un langage de programmation fiable}
	Ada \cite{ada-about} est un langage de programmation moderne conçu pour des grandes applications à long terme et des systèmes embarqués en particulier où la fiabilité et l'efficacité sont essentielles.
	
	Ada est utilisé dans de nombreux domaines comme la commerce, l'avionique militaire, le contrôle aérien, le système ferroviaire, les appareils médicaux, etc. Ce sont en effet des domaines qui ont des besoins d'sûreté importante, de sécurité et Ada est le langage le plus adapté à ce contexte en raison de son typage fort et de nombreuses autres caractéristiques de sécurité intrinsèques, qui rendent les programmes plus sûrs et plus facilement maintenables.

De plus dans le cadre de l'analyse de programmes, l'intérêt du langage Ada est de définir précisément la sémantique des comportements, en particulier dans le cadre du \textbf{"multitasking"} qui s'appuie sur des mécanismes précis de communication et de synchronisation inter tâches comme l'objet protégé, ou l'ancien mécanisme du rendez-vous.
	
	Dans le cadre de notre stage, on aborde seulement l'objet protégé et on ne traite que les appels de procédures ou d'entrée ; la prise en charge des fonctions
	n'apporte pas de changements fondamentaux, ni en terme de performance ni en terme de méthodologie.
	
\subsection*{Objet protégé}
\label{section/objet-protege}
	Inventé par un britanique Sir Charles Antony Richard Hoare en 1974, un \textit{objet protégé} (ou \textit{moniteur}) garantie l'accès atomique en encapsulant les sections critiques de manière à ce que les tâches n'y aient plus accès. Les données brutes ne sont plus accessibles directement, par contre, on fournit des méthodes pour les manipuler mais toujours pour éviter des accès multiples. Ada gère lui-même les mises en attente ou les réveils de tâches.

	\begin{figure}[!htbp]
		\centering
		\includegraphics[width = 10cm]{../../data/images/doc/ada-protected-object.png}
		\caption{Représentation d'un objet protégé \cite{ada-eggshell}.}
		\label{images/eggshell}
	\end{figure}
	%\newpage

\label{section/objet-protege/function}
	Pour mémoire, l'objet protégé Ada propose trois façons d'y accéder : \textit{function}, \textit{procedure} et \textit{entry}. Dans une \textit{fonction} l'appelant ne peut modifier les variables internes dans l'objet protégé, donc plusieurs fonctions du même objet peuvent être exécutées simultanément. Au contraire des fonctions, les accès par  \textit{procédure} doivent être atomiques car l'appelant peut modifier l'objet protégé. La dernière façon d'accès par \textit{entry}  est similaire à la \textit{procédure} mais elle est associée avec une barrière.
		
	La figure \ref{images/eggshell} (page \pageref{images/eggshell}) représente la sémantique d'un objet protégé qui n'a pas de fonction. Une tâche est représentée par une boule remplie de couleur brune. Un grand cercle associé avec un verrou \textit{lock} et un grand rectangle (associé avec l'objet \textit{state} et les opérations - représentées par des moyens rectangles) représentent l'objet protégé. Les entrées sont représentées par les petits rectangles - remplis de noir si leur état est fermé, de blanc sinon.
	
	Dans cet exemple, on voit une seule tâche qui est en train d'exécuter une opération en exclusion mutuelle, d'autres qui sont déjà entrées dans l'objet et en attente sur une entrée, sont plus prioritaires que les tâches non encore entré dans l'objet et qui sont en attente du verrou de delui-ci.
	
	En effet, pour accéder à l'objet protégé, une tâche doit entrer dans la coquille en passant par le verrou \textit{lock} ; ainsi une tâche qui essaie d'acquérir le verrou alors qu'une procédure ou une entrée est en cours d'exécution sera bloquée. 
	
	L'évaluation de la barrière \cite{ada-protected-type} associée à une entrée (représentée par le petit rectangle) ne peut se faire qu'au sein de la coquille de l'objet. Lorsque une \textit{entry} est appelée la première fois, la barrière associée avec cette entrée est évaluée, si elle est \textbf{false(fausse)}, l'appelant se met en attente devant cette barrière et rend le verrou. Si la première évaluation de barrière rend \textbf{true(vraie)}, l'appellant peut accéder et exécuter le corps de l'entrée correspondante. Toutes les barrières de cet objet protégé seront réévaluées à la fin de l'exécution d'une procédure ou d'une entrée. En plus, une tâche en attente d'accéder au objet protégé dans la coquille qui a sa barrière évaluée à \textbf{true} est plus prioritaire que les autres tâches de même priorité.
		
	La conséquence importante garantie est : \textit{si l'état d'un objet protégé change à nouveau et il existe une tâche qui attend ce nouvel état, alors cette tâche aura l'accès à la ressource et sera garantie que l'état de cette ressource est exactement la même lorsqu'elle a été relâchée}.
	
	Si la barrière correspondante avec l'appel d'entrée est \textbf{ouverte}, donc la tâche en attente sera choisie et exécutée dans la coquille, sinon cet appel restera toujours  en attente dans la file devant la barrière de cette entrée. Ce processus continue jusqu'à qu'il n'existe plus de tâche en attente devant une barrière \textbf{ouverte}. S'il n'y a aucune  barrière \textbf{ouverte} avec une tâche en attente sur cette entrée, l'objet protégé devient accessible pour les autres, les demandes d'accès depuis la file d'attente externe seront traitées selon la priorité des tâches.
	
	La réévaluation des barrières dépend du résultat de la procédure ou de l'appel d'entrée qui vient d'avoir lieu sur le même objet protégé. Par conséquent, avant qu'une tâche qui a peut-être changé l'état d'un objet protégé sorte de la coquille, un des appels d'entrée \textit{entry} éligibles en attente doit être exécutés. Seulement lorsque toutes les barrières de cet appel d'entrée devient \textbf{false}, la tâche peut sortir de la coquille  (cela dépend la façon d'implémenter d'objet protégé - voir chapitre \ref{chapter/adaversC}). Ceci assure que toutes les \textit{entry} qui deviennent éligibles par un changement de l'état de l'objet seront exécutées avant que les autres opérations soient initiées.
	
	Ada nous interdit de faire un appel potentiellement bloquant (par exemple par une instruction \texttt{select} un appel d'entrée, la création des tâches, etc.) dans l'objet protégé \cite{ada-eggshell}, %par exemple, une tâche qui fait un appel sur un objet protégé et cet appel qui exécute un autre appel d'entrée sur un autre objet protégé. %http://www.iuma.ulpgc.es/users/jmiranda/gnat-rts/node25.htm
	 Donc, lorsque l'on veut réaliser un appel sur une autre entrée, c'est impossible. Toutefois, cette action n'est pas interdite si l'on fait dans le même objet protégé grâce à l'instruction \textbf{requeue} (ici, on ne parle que de l'objet protégé). Notez que l'on ne peut faire un \textbf{requeue} sur un autre objet protégé et cette instruction est utilisable seulement entre les entrées, ni fonction, ni procédure. Lorsque l'on \textbf{requeue} sur une entrée, cette action sera traitée comme une nouvelle entrée. Le traitement est un peu différent selon la façon d'implémentation mais on peut expliquer de façon générale comme : si la barrière est ouverte, cet appel sera traité imédiatement, sinon la tâche appelante se met en attente. Pour l'information, dans GNAT, si l'on \textbf{requeue} sur la même entrée, cet appel sera mis dans la file d'attente devant cette entrée, car de toute façon, cette file sera utilisée avant que cette tâche sorte. Par contre, si le \textbf{requeue} porte sur une autre entrée, le traitement reste normal.
	
	De plus, avec l'instruction facultative \textbf{with abort}, lorsque l'on fait un \textbf{requeue} \textit{appel} \textbf{with abort}, cet appel peut être annulé. L'annulation ne peut être accomplie que si l'entrée appelée par \textbf{requeue} n'a pas été effectuée; si elle ne peut être exécutée immédiatement, elle sera annulée et l'appelant continue son exécution. Notez que l'annulation est réalisée dans l'objet protégé, l'objet protégé reste toujours à disposition de la tâche appelante.
	
\section{Vérification de modèles (Model checking)}
	La \textbf{vérification de modèles} (en anglais \textit{model checking}) désigne une famille de techniques de vérification automatique des systèmes dynamiques (souvent d'origine \textit{informatique} ou \textit{électronique}). Il s'agit d'examiner algorithmiquement si un modèle donné, le systèmes lui-même ou une abstraction du système, satisfait une spécification, souvent formulée en termes de logique temporelle (développé indépendamment  par Clarke et Emerson \cite{model-checking-about1}, et par Queille et Sifakis \cite{model-checking-about2} dans le début des années 80) .
	 
	On peut distinguer deux aspects de la recherche en \textit{model checking}:
	\begin{itemize}
		\item Une recherche plutôt théorique où l'on va chercher à démontrer qu'une certaine classe de propriétés, ou une certaine logique, est décidable, ou que sa décision appartient à une certaine classe de complexité ;
		\item Une recherche plus appliquée qui va se concentrer sur la mise au point d'algorithmes efficaces sur des cas intéressants en pratique, de leur implémentation et de leur appliquation à des problèmes réels, voir à des transferts industriels.
	\end{itemize}	 
	
	Sur le plan pratique, la vérification de modèles passe plus facilement dans l'industrie car il s'agit avant tout, pour l'ingénieur qui l'utilise,  d'une démarche "presse bouton" comparée aux démarches basées sur la preuve qui demandent une plus grande formation et implication des équipes de développement et de maintenance.
	
	Il existe plusieurs stratégies de mise en oeuvre de la vérification de modèle : méthodes explicites (parcours de modèle), méthodes symboliques (diagrammes de décision binaire - BDD), ou résolution SAT-SMT (voir ci-dessous). Nous nous instéressons ici à cette dernière stratégie car elle a donné ces derniers temps d'excellents résultats dans certains contextes (comme la vérification de circuit). Par ailleurs, il est possible, au lieu de considérer l'ensemble des traces infinies d'exécution du sytème, de se limiter à des traces finies, de longueur bornée ; on parle alors de "\textit{Bounded Model Checking}".
	 
\subsection*{Problème SAT}
\label{probleme-sat}
	%\addcontentsline{toc}{subsection}{Problème SAT} % Ajout dans la table des matières
	
	En informatique théorique, un \textbf{problème SAT} (satisfiable) est un problème de décision, qui, étant donné une formule de logique propositionnelle, détermine s'il existe une assignation des variables propositionnelles qui rend la formule vraie.
	
	Ce problème est très important en théorie de la complexité. Il a été mis en lumière par le théorème de Cook \cite{sat-about}, qui est à la base de la théorie de la \textit{NP-complétude}. 
	%- trouver une combinaison de $a$, $b$ et $c$ pour laquelle, dans la formule $a (b+c) (a+b+c)$, chaque terme est vrai - ou de la questio $P = NP$. 
	La notion de problème SAT a aussi de nombreuses applications notamment en planification classique, \textit{model checking}, diagnostic et jusqu'au configurateur d'un PC ou de son système d'exploitation: on se ramène à des formules propositionnelles et on utilise un solveur SAT.
	
	Les formules de la logique propositionnelle sont construites à partir de variables propositionnelles et des connecteurs booléens \textit{et, ou, non}. Une formule est \textbf{satisfaisable} (on dit aussi \textbf{satifiable}) s'il existe une assignation des variables propositionnelles qui rend la formule logiquement vraie. Par exemple:
	\begin{itemize}
		\item La formule $(p \wedge q) \vee \neg p$ est satifaisable car si $p$ prend la valeur fausse, la formule est évaluée à vraie.
		\item La formule $(p \wedge \neg p)$ n'est pas satisfaisable car aucune valeur de $p$ ne peut rendre la formule vraie.
	\end{itemize}
	
\subsection*{Bounded model checking (BMC)}
	Le \textbf{Bounded model checking (BMC)} est une technique (partielle) de \textit{model checking}. La principale différence est que le \textit{model checking} s'applique à des exécutions de longueur infinie alors que le BMC s'applique à celles de longueur finie. 
	En effet, la plupart des exécutions infinies bouclent à un certain point et on peut alors borner leur longueur qu'il est nécessaire de vérifier. Le \textit{bounded model checking} s'appuie sur ce principe pour transformer le problème de \textit{model checking} en un problèmes SAT.
	
	 En effet, l'existence d'une trace vérifiant une certaine propriété est équivalente à la satisfiabilité d'une certaine formule booléenne. Si un état du système est décrit par un k-uplets booléens $\vec{x}$ , et que l'on s'intéresse aux traces de longueur bornée par un certain $n$, on se ramène au problème de la satisfiabilité d'une formule propositionnelle (problème SAT, page \pageref{probleme-sat}). 
	 Plus précisément, si une formule $I$ identifie les états initiaux du système, une formule $F$ identifie les états dont on veut tester l'accessibilité, et si $T$ est la relation de transition, alors vérifier l'accessibilité des états identifiés par $F$ à partir des états initiaux définis par $I$ à travers $T$ est équivalent à tester
	  la satisfiabilité de la formule booléenne $I(\vec{x}_1) \wedge T(\vec{x}_1, \vec{x}_2) \wedge ... \wedge T(\vec{x}_{n-1}, \vec{x}_n)\wedge F(\vec{x}_n)$ où $\vec{x}_i$ sont des propositions atomiques qui représentent l'état à l'étape $i$ de l'exécution du système. 
	  
	  Il existe divers logiciels, appelés \textit{solveurs SAT}, qui peuvent décider, \textit{efficacement en pratique}, le problème SAT. De plus, ces logiciels fournissent habituellement un exemple de valuation satisfaisant la formule en cas de succès. Certains peuvent produire des éléments d'une preuve de non satisfiabilité en cas d'échec.
	
	Une évolution récente est l'ajout, en sus de variables booléennes, de variables entières ou réelles. Les formules atomiques ne sont alors plus seulement les variables booléennes, mais des prédicats atomiques sur ces variables entières ou réelles, ou plus généralement des prédicats pris dans une théorie (par exemple $x < 5$, $T[x] + T[x+4] < T[x+5]$ etc.). On parle alors de \textbf{Satisfiabilité modulo théories (SMT)}.
	
\subsection*{Satisfiabilité modulo théories (SMT)}
	%\addcontentsline{toc}{subsection}{SMT} % Ajout dans la table des matières
	
	La \textbf{satisfiabilité modulo théories (SMT)} est un problème de décision pour des formules logiques, par rapport à des théories sous-jacentes exprimées dans la logique classique du premier ordre (qui contient la notion de \textit{prédicat} - fonction transformant un ensemble de $n$ paramètres de types $T_1, ... T_n$ en une valeur booléenne et de \textit{quantification} - l'ensemble des valeurs d'une variable pour lesquelles il faut évaluer une formule $\exists$ et $\forall$) avec égalité.  Des exemples de théories incluent la théorie des nombres réels, la théorie de l'arithmétique linéaire, des théories de diverses structures de données comme les listes, les tableaux ou les vecteurs de bits, ainsi que des combinaisons de celles-ci.
	
	Formellement, une instance de SMT est une formule du premier ordre sans quantificateur. Le problème SMT est de déterminer si une telle formule est satisfiable. En d'autre mot, imaginons une instance de SAT dans laquelle les variables booléennes sont remplacées par des prédicats (les fonctions qui transforment les paramètres de type quelconque en une valeur booléenne).
	
	Les solveurs SMT fonctionnent autour de deux cœurs principaux: un solveur SAT et une ou plusieurs procédures de décision de la théorie. Une instance de SMT, même si elle est satisfiable lorsqu'elle est vue comme une instance de SAT (obtenue en remplaçant les prédicats par des variables booléennes), n'est pas forcément satisfiable modulo une certaine théorie. Il s'agit alors de demander des modèles SAT au solveur SAT puis de vérifier leur cohérence par les procédures de décision de la théorie. Le fonctionnement général d'un solveur SMT est décrit de la manière suivante pour une instance de SMT $F$ donnée :
	
	\begin{itemize}
		\item[I.] Remplacer les prédicats et contraintes de $F$ par des variables booléennes.
		\item[II.] Demander au solveur SAT un modèle de satisfiabilité $M$.
			\begin{itemize}
				\item[1.] S'il n'existe pas de modèle SAT, alors la formule $F$ n'est pas satisfiable, donc pas satisfiable modulo la théorie.
				\item[2.] Sinon,
					\begin{itemize}
						\item[a.] Remplacer dans $M$ les variables par les prédicats de l'étape I.
						\item[b.] Vérifier la cohérence du modèle par la procédure de décision de la théorie.
							\begin{itemize}							
								\item Si le modèle est cohérent, alors la formule $F$ est satisfiable modulo la théorie et le modèle est explicité.
								\item Sinon, recommencer à l'étape I avec la formule $F \wedge \neg M$.
							\end{itemize}
					\end{itemize}
			\end{itemize}
	\end{itemize}
	
	L'architecture des solveurs SMT est donc divisée comme suit: le solveur SAT basé sur l'algorithme DPLL (Davis-Putnam-Logemann-Loveland) résout la partie booléenne du problème et interagit avec le solveur de la théorie pour progager ses solutions. Ce dernier vérifie la satisfiabilité des conjonctions de prédicats de la théorie. Pour des raisons d'efficacité, on souhaite généralement que le solveur de la théorie participe à la propagation et à l'analyse des conflits.	

	\textbf{Satisfiability Modulo Theories Library (SMT-LIB)} (la bibliothèque de satisfiabilité modulo théries) a pour but de faciliter la recherche et le développement des solveurs SMT. SMT-LIB propose une bibliothèque unique pour l'évaluation et pour les compétitions entre plusieurs solveurs SMT qui connecte les développeurs, les rechercheurs, etc. Elle est supportée par un grand nombre d'équipe de recherche et financiée par des industries et par des différents gouvernements dans le monde.
	
	
	
	
	
	
	
	
	
	
	
	
	