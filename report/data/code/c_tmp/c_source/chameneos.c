#include <stdio.h>
#include <assert.h>

#define TRUE 1
#define FALSE 0
#define FREE -1

#define NB_TASK 6 //without main task
#define NB_ENTRY 2
#define NB_PROCEDURE 0
#define NB_FUNCTION 0

/*-------------------------------------------------------------*/
int active[NB_TASK+1] = {TRUE, FALSE, FALSE};
int cs, ct;
int pc[NB_TASK+1];
int size[NB_TASK+1];

void create(int t, int id){
    active[id] = TRUE;
    t = id;
}
/*-------------------------------------------------------------*/
int tabStatus[NB_TASK+1] = {TRUE, TRUE, TRUE, TRUE};
int tabWaiting[NB_TASK+1] = {0, 0, 0, 0};
int tabBarrier[NB_ENTRY+1] = {FALSE, FALSE, FALSE};

int currentTask=1;

int tabEntryCount[NB_ENTRY+1] = {0,0,0};
int taskInWait = 0;

int externalLock = FREE;

void lock(int id){
    __CPROVER_assume(externalLock==FREE);
    externalLock=id;
}

void transfert(int id, int newId){
    __CPROVER_assert(externalLock==id, "OKexternalLock!=id");
    externalLock=newId;
}

void unlock(int id){
    __CPROVER_assert(externalLock==id, "OKexternalLock!=id");
    externalLock=FREE;
}
/*---------------------------------------------------------------------------*/
#define BLUE 1
#define RED 2
#define YELLOW 3

typedef int colour;

int firstCall = TRUE;
colour aColour;
colour bColour;

int x1[NB_TASK+1];
colour c1[NB_TASK+1];
colour cOther1[NB_TASK+1];

colour complementary_colour(colour c1, colour c2){
    if (c1==c2){
        return c1;
    }else{
        return RED; //3-c1-c2;
    }
}

colour myColour[NB_TASK+1];
colour otherColour[NB_TASK+1];

/*---------------------------------------------------------------------------*/

void evaluate_barrier(int index){
    if (index == 1){ //Cooperate
        tabBarrier[index] = TRUE;
    }else if (index == 2){ //Waiting
        tabBarrier[index] = firstCall;
    }
}

void reevaluate_barrier(){
    tabBarrier[1] = TRUE; //Cooperate
    tabBarrier[2] = firstCall; //Waiting
}

void add_entry_queue(int taskId, int index){
    tabEntryCount[index] = tabEntryCount[index]+1;
    taskInWait = taskInWait + 1;
    __CPROVER_assert(taskInWait<NB_TASK, "OKdeadlock");
    
    tabWaiting[taskId] = index;
    tabStatus[taskId] = FALSE;
}

void remove_entry_queue(int taskId, int index){
    tabEntryCount[index] = tabEntryCount[index]-1;
    taskInWait = taskInWait - 1;
    
    tabWaiting[taskId] = 0;
}

int current_task_eligible(){ //return taskId
    int j;
    for (j=1; j<=NB_TASK;j++){
        if (tabWaiting[currentTask]!=0&&tabBarrier[tabWaiting[currentTask]]){
            return currentTask;
        }
        currentTask = (currentTask % NB_TASK)+1;
    }
    return 0;
}

void run_entry_body(int taskId, int index);

void requeue(int taskId, int index){ //requeue to another or same entry queue
    evaluate_barrier(index);
    
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        remove_entry_queue(taskId, index);
    }
    run_entry_body(taskId,index);
}

void run_entry_body(int taskId, int index){
    if (index == 1) { //Request
        if (firstCall){
            aColour=c1[taskId];
            firstCall=FALSE;
            requeue(taskId, 2); //requeue Assign
        }else{
            bColour=c1[taskId];
            cOther1[taskId]=aColour;
            firstCall=TRUE;
        }
    }else if (index == 2){ //Assign
        cOther1[taskId]=bColour;
    }
}

int check_and_run_any_entry(){
    reevaluate_barrier();
    int taskIdEligible = current_task_eligible();
    if (taskIdEligible==0){
        return FREE;
    }
    tabStatus[taskIdEligible] = TRUE;
    return taskIdEligible;
}

void run_function_call(taskId, index){
}

void run_procedure_call(taskId, index){
  /*  int taskIdEligible;
    if (index == 1){
        stock = stock + y[taskId];
    }
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
   */
}

void run_entry_call(int taskId, int index){
    int taskIdEligible;
    evaluate_barrier(index);
    
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        remove_entry_queue(taskId, index);
    }
    run_entry_body(taskId,index);
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
}

void protected_object_call(int taskId, int appel, int index){
    lock(taskId);
    
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        run_entry_call(taskId, index);
    }
}

//yellow, blue, red, blue, yellow, blue

void t_one(){
    static int taskId=1;
    static int taskIdEligible;
    static int index;
    
    myColour[taskId]=YELLOW;
    otherColour[taskId]=0;
    
    lock(taskId);
    evaluate_barrier(index);
    
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        remove_entry_queue(taskId, index);
    }
    //run_entry_body(taskId,index);
    index=1;
    c1[taskId]=myColour[taskId];
    
    
    if (index == 1) { //Request
        if (firstCall){
            aColour=c1[taskId];
            firstCall=FALSE;
            
            //requeue(taskId, 2); //requeue Assign
            index = 2;
            evaluate_barrier(index);
            
            if (tabBarrier[index]==FALSE){
                add_entry_queue(taskId, index);
                unlock(taskId);
                __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                remove_entry_queue(taskId, index);
            }
            //run_entry_body(taskId,index);
            cOther1[taskId]=bColour;
        }else{
            bColour=c1[taskId];
            cOther1[taskId]=aColour;
            firstCall=TRUE;
        }
    }
    
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);

    otherColour[taskId]=cOther1[taskId];
    
    myColour[taskId]=complementary_colour(myColour[taskId], otherColour[taskId]);
}











