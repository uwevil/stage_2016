#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#define NB_TASK 4 //without task main
#define NB_ENTRY 2
#define NB_PROCEDURE 0
#define NB_FUNCTION 0

#define TRUE 1
#define FALSE 0

#define FREE -1
#define DESTROY -2

pthread_mutex_t m;

volatile int flag[NB_TASK+1] =
    {-1, -1, -1, -1, -1}; //n level
volatile int turn[NB_TASK] =
    {-1, -1, -1, -1}; // n-1 last
static int x = 0;

void t_one(int *id){
    int taskId = *id;
    int i, j, wait, tmp;
    for(i = 1; i < NB_TASK; i++){
        flag[taskId] = i;
        turn[i] = taskId;
        do {
            sleep(1);
            wait = FALSE;
            for(j=1;j<NB_TASK+1
                &&j!=taskId;j++){
                wait=wait||flag[j]>=i;
            }
        }while(wait&&turn[i]==taskId);
    }
    printf("id = %d in SC\n", taskId);
    if (taskId % 2 != 0){
        tmp = x;
        sleep(1);
        tmp = tmp + 1;
        sleep(1);
        x = tmp;
        sleep(1);
    }else{
        tmp = x;
        sleep(1);
        tmp = tmp - 1;
        sleep(1);
        x = tmp;
        sleep(1);
    }
    printf("tmp %d = %d out SC\n",taskId,x);
    flag[taskId] = -1;
}

int main(void){
    pthread_mutex_init(&m, NULL);
    pthread_t t1, t2, t3, t4;
    int arg[NB_TASK+1] = {0, 1, 2,3,4};
    pthread_create(&t1, NULL,
            (void *(*)(void *))t_one, &arg[1]);
    pthread_create(&t2, NULL,
            (void *(*)(void *))t_one, &arg[2]);
    pthread_create(&t3, NULL,
            (void *(*)(void *))t_one, &arg[3]);
    pthread_create(&t4, NULL,
            (void *(*)(void *))t_one, &arg[4]);
    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    pthread_join(t3, NULL);
    pthread_join(t4, NULL);
    printf("x = %d\n", x);
    return 0;
}













