with Common; use Common;
package Chopstick_Pkg is
   type Boolean Array is array( Phil_Range )
     of Boolean;
   protected Chopstick Sentry is
      entry Get Pair( Phil_Range ); -- Entry family
      procedure Release Pair( Id : in Phil_Range );
      -- Philosophers(J) calls Get_Pair(J) and
      -- Release_Pair(J)
   private
      Available : Boolean_Array := (others => True);
      -- Philosophers(J) needs chopsticks J and J+l
   end Chopstick_Sentry;
end Chopstick_Pkg;
