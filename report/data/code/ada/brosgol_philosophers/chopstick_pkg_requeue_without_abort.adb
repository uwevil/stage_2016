package body Chopstick_Pkg_requeue_without_abort is
   protected body Chopstick_Sentry is
      entry Get_Pair( Id : in Phil_Range ) when True is
      begin
         if Available(Id) and Available(Id+1) then
            Available(Id) := False;
            Available(Id+1) := False;
            return;
         else
            requeue Please_Get_Pair;
         end if;
      end Get_Pair;
      entry Please_Get_Pair( Id : in Phil_Range )
        when Flush_Count > 0 is
      begin
         Flush_Count := Flush_Count - 1;
         if Available(Id) and Available(Id+1) then
            Available(Id) := False;
            Available(Id+1) := False;
            return;
         else
            requeue Please_Get_Pair; -- m
         end if;
      end Please_Get_Pair;
      procedure Release_Pair( Id : in Phil_Range ) is
      begin
         Available(Id) := True;
         Available(Id+1) := True;
         Flush_Count := Please_Get_Pair'Count;
      end Release_Pair;
   end Chopstick_Sentry;
end Chopstick_Pkg_requeue_without_abort;
