with Common, Philosopher_Pkg, User_Pkg;
use Common;
procedure Dining_Philosophers is
begin
   User_Pkg.User.Startup;
   for J in Phil_Range loop
      Philosopher_Pkg.Philosophers(J).Identify(J);
   end loop;
end Dining_Philosophers;
