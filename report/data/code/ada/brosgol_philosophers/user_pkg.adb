with Philosopher_Pkg;
with Ada.Text_IO; use Ada.Text_IO;
package body User_Pkg is
   task body User is
      D : Duration;
      Line : String(1..80);
      Last : Natural;
   begin
      accept Startup do
         Put( "Duration of simulation in seconds: " );
         Get_Line( Line, Last );
         D := Duration'Value(Line(1..Last) );
      end Startup;
      delay D;
      Put_Line("Meal is over;" &
                 " philosophers shutting down...");
      Philosopher_Pkg.Shutdown;
   end User;
end User_Pkg;
