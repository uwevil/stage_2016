with Common;
use Common;
package Chopstick_Pkg_requeue_without_abort is
   type Boolean_Array is array( Phil_Range ) of Boolean;
   protected Chopstick_Sentry is
      entry Get_Pair( Id : in Phil_Range );
      procedure Release_Pair( Id : in Phil_Range );
   private
      Flush_Count : Natural := 0;
      Available : Boolean_Array := (others => True);
      entry Please_Get_Pair( Id : in Phil_Range );
   end Chopstick_Sentry;
end Chopstick_Pkg_requeue_without_abort;
