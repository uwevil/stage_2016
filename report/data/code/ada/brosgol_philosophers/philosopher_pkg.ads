with Common;
use Common;
package Philosopher_Pkg is
   task type Philosopher is
      entry Identify( Id : in Phil_Range );
   end Philosopher;
   Philosophers : array( Phil_Range ) of Philosopher;
   procedure Shutdown;
end Philosopher_Pkg;
