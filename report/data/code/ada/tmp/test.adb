with Ada.Text_IO;
use Ada.Text_IO;
with Protected_Object;
use Protected_Object;

procedure test is
   -------------------------------DECLARATION-------------------------
   type abc is mod 10;
   m : abc := 3;

   -------------------------------MAIN-------------------------
begin

   for i in 1..10 loop
      m := m + 1;
      Put(m);
      New_Line;
   end loop;

   put(Boolean'Image(is_lock));
   New_Line;
   add_external_queue(1);
   currentPositionExternalQueue := (currentPositionExternalQueue mod 2) + 1;
   add_external_queue(2);
   currentPositionExternalQueue := (currentPositionExternalQueue mod 2) + 1;
   add_external_queue(3);

   put(Boolean'Image(is_lock));
   New_Line;

   for i in 1..2 loop
      put(Natural'Image(tabExternalQueue(i)));
      New_Line;
   end loop;

  -- isLock := True;
  -- entry_call(2, 1);

  -- isLock := False;
  -- entry_call(1,1);

   entry_call(2, 1);

   Put_Line("test");
    for i in 1..2 loop
      put(Natural'Image(tabExternalQueue(i)));
      New_Line;
   end loop;

   put(Natural'Image(currentPositionExternalQueue));
   New_Line;

   put("end testtt");

end test;
