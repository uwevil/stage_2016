with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;
with Ada.Calendar;
use Ada.Calendar;

package body Protected_Object is

   function is_lock return Boolean is
   begin
      return isLock or not (tabExternalQueue(currentPositionExternalQueue) = 0);
   end is_lock;

   procedure add_external_queue(taskId : in Natural) is
   begin
      if tabExternalQueue(currentPositionExternalQueue) = 0 then
         tabExternalQueue(currentPositionExternalQueue) := taskId;
      else
         tmp := currentPositionExternalQueue;
         for i in 1..(nbTask - 1) loop
            tmp := tmp mod nbTask + 1;
            if tabExternalQueue(tmp) = 0 then
               tabExternalQueue(tmp) := taskId;
               exit;
            end if;
         end loop;
      end if;

      Put_Line("-- externalQueue status " & Natural'Image(tabExternalQueue(1)) & " " & Natural'Image(tabExternalQueue(2))
               & " " & Natural'Image(tabExternalQueue(3)) & " " & Natural'Image(tabExternalQueue(4)));
   end add_external_queue;

   procedure remove_external_queue is
   begin
      tabExternalQueue(currentPositionExternalQueue) := 0;
      currentPositionExternalQueue := (currentPositionExternalQueue mod nbTask) + 1;
      Put_Line("-- currentPositionExternalQueue " & Natural'Image(currentPositionExternalQueue));
   end remove_external_queue;

   procedure wait_external_queue(taskId : in Natural) is
   begin
      while isLock or not (tabExternalQueue(currentPositionExternalQueue) = taskId) loop
         Put_Line("wait_external_queue taskID and entryId = " & Natural'Image(taskId));
         Put_Line("externalQueue status " & Natural'Image(tabExternalQueue(1)) & " " & Natural'Image(tabExternalQueue(2))
                 & " " & Natural'Image(tabExternalQueue(3)) & " " & Natural'Image(tabExternalQueue(4)));
         Put_Line("currentPositionExternalQueue " & Natural'Image(currentPositionExternalQueue));
         delay 0.1;
         null;
        -- Put_Line("wait_external_queue taskID and entryId = " & Natural'Image(taskId));
        --delay 1*Duration(1);
      end loop;
   end wait_external_queue;

   procedure evaluate_barrier(entryId : in Natural) is
   begin
      if entryId = 1 then
         tabBarrier(entryId) := x < 10;
      elsif entryId = 2 then
         tabBarrier(entryId) := x > 0;
      end if;
   end evaluate_barrier;

   function is_open(entryId : in Natural) return Boolean is
   begin
      return tabBarrier(entryId);
   end is_open;

   procedure add_entry_queue(taskId : in Natural; entryId : in Natural) is
   begin
      if tabEntryQueue(entryId)(tabCurrentPositionEntryQueue(entryId)) = 0 then
         tabEntryQueue(entryId)(tabCurrentPositionEntryQueue(entryId)) := taskId;
      else
         tmp := tabCurrentPositionEntryQueue(entryId);
         for i in 1..(nbTask - 1) loop
            tmp := tmp mod nbTask + 1;
            if tabEntryQueue(entryId)(tmp) = 0 then
               tabEntryQueue(entryId)(tmp) := taskId;
               exit;
            end if;
         end loop;
      end if;
      Put("-- entryQueue status ");
      for i in 1..nbEntry loop
         put(Natural'Image(i)& "( ");
         for j in 1..nbTask loop
            Put(Natural'Image(tabEntryQueue(i)(j)) & " ");
         end loop;
         Put(") ");
      end loop;
      New_Line;
   end add_entry_queue;

   procedure remove_entry_queue(entryId : in Natural) is
   begin
      tabEntryQueue(entryId)(tabCurrentPositionEntryQueue(entryId)) := 0;
      tabCurrentPositionEntryQueue(entryId) := (tabCurrentPositionEntryQueue(entryId) mod nbTask) + 1;
   end remove_entry_queue;

   procedure wait_entry_queue(taskId : in Natural; entryId : in Natural) is
   begin
      --while tabBarrier(entryId) = False or not (tabEntryQueue(entryId)(tabCurrentPositionEntryQueue(entryId)) = taskId) loop
      while tabStatus(taskId) = 0 loop
         Put_Line("wait_entry_queue taskID and entryId = " & Natural'Image(taskId));
         delay 0.1;
         null;
         --Put_Line("wait_entry_queue taskID and entryId = " & Natural'Image(taskId));
         --Put_Line("tabStatus " & Natural'Image(tabStatus(1)) & " " & Natural'Image(tabStatus(2)));
         --Put_Line("externalQueue status " & Natural'Image(tabExternalQueue(1)) & " " & Natural'Image(tabExternalQueue(2)));
        -- delay 1*Duration(1);
      end loop;
   end wait_entry_queue;

   procedure reevaluate_barrier is
   begin
      tabBarrier(1) := x < 10;
      tabBarrier(2) := x > 0;
      Put_Line("tabBarrier status " & Boolean'Image(tabBarrier(1)) & " " & Boolean'Image(tabBarrier(2)));
   end reevaluate_barrier;

   procedure run_entry_body(taskId : Natural; entryId : in Natural) is
   begin
      if entryId = 1 then
         ax := x;
         ax := ax + 1;
         x := ax;
         Put_Line(Integer'Image(taskId) & " inc x" & Integer'Image(x));
      elsif entryId = 2 then
         bx := x;
         bx := bx - 1;
         x := bx;
         Put_Line(Integer'Image(taskId) & " dec x" & Integer'Image(x));
      end if;
   end run_entry_body;

   function current_entry_eligible return Natural is
   begin
      for i in 1..nbEntry loop
         if tabBarrier(i) and then not (tabEntryQueue(i)(tabCurrentPositionEntryQueue(i)) = 0) then
            return i;
         end if;
      end loop;

      return 0;
   end current_entry_eligible;


   procedure check_and_run_all_entry is --return entryId
      taskIdTmp : Integer := 0;
   begin
      loop
         reevaluate_barrier;
         currentEntryIdEligible := current_entry_eligible;
         exit when currentEntryIdEligible = 0;
      --   Put_Line("currentEntryIdEligible " & Natural'Image(currentEntryIdEligible));

         taskIdTmp := tabEntryQueue(currentEntryIdEligible)(tabCurrentPositionEntryQueue(currentEntryIdEligible));

         run_entry_body(taskIdTmp, currentEntryIdEligible);

         remove_entry_queue(currentEntryIdEligible);

         tabStatus(taskIdTmp) := 1;
      end loop;
   end check_and_run_all_entry;



   procedure entry_call(taskId : in Natural; entryId : in Natural) is
   begin
      add_external_queue(taskId);
      if is_lock then
         wait_external_queue(taskId);
      end if;

      isLock := True;
      Put_Line(Natural'Image(taskId) & " lock " & Boolean'Image(isLock));
      remove_external_queue;
     -- Put_Line("task id " & Natural'Image(taskId) & " in SC");
     -- delay 1*Duration(2);
      evaluate_barrier(entryId);
      add_entry_queue(taskId, entryId);

      if not is_open(entryId) then
         tabStatus(taskId) := 0;
         isLock := False;
         Put_Line(Natural'Image(taskId) & " unlock " & Boolean'Image(isLock));
         wait_entry_queue(taskId, entryId);
      else
   --      Put_Line("------------------------Problem begin-----------------------");
         run_entry_body(taskId, entryId);
   --      Put_Line("run_entry_body task " & Natural'Image(taskId) & " entryId " & Natural'Image(entryId));
         remove_entry_queue(entryId);
     --    Put_Line("remove_entry_queue task " & Natural'Image(taskId) & " entryId " & Natural'Image(entryId));
     --  New_Line;
         Put_Line(Natural'Image(taskId) & " unlock " & Boolean'Image(isLock));
         check_and_run_all_entry;
--         Put_Line("check_and_run_all_entry task " & Natural'Image(taskId) & " entryId " & Natural'Image(entryId));
         isLock := False;
        -- Put_Line("------------------------Problem end-----------------------");
      end if;
--      Put_Line("EXIT taskId " & Natural'Image(taskId));
   end entry_call;




end Protected_Object;
