generic package Generic_Lock is
    procedure Acquire;
    procedure Release;
end Generic_Lock;
-- calling Lock_Acquire either
-- allows caller to operate in mutual exclusion
--   with partners requiring also this Lock by Lock_Acquire
-- or causes the caller to wait until the Lock is Released