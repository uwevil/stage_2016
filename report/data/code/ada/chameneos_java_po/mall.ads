with P_Id_Chameneos; use P_Id_Chameneos;
package Mall is
   function Get_A_Peer(X: Id_Chameneos) return Id_Chameneos;
end Mall;
-- Get_A_Peer(X) returns the name Y of a peer that is already in the Mall
-- i.e. that has called Get_A_Peer(Y) beforehand
-- the precedence relation  Get_A_Peer(Y) -> Get_A_Peer(X) holds
-- When X = Get_A_Peer(X), this is the result of a voluntary call
--  done to rescue a caller remaining single in the mall,
--  awaiting an impossible advent of a second visitor,
--   and is usually used to terminate correctly the simulation
