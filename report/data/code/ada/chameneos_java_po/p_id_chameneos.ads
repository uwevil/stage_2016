package P_Id_Chameneos is
   Nb_Chameneos : constant Integer := 9; -- number of players
   type Id_Chameneos is mod Nb_Chameneos;
   Nb_Requests : Integer := 6; -- number of strokes of each player
end P_Id_Chameneos;
