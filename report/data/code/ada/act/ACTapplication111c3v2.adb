with Ada.Text_IO; use Ada.Text_IO;

procedure application211c3 is

   type Client_Id is mod 3;
        -- Max_Client;
   Stock : Integer := 22;
        -- Resource_Number;

   protected type Control_Type is
      entry Request(Y : in Integer);
      procedure Release(Y : in Integer);
   private
      entry Assign(Y : in Integer);
   end Control_Type;

   protected body Control_Type is
      entry Request(Y : in Integer)
        when Assign'Count = 0 is
      begin
         Stock := Stock - Y;
         if Stock < 0 then
            requeue Assign;
         end if;
      end Request;

      entry Assign(Y : in Integer)
        when Stock >= 0 is
      begin
         null;
      end Assign;

      procedure Release(Y : in Integer) is
      begin
         Stock := Stock + Y;
      end Release;
   end Control_Type;

   resource : Control_Type;

   task type Client is
      entry Get_Id (Name : in Client_Id);
   end Client	;

   task body Client is
      My_Id : Client_Id := 0;
      My_Claim : Integer := 0;
      Full_Allocation : Integer := 0;
   begin
      accept Get_Id (Name : in Client_Id) do
         My_Id := Name;
      end Get_Id;

      loop
         for I in 1..4 loop
            --My_Claim :=(5 + I) mod 5;
            My_Claim :=(5 - I) mod 5; --deadlock
            resource.Request(My_Claim);
            Full_Allocation :=
                Full_Allocation + My_Claim;
         end loop;
         resource.Release(Full_Allocation);
         Full_Allocation := 0;
      end loop;
   end Client;

   type Client_Array is array(Client_Id) of Client;
   The_Clients : Client_Array;

begin
   for I in  Client_Id loop
      The_Clients(I).Get_Id (I);
   end loop;
end application211c3;
