with P_Chameneos; use P_Chameneos;
with Mall; use Mall;
with P_Colour; use P_Colour;
with P_Id_Chameneos; use P_Id_Chameneos; 

procedure Simulation is

   The_Colours : array(1..6) of Colour := (Yellow, Blue, Red, Blue, Yellow, Blue);
     --: array(Natural range<>) of Colour := (Yellow, Blue, Red, Blue, Yellow, Blue);
   The_Chaemenos : array(1..6) of Chameneos;
--   The_Chaemenos : array(The_Colours'Range) of Chameneos;

   C  : Colour;
   Id : Id_Chameneos;
begin
   for I in The_Chaemenos'Range loop
      C  := The_Colours(I);
      Id := I;
      The_Chaemenos(I).Start(Id, C);
   end loop;
end Simulation;
