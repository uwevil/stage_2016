public class Monitor {
    private int stock;
    private int assignCount;
    Monitor(){
        this.stock = 22;
        this.assignCount = 0;
    }
    public synchronized void request(int id, int y) {
        System.out.println(id+" IN Monitor: current stock = "+stock);
        try{
            while (assignCount != 0){
                wait();}
            stock -= y;
            System.out.println(id+" IN Monitor: stock after resquest of "+id+" = "+stock);
            if (stock<0)
                assign(id, y);
        }catch(InterruptedException e){
        }finally{
    }}
    public synchronized void assign(int id, int y){
        assignCount++;
        System.out.println(id+" IN Monitor: "+id+" is waiting (assignCount = "+assignCount+")");
        try{
            assert assignCount<3 : "assignCount == 3";
            while (stock < 0){
                wait();}
        }catch(InterruptedException e){
        }finally{
            assignCount--;
            System.out.println(id+" IN Monitor: "+id+" is getting out (assignCount = "+assignCount+")");
            notifyAll();
    }}
    public synchronized void release(int id, int y) {
        stock += y;
        System.out.println(id+" IN Monitor: "+id+" release "+y+" unit (current stock = "+stock+")");
        notifyAll();
}}