public class ActThread extends Thread{
    private int id;
    private Monitor m;
    public ActThread(int id, Monitor m){
        this.id = id;
        this.m = m;
    }
    public void run(){
        int i = 0;
        while (true){
            int MyClaim = 0;
            int FullAllocation = 0;
            for (i = 1; i<5; i++){
             //   MyClaim = (5 + i) % 5;
                MyClaim = (5 - i) % 5; //deadlock
                m.request(id, MyClaim);
                FullAllocation += MyClaim;
            }
            m.release(id, FullAllocation);
}}}