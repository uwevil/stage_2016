public class Combo{
    int id;
    Colour colour;
    
    Combo(int id, Colour colour){
        this.id = id;
        this.colour = colour;
    }
    
    public int getId(){
        return id;
    }
    
    public Colour getColour(){return colour;}
}