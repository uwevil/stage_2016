public class Simulation{
    static Colour[] TheColours = {Colour.YELLOW,
    Colour.BLUE, Colour.RED, Colour.BLUE}; //, Colour.YELLOW, Colour.BLUE};
    
    static Chameneos[] TheChameneos = new Chameneos[TheColours.length];
    static int[] test = new int[TheColours.length];
    
    public static void main (String args[]){
        Mall myMall = new Mall();
        
        for (int i = 0; i<TheColours.length; i++){
            test[i] = 0;
        }
        
        for (int i = 0; i<TheColours.length; i++){
            TheChameneos[i] = new Chameneos(myMall, new IdChameneos(i), TheColours[i], test);
        }
        
        for (int i = 0; i<TheColours.length; i++){
            TheChameneos[i].start();
        }
        
        for (int i = 0; i<TheColours.length; i++){
         
            try{   TheChameneos[i].join();} catch(InterruptedException e){}
        }
        
        for (int i = 0; i<TheColours.length; i++){
            System.out.println("i = "+ i + " ; test[i] = "+ test[i] + " ; test[test[i]] = "+ test[test[i]] );
        }

        
        for (int i = 0; i<TheColours.length; i++){
            if (test[test[i]] != i){
                System.out.println("assert error");
                System.out.println("i = "+ i + " ; test[i] = "+ test[i] + " ; test[test[i]] = "+ test[test[i]] );
            }
        }
    }
}