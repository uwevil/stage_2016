public class Chameneos extends Thread{
    private Mall mall;
    private IdChameneos id;
    private Colour myColour, otherColour;
    private int[] test;
    
    public Chameneos(Mall m, IdChameneos id, Colour c , int[] test){
        this.mall = m;
        this.id = id;
        this.myColour = c;
        this.test = test;
    }
    
    private void Message(String Mess){
        System.out.println("("+ id.toString() + ") i am "+ myColour.toString() + " and " + Mess);
    }
    private void EatingHoneysuckleAndTraining(){
        Message("I am Eating Honey suckle and Training");
    }
    private void GoingToTheMall(){
        Message("I am going to the mall");
    }
    private void Mutating(){
        Message("I am going to mute");
        Combo combo = mall.Cooperation(id, myColour);
        otherColour = combo.getColour();
        test[id.getId()] = combo.getId();
        myColour = myColour.ComplementaryColour(otherColour);
        Message("I have done a mutation");
    }
    public void run(){
        int i = 0;
        while(i<2){
            EatingHoneysuckleAndTraining();
            GoingToTheMall();
            Mutating();
            i++;
        }
    }
}