package body P_Colour is
   function Complementary_Colour
    (C1,C2:Colour) return Colour is
   begin
      if (C1 = C2) then
         return C1;
      else
         return Red;
      end if;
   end Complementary_Colour;
end P_Colour;
