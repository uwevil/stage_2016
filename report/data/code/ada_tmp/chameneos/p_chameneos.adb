package body P_Chameneos is

   task body Chameneos is
      My_Id        : Id_Chameneos;
      My_Colour    : Colour;
      Other_Colourz : Colour;

      procedure Message(Mess:in String)
        is
      begin
         Put(
        "(" & Id_Chameneos'Image(My_Id)
            & ") I am ");
         Put_Line(Colour'Image(My_Colour) 
            & " and " & Mess);
      end Message;

      procedure
    Eating_Honey_Suckle_And_Training
        is
      begin
         Message(
"I am eating honey suckle and training");
      end
    Eating_Honey_Suckle_And_Training;

      procedure Going_To_The_Mall is
      begin
         Message(
    "I am going to the mall");
      end Going_To_The_Mall;

      procedure Mutating
         (My_Id : Id_Chameneos ;
          My_Colour : in out Colour ; 
          Other_ColourT : in out Colour)
    is
      begin
         Message("I am ready to mute");
         Other_ColourT :=
        Cooperation(My_Id, My_Colour);
         My_Colour :=
            Complementary_Colour(My_Colour,
            Other_ColourT);
         Message(
    "I have performed a mutation");
      end Mutating;
   begin
      accept Start(Id2: in Id_Chameneos;
        C2: in Colour) do
         My_Id := Id2; 
         My_Colour := C2;
      end Start;
      loop
         Eating_Honey_Suckle_And_Training;
         Going_To_The_Mall;
         Mutating(My_Id, My_Colour,
            Other_Colourz);
      end loop;
   end Chameneos;
end P_Chameneos;
