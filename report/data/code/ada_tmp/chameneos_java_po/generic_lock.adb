package body Generic_Lock is
   ----------------------------------------------------------
   protected Server is
      entry Acquire;
      procedure Release;
   private
      Free : Boolean := True;
   end Server;
	
   protected body Server is
      entry Acquire when Free is 
      begin 
         Free := False; 
      end Acquire ;
      procedure Release is 
      begin 
         Free := True; 
      end Release;
   end Server;
   ----------------------------------------------------------	
   procedure Acquire is 
   begin 
      Server.Acquire; 
   end Acquire;
   procedure Release is 
   begin 
      Server.Release; 
   end Release;
   ----------------------------------------------------------	
end Generic_Lock;