with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Calendar;
use Ada.Calendar;

with Ada.Numerics.Discrete_Random;

procedure inc_dec is

   protected type t_produit is
      entry inc(id : Integer);
      entry dec(id : Integer);
   private
      x : Integer := 0;
   end t_produit;

   protected body t_produit is
      entry inc(id:Integer)when x<10 is
      begin
         x := x + 1;
      end inc;

      entry dec(id:Integer)when x>0 is
      begin
         x := x - 1;
      end dec;
   end t_produit;

   produit : t_produit;

   task type t_inc(id : Integer);
   task body t_inc is
   begin
      loop
         produit.inc(id);
         delay 1*Duration(1);
      end loop;
   end t_inc;

   task type t_dec(id : Integer);
   task body t_dec is
   begin
      loop
         produit.dec(id);
         delay 1*Duration(1);
      end loop;
   end t_dec;

   inc1 : t_inc(1);
   dec1 : t_dec(2);

begin
   null;
end inc_dec;
