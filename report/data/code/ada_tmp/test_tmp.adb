with Ada.Text_IO;
use Ada.Text_IO;

procedure test is
   -------------------------------DECLARATION-------------------------
   subtype modulo is Natural range 1..10;
   m : modulo := 3;

   -------------------------------MAIN-------------------------
begin

   for i in 1..(10 - 1) loop
      m := (m) mod 10 + 1;
     -- Put(Natural'Image(m mod 10 + i));
     -- Put(" ");
      Put(Natural'Image(m));
      New_Line;
   end loop;

end test;
