with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;

with Ada.Numerics.Discrete_Random;

procedure inc_dec_simple is

   nb_task : constant Natural := 2;

   type t_array_boolean is array(1..nb_task) of Boolean;
   is_alive : t_array_boolean := (others => True);

   type t_array_integer is array(1..nb_task) of Integer;
   last_hope : t_array_integer := (others => 0);
   hope_counter : t_array_integer := (others => 0);

   current_task : Integer := 0;
   context_switch : Integer := 0;

   x : Integer := 0;
   ax : Integer := 0;
   bx : Integer := 0;

   procedure dec;
   procedure inc;

   function nondet_uint return Integer is
      subtype range_hope is Integer range 0..(last_hope(current_task) - hope_counter(current_task));
      package nondeterminism is new Ada.Numerics.Discrete_Random(range_hope);
      use nondeterminism;
      G : Generator;
      res : Integer;
   begin
      Reset(G);
      res := Random(G);
      return res;
   end nondet_uint;

   function check_current_position (pos : Integer) return Boolean is
   begin
      return not (hope_counter(current_task) > pos or pos >= context_switch);
   end check_current_position;


   procedure inc is
   begin
      <<a1>> if not(check_current_position(1)) then goto a2; end if;
      ax := x;
      <<a2>> if not(check_current_position(2)) then goto a3; end if;
      ax := ax + 1;
      <<a3>> if not(check_current_position(3)) then goto a4; end if;
      x := ax;
      <<a4>> if not(check_current_position(4)) then goto a5; end if;
      ax := x;
      <<a5>> if not(check_current_position(5)) then goto a6; end if;
      ax := ax + 1;
      <<a6>> if not(check_current_position(6)) then goto a7; end if;
      x := ax;
      <<a7>>
   end inc;

   procedure dec is
   begin
      <<b1>> if not(check_current_position(1)) then goto b2; end if;
      bx := x;
      <<b2>> if not(check_current_position(2)) then goto b3; end if;
      bx := bx - 1;
      <<b3>> if not(check_current_position(3)) then goto b4; end if;
      x := bx;
      <<b4>> if not(check_current_position(4)) then goto b5; end if;
      bx := x;
      <<b5>> if not(check_current_position(5)) then goto b6; end if;
      bx := bx - 1;
      <<b6>> if not(check_current_position(6)) then goto b7; end if;
      x := bx;
      <<b7>>
   end dec;


begin
   last_hope(1) := 7;
   last_hope(2) := 7;
   current_task := 1;

   for i in 1..100 loop
      current_task := 1;
      if (is_alive(current_task)) then
         context_switch := hope_counter(current_task) + nondet_uint;

         inc;

         hope_counter(current_task) := context_switch;
         if context_switch = last_hope(current_task) then
            is_alive(current_task) := False;
         end if;
      end if;
      current_task := 2;
      if (is_alive(current_task)) then
         context_switch := hope_counter(current_task) + nondet_uint;

         dec;

         hope_counter(current_task) := context_switch;
         if context_switch = last_hope(current_task) then
            is_alive(current_task) := False;
         end if;
      end if;

     -- if (is_alive(1) or is_alive(2)) then
         put("ax ");
         put(ax);
         New_Line;
         put("bx ");
         put(bx);
         New_Line;
         put("x ");
         put(x);
         New_Line;
         put(hope_counter(1));
         New_Line;
         put(hope_counter(2));
         New_Line;
   --   end if;
   end loop;

   pragma Assert(x = 0);

end inc_dec_simple;
