with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Calendar;
use Ada.Calendar;

with Protected_Object;
use Protected_Object;

with Ada.Numerics.Discrete_Random;

procedure inc_dec is
   task type t_inc(id : Integer);
   task type t_dec(id : Integer);

   task body t_inc is
   begin
      id1 := 1;
      loop
         entry_call(id1, 1);
         delay 1*Duration(1);
      end loop;
   end t_inc;

   task body t_dec is
   begin
      id2 := 2;
      loop
         entry_call(id2, 2);
         delay 1*Duration(2);
      end loop;
   end t_dec;

   inc1 : t_inc(1);
   dec1 : t_dec(2);

begin
   null;
end inc_dec;
