with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Calendar;
use Ada.Calendar;

with Protected_Object_4;
use Protected_Object_4;

with Ada.Numerics.Discrete_Random;

procedure inc_dec is
   task type t_inc(id : Natural);
   task type t_dec(id : Natural);

   max : Natural := 100;

   task body t_inc is
   begin
      for i in 1..max loop
         Put_Line("lollllllllllllllllllll----------inc--begin----" & Natural'Image(id) &"----"
                  & Natural'Image(i));
         entry_call(id, 1);
         Put_Line("lollllllllllllllllllll-----------inc-end------" & Natural'Image(id) &"--"
                 & Natural'Image(i));
         delay 1*Duration(3);
      end loop;
   end t_inc;

   task body t_dec is
   begin
      for i in 1..max loop
         Put_Line("lollllllllllllllllllll----------dec--begin----" & Natural'Image(id) &"----"
                  & Natural'Image(i));
         entry_call(id, 2);
         Put_Line("lollllllllllllllllllll----------dec--end----" & Natural'Image(id) &"----"
                  & Natural'Image(i));
         delay 1*Duration(1);
      end loop;
   end t_dec;


   inc1 : t_inc(1);
   dec2 : t_dec(2);
   inc3 : t_inc(3);
   dec4 : t_dec(4);
   --dec2 : t_dec(12);
   --dec3 : t_dec(13);
   --dec4 : t_dec(14);

begin
   null;
end inc_dec;
