with Ada.Text_IO;
use Ada.Text_IO;
procedure peterson is
   -------------------------------DECLARATION-------------------------
   type t_id is range 1..2;
   type t_flag is array(t_id'Range) of Boolean;
   flag : t_flag := (others => False);
   turn : t_id;
   -------------------------------PROCEDURE-------------------------
   procedure enter_cs(id : t_id) is
   begin
      flag(id) := True;
      turn := (id mod 2) + 1;
      loop
         exit when not flag((id mod 2) + 1) or turn = id;
      end loop;
      Put_Line((1..Integer(id) => ' ') & "Enter critical section : " & Integer'image(Integer(id)));
      delay 0.1*Duration(Integer(id));
   end enter_cs;
   procedure exit_cs(id : t_id) is
   begin
      Put_Line((1..Integer(id) => ' ') & "Exit critical section :  " & Integer'image(Integer(id)));
      delay 0.2*Duration(Integer(id));
      flag(id) := False;
   end exit_cs;
   procedure in_cs(id : t_id) is
   begin
      Put_Line((1..Integer(id) => ' ') & "In critical section :    " & Integer'image(Integer(id)));
      delay 0.5*Duration(Integer(id));
   end in_cs;
   -------------------------------TASK-------------------------
   task T1;
   task body T1 is
      id : t_id := 1;
   begin
      loop
         enter_cs(id);
         in_cs(id);
         exit_cs(id);
      end loop;
   end T1;

   task T2;
   task body T2 is
      id : t_id := 2;
   begin
      loop
         enter_cs(id);
         in_cs(id);
         exit_cs(id);
      end loop;
   end T2;
   -------------------------------MAIN-------------------------
begin
   null;
end peterson;
