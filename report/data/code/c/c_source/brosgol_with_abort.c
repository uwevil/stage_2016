#include <stdio.h>
#include <assert.h>

#define TRUE 1
#define FALSE 0
#define FREE -1

#define NB_TASK 5 //without main task
#define NB_ENTRY 2
#define NB_PROCEDURE 1
#define NB_FUNCTION 0

/*-------------------------------------------------------------*/
int active[NB_TASK+1] = {FALSE, FALSE, FALSE, FALSE, FALSE, FALSE};
int cs, ct;
int pc[NB_TASK+1];
int size[NB_TASK+1];

void create(int t, int id){
    active[id] = TRUE;
    t = id;
}
/*-------------------------------------------------------------*/
int tabStatus[NB_TASK+1] = {TRUE, TRUE, TRUE, TRUE, TRUE, TRUE};
int tabWaiting[NB_TASK+1] = {0, 0, 0, 0, 0, 0};
int tabBarrier[NB_ENTRY+1] = {FALSE, FALSE, FALSE, FALSE, FALSE, FALSE};

int currentTask=1;

int tabEntryCount[NB_ENTRY+1] = {0,0,0};
int taskInWait = 0;

int externalLock = FREE;

void lock(int id){
    __CPROVER_assume(externalLock==FREE);
    externalLock=id;
}

void transfert(int id, int newId){
    __CPROVER_assert(externalLock==id, "OKexternalLock!=id");
    externalLock=newId;
}

void unlock(int id){
    __CPROVER_assert(externalLock==id, "OKexternalLock!=id");
    externalLock=FREE;
}
/*---------------------------------------------------------------------------*/

int flush_count = 0;

int available[NB_TASK] = {TRUE, TRUE, TRUE, TRUE, TRUE};

int eid1[NB_TASK];
int eid2[NB_TASK];
int pid1[NB_TASK];

/*---------------------------------------------------------------------------*/

void evaluate_barrier(int index){
    if (index == 1){ //Get_pair
        tabBarrier[index] = TRUE;
    }else if (index == 2){ //Waiting
        tabBarrier[index] = flush_count >0;
    }
}

void reevaluate_barrier(){
    tabBarrier[1] = TRUE; //Cooperate
    tabBarrier[2] = flush_count>0; //Waiting
}

void add_entry_queue(int taskId, int index){
    tabEntryCount[index] = tabEntryCount[index]+1;
    taskInWait = taskInWait + 1;
    __CPROVER_assert(taskInWait<NB_TASK, "OKdeadlock");
    
    tabWaiting[taskId] = index;
    tabStatus[taskId] = FALSE;
}

void remove_entry_queue(int taskId, int index){
    tabEntryCount[index] = tabEntryCount[index]-1;
    taskInWait = taskInWait - 1;
    
    tabWaiting[taskId] = 0;
}

int current_task_eligible(){ //return taskId
    int j;
    for (j=1; j<=NB_TASK;j++){
        if (tabWaiting[currentTask]!=0&&tabBarrier[tabWaiting[currentTask]]){
            return currentTask;
        }
        currentTask = (currentTask % NB_TASK)+1;
    }
    return 0;
}

void run_entry_body(int taskId, int index);

void requeue(int taskId, int index, int with_abort){ //requeue to another or same entry queue
    evaluate_barrier(index);
    
    if (tabBarrier[index]==FALSE && !with_abort){
        add_entry_queue(taskId, index);
        unlock(taskId);
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        remove_entry_queue(taskId, index);
    }
    
    if (tabBarrier[index]==TRUE){
        run_entry_body(taskId,index);
    }
}

void run_entry_body(int taskId, int index){
    if (index == 1) { //get_pair
        if (available[eid1[taskId]] && available[(eid1[taskId] +1) % NB_TASK]){
            available[eid1[taskId]] = FALSE;
            available[(eid1[taskId] + 1) % NB_TASK] = FALSE;
        }else{
            eid2[taskId] = eid1[taskId];
            requeue(taskId, 2, TRUE);
        }
    }else if (index == 2){ //please_get_pair
        flush_count = flush_count-1;
        if (available[eid2[taskId]] && available[(eid2[taskId]+1)%NB_TASK]){
            available[eid2[taskId]] = FALSE;
            available[(eid2[taskId]+1)%NB_TASK] = FALSE;
        }else{
            eid2[taskId] = eid2[taskId];
            requeue(taskId, 2, TRUE);
        }
    }
}

int check_and_run_any_entry(){
    reevaluate_barrier();
    int taskIdEligible = current_task_eligible();
    if (taskIdEligible==0){
        return FREE;
    }
    tabStatus[taskIdEligible] = TRUE;
    return taskIdEligible;
}

void run_function_call(taskId, index){
}

void run_procedure_call(taskId, index){
    int taskIdEligible;
    if (index == 1){
        available[pid1[taskId]] = TRUE;
        available[(pid1[taskId] + 1) % NB_TASK] = TRUE;
        flush_count = tabEntryCount[2];
    }
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
}

void run_entry_call(int taskId, int index){
    int taskIdEligible;
    evaluate_barrier(index);
    
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        remove_entry_queue(taskId, index);
    }
    run_entry_body(taskId,index);
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
}

void protected_object_call(int taskId, int appel, int index){
    lock(taskId);
    
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        run_entry_call(taskId, index);
    }
}

void t_one(){
    static int taskId=1;
    static int taskIdEligible;
    static int appel = 2;
    static int index = 1;

    //protected_object_call(taskId, appel, index)
    lock(taskId);
    appel=2;
    index=1;
    
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        //run_entry_call(taskId, index);
        static int taskIdEligible;
        evaluate_barrier(index);
        
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
            remove_entry_queue(taskId, index);
        }
        //run_entry_body(taskId,index);
        label1:
        if (index == 1) { //get_pair
            if (available[eid1[taskId]] && available[(eid1[taskId] +1) % NB_TASK]){
                available[eid1[taskId]] = FALSE;
                available[(eid1[taskId] + 1) % NB_TASK] = FALSE;
            }else{
                //requeue(taskId, 2, TRUE);
                static int with_abort = TRUE;
                evaluate_barrier(index);
                
                if (tabBarrier[index]==FALSE && !with_abort){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                    remove_entry_queue(taskId, index);
                }
                
                if (tabBarrier[index]==TRUE){
                    //run_entry_body(taskId,index);
                    index = 2;
                    goto label1;
                }
            }
        }else if (index == 2){ //please_get_pair
            flush_count = flush_count-1;
            if (available[eid2[taskId]] && available[(eid2[taskId]+1)%NB_TASK]){
                available[eid2[taskId]] = FALSE;
                available[(eid2[taskId]+1)%NB_TASK] = FALSE;
            }else{
                index = 2;
                //requeue(taskId, 2, TRUE);
                static int with_abort = TRUE;
                evaluate_barrier(index);
                
                if (tabBarrier[index]==FALSE && !with_abort){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                    remove_entry_queue(taskId, index);
                }
                
                if (tabBarrier[index]==TRUE){
                    //run_entry_body(taskId,index);
                    index = 2;
                    goto label1;
                }
            }
        }
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    
    lock(taskId);
    
    appel=1;
    index=1;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        run_entry_call(taskId, index);
    }
    
}











