#include <stdio.h>
#include <assert.h>

#define TRUE 1
#define FALSE 0
#define FREE -1

#define NB_TASK 3 //without main task
#define NB_ENTRY 2
#define NB_PROCEDURE 1
#define NB_FUNCTION 0

/*-------------------------------------------------------------*/
int active[NB_TASK+1] = {TRUE, FALSE, FALSE};
int cs, ct;
int pc[NB_TASK+1];
int size[NB_TASK+1];

void create(int t, int id){
    active[id] = TRUE;
    t = id;
}
/*-------------------------------------------------------------*/
int tabStatus[NB_TASK+1] = {TRUE, TRUE, TRUE, TRUE};
int tabWaiting[NB_TASK+1] = {0, 0, 0, 0};
int tabBarrier[NB_ENTRY+1] = {FALSE, FALSE, FALSE};

int currentTask=1;

int y[NB_TASK+1] = {0,0,0,0};

int tabEntryCount[NB_ENTRY+1] = {0,0,0};
int taskInWait = 0;

int stock = 22;

int externalLock = FREE;

void lock(int id){
    __CPROVER_assume(externalLock==FREE);
    externalLock=id;
}

void transfert(int id, int newId){
    __CPROVER_assert(externalLock==id, "OKexternalLock!=id");
    externalLock=newId;
}

void unlock(int id){
    __CPROVER_assert(externalLock==id, "OKexternalLock!=id");
    externalLock=FREE;
}
/*---------------------------------------------------------------------------*/

void evaluate_barrier(int index){
    if (index == 1){ //Request
        tabBarrier[index] = tabEntryCount[2]==0;
    }else if (index == 2){ //Assign
        tabBarrier[index] = stock>=0;
    }
}

void reevaluate_barrier(){
    tabBarrier[1] = tabEntryCount[2]==0; //request
    tabBarrier[2] = stock>=0; //assign
}

void add_entry_queue(int taskId, int index){
    tabEntryCount[index] = tabEntryCount[index]+1;
    taskInWait = taskInWait + 1;
    __CPROVER_assert(taskInWait<NB_TASK, "OKdeadlock");
    
    tabWaiting[taskId] = index;
    tabStatus[taskId] = FALSE;
}

void remove_entry_queue(int taskId, int index){
    tabEntryCount[index] = tabEntryCount[index]-1;
    taskInWait = taskInWait - 1;
    
    tabWaiting[taskId] = 0;
}

int current_task_eligible(){ //return taskId
    int j;
    for (j=1; j<=NB_TASK;j++){
        if (tabWaiting[currentTask]!=0&&tabBarrier[tabWaiting[currentTask]]){
            return currentTask;
        }
        currentTask = (currentTask % NB_TASK)+1;
    }
    return 0;
}

void run_entry_body(int taskId, int index);

void requeue(int taskId, int index, int with_abort){ //requeue to another or same entry queue
    evaluate_barrier(index);
    
    if (tabBarrier[index]==FALSE && !with_abort){
        add_entry_queue(taskId, index);
        unlock(taskId);
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        remove_entry_queue(taskId, index);
    }
    
    if (tabBarrier[index]==TRUE){
        run_entry_body(taskId,index);
    }
}

void run_entry_body(int taskId, int index){
    if (index == 1) { //Request
        stock = stock - y[taskId];
        if (stock < 0){
            requeue(taskId, 2, FALSE); //requeue Assign
        }
    }else if (index == 2){ //Assign
    }
}

int check_and_run_any_entry(){
    reevaluate_barrier();
    int taskIdEligible = current_task_eligible();
    if (taskIdEligible==0){
        return FREE;
    }
    tabStatus[taskIdEligible] = TRUE;
    return taskIdEligible;
}

void run_function_call(taskId, index){
}

void run_procedure_call(taskId, index){
    int taskIdEligible;
    if (index == 1){
        stock = stock + y[taskId];
    }
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
}

void run_entry_call(int taskId, int index){
    int taskIdEligible;
    evaluate_barrier(index);
    
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        remove_entry_queue(taskId, index);
    }
    run_entry_body(taskId,index);
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
}

void protected_object_call(int taskId, int appel, int index){
    lock(taskId);
    
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        run_entry_call(taskId, index);
    }
}


void t_one(){
    static int taskId = 1;
    static int appel = 1;
    static int index = 1;
    static int taskIdEligible;
    static int taskIdTmp;
    static int i = 0;
    static int myClaim = 0;
    static int fullAllocation = 0;
    
    for (i=1); i<=4; i++){
        myClaim = (5+i)%5;
        protected_object_call(taskId, 2, 1);
        fullAllocation = fullAllocation+myClaim;
    }
    
    myClaim = fullAllocation;
    protected_object_call(taskId, 1, 1);
    fullAllocation = 0;
}

void t_two(){
    entry_call(taskId, 2, 1);
}

void t_three(){
    entry_call(taskId, 2, 1);
}

int main(){
    pthread_t t1, t2, t3;
    pthread_create(&t1, t_one, NULL, 1);
    pthread_create(&t2, t_two, NULL, 2);
    pthread_create(&t3, t_three, NULL, 3);
    return 0;
}
