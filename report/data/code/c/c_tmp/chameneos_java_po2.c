void t_one(){
    static int taskId=1;
    static int taskIdEligible;
    static int my_peer = 0;
    
    //@begin
    static int i = 1;
    // for (i = 1; i<=nb_request; i++){
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
    static int score=0;
    //  while (score!=2) {
    //@controlLabel
    x1[taskId]=x3[taskId];
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
    static int appel = 2;
    static int index = 1;
    //@controlBegin
    lock2(taskId);
    
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        //@controlAssume
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        //@controlAssume
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
            //@controlEnd
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            //@controlBegin
            remove_entry_queue2(taskId, index);
        }
        //@controlAssume
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    //@controlBegin
                    remove_entry_queue2(taskId, index);
                }
                //@controlAssume
                //run_entry_body2(taskId,index);
                goto while1;
                //--------------------------
                //--------------------------
            }
            //@controlAssume
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    //@controlBegin
                    remove_entry_queue2(taskId, index);
                }
                //@controlAssume
                //run_entry_body2(taskId,index);
                goto while1;
                //--------------------------
                //--------------------------
            }
            //@controlAssume
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            //@controlAssume
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            //@controlAssume
        }else if (index == 2){ //Wait
            //@controlAssume
        }
        //@controlAssume
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    //@controlAssume
    //--------------------------
while1:
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = ?;
        goto end;
    }
    //@controlAssume
    //@controlEnd
    
    //  }
    
    return3[taskId]=x_other1[taskId];
    //--------------------------
    my_peer=return3[taskId];
    if (my_peer==taskId){
        cs = size[taskId];
        goto end;
    }
    //@controlAssume
    my_peer1[taskId]=my_peer;
    //--------------------------
    
    i = 2;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
    score=0;
    //  while (score!=2) {
    //@controlLabel
    x1[taskId]=x3[taskId];
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
    appel = 2;
    index = 1;
    //@controlBegin
    lock2(taskId);
    
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        //@controlAssume
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        //@controlAssume
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
            //@controlEnd
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            //@controlBegin
            remove_entry_queue2(taskId, index);
        }
        //@controlAssume
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    //@controlBegin
                    remove_entry_queue2(taskId, index);
                }
                //@controlAssume
                //run_entry_body2(taskId,index);
                goto while2;
                //--------------------------
                //--------------------------
            }
            //@controlAssume
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    //@controlBegin
                    remove_entry_queue2(taskId, index);
                }
                //@controlAssume
                //run_entry_body2(taskId,index);
                goto while2;
                //--------------------------
                //--------------------------
            }
            //@controlAssume
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            //@controlAssume
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            //@controlAssume
        }else if (index == 2){ //Wait
            //@controlAssume
        }
        //@controlAssume
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    //@controlAssume
    //--------------------------
while2:
    score=score1[taskId];
    
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = ?;
        goto end;
    }
    //@controlAssume
    //@controlEnd
    
    //  }
    
    return3[taskId]=x_other1[taskId];
    //--------------------------
    my_peer=return3[taskId];
    if (my_peer==taskId){
        cs = size[taskId];
        goto end;
    }
    //@controlAssume
    my_peer1[taskId]=my_peer;
    //--------------------------
    
    i=3;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
    score=0;
    //  while (score!=2) {
    //@controlLabel
    x1[taskId]=x3[taskId];
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
    appel = 2;
    index = 1;
    //@controlBegin
    lock2(taskId);
    
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        //@controlAssume
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        //@controlAssume
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
            //@controlEnd
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            //@controlBegin
            remove_entry_queue2(taskId, index);
        }
        //@controlAssume
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    //@controlBegin
                    remove_entry_queue2(taskId, index);
                }
                //@controlAssume
                //run_entry_body2(taskId,index);
                goto while3;
                //--------------------------
                //--------------------------
            }
            //@controlAssume
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    //@controlBegin
                    remove_entry_queue2(taskId, index);
                }
                //@controlAssume
                //run_entry_body2(taskId,index);
                goto while3;
                //--------------------------
                //--------------------------
            }
            //@controlAssume
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            //@controlAssume
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            //@controlAssume
        }else if (index == 2){ //Wait
            //@controlAssume
        }
        //@controlAssume
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    //@controlAssume
    //--------------------------
while3:
    score=score1[taskId];
    
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = ?;
        goto end;
    }
    //@controlAssume
    //@controlEnd
    
    //  }
    
    return3[taskId]=x_other1[taskId];
    //--------------------------
    my_peer=return3[taskId];
    if (my_peer==taskId){
        cs = size[taskId];
        goto end;
    }
    //@controlAssume
    my_peer1[taskId]=my_peer;
    //--------------------------
    
    i=4;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
    score=0;
    //  while (score!=2) {
    //@controlLabel
    x1[taskId]=x3[taskId];
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
    appel = 2;
    index = 1;
    //@controlBegin
    lock2(taskId);
    
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        //@controlAssume
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        //@controlAssume
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
            //@controlEnd
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            //@controlBegin
            remove_entry_queue2(taskId, index);
        }
        //@controlAssume
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    //@controlBegin
                    remove_entry_queue2(taskId, index);
                }
                //@controlAssume
                //run_entry_body2(taskId,index);
                goto while4;
                //--------------------------
                //--------------------------
            }
            //@controlAssume
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    //@controlBegin
                    remove_entry_queue2(taskId, index);
                }
                //@controlAssume
                //run_entry_body2(taskId,index);
                goto while4;
                //--------------------------
                //--------------------------
            }
            //@controlAssume
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            //@controlAssume
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            //@controlAssume
        }else if (index == 2){ //Wait
            //@controlAssume
        }
        //@controlAssume
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    //@controlAssume
    //--------------------------
while4:
    score=score1[taskId];
    
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = ?;
        goto end;
    }
    //@controlAssume
    //@controlEnd
    
    //  }
    
    return3[taskId]=x_other1[taskId];
    //--------------------------
    my_peer=return3[taskId];
    if (my_peer==taskId){
        cs = size[taskId];
        goto end;
    }
    //@controlAssume
    my_peer1[taskId]=my_peer;
    //--------------------------
    
    i=5;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
    score=0;
    //  while (score!=2) {
    //@controlLabel
    x1[taskId]=x3[taskId];
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
    appel = 2;
    index = 1;
    //@controlBegin
    lock2(taskId);
    
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        //@controlAssume
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        //@controlAssume
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
            //@controlEnd
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            //@controlBegin
            remove_entry_queue2(taskId, index);
        }
        //@controlAssume
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    //@controlBegin
                    remove_entry_queue2(taskId, index);
                }
                //@controlAssume
                //run_entry_body2(taskId,index);
                goto while5;
                //--------------------------
                //--------------------------
            }
            //@controlAssume
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    //@controlBegin
                    remove_entry_queue2(taskId, index);
                }
                //@controlAssume
                //run_entry_body2(taskId,index);
                goto while5;
                //--------------------------
                //--------------------------
            }
            //@controlAssume
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            //@controlAssume
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            //@controlAssume
        }else if (index == 2){ //Wait
            //@controlAssume
        }
        //@controlAssume
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    //@controlAssume
    //--------------------------
while5:
    score=score1[taskId];
    
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = ?;
        goto end;
    }
    //@controlAssume
    //@controlEnd
    
    //  }
    
    return3[taskId]=x_other1[taskId];
    //--------------------------
    my_peer=return3[taskId];
    if (my_peer==taskId){
        cs = size[taskId];
        goto end;
    }
    //@controlAssume
    my_peer1[taskId]=my_peer;
    //--------------------------
    
    i=6;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
    score=0;
    //  while (score!=2) {
    //@controlLabel
    x1[taskId]=x3[taskId];
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
    appel = 2;
    index = 1;
    //@controlBegin
    lock2(taskId);
    
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        //@controlAssume
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        //@controlAssume
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
            //@controlEnd
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            //@controlBegin
            remove_entry_queue2(taskId, index);
        }
        //@controlAssume
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    //@controlBegin
                    remove_entry_queue2(taskId, index);
                }
                //@controlAssume
                //run_entry_body2(taskId,index);
                goto while6;
                //--------------------------
                //--------------------------
            }
            //@controlAssume
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    //@controlBegin
                    remove_entry_queue2(taskId, index);
                }
                //@controlAssume
                //run_entry_body2(taskId,index);
                goto while6;
                //--------------------------
                //--------------------------
            }
            //@controlAssume
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            //@controlAssume
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            //@controlAssume
        }else if (index == 2){ //Wait
            //@controlAssume
        }
        //@controlAssume
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    //@controlAssume
    //--------------------------
while6:
    score=score1[taskId];
    
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = ?;
        goto end;
    }
    //@controlAssume
    //@controlEnd
    
    //  }
    
    return3[taskId]=x_other1[taskId];
    //--------------------------
    my_peer=return3[taskId];
    if (my_peer==taskId){
        cs = size[taskId];
        goto end;
    }
    //@controlAssume
    my_peer1[taskId]=my_peer;
    //--------------------------
    //  }
    //@end
}