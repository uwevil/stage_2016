void t_one(){
    //@begin
    static int taskId = 1;
    static int other = 0;
    other = (taskId % NB_TASK) + 1;
    flag[taskId] = TRUE;
    
    turn = other;
    //while (flag[other] && turn == other) {}
    __CPROVER_assume(!(flag[other] && turn == other));
    
    static ax;
    ax = x;
    ax = ax + 1;
    x = ax;
    
    flag[taskId] = FALSE;
    //@end
}

void t_two(){
    //@begin
    static int taskId = 2;
    static int other = 0;
    other = (taskId % NB_TASK) + 1;
    flag[taskId] = TRUE;
    
    turn = other;
    //while (flag[other] && turn == other) {}
    __CPROVER_assume(!(flag[other] && turn == other));
    
    static int bx;
    bx = x;
    bx = bx - 1;
    x = bx;
    
    flag[taskId] = FALSE;
    //@end
}