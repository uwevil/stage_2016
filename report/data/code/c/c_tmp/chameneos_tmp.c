//yellow, blue, red, blue, yellow, blue

void t_one(){
    static int taskId=1;
    static int taskIdEligible;
    static int index;
l0: if (pc[ct]>0||0>=cs) goto l1;
    myColour[taskId]=YELLOW;
l1: if (pc[ct]>1||1>=cs) goto l2;
    otherColour[taskId]=0;
l2: if (pc[ct]>2||2>=cs) goto l3;
    lock(taskId);
l3: if (pc[ct]>3||3>=cs) goto l4;
    index=1;
    evaluate_barrier(index);
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
l4: if (pc[ct]>4||4>=cs) goto l5;
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
l5: if (pc[ct]>5||5>=cs) goto l6;
        remove_entry_queue(taskId, index);
    }
__CPROVER_assume(cs>=6);

l6: if (pc[ct]>6||6>=cs) goto l7;
    //run_entry_body(taskId,index);
    index=1;
    c1[taskId]=myColour[taskId];
    if (index == 1) { //Request
        if (firstCall){
            aColour=c1[taskId];
            firstCall=FALSE;
            
            //requeue(taskId, 2); //requeue Assign
            index = 2;
            evaluate_barrier(index);
            if (tabBarrier[index]==FALSE){
                add_entry_queue(taskId, index);
                unlock(taskId);
l7: if (pc[ct]>7||7>=cs) goto l8;
                __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
l8: if (pc[ct]>8||8>=cs) goto l9;
                remove_entry_queue(taskId, index);
            }
__CPROVER_assume(cs>=9);

            //run_entry_body(taskId,index);
l9: if (pc[ct]>9||9>=cs) goto l10;
            cOther1[taskId]=bColour;
        }else{
__CPROVER_assume(cs>=10);

l10: if (pc[ct]>10||10>=cs) goto l11;
            bColour=c1[taskId];
            cOther1[taskId]=aColour;
            firstCall=TRUE;
//@controlend
        }
__CPROVER_assume(cs>=11);

    }
__CPROVER_assume(cs>=11);

l11: if (pc[ct]>11||11>=cs) goto l12;
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
l12: if (pc[ct]>12||12>=cs) goto l13;
    otherColour[taskId]=cOther1[taskId];
    
l13: if (pc[ct]>13||13>=cs) goto l14;
    myColour[taskId]=complementary_colour(myColour[taskId], otherColour[taskId]);
l14:
 return;
}

//yellow, blue, red, blue, yellow, blue

void t_two(){
    static int taskId=2;
    static int taskIdEligible;
    static int index;
l0: if (pc[ct]>0||0>=cs) goto l1;
    myColour[taskId]=YELLOW;
l1: if (pc[ct]>1||1>=cs) goto l2;
    otherColour[taskId]=0;
l2: if (pc[ct]>2||2>=cs) goto l3;
    lock(taskId);
l3: if (pc[ct]>3||3>=cs) goto l4;
    index=1;
    evaluate_barrier(index);
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
l4: if (pc[ct]>4||4>=cs) goto l5;
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
l5: if (pc[ct]>5||5>=cs) goto l6;
        remove_entry_queue(taskId, index);
    }
__CPROVER_assume(cs>=6);

l6: if (pc[ct]>6||6>=cs) goto l7;
    //run_entry_body(taskId,index);
    index=1;
    c1[taskId]=myColour[taskId];
    if (index == 1) { //Request
        if (firstCall){
            aColour=c1[taskId];
            firstCall=FALSE;
            
            //requeue(taskId, 2); //requeue Assign
            index = 2;
            evaluate_barrier(index);
            if (tabBarrier[index]==FALSE){
                add_entry_queue(taskId, index);
                unlock(taskId);
l7: if (pc[ct]>7||7>=cs) goto l8;
                __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
l8: if (pc[ct]>8||8>=cs) goto l9;
                remove_entry_queue(taskId, index);
            }
__CPROVER_assume(cs>=9);

            //run_entry_body(taskId,index);
l9: if (pc[ct]>9||9>=cs) goto l10;
            cOther1[taskId]=bColour;
        }else{
__CPROVER_assume(cs>=10);

l10: if (pc[ct]>10||10>=cs) goto l11;
            bColour=c1[taskId];
            cOther1[taskId]=aColour;
            firstCall=TRUE;
            //@controlend
        }
__CPROVER_assume(cs>=11);

    }
__CPROVER_assume(cs>=11);

l11: if (pc[ct]>11||11>=cs) goto l12;
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
l12: if (pc[ct]>12||12>=cs) goto l13;
    otherColour[taskId]=cOther1[taskId];
    
l13: if (pc[ct]>13||13>=cs) goto l14;
    myColour[taskId]=complementary_colour(myColour[taskId], otherColour[taskId]);
l14:
 return;
}

//yellow, blue, red, blue, yellow, blue

void t_three(){
    static int taskId=3;
    static int taskIdEligible;
    static int index;
l0: if (pc[ct]>0||0>=cs) goto l1;
    myColour[taskId]=YELLOW;
l1: if (pc[ct]>1||1>=cs) goto l2;
    otherColour[taskId]=0;
l2: if (pc[ct]>2||2>=cs) goto l3;
    lock(taskId);
l3: if (pc[ct]>3||3>=cs) goto l4;
    index=1;
    evaluate_barrier(index);
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
l4: if (pc[ct]>4||4>=cs) goto l5;
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
l5: if (pc[ct]>5||5>=cs) goto l6;
        remove_entry_queue(taskId, index);
    }
__CPROVER_assume(cs>=6);

l6: if (pc[ct]>6||6>=cs) goto l7;
    //run_entry_body(taskId,index);
    index=1;
    c1[taskId]=myColour[taskId];
    if (index == 1) { //Request
        if (firstCall){
            aColour=c1[taskId];
            firstCall=FALSE;
            
            //requeue(taskId, 2); //requeue Assign
            index = 2;
            evaluate_barrier(index);
            if (tabBarrier[index]==FALSE){
                add_entry_queue(taskId, index);
                unlock(taskId);
l7: if (pc[ct]>7||7>=cs) goto l8;
                __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
l8: if (pc[ct]>8||8>=cs) goto l9;
                remove_entry_queue(taskId, index);
            }
__CPROVER_assume(cs>=9);

            //run_entry_body(taskId,index);
l9: if (pc[ct]>9||9>=cs) goto l10;
            cOther1[taskId]=bColour;
        }else{
__CPROVER_assume(cs>=10);

l10: if (pc[ct]>10||10>=cs) goto l11;
            bColour=c1[taskId];
            cOther1[taskId]=aColour;
            firstCall=TRUE;
            //@controlend
        }
__CPROVER_assume(cs>=11);

    }
__CPROVER_assume(cs>=11);

l11: if (pc[ct]>11||11>=cs) goto l12;
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
l12: if (pc[ct]>12||12>=cs) goto l13;
    otherColour[taskId]=cOther1[taskId];
    
l13: if (pc[ct]>13||13>=cs) goto l14;
    myColour[taskId]=complementary_colour(myColour[taskId], otherColour[taskId]);
l14:
 return;
}
//yellow, blue, red, blue, yellow, blue

void t_four(){
    static int taskId=4;
    static int taskIdEligible;
    static int index;
l0: if (pc[ct]>0||0>=cs) goto l1;
    myColour[taskId]=YELLOW;
l1: if (pc[ct]>1||1>=cs) goto l2;
    otherColour[taskId]=0;
l2: if (pc[ct]>2||2>=cs) goto l3;
    lock(taskId);
l3: if (pc[ct]>3||3>=cs) goto l4;
    index=1;
    evaluate_barrier(index);
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
l4: if (pc[ct]>4||4>=cs) goto l5;
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
l5: if (pc[ct]>5||5>=cs) goto l6;
        remove_entry_queue(taskId, index);
    }
__CPROVER_assume(cs>=6);

l6: if (pc[ct]>6||6>=cs) goto l7;
    //run_entry_body(taskId,index);
    index=1;
    c1[taskId]=myColour[taskId];
    if (index == 1) { //Request
        if (firstCall){
            aColour=c1[taskId];
            firstCall=FALSE;
            
            //requeue(taskId, 2); //requeue Assign
            index = 2;
            evaluate_barrier(index);
            if (tabBarrier[index]==FALSE){
                add_entry_queue(taskId, index);
                unlock(taskId);
l7: if (pc[ct]>7||7>=cs) goto l8;
                __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
l8: if (pc[ct]>8||8>=cs) goto l9;
                remove_entry_queue(taskId, index);
            }
__CPROVER_assume(cs>=9);

            //run_entry_body(taskId,index);
l9: if (pc[ct]>9||9>=cs) goto l10;
            cOther1[taskId]=bColour;
        }else{
__CPROVER_assume(cs>=10);

l10: if (pc[ct]>10||10>=cs) goto l11;
            bColour=c1[taskId];
            cOther1[taskId]=aColour;
            firstCall=TRUE;
            //@controlend
        }
__CPROVER_assume(cs>=11);

    }
__CPROVER_assume(cs>=11);

l11: if (pc[ct]>11||11>=cs) goto l12;
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
l12: if (pc[ct]>12||12>=cs) goto l13;
    otherColour[taskId]=cOther1[taskId];
    
l13: if (pc[ct]>13||13>=cs) goto l14;
    myColour[taskId]=complementary_colour(myColour[taskId], otherColour[taskId]);
l14:
 return;
}
//yellow, blue, red, blue, yellow, blue

void t_five(){
    static int taskId=5;
    static int taskIdEligible;
    static int index;
l0: if (pc[ct]>0||0>=cs) goto l1;
    myColour[taskId]=YELLOW;
l1: if (pc[ct]>1||1>=cs) goto l2;
    otherColour[taskId]=0;
l2: if (pc[ct]>2||2>=cs) goto l3;
    lock(taskId);
l3: if (pc[ct]>3||3>=cs) goto l4;
    index=1;
    evaluate_barrier(index);
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
l4: if (pc[ct]>4||4>=cs) goto l5;
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
l5: if (pc[ct]>5||5>=cs) goto l6;
        remove_entry_queue(taskId, index);
    }
__CPROVER_assume(cs>=6);

l6: if (pc[ct]>6||6>=cs) goto l7;
    //run_entry_body(taskId,index);
    index=1;
    c1[taskId]=myColour[taskId];
    if (index == 1) { //Request
        if (firstCall){
            aColour=c1[taskId];
            firstCall=FALSE;
            
            //requeue(taskId, 2); //requeue Assign
            index = 2;
            evaluate_barrier(index);
            if (tabBarrier[index]==FALSE){
                add_entry_queue(taskId, index);
                unlock(taskId);
l7: if (pc[ct]>7||7>=cs) goto l8;
                __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
l8: if (pc[ct]>8||8>=cs) goto l9;
                remove_entry_queue(taskId, index);
            }
__CPROVER_assume(cs>=9);

            //run_entry_body(taskId,index);
l9: if (pc[ct]>9||9>=cs) goto l10;
            cOther1[taskId]=bColour;
        }else{
__CPROVER_assume(cs>=10);

l10: if (pc[ct]>10||10>=cs) goto l11;
            bColour=c1[taskId];
            cOther1[taskId]=aColour;
            firstCall=TRUE;
            //@controlend
        }
__CPROVER_assume(cs>=11);

    }
__CPROVER_assume(cs>=11);

l11: if (pc[ct]>11||11>=cs) goto l12;
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
l12: if (pc[ct]>12||12>=cs) goto l13;
    otherColour[taskId]=cOther1[taskId];
    
l13: if (pc[ct]>13||13>=cs) goto l14;
    myColour[taskId]=complementary_colour(myColour[taskId], otherColour[taskId]);
l14:
 return;
}

//yellow, blue, red, blue, yellow, blue

void t_six(){
    static int taskId=6;
    static int taskIdEligible;
    static int index;
l0: if (pc[ct]>0||0>=cs) goto l1;
    myColour[taskId]=YELLOW;
l1: if (pc[ct]>1||1>=cs) goto l2;
    otherColour[taskId]=0;
l2: if (pc[ct]>2||2>=cs) goto l3;
    lock(taskId);
l3: if (pc[ct]>3||3>=cs) goto l4;
    index=1;
    evaluate_barrier(index);
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
l4: if (pc[ct]>4||4>=cs) goto l5;
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
l5: if (pc[ct]>5||5>=cs) goto l6;
        remove_entry_queue(taskId, index);
    }
__CPROVER_assume(cs>=6);

l6: if (pc[ct]>6||6>=cs) goto l7;
    //run_entry_body(taskId,index);
    index=1;
    c1[taskId]=myColour[taskId];
    if (index == 1) { //Request
        if (firstCall){
            aColour=c1[taskId];
            firstCall=FALSE;
            
            //requeue(taskId, 2); //requeue Assign
            index = 2;
            evaluate_barrier(index);
            if (tabBarrier[index]==FALSE){
                add_entry_queue(taskId, index);
                unlock(taskId);
l7: if (pc[ct]>7||7>=cs) goto l8;
                __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
l8: if (pc[ct]>8||8>=cs) goto l9;
                remove_entry_queue(taskId, index);
            }
__CPROVER_assume(cs>=9);

            //run_entry_body(taskId,index);
l9: if (pc[ct]>9||9>=cs) goto l10;
            cOther1[taskId]=bColour;
        }else{
__CPROVER_assume(cs>=10);

l10: if (pc[ct]>10||10>=cs) goto l11;
            bColour=c1[taskId];
            cOther1[taskId]=aColour;
            firstCall=TRUE;
            //@controlend
        }
__CPROVER_assume(cs>=11);

    }
__CPROVER_assume(cs>=11);

l11: if (pc[ct]>11||11>=cs) goto l12;
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
l12: if (pc[ct]>12||12>=cs) goto l13;
    otherColour[taskId]=cOther1[taskId];
    
l13: if (pc[ct]>13||13>=cs) goto l14;
    myColour[taskId]=complementary_colour(myColour[taskId], otherColour[taskId]);
l14:
 return;
}