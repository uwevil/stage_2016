void t_one(){
    static int taskId=1;
    static int taskIdEligible;
    static int my_peer = 0;
    
l0: if (pc[ct]>0||0>=cs) goto l1;
    static int i = 1;
    // for (i = 1; i<=nb_request; i++){
l1: if (pc[ct]>1||1>=cs) goto l2;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
l2: if (pc[ct]>2||2>=cs) goto l3;
    static int score=0;
    //  while (score!=2) {
l3: if (pc[ct]>3||3>=cs) goto l4;
    x1[taskId]=x3[taskId];
l4: if (pc[ct]>4||4>=cs) goto l5;
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
l5: if (pc[ct]>5||5>=cs) goto l6;
    static int appel = 2;
l6: if (pc[ct]>6||6>=cs) goto l7;
    static int index = 1;
l7: if (pc[ct]>7||7>=cs) goto l8;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
__CPROVER_assume(cs>=8);

        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
__CPROVER_assume(cs>=8);

        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
l8: if (pc[ct]>8||8>=cs) goto l9;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
l9: if (pc[ct]>9||9>=cs) goto l10;
            remove_entry_queue2(taskId, index);
        }
__CPROVER_assume(cs>=10);

        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
l10: if (pc[ct]>10||10>=cs) goto l11;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
l11: if (pc[ct]>11||11>=cs) goto l12;
                    remove_entry_queue2(taskId, index);
                }
__CPROVER_assume(cs>=12);

                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && First_Call){
__CPROVER_assume(cs>=12);

                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
l12: if (pc[ct]>12||12>=cs) goto l13;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
l13: if (pc[ct]>13||13>=cs) goto l14;
                    remove_entry_queue2(taskId, index);
                }
__CPROVER_assume(cs>=14);

                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && !First_Call){
__CPROVER_assume(cs>=14);

                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }else if (score1[taskId]=1 && First_Call){
__CPROVER_assume(cs>=14);

                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
__CPROVER_assume(cs>=14);

        }else if (index == 2){ //Wait
__CPROVER_assume(cs>=14);

        }
__CPROVER_assume(cs>=14);

        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
__CPROVER_assume(cs>=14);

    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 3;
        goto end;
    }
__CPROVER_assume(cs>=14);

    //  }
l14: if (pc[ct]>14||14>=cs) goto l15;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l15: if (pc[ct]>15||15>=cs) goto l16;
    my_peer=return3[taskId];
l16: if (pc[ct]>16||16>=cs) goto l17;
    if (my_peer==taskId){
l17: if (pc[ct]>17||17>=cs) goto l18;
        cs = size[taskId];
l18: if (pc[ct]>18||18>=cs) goto l19;
        goto end;
    }
__CPROVER_assume(cs>=19);

l19: if (pc[ct]>19||19>=cs) goto end;
    test[taskId]=my_peer;
    //--------------------------
end:
 return;
}