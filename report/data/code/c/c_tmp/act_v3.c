void t_one(){
    static int taskId = 1;
    static int appel = 1;
    static int index = 1;
    static int taskIdEligible;
    static int taskIdTmp;
    static int i = 0;
    static int myClaim = 0;
    static int fullAllocation = 0;
    //@begin
    i = 1;
    myClaim = (5 - i) % 5;
    //protected_object_call(taskId, 2, 1);
    y[taskId] = myClaim;
    appel = 2;
    index = 1;
    
    lock(taskId);
    
    //@controlBegin
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        //@controlAssume
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        //@controlAssume
        //run_entry_call(taskId, index);
        // int taskIdEligible;
        evaluate_barrier(index);
        
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
            //@controlEnd
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
            remove_entry_queue(taskId, index);
        }
        //@controlAssume
        //run_entry_body(taskId,index);
        //@controlLabel
        //@controlBegin
        if (index == 1) { //Request
            stock = stock - y[taskId];
            if (stock < 0){
                //requeue(taskId, 2, FALSE); //requeue Assign
                static int with_abort = FALSE;
                index = 2;
                evaluate_barrier(index);
                
                if (tabBarrier[index]==FALSE && !with_abort){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                    remove_entry_queue(taskId, index);
                }
                //@controlAssume
                //@controlBegin
                if (tabBarrier[index]==TRUE){
                }
                //@controlAssume
            }
            //@controlAssume
        }else if (index == 2){ //Assign
            //@controlAssume
        }
        //@controlAssume
        
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
        //@controlEnd
    }
    //@controlAssume
    fullAllocation = fullAllocation + myClaim;
    
    i = 2;
    myClaim = (5 - i) % 5;
    //protected_object_call(taskId, 2, 1);
    y[taskId] = myClaim;
    appel = 2;
    index = 1;
    
    lock(taskId);
    
    //@controlBegin
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        //@controlAssume
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        //@controlAssume
        //run_entry_call(taskId, index);
        // int taskIdEligible;
        evaluate_barrier(index);
        
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
            //@controlEnd
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
            remove_entry_queue(taskId, index);
        }
        //@controlAssume
        //run_entry_body(taskId,index);
        //@controlLabel
        //@controlBegin
        if (index == 1) { //Request
            stock = stock - y[taskId];
            if (stock < 0){
                //requeue(taskId, 2, FALSE); //requeue Assign
                static int with_abort = FALSE;
                index = 2;
                evaluate_barrier(index);
                
                if (tabBarrier[index]==FALSE && !with_abort){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                    remove_entry_queue(taskId, index);
                }
                //@controlAssume
                //@controlBegin
                if (tabBarrier[index]==TRUE){
                }
                //@controlAssume
            }
            //@controlAssume
        }else if (index == 2){ //Assign
            //@controlAssume
        }
        //@controlAssume
        
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
        //@controlEnd
    }
    //@controlAssume
    fullAllocation = fullAllocation + myClaim;
    
    i = 3;
    myClaim = (5 - i) % 5;
    //protected_object_call(taskId, 2, 1);
    y[taskId] = myClaim;
    appel = 2;
    index = 1;
    
    lock(taskId);
    
    //@controlBegin
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        //@controlAssume
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        //@controlAssume
        //run_entry_call(taskId, index);
        // int taskIdEligible;
        evaluate_barrier(index);
        
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
            //@controlEnd
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
            remove_entry_queue(taskId, index);
        }
        //@controlAssume
        //run_entry_body(taskId,index);
        //@controlLabel
        //@controlBegin
        if (index == 1) { //Request
            stock = stock - y[taskId];
            if (stock < 0){
                //requeue(taskId, 2, FALSE); //requeue Assign
                static int with_abort = FALSE;
                index = 2;
                evaluate_barrier(index);
                
                if (tabBarrier[index]==FALSE && !with_abort){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                    remove_entry_queue(taskId, index);
                }
                //@controlAssume
                //@controlBegin
                if (tabBarrier[index]==TRUE){
                }
                //@controlAssume
            }
            //@controlAssume
        }else if (index == 2){ //Assign
            //@controlAssume
        }
        //@controlAssume
        
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
        //@controlEnd
    }
    //@controlAssume
    fullAllocation = fullAllocation + myClaim;
    
    i = 4;
    myClaim = (5 - i) % 5;
    //protected_object_call(taskId, 2, 1);
    y[taskId] = myClaim;
    appel = 2;
    index = 1;
    
    lock(taskId);
    
    //@controlBegin
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        //@controlAssume
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        //@controlAssume
        //run_entry_call(taskId, index);
        // int taskIdEligible;
        evaluate_barrier(index);
        
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
            //@controlEnd
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
            remove_entry_queue(taskId, index);
        }
        //@controlAssume
        //run_entry_body(taskId,index);
        //@controlLabel
        //@controlBegin
        if (index == 1) { //Request
            stock = stock - y[taskId];
            if (stock < 0){
                //requeue(taskId, 2, FALSE); //requeue Assign
                static int with_abort = FALSE;
                index = 2;
                evaluate_barrier(index);
                
                if (tabBarrier[index]==FALSE && !with_abort){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                    remove_entry_queue(taskId, index);
                }
                //@controlAssume
                //@controlBegin
                if (tabBarrier[index]==TRUE){
                }
                //@controlAssume
            }
            //@controlAssume
        }else if (index == 2){ //Assign
            //@controlAssume
        }
        //@controlAssume
        
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
        //@controlEnd
    }
    //@controlAssume
    fullAllocation = fullAllocation + myClaim;
    
    myClaim = fullAllocation;
    //protected_object_call(taskId, 1, 1);
    y[taskId] = myClaim;
    appel = 1;
    index = 1;
    
    lock(taskId);
    
    //@controlBegin
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        //@controlAssume
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        //@controlAssume
        //run_entry_call(taskId, index);
        // int taskIdEligible;
        evaluate_barrier(index);
        
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
            //@controlEnd
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
            remove_entry_queue(taskId, index);
        }
        //@controlAssume
        //run_entry_body(taskId,index);
        //@controlLabel
        //@controlBegin
        if (index == 1) { //Request
            stock = stock - y[taskId];
            if (stock < 0){
                //requeue(taskId, 2, FALSE); //requeue Assign
                static int with_abort = FALSE;
                index = 2;
                evaluate_barrier(index);
                
                if (tabBarrier[index]==FALSE && !with_abort){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                    //@controlEnd
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                    remove_entry_queue(taskId, index);
                }
                //@controlAssume
                //@controlBegin
                if (tabBarrier[index]==TRUE){
                    //run_entry_body(taskId,index);
                    index = 2;
                    cs = ?;
                    goto end;
                }
                //@controlAssume
            }
            //@controlAssume
        }else if (index == 2){ //Assign
            //@controlAssume
        }
        //@controlAssume
        
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
        //@controlEnd
    }
    //@controlAssume
    fullAllocation = 0;
    //@end
}