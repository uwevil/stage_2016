void t_one(){
l0: if (pc[ct]>0||0>=cs) goto l1;
    static int taskId = 1;
l1: if (pc[ct]>1||1>=cs) goto l2;
    static int other = 0;
l2: if (pc[ct]>2||2>=cs) goto l3;
    other = (taskId % NB_TASK) + 1;
l3: if (pc[ct]>3||3>=cs) goto l4;
    flag[taskId] = TRUE;
l4: if (pc[ct]>4||4>=cs) goto l5;
    turn = other;
    //while (flag[other] && turn == other) {}
l5: if (pc[ct]>5||5>=cs) goto l6;
    __CPROVER_assume(!(flag[other] && turn == other));
l6: if (pc[ct]>6||6>=cs) goto l7;
    static ax;
l7: if (pc[ct]>7||7>=cs) goto l8;
    ax = x;
l8: if (pc[ct]>8||8>=cs) goto l9;
    ax = ax + 1;
l9: if (pc[ct]>9||9>=cs) goto l10;
    x = ax;
l10: if (pc[ct]>10||10>=cs) goto l11;
    flag[taskId] = FALSE;
l11:
 return;
}

void t_two(){
l0: if (pc[ct]>0||0>=cs) goto l1;
    static int taskId = 2;
l1: if (pc[ct]>1||1>=cs) goto l2;
    static int other = 0;
l2: if (pc[ct]>2||2>=cs) goto l3;
    other = (taskId % NB_TASK) + 1;
l3: if (pc[ct]>3||3>=cs) goto l4;
    flag[taskId] = TRUE;
l4: if (pc[ct]>4||4>=cs) goto l5;
    turn = other;
    //while (flag[other] && turn == other) {}
l5: if (pc[ct]>5||5>=cs) goto l6;
    __CPROVER_assume(!(flag[other] && turn == other));
l6: if (pc[ct]>6||6>=cs) goto l7;
    static int bx;
l7: if (pc[ct]>7||7>=cs) goto l8;
    bx = x;
l8: if (pc[ct]>8||8>=cs) goto l9;
    bx = bx - 1;
l9: if (pc[ct]>9||9>=cs) goto l10;
    x = bx;
l10: if (pc[ct]>10||10>=cs) goto l11;
    flag[taskId] = FALSE;
l11:
 return;
}