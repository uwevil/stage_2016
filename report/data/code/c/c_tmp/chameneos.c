//yellow, blue, red, blue, yellow, blue

void t_one(){
    static int taskId=1;
    static int taskIdEligible;
    static int index;
//@begin
    myColour[taskId]=YELLOW;
    otherColour[taskId]=0;
    lock(taskId);
//@controlBegin
    index=1;
    evaluate_barrier(index);
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
//@controlEnd
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        remove_entry_queue(taskId, index);
    }
//@controlAssume
//@controlBegin
    //run_entry_body(taskId,index);
    index=1;
    c1[taskId]=myColour[taskId];
    if (index == 1) { //Request
        if (firstCall){
            aColour=c1[taskId];
            firstCall=FALSE;
            
            //requeue(taskId, 2); //requeue Assign
            index = 2;
            evaluate_barrier(index);
            if (tabBarrier[index]==FALSE){
                add_entry_queue(taskId, index);
                unlock(taskId);
//@controlEnd
                __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                remove_entry_queue(taskId, index);
            }
//@controlAssume
            //run_entry_body(taskId,index);
            cOther1[taskId]=bColour;
        }else{
//@controlAssume
//@controlBegin
            bColour=c1[taskId];
            cOther1[taskId]=aColour;
            firstCall=TRUE;
//@controlend
        }
//@controlAssume
    }
//@controlAssume
//@controlBegin
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
//@controlEnd
    otherColour[taskId]=cOther1[taskId];
    
    myColour[taskId]=complementary_colour(myColour[taskId], otherColour[taskId]);
//@end
}

//yellow, blue, red, blue, yellow, blue

void t_two(){
    static int taskId=2;
    static int taskIdEligible;
    static int index;
    //@begin
    myColour[taskId]=YELLOW;
    otherColour[taskId]=0;
    lock(taskId);
    //@controlBegin
    index=1;
    evaluate_barrier(index);
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
        //@controlEnd
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        remove_entry_queue(taskId, index);
    }
    //@controlAssume
    //@controlBegin
    //run_entry_body(taskId,index);
    index=1;
    c1[taskId]=myColour[taskId];
    if (index == 1) { //Request
        if (firstCall){
            aColour=c1[taskId];
            firstCall=FALSE;
            
            //requeue(taskId, 2); //requeue Assign
            index = 2;
            evaluate_barrier(index);
            if (tabBarrier[index]==FALSE){
                add_entry_queue(taskId, index);
                unlock(taskId);
                //@controlEnd
                __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                remove_entry_queue(taskId, index);
            }
            //@controlAssume
            //run_entry_body(taskId,index);
            cOther1[taskId]=bColour;
        }else{
            //@controlAssume
            //@controlBegin
            bColour=c1[taskId];
            cOther1[taskId]=aColour;
            firstCall=TRUE;
            //@controlend
        }
        //@controlAssume
    }
    //@controlAssume
    //@controlBegin
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
    //@controlEnd
    otherColour[taskId]=cOther1[taskId];
    
    myColour[taskId]=complementary_colour(myColour[taskId], otherColour[taskId]);
    //@end
}

//yellow, blue, red, blue, yellow, blue

void t_three(){
    static int taskId=3;
    static int taskIdEligible;
    static int index;
    //@begin
    myColour[taskId]=YELLOW;
    otherColour[taskId]=0;
    lock(taskId);
    //@controlBegin
    index=1;
    evaluate_barrier(index);
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
        //@controlEnd
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        remove_entry_queue(taskId, index);
    }
    //@controlAssume
    //@controlBegin
    //run_entry_body(taskId,index);
    index=1;
    c1[taskId]=myColour[taskId];
    if (index == 1) { //Request
        if (firstCall){
            aColour=c1[taskId];
            firstCall=FALSE;
            
            //requeue(taskId, 2); //requeue Assign
            index = 2;
            evaluate_barrier(index);
            if (tabBarrier[index]==FALSE){
                add_entry_queue(taskId, index);
                unlock(taskId);
                //@controlEnd
                __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                remove_entry_queue(taskId, index);
            }
            //@controlAssume
            //run_entry_body(taskId,index);
            cOther1[taskId]=bColour;
        }else{
            //@controlAssume
            //@controlBegin
            bColour=c1[taskId];
            cOther1[taskId]=aColour;
            firstCall=TRUE;
            //@controlend
        }
        //@controlAssume
    }
    //@controlAssume
    //@controlBegin
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
    //@controlEnd
    otherColour[taskId]=cOther1[taskId];
    
    myColour[taskId]=complementary_colour(myColour[taskId], otherColour[taskId]);
    //@end
}
//yellow, blue, red, blue, yellow, blue

void t_four(){
    static int taskId=4;
    static int taskIdEligible;
    static int index;
    //@begin
    myColour[taskId]=YELLOW;
    otherColour[taskId]=0;
    lock(taskId);
    //@controlBegin
    index=1;
    evaluate_barrier(index);
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
        //@controlEnd
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        remove_entry_queue(taskId, index);
    }
    //@controlAssume
    //@controlBegin
    //run_entry_body(taskId,index);
    index=1;
    c1[taskId]=myColour[taskId];
    if (index == 1) { //Request
        if (firstCall){
            aColour=c1[taskId];
            firstCall=FALSE;
            
            //requeue(taskId, 2); //requeue Assign
            index = 2;
            evaluate_barrier(index);
            if (tabBarrier[index]==FALSE){
                add_entry_queue(taskId, index);
                unlock(taskId);
                //@controlEnd
                __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                remove_entry_queue(taskId, index);
            }
            //@controlAssume
            //run_entry_body(taskId,index);
            cOther1[taskId]=bColour;
        }else{
            //@controlAssume
            //@controlBegin
            bColour=c1[taskId];
            cOther1[taskId]=aColour;
            firstCall=TRUE;
            //@controlend
        }
        //@controlAssume
    }
    //@controlAssume
    //@controlBegin
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
    //@controlEnd
    otherColour[taskId]=cOther1[taskId];
    
    myColour[taskId]=complementary_colour(myColour[taskId], otherColour[taskId]);
    //@end
}
//yellow, blue, red, blue, yellow, blue

void t_five(){
    static int taskId=5;
    static int taskIdEligible;
    static int index;
    //@begin
    myColour[taskId]=YELLOW;
    otherColour[taskId]=0;
    lock(taskId);
    //@controlBegin
    index=1;
    evaluate_barrier(index);
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
        //@controlEnd
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        remove_entry_queue(taskId, index);
    }
    //@controlAssume
    //@controlBegin
    //run_entry_body(taskId,index);
    index=1;
    c1[taskId]=myColour[taskId];
    if (index == 1) { //Request
        if (firstCall){
            aColour=c1[taskId];
            firstCall=FALSE;
            
            //requeue(taskId, 2); //requeue Assign
            index = 2;
            evaluate_barrier(index);
            if (tabBarrier[index]==FALSE){
                add_entry_queue(taskId, index);
                unlock(taskId);
                //@controlEnd
                __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                remove_entry_queue(taskId, index);
            }
            //@controlAssume
            //run_entry_body(taskId,index);
            cOther1[taskId]=bColour;
        }else{
            //@controlAssume
            //@controlBegin
            bColour=c1[taskId];
            cOther1[taskId]=aColour;
            firstCall=TRUE;
            //@controlend
        }
        //@controlAssume
    }
    //@controlAssume
    //@controlBegin
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
    //@controlEnd
    otherColour[taskId]=cOther1[taskId];
    
    myColour[taskId]=complementary_colour(myColour[taskId], otherColour[taskId]);
    //@end
}

//yellow, blue, red, blue, yellow, blue

void t_six(){
    static int taskId=6;
    static int taskIdEligible;
    static int index;
    //@begin
    myColour[taskId]=YELLOW;
    otherColour[taskId]=0;
    lock(taskId);
    //@controlBegin
    index=1;
    evaluate_barrier(index);
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
        //@controlEnd
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        remove_entry_queue(taskId, index);
    }
    //@controlAssume
    //@controlBegin
    //run_entry_body(taskId,index);
    index=1;
    c1[taskId]=myColour[taskId];
    if (index == 1) { //Request
        if (firstCall){
            aColour=c1[taskId];
            firstCall=FALSE;
            
            //requeue(taskId, 2); //requeue Assign
            index = 2;
            evaluate_barrier(index);
            if (tabBarrier[index]==FALSE){
                add_entry_queue(taskId, index);
                unlock(taskId);
                //@controlEnd
                __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                remove_entry_queue(taskId, index);
            }
            //@controlAssume
            //run_entry_body(taskId,index);
            cOther1[taskId]=bColour;
        }else{
            //@controlAssume
            //@controlBegin
            bColour=c1[taskId];
            cOther1[taskId]=aColour;
            firstCall=TRUE;
            //@controlend
        }
        //@controlAssume
    }
    //@controlAssume
    //@controlBegin
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
    //@controlEnd
    otherColour[taskId]=cOther1[taskId];
    
    myColour[taskId]=complementary_colour(myColour[taskId], otherColour[taskId]);
    //@end
}