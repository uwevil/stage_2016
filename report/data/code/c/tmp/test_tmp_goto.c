#include <stdio.h>
#include <assert.h>
 
int main () {

   /* local variable definition */
   int a = 10;

   /* do loop execution */
   l1:do {
   
      if( a == 15) {
         /* skip the iteration */
         a = a + 1;
         goto l1;
      }
		
      printf("value of a: %d\n", a);
      a++;
     
   }while( a < 20 );
 
    assert(a==21);
    
   return 0;
}