	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	_pthread_create
	.align	4, 0x90
_pthread_create:                        ## @pthread_create
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp2:
	.cfi_def_cfa_register %rbp
	xorl	%eax, %eax
	movq	_arg@GOTPCREL(%rip), %r8
	movq	_active@GOTPCREL(%rip), %r9
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movl	%ecx, -28(%rbp)
	movslq	-28(%rbp), %rdx
	movl	$1, (%r9,%rdx,4)
	movl	-28(%rbp), %ecx
	movslq	-28(%rbp), %rdx
	movl	%ecx, (%r8,%rdx,4)
	movl	-28(%rbp), %ecx
	movq	-8(%rbp), %rdx
	movl	%ecx, (%rdx)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_pthread_mutex_init
	.align	4, 0x90
_pthread_mutex_init:                    ## @pthread_mutex_init
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp3:
	.cfi_def_cfa_offset 16
Ltmp4:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp5:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rdi
	movl	$-1, (%rdi)
	movl	-4(%rbp), %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_pthread_mutex_destroy
	.align	4, 0x90
_pthread_mutex_destroy:                 ## @pthread_mutex_destroy
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp6:
	.cfi_def_cfa_offset 16
Ltmp7:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp8:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -16(%rbp)
	movl	%esi, -20(%rbp)
	movq	-16(%rbp), %rdi
	movl	$-2, (%rdi)
	movl	-4(%rbp), %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_pthread_mutex_lock
	.align	4, 0x90
_pthread_mutex_lock:                    ## @pthread_mutex_lock
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp9:
	.cfi_def_cfa_offset 16
Ltmp10:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp11:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -16(%rbp)
	movl	%esi, -20(%rbp)
	movl	-20(%rbp), %esi
	movq	-16(%rbp), %rdi
	movl	%esi, (%rdi)
	movl	-4(%rbp), %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_pthread_mutex_unlock
	.align	4, 0x90
_pthread_mutex_unlock:                  ## @pthread_mutex_unlock
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp12:
	.cfi_def_cfa_offset 16
Ltmp13:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp14:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -16(%rbp)
	movl	%esi, -20(%rbp)
	movq	-16(%rbp), %rdi
	movl	$-1, (%rdi)
	movl	-4(%rbp), %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_add_external_queue
	.align	4, 0x90
_add_external_queue:                    ## @add_external_queue
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp15:
	.cfi_def_cfa_offset 16
Ltmp16:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp17:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	$1, -12(%rbp)
	movl	$0, -8(%rbp)
LBB5_1:                                 ## =>This Inner Loop Header: Depth=1
	cmpl	$1, -8(%rbp)
	jge	LBB5_4
## BB#2:                                ##   in Loop: Header=BB5_1 Depth=1
	movl	$2, %eax
	movl	-12(%rbp), %ecx
	addl	$1, %ecx
	movl	%eax, -16(%rbp)         ## 4-byte Spill
	movl	%ecx, %eax
	cltd
	movl	-16(%rbp), %ecx         ## 4-byte Reload
	idivl	%ecx
	movl	%edx, -12(%rbp)
## BB#3:                                ##   in Loop: Header=BB5_1 Depth=1
	movl	-8(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -8(%rbp)
	jmp	LBB5_1
LBB5_4:
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_func
	.align	4, 0x90
_func:                                  ## @func
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp18:
	.cfi_def_cfa_offset 16
Ltmp19:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp20:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	leaq	L_.str(%rip), %rdi
	movl	_func.x(%rip), %eax
	addl	$1, %eax
	movl	%eax, _func.x(%rip)
	movl	_func.x(%rip), %esi
	movb	$0, %al
	callq	_printf
	movl	%eax, -4(%rbp)          ## 4-byte Spill
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_func1
	.align	4, 0x90
_func1:                                 ## @func1
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp21:
	.cfi_def_cfa_offset 16
Ltmp22:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp23:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	leaq	L_.str(%rip), %rdi
	movl	_func1.x(%rip), %eax
	addl	$1, %eax
	movl	%eax, _func1.x(%rip)
	movl	_func1.x(%rip), %esi
	movb	$0, %al
	callq	_printf
	movl	%eax, -4(%rbp)          ## 4-byte Spill
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp24:
	.cfi_def_cfa_offset 16
Ltmp25:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp26:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	$0, -4(%rbp)
	callq	_func
	callq	_func
	callq	_func
	callq	_func1
	callq	_func1
	callq	_func1
	movl	$1, %edi
	callq	_add_external_queue
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__const
	.globl	_NB_TASK                ## @NB_TASK
	.align	2
_NB_TASK:
	.long	2                       ## 0x2

	.globl	_NB_ENTRY               ## @NB_ENTRY
	.align	2
_NB_ENTRY:
	.long	2                       ## 0x2

	.globl	_NB_PROCEDURE           ## @NB_PROCEDURE
	.align	2
_NB_PROCEDURE:
	.long	0                       ## 0x0

	.globl	_NB_FUNCTION            ## @NB_FUNCTION
	.align	2
_NB_FUNCTION:
	.long	0                       ## 0x0

	.globl	_TRUE                   ## @TRUE
	.align	2
_TRUE:
	.long	1                       ## 0x1

	.globl	_FALSE                  ## @FALSE
	.align	2
_FALSE:
	.long	0                       ## 0x0

	.globl	_FREE                   ## @FREE
	.align	2
_FREE:
	.long	4294967295              ## 0xffffffff

	.globl	_DESTROY                ## @DESTROY
	.align	2
_DESTROY:
	.long	4294967294              ## 0xfffffffe

	.globl	_currentPositionExternalQueue ## @currentPositionExternalQueue
.zerofill __DATA,__common,_currentPositionExternalQueue,4,2
	.globl	_isLock                 ## @isLock
.zerofill __DATA,__common,_isLock,4,2
	.globl	_currentEntryIdEligible ## @currentEntryIdEligible
.zerofill __DATA,__common,_currentEntryIdEligible,4,2
	.comm	_active,12,2            ## @active
	.comm	_arg,12,2               ## @arg
.zerofill __DATA,__bss,_func.x,4,2      ## @func.x
	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"%d\n"

	.section	__DATA,__data
	.align	2                       ## @func1.x
_func1.x:
	.long	5                       ## 0x5

	.comm	_cs,4,2                 ## @cs
	.comm	_ct,4,2                 ## @ct
	.comm	_pc,12,2                ## @pc
	.comm	_size,12,2              ## @size
	.comm	_m,4,2                  ## @m
	.comm	_tabStatus,8,2          ## @tabStatus
	.comm	_tabExternalQueue,8,2   ## @tabExternalQueue
	.comm	_tabEntryQueue,16,4     ## @tabEntryQueue
	.comm	_tabCurrentPositionEntryQueue,8,2 ## @tabCurrentPositionEntryQueue
	.comm	_tabBarrier,8,2         ## @tabBarrier

.subsections_via_symbols
