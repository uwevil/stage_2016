#include <stdio.h>
#include "protected_object.h"

void func() {
	static int x = 0;
   // x = 0;
	/* x is initialized only once across five calls of func() and
	  the variable will get incremented five 
	  times after these calls. The final value of x will be 5. */
	x++;
	printf("%d\n", x); // outputs the value of x
}

void func1() {
	static int x = 5;
   // x = 0;
	/* x is initialized only once across five calls of func() and
	  the variable will get incremented five 
	  times after these calls. The final value of x will be 5. */
	x++;
	printf("%d\n", x); // outputs the value of x
}

int main() { //int argc, char *argv[] inside the main is optional in the particular program
	func(); // prints 1
	func(); // prints 2
	func(); // prints 3
	func1(); // prints 6
	func1(); // prints 7
    func1(); // prints 8
    
    add_external_queue(1);
    
	return 0;
}