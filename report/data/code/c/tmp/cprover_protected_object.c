#include <stdio.h>
#include <assert.h>
#include <pthread.h>

#define NB_TASK 2 //without main task
#define NB_ENTRY 2
#define NB_PROCEDURE 0
#define NB_FUNCTION 0

#define K 20


/*-------------------------------------------------------------*/
const int TRUE = 1;
const int FALSE = 0;

const int FREE = -1;
const int DESTROY = -2;

int active[NB_TASK+1] = {TRUE, FALSE, FALSE};
int cs, ct, r;
int pc[NB_TASK+1];
int size[NB_TASK+1];

int arg[NB_TASK+1];

int m = FREE;

/*-------------------------------------------------------------*/
int tabStatus[NB_TASK+1] = {TRUE, TRUE, TRUE};
int tabExternalQueue[NB_TASK+1] = {0,0,0};
int currentPositionExternalQueue = 1;

int tabEntryQueue[NB_ENTRY+1][NB_TASK+1] = {{0,0,0}, {0,0,0}, {0,0,0}};
int tabCurrentPositionEntryQueue[NB_ENTRY+1] = {1,1,1};

int tabBarrier[NB_ENTRY+1] = {FALSE, FALSE, FALSE};

int isLock = FALSE;

static int x = 0;

void add_external_queue(int taskId){
    int i, tmp;
    tmp = currentPositionExternalQueue;
    for (i = 0; i < NB_TASK; i++){
        if (tabExternalQueue[tmp] == 0){
            tabExternalQueue[tmp] = taskId;
            break;
        }
        tmp = (tmp % NB_TASK)+1;
    }
}

void remove_external_queue(){
    tabExternalQueue[currentPositionExternalQueue] = 0;
    currentPositionExternalQueue = (currentPositionExternalQueue % NB_TASK)+1;
}

void evaluate_barrier(int entryId){
    if (entryId == 1){
        tabBarrier[entryId] = x < 10;
    }else if (entryId == 2){
        tabBarrier[entryId] = x > 0;
    }
}

void reevaluate_barrier(){
    tabBarrier[1] = x < 10;
    tabBarrier[2] = x > 0;
}

void add_entry_queue(int taskId, int entryId){
    int i, tmp;
    tmp = tabCurrentPositionEntryQueue[entryId];
    for (i = 0; i < NB_TASK; i++){
        if (tabEntryQueue[entryId][tmp] == 0){
            tabEntryQueue[entryId][tmp] = taskId;
            break;
        }
        tmp = (tmp % NB_TASK)+1;
    }
}

void remove_entry_queue(int entryId){
    tabEntryQueue[entryId][tabCurrentPositionEntryQueue[entryId]] = 0;
    tabCurrentPositionEntryQueue[entryId] = (tabCurrentPositionEntryQueue[entryId] % NB_TASK)+1;
}

int current_entry_eligible(){ //return entryId
    int i;
    for (i = 1; i <= NB_ENTRY; i++){
        if (tabBarrier[i] && tabEntryQueue[i][tabCurrentPositionEntryQueue[i]] != 0){
            return i;
        }
    }
    return 0;
}


void t_one(){
    static int taskId = 1;
    static int entryId = 1;
    static int x_tmp = 0;
    static int currentEntryIdEligible;
    static int taskIdTmp;

//@begin
    //pthread_mutex_lock(&m, taskId);
    __CPROVER_atomic_begin();
    __CPROVER_assume(m==FREE);
    m = taskId;
    __CPROVER_atomic_end();
    add_external_queue(taskId);
    //pthread_mutex_unlock(&m, taskId);
    __CPROVER_atomic_begin();
    __CPROVER_assert(m==taskId, "error unlock mutex");
    m = FREE;
    __CPROVER_atomic_end();
    __CPROVER_assume(!(isLock || tabExternalQueue[currentPositionExternalQueue] != taskId));
    isLock = TRUE;
    remove_external_queue();
    evaluate_barrier(entryId);
    if (tabBarrier[entryId]==FALSE){
        add_entry_queue(taskId, entryId);

        tabStatus[taskId] = FALSE;
        isLock = FALSE;
        __CPROVER_assume(!(tabStatus[taskId]==FALSE));
    }else{
//@controlAssume
        //run_entry_body(taskId, entryId);
        if (entryId == 1) {
            x_tmp = x;
            x_tmp = x_tmp + 1;
            x = x_tmp;
        }else if (entryId == 2){
//@controlAssume
            x_tmp = x;
            x_tmp = x_tmp - 1;
            x = x_tmp;
        }
//@controlAssume
      //  for(;;){
            reevaluate_barrier();
            currentEntryIdEligible = current_entry_eligible();
            if (currentEntryIdEligible != 0){
                taskIdTmp = tabEntryQueue[currentEntryIdEligible][tabCurrentPositionEntryQueue[currentEntryIdEligible]];
        //run_entry_body(taskIdTmp, currentEntryIdEligible);
                if (currentEntryIdEligible == 1) {
                    x_tmp = x;
                    x_tmp = x_tmp + 1;
                    x = x_tmp;
                }else if (currentEntryIdEligible == 2){
//@controlAssume
                    x_tmp = x;
                    x_tmp = x_tmp - 1;
                    x = x_tmp;
                }
//@controlAssume
                remove_entry_queue(currentEntryIdEligible);
                tabStatus[taskIdTmp] = TRUE;
            }
//@controlAssume
        /*----------*/
            reevaluate_barrier();
            currentEntryIdEligible = current_entry_eligible();
            if (currentEntryIdEligible != 0){
                taskIdTmp = tabEntryQueue[currentEntryIdEligible][tabCurrentPositionEntryQueue[currentEntryIdEligible]];
        //run_entry_body(taskIdTmp, currentEntryIdEligible);
                if (currentEntryIdEligible == 1) {
                    x_tmp = x;
                    x_tmp = x_tmp + 1;
                    x = x_tmp;
                }else if (currentEntryIdEligible == 2){
//@controlAssume
                    x_tmp = x;
                    x_tmp = x_tmp - 1;
                    x = x_tmp;
                }
//@controlAssume
                remove_entry_queue(currentEntryIdEligible);
                tabStatus[taskIdTmp] = TRUE;
            }
//@controlAssume
    //    }
        isLock = FALSE;
    }
//@controlAssume
//@end
}

void t_two(){
    static int taskId = 2;
    static int entryId = 2;
    static int x_tmp = 0;
    static int currentEntryIdEligible;
    static int taskIdTmp;

//@begin
    //pthread_mutex_lock(&m, taskId);
    __CPROVER_atomic_begin();
    __CPROVER_assume(m==FREE);
    m = taskId;
    __CPROVER_atomic_end();
    add_external_queue(taskId);
    //pthread_mutex_unlock(&m, taskId);
    __CPROVER_atomic_begin();
    __CPROVER_assert(m==taskId, "error unlock mutex");
    m = FREE;
    __CPROVER_atomic_end();
    __CPROVER_assume(!(isLock || tabExternalQueue[currentPositionExternalQueue] != taskId));
    isLock = TRUE;
    remove_external_queue();
    evaluate_barrier(entryId);
    if (tabBarrier[entryId]==FALSE){
        add_entry_queue(taskId, entryId);

        tabStatus[taskId] = FALSE;
        isLock = FALSE;
        __CPROVER_assume(!(tabStatus[taskId]==FALSE));
    }else{
//@controlAssume
        //run_entry_body(taskId, entryId);
        if (entryId == 1) {
            x_tmp = x;
            x_tmp = x_tmp + 1;
            x = x_tmp;
        }else if (entryId == 2){
//@controlAssume
            x_tmp = x;
            x_tmp = x_tmp - 1;
            x = x_tmp;
        }
//@controlAssume
      //  for(;;){
            reevaluate_barrier();
            currentEntryIdEligible = current_entry_eligible();
            if (currentEntryIdEligible != 0){
                taskIdTmp = tabEntryQueue[currentEntryIdEligible][tabCurrentPositionEntryQueue[currentEntryIdEligible]];
        //run_entry_body(taskIdTmp, currentEntryIdEligible);
                if (currentEntryIdEligible == 1) {
                    x_tmp = x;
                    x_tmp = x_tmp + 1;
                    x = x_tmp;
                }else if (currentEntryIdEligible == 2){
//@controlAssume
                    x_tmp = x;
                    x_tmp = x_tmp - 1;
                    x = x_tmp;
                }
//@controlAssume
                remove_entry_queue(currentEntryIdEligible);
                tabStatus[taskIdTmp] = TRUE;
            }
//@controlAssume
        /*----------*/
            reevaluate_barrier();
            currentEntryIdEligible = current_entry_eligible();
            if (currentEntryIdEligible != 0){
                taskIdTmp = tabEntryQueue[currentEntryIdEligible][tabCurrentPositionEntryQueue[currentEntryIdEligible]];
        //run_entry_body(taskIdTmp, currentEntryIdEligible);
                if (currentEntryIdEligible == 1) {
                    x_tmp = x;
                    x_tmp = x_tmp + 1;
                    x = x_tmp;
                }else if (currentEntryIdEligible == 2){
//@controlAssume
                    x_tmp = x;
                    x_tmp = x_tmp - 1;
                    x = x_tmp;
                }
//@controlAssume
                remove_entry_queue(currentEntryIdEligible);
                tabStatus[taskIdTmp] = TRUE;
            }
//@controlAssume
    //    }
        isLock = FALSE;
    }
//@controlAssume
//@end
}

int main(){
    pthread_t t1, t2;
    
    pthread_create(&t1, t_one, NULL, 1);
    pthread_create(&t2, t_two, NULL, 2);
    
    pthread_join(t1, 0);
    pthread_join(t2, 0);
    
    __CPROVER_assert(x == 0, "OK x != 0");
    __CPROVER_assert(FALSE, "OK");

    return 0;
}






