#include <stdio.h>
#include <assert.h>

#define TRUE 1
#define FALSE 0
#define FREE -1

#define NB_TASK 2 //without main task
#define NB_ENTRY 0
#define NB_PROCEDURE 0
#define NB_FUNCTION 0

/*----------------------------------*/
int active[NB_TASK+1] =
    {TRUE, FALSE, FALSE};
int cs, ct;
int pc[NB_TASK+1];
int size[NB_TASK+1];

void create(int t, int id){
    active[id] = TRUE;
    t = id;
}
/*----------------------------------*/

static int turn = 1;
static int flag[NB_TASK+1] =
    {FALSE, FALSE, FALSE};

static int x = 0;
int inSC = 0;

void t_one(){
l0: if (pc[ct]>0||0>=cs) goto l1;
    static int taskId = 1;
l1: if (pc[ct]>1||1>=cs) goto l2;
    static int other = 0;
l2: if (pc[ct]>2||2>=cs) goto l3;
    other = (taskId % NB_TASK) + 1;
l3: if (pc[ct]>3||3>=cs) goto l4;
    flag[taskId] = TRUE;
    
l4: if (pc[ct]>4||4>=cs) goto l5;
    turn = other;
    //while(flag[other]&&turn==other){}
l5: if (pc[ct]>5||5>=cs) goto l6;
    __CPROVER_assume(!(flag[other]
                       && turn == other));
    
l6: if (pc[ct]>6||6>=cs) goto l7;
    inSC = inSC+1;
    static ax;
l7: if (pc[ct]>7||7>=cs) goto l8;
    ax = x;
l8: if (pc[ct]>8||8>=cs) goto l9;
    ax = ax + 1;
l9: if (pc[ct]>9||9>=cs) goto l10;
    x = ax;
    
l10: if (pc[ct]>10||10>=cs) goto l11;
    inSC = inSC-1;
    flag[taskId] = FALSE;
l11:
    return;
}

void t_two(){
l0: if (pc[ct]>0||0>=cs) goto l1;
    static int taskId = 2;
l1: if (pc[ct]>1||1>=cs) goto l2;
    static int other = 0;
l2: if (pc[ct]>2||2>=cs) goto l3;
    other = (taskId % NB_TASK) + 1;
l3: if (pc[ct]>3||3>=cs) goto l4;
    flag[taskId] = TRUE;
    
l4: if (pc[ct]>4||4>=cs) goto l5;
    turn = other;
    //while(flag[other]&&turn==other){}
l5: if (pc[ct]>5||5>=cs) goto l6;
    __CPROVER_assume(!(flag[other]
                       && turn == other));
    
l6: if (pc[ct]>6||6>=cs) goto l7;
    inSC = inSC+1;
    static int bx;
l7: if (pc[ct]>7||7>=cs) goto l8;
    bx = x;
l8: if (pc[ct]>8||8>=cs) goto l9;
    bx = bx - 1;
l9: if (pc[ct]>9||9>=cs) goto l10;
    x = bx;
    
l10: if (pc[ct]>10||10>=cs) goto l11;
    inSC = inSC-1;
    flag[taskId] = FALSE;
l11:
    return;
}

void init(){
    pc[0] = 0;
    size[0] = 0;
    
    pc[1] = 0;
    pc[2] = 0;
    size[1] = 11;
    size[2] = 11;
    
    int t1, t2, t3, t4, t5, t6;
    
    create(t1, 1);
    create(t2, 2);
}

int main(){
    init();
    
    for (;;) {
        ct = 1;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs>=pc[ct]
                        &&cs<=size[ct]);
            t_one();
            pc[ct] = cs;
            active[ct]=pc[ct]!=size[ct];
        }
        
        ct = 2;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs>=pc[ct]
                        &&cs<=size[ct]);
            t_two();
            pc[ct] = cs;
            active[ct]=pc[ct]!=size[ct];
        }
        
        __CPROVER_assert(inSC<2,"OK inSC<2");
        
        if (pc[1]==size[1]&&pc[2]==size[2]){
            __CPROVER_assert(FALSE, "OK");
        }
    }
    __CPROVER_assert(FALSE, "KO");
}
