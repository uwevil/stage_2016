#include <stdio.h>
#include <assert.h>

#define TRUE 1
#define FALSE 0
#define FREE -1

#define NB_TASK 4 //without main task

int const nb_request = 6;

/*-------------------------------------------------------------*/
int active[NB_TASK+1] = {TRUE, FALSE, FALSE, FALSE, FALSE};//, FALSE, FALSE, FALSE, FALSE, FALSE};
int cs, ct;
int pc[NB_TASK+1];
int size[NB_TASK+1];
int dead = 0;

void create(int t, int id){
    active[id] = TRUE;
    t = id;
}
/*-------------------------------------------------------------*/
#define NB_ENTRY1 1
#define NB_PROCEDURE1 1
#define NB_FUNCTION1 0

int tabStatus1[NB_TASK+1] = {TRUE, TRUE, TRUE, TRUE, TRUE}; //, TRUE, TRUE, TRUE, TRUE, TRUE};
int tabWaiting1[NB_TASK+1] = {0, 0, 0, 0,0}; //, 0, 0, 0, 0, 0};
int tabBarrier1[NB_ENTRY1+1] = {FALSE, FALSE};

int currentTask1=1;

int tabEntryCount1[NB_ENTRY1+1] = {0,0};
int taskInWait = 0;

int externalLock1 = FREE;

void lock1(int id){
    __CPROVER_assume(externalLock1==FREE);
    externalLock1=id;
}

void transfert1(int id, int newId){
    __CPROVER_assert(externalLock1==id, "OKexternalLock1!=id");
    externalLock1=newId;
}

void unlock1(int id){
    __CPROVER_assert(externalLock1==id, "OKexternalLock1!=id");
    externalLock1=FREE;
}
/*---------------------------------------------------------------------------*/

int free1 = TRUE;

void evaluate_barrier1(int index){
    if (index == 1){ //Acquire
        tabBarrier1[index] = free1;
    }
}

void reevaluate_barrier1(){
    tabBarrier1[1] = free1; //Acquire
}

void setTabStatusOn1(int taskId){
    tabStatus1[taskId] = TRUE;
    taskInWait = taskInWait - 1;
}

void setTabStatusOff1(int taskId){
    tabStatus1[taskId] = FALSE;
    taskInWait = taskInWait + 1;
    __CPROVER_assert(taskInWait<NB_TASK, "OKdeadlock1");
}


void add_entry_queue1(int taskId, int index){
    tabEntryCount1[index] = tabEntryCount1[index]+1;
    tabWaiting1[taskId] = index;
    setTabStatusOff1(taskId);
}

void remove_entry_queue1(int taskId, int index){
    tabEntryCount1[index] = tabEntryCount1[index]-1;
    tabWaiting1[taskId] = 0;
}

int current_task_eligible1(){ //return taskId
    int j;
    for (j=1; j<=NB_TASK;j++){
        if (tabWaiting1[currentTask1]!=0&&tabBarrier1[tabWaiting1[currentTask1]]){
            return currentTask1;
        }
        currentTask1 = (currentTask1 % NB_TASK)+1;
    }
    return 0;
}

void run_entry_body1(int taskId, int index);

void requeue1(int taskId, int index){ //requeue to another or same entry queue
    evaluate_barrier1(index);
    
    if (tabBarrier1[index]==FALSE){
        add_entry_queue1(taskId, index);
        unlock1(taskId);
        __CPROVER_assume(tabStatus1[taskId]&&externalLock1==taskId);
        remove_entry_queue1(taskId, index);
    }
    run_entry_body1(taskId,index);
}

void run_entry_body1(int taskId, int index){
    if (index == 1) { //Acquire
        free1 = FALSE;
    }
}

int check_and_run_any_entry1(){
    reevaluate_barrier1();
    int taskIdEligible1 = current_task_eligible1();
    if (taskIdEligible1==0){
        return FREE;
    }
    setTabStatusOn1(taskIdEligible1);
    return taskIdEligible1;
}

void run_function_call1(taskId, index){
}

void run_procedure_call1(taskId, index){
    int taskIdEligible1;
    if (index == 1){
        free1 = TRUE;
    }
    taskIdEligible1 = check_and_run_any_entry1();
    transfert1(taskId, taskIdEligible1);
}

void run_entry_call1(int taskId, int index){
    int taskIdEligible1;
    evaluate_barrier1(index);
    
    if (tabBarrier1[index]==FALSE){
        add_entry_queue1(taskId, index);
        unlock1(taskId);
        __CPROVER_assume(tabStatus1[taskId]&&externalLock1==taskId);
        remove_entry_queue1(taskId, index);
    }
    run_entry_body1(taskId,index);
    taskIdEligible1 = check_and_run_any_entry1();
    transfert1(taskId, taskIdEligible1);
}

void protected_object_call1(int taskId, int appel, int index){
    lock1(taskId);
    
    if (appel == 0){ //function
        run_function_call1(taskId, index);
    }else if (appel == 1){ //procedure
        run_procedure_call1(taskId, index);
    }else if (appel == 2){ //entry
        run_entry_call1(taskId, index);
    }
}

/*---------------------------------------------------------------------*/

/*-------------------------------------------------------------*/
#define NB_ENTRY2 2
#define NB_PROCEDURE2 0
#define NB_FUNCTION2 0

int tabStatus2[NB_TASK+1] = {TRUE, TRUE, TRUE, TRUE, TRUE}; //, TRUE, TRUE, TRUE, TRUE, TRUE};
int tabWaiting2[NB_TASK+1] = {0, 0, 0, 0, 0}; //, 0, 0, 0, 0, 0};
int tabBarrier2[NB_ENTRY2+1] = {FALSE, FALSE, FALSE};

int currentTask2=1;

int tabEntryCount2[NB_ENTRY2+1] = {0,0,0};
//int taskInWait = 0;

int externalLock2 = FREE;

void lock2(int id){
    __CPROVER_assume(externalLock2==FREE);
    externalLock2=id;
}

void transfert2(int id, int newId){
    __CPROVER_assert(externalLock2==id, "OKexternalLock2!=id");
    externalLock2=newId;
}

void unlock2(int id){
    __CPROVER_assert(externalLock2==id, "OKexternalLock2!=id");
    externalLock2=FREE;
}
/*---------------------------------------------------------------------------*/

#define BLUE 1
#define RED 2
#define YELLOW 3

int First_Call = TRUE;
int MustWait = FALSE;
int a_chameneos, b_chameneos;
int NotifyAll = FALSE;

typedef int colour;

int firstCall = TRUE;
colour aColour;
colour bColour;

int test[NB_TASK+1];

int x1[NB_TASK+1];
int x_other1[NB_TASK+1];
int score1[NB_TASK+1];

int x2[NB_TASK+1];
int x_other2[NB_TASK+1];
int score2[NB_TASK+1];

int x3[NB_TASK+1];
int return3[NB_TASK+1];

void protected_object_call2(int taskId, int appel, int index);

void get_a_peer3(int taskId){
    int score=0;
    while (score!=2) {
        x1[taskId]=x3[taskId];
        protected_object_call2(taskId, 2, 1);
        score=score1[taskId];
    }
    return3[taskId]=x_other1[taskId];
}

/*---------------------------------------------------------------------------*/

void evaluate_barrier2(int index){
    if (index == 1){ //Cooperate
        tabBarrier2[index] = TRUE;
    }else if (index == 2){ //Wait
        tabBarrier2[index] = NotifyAll;
    }
}

void reevaluate_barrier2(){
    tabBarrier2[1] = TRUE; //Cooperate
    tabBarrier2[2] = NotifyAll; //Wait
}

void setTabStatusOn2(int taskId){
    tabStatus2[taskId] = TRUE;
    taskInWait = taskInWait - 1;
}

void setTabStatusOff2(int taskId){
    tabStatus2[taskId] = FALSE;
    taskInWait = taskInWait + 1;
    __CPROVER_assert(taskInWait<NB_TASK, "OKdeadlock2");
}


void add_entry_queue2(int taskId, int index){
    tabEntryCount2[index] = tabEntryCount2[index]+1;
    tabWaiting2[taskId] = index;
    setTabStatusOff2(taskId);
}

void remove_entry_queue2(int taskId, int index){
    tabEntryCount2[index] = tabEntryCount2[index]-1;
    tabWaiting2[taskId] = 0;
}

int current_task_eligible2(){ //return taskId
    int j;
    for (j=1; j<=NB_TASK;j++){
        if (tabWaiting2[currentTask2]!=0&&tabBarrier2[tabWaiting2[currentTask2]]){
            return currentTask2;
        }
        currentTask2 = (currentTask2 % NB_TASK)+1;
    }
    return 0;
}

void run_entry_body2(int taskId, int index);

void requeue2(int taskId, int index){ //requeue to another or same entry queue
    evaluate_barrier2(index);
    
    if (tabBarrier2[index]==FALSE){
        add_entry_queue2(taskId, index);
        unlock2(taskId);
        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        remove_entry_queue2(taskId, index);
    }
    run_entry_body2(taskId,index);
}

void run_entry_body2(int taskId, int index){
    if (index == 1) { //Cooperate
        if (score1[taskId]==0 && MustWait){
            NotifyAll = FALSE;
            requeue2(taskId, 2);
        }
        if (score1[taskId]==0 && First_Call){
            a_chameneos = x1[taskId];
            First_Call = FALSE;
            score1[taskId]=1;
            NotifyAll = FALSE;
            requeue2(taskId, 2);
        }
        if (score1[taskId]==0 && !First_Call){
            b_chameneos=x1[taskId];
            x_other1[taskId]=a_chameneos;
            First_Call=TRUE;
            MustWait=TRUE;
            score1[taskId]=2;
            NotifyAll=TRUE;
        }
        if (score1[taskId]=1 && First_Call){
            x_other1[taskId]=b_chameneos;
            MustWait=FALSE;
            score1[taskId]=2;
            NotifyAll=TRUE;
        }
    }else if (index == 2){ //Wait
    }
}

int check_and_run_any_entry2(){
    reevaluate_barrier2();
    int taskIdEligible2 = current_task_eligible2();
    if (taskIdEligible2==0){
        return FREE;
    }
    setTabStatusOn2(taskIdEligible2);
    return taskIdEligible2;
}

void run_function_call2(taskId, index){
}

void run_procedure_call2(taskId, index){
    /*  int taskIdEligible;
     if (index == 1){
     stock = stock + y[taskId];
     }
     taskIdEligible = check_and_run_any_entry();
     transfert(taskId, taskIdEligible);
     */
}

void run_entry_call2(int taskId, int index){
    int taskIdEligible2;
    evaluate_barrier2(index);
    
    if (tabBarrier2[index]==FALSE){
        add_entry_queue2(taskId, index);
        unlock2(taskId);
        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        remove_entry_queue2(taskId, index);
    }
    run_entry_body2(taskId,index);
    taskIdEligible2 = check_and_run_any_entry2();
    transfert2(taskId, taskIdEligible2);
}

void protected_object_call2(int taskId, int appel, int index){
    lock2(taskId);
    
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        run_entry_call2(taskId, index);
    }
}

/*---------------------------------------------------------------------*/

/*---------------------------------------------------------------------*/


void t_one(){
    static int taskId=1;
    static int taskIdEligible;
    static int my_peer = 0;
    
l0: if (pc[ct]>0||0>=cs) goto l1;
    static int i = 1;
    // for (i = 1; i<=nb_request; i++){
l1: if (pc[ct]>1||1>=cs) goto l2;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
l2: if (pc[ct]>2||2>=cs) goto l3;
    static int score=0;
    //  while (score!=2) {
l3: if (pc[ct]>3||3>=cs) goto l4;
    x1[taskId]=x3[taskId];
l4: if (pc[ct]>4||4>=cs) goto l5;
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
l5: if (pc[ct]>5||5>=cs) goto l6;
    static int appel = 2;
l6: if (pc[ct]>6||6>=cs) goto l7;
    static int index = 1;
l7: if (pc[ct]>7||7>=cs) goto l8;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=8);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=8);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l8: if (pc[ct]>8||8>=cs) goto l9;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l9: if (pc[ct]>9||9>=cs) goto l10;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=10);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l10: if (pc[ct]>10||10>=cs) goto l11;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l11: if (pc[ct]>11||11>=cs) goto l12;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=12);
                
                //run_entry_body2(taskId,index);
                goto while1;
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=12);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l12: if (pc[ct]>12||12>=cs) goto l13;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l13: if (pc[ct]>13||13>=cs) goto l14;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=14);
                
                //run_entry_body2(taskId,index);
                goto while1;
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=14);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=14);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=14);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=14);
            
        }
        __CPROVER_assume(cs>=14);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=14);
    
    //--------------------------
while1:
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 3;
        goto end;
    }
    __CPROVER_assume(cs>=14);
    
    //  }
l14: if (pc[ct]>14||14>=cs) goto l15;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l15: if (pc[ct]>15||15>=cs) goto l16;
    my_peer=return3[taskId];
l16: if (pc[ct]>16||16>=cs) goto l17;
    if (my_peer==taskId){
    l17: if (pc[ct]>17||17>=cs) goto l18;
        cs = size[taskId];
    l18: if (pc[ct]>18||18>=cs) goto l19;
        goto end;
    }
    __CPROVER_assume(cs>=19);
    
l19: if (pc[ct]>19||19>=cs) goto l20;
    test[my_peer]=taskId;
    //--------------------------
l20: if (pc[ct]>20||20>=cs) goto l21;
    i = 2;
l21: if (pc[ct]>21||21>=cs) goto l22;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
l22: if (pc[ct]>22||22>=cs) goto l23;
    score=0;
    //  while (score!=2) {
l23: if (pc[ct]>23||23>=cs) goto l24;
    x1[taskId]=x3[taskId];
l24: if (pc[ct]>24||24>=cs) goto l25;
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
l25: if (pc[ct]>25||25>=cs) goto l26;
    appel = 2;
l26: if (pc[ct]>26||26>=cs) goto l27;
    index = 1;
l27: if (pc[ct]>27||27>=cs) goto l28;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=28);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=28);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l28: if (pc[ct]>28||28>=cs) goto l29;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l29: if (pc[ct]>29||29>=cs) goto l30;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=30);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l30: if (pc[ct]>30||30>=cs) goto l31;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l31: if (pc[ct]>31||31>=cs) goto l32;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=32);
                
                //run_entry_body2(taskId,index);
                goto while2;
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=32);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l32: if (pc[ct]>32||32>=cs) goto l33;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l33: if (pc[ct]>33||33>=cs) goto l34;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=34);
                
                //run_entry_body2(taskId,index);
                goto while2;
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=34);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=34);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=34);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=34);
            
        }
        __CPROVER_assume(cs>=34);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=34);
    
    //--------------------------
while2:
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 23;
        goto end;
    }
    __CPROVER_assume(cs>=34);
    
    //  }
l34: if (pc[ct]>34||34>=cs) goto l35;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l35: if (pc[ct]>35||35>=cs) goto l36;
    my_peer=return3[taskId];
l36: if (pc[ct]>36||36>=cs) goto l37;
    if (my_peer==taskId){
    l37: if (pc[ct]>37||37>=cs) goto l38;
        cs = size[taskId];
    l38: if (pc[ct]>38||38>=cs) goto l39;
        goto end;
    }
    __CPROVER_assume(cs>=39);
    
l39: if (pc[ct]>39||39>=cs) goto l40;
    test[my_peer]=taskId;
    //--------------------------
l40: if (pc[ct]>40||40>=cs) goto l41;
    i=3;
l41: if (pc[ct]>41||41>=cs) goto l42;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
l42: if (pc[ct]>42||42>=cs) goto l43;
    score=0;
    //  while (score!=2) {
l43: if (pc[ct]>43||43>=cs) goto l44;
    x1[taskId]=x3[taskId];
l44: if (pc[ct]>44||44>=cs) goto l45;
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
l45: if (pc[ct]>45||45>=cs) goto l46;
    appel = 2;
l46: if (pc[ct]>46||46>=cs) goto l47;
    index = 1;
l47: if (pc[ct]>47||47>=cs) goto l48;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=48);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=48);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l48: if (pc[ct]>48||48>=cs) goto l49;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l49: if (pc[ct]>49||49>=cs) goto l50;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=50);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l50: if (pc[ct]>50||50>=cs) goto l51;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l51: if (pc[ct]>51||51>=cs) goto l52;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=52);
                
                //run_entry_body2(taskId,index);
                goto while3;
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=52);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l52: if (pc[ct]>52||52>=cs) goto l53;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l53: if (pc[ct]>53||53>=cs) goto l54;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=54);
                
                //run_entry_body2(taskId,index);
                goto while3;
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=54);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=54);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=54);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=54);
            
        }
        __CPROVER_assume(cs>=54);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=54);
    
    //--------------------------
while3:
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 43;
        goto end;
    }
    __CPROVER_assume(cs>=54);
    
    //  }
l54: if (pc[ct]>54||54>=cs) goto l55;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l55: if (pc[ct]>55||55>=cs) goto l56;
    my_peer=return3[taskId];
l56: if (pc[ct]>56||56>=cs) goto l57;
    if (my_peer==taskId){
    l57: if (pc[ct]>57||57>=cs) goto l58;
        cs = size[taskId];
    l58: if (pc[ct]>58||58>=cs) goto l59;
        goto end;
    }
    __CPROVER_assume(cs>=59);
    
l59: if (pc[ct]>59||59>=cs) goto l60;
    test[my_peer]=taskId;
    //--------------------------
l60: if (pc[ct]>60||60>=cs) goto l61;
    i=4;
l61: if (pc[ct]>61||61>=cs) goto l62;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
l62: if (pc[ct]>62||62>=cs) goto l63;
    score=0;
    //  while (score!=2) {
l63: if (pc[ct]>63||63>=cs) goto l64;
    x1[taskId]=x3[taskId];
l64: if (pc[ct]>64||64>=cs) goto l65;
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
l65: if (pc[ct]>65||65>=cs) goto l66;
    appel = 2;
l66: if (pc[ct]>66||66>=cs) goto l67;
    index = 1;
l67: if (pc[ct]>67||67>=cs) goto l68;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=68);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=68);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l68: if (pc[ct]>68||68>=cs) goto l69;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l69: if (pc[ct]>69||69>=cs) goto l70;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=70);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l70: if (pc[ct]>70||70>=cs) goto l71;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l71: if (pc[ct]>71||71>=cs) goto l72;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=72);
                
                //run_entry_body2(taskId,index);
                goto while4;
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=72);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l72: if (pc[ct]>72||72>=cs) goto l73;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l73: if (pc[ct]>73||73>=cs) goto l74;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=74);
                
                //run_entry_body2(taskId,index);
                goto while4;
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=74);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=74);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=74);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=74);
            
        }
        __CPROVER_assume(cs>=74);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=74);
    
    //--------------------------
while4:
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 63;
        goto end;
    }
    __CPROVER_assume(cs>=74);
    
    //  }
l74: if (pc[ct]>74||74>=cs) goto l75;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l75: if (pc[ct]>75||75>=cs) goto l76;
    my_peer=return3[taskId];
l76: if (pc[ct]>76||76>=cs) goto l77;
    if (my_peer==taskId){
    l77: if (pc[ct]>77||77>=cs) goto l78;
        cs = size[taskId];
    l78: if (pc[ct]>78||78>=cs) goto l79;
        goto end;
    }
    __CPROVER_assume(cs>=79);
    
l79: if (pc[ct]>79||79>=cs) goto l80;
    test[my_peer]=taskId;
    //--------------------------
l80: if (pc[ct]>80||80>=cs) goto l81;
    i=5;
l81: if (pc[ct]>81||81>=cs) goto l82;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
l82: if (pc[ct]>82||82>=cs) goto l83;
    score=0;
    //  while (score!=2) {
l83: if (pc[ct]>83||83>=cs) goto l84;
    x1[taskId]=x3[taskId];
l84: if (pc[ct]>84||84>=cs) goto l85;
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
l85: if (pc[ct]>85||85>=cs) goto l86;
    appel = 2;
l86: if (pc[ct]>86||86>=cs) goto l87;
    index = 1;
l87: if (pc[ct]>87||87>=cs) goto l88;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=88);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=88);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l88: if (pc[ct]>88||88>=cs) goto l89;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l89: if (pc[ct]>89||89>=cs) goto l90;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=90);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l90: if (pc[ct]>90||90>=cs) goto l91;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l91: if (pc[ct]>91||91>=cs) goto l92;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=92);
                
                //run_entry_body2(taskId,index);
                goto while5;
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=92);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l92: if (pc[ct]>92||92>=cs) goto l93;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l93: if (pc[ct]>93||93>=cs) goto l94;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=94);
                
                //run_entry_body2(taskId,index);
                goto while5;
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=94);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=94);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=94);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=94);
            
        }
        __CPROVER_assume(cs>=94);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=94);
    
    //--------------------------
while5:
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 83;
        goto end;
    }
    __CPROVER_assume(cs>=94);
    
    //  }
l94: if (pc[ct]>94||94>=cs) goto l95;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l95: if (pc[ct]>95||95>=cs) goto l96;
    my_peer=return3[taskId];
l96: if (pc[ct]>96||96>=cs) goto l97;
    if (my_peer==taskId){
    l97: if (pc[ct]>97||97>=cs) goto l98;
        cs = size[taskId];
    l98: if (pc[ct]>98||98>=cs) goto l99;
        goto end;
    }
    __CPROVER_assume(cs>=99);
    
l99: if (pc[ct]>99||99>=cs) goto l100;
    test[my_peer]=taskId;
    //--------------------------
l100: if (pc[ct]>100||100>=cs) goto l101;
    i=6;
l101: if (pc[ct]>101||101>=cs) goto l102;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
l102: if (pc[ct]>102||102>=cs) goto l103;
    score=0;
    //  while (score!=2) {
l103: if (pc[ct]>103||103>=cs) goto l104;
    x1[taskId]=x3[taskId];
l104: if (pc[ct]>104||104>=cs) goto l105;
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
l105: if (pc[ct]>105||105>=cs) goto l106;
    appel = 2;
l106: if (pc[ct]>106||106>=cs) goto l107;
    index = 1;
l107: if (pc[ct]>107||107>=cs) goto l108;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=108);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=108);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l108: if (pc[ct]>108||108>=cs) goto l109;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l109: if (pc[ct]>109||109>=cs) goto l110;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=110);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l110: if (pc[ct]>110||110>=cs) goto l111;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l111: if (pc[ct]>111||111>=cs) goto l112;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=112);
                
                //run_entry_body2(taskId,index);
                goto while6;
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=112);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l112: if (pc[ct]>112||112>=cs) goto l113;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l113: if (pc[ct]>113||113>=cs) goto l114;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=114);
                
                //run_entry_body2(taskId,index);
                goto while6;
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=114);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=114);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=114);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=114);
            
        }
        __CPROVER_assume(cs>=114);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=114);
    
    //--------------------------
while6:
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 103;
        goto end;
    }
    __CPROVER_assume(cs>=114);
    
    //  }
l114: if (pc[ct]>114||114>=cs) goto l115;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l115: if (pc[ct]>115||115>=cs) goto l116;
    my_peer=return3[taskId];
l116: if (pc[ct]>116||116>=cs) goto l117;
    if (my_peer==taskId){
    l117: if (pc[ct]>117||117>=cs) goto l118;
        cs = size[taskId];
    l118: if (pc[ct]>118||118>=cs) goto l119;
        goto end;
    }
    __CPROVER_assume(cs>=119);
    
l119: if (pc[ct]>119||119>=cs) goto end;
    test[my_peer]=taskId;
    //--------------------------
    //  }
end:
    return;
}


void t_two(){
    static int taskId=2;
    static int taskIdEligible;
    static int my_peer = 0;
    
        
    l0: if (pc[ct]>0||0>=cs) goto l1;
        static int i = 1;
        // for (i = 1; i<=nb_request; i++){
    l1: if (pc[ct]>1||1>=cs) goto l2;
        x3[taskId]=taskId;
        //get_a_peer3(taskId);
    l2: if (pc[ct]>2||2>=cs) goto l3;
        static int score=0;
        //  while (score!=2) {
    l3: if (pc[ct]>3||3>=cs) goto l4;
        x1[taskId]=x3[taskId];
    l4: if (pc[ct]>4||4>=cs) goto l5;
        score1[taskId]=score;
        //protected_object_call2(taskId, 2, 1);
    l5: if (pc[ct]>5||5>=cs) goto l6;
        static int appel = 2;
    l6: if (pc[ct]>6||6>=cs) goto l7;
        static int index = 1;
    l7: if (pc[ct]>7||7>=cs) goto l8;
        lock2(taskId);
        if (appel == 0){ //function
            run_function_call2(taskId, index);
        }else if (appel == 1){ //procedure
            __CPROVER_assume(cs>=8);
            
            run_procedure_call2(taskId, index);
        }else if (appel == 2){ //entry
            __CPROVER_assume(cs>=8);
            
            //run_entry_call2(taskId, index);
            static int taskIdEligible2;
            evaluate_barrier2(index);
            if (tabBarrier2[index]==FALSE){
                add_entry_queue2(taskId, index);
                unlock2(taskId);
            l8: if (pc[ct]>8||8>=cs) goto l9;
                __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            l9: if (pc[ct]>9||9>=cs) goto l10;
                remove_entry_queue2(taskId, index);
            }
            __CPROVER_assume(cs>=10);
            
            //run_entry_body2(taskId,index);
            if (index == 1) { //Cooperate
                if (score1[taskId]==0 && MustWait){
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l10: if (pc[ct]>10||10>=cs) goto l11;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l11: if (pc[ct]>11||11>=cs) goto l12;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=12);
                    
                    //run_entry_body2(taskId,index);
                    goto while1;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=12);
                
                if (score1[taskId]==0 && First_Call){
                    a_chameneos = x1[taskId];
                    First_Call = FALSE;
                    score1[taskId]=1;
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l12: if (pc[ct]>12||12>=cs) goto l13;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l13: if (pc[ct]>13||13>=cs) goto l14;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=14);
                    
                    //run_entry_body2(taskId,index);
                    goto while1;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=14);
                
                if (score1[taskId]==0 && !First_Call){
                    b_chameneos=x1[taskId];
                    x_other1[taskId]=a_chameneos;
                    First_Call=TRUE;
                    MustWait=TRUE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=14);
                
                if (score1[taskId]=1 && First_Call){
                    x_other1[taskId]=b_chameneos;
                    MustWait=FALSE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=14);
                
            }else if (index == 2){ //Wait
                __CPROVER_assume(cs>=14);
                
            }
            __CPROVER_assume(cs>=14);
            
            //--------------------------
            taskIdEligible2 = check_and_run_any_entry2();
            transfert2(taskId, taskIdEligible2);
        }
        __CPROVER_assume(cs>=14);
        
        //--------------------------
    while1:
        score=score1[taskId];
        // __CPROVER_assume(score==2);
        if (score!=2){
            cs = 3;
            goto end;
        }
        __CPROVER_assume(cs>=14);
        
        //  }
    l14: if (pc[ct]>14||14>=cs) goto l15;
        return3[taskId]=x_other1[taskId];
        //--------------------------
    l15: if (pc[ct]>15||15>=cs) goto l16;
        my_peer=return3[taskId];
    l16: if (pc[ct]>16||16>=cs) goto l17;
        if (my_peer==taskId){
        l17: if (pc[ct]>17||17>=cs) goto l18;
            cs = size[taskId];
        l18: if (pc[ct]>18||18>=cs) goto l19;
            goto end;
        }
        __CPROVER_assume(cs>=19);
        
    l19: if (pc[ct]>19||19>=cs) goto l20;
        test[my_peer]=taskId;
        //--------------------------
    l20: if (pc[ct]>20||20>=cs) goto l21;
        i = 2;
    l21: if (pc[ct]>21||21>=cs) goto l22;
        x3[taskId]=taskId;
        //get_a_peer3(taskId);
    l22: if (pc[ct]>22||22>=cs) goto l23;
        score=0;
        //  while (score!=2) {
    l23: if (pc[ct]>23||23>=cs) goto l24;
        x1[taskId]=x3[taskId];
    l24: if (pc[ct]>24||24>=cs) goto l25;
        score1[taskId]=score;
        //protected_object_call2(taskId, 2, 1);
    l25: if (pc[ct]>25||25>=cs) goto l26;
        appel = 2;
    l26: if (pc[ct]>26||26>=cs) goto l27;
        index = 1;
    l27: if (pc[ct]>27||27>=cs) goto l28;
        lock2(taskId);
        if (appel == 0){ //function
            run_function_call2(taskId, index);
        }else if (appel == 1){ //procedure
            __CPROVER_assume(cs>=28);
            
            run_procedure_call2(taskId, index);
        }else if (appel == 2){ //entry
            __CPROVER_assume(cs>=28);
            
            //run_entry_call2(taskId, index);
            static int taskIdEligible2;
            evaluate_barrier2(index);
            if (tabBarrier2[index]==FALSE){
                add_entry_queue2(taskId, index);
                unlock2(taskId);
            l28: if (pc[ct]>28||28>=cs) goto l29;
                __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            l29: if (pc[ct]>29||29>=cs) goto l30;
                remove_entry_queue2(taskId, index);
            }
            __CPROVER_assume(cs>=30);
            
            //run_entry_body2(taskId,index);
            if (index == 1) { //Cooperate
                if (score1[taskId]==0 && MustWait){
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l30: if (pc[ct]>30||30>=cs) goto l31;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l31: if (pc[ct]>31||31>=cs) goto l32;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=32);
                    
                    //run_entry_body2(taskId,index);
                    goto while2;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=32);
                
                if (score1[taskId]==0 && First_Call){
                    a_chameneos = x1[taskId];
                    First_Call = FALSE;
                    score1[taskId]=1;
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l32: if (pc[ct]>32||32>=cs) goto l33;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l33: if (pc[ct]>33||33>=cs) goto l34;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=34);
                    
                    //run_entry_body2(taskId,index);
                    goto while2;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=34);
                
                if (score1[taskId]==0 && !First_Call){
                    b_chameneos=x1[taskId];
                    x_other1[taskId]=a_chameneos;
                    First_Call=TRUE;
                    MustWait=TRUE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=34);
                
                if (score1[taskId]=1 && First_Call){
                    x_other1[taskId]=b_chameneos;
                    MustWait=FALSE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=34);
                
            }else if (index == 2){ //Wait
                __CPROVER_assume(cs>=34);
                
            }
            __CPROVER_assume(cs>=34);
            
            //--------------------------
            taskIdEligible2 = check_and_run_any_entry2();
            transfert2(taskId, taskIdEligible2);
        }
        __CPROVER_assume(cs>=34);
        
        //--------------------------
    while2:
        score=score1[taskId];
        // __CPROVER_assume(score==2);
        if (score!=2){
            cs = 23;
            goto end;
        }
        __CPROVER_assume(cs>=34);
        
        //  }
    l34: if (pc[ct]>34||34>=cs) goto l35;
        return3[taskId]=x_other1[taskId];
        //--------------------------
    l35: if (pc[ct]>35||35>=cs) goto l36;
        my_peer=return3[taskId];
    l36: if (pc[ct]>36||36>=cs) goto l37;
        if (my_peer==taskId){
        l37: if (pc[ct]>37||37>=cs) goto l38;
            cs = size[taskId];
        l38: if (pc[ct]>38||38>=cs) goto l39;
            goto end;
        }
        __CPROVER_assume(cs>=39);
        
    l39: if (pc[ct]>39||39>=cs) goto l40;
        test[my_peer]=taskId;
        //--------------------------
    l40: if (pc[ct]>40||40>=cs) goto l41;
        i=3;
    l41: if (pc[ct]>41||41>=cs) goto l42;
        x3[taskId]=taskId;
        //get_a_peer3(taskId);
    l42: if (pc[ct]>42||42>=cs) goto l43;
        score=0;
        //  while (score!=2) {
    l43: if (pc[ct]>43||43>=cs) goto l44;
        x1[taskId]=x3[taskId];
    l44: if (pc[ct]>44||44>=cs) goto l45;
        score1[taskId]=score;
        //protected_object_call2(taskId, 2, 1);
    l45: if (pc[ct]>45||45>=cs) goto l46;
        appel = 2;
    l46: if (pc[ct]>46||46>=cs) goto l47;
        index = 1;
    l47: if (pc[ct]>47||47>=cs) goto l48;
        lock2(taskId);
        if (appel == 0){ //function
            run_function_call2(taskId, index);
        }else if (appel == 1){ //procedure
            __CPROVER_assume(cs>=48);
            
            run_procedure_call2(taskId, index);
        }else if (appel == 2){ //entry
            __CPROVER_assume(cs>=48);
            
            //run_entry_call2(taskId, index);
            static int taskIdEligible2;
            evaluate_barrier2(index);
            if (tabBarrier2[index]==FALSE){
                add_entry_queue2(taskId, index);
                unlock2(taskId);
            l48: if (pc[ct]>48||48>=cs) goto l49;
                __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            l49: if (pc[ct]>49||49>=cs) goto l50;
                remove_entry_queue2(taskId, index);
            }
            __CPROVER_assume(cs>=50);
            
            //run_entry_body2(taskId,index);
            if (index == 1) { //Cooperate
                if (score1[taskId]==0 && MustWait){
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l50: if (pc[ct]>50||50>=cs) goto l51;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l51: if (pc[ct]>51||51>=cs) goto l52;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=52);
                    
                    //run_entry_body2(taskId,index);
                    goto while3;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=52);
                
                if (score1[taskId]==0 && First_Call){
                    a_chameneos = x1[taskId];
                    First_Call = FALSE;
                    score1[taskId]=1;
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l52: if (pc[ct]>52||52>=cs) goto l53;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l53: if (pc[ct]>53||53>=cs) goto l54;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=54);
                    
                    //run_entry_body2(taskId,index);
                    goto while3;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=54);
                
                if (score1[taskId]==0 && !First_Call){
                    b_chameneos=x1[taskId];
                    x_other1[taskId]=a_chameneos;
                    First_Call=TRUE;
                    MustWait=TRUE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=54);
                
                if (score1[taskId]=1 && First_Call){
                    x_other1[taskId]=b_chameneos;
                    MustWait=FALSE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=54);
                
            }else if (index == 2){ //Wait
                __CPROVER_assume(cs>=54);
                
            }
            __CPROVER_assume(cs>=54);
            
            //--------------------------
            taskIdEligible2 = check_and_run_any_entry2();
            transfert2(taskId, taskIdEligible2);
        }
        __CPROVER_assume(cs>=54);
        
        //--------------------------
    while3:
        score=score1[taskId];
        // __CPROVER_assume(score==2);
        if (score!=2){
            cs = 43;
            goto end;
        }
        __CPROVER_assume(cs>=54);
        
        //  }
    l54: if (pc[ct]>54||54>=cs) goto l55;
        return3[taskId]=x_other1[taskId];
        //--------------------------
    l55: if (pc[ct]>55||55>=cs) goto l56;
        my_peer=return3[taskId];
    l56: if (pc[ct]>56||56>=cs) goto l57;
        if (my_peer==taskId){
        l57: if (pc[ct]>57||57>=cs) goto l58;
            cs = size[taskId];
        l58: if (pc[ct]>58||58>=cs) goto l59;
            goto end;
        }
        __CPROVER_assume(cs>=59);
        
    l59: if (pc[ct]>59||59>=cs) goto l60;
        test[my_peer]=taskId;
        //--------------------------
    l60: if (pc[ct]>60||60>=cs) goto l61;
        i=4;
    l61: if (pc[ct]>61||61>=cs) goto l62;
        x3[taskId]=taskId;
        //get_a_peer3(taskId);
    l62: if (pc[ct]>62||62>=cs) goto l63;
        score=0;
        //  while (score!=2) {
    l63: if (pc[ct]>63||63>=cs) goto l64;
        x1[taskId]=x3[taskId];
    l64: if (pc[ct]>64||64>=cs) goto l65;
        score1[taskId]=score;
        //protected_object_call2(taskId, 2, 1);
    l65: if (pc[ct]>65||65>=cs) goto l66;
        appel = 2;
    l66: if (pc[ct]>66||66>=cs) goto l67;
        index = 1;
    l67: if (pc[ct]>67||67>=cs) goto l68;
        lock2(taskId);
        if (appel == 0){ //function
            run_function_call2(taskId, index);
        }else if (appel == 1){ //procedure
            __CPROVER_assume(cs>=68);
            
            run_procedure_call2(taskId, index);
        }else if (appel == 2){ //entry
            __CPROVER_assume(cs>=68);
            
            //run_entry_call2(taskId, index);
            static int taskIdEligible2;
            evaluate_barrier2(index);
            if (tabBarrier2[index]==FALSE){
                add_entry_queue2(taskId, index);
                unlock2(taskId);
            l68: if (pc[ct]>68||68>=cs) goto l69;
                __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            l69: if (pc[ct]>69||69>=cs) goto l70;
                remove_entry_queue2(taskId, index);
            }
            __CPROVER_assume(cs>=70);
            
            //run_entry_body2(taskId,index);
            if (index == 1) { //Cooperate
                if (score1[taskId]==0 && MustWait){
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l70: if (pc[ct]>70||70>=cs) goto l71;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l71: if (pc[ct]>71||71>=cs) goto l72;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=72);
                    
                    //run_entry_body2(taskId,index);
                    goto while4;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=72);
                
                if (score1[taskId]==0 && First_Call){
                    a_chameneos = x1[taskId];
                    First_Call = FALSE;
                    score1[taskId]=1;
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l72: if (pc[ct]>72||72>=cs) goto l73;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l73: if (pc[ct]>73||73>=cs) goto l74;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=74);
                    
                    //run_entry_body2(taskId,index);
                    goto while4;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=74);
                
                if (score1[taskId]==0 && !First_Call){
                    b_chameneos=x1[taskId];
                    x_other1[taskId]=a_chameneos;
                    First_Call=TRUE;
                    MustWait=TRUE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=74);
                
                if (score1[taskId]=1 && First_Call){
                    x_other1[taskId]=b_chameneos;
                    MustWait=FALSE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=74);
                
            }else if (index == 2){ //Wait
                __CPROVER_assume(cs>=74);
                
            }
            __CPROVER_assume(cs>=74);
            
            //--------------------------
            taskIdEligible2 = check_and_run_any_entry2();
            transfert2(taskId, taskIdEligible2);
        }
        __CPROVER_assume(cs>=74);
        
        //--------------------------
    while4:
        score=score1[taskId];
        // __CPROVER_assume(score==2);
        if (score!=2){
            cs = 63;
            goto end;
        }
        __CPROVER_assume(cs>=74);
        
        //  }
    l74: if (pc[ct]>74||74>=cs) goto l75;
        return3[taskId]=x_other1[taskId];
        //--------------------------
    l75: if (pc[ct]>75||75>=cs) goto l76;
        my_peer=return3[taskId];
    l76: if (pc[ct]>76||76>=cs) goto l77;
        if (my_peer==taskId){
        l77: if (pc[ct]>77||77>=cs) goto l78;
            cs = size[taskId];
        l78: if (pc[ct]>78||78>=cs) goto l79;
            goto end;
        }
        __CPROVER_assume(cs>=79);
        
    l79: if (pc[ct]>79||79>=cs) goto l80;
        test[my_peer]=taskId;
        //--------------------------
    l80: if (pc[ct]>80||80>=cs) goto l81;
        i=5;
    l81: if (pc[ct]>81||81>=cs) goto l82;
        x3[taskId]=taskId;
        //get_a_peer3(taskId);
    l82: if (pc[ct]>82||82>=cs) goto l83;
        score=0;
        //  while (score!=2) {
    l83: if (pc[ct]>83||83>=cs) goto l84;
        x1[taskId]=x3[taskId];
    l84: if (pc[ct]>84||84>=cs) goto l85;
        score1[taskId]=score;
        //protected_object_call2(taskId, 2, 1);
    l85: if (pc[ct]>85||85>=cs) goto l86;
        appel = 2;
    l86: if (pc[ct]>86||86>=cs) goto l87;
        index = 1;
    l87: if (pc[ct]>87||87>=cs) goto l88;
        lock2(taskId);
        if (appel == 0){ //function
            run_function_call2(taskId, index);
        }else if (appel == 1){ //procedure
            __CPROVER_assume(cs>=88);
            
            run_procedure_call2(taskId, index);
        }else if (appel == 2){ //entry
            __CPROVER_assume(cs>=88);
            
            //run_entry_call2(taskId, index);
            static int taskIdEligible2;
            evaluate_barrier2(index);
            if (tabBarrier2[index]==FALSE){
                add_entry_queue2(taskId, index);
                unlock2(taskId);
            l88: if (pc[ct]>88||88>=cs) goto l89;
                __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            l89: if (pc[ct]>89||89>=cs) goto l90;
                remove_entry_queue2(taskId, index);
            }
            __CPROVER_assume(cs>=90);
            
            //run_entry_body2(taskId,index);
            if (index == 1) { //Cooperate
                if (score1[taskId]==0 && MustWait){
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l90: if (pc[ct]>90||90>=cs) goto l91;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l91: if (pc[ct]>91||91>=cs) goto l92;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=92);
                    
                    //run_entry_body2(taskId,index);
                    goto while5;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=92);
                
                if (score1[taskId]==0 && First_Call){
                    a_chameneos = x1[taskId];
                    First_Call = FALSE;
                    score1[taskId]=1;
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l92: if (pc[ct]>92||92>=cs) goto l93;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l93: if (pc[ct]>93||93>=cs) goto l94;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=94);
                    
                    //run_entry_body2(taskId,index);
                    goto while5;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=94);
                
                if (score1[taskId]==0 && !First_Call){
                    b_chameneos=x1[taskId];
                    x_other1[taskId]=a_chameneos;
                    First_Call=TRUE;
                    MustWait=TRUE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=94);
                
                if (score1[taskId]=1 && First_Call){
                    x_other1[taskId]=b_chameneos;
                    MustWait=FALSE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=94);
                
            }else if (index == 2){ //Wait
                __CPROVER_assume(cs>=94);
                
            }
            __CPROVER_assume(cs>=94);
            
            //--------------------------
            taskIdEligible2 = check_and_run_any_entry2();
            transfert2(taskId, taskIdEligible2);
        }
        __CPROVER_assume(cs>=94);
        
        //--------------------------
    while5:
        score=score1[taskId];
        // __CPROVER_assume(score==2);
        if (score!=2){
            cs = 83;
            goto end;
        }
        __CPROVER_assume(cs>=94);
        
        //  }
    l94: if (pc[ct]>94||94>=cs) goto l95;
        return3[taskId]=x_other1[taskId];
        //--------------------------
    l95: if (pc[ct]>95||95>=cs) goto l96;
        my_peer=return3[taskId];
    l96: if (pc[ct]>96||96>=cs) goto l97;
        if (my_peer==taskId){
        l97: if (pc[ct]>97||97>=cs) goto l98;
            cs = size[taskId];
        l98: if (pc[ct]>98||98>=cs) goto l99;
            goto end;
        }
        __CPROVER_assume(cs>=99);
        
    l99: if (pc[ct]>99||99>=cs) goto l100;
        test[my_peer]=taskId;
        //--------------------------
    l100: if (pc[ct]>100||100>=cs) goto l101;
        i=6;
    l101: if (pc[ct]>101||101>=cs) goto l102;
        x3[taskId]=taskId;
        //get_a_peer3(taskId);
    l102: if (pc[ct]>102||102>=cs) goto l103;
        score=0;
        //  while (score!=2) {
    l103: if (pc[ct]>103||103>=cs) goto l104;
        x1[taskId]=x3[taskId];
    l104: if (pc[ct]>104||104>=cs) goto l105;
        score1[taskId]=score;
        //protected_object_call2(taskId, 2, 1);
    l105: if (pc[ct]>105||105>=cs) goto l106;
        appel = 2;
    l106: if (pc[ct]>106||106>=cs) goto l107;
        index = 1;
    l107: if (pc[ct]>107||107>=cs) goto l108;
        lock2(taskId);
        if (appel == 0){ //function
            run_function_call2(taskId, index);
        }else if (appel == 1){ //procedure
            __CPROVER_assume(cs>=108);
            
            run_procedure_call2(taskId, index);
        }else if (appel == 2){ //entry
            __CPROVER_assume(cs>=108);
            
            //run_entry_call2(taskId, index);
            static int taskIdEligible2;
            evaluate_barrier2(index);
            if (tabBarrier2[index]==FALSE){
                add_entry_queue2(taskId, index);
                unlock2(taskId);
            l108: if (pc[ct]>108||108>=cs) goto l109;
                __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            l109: if (pc[ct]>109||109>=cs) goto l110;
                remove_entry_queue2(taskId, index);
            }
            __CPROVER_assume(cs>=110);
            
            //run_entry_body2(taskId,index);
            if (index == 1) { //Cooperate
                if (score1[taskId]==0 && MustWait){
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l110: if (pc[ct]>110||110>=cs) goto l111;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l111: if (pc[ct]>111||111>=cs) goto l112;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=112);
                    
                    //run_entry_body2(taskId,index);
                    goto while6;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=112);
                
                if (score1[taskId]==0 && First_Call){
                    a_chameneos = x1[taskId];
                    First_Call = FALSE;
                    score1[taskId]=1;
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l112: if (pc[ct]>112||112>=cs) goto l113;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l113: if (pc[ct]>113||113>=cs) goto l114;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=114);
                    
                    //run_entry_body2(taskId,index);
                    goto while6;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=114);
                
                if (score1[taskId]==0 && !First_Call){
                    b_chameneos=x1[taskId];
                    x_other1[taskId]=a_chameneos;
                    First_Call=TRUE;
                    MustWait=TRUE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=114);
                
                if (score1[taskId]=1 && First_Call){
                    x_other1[taskId]=b_chameneos;
                    MustWait=FALSE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=114);
                
            }else if (index == 2){ //Wait
                __CPROVER_assume(cs>=114);
                
            }
            __CPROVER_assume(cs>=114);
            
            //--------------------------
            taskIdEligible2 = check_and_run_any_entry2();
            transfert2(taskId, taskIdEligible2);
        }
        __CPROVER_assume(cs>=114);
        
        //--------------------------
    while6:
        score=score1[taskId];
        // __CPROVER_assume(score==2);
        if (score!=2){
            cs = 103;
            goto end;
        }
        __CPROVER_assume(cs>=114);
        
        //  }
    l114: if (pc[ct]>114||114>=cs) goto l115;
        return3[taskId]=x_other1[taskId];
        //--------------------------
    l115: if (pc[ct]>115||115>=cs) goto l116;
        my_peer=return3[taskId];
    l116: if (pc[ct]>116||116>=cs) goto l117;
        if (my_peer==taskId){
        l117: if (pc[ct]>117||117>=cs) goto l118;
            cs = size[taskId];
        l118: if (pc[ct]>118||118>=cs) goto l119;
            goto end;
        }
        __CPROVER_assume(cs>=119);
        
    l119: if (pc[ct]>119||119>=cs) goto end;
        test[my_peer]=taskId;
        //--------------------------
        //  }
    end:
        return;
    }
    
void t_three(){
    static int taskId=3;
    static int taskIdEligible;
    static int my_peer = 0;
        
    l0: if (pc[ct]>0||0>=cs) goto l1;
        static int i = 1;
        // for (i = 1; i<=nb_request; i++){
    l1: if (pc[ct]>1||1>=cs) goto l2;
        x3[taskId]=taskId;
        //get_a_peer3(taskId);
    l2: if (pc[ct]>2||2>=cs) goto l3;
        static int score=0;
        //  while (score!=2) {
    l3: if (pc[ct]>3||3>=cs) goto l4;
        x1[taskId]=x3[taskId];
    l4: if (pc[ct]>4||4>=cs) goto l5;
        score1[taskId]=score;
        //protected_object_call2(taskId, 2, 1);
    l5: if (pc[ct]>5||5>=cs) goto l6;
        static int appel = 2;
    l6: if (pc[ct]>6||6>=cs) goto l7;
        static int index = 1;
    l7: if (pc[ct]>7||7>=cs) goto l8;
        lock2(taskId);
        if (appel == 0){ //function
            run_function_call2(taskId, index);
        }else if (appel == 1){ //procedure
            __CPROVER_assume(cs>=8);
            
            run_procedure_call2(taskId, index);
        }else if (appel == 2){ //entry
            __CPROVER_assume(cs>=8);
            
            //run_entry_call2(taskId, index);
            static int taskIdEligible2;
            evaluate_barrier2(index);
            if (tabBarrier2[index]==FALSE){
                add_entry_queue2(taskId, index);
                unlock2(taskId);
            l8: if (pc[ct]>8||8>=cs) goto l9;
                __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            l9: if (pc[ct]>9||9>=cs) goto l10;
                remove_entry_queue2(taskId, index);
            }
            __CPROVER_assume(cs>=10);
            
            //run_entry_body2(taskId,index);
            if (index == 1) { //Cooperate
                if (score1[taskId]==0 && MustWait){
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l10: if (pc[ct]>10||10>=cs) goto l11;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l11: if (pc[ct]>11||11>=cs) goto l12;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=12);
                    
                    //run_entry_body2(taskId,index);
                    goto while1;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=12);
                
                if (score1[taskId]==0 && First_Call){
                    a_chameneos = x1[taskId];
                    First_Call = FALSE;
                    score1[taskId]=1;
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l12: if (pc[ct]>12||12>=cs) goto l13;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l13: if (pc[ct]>13||13>=cs) goto l14;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=14);
                    
                    //run_entry_body2(taskId,index);
                    goto while1;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=14);
                
                if (score1[taskId]==0 && !First_Call){
                    b_chameneos=x1[taskId];
                    x_other1[taskId]=a_chameneos;
                    First_Call=TRUE;
                    MustWait=TRUE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=14);
                
                if (score1[taskId]=1 && First_Call){
                    x_other1[taskId]=b_chameneos;
                    MustWait=FALSE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=14);
                
            }else if (index == 2){ //Wait
                __CPROVER_assume(cs>=14);
                
            }
            __CPROVER_assume(cs>=14);
            
            //--------------------------
            taskIdEligible2 = check_and_run_any_entry2();
            transfert2(taskId, taskIdEligible2);
        }
        __CPROVER_assume(cs>=14);
        
        //--------------------------
    while1:
        score=score1[taskId];
        // __CPROVER_assume(score==2);
        if (score!=2){
            cs = 3;
            goto end;
        }
        __CPROVER_assume(cs>=14);
        
        //  }
    l14: if (pc[ct]>14||14>=cs) goto l15;
        return3[taskId]=x_other1[taskId];
        //--------------------------
    l15: if (pc[ct]>15||15>=cs) goto l16;
        my_peer=return3[taskId];
    l16: if (pc[ct]>16||16>=cs) goto l17;
        if (my_peer==taskId){
        l17: if (pc[ct]>17||17>=cs) goto l18;
            cs = size[taskId];
        l18: if (pc[ct]>18||18>=cs) goto l19;
            goto end;
        }
        __CPROVER_assume(cs>=19);
        
    l19: if (pc[ct]>19||19>=cs) goto l20;
        test[my_peer]=taskId;
        //--------------------------
    l20: if (pc[ct]>20||20>=cs) goto l21;
        i = 2;
    l21: if (pc[ct]>21||21>=cs) goto l22;
        x3[taskId]=taskId;
        //get_a_peer3(taskId);
    l22: if (pc[ct]>22||22>=cs) goto l23;
        score=0;
        //  while (score!=2) {
    l23: if (pc[ct]>23||23>=cs) goto l24;
        x1[taskId]=x3[taskId];
    l24: if (pc[ct]>24||24>=cs) goto l25;
        score1[taskId]=score;
        //protected_object_call2(taskId, 2, 1);
    l25: if (pc[ct]>25||25>=cs) goto l26;
        appel = 2;
    l26: if (pc[ct]>26||26>=cs) goto l27;
        index = 1;
    l27: if (pc[ct]>27||27>=cs) goto l28;
        lock2(taskId);
        if (appel == 0){ //function
            run_function_call2(taskId, index);
        }else if (appel == 1){ //procedure
            __CPROVER_assume(cs>=28);
            
            run_procedure_call2(taskId, index);
        }else if (appel == 2){ //entry
            __CPROVER_assume(cs>=28);
            
            //run_entry_call2(taskId, index);
            static int taskIdEligible2;
            evaluate_barrier2(index);
            if (tabBarrier2[index]==FALSE){
                add_entry_queue2(taskId, index);
                unlock2(taskId);
            l28: if (pc[ct]>28||28>=cs) goto l29;
                __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            l29: if (pc[ct]>29||29>=cs) goto l30;
                remove_entry_queue2(taskId, index);
            }
            __CPROVER_assume(cs>=30);
            
            //run_entry_body2(taskId,index);
            if (index == 1) { //Cooperate
                if (score1[taskId]==0 && MustWait){
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l30: if (pc[ct]>30||30>=cs) goto l31;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l31: if (pc[ct]>31||31>=cs) goto l32;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=32);
                    
                    //run_entry_body2(taskId,index);
                    goto while2;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=32);
                
                if (score1[taskId]==0 && First_Call){
                    a_chameneos = x1[taskId];
                    First_Call = FALSE;
                    score1[taskId]=1;
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l32: if (pc[ct]>32||32>=cs) goto l33;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l33: if (pc[ct]>33||33>=cs) goto l34;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=34);
                    
                    //run_entry_body2(taskId,index);
                    goto while2;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=34);
                
                if (score1[taskId]==0 && !First_Call){
                    b_chameneos=x1[taskId];
                    x_other1[taskId]=a_chameneos;
                    First_Call=TRUE;
                    MustWait=TRUE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=34);
                
                if (score1[taskId]=1 && First_Call){
                    x_other1[taskId]=b_chameneos;
                    MustWait=FALSE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=34);
                
            }else if (index == 2){ //Wait
                __CPROVER_assume(cs>=34);
                
            }
            __CPROVER_assume(cs>=34);
            
            //--------------------------
            taskIdEligible2 = check_and_run_any_entry2();
            transfert2(taskId, taskIdEligible2);
        }
        __CPROVER_assume(cs>=34);
        
        //--------------------------
    while2:
        score=score1[taskId];
        // __CPROVER_assume(score==2);
        if (score!=2){
            cs = 23;
            goto end;
        }
        __CPROVER_assume(cs>=34);
        
        //  }
    l34: if (pc[ct]>34||34>=cs) goto l35;
        return3[taskId]=x_other1[taskId];
        //--------------------------
    l35: if (pc[ct]>35||35>=cs) goto l36;
        my_peer=return3[taskId];
    l36: if (pc[ct]>36||36>=cs) goto l37;
        if (my_peer==taskId){
        l37: if (pc[ct]>37||37>=cs) goto l38;
            cs = size[taskId];
        l38: if (pc[ct]>38||38>=cs) goto l39;
            goto end;
        }
        __CPROVER_assume(cs>=39);
        
    l39: if (pc[ct]>39||39>=cs) goto l40;
        test[my_peer]=taskId;
        //--------------------------
    l40: if (pc[ct]>40||40>=cs) goto l41;
        i=3;
    l41: if (pc[ct]>41||41>=cs) goto l42;
        x3[taskId]=taskId;
        //get_a_peer3(taskId);
    l42: if (pc[ct]>42||42>=cs) goto l43;
        score=0;
        //  while (score!=2) {
    l43: if (pc[ct]>43||43>=cs) goto l44;
        x1[taskId]=x3[taskId];
    l44: if (pc[ct]>44||44>=cs) goto l45;
        score1[taskId]=score;
        //protected_object_call2(taskId, 2, 1);
    l45: if (pc[ct]>45||45>=cs) goto l46;
        appel = 2;
    l46: if (pc[ct]>46||46>=cs) goto l47;
        index = 1;
    l47: if (pc[ct]>47||47>=cs) goto l48;
        lock2(taskId);
        if (appel == 0){ //function
            run_function_call2(taskId, index);
        }else if (appel == 1){ //procedure
            __CPROVER_assume(cs>=48);
            
            run_procedure_call2(taskId, index);
        }else if (appel == 2){ //entry
            __CPROVER_assume(cs>=48);
            
            //run_entry_call2(taskId, index);
            static int taskIdEligible2;
            evaluate_barrier2(index);
            if (tabBarrier2[index]==FALSE){
                add_entry_queue2(taskId, index);
                unlock2(taskId);
            l48: if (pc[ct]>48||48>=cs) goto l49;
                __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            l49: if (pc[ct]>49||49>=cs) goto l50;
                remove_entry_queue2(taskId, index);
            }
            __CPROVER_assume(cs>=50);
            
            //run_entry_body2(taskId,index);
            if (index == 1) { //Cooperate
                if (score1[taskId]==0 && MustWait){
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l50: if (pc[ct]>50||50>=cs) goto l51;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l51: if (pc[ct]>51||51>=cs) goto l52;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=52);
                    
                    //run_entry_body2(taskId,index);
                    goto while3;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=52);
                
                if (score1[taskId]==0 && First_Call){
                    a_chameneos = x1[taskId];
                    First_Call = FALSE;
                    score1[taskId]=1;
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l52: if (pc[ct]>52||52>=cs) goto l53;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l53: if (pc[ct]>53||53>=cs) goto l54;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=54);
                    
                    //run_entry_body2(taskId,index);
                    goto while3;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=54);
                
                if (score1[taskId]==0 && !First_Call){
                    b_chameneos=x1[taskId];
                    x_other1[taskId]=a_chameneos;
                    First_Call=TRUE;
                    MustWait=TRUE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=54);
                
                if (score1[taskId]=1 && First_Call){
                    x_other1[taskId]=b_chameneos;
                    MustWait=FALSE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=54);
                
            }else if (index == 2){ //Wait
                __CPROVER_assume(cs>=54);
                
            }
            __CPROVER_assume(cs>=54);
            
            //--------------------------
            taskIdEligible2 = check_and_run_any_entry2();
            transfert2(taskId, taskIdEligible2);
        }
        __CPROVER_assume(cs>=54);
        
        //--------------------------
    while3:
        score=score1[taskId];
        // __CPROVER_assume(score==2);
        if (score!=2){
            cs = 43;
            goto end;
        }
        __CPROVER_assume(cs>=54);
        
        //  }
    l54: if (pc[ct]>54||54>=cs) goto l55;
        return3[taskId]=x_other1[taskId];
        //--------------------------
    l55: if (pc[ct]>55||55>=cs) goto l56;
        my_peer=return3[taskId];
    l56: if (pc[ct]>56||56>=cs) goto l57;
        if (my_peer==taskId){
        l57: if (pc[ct]>57||57>=cs) goto l58;
            cs = size[taskId];
        l58: if (pc[ct]>58||58>=cs) goto l59;
            goto end;
        }
        __CPROVER_assume(cs>=59);
        
    l59: if (pc[ct]>59||59>=cs) goto l60;
        test[my_peer]=taskId;
        //--------------------------
    l60: if (pc[ct]>60||60>=cs) goto l61;
        i=4;
    l61: if (pc[ct]>61||61>=cs) goto l62;
        x3[taskId]=taskId;
        //get_a_peer3(taskId);
    l62: if (pc[ct]>62||62>=cs) goto l63;
        score=0;
        //  while (score!=2) {
    l63: if (pc[ct]>63||63>=cs) goto l64;
        x1[taskId]=x3[taskId];
    l64: if (pc[ct]>64||64>=cs) goto l65;
        score1[taskId]=score;
        //protected_object_call2(taskId, 2, 1);
    l65: if (pc[ct]>65||65>=cs) goto l66;
        appel = 2;
    l66: if (pc[ct]>66||66>=cs) goto l67;
        index = 1;
    l67: if (pc[ct]>67||67>=cs) goto l68;
        lock2(taskId);
        if (appel == 0){ //function
            run_function_call2(taskId, index);
        }else if (appel == 1){ //procedure
            __CPROVER_assume(cs>=68);
            
            run_procedure_call2(taskId, index);
        }else if (appel == 2){ //entry
            __CPROVER_assume(cs>=68);
            
            //run_entry_call2(taskId, index);
            static int taskIdEligible2;
            evaluate_barrier2(index);
            if (tabBarrier2[index]==FALSE){
                add_entry_queue2(taskId, index);
                unlock2(taskId);
            l68: if (pc[ct]>68||68>=cs) goto l69;
                __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            l69: if (pc[ct]>69||69>=cs) goto l70;
                remove_entry_queue2(taskId, index);
            }
            __CPROVER_assume(cs>=70);
            
            //run_entry_body2(taskId,index);
            if (index == 1) { //Cooperate
                if (score1[taskId]==0 && MustWait){
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l70: if (pc[ct]>70||70>=cs) goto l71;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l71: if (pc[ct]>71||71>=cs) goto l72;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=72);
                    
                    //run_entry_body2(taskId,index);
                    goto while4;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=72);
                
                if (score1[taskId]==0 && First_Call){
                    a_chameneos = x1[taskId];
                    First_Call = FALSE;
                    score1[taskId]=1;
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l72: if (pc[ct]>72||72>=cs) goto l73;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l73: if (pc[ct]>73||73>=cs) goto l74;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=74);
                    
                    //run_entry_body2(taskId,index);
                    goto while4;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=74);
                
                if (score1[taskId]==0 && !First_Call){
                    b_chameneos=x1[taskId];
                    x_other1[taskId]=a_chameneos;
                    First_Call=TRUE;
                    MustWait=TRUE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=74);
                
                if (score1[taskId]=1 && First_Call){
                    x_other1[taskId]=b_chameneos;
                    MustWait=FALSE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=74);
                
            }else if (index == 2){ //Wait
                __CPROVER_assume(cs>=74);
                
            }
            __CPROVER_assume(cs>=74);
            
            //--------------------------
            taskIdEligible2 = check_and_run_any_entry2();
            transfert2(taskId, taskIdEligible2);
        }
        __CPROVER_assume(cs>=74);
        
        //--------------------------
    while4:
        score=score1[taskId];
        // __CPROVER_assume(score==2);
        if (score!=2){
            cs = 63;
            goto end;
        }
        __CPROVER_assume(cs>=74);
        
        //  }
    l74: if (pc[ct]>74||74>=cs) goto l75;
        return3[taskId]=x_other1[taskId];
        //--------------------------
    l75: if (pc[ct]>75||75>=cs) goto l76;
        my_peer=return3[taskId];
    l76: if (pc[ct]>76||76>=cs) goto l77;
        if (my_peer==taskId){
        l77: if (pc[ct]>77||77>=cs) goto l78;
            cs = size[taskId];
        l78: if (pc[ct]>78||78>=cs) goto l79;
            goto end;
        }
        __CPROVER_assume(cs>=79);
        
    l79: if (pc[ct]>79||79>=cs) goto l80;
        test[my_peer]=taskId;
        //--------------------------
    l80: if (pc[ct]>80||80>=cs) goto l81;
        i=5;
    l81: if (pc[ct]>81||81>=cs) goto l82;
        x3[taskId]=taskId;
        //get_a_peer3(taskId);
    l82: if (pc[ct]>82||82>=cs) goto l83;
        score=0;
        //  while (score!=2) {
    l83: if (pc[ct]>83||83>=cs) goto l84;
        x1[taskId]=x3[taskId];
    l84: if (pc[ct]>84||84>=cs) goto l85;
        score1[taskId]=score;
        //protected_object_call2(taskId, 2, 1);
    l85: if (pc[ct]>85||85>=cs) goto l86;
        appel = 2;
    l86: if (pc[ct]>86||86>=cs) goto l87;
        index = 1;
    l87: if (pc[ct]>87||87>=cs) goto l88;
        lock2(taskId);
        if (appel == 0){ //function
            run_function_call2(taskId, index);
        }else if (appel == 1){ //procedure
            __CPROVER_assume(cs>=88);
            
            run_procedure_call2(taskId, index);
        }else if (appel == 2){ //entry
            __CPROVER_assume(cs>=88);
            
            //run_entry_call2(taskId, index);
            static int taskIdEligible2;
            evaluate_barrier2(index);
            if (tabBarrier2[index]==FALSE){
                add_entry_queue2(taskId, index);
                unlock2(taskId);
            l88: if (pc[ct]>88||88>=cs) goto l89;
                __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            l89: if (pc[ct]>89||89>=cs) goto l90;
                remove_entry_queue2(taskId, index);
            }
            __CPROVER_assume(cs>=90);
            
            //run_entry_body2(taskId,index);
            if (index == 1) { //Cooperate
                if (score1[taskId]==0 && MustWait){
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l90: if (pc[ct]>90||90>=cs) goto l91;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l91: if (pc[ct]>91||91>=cs) goto l92;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=92);
                    
                    //run_entry_body2(taskId,index);
                    goto while5;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=92);
                
                if (score1[taskId]==0 && First_Call){
                    a_chameneos = x1[taskId];
                    First_Call = FALSE;
                    score1[taskId]=1;
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l92: if (pc[ct]>92||92>=cs) goto l93;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l93: if (pc[ct]>93||93>=cs) goto l94;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=94);
                    
                    //run_entry_body2(taskId,index);
                    goto while5;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=94);
                
                if (score1[taskId]==0 && !First_Call){
                    b_chameneos=x1[taskId];
                    x_other1[taskId]=a_chameneos;
                    First_Call=TRUE;
                    MustWait=TRUE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=94);
                
                if (score1[taskId]=1 && First_Call){
                    x_other1[taskId]=b_chameneos;
                    MustWait=FALSE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=94);
                
            }else if (index == 2){ //Wait
                __CPROVER_assume(cs>=94);
                
            }
            __CPROVER_assume(cs>=94);
            
            //--------------------------
            taskIdEligible2 = check_and_run_any_entry2();
            transfert2(taskId, taskIdEligible2);
        }
        __CPROVER_assume(cs>=94);
        
        //--------------------------
    while5:
        score=score1[taskId];
        // __CPROVER_assume(score==2);
        if (score!=2){
            cs = 83;
            goto end;
        }
        __CPROVER_assume(cs>=94);
        
        //  }
    l94: if (pc[ct]>94||94>=cs) goto l95;
        return3[taskId]=x_other1[taskId];
        //--------------------------
    l95: if (pc[ct]>95||95>=cs) goto l96;
        my_peer=return3[taskId];
    l96: if (pc[ct]>96||96>=cs) goto l97;
        if (my_peer==taskId){
        l97: if (pc[ct]>97||97>=cs) goto l98;
            cs = size[taskId];
        l98: if (pc[ct]>98||98>=cs) goto l99;
            goto end;
        }
        __CPROVER_assume(cs>=99);
        
    l99: if (pc[ct]>99||99>=cs) goto l100;
        test[my_peer]=taskId;
        //--------------------------
    l100: if (pc[ct]>100||100>=cs) goto l101;
        i=6;
    l101: if (pc[ct]>101||101>=cs) goto l102;
        x3[taskId]=taskId;
        //get_a_peer3(taskId);
    l102: if (pc[ct]>102||102>=cs) goto l103;
        score=0;
        //  while (score!=2) {
    l103: if (pc[ct]>103||103>=cs) goto l104;
        x1[taskId]=x3[taskId];
    l104: if (pc[ct]>104||104>=cs) goto l105;
        score1[taskId]=score;
        //protected_object_call2(taskId, 2, 1);
    l105: if (pc[ct]>105||105>=cs) goto l106;
        appel = 2;
    l106: if (pc[ct]>106||106>=cs) goto l107;
        index = 1;
    l107: if (pc[ct]>107||107>=cs) goto l108;
        lock2(taskId);
        if (appel == 0){ //function
            run_function_call2(taskId, index);
        }else if (appel == 1){ //procedure
            __CPROVER_assume(cs>=108);
            
            run_procedure_call2(taskId, index);
        }else if (appel == 2){ //entry
            __CPROVER_assume(cs>=108);
            
            //run_entry_call2(taskId, index);
            static int taskIdEligible2;
            evaluate_barrier2(index);
            if (tabBarrier2[index]==FALSE){
                add_entry_queue2(taskId, index);
                unlock2(taskId);
            l108: if (pc[ct]>108||108>=cs) goto l109;
                __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            l109: if (pc[ct]>109||109>=cs) goto l110;
                remove_entry_queue2(taskId, index);
            }
            __CPROVER_assume(cs>=110);
            
            //run_entry_body2(taskId,index);
            if (index == 1) { //Cooperate
                if (score1[taskId]==0 && MustWait){
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l110: if (pc[ct]>110||110>=cs) goto l111;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l111: if (pc[ct]>111||111>=cs) goto l112;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=112);
                    
                    //run_entry_body2(taskId,index);
                    goto while6;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=112);
                
                if (score1[taskId]==0 && First_Call){
                    a_chameneos = x1[taskId];
                    First_Call = FALSE;
                    score1[taskId]=1;
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l112: if (pc[ct]>112||112>=cs) goto l113;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l113: if (pc[ct]>113||113>=cs) goto l114;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=114);
                    
                    //run_entry_body2(taskId,index);
                    goto while6;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=114);
                
                if (score1[taskId]==0 && !First_Call){
                    b_chameneos=x1[taskId];
                    x_other1[taskId]=a_chameneos;
                    First_Call=TRUE;
                    MustWait=TRUE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=114);
                
                if (score1[taskId]=1 && First_Call){
                    x_other1[taskId]=b_chameneos;
                    MustWait=FALSE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=114);
                
            }else if (index == 2){ //Wait
                __CPROVER_assume(cs>=114);
                
            }
            __CPROVER_assume(cs>=114);
            
            //--------------------------
            taskIdEligible2 = check_and_run_any_entry2();
            transfert2(taskId, taskIdEligible2);
        }
        __CPROVER_assume(cs>=114);
        
        //--------------------------
    while6:
        score=score1[taskId];
        // __CPROVER_assume(score==2);
        if (score!=2){
            cs = 103;
            goto end;
        }
        __CPROVER_assume(cs>=114);
        
        //  }
    l114: if (pc[ct]>114||114>=cs) goto l115;
        return3[taskId]=x_other1[taskId];
        //--------------------------
    l115: if (pc[ct]>115||115>=cs) goto l116;
        my_peer=return3[taskId];
    l116: if (pc[ct]>116||116>=cs) goto l117;
        if (my_peer==taskId){
        l117: if (pc[ct]>117||117>=cs) goto l118;
            cs = size[taskId];
        l118: if (pc[ct]>118||118>=cs) goto l119;
            goto end;
        }
        __CPROVER_assume(cs>=119);
        
    l119: if (pc[ct]>119||119>=cs) goto end;
        test[my_peer]=taskId;
        //--------------------------
        //  }
    end:
        return;
    }

void t_four(){
    static int taskId=4;
    static int taskIdEligible;
    static int my_peer = 0;
        
    l0: if (pc[ct]>0||0>=cs) goto l1;
        static int i = 1;
        // for (i = 1; i<=nb_request; i++){
    l1: if (pc[ct]>1||1>=cs) goto l2;
        x3[taskId]=taskId;
        //get_a_peer3(taskId);
    l2: if (pc[ct]>2||2>=cs) goto l3;
        static int score=0;
        //  while (score!=2) {
    l3: if (pc[ct]>3||3>=cs) goto l4;
        x1[taskId]=x3[taskId];
    l4: if (pc[ct]>4||4>=cs) goto l5;
        score1[taskId]=score;
        //protected_object_call2(taskId, 2, 1);
    l5: if (pc[ct]>5||5>=cs) goto l6;
        static int appel = 2;
    l6: if (pc[ct]>6||6>=cs) goto l7;
        static int index = 1;
    l7: if (pc[ct]>7||7>=cs) goto l8;
        lock2(taskId);
        if (appel == 0){ //function
            run_function_call2(taskId, index);
        }else if (appel == 1){ //procedure
            __CPROVER_assume(cs>=8);
            
            run_procedure_call2(taskId, index);
        }else if (appel == 2){ //entry
            __CPROVER_assume(cs>=8);
            
            //run_entry_call2(taskId, index);
            static int taskIdEligible2;
            evaluate_barrier2(index);
            if (tabBarrier2[index]==FALSE){
                add_entry_queue2(taskId, index);
                unlock2(taskId);
            l8: if (pc[ct]>8||8>=cs) goto l9;
                __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            l9: if (pc[ct]>9||9>=cs) goto l10;
                remove_entry_queue2(taskId, index);
            }
            __CPROVER_assume(cs>=10);
            
            //run_entry_body2(taskId,index);
            if (index == 1) { //Cooperate
                if (score1[taskId]==0 && MustWait){
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l10: if (pc[ct]>10||10>=cs) goto l11;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l11: if (pc[ct]>11||11>=cs) goto l12;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=12);
                    
                    //run_entry_body2(taskId,index);
                    goto while1;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=12);
                
                if (score1[taskId]==0 && First_Call){
                    a_chameneos = x1[taskId];
                    First_Call = FALSE;
                    score1[taskId]=1;
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l12: if (pc[ct]>12||12>=cs) goto l13;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l13: if (pc[ct]>13||13>=cs) goto l14;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=14);
                    
                    //run_entry_body2(taskId,index);
                    goto while1;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=14);
                
                if (score1[taskId]==0 && !First_Call){
                    b_chameneos=x1[taskId];
                    x_other1[taskId]=a_chameneos;
                    First_Call=TRUE;
                    MustWait=TRUE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=14);
                
                if (score1[taskId]=1 && First_Call){
                    x_other1[taskId]=b_chameneos;
                    MustWait=FALSE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=14);
                
            }else if (index == 2){ //Wait
                __CPROVER_assume(cs>=14);
                
            }
            __CPROVER_assume(cs>=14);
            
            //--------------------------
            taskIdEligible2 = check_and_run_any_entry2();
            transfert2(taskId, taskIdEligible2);
        }
        __CPROVER_assume(cs>=14);
        
        //--------------------------
    while1:
        score=score1[taskId];
        // __CPROVER_assume(score==2);
        if (score!=2){
            cs = 3;
            goto end;
        }
        __CPROVER_assume(cs>=14);
        
        //  }
    l14: if (pc[ct]>14||14>=cs) goto l15;
        return3[taskId]=x_other1[taskId];
        //--------------------------
    l15: if (pc[ct]>15||15>=cs) goto l16;
        my_peer=return3[taskId];
    l16: if (pc[ct]>16||16>=cs) goto l17;
        if (my_peer==taskId){
        l17: if (pc[ct]>17||17>=cs) goto l18;
            cs = size[taskId];
        l18: if (pc[ct]>18||18>=cs) goto l19;
            goto end;
        }
        __CPROVER_assume(cs>=19);
        
    l19: if (pc[ct]>19||19>=cs) goto l20;
        test[my_peer]=taskId;
        //--------------------------
    l20: if (pc[ct]>20||20>=cs) goto l21;
        i = 2;
    l21: if (pc[ct]>21||21>=cs) goto l22;
        x3[taskId]=taskId;
        //get_a_peer3(taskId);
    l22: if (pc[ct]>22||22>=cs) goto l23;
        score=0;
        //  while (score!=2) {
    l23: if (pc[ct]>23||23>=cs) goto l24;
        x1[taskId]=x3[taskId];
    l24: if (pc[ct]>24||24>=cs) goto l25;
        score1[taskId]=score;
        //protected_object_call2(taskId, 2, 1);
    l25: if (pc[ct]>25||25>=cs) goto l26;
        appel = 2;
    l26: if (pc[ct]>26||26>=cs) goto l27;
        index = 1;
    l27: if (pc[ct]>27||27>=cs) goto l28;
        lock2(taskId);
        if (appel == 0){ //function
            run_function_call2(taskId, index);
        }else if (appel == 1){ //procedure
            __CPROVER_assume(cs>=28);
            
            run_procedure_call2(taskId, index);
        }else if (appel == 2){ //entry
            __CPROVER_assume(cs>=28);
            
            //run_entry_call2(taskId, index);
            static int taskIdEligible2;
            evaluate_barrier2(index);
            if (tabBarrier2[index]==FALSE){
                add_entry_queue2(taskId, index);
                unlock2(taskId);
            l28: if (pc[ct]>28||28>=cs) goto l29;
                __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            l29: if (pc[ct]>29||29>=cs) goto l30;
                remove_entry_queue2(taskId, index);
            }
            __CPROVER_assume(cs>=30);
            
            //run_entry_body2(taskId,index);
            if (index == 1) { //Cooperate
                if (score1[taskId]==0 && MustWait){
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l30: if (pc[ct]>30||30>=cs) goto l31;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l31: if (pc[ct]>31||31>=cs) goto l32;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=32);
                    
                    //run_entry_body2(taskId,index);
                    goto while2;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=32);
                
                if (score1[taskId]==0 && First_Call){
                    a_chameneos = x1[taskId];
                    First_Call = FALSE;
                    score1[taskId]=1;
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l32: if (pc[ct]>32||32>=cs) goto l33;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l33: if (pc[ct]>33||33>=cs) goto l34;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=34);
                    
                    //run_entry_body2(taskId,index);
                    goto while2;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=34);
                
                if (score1[taskId]==0 && !First_Call){
                    b_chameneos=x1[taskId];
                    x_other1[taskId]=a_chameneos;
                    First_Call=TRUE;
                    MustWait=TRUE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=34);
                
                if (score1[taskId]=1 && First_Call){
                    x_other1[taskId]=b_chameneos;
                    MustWait=FALSE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=34);
                
            }else if (index == 2){ //Wait
                __CPROVER_assume(cs>=34);
                
            }
            __CPROVER_assume(cs>=34);
            
            //--------------------------
            taskIdEligible2 = check_and_run_any_entry2();
            transfert2(taskId, taskIdEligible2);
        }
        __CPROVER_assume(cs>=34);
        
        //--------------------------
    while2:
        score=score1[taskId];
        // __CPROVER_assume(score==2);
        if (score!=2){
            cs = 23;
            goto end;
        }
        __CPROVER_assume(cs>=34);
        
        //  }
    l34: if (pc[ct]>34||34>=cs) goto l35;
        return3[taskId]=x_other1[taskId];
        //--------------------------
    l35: if (pc[ct]>35||35>=cs) goto l36;
        my_peer=return3[taskId];
    l36: if (pc[ct]>36||36>=cs) goto l37;
        if (my_peer==taskId){
        l37: if (pc[ct]>37||37>=cs) goto l38;
            cs = size[taskId];
        l38: if (pc[ct]>38||38>=cs) goto l39;
            goto end;
        }
        __CPROVER_assume(cs>=39);
        
    l39: if (pc[ct]>39||39>=cs) goto l40;
        test[my_peer]=taskId;
        //--------------------------
    l40: if (pc[ct]>40||40>=cs) goto l41;
        i=3;
    l41: if (pc[ct]>41||41>=cs) goto l42;
        x3[taskId]=taskId;
        //get_a_peer3(taskId);
    l42: if (pc[ct]>42||42>=cs) goto l43;
        score=0;
        //  while (score!=2) {
    l43: if (pc[ct]>43||43>=cs) goto l44;
        x1[taskId]=x3[taskId];
    l44: if (pc[ct]>44||44>=cs) goto l45;
        score1[taskId]=score;
        //protected_object_call2(taskId, 2, 1);
    l45: if (pc[ct]>45||45>=cs) goto l46;
        appel = 2;
    l46: if (pc[ct]>46||46>=cs) goto l47;
        index = 1;
    l47: if (pc[ct]>47||47>=cs) goto l48;
        lock2(taskId);
        if (appel == 0){ //function
            run_function_call2(taskId, index);
        }else if (appel == 1){ //procedure
            __CPROVER_assume(cs>=48);
            
            run_procedure_call2(taskId, index);
        }else if (appel == 2){ //entry
            __CPROVER_assume(cs>=48);
            
            //run_entry_call2(taskId, index);
            static int taskIdEligible2;
            evaluate_barrier2(index);
            if (tabBarrier2[index]==FALSE){
                add_entry_queue2(taskId, index);
                unlock2(taskId);
            l48: if (pc[ct]>48||48>=cs) goto l49;
                __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            l49: if (pc[ct]>49||49>=cs) goto l50;
                remove_entry_queue2(taskId, index);
            }
            __CPROVER_assume(cs>=50);
            
            //run_entry_body2(taskId,index);
            if (index == 1) { //Cooperate
                if (score1[taskId]==0 && MustWait){
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l50: if (pc[ct]>50||50>=cs) goto l51;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l51: if (pc[ct]>51||51>=cs) goto l52;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=52);
                    
                    //run_entry_body2(taskId,index);
                    goto while3;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=52);
                
                if (score1[taskId]==0 && First_Call){
                    a_chameneos = x1[taskId];
                    First_Call = FALSE;
                    score1[taskId]=1;
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l52: if (pc[ct]>52||52>=cs) goto l53;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l53: if (pc[ct]>53||53>=cs) goto l54;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=54);
                    
                    //run_entry_body2(taskId,index);
                    goto while3;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=54);
                
                if (score1[taskId]==0 && !First_Call){
                    b_chameneos=x1[taskId];
                    x_other1[taskId]=a_chameneos;
                    First_Call=TRUE;
                    MustWait=TRUE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=54);
                
                if (score1[taskId]=1 && First_Call){
                    x_other1[taskId]=b_chameneos;
                    MustWait=FALSE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=54);
                
            }else if (index == 2){ //Wait
                __CPROVER_assume(cs>=54);
                
            }
            __CPROVER_assume(cs>=54);
            
            //--------------------------
            taskIdEligible2 = check_and_run_any_entry2();
            transfert2(taskId, taskIdEligible2);
        }
        __CPROVER_assume(cs>=54);
        
        //--------------------------
    while3:
        score=score1[taskId];
        // __CPROVER_assume(score==2);
        if (score!=2){
            cs = 43;
            goto end;
        }
        __CPROVER_assume(cs>=54);
        
        //  }
    l54: if (pc[ct]>54||54>=cs) goto l55;
        return3[taskId]=x_other1[taskId];
        //--------------------------
    l55: if (pc[ct]>55||55>=cs) goto l56;
        my_peer=return3[taskId];
    l56: if (pc[ct]>56||56>=cs) goto l57;
        if (my_peer==taskId){
        l57: if (pc[ct]>57||57>=cs) goto l58;
            cs = size[taskId];
        l58: if (pc[ct]>58||58>=cs) goto l59;
            goto end;
        }
        __CPROVER_assume(cs>=59);
        
    l59: if (pc[ct]>59||59>=cs) goto l60;
        test[my_peer]=taskId;
        //--------------------------
    l60: if (pc[ct]>60||60>=cs) goto l61;
        i=4;
    l61: if (pc[ct]>61||61>=cs) goto l62;
        x3[taskId]=taskId;
        //get_a_peer3(taskId);
    l62: if (pc[ct]>62||62>=cs) goto l63;
        score=0;
        //  while (score!=2) {
    l63: if (pc[ct]>63||63>=cs) goto l64;
        x1[taskId]=x3[taskId];
    l64: if (pc[ct]>64||64>=cs) goto l65;
        score1[taskId]=score;
        //protected_object_call2(taskId, 2, 1);
    l65: if (pc[ct]>65||65>=cs) goto l66;
        appel = 2;
    l66: if (pc[ct]>66||66>=cs) goto l67;
        index = 1;
    l67: if (pc[ct]>67||67>=cs) goto l68;
        lock2(taskId);
        if (appel == 0){ //function
            run_function_call2(taskId, index);
        }else if (appel == 1){ //procedure
            __CPROVER_assume(cs>=68);
            
            run_procedure_call2(taskId, index);
        }else if (appel == 2){ //entry
            __CPROVER_assume(cs>=68);
            
            //run_entry_call2(taskId, index);
            static int taskIdEligible2;
            evaluate_barrier2(index);
            if (tabBarrier2[index]==FALSE){
                add_entry_queue2(taskId, index);
                unlock2(taskId);
            l68: if (pc[ct]>68||68>=cs) goto l69;
                __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            l69: if (pc[ct]>69||69>=cs) goto l70;
                remove_entry_queue2(taskId, index);
            }
            __CPROVER_assume(cs>=70);
            
            //run_entry_body2(taskId,index);
            if (index == 1) { //Cooperate
                if (score1[taskId]==0 && MustWait){
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l70: if (pc[ct]>70||70>=cs) goto l71;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l71: if (pc[ct]>71||71>=cs) goto l72;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=72);
                    
                    //run_entry_body2(taskId,index);
                    goto while4;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=72);
                
                if (score1[taskId]==0 && First_Call){
                    a_chameneos = x1[taskId];
                    First_Call = FALSE;
                    score1[taskId]=1;
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l72: if (pc[ct]>72||72>=cs) goto l73;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l73: if (pc[ct]>73||73>=cs) goto l74;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=74);
                    
                    //run_entry_body2(taskId,index);
                    goto while4;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=74);
                
                if (score1[taskId]==0 && !First_Call){
                    b_chameneos=x1[taskId];
                    x_other1[taskId]=a_chameneos;
                    First_Call=TRUE;
                    MustWait=TRUE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=74);
                
                if (score1[taskId]=1 && First_Call){
                    x_other1[taskId]=b_chameneos;
                    MustWait=FALSE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=74);
                
            }else if (index == 2){ //Wait
                __CPROVER_assume(cs>=74);
                
            }
            __CPROVER_assume(cs>=74);
            
            //--------------------------
            taskIdEligible2 = check_and_run_any_entry2();
            transfert2(taskId, taskIdEligible2);
        }
        __CPROVER_assume(cs>=74);
        
        //--------------------------
    while4:
        score=score1[taskId];
        // __CPROVER_assume(score==2);
        if (score!=2){
            cs = 63;
            goto end;
        }
        __CPROVER_assume(cs>=74);
        
        //  }
    l74: if (pc[ct]>74||74>=cs) goto l75;
        return3[taskId]=x_other1[taskId];
        //--------------------------
    l75: if (pc[ct]>75||75>=cs) goto l76;
        my_peer=return3[taskId];
    l76: if (pc[ct]>76||76>=cs) goto l77;
        if (my_peer==taskId){
        l77: if (pc[ct]>77||77>=cs) goto l78;
            cs = size[taskId];
        l78: if (pc[ct]>78||78>=cs) goto l79;
            goto end;
        }
        __CPROVER_assume(cs>=79);
        
    l79: if (pc[ct]>79||79>=cs) goto l80;
        test[my_peer]=taskId;
        //--------------------------
    l80: if (pc[ct]>80||80>=cs) goto l81;
        i=5;
    l81: if (pc[ct]>81||81>=cs) goto l82;
        x3[taskId]=taskId;
        //get_a_peer3(taskId);
    l82: if (pc[ct]>82||82>=cs) goto l83;
        score=0;
        //  while (score!=2) {
    l83: if (pc[ct]>83||83>=cs) goto l84;
        x1[taskId]=x3[taskId];
    l84: if (pc[ct]>84||84>=cs) goto l85;
        score1[taskId]=score;
        //protected_object_call2(taskId, 2, 1);
    l85: if (pc[ct]>85||85>=cs) goto l86;
        appel = 2;
    l86: if (pc[ct]>86||86>=cs) goto l87;
        index = 1;
    l87: if (pc[ct]>87||87>=cs) goto l88;
        lock2(taskId);
        if (appel == 0){ //function
            run_function_call2(taskId, index);
        }else if (appel == 1){ //procedure
            __CPROVER_assume(cs>=88);
            
            run_procedure_call2(taskId, index);
        }else if (appel == 2){ //entry
            __CPROVER_assume(cs>=88);
            
            //run_entry_call2(taskId, index);
            static int taskIdEligible2;
            evaluate_barrier2(index);
            if (tabBarrier2[index]==FALSE){
                add_entry_queue2(taskId, index);
                unlock2(taskId);
            l88: if (pc[ct]>88||88>=cs) goto l89;
                __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            l89: if (pc[ct]>89||89>=cs) goto l90;
                remove_entry_queue2(taskId, index);
            }
            __CPROVER_assume(cs>=90);
            
            //run_entry_body2(taskId,index);
            if (index == 1) { //Cooperate
                if (score1[taskId]==0 && MustWait){
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l90: if (pc[ct]>90||90>=cs) goto l91;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l91: if (pc[ct]>91||91>=cs) goto l92;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=92);
                    
                    //run_entry_body2(taskId,index);
                    goto while5;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=92);
                
                if (score1[taskId]==0 && First_Call){
                    a_chameneos = x1[taskId];
                    First_Call = FALSE;
                    score1[taskId]=1;
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l92: if (pc[ct]>92||92>=cs) goto l93;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l93: if (pc[ct]>93||93>=cs) goto l94;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=94);
                    
                    //run_entry_body2(taskId,index);
                    goto while5;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=94);
                
                if (score1[taskId]==0 && !First_Call){
                    b_chameneos=x1[taskId];
                    x_other1[taskId]=a_chameneos;
                    First_Call=TRUE;
                    MustWait=TRUE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=94);
                
                if (score1[taskId]=1 && First_Call){
                    x_other1[taskId]=b_chameneos;
                    MustWait=FALSE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=94);
                
            }else if (index == 2){ //Wait
                __CPROVER_assume(cs>=94);
                
            }
            __CPROVER_assume(cs>=94);
            
            //--------------------------
            taskIdEligible2 = check_and_run_any_entry2();
            transfert2(taskId, taskIdEligible2);
        }
        __CPROVER_assume(cs>=94);
        
        //--------------------------
    while5:
        score=score1[taskId];
        // __CPROVER_assume(score==2);
        if (score!=2){
            cs = 83;
            goto end;
        }
        __CPROVER_assume(cs>=94);
        
        //  }
    l94: if (pc[ct]>94||94>=cs) goto l95;
        return3[taskId]=x_other1[taskId];
        //--------------------------
    l95: if (pc[ct]>95||95>=cs) goto l96;
        my_peer=return3[taskId];
    l96: if (pc[ct]>96||96>=cs) goto l97;
        if (my_peer==taskId){
        l97: if (pc[ct]>97||97>=cs) goto l98;
            cs = size[taskId];
        l98: if (pc[ct]>98||98>=cs) goto l99;
            goto end;
        }
        __CPROVER_assume(cs>=99);
        
    l99: if (pc[ct]>99||99>=cs) goto l100;
        test[my_peer]=taskId;
        //--------------------------
    l100: if (pc[ct]>100||100>=cs) goto l101;
        i=6;
    l101: if (pc[ct]>101||101>=cs) goto l102;
        x3[taskId]=taskId;
        //get_a_peer3(taskId);
    l102: if (pc[ct]>102||102>=cs) goto l103;
        score=0;
        //  while (score!=2) {
    l103: if (pc[ct]>103||103>=cs) goto l104;
        x1[taskId]=x3[taskId];
    l104: if (pc[ct]>104||104>=cs) goto l105;
        score1[taskId]=score;
        //protected_object_call2(taskId, 2, 1);
    l105: if (pc[ct]>105||105>=cs) goto l106;
        appel = 2;
    l106: if (pc[ct]>106||106>=cs) goto l107;
        index = 1;
    l107: if (pc[ct]>107||107>=cs) goto l108;
        lock2(taskId);
        if (appel == 0){ //function
            run_function_call2(taskId, index);
        }else if (appel == 1){ //procedure
            __CPROVER_assume(cs>=108);
            
            run_procedure_call2(taskId, index);
        }else if (appel == 2){ //entry
            __CPROVER_assume(cs>=108);
            
            //run_entry_call2(taskId, index);
            static int taskIdEligible2;
            evaluate_barrier2(index);
            if (tabBarrier2[index]==FALSE){
                add_entry_queue2(taskId, index);
                unlock2(taskId);
            l108: if (pc[ct]>108||108>=cs) goto l109;
                __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
            l109: if (pc[ct]>109||109>=cs) goto l110;
                remove_entry_queue2(taskId, index);
            }
            __CPROVER_assume(cs>=110);
            
            //run_entry_body2(taskId,index);
            if (index == 1) { //Cooperate
                if (score1[taskId]==0 && MustWait){
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l110: if (pc[ct]>110||110>=cs) goto l111;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l111: if (pc[ct]>111||111>=cs) goto l112;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=112);
                    
                    //run_entry_body2(taskId,index);
                    goto while6;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=112);
                
                if (score1[taskId]==0 && First_Call){
                    a_chameneos = x1[taskId];
                    First_Call = FALSE;
                    score1[taskId]=1;
                    NotifyAll = FALSE;
                    //requeue2(taskId, 2);
                    index = 2;
                    evaluate_barrier2(index);
                    if (tabBarrier2[index]==FALSE){
                        add_entry_queue2(taskId, index);
                        unlock2(taskId);
                    l112: if (pc[ct]>112||112>=cs) goto l113;
                        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                    l113: if (pc[ct]>113||113>=cs) goto l114;
                        remove_entry_queue2(taskId, index);
                    }
                    __CPROVER_assume(cs>=114);
                    
                    //run_entry_body2(taskId,index);
                    goto while6;
                    //--------------------------
                    //--------------------------
                }
                __CPROVER_assume(cs>=114);
                
                if (score1[taskId]==0 && !First_Call){
                    b_chameneos=x1[taskId];
                    x_other1[taskId]=a_chameneos;
                    First_Call=TRUE;
                    MustWait=TRUE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=114);
                
                if (score1[taskId]=1 && First_Call){
                    x_other1[taskId]=b_chameneos;
                    MustWait=FALSE;
                    score1[taskId]=2;
                    NotifyAll=TRUE;
                }
                __CPROVER_assume(cs>=114);
                
            }else if (index == 2){ //Wait
                __CPROVER_assume(cs>=114);
                
            }
            __CPROVER_assume(cs>=114);
            
            //--------------------------
            taskIdEligible2 = check_and_run_any_entry2();
            transfert2(taskId, taskIdEligible2);
        }
        __CPROVER_assume(cs>=114);
        
        //--------------------------
    while6:
        score=score1[taskId];
        // __CPROVER_assume(score==2);
        if (score!=2){
            cs = 103;
            goto end;
        }
        __CPROVER_assume(cs>=114);
        
        //  }
    l114: if (pc[ct]>114||114>=cs) goto l115;
        return3[taskId]=x_other1[taskId];
        //--------------------------
    l115: if (pc[ct]>115||115>=cs) goto l116;
        my_peer=return3[taskId];
    l116: if (pc[ct]>116||116>=cs) goto l117;
        if (my_peer==taskId){
        l117: if (pc[ct]>117||117>=cs) goto l118;
            cs = size[taskId];
        l118: if (pc[ct]>118||118>=cs) goto l119;
            goto end;
        }
        __CPROVER_assume(cs>=119);
        
    l119: if (pc[ct]>119||119>=cs) goto end;
        test[my_peer]=taskId;
        //--------------------------
        //  }
    end:
        return;
    }
    
    
/*
void t_five(){
    static int taskId=5;
    static int taskIdEligible;
    static int my_peer = 0;
 

void t_six(){
    static int taskId=6;
    static int taskIdEligible;
    static int my_peer = 0;
 

void t_seven(){
    static int taskId=7;
    static int taskIdEligible;
    static int my_peer = 0;
 

void t_eight(){
    static int taskId=8;
    static int taskIdEligible;
    static int my_peer = 0;
 

void t_nine(){
    static int taskId=9;
    static int taskIdEligible;
    static int my_peer = 0;
    


*/


void init(){
    pc[0] = 0;
    size[0] = 0;
    
    pc[1] = 0;
    pc[2] = 0;
    pc[3] = 0;
    pc[4] = 0;
  /*  pc[5] = 0;
    pc[6] = 0;
    pc[7] = 0;
    pc[8] = 0;
    pc[9] = 0;*/
    
    int s = 120;

    size[1] = s;
    size[2] = s;
    size[3] = s;
    size[4] = s;
  /*  size[5] = s;
    size[6] = s;
    size[7] = s;
    size[8] = s;
    size[9] = s;*/
    
    int t1, t2, t3, t4, t5, t6, t7, t8, t9;
    
    create(t1, 1);
    create(t2, 2);
    create(t3, 3);
    create(t4, 4);
  /*  create(t5, 5);
    create(t6, 6);
    create(t7, 7);
    create(t8, 8);
    create(t9, 9);*/
}

int main(){
    init();
    
    for (;;) {
        ct = 1;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_one();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
     
        ct = 2;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_two();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        ct = 3;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_three();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        ct = 4;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_four();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
  /*      ct = 5;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_five();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        ct = 6;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_six();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        ct = 7;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_seven();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }

        ct = 8;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_eight();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }

        ct = 9;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_nine();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        */
        
        __CPROVER_assert(tabEntryCount2[2]<2, "OK tabEntryCount2[2]<2");
        
        if (dead>0 && dead<NB_TASK){
            __CPROVER_assert(dead+taskInWait<NB_TASK, "OK dead+taskInWait>=NB_TASK");
        }
        
        if (pc[1] == size[1] && pc[2] == size[2] && pc[3] == size[3] && pc[4] == size[4]){ // && pc[5] == size[5] && pc[6] == size[6]){
 //           pragma Assert(test(test(I))=I, "erreur assert pragma");

            __CPROVER_assert(test[test[1]]==1, "OK erreur assert test[test[1]]==1");
            __CPROVER_assert(test[test[2]]==2, "OK erreur assert test[test[2]]==2");
            __CPROVER_assert(test[test[3]]==3, "OK erreur assert test[test[3]]==3");
            __CPROVER_assert(test[test[4]]==4, "OK erreur assert test[test[4]]==4");

            __CPROVER_assert(FALSE, "OK");
        }
    }
    
    __CPROVER_assert(FALSE, "KO");
    
}
