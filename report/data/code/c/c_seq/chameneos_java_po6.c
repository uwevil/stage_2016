#include <stdio.h>
#include <assert.h>

#define TRUE 1
#define FALSE 0
#define FREE -1

#define NB_TASK 4 //without main task

int const nb_request = 6;

/*-------------------------------------------------------------*/
int active[NB_TASK+1] = {TRUE, FALSE, FALSE, FALSE, FALSE};//, FALSE, FALSE, FALSE, FALSE, FALSE};
int cs, ct;
int pc[NB_TASK+1];
int size[NB_TASK+1];
int dead = 0;

void create(int t, int id){
    active[id] = TRUE;
    t = id;
}
/*---------------------------------------------------------------------*/

/*-------------------------------------------------------------*/
#define NB_ENTRY2 2
#define NB_PROCEDURE2 0
#define NB_FUNCTION2 0

int tabStatus2[NB_TASK+1] = {TRUE, TRUE, TRUE, TRUE, TRUE}; //, TRUE, TRUE, TRUE, TRUE, TRUE};
int tabWaiting2[NB_TASK+1] = {0, 0, 0, 0, 0}; //, 0, 0, 0, 0, 0};
int tabBarrier2[NB_ENTRY2+1] = {FALSE, FALSE, FALSE};

int currentTask2=1;

int tabEntryCount2[NB_ENTRY2+1] = {0,0,0};
int taskInWait = 0;

int externalLock2 = FREE;

void lock2(int id){
    __CPROVER_assume(externalLock2==FREE);
    externalLock2=id;
}

void transfert2(int id, int newId){
    __CPROVER_assert(externalLock2==id, "OKexternalLock2!=id");
    externalLock2=newId;
}

void unlock2(int id){
    __CPROVER_assert(externalLock2==id, "OKexternalLock2!=id");
    externalLock2=FREE;
}
/*---------------------------------------------------------------------------*/

#define BLUE 1
#define RED 2
#define YELLOW 3

int First_Call = TRUE;
int MustWait = FALSE;
int a_chameneos, b_chameneos;
int NotifyAll = FALSE;

typedef int colour;

int firstCall = TRUE;
colour aColour;
colour bColour;

int test[NB_TASK+1];

int x1[NB_TASK+1];
int x_other1[NB_TASK+1];
int score1[NB_TASK+1];

int x2[NB_TASK+1];
int x_other2[NB_TASK+1];
int score2[NB_TASK+1];

int x3[NB_TASK+1];
int return3[NB_TASK+1];

void protected_object_call2(int taskId, int appel, int index);

void get_a_peer3(int taskId){
    int score=0;
    while (score!=2) {
        x1[taskId]=x3[taskId];
        score1[taskId]=score;
        protected_object_call2(taskId, 2, 1);
        score=score1[taskId];
    }
    return3[taskId]=x_other1[taskId];
}

/*---------------------------------------------------------------------------*/

void evaluate_barrier2(int index){
    if (index == 1){ //Cooperate
        tabBarrier2[index] = TRUE;
    }else if (index == 2){ //Wait
        tabBarrier2[index] = NotifyAll;
    }
}

void reevaluate_barrier2(){
    tabBarrier2[1] = TRUE; //Cooperate
    tabBarrier2[2] = NotifyAll; //Wait
}

void setTabStatusOn2(int taskId){
    tabStatus2[taskId] = TRUE;
    taskInWait = taskInWait - 1;
}

void setTabStatusOff2(int taskId){
    tabStatus2[taskId] = FALSE;
    taskInWait = taskInWait + 1;
    __CPROVER_assert(taskInWait<NB_TASK, "OKdeadlock2");
}


void add_entry_queue2(int taskId, int index){
    tabEntryCount2[index] = tabEntryCount2[index]+1;
    tabWaiting2[taskId] = index;
    setTabStatusOff2(taskId);
}

void remove_entry_queue2(int taskId, int index){
    tabEntryCount2[index] = tabEntryCount2[index]-1;
    tabWaiting2[taskId] = 0;
}

int current_task_eligible2(){ //return taskId
    int j;
    for (j=1; j<=NB_TASK;j++){
        if (tabWaiting2[currentTask2]!=0&&tabBarrier2[tabWaiting2[currentTask2]]){
            return currentTask2;
        }
        currentTask2 = (currentTask2 % NB_TASK)+1;
    }
    return 0;
}

void run_entry_body2(int taskId, int index);

void requeue2(int taskId, int index){ //requeue to another or same entry queue
    evaluate_barrier2(index);
    
    if (tabBarrier2[index]==FALSE){
        add_entry_queue2(taskId, index);
        unlock2(taskId);
        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        remove_entry_queue2(taskId, index);
    }
    run_entry_body2(taskId,index);
}

void run_entry_body2(int taskId, int index){
    if (index == 1) { //Cooperate
        if (score1[taskId]==0 && MustWait){
            NotifyAll = FALSE;
            requeue2(taskId, 2);
        }
        if (score1[taskId]==0 && First_Call){
            a_chameneos = x1[taskId];
            First_Call = FALSE;
            score1[taskId]=1;
            NotifyAll = FALSE;
            requeue2(taskId, 2);
        }
        if (score1[taskId]==0 && !First_Call){
            b_chameneos=x1[taskId];
            x_other1[taskId]=a_chameneos;
            First_Call=TRUE;
            MustWait=TRUE;
            score1[taskId]=2;
            NotifyAll=TRUE;
        }
        if (score1[taskId]=1 && First_Call){
            x_other1[taskId]=b_chameneos;
            MustWait=FALSE;
            score1[taskId]=2;
            NotifyAll=TRUE;
        }
    }else if (index == 2){ //Wait
    }
}

int check_and_run_any_entry2(){
    reevaluate_barrier2();
    int taskIdEligible2 = current_task_eligible2();
    if (taskIdEligible2==0){
        return FREE;
    }
    setTabStatusOn2(taskIdEligible2);
    return taskIdEligible2;
}

void run_function_call2(taskId, index){
}

void run_procedure_call2(taskId, index){
}

void run_entry_call2(int taskId, int index){
    int taskIdEligible2;
    evaluate_barrier2(index);
    
    if (tabBarrier2[index]==FALSE){
        add_entry_queue2(taskId, index);
        unlock2(taskId);
        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        remove_entry_queue2(taskId, index);
    }
    run_entry_body2(taskId,index);
    taskIdEligible2 = check_and_run_any_entry2();
    transfert2(taskId, taskIdEligible2);
}

void protected_object_call2(int taskId, int appel, int index){
    lock2(taskId);
    
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        run_entry_call2(taskId, index);
    }
}

/*---------------------------------------------------------------------*/

/*---------------------------------------------------------------------*/


void t_one(){
    static int taskId=1;
    static int taskIdEligible;
    static int my_peer = 0;
    static int i;
    static int score;
    static int appel;
    static int index;
    static int taskIdEligible2;

    
l0: if (pc[ct]>0||0>=cs) goto l1;
    i = 1;
    // for (i = 1; i<=nb_request; i++){
l1: if (pc[ct]>1||1>=cs) goto l2;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
l2: if (pc[ct]>2||2>=cs) goto l3;
    score=0;
    //  while (score!=2) {
l3: if (pc[ct]>3||3>=cs) goto l4;
    x1[taskId]=x3[taskId];
l4: if (pc[ct]>4||4>=cs) goto l5;
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
l5: if (pc[ct]>5||5>=cs) goto l6;
    appel = 2;
l6: if (pc[ct]>6||6>=cs) goto l7;
    index = 1;
l7: if (pc[ct]>7||7>=cs) goto l8;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=8);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=8);
        
        //run_entry_call2(taskId, index);
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l8: if (pc[ct]>8||8>=cs) goto l9;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l9: if (pc[ct]>9||9>=cs) goto l10;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=10);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l10: if (pc[ct]>10||10>=cs) goto l11;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l11: if (pc[ct]>11||11>=cs) goto l12;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=12);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && First_Call){
                __CPROVER_assume(cs>=12);
                
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l12: if (pc[ct]>12||12>=cs) goto l13;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l13: if (pc[ct]>13||13>=cs) goto l14;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=14);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && !First_Call){
                __CPROVER_assume(cs>=14);
                
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }else if (score1[taskId]=1 && First_Call){
                __CPROVER_assume(cs>=14);
                
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=14);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=14);
            
        }
        __CPROVER_assume(cs>=14);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=14);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 3;
        goto end;
    }
    __CPROVER_assume(cs>=14);
    
    //  }
l14: if (pc[ct]>14||14>=cs) goto l15;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l15: if (pc[ct]>15||15>=cs) goto l16;
    my_peer=return3[taskId];
l16: if (pc[ct]>16||16>=cs) goto l17;
    if (my_peer==taskId){
    l17: if (pc[ct]>17||17>=cs) goto l18;
        cs = size[taskId];
    l18: if (pc[ct]>18||18>=cs) goto l19;
        goto end;
    }
    __CPROVER_assume(cs>=19);
    
l19: if (pc[ct]>19||19>=cs) goto l20;
    test[taskId]=my_peer;
    //--------------------------
l20: if (pc[ct]>20||20>=cs) goto l21;
    i = 2;
l21: if (pc[ct]>21||21>=cs) goto l22;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
l22: if (pc[ct]>22||22>=cs) goto l23;
    score=0;
    //  while (score!=2) {
l23: if (pc[ct]>23||23>=cs) goto l24;
    x1[taskId]=x3[taskId];
l24: if (pc[ct]>24||24>=cs) goto l25;
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
l25: if (pc[ct]>25||25>=cs) goto l26;
    appel = 2;
l26: if (pc[ct]>26||26>=cs) goto l27;
    index = 1;
l27: if (pc[ct]>27||27>=cs) goto l28;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=28);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=28);
        
        //run_entry_call2(taskId, index);
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l28: if (pc[ct]>28||28>=cs) goto l29;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l29: if (pc[ct]>29||29>=cs) goto l30;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=30);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l30: if (pc[ct]>30||30>=cs) goto l31;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l31: if (pc[ct]>31||31>=cs) goto l32;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=32);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && First_Call){
                __CPROVER_assume(cs>=32);
                
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l32: if (pc[ct]>32||32>=cs) goto l33;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l33: if (pc[ct]>33||33>=cs) goto l34;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=34);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && !First_Call){
                __CPROVER_assume(cs>=34);
                
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }else if (score1[taskId]=1 && First_Call){
                __CPROVER_assume(cs>=34);
                
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=34);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=34);
            
        }
        __CPROVER_assume(cs>=34);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=34);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 23;
        goto end;
    }
    __CPROVER_assume(cs>=34);
    
    //  }
l34: if (pc[ct]>34||34>=cs) goto l35;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l35: if (pc[ct]>35||35>=cs) goto l36;
    my_peer=return3[taskId];
l36: if (pc[ct]>36||36>=cs) goto l37;
    if (my_peer==taskId){
    l37: if (pc[ct]>37||37>=cs) goto l38;
        cs = size[taskId];
    l38: if (pc[ct]>38||38>=cs) goto l39;
        goto end;
    }
    __CPROVER_assume(cs>=39);
    
l39: if (pc[ct]>39||39>=cs) goto end;
    test[taskId]=my_peer;
    //--------------------------
    //  }
end:
    return;
}


void t_two(){
    static int taskId=2;
    static int taskIdEligible;
    static int my_peer = 0;
    static int i;
    static int score;
    static int appel;
    static int index;
    static int taskIdEligible2;
    
l0: if (pc[ct]>0||0>=cs) goto l1;
    i = 1;
    // for (i = 1; i<=nb_request; i++){
l1: if (pc[ct]>1||1>=cs) goto l2;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
l2: if (pc[ct]>2||2>=cs) goto l3;
    score=0;
    //  while (score!=2) {
l3: if (pc[ct]>3||3>=cs) goto l4;
    x1[taskId]=x3[taskId];
l4: if (pc[ct]>4||4>=cs) goto l5;
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
l5: if (pc[ct]>5||5>=cs) goto l6;
    appel = 2;
l6: if (pc[ct]>6||6>=cs) goto l7;
    index = 1;
l7: if (pc[ct]>7||7>=cs) goto l8;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=8);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=8);
        
        //run_entry_call2(taskId, index);
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l8: if (pc[ct]>8||8>=cs) goto l9;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l9: if (pc[ct]>9||9>=cs) goto l10;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=10);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l10: if (pc[ct]>10||10>=cs) goto l11;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l11: if (pc[ct]>11||11>=cs) goto l12;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=12);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && First_Call){
                __CPROVER_assume(cs>=12);
                
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l12: if (pc[ct]>12||12>=cs) goto l13;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l13: if (pc[ct]>13||13>=cs) goto l14;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=14);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && !First_Call){
                __CPROVER_assume(cs>=14);
                
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }else if (score1[taskId]=1 && First_Call){
                __CPROVER_assume(cs>=14);
                
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=14);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=14);
            
        }
        __CPROVER_assume(cs>=14);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=14);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 3;
        goto end;
    }
    __CPROVER_assume(cs>=14);
    
    //  }
l14: if (pc[ct]>14||14>=cs) goto l15;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l15: if (pc[ct]>15||15>=cs) goto l16;
    my_peer=return3[taskId];
l16: if (pc[ct]>16||16>=cs) goto l17;
    if (my_peer==taskId){
    l17: if (pc[ct]>17||17>=cs) goto l18;
        cs = size[taskId];
    l18: if (pc[ct]>18||18>=cs) goto l19;
        goto end;
    }
    __CPROVER_assume(cs>=19);
    
l19: if (pc[ct]>19||19>=cs) goto l20;
    test[taskId]=my_peer;
    //--------------------------
l20: if (pc[ct]>20||20>=cs) goto l21;
    i = 2;
l21: if (pc[ct]>21||21>=cs) goto l22;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
l22: if (pc[ct]>22||22>=cs) goto l23;
    score=0;
    //  while (score!=2) {
l23: if (pc[ct]>23||23>=cs) goto l24;
    x1[taskId]=x3[taskId];
l24: if (pc[ct]>24||24>=cs) goto l25;
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
l25: if (pc[ct]>25||25>=cs) goto l26;
    appel = 2;
l26: if (pc[ct]>26||26>=cs) goto l27;
    index = 1;
l27: if (pc[ct]>27||27>=cs) goto l28;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=28);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=28);
        
        //run_entry_call2(taskId, index);
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l28: if (pc[ct]>28||28>=cs) goto l29;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l29: if (pc[ct]>29||29>=cs) goto l30;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=30);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l30: if (pc[ct]>30||30>=cs) goto l31;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l31: if (pc[ct]>31||31>=cs) goto l32;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=32);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && First_Call){
                __CPROVER_assume(cs>=32);
                
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l32: if (pc[ct]>32||32>=cs) goto l33;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l33: if (pc[ct]>33||33>=cs) goto l34;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=34);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && !First_Call){
                __CPROVER_assume(cs>=34);
                
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }else if (score1[taskId]=1 && First_Call){
                __CPROVER_assume(cs>=34);
                
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=34);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=34);
            
        }
        __CPROVER_assume(cs>=34);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=34);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 23;
        goto end;
    }
    __CPROVER_assume(cs>=34);
    
    //  }
l34: if (pc[ct]>34||34>=cs) goto l35;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l35: if (pc[ct]>35||35>=cs) goto l36;
    my_peer=return3[taskId];
l36: if (pc[ct]>36||36>=cs) goto l37;
    if (my_peer==taskId){
    l37: if (pc[ct]>37||37>=cs) goto l38;
        cs = size[taskId];
    l38: if (pc[ct]>38||38>=cs) goto l39;
        goto end;
    }
    __CPROVER_assume(cs>=39);
    
l39: if (pc[ct]>39||39>=cs) goto end;
    test[taskId]=my_peer;
    //--------------------------
    //  }
end:
    return;
}


    
void t_three(){
    static int taskId=3;
    static int taskIdEligible;
    static int my_peer = 0;
    static int i;
    static int score;
    static int appel;
    static int index;
    static int taskIdEligible2;
    
l0: if (pc[ct]>0||0>=cs) goto l1;
    i = 1;
    // for (i = 1; i<=nb_request; i++){
l1: if (pc[ct]>1||1>=cs) goto l2;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
l2: if (pc[ct]>2||2>=cs) goto l3;
    score=0;
    //  while (score!=2) {
l3: if (pc[ct]>3||3>=cs) goto l4;
    x1[taskId]=x3[taskId];
l4: if (pc[ct]>4||4>=cs) goto l5;
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
l5: if (pc[ct]>5||5>=cs) goto l6;
    appel = 2;
l6: if (pc[ct]>6||6>=cs) goto l7;
    index = 1;
l7: if (pc[ct]>7||7>=cs) goto l8;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=8);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=8);
        
        //run_entry_call2(taskId, index);
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l8: if (pc[ct]>8||8>=cs) goto l9;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l9: if (pc[ct]>9||9>=cs) goto l10;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=10);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l10: if (pc[ct]>10||10>=cs) goto l11;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l11: if (pc[ct]>11||11>=cs) goto l12;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=12);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && First_Call){
                __CPROVER_assume(cs>=12);
                
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l12: if (pc[ct]>12||12>=cs) goto l13;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l13: if (pc[ct]>13||13>=cs) goto l14;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=14);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && !First_Call){
                __CPROVER_assume(cs>=14);
                
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }else if (score1[taskId]=1 && First_Call){
                __CPROVER_assume(cs>=14);
                
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=14);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=14);
            
        }
        __CPROVER_assume(cs>=14);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=14);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 3;
        goto end;
    }
    __CPROVER_assume(cs>=14);
    
    //  }
l14: if (pc[ct]>14||14>=cs) goto l15;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l15: if (pc[ct]>15||15>=cs) goto l16;
    my_peer=return3[taskId];
l16: if (pc[ct]>16||16>=cs) goto l17;
    if (my_peer==taskId){
    l17: if (pc[ct]>17||17>=cs) goto l18;
        cs = size[taskId];
    l18: if (pc[ct]>18||18>=cs) goto l19;
        goto end;
    }
    __CPROVER_assume(cs>=19);
    
l19: if (pc[ct]>19||19>=cs) goto l20;
    test[taskId]=my_peer;
    //--------------------------
l20: if (pc[ct]>20||20>=cs) goto l21;
    i = 2;
l21: if (pc[ct]>21||21>=cs) goto l22;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
l22: if (pc[ct]>22||22>=cs) goto l23;
    score=0;
    //  while (score!=2) {
l23: if (pc[ct]>23||23>=cs) goto l24;
    x1[taskId]=x3[taskId];
l24: if (pc[ct]>24||24>=cs) goto l25;
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
l25: if (pc[ct]>25||25>=cs) goto l26;
    appel = 2;
l26: if (pc[ct]>26||26>=cs) goto l27;
    index = 1;
l27: if (pc[ct]>27||27>=cs) goto l28;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=28);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=28);
        
        //run_entry_call2(taskId, index);
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l28: if (pc[ct]>28||28>=cs) goto l29;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l29: if (pc[ct]>29||29>=cs) goto l30;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=30);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l30: if (pc[ct]>30||30>=cs) goto l31;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l31: if (pc[ct]>31||31>=cs) goto l32;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=32);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && First_Call){
                __CPROVER_assume(cs>=32);
                
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l32: if (pc[ct]>32||32>=cs) goto l33;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l33: if (pc[ct]>33||33>=cs) goto l34;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=34);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && !First_Call){
                __CPROVER_assume(cs>=34);
                
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }else if (score1[taskId]=1 && First_Call){
                __CPROVER_assume(cs>=34);
                
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=34);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=34);
            
        }
        __CPROVER_assume(cs>=34);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=34);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 23;
        goto end;
    }
    __CPROVER_assume(cs>=34);
    
    //  }
l34: if (pc[ct]>34||34>=cs) goto l35;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l35: if (pc[ct]>35||35>=cs) goto l36;
    my_peer=return3[taskId];
l36: if (pc[ct]>36||36>=cs) goto l37;
    if (my_peer==taskId){
    l37: if (pc[ct]>37||37>=cs) goto l38;
        cs = size[taskId];
    l38: if (pc[ct]>38||38>=cs) goto l39;
        goto end;
    }
    __CPROVER_assume(cs>=39);
    
l39: if (pc[ct]>39||39>=cs) goto end;
    test[taskId]=my_peer;
    //--------------------------
    //  }
end:
    return;
}

    
void t_four(){
    static int taskId=4;
    static int taskIdEligible;
    static int my_peer = 0;
    static int i;
    static int score;
    static int appel;
    static int index;
    static int taskIdEligible2;
    
l0: if (pc[ct]>0||0>=cs) goto l1;
    i = 1;
    // for (i = 1; i<=nb_request; i++){
l1: if (pc[ct]>1||1>=cs) goto l2;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
l2: if (pc[ct]>2||2>=cs) goto l3;
    score=0;
    //  while (score!=2) {
l3: if (pc[ct]>3||3>=cs) goto l4;
    x1[taskId]=x3[taskId];
l4: if (pc[ct]>4||4>=cs) goto l5;
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
l5: if (pc[ct]>5||5>=cs) goto l6;
    appel = 2;
l6: if (pc[ct]>6||6>=cs) goto l7;
    index = 1;
l7: if (pc[ct]>7||7>=cs) goto l8;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=8);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=8);
        
        //run_entry_call2(taskId, index);
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l8: if (pc[ct]>8||8>=cs) goto l9;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l9: if (pc[ct]>9||9>=cs) goto l10;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=10);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l10: if (pc[ct]>10||10>=cs) goto l11;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l11: if (pc[ct]>11||11>=cs) goto l12;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=12);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && First_Call){
                __CPROVER_assume(cs>=12);
                
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l12: if (pc[ct]>12||12>=cs) goto l13;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l13: if (pc[ct]>13||13>=cs) goto l14;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=14);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && !First_Call){
                __CPROVER_assume(cs>=14);
                
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }else if (score1[taskId]=1 && First_Call){
                __CPROVER_assume(cs>=14);
                
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=14);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=14);
            
        }
        __CPROVER_assume(cs>=14);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=14);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 3;
        goto end;
    }
    __CPROVER_assume(cs>=14);
    
    //  }
l14: if (pc[ct]>14||14>=cs) goto l15;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l15: if (pc[ct]>15||15>=cs) goto l16;
    my_peer=return3[taskId];
l16: if (pc[ct]>16||16>=cs) goto l17;
    if (my_peer==taskId){
    l17: if (pc[ct]>17||17>=cs) goto l18;
        cs = size[taskId];
    l18: if (pc[ct]>18||18>=cs) goto l19;
        goto end;
    }
    __CPROVER_assume(cs>=19);
    
l19: if (pc[ct]>19||19>=cs) goto l20;
    test[taskId]=my_peer;
    //--------------------------
l20: if (pc[ct]>20||20>=cs) goto l21;
    i = 2;
l21: if (pc[ct]>21||21>=cs) goto l22;
    x3[taskId]=taskId;
    //get_a_peer3(taskId);
l22: if (pc[ct]>22||22>=cs) goto l23;
    score=0;
    //  while (score!=2) {
l23: if (pc[ct]>23||23>=cs) goto l24;
    x1[taskId]=x3[taskId];
l24: if (pc[ct]>24||24>=cs) goto l25;
    score1[taskId]=score;
    //protected_object_call2(taskId, 2, 1);
l25: if (pc[ct]>25||25>=cs) goto l26;
    appel = 2;
l26: if (pc[ct]>26||26>=cs) goto l27;
    index = 1;
l27: if (pc[ct]>27||27>=cs) goto l28;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=28);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=28);
        
        //run_entry_call2(taskId, index);
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l28: if (pc[ct]>28||28>=cs) goto l29;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l29: if (pc[ct]>29||29>=cs) goto l30;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=30);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l30: if (pc[ct]>30||30>=cs) goto l31;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l31: if (pc[ct]>31||31>=cs) goto l32;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=32);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && First_Call){
                __CPROVER_assume(cs>=32);
                
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l32: if (pc[ct]>32||32>=cs) goto l33;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l33: if (pc[ct]>33||33>=cs) goto l34;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=34);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }else if (score1[taskId]==0 && !First_Call){
                __CPROVER_assume(cs>=34);
                
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }else if (score1[taskId]=1 && First_Call){
                __CPROVER_assume(cs>=34);
                
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=34);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=34);
            
        }
        __CPROVER_assume(cs>=34);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=34);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 23;
        goto end;
    }
    __CPROVER_assume(cs>=34);
    
    //  }
l34: if (pc[ct]>34||34>=cs) goto l35;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l35: if (pc[ct]>35||35>=cs) goto l36;
    my_peer=return3[taskId];
l36: if (pc[ct]>36||36>=cs) goto l37;
    if (my_peer==taskId){
    l37: if (pc[ct]>37||37>=cs) goto l38;
        cs = size[taskId];
    l38: if (pc[ct]>38||38>=cs) goto l39;
        goto end;
    }
    __CPROVER_assume(cs>=39);
    
l39: if (pc[ct]>39||39>=cs) goto end;
    test[taskId]=my_peer;
    //--------------------------
    //  }
end:
    return;
}


/*
void t_five(){
    static int taskId=5;
    static int taskIdEligible;
    static int my_peer = 0;
 

void t_six(){
    static int taskId=6;
    static int taskIdEligible;
    static int my_peer = 0;
 

void t_seven(){
    static int taskId=7;
    static int taskIdEligible;
    static int my_peer = 0;
 

void t_eight(){
    static int taskId=8;
    static int taskIdEligible;
    static int my_peer = 0;
 

void t_nine(){
    static int taskId=9;
    static int taskIdEligible;
    static int my_peer = 0;
    


*/


void init(){
    pc[0] = 0;
    size[0] = 0;
    
    pc[1] = 0;
    pc[2] = 0;
    pc[3] = 0;
    pc[4] = 0;
  /*  pc[5] = 0;
    pc[6] = 0;
    pc[7] = 0;
    pc[8] = 0;
    pc[9] = 0;*/
    
    int s = 40;

    size[1] = s;
    size[2] = s;
    size[3] = s;
    size[4] = s;
  /*  size[5] = s;
    size[6] = s;
    size[7] = s;
    size[8] = s;
    size[9] = s;*/
    
    int t1, t2, t3, t4, t5, t6, t7, t8, t9;
    
    create(t1, 1);
    create(t2, 2);
    create(t3, 3);
    create(t4, 4);
  /*  create(t5, 5);
    create(t6, 6);
    create(t7, 7);
    create(t8, 8);
    create(t9, 9);*/
}

int main(){
    init();
    
    for (;;) {
        ct = 1;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_one();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
     
        ct = 2;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_two();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        ct = 3;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_three();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        ct = 4;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_four();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
  /*      ct = 5;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_five();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        ct = 6;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_six();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        ct = 7;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_seven();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }

        ct = 8;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_eight();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }

        ct = 9;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_nine();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        */
        
        if (dead>0 && dead<NB_TASK){
            __CPROVER_assert(dead+taskInWait<NB_TASK, "OK dead+taskInWait>=NB_TASK");
        }
        
        if (pc[1] == size[1] && pc[2] == size[2] && pc[3] == size[3] && pc[4] == size[4]){ // && pc[5] == size[5] && pc[6] == size[6]){
 //           pragma Assert(test(test(I))=I, "erreur assert pragma");

            __CPROVER_assert(test[test[1]]==1, "OK erreur assert test[test[1]]==1");
            __CPROVER_assert(test[test[2]]==2, "OK erreur assert test[test[2]]==2");
            __CPROVER_assert(test[test[3]]==3, "OK erreur assert test[test[3]]==3");
            __CPROVER_assert(test[test[4]]==4, "OK erreur assert test[test[4]]==4");

            __CPROVER_assert(FALSE, "OK");
        }
    }
    
    __CPROVER_assert(FALSE, "KO");
    
}
