#include <stdio.h>
#include <assert.h>

#define TRUE 1
#define FALSE 0
#define FREE -1

#define NB_TASK 5 //without main task
#define NB_ENTRY 2
#define NB_PROCEDURE 1
#define NB_FUNCTION 0

/*-------------------------------------------------------------*/
int active[NB_TASK+1] = {FALSE, FALSE, FALSE, FALSE, FALSE, FALSE};
int cs, ct;
int pc[NB_TASK+1];
int size[NB_TASK+1];
int dead = 0;

void create(int t, int id){
    active[id] = TRUE;
    t = id;
}
/*-------------------------------------------------------------*/
int tabStatus[NB_TASK+1] = {TRUE, TRUE, TRUE, TRUE, TRUE, TRUE};
int tabWaiting[NB_TASK+1] = {0, 0, 0, 0, 0, 0};
int tabBarrier[NB_ENTRY+1] = {FALSE, FALSE, FALSE};

int currentTask=1;

int tabEntryCount[NB_ENTRY+1] = {0,0,0};
int taskInWait = 0;

int externalLock = FREE;

void lock(int id){
    __CPROVER_assume(externalLock==FREE);
    externalLock=id;
}

void transfert(int id, int newId){
    __CPROVER_assert(externalLock==id, "OKexternalLock!=id");
    externalLock=newId;
}

void unlock(int id){
    __CPROVER_assert(externalLock==id, "OKexternalLock!=id");
    externalLock=FREE;
}
/*---------------------------------------------------------------------------*/

int flush_count = 0;

int available[NB_TASK+1] = {TRUE, TRUE, TRUE, TRUE, TRUE, TRUE};

int eid1[NB_TASK+1];
int eid2[NB_TASK+1];
int pid1[NB_TASK+1];

/*---------------------------------------------------------------------------*/

void evaluate_barrier(int index){
    if (index == 1){ //Get_pair
        tabBarrier[index] = TRUE;
    }else if (index == 2){ //Waiting
        tabBarrier[index] = flush_count >0;
    }
}

void reevaluate_barrier(){
    tabBarrier[1] = TRUE; //Cooperate
    tabBarrier[2] = flush_count>0; //Waiting
}

void setTabStatusOn(int taskId){
    tabStatus[taskId] = TRUE;
    taskInWait = taskInWait - 1;
}

void setTabStatusOff(int taskId){
    tabStatus[taskId] = FALSE;
    taskInWait = taskInWait + 1;
    __CPROVER_assert(taskInWait<NB_TASK, "OKdeadlock");
}


void add_entry_queue(int taskId, int index){
    tabEntryCount[index] = tabEntryCount[index]+1;
    //  taskInWait = taskInWait + 1;
    //  __CPROVER_assert(taskInWait<NB_TASK, "OKdeadlock");
    
    tabWaiting[taskId] = index;
    //    tabStatus[taskId] = FALSE;
    setTabStatusOff(taskId);
}

void remove_entry_queue(int taskId, int index){
    tabEntryCount[index] = tabEntryCount[index]-1;
    //   taskInWait = taskInWait - 1;
    
    tabWaiting[taskId] = 0;
}

int current_task_eligible(){ //return taskId
    int j;
    for (j=1; j<=NB_TASK;j++){
        if (tabWaiting[currentTask]!=0&&tabBarrier[tabWaiting[currentTask]]){
            return currentTask;
        }
        currentTask = (currentTask % NB_TASK)+1;
    }
    return 0;
}

void run_entry_body(int taskId, int index);

void requeue(int taskId, int index, int with_abort){ //requeue to another or same entry queue
    evaluate_barrier(index);
    
    if (tabBarrier[index]==FALSE && !with_abort){
        add_entry_queue(taskId, index);
        unlock(taskId);
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        remove_entry_queue(taskId, index);
    }
    
    if (tabBarrier[index]==TRUE){
        run_entry_body(taskId,index);
    }
}


void run_entry_body(int taskId, int index){
    if (index == 1) { //get_pair
        if (available[eid1[taskId]] && available[(eid1[taskId] +1) % NB_TASK]){
            available[eid1[taskId]] = FALSE;
            available[(eid1[taskId] + 1) % NB_TASK] = FALSE;
        }else{
            eid2[taskId] = eid1[taskId];
            requeue(taskId, 2, TRUE);
        }
    }else if (index == 2){ //please_get_pair
        flush_count = flush_count-1;
        if (available[eid2[taskId]] && available[(eid2[taskId]+1)%NB_TASK]){
            available[eid2[taskId]] = FALSE;
            available[(eid2[taskId]+1)%NB_TASK] = FALSE;
        }else{
            eid2[taskId] = eid2[taskId];
            requeue(taskId, 2, TRUE);
        }
    }
}

int check_and_run_any_entry(){
    reevaluate_barrier();
    int taskIdEligible = current_task_eligible();
    if (taskIdEligible==0){
        return FREE;
    }
   // tabStatus[taskIdEligible] = TRUE;
    setTabStatusOn(taskIdEligible);
    return taskIdEligible;
}

void run_function_call(taskId, index){
}

void run_procedure_call(taskId, index){
    int taskIdEligible;
    if (index == 1){
        available[pid1[taskId]] = TRUE;
        available[(pid1[taskId] + 1) % NB_TASK] = TRUE;
        flush_count = tabEntryCount[2];
    }
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
}

void run_entry_call(int taskId, int index){
    int taskIdEligible;
    evaluate_barrier(index);
    
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        remove_entry_queue(taskId, index);
    }
    run_entry_body(taskId,index);
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
}

void protected_object_call(int taskId, int appel, int index){
    lock(taskId);
    
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        run_entry_call(taskId, index);
    }
}


void t_one(){
    static int taskId=1;
    static int taskIdEligible;
    static int appel = 2;
    static int index = 1;
    
    //protected_object_call(taskId, appel, index)
l0: if (pc[ct]>0||0>=cs) goto l1;
    lock(taskId);
    appel=2;
    index=1;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=1);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=1);
        
        //run_entry_call(taskId, index);
        static int taskIdEligible;
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
        l1: if (pc[ct]>1||1>=cs) goto l2;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        l2: if (pc[ct]>2||2>=cs) goto l3;
            remove_entry_queue(taskId, index);
        }
        __CPROVER_assume(cs>=3);
        
    l3: if (pc[ct]>3||3>=cs) goto l4;
        //run_entry_body(taskId,index);
        if (index == 1) { //get_pair
            if (available[eid1[taskId]] && available[(eid1[taskId] +1) % NB_TASK]){
                available[eid1[taskId]] = FALSE;
                available[(eid1[taskId] + 1) % NB_TASK] = FALSE;
            }else{
                __CPROVER_assume(cs>=4);
                
                //requeue(taskId, 2, TRUE);
                static int with_abort = TRUE;
                evaluate_barrier(index);
                if (tabBarrier[index]==FALSE && !with_abort){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l4: if (pc[ct]>4||4>=cs) goto l5;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l5: if (pc[ct]>5||5>=cs) goto l6;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=6);
                
            l6: if (pc[ct]>6||6>=cs) goto l7;
                if (tabBarrier[index]==TRUE){
                    //run_entry_body(taskId,index);
                    // index = 2;
                    //  goto label1;
                l7: if (pc[ct]>7||7>=cs) goto l8;
                    //run_entry_body(taskId,index);
                    index = 2;
                    cs = 3;
                    goto end;
                }
                __CPROVER_assume(cs>=8);
                
            }
            __CPROVER_assume(cs>=8);
            
        }else if (index == 2){ //please_get_pair
            __CPROVER_assume(cs>=8);
            
            flush_count = flush_count-1;
            if (available[eid2[taskId]] && available[(eid2[taskId]+1)%NB_TASK]){
                available[eid2[taskId]] = FALSE;
                available[(eid2[taskId]+1)%NB_TASK] = FALSE;
            }else{
                __CPROVER_assume(cs>=8);
                
                index = 2;
                //requeue(taskId, 2, TRUE);
                static int with_abort = TRUE;
                evaluate_barrier(index);
                if (tabBarrier[index]==FALSE && !with_abort){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l8: if (pc[ct]>8||8>=cs) goto l9;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l9: if (pc[ct]>9||9>=cs) goto l10;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=10);
                
            l10: if (pc[ct]>10||10>=cs) goto l11;
                if (tabBarrier[index]==TRUE){
                    //run_entry_body(taskId,index);
                    // index = 2;
                    //  goto label1;
                l11: if (pc[ct]>11||11>=cs) goto l12;
                    //run_entry_body(taskId,index);
                    index = 2;
                    cs = 3;
                    goto end;
                }
                __CPROVER_assume(cs>=12);
                
            }
            __CPROVER_assume(cs>=12);
            
        }
        __CPROVER_assume(cs>=12);
        
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    __CPROVER_assume(cs>=12);
    
l12: if (pc[ct]>12||12>=cs) goto end;
    lock(taskId);
    appel=1;
    index=1;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=13);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=13);
        
        run_entry_call(taskId, index);
    }
    __CPROVER_assume(cs>=13);
    
end:
    return;
}


void t_two(){
    static int taskId=2;
    static int taskIdEligible;
    static int appel = 2;
    static int index = 1;
    
    //protected_object_call(taskId, appel, index)
l0: if (pc[ct]>0||0>=cs) goto l1;
    lock(taskId);
    appel=2;
    index=1;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=1);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=1);
        
        //run_entry_call(taskId, index);
        static int taskIdEligible;
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
        l1: if (pc[ct]>1||1>=cs) goto l2;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        l2: if (pc[ct]>2||2>=cs) goto l3;
            remove_entry_queue(taskId, index);
        }
        __CPROVER_assume(cs>=3);
        
    l3: if (pc[ct]>3||3>=cs) goto l4;
        //run_entry_body(taskId,index);
        if (index == 1) { //get_pair
            if (available[eid1[taskId]] && available[(eid1[taskId] +1) % NB_TASK]){
                available[eid1[taskId]] = FALSE;
                available[(eid1[taskId] + 1) % NB_TASK] = FALSE;
            }else{
                __CPROVER_assume(cs>=4);
                
                //requeue(taskId, 2, TRUE);
                static int with_abort = TRUE;
                evaluate_barrier(index);
                if (tabBarrier[index]==FALSE && !with_abort){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l4: if (pc[ct]>4||4>=cs) goto l5;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l5: if (pc[ct]>5||5>=cs) goto l6;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=6);
                
            l6: if (pc[ct]>6||6>=cs) goto l7;
                if (tabBarrier[index]==TRUE){
                    //run_entry_body(taskId,index);
                    // index = 2;
                    //  goto label1;
                l7: if (pc[ct]>7||7>=cs) goto l8;
                    //run_entry_body(taskId,index);
                    index = 2;
                    cs = 3;
                    goto end;
                }
                __CPROVER_assume(cs>=8);
                
            }
            __CPROVER_assume(cs>=8);
            
        }else if (index == 2){ //please_get_pair
            __CPROVER_assume(cs>=8);
            
            flush_count = flush_count-1;
            if (available[eid2[taskId]] && available[(eid2[taskId]+1)%NB_TASK]){
                available[eid2[taskId]] = FALSE;
                available[(eid2[taskId]+1)%NB_TASK] = FALSE;
            }else{
                __CPROVER_assume(cs>=8);
                
                index = 2;
                //requeue(taskId, 2, TRUE);
                static int with_abort = TRUE;
                evaluate_barrier(index);
                if (tabBarrier[index]==FALSE && !with_abort){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l8: if (pc[ct]>8||8>=cs) goto l9;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l9: if (pc[ct]>9||9>=cs) goto l10;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=10);
                
            l10: if (pc[ct]>10||10>=cs) goto l11;
                if (tabBarrier[index]==TRUE){
                    //run_entry_body(taskId,index);
                    // index = 2;
                    //  goto label1;
                l11: if (pc[ct]>11||11>=cs) goto l12;
                    //run_entry_body(taskId,index);
                    index = 2;
                    cs = 3;
                    goto end;
                }
                __CPROVER_assume(cs>=12);
                
            }
            __CPROVER_assume(cs>=12);
            
        }
        __CPROVER_assume(cs>=12);
        
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    __CPROVER_assume(cs>=12);
    
l12: if (pc[ct]>12||12>=cs) goto end;
    lock(taskId);
    appel=1;
    index=1;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=13);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=13);
        
        run_entry_call(taskId, index);
    }
    __CPROVER_assume(cs>=13);
    
end:
    return;
}


void t_three(){
    static int taskId=3;
    static int taskIdEligible;
    static int appel = 2;
    static int index = 1;
    
    //protected_object_call(taskId, appel, index)
l0: if (pc[ct]>0||0>=cs) goto l1;
    lock(taskId);
    appel=2;
    index=1;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=1);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=1);
        
        //run_entry_call(taskId, index);
        static int taskIdEligible;
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
        l1: if (pc[ct]>1||1>=cs) goto l2;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        l2: if (pc[ct]>2||2>=cs) goto l3;
            remove_entry_queue(taskId, index);
        }
        __CPROVER_assume(cs>=3);
        
    l3: if (pc[ct]>3||3>=cs) goto l4;
        //run_entry_body(taskId,index);
        if (index == 1) { //get_pair
            if (available[eid1[taskId]] && available[(eid1[taskId] +1) % NB_TASK]){
                available[eid1[taskId]] = FALSE;
                available[(eid1[taskId] + 1) % NB_TASK] = FALSE;
            }else{
                __CPROVER_assume(cs>=4);
                
                //requeue(taskId, 2, TRUE);
                static int with_abort = TRUE;
                evaluate_barrier(index);
                if (tabBarrier[index]==FALSE && !with_abort){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l4: if (pc[ct]>4||4>=cs) goto l5;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l5: if (pc[ct]>5||5>=cs) goto l6;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=6);
                
            l6: if (pc[ct]>6||6>=cs) goto l7;
                if (tabBarrier[index]==TRUE){
                    //run_entry_body(taskId,index);
                    // index = 2;
                    //  goto label1;
                l7: if (pc[ct]>7||7>=cs) goto l8;
                    //run_entry_body(taskId,index);
                    index = 2;
                    cs = 3;
                    goto end;
                }
                __CPROVER_assume(cs>=8);
                
            }
            __CPROVER_assume(cs>=8);
            
        }else if (index == 2){ //please_get_pair
            __CPROVER_assume(cs>=8);
            
            flush_count = flush_count-1;
            if (available[eid2[taskId]] && available[(eid2[taskId]+1)%NB_TASK]){
                available[eid2[taskId]] = FALSE;
                available[(eid2[taskId]+1)%NB_TASK] = FALSE;
            }else{
                __CPROVER_assume(cs>=8);
                
                index = 2;
                //requeue(taskId, 2, TRUE);
                static int with_abort = TRUE;
                evaluate_barrier(index);
                if (tabBarrier[index]==FALSE && !with_abort){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l8: if (pc[ct]>8||8>=cs) goto l9;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l9: if (pc[ct]>9||9>=cs) goto l10;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=10);
                
            l10: if (pc[ct]>10||10>=cs) goto l11;
                if (tabBarrier[index]==TRUE){
                    //run_entry_body(taskId,index);
                    // index = 2;
                    //  goto label1;
                l11: if (pc[ct]>11||11>=cs) goto l12;
                    //run_entry_body(taskId,index);
                    index = 2;
                    cs = 3;
                    goto end;
                }
                __CPROVER_assume(cs>=12);
                
            }
            __CPROVER_assume(cs>=12);
            
        }
        __CPROVER_assume(cs>=12);
        
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    __CPROVER_assume(cs>=12);
    
l12: if (pc[ct]>12||12>=cs) goto end;
    lock(taskId);
    appel=1;
    index=1;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=13);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=13);
        
        run_entry_call(taskId, index);
    }
    __CPROVER_assume(cs>=13);
    
end:
    return;
}


void t_four(){
    static int taskId=4;
    static int taskIdEligible;
    static int appel = 2;
    static int index = 1;
    
    //protected_object_call(taskId, appel, index)
l0: if (pc[ct]>0||0>=cs) goto l1;
    lock(taskId);
    appel=2;
    index=1;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=1);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=1);
        
        //run_entry_call(taskId, index);
        static int taskIdEligible;
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
        l1: if (pc[ct]>1||1>=cs) goto l2;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        l2: if (pc[ct]>2||2>=cs) goto l3;
            remove_entry_queue(taskId, index);
        }
        __CPROVER_assume(cs>=3);
        
    l3: if (pc[ct]>3||3>=cs) goto l4;
        //run_entry_body(taskId,index);
        if (index == 1) { //get_pair
            if (available[eid1[taskId]] && available[(eid1[taskId] +1) % NB_TASK]){
                available[eid1[taskId]] = FALSE;
                available[(eid1[taskId] + 1) % NB_TASK] = FALSE;
            }else{
                __CPROVER_assume(cs>=4);
                
                //requeue(taskId, 2, TRUE);
                static int with_abort = TRUE;
                evaluate_barrier(index);
                if (tabBarrier[index]==FALSE && !with_abort){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l4: if (pc[ct]>4||4>=cs) goto l5;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l5: if (pc[ct]>5||5>=cs) goto l6;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=6);
                
            l6: if (pc[ct]>6||6>=cs) goto l7;
                if (tabBarrier[index]==TRUE){
                    //run_entry_body(taskId,index);
                    // index = 2;
                    //  goto label1;
                l7: if (pc[ct]>7||7>=cs) goto l8;
                    //run_entry_body(taskId,index);
                    index = 2;
                    cs = 3;
                    goto end;
                }
                __CPROVER_assume(cs>=8);
                
            }
            __CPROVER_assume(cs>=8);
            
        }else if (index == 2){ //please_get_pair
            __CPROVER_assume(cs>=8);
            
            flush_count = flush_count-1;
            if (available[eid2[taskId]] && available[(eid2[taskId]+1)%NB_TASK]){
                available[eid2[taskId]] = FALSE;
                available[(eid2[taskId]+1)%NB_TASK] = FALSE;
            }else{
                __CPROVER_assume(cs>=8);
                
                index = 2;
                //requeue(taskId, 2, TRUE);
                static int with_abort = TRUE;
                evaluate_barrier(index);
                if (tabBarrier[index]==FALSE && !with_abort){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l8: if (pc[ct]>8||8>=cs) goto l9;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l9: if (pc[ct]>9||9>=cs) goto l10;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=10);
                
            l10: if (pc[ct]>10||10>=cs) goto l11;
                if (tabBarrier[index]==TRUE){
                    //run_entry_body(taskId,index);
                    // index = 2;
                    //  goto label1;
                l11: if (pc[ct]>11||11>=cs) goto l12;
                    //run_entry_body(taskId,index);
                    index = 2;
                    cs = 3;
                    goto end;
                }
                __CPROVER_assume(cs>=12);
                
            }
            __CPROVER_assume(cs>=12);
            
        }
        __CPROVER_assume(cs>=12);
        
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    __CPROVER_assume(cs>=12);
    
l12: if (pc[ct]>12||12>=cs) goto end;
    lock(taskId);
    appel=1;
    index=1;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=13);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=13);
        
        run_entry_call(taskId, index);
    }
    __CPROVER_assume(cs>=13);
    
end:
    return;
}


void t_five(){
    static int taskId=5;
    static int taskIdEligible;
    static int appel = 2;
    static int index = 1;
    
    //protected_object_call(taskId, appel, index)
l0: if (pc[ct]>0||0>=cs) goto l1;
    lock(taskId);
    appel=2;
    index=1;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=1);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=1);
        
        //run_entry_call(taskId, index);
        static int taskIdEligible;
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
        l1: if (pc[ct]>1||1>=cs) goto l2;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        l2: if (pc[ct]>2||2>=cs) goto l3;
            remove_entry_queue(taskId, index);
        }
        __CPROVER_assume(cs>=3);
        
    l3: if (pc[ct]>3||3>=cs) goto l4;
        //run_entry_body(taskId,index);
        if (index == 1) { //get_pair
            if (available[eid1[taskId]] && available[(eid1[taskId] +1) % NB_TASK]){
                available[eid1[taskId]] = FALSE;
                available[(eid1[taskId] + 1) % NB_TASK] = FALSE;
            }else{
                __CPROVER_assume(cs>=4);
                
                //requeue(taskId, 2, TRUE);
                static int with_abort = TRUE;
                evaluate_barrier(index);
                if (tabBarrier[index]==FALSE && !with_abort){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l4: if (pc[ct]>4||4>=cs) goto l5;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l5: if (pc[ct]>5||5>=cs) goto l6;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=6);
                
            l6: if (pc[ct]>6||6>=cs) goto l7;
                if (tabBarrier[index]==TRUE){
                    //run_entry_body(taskId,index);
                    // index = 2;
                    //  goto label1;
                l7: if (pc[ct]>7||7>=cs) goto l8;
                    //run_entry_body(taskId,index);
                    index = 2;
                    cs = 3;
                    goto end;
                }
                __CPROVER_assume(cs>=8);
                
            }
            __CPROVER_assume(cs>=8);
            
        }else if (index == 2){ //please_get_pair
            __CPROVER_assume(cs>=8);
            
            flush_count = flush_count-1;
            if (available[eid2[taskId]] && available[(eid2[taskId]+1)%NB_TASK]){
                available[eid2[taskId]] = FALSE;
                available[(eid2[taskId]+1)%NB_TASK] = FALSE;
            }else{
                __CPROVER_assume(cs>=8);
                
                index = 2;
                //requeue(taskId, 2, TRUE);
                static int with_abort = TRUE;
                evaluate_barrier(index);
                if (tabBarrier[index]==FALSE && !with_abort){
                    add_entry_queue(taskId, index);
                    unlock(taskId);
                l8: if (pc[ct]>8||8>=cs) goto l9;
                    __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
                l9: if (pc[ct]>9||9>=cs) goto l10;
                    remove_entry_queue(taskId, index);
                }
                __CPROVER_assume(cs>=10);
                
            l10: if (pc[ct]>10||10>=cs) goto l11;
                if (tabBarrier[index]==TRUE){
                    //run_entry_body(taskId,index);
                    // index = 2;
                    //  goto label1;
                l11: if (pc[ct]>11||11>=cs) goto l12;
                    //run_entry_body(taskId,index);
                    index = 2;
                    cs = 3;
                    goto end;
                }
                __CPROVER_assume(cs>=12);
                
            }
            __CPROVER_assume(cs>=12);
            
        }
        __CPROVER_assume(cs>=12);
        
        taskIdEligible = check_and_run_any_entry();
        transfert(taskId, taskIdEligible);
    }
    __CPROVER_assume(cs>=12);
    
l12: if (pc[ct]>12||12>=cs) goto end;
    lock(taskId);
    appel=1;
    index=1;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=13);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=13);
        
        run_entry_call(taskId, index);
    }
    __CPROVER_assume(cs>=13);
    
end:
    return;
}










void init(){
    pc[0] = 0;
    size[0] = 0;
    
    pc[1] = 0;
    pc[2] = 0;
    pc[3] = 0;
    pc[4] = 0;
    pc[5] = 0;
    size[1] = 13;
    size[2] = 13;
    size[3] = 13;
    size[4] = 13;
    size[5] = 13;
    
    eid1[0] = 0;
    eid1[1] = 0;
    eid1[2] = 1;
    eid1[3] = 2;
    eid1[4] = 3;
    eid1[5] = 4;
    
    eid2[0] = 0;
    eid2[1] = 0;
    eid2[2] = 1;
    eid2[3] = 2;
    eid2[4] = 3;
    eid2[5] = 4;

    pid1[0] = 0;
    pid1[1] = 0;
    pid1[2] = 1;
    pid1[3] = 2;
    pid1[4] = 3;
    pid1[5] = 4;

    int t1, t2, t3, t4, t5, t6;
    
    create(t1, 1);
    create(t2, 2);
    create(t3, 3);
    create(t4, 4);
    create(t5, 5);
}

int main(){
    init();
    
    for (;;) {
        ct = 1;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_one();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        ct = 2;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_two();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        ct = 3;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_three();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        ct = 4;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_four();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        ct = 5;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_five();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        if (dead>0 && dead<NB_TASK){
            __CPROVER_assert(dead+taskInWait<NB_TASK, "OK dead+taskInWait>=NB_TASK");
        }
        
        if (pc[1] == size[1] && pc[2] == size[2] && pc[3] == size[3] && pc[4] == size[4] && pc[5] == size[5]){
            break;
        }
    }
    
    if (pc[1] == size[1] && pc[2] == size[2] && pc[3] == size[3] && pc[4] == size[4] && pc[5] == size[5]){
        __CPROVER_assert(FALSE, "OK");
    }else{
        __CPROVER_assert(FALSE, "KO");
    }
}
