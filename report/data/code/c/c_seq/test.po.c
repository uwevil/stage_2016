#include <stdio.h>
#include <assert.h>

#define TRUE 1
#define FALSE 0
#define FREE -1

#define NB_TASK 2 //without main task
#define NB_ENTRY 2
#define NB_PROCEDURE 0
#define NB_FUNCTION 0

/*-------------------------------------------------------------*/
int active[NB_TASK+1] = {TRUE, FALSE, FALSE};
int cs, ct;
int pc[NB_TASK+1];
int size[NB_TASK+1];
int dead = 0;

void create(int t, int id){
    active[id] = TRUE;
    t = id;
}
/*-------------------------------------------------------------*/
int tabStatus[NB_TASK+1] = {TRUE, TRUE, TRUE};
int tabWaiting[NB_TASK+1] = {0, 0, 0};
int tabBarrier[NB_ENTRY+1] = {FALSE, FALSE, FALSE};

int currentTask=1;

int y[NB_TASK+1] = {0,0,0};

int tabEntryCount[NB_ENTRY+1] = {0,0,0};
int taskInWait = 0;

int x = 0;

int externalLock = FREE;

void lock(int id){
    __CPROVER_assume(externalLock==FREE);
    externalLock=id;
}

void transfert(int id, int newId){
    __CPROVER_assert(externalLock==id, "OKexternalLock!=id");
    externalLock=newId;
}

void unlock(int id){
    __CPROVER_assert(externalLock==id, "OKexternalLock!=id");
    externalLock=FREE;
}
/*---------------------------------------------------------------------------*/

void evaluate_barrier(int index){
    if (index == 1){ //Request
        tabBarrier[index] = x<10;
    }else if (index == 2){ //Assign
        tabBarrier[index] = x>0;
    }
}

void reevaluate_barrier(){
    tabBarrier[1] = x<10; //request
    tabBarrier[2] = x>0; //assign
}

void setTabStatusOn(int taskId){
    tabStatus[taskId] = TRUE;
    taskInWait = taskInWait - 1;
}

void setTabStatusOff(int taskId){
    tabStatus[taskId] = FALSE;
    taskInWait = taskInWait + 1;
    __CPROVER_assert(taskInWait<NB_TASK, "OKdeadlock");
}

void add_entry_queue(int taskId, int index){
    tabEntryCount[index] = tabEntryCount[index]+1;
  //  taskInWait = taskInWait + 1;
  //  __CPROVER_assert(taskInWait<NB_TASK, "OKdeadlock");
    
    tabWaiting[taskId] = index;
//    tabStatus[taskId] = FALSE;
    setTabStatusOff(taskId);
}

void remove_entry_queue(int taskId, int index){
    tabEntryCount[index] = tabEntryCount[index]-1;
 //   taskInWait = taskInWait - 1;
    
    tabWaiting[taskId] = 0;
}

int current_task_eligible(){ //return taskId
    int j;
    for (j=1; j<=NB_TASK;j++){
        if (tabWaiting[currentTask]!=0&&tabBarrier[tabWaiting[currentTask]]){
            return currentTask;
        }
        currentTask = (currentTask % NB_TASK)+1;
    }
    return 0;
}

void run_entry_body(int taskId, int index);

void requeue(int taskId, int index){ //requeue to another or same entry queue
    evaluate_barrier(index);
    
    if (tabBarrier[index]==FALSE){
        add_entry_queue(taskId, index);
        unlock(taskId);
        __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        remove_entry_queue(taskId, index);
    }
    run_entry_body(taskId,index);
}

void run_entry_body(int taskId, int index){
    if (index == 1) { //Request
        x = x + 1;
    }else if (index == 2){ //Assign
        x = x - 1;
    }
}

int check_and_run_any_entry(){
    reevaluate_barrier();
    int taskIdEligible = current_task_eligible();
    if (taskIdEligible==0){
        return FREE;
    }
   // tabStatus[taskIdEligible] = TRUE;
    setTabStatusOn(taskIdEligible);
    return taskIdEligible;
}

void run_function_call(taskId, index){
}

void run_procedure_call(taskId, index){
    int taskIdEligible;
    
    taskIdEligible = check_and_run_any_entry();
    transfert(taskId, taskIdEligible);
}


void t_one(){
    static int taskId = 1;
    static int appel = 2;
    static int index = 1;
    static int taskIdEligible;
    static int ax, bx;
    
l0: if (pc[ct]>0||0>=cs) goto l1;
    lock(taskId);
l1: if (pc[ct]>1||1>=cs) goto l2;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=2);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=2);
        
        //run_entry_call(taskId, index);
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
        l2: if (pc[ct]>2||2>=cs) goto l3;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        l3: if (pc[ct]>3||3>=cs) goto l4;
            remove_entry_queue(taskId, index);
        }
        __CPROVER_assume(cs>=4);
        
        //run_entry_body(taskId,index);
    l4: if (pc[ct]>4||4>=cs) goto l5;
        if (index == 1) { //Request
        l5: if (pc[ct]>5||5>=cs) goto l6;
            ax = x;
        l6: if (pc[ct]>6||6>=cs) goto l7;
            ax = ax + 1;
        l7: if (pc[ct]>7||7>=cs) goto l8;
            x=ax;
        }else if (index == 2){ //Assign
            __CPROVER_assume(cs>=8);
            
        l8: if (pc[ct]>8||8>=cs) goto l9;
            bx=x;
        l9: if (pc[ct]>9||9>=cs) goto l10;
            bx = bx - 1;
        l10: if (pc[ct]>10||10>=cs) goto l11;
            x=bx;
        }
        __CPROVER_assume(cs>=11);
        
    l11: if (pc[ct]>11||11>=cs) goto l12;
        taskIdEligible = check_and_run_any_entry();
    l12: if (pc[ct]>12||12>=cs) goto end;
        transfert(taskId, taskIdEligible);
    }
    __CPROVER_assume(cs>=13);
    
end:
    return;
}

void t_two(){
    static int taskId = 2;
    static int appel = 2;
    static int index = 2;
    static int taskIdEligible;
    static int ax, bx;
    
l0: if (pc[ct]>0||0>=cs) goto l1;
    lock(taskId);
l1: if (pc[ct]>1||1>=cs) goto l2;
    if (appel == 0){ //function
        run_function_call(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=2);
        
        run_procedure_call(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=2);
        
        //run_entry_call(taskId, index);
        evaluate_barrier(index);
        if (tabBarrier[index]==FALSE){
            add_entry_queue(taskId, index);
            unlock(taskId);
        l2: if (pc[ct]>2||2>=cs) goto l3;
            __CPROVER_assume(tabStatus[taskId]&&externalLock==taskId);
        l3: if (pc[ct]>3||3>=cs) goto l4;
            remove_entry_queue(taskId, index);
        }
        __CPROVER_assume(cs>=4);
        
        //run_entry_body(taskId,index);
    l4: if (pc[ct]>4||4>=cs) goto l5;
        if (index == 1) { //Request
        l5: if (pc[ct]>5||5>=cs) goto l6;
            ax = x;
        l6: if (pc[ct]>6||6>=cs) goto l7;
            ax = ax + 1;
        l7: if (pc[ct]>7||7>=cs) goto l8;
            x=ax;
        }else if (index == 2){ //Assign
            __CPROVER_assume(cs>=8);
            
        l8: if (pc[ct]>8||8>=cs) goto l9;
            bx=x;
        l9: if (pc[ct]>9||9>=cs) goto l10;
            bx = bx - 1;
        l10: if (pc[ct]>10||10>=cs) goto l11;
            x=bx;
        }
        __CPROVER_assume(cs>=11);
        
    l11: if (pc[ct]>11||11>=cs) goto l12;
        taskIdEligible = check_and_run_any_entry();
    l12: if (pc[ct]>12||12>=cs) goto end;
        transfert(taskId, taskIdEligible);
    }
    __CPROVER_assume(cs>=13);
    
end:
    return;
}

void init(){
    pc[0] = 0;
    size[0] = 0;
    
    pc[1] = 0;
    pc[2] = 0;
    size[1] = 13;
    size[2] = 13;
    
    int t1, t2, t3, t4, t5, t6;
    
    create(t1, 1);
    create(t2, 2);
}

int main(){
    init();
    
    for (;;) {
        ct = 1;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_one();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        ct = 2;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_two();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        if (dead>0 && dead<NB_TASK){
            __CPROVER_assert(dead+taskInWait<NB_TASK, "OK dead+taskInWait>=NB_TASK");
        }
        
        if (pc[1] == size[1] && pc[2] == size[2]){
            break;
        }
    }
    
    if (pc[1] == size[1] && pc[2] == size[2]){
        __CPROVER_assert(x == 0, "OK x != 0");
        __CPROVER_assert(FALSE, "OK");
    }else{
        __CPROVER_assert(FALSE, "KO");
    }
}
