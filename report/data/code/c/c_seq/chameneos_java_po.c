#include <stdio.h>
#include <assert.h>

#define TRUE 1
#define FALSE 0
#define FREE -1

#define NB_TASK 4 //without main task

int const nb_request = 6;

/*-------------------------------------------------------------*/
int active[NB_TASK+1] = {TRUE, FALSE, FALSE, FALSE, FALSE};//, FALSE, FALSE, FALSE, FALSE, FALSE};
int cs, ct;
int pc[NB_TASK+1];
int size[NB_TASK+1];
int dead = 0;

void create(int t, int id){
    active[id] = TRUE;
    t = id;
}
/*-------------------------------------------------------------*/
#define NB_ENTRY1 1
#define NB_PROCEDURE1 1
#define NB_FUNCTION1 0

int tabStatus1[NB_TASK+1] = {TRUE, TRUE, TRUE, TRUE, TRUE}; //, TRUE, TRUE, TRUE, TRUE, TRUE};
int tabWaiting1[NB_TASK+1] = {0, 0, 0, 0,0}; //, 0, 0, 0, 0, 0};
int tabBarrier1[NB_ENTRY1+1] = {FALSE, FALSE};

int currentTask1=1;

int tabEntryCount1[NB_ENTRY1+1] = {0,0};
int taskInWait = 0;

int externalLock1 = FREE;

void lock1(int id){
    __CPROVER_assume(externalLock1==FREE);
    externalLock1=id;
}

void transfert1(int id, int newId){
    __CPROVER_assert(externalLock1==id, "OKexternalLock1!=id");
    externalLock1=newId;
}

void unlock1(int id){
    __CPROVER_assert(externalLock1==id, "OKexternalLock1!=id");
    externalLock1=FREE;
}
/*---------------------------------------------------------------------------*/

int free1 = TRUE;

void evaluate_barrier1(int index){
    if (index == 1){ //Acquire
        tabBarrier1[index] = free1;
    }
}

void reevaluate_barrier1(){
    tabBarrier1[1] = free1; //Acquire
}

void setTabStatusOn1(int taskId){
    tabStatus1[taskId] = TRUE;
    taskInWait = taskInWait - 1;
}

void setTabStatusOff1(int taskId){
    tabStatus1[taskId] = FALSE;
    taskInWait = taskInWait + 1;
    __CPROVER_assert(taskInWait<NB_TASK, "OKdeadlock1");
}


void add_entry_queue1(int taskId, int index){
    tabEntryCount1[index] = tabEntryCount1[index]+1;
    tabWaiting1[taskId] = index;
    setTabStatusOff1(taskId);
}

void remove_entry_queue1(int taskId, int index){
    tabEntryCount1[index] = tabEntryCount1[index]-1;
    tabWaiting1[taskId] = 0;
}

int current_task_eligible1(){ //return taskId
    int j;
    for (j=1; j<=NB_TASK;j++){
        if (tabWaiting1[currentTask1]!=0&&tabBarrier1[tabWaiting1[currentTask1]]){
            return currentTask1;
        }
        currentTask1 = (currentTask1 % NB_TASK)+1;
    }
    return 0;
}

void run_entry_body1(int taskId, int index);

void requeue1(int taskId, int index){ //requeue to another or same entry queue
    evaluate_barrier1(index);
    
    if (tabBarrier1[index]==FALSE){
        add_entry_queue1(taskId, index);
        unlock1(taskId);
        __CPROVER_assume(tabStatus1[taskId]&&externalLock1==taskId);
        remove_entry_queue1(taskId, index);
    }
    run_entry_body1(taskId,index);
}

void run_entry_body1(int taskId, int index){
    if (index == 1) { //Acquire
        free1 = FALSE;
    }
}

int check_and_run_any_entry1(){
    reevaluate_barrier1();
    int taskIdEligible1 = current_task_eligible1();
    if (taskIdEligible1==0){
        return FREE;
    }
    setTabStatusOn1(taskIdEligible1);
    return taskIdEligible1;
}

void run_function_call1(taskId, index){
}

void run_procedure_call1(taskId, index){
    int taskIdEligible1;
    if (index == 1){
        free1 = TRUE;
    }
    taskIdEligible1 = check_and_run_any_entry1();
    transfert1(taskId, taskIdEligible1);
}

void run_entry_call1(int taskId, int index){
    int taskIdEligible1;
    evaluate_barrier1(index);
    
    if (tabBarrier1[index]==FALSE){
        add_entry_queue1(taskId, index);
        unlock1(taskId);
        __CPROVER_assume(tabStatus1[taskId]&&externalLock1==taskId);
        remove_entry_queue1(taskId, index);
    }
    run_entry_body1(taskId,index);
    taskIdEligible1 = check_and_run_any_entry1();
    transfert1(taskId, taskIdEligible1);
}

void protected_object_call1(int taskId, int appel, int index){
    lock1(taskId);
    
    if (appel == 0){ //function
        run_function_call1(taskId, index);
    }else if (appel == 1){ //procedure
        run_procedure_call1(taskId, index);
    }else if (appel == 2){ //entry
        run_entry_call1(taskId, index);
    }
}

/*---------------------------------------------------------------------*/

/*-------------------------------------------------------------*/
#define NB_ENTRY2 2
#define NB_PROCEDURE2 0
#define NB_FUNCTION2 0

int tabStatus2[NB_TASK+1] = {TRUE, TRUE, TRUE, TRUE, TRUE}; //, TRUE, TRUE, TRUE, TRUE, TRUE};
int tabWaiting2[NB_TASK+1] = {0, 0, 0, 0, 0}; //, 0, 0, 0, 0, 0};
int tabBarrier2[NB_ENTRY2+1] = {FALSE, FALSE, FALSE};

int currentTask2=1;

int tabEntryCount2[NB_ENTRY2+1] = {0,0,0};
//int taskInWait = 0;

int externalLock2 = FREE;

void lock2(int id){
    __CPROVER_assume(externalLock2==FREE);
    externalLock2=id;
}

void transfert2(int id, int newId){
    __CPROVER_assert(externalLock2==id, "OKexternalLock2!=id");
    externalLock2=newId;
}

void unlock2(int id){
    __CPROVER_assert(externalLock2==id, "OKexternalLock2!=id");
    externalLock2=FREE;
}
/*---------------------------------------------------------------------------*/

#define BLUE 1
#define RED 2
#define YELLOW 3

int First_Call = TRUE;
int MustWait = FALSE;
int a_chameneos, b_chameneos;
int NotifyAll = FALSE;

typedef int colour;

int firstCall = TRUE;
colour aColour;
colour bColour;

int x1[NB_TASK+1];
int x_other1[NB_TASK+1];
int score1[NB_TASK+1];

int x2[NB_TASK+1];
int x_other2[NB_TASK+1];
int score2[NB_TASK+1];

int x3[NB_TASK+1];
int return3[NB_TASK+1];

void protected_object_call2(int taskId, int appel, int index);

void get_a_peer3(int taskId){
    int score=0;
    while (score!=2) {
        x1[taskId]=x3[taskId];
        score1[taskId]=score;
        protected_object_call2(taskId, 2, 1);
        score=score1[taskId];
    }
    return3[taskId]=x_other1[taskId];
}

/*---------------------------------------------------------------------------*/

void evaluate_barrier2(int index){
    if (index == 1){ //Cooperate
        tabBarrier2[index] = TRUE;
    }else if (index == 2){ //Wait
        tabBarrier2[index] = NotifyAll;
    }
}

void reevaluate_barrier2(){
    tabBarrier2[1] = TRUE; //Cooperate
    tabBarrier2[2] = NotifyAll; //Wait
}

void setTabStatusOn2(int taskId){
    tabStatus2[taskId] = TRUE;
    taskInWait = taskInWait - 1;
}

void setTabStatusOff2(int taskId){
    tabStatus2[taskId] = FALSE;
    taskInWait = taskInWait + 1;
    __CPROVER_assert(taskInWait<NB_TASK, "OKdeadlock2");
}


void add_entry_queue2(int taskId, int index){
    tabEntryCount2[index] = tabEntryCount2[index]+1;
    tabWaiting2[taskId] = index;
    setTabStatusOff2(taskId);
}

void remove_entry_queue2(int taskId, int index){
    tabEntryCount2[index] = tabEntryCount2[index]-1;
    tabWaiting2[taskId] = 0;
}

int current_task_eligible2(){ //return taskId
    int j;
    for (j=1; j<=NB_TASK;j++){
        if (tabWaiting2[currentTask2]!=0&&tabBarrier2[tabWaiting2[currentTask2]]){
            return currentTask2;
        }
        currentTask2 = (currentTask2 % NB_TASK)+1;
    }
    return 0;
}

void run_entry_body2(int taskId, int index);

void requeue2(int taskId, int index){ //requeue to another or same entry queue
    evaluate_barrier2(index);
    
    if (tabBarrier2[index]==FALSE){
        add_entry_queue2(taskId, index);
        unlock2(taskId);
        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        remove_entry_queue2(taskId, index);
    }
    run_entry_body2(taskId,index);
}

void run_entry_body2(int taskId, int index){
    if (index == 1) { //Cooperate
        if (score1[taskId]==0 && MustWait){
            NotifyAll = FALSE;
            requeue2(taskId, 2);
        }
        if (score1[taskId]==0 && First_Call){
            a_chameneos = x1[taskId];
            First_Call = FALSE;
            score1[taskId]=1;
            NotifyAll = FALSE;
            requeue2(taskId, 2);
        }
        if (score1[taskId]==0 && !First_Call){
            b_chameneos=x1[taskId];
            x_other1[taskId]=a_chameneos;
            First_Call=TRUE;
            MustWait=TRUE;
            score1[taskId]=2;
            NotifyAll=TRUE;
        }
        if (score1[taskId]=1 && First_Call){
            x_other1[taskId]=b_chameneos;
            MustWait=FALSE;
            score1[taskId]=2;
            NotifyAll=TRUE;
        }
    }else if (index == 2){ //Wait
    }
}

int check_and_run_any_entry2(){
    reevaluate_barrier2();
    int taskIdEligible2 = current_task_eligible2();
    if (taskIdEligible2==0){
        return FREE;
    }
    setTabStatusOn2(taskIdEligible2);
    return taskIdEligible2;
}

void run_function_call2(taskId, index){
}

void run_procedure_call2(taskId, index){
    /*  int taskIdEligible;
     if (index == 1){
     stock = stock + y[taskId];
     }
     taskIdEligible = check_and_run_any_entry();
     transfert(taskId, taskIdEligible);
     */
}

void run_entry_call2(int taskId, int index){
    int taskIdEligible2;
    evaluate_barrier2(index);
    
    if (tabBarrier2[index]==FALSE){
        add_entry_queue2(taskId, index);
        unlock2(taskId);
        __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        remove_entry_queue2(taskId, index);
    }
    run_entry_body2(taskId,index);
    taskIdEligible2 = check_and_run_any_entry2();
    transfert2(taskId, taskIdEligible2);
}

void protected_object_call2(int taskId, int appel, int index){
    lock2(taskId);
    
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        run_entry_call2(taskId, index);
    }
}

/*---------------------------------------------------------------------*/

/*---------------------------------------------------------------------*/


void t_one(){
    static int taskId=1;
    static int taskIdEligible;
    static int my_peer = 0;
    
l0: if (pc[ct]>0||0>=cs) goto l1;
    static int i = 1;
    // for (i = 1; i<=nb_request; i++){
l1: if (pc[ct]>1||1>=cs) goto l2;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l2: if (pc[ct]>2||2>=cs) goto l3;
    static int score=0;
    //  while (score!=2) {
l3: if (pc[ct]>3||3>=cs) goto l4;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l4: if (pc[ct]>4||4>=cs) goto l5;
    static int appel = 2;
l5: if (pc[ct]>5||5>=cs) goto l6;
    static int index = 1;
l6: if (pc[ct]>6||6>=cs) goto l7;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=7);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=7);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l7: if (pc[ct]>7||7>=cs) goto l8;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l8: if (pc[ct]>8||8>=cs) goto l9;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=9);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l9: if (pc[ct]>9||9>=cs) goto l10;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l10: if (pc[ct]>10||10>=cs) goto l11;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=11);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=11);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l11: if (pc[ct]>11||11>=cs) goto l12;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l12: if (pc[ct]>12||12>=cs) goto l13;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=13);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=13);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=13);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=13);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=13);
            
        }
        __CPROVER_assume(cs>=13);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=13);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 3;
        goto end;
    }
    __CPROVER_assume(cs>=13);
    
    //  }
l13: if (pc[ct]>13||13>=cs) goto l14;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l14: if (pc[ct]>14||14>=cs) goto l15;
    my_peer=return3[taskId];
l15: if (pc[ct]>15||15>=cs) goto l16;
    if (my_peer==taskId-1){
    l16: if (pc[ct]>16||16>=cs) goto l17;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=17);
    
    //--------------------------
l17: if (pc[ct]>17||17>=cs) goto l18;
    i = 2;
l18: if (pc[ct]>18||18>=cs) goto l19;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l19: if (pc[ct]>19||19>=cs) goto l20;
    score=0;
    //  while (score!=2) {
l20: if (pc[ct]>20||20>=cs) goto l21;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l21: if (pc[ct]>21||21>=cs) goto l22;
    appel = 2;
l22: if (pc[ct]>22||22>=cs) goto l23;
    index = 1;
l23: if (pc[ct]>23||23>=cs) goto l24;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=24);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=24);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l24: if (pc[ct]>24||24>=cs) goto l25;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l25: if (pc[ct]>25||25>=cs) goto l26;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=26);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l26: if (pc[ct]>26||26>=cs) goto l27;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l27: if (pc[ct]>27||27>=cs) goto l28;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=28);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=28);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l28: if (pc[ct]>28||28>=cs) goto l29;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l29: if (pc[ct]>29||29>=cs) goto l30;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=30);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=30);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=30);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=30);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=30);
            
        }
        __CPROVER_assume(cs>=30);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=30);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 20;
        goto end;
    }
    __CPROVER_assume(cs>=30);
    
    //  }
l30: if (pc[ct]>30||30>=cs) goto l31;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l31: if (pc[ct]>31||31>=cs) goto l32;
    my_peer=return3[taskId];
l32: if (pc[ct]>32||32>=cs) goto l33;
    if (my_peer==taskId-1){
    l33: if (pc[ct]>33||33>=cs) goto l34;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=34);
    
    //--------------------------
l34: if (pc[ct]>34||34>=cs) goto l35;
    i=3;
l35: if (pc[ct]>35||35>=cs) goto l36;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l36: if (pc[ct]>36||36>=cs) goto l37;
    score=0;
    //  while (score!=2) {
l37: if (pc[ct]>37||37>=cs) goto l38;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l38: if (pc[ct]>38||38>=cs) goto l39;
    appel = 2;
l39: if (pc[ct]>39||39>=cs) goto l40;
    index = 1;
l40: if (pc[ct]>40||40>=cs) goto l41;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=41);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=41);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l41: if (pc[ct]>41||41>=cs) goto l42;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l42: if (pc[ct]>42||42>=cs) goto l43;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=43);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l43: if (pc[ct]>43||43>=cs) goto l44;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l44: if (pc[ct]>44||44>=cs) goto l45;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=45);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=45);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l45: if (pc[ct]>45||45>=cs) goto l46;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l46: if (pc[ct]>46||46>=cs) goto l47;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=47);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=47);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=47);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=47);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=47);
            
        }
        __CPROVER_assume(cs>=47);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=47);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 37;
        goto end;
    }
    __CPROVER_assume(cs>=47);
    
    //  }
l47: if (pc[ct]>47||47>=cs) goto l48;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l48: if (pc[ct]>48||48>=cs) goto l49;
    my_peer=return3[taskId];
l49: if (pc[ct]>49||49>=cs) goto l50;
    if (my_peer==taskId-1){
    l50: if (pc[ct]>50||50>=cs) goto l51;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=51);
    
    //--------------------------
l51: if (pc[ct]>51||51>=cs) goto l52;
    i=4;
l52: if (pc[ct]>52||52>=cs) goto l53;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l53: if (pc[ct]>53||53>=cs) goto l54;
    score=0;
    //  while (score!=2) {
l54: if (pc[ct]>54||54>=cs) goto l55;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l55: if (pc[ct]>55||55>=cs) goto l56;
    appel = 2;
l56: if (pc[ct]>56||56>=cs) goto l57;
    index = 1;
l57: if (pc[ct]>57||57>=cs) goto l58;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=58);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=58);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l58: if (pc[ct]>58||58>=cs) goto l59;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l59: if (pc[ct]>59||59>=cs) goto l60;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=60);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l60: if (pc[ct]>60||60>=cs) goto l61;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l61: if (pc[ct]>61||61>=cs) goto l62;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=62);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=62);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l62: if (pc[ct]>62||62>=cs) goto l63;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l63: if (pc[ct]>63||63>=cs) goto l64;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=64);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=64);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=64);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=64);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=64);
            
        }
        __CPROVER_assume(cs>=64);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=64);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 54;
        goto end;
    }
    __CPROVER_assume(cs>=64);
    
    //  }
l64: if (pc[ct]>64||64>=cs) goto l65;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l65: if (pc[ct]>65||65>=cs) goto l66;
    my_peer=return3[taskId];
l66: if (pc[ct]>66||66>=cs) goto l67;
    if (my_peer==taskId-1){
    l67: if (pc[ct]>67||67>=cs) goto l68;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=68);
    
    //--------------------------
l68: if (pc[ct]>68||68>=cs) goto l69;
    i=5;
l69: if (pc[ct]>69||69>=cs) goto l70;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l70: if (pc[ct]>70||70>=cs) goto l71;
    score=0;
    //  while (score!=2) {
l71: if (pc[ct]>71||71>=cs) goto l72;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l72: if (pc[ct]>72||72>=cs) goto l73;
    appel = 2;
l73: if (pc[ct]>73||73>=cs) goto l74;
    index = 1;
l74: if (pc[ct]>74||74>=cs) goto l75;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=75);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=75);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l75: if (pc[ct]>75||75>=cs) goto l76;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l76: if (pc[ct]>76||76>=cs) goto l77;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=77);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l77: if (pc[ct]>77||77>=cs) goto l78;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l78: if (pc[ct]>78||78>=cs) goto l79;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=79);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=79);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l79: if (pc[ct]>79||79>=cs) goto l80;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l80: if (pc[ct]>80||80>=cs) goto l81;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=81);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=81);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=81);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=81);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=81);
            
        }
        __CPROVER_assume(cs>=81);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=81);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 71;
        goto end;
    }
    __CPROVER_assume(cs>=81);
    
    //  }
l81: if (pc[ct]>81||81>=cs) goto l82;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l82: if (pc[ct]>82||82>=cs) goto l83;
    my_peer=return3[taskId];
l83: if (pc[ct]>83||83>=cs) goto l84;
    if (my_peer==taskId-1){
    l84: if (pc[ct]>84||84>=cs) goto l85;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=85);
    
    //--------------------------
l85: if (pc[ct]>85||85>=cs) goto l86;
    i=6;
l86: if (pc[ct]>86||86>=cs) goto l87;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l87: if (pc[ct]>87||87>=cs) goto l88;
    score=0;
    //  while (score!=2) {
l88: if (pc[ct]>88||88>=cs) goto l89;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l89: if (pc[ct]>89||89>=cs) goto l90;
    appel = 2;
l90: if (pc[ct]>90||90>=cs) goto l91;
    index = 1;
l91: if (pc[ct]>91||91>=cs) goto l92;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=92);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=92);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l92: if (pc[ct]>92||92>=cs) goto l93;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l93: if (pc[ct]>93||93>=cs) goto l94;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=94);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l94: if (pc[ct]>94||94>=cs) goto l95;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l95: if (pc[ct]>95||95>=cs) goto l96;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=96);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=96);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l96: if (pc[ct]>96||96>=cs) goto l97;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l97: if (pc[ct]>97||97>=cs) goto l98;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=98);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=98);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=98);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=98);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=98);
            
        }
        __CPROVER_assume(cs>=98);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=98);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 88;
        goto end;
    }
    __CPROVER_assume(cs>=98);
    
    //  }
l98: if (pc[ct]>98||98>=cs) goto l99;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l99: if (pc[ct]>99||99>=cs) goto l100;
    my_peer=return3[taskId];
l100: if (pc[ct]>100||100>=cs) goto l101;
    if (my_peer==taskId-1){
    l101: if (pc[ct]>101||101>=cs) goto end;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=102);
    
    //--------------------------
    //  }
end:
    return;
}

void t_two(){
    static int taskId=2;
    static int taskIdEligible;
    static int my_peer = 0;
    
l0: if (pc[ct]>0||0>=cs) goto l1;
    static int i = 1;
    // for (i = 1; i<=nb_request; i++){
l1: if (pc[ct]>1||1>=cs) goto l2;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l2: if (pc[ct]>2||2>=cs) goto l3;
    static int score=0;
    //  while (score!=2) {
l3: if (pc[ct]>3||3>=cs) goto l4;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l4: if (pc[ct]>4||4>=cs) goto l5;
    static int appel = 2;
l5: if (pc[ct]>5||5>=cs) goto l6;
    static int index = 1;
l6: if (pc[ct]>6||6>=cs) goto l7;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=7);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=7);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l7: if (pc[ct]>7||7>=cs) goto l8;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l8: if (pc[ct]>8||8>=cs) goto l9;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=9);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l9: if (pc[ct]>9||9>=cs) goto l10;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l10: if (pc[ct]>10||10>=cs) goto l11;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=11);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=11);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l11: if (pc[ct]>11||11>=cs) goto l12;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l12: if (pc[ct]>12||12>=cs) goto l13;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=13);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=13);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=13);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=13);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=13);
            
        }
        __CPROVER_assume(cs>=13);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=13);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 3;
        goto end;
    }
    __CPROVER_assume(cs>=13);
    
    //  }
l13: if (pc[ct]>13||13>=cs) goto l14;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l14: if (pc[ct]>14||14>=cs) goto l15;
    my_peer=return3[taskId];
l15: if (pc[ct]>15||15>=cs) goto l16;
    if (my_peer==taskId-1){
    l16: if (pc[ct]>16||16>=cs) goto l17;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=17);
    
    //--------------------------
l17: if (pc[ct]>17||17>=cs) goto l18;
    i = 2;
l18: if (pc[ct]>18||18>=cs) goto l19;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l19: if (pc[ct]>19||19>=cs) goto l20;
    score=0;
    //  while (score!=2) {
l20: if (pc[ct]>20||20>=cs) goto l21;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l21: if (pc[ct]>21||21>=cs) goto l22;
    appel = 2;
l22: if (pc[ct]>22||22>=cs) goto l23;
    index = 1;
l23: if (pc[ct]>23||23>=cs) goto l24;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=24);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=24);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l24: if (pc[ct]>24||24>=cs) goto l25;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l25: if (pc[ct]>25||25>=cs) goto l26;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=26);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l26: if (pc[ct]>26||26>=cs) goto l27;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l27: if (pc[ct]>27||27>=cs) goto l28;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=28);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=28);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l28: if (pc[ct]>28||28>=cs) goto l29;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l29: if (pc[ct]>29||29>=cs) goto l30;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=30);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=30);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=30);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=30);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=30);
            
        }
        __CPROVER_assume(cs>=30);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=30);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 20;
        goto end;
    }
    __CPROVER_assume(cs>=30);
    
    //  }
l30: if (pc[ct]>30||30>=cs) goto l31;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l31: if (pc[ct]>31||31>=cs) goto l32;
    my_peer=return3[taskId];
l32: if (pc[ct]>32||32>=cs) goto l33;
    if (my_peer==taskId-1){
    l33: if (pc[ct]>33||33>=cs) goto l34;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=34);
    
    //--------------------------
l34: if (pc[ct]>34||34>=cs) goto l35;
    i=3;
l35: if (pc[ct]>35||35>=cs) goto l36;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l36: if (pc[ct]>36||36>=cs) goto l37;
    score=0;
    //  while (score!=2) {
l37: if (pc[ct]>37||37>=cs) goto l38;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l38: if (pc[ct]>38||38>=cs) goto l39;
    appel = 2;
l39: if (pc[ct]>39||39>=cs) goto l40;
    index = 1;
l40: if (pc[ct]>40||40>=cs) goto l41;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=41);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=41);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l41: if (pc[ct]>41||41>=cs) goto l42;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l42: if (pc[ct]>42||42>=cs) goto l43;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=43);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l43: if (pc[ct]>43||43>=cs) goto l44;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l44: if (pc[ct]>44||44>=cs) goto l45;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=45);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=45);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l45: if (pc[ct]>45||45>=cs) goto l46;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l46: if (pc[ct]>46||46>=cs) goto l47;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=47);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=47);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=47);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=47);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=47);
            
        }
        __CPROVER_assume(cs>=47);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=47);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 37;
        goto end;
    }
    __CPROVER_assume(cs>=47);
    
    //  }
l47: if (pc[ct]>47||47>=cs) goto l48;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l48: if (pc[ct]>48||48>=cs) goto l49;
    my_peer=return3[taskId];
l49: if (pc[ct]>49||49>=cs) goto l50;
    if (my_peer==taskId-1){
    l50: if (pc[ct]>50||50>=cs) goto l51;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=51);
    
    //--------------------------
l51: if (pc[ct]>51||51>=cs) goto l52;
    i=4;
l52: if (pc[ct]>52||52>=cs) goto l53;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l53: if (pc[ct]>53||53>=cs) goto l54;
    score=0;
    //  while (score!=2) {
l54: if (pc[ct]>54||54>=cs) goto l55;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l55: if (pc[ct]>55||55>=cs) goto l56;
    appel = 2;
l56: if (pc[ct]>56||56>=cs) goto l57;
    index = 1;
l57: if (pc[ct]>57||57>=cs) goto l58;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=58);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=58);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l58: if (pc[ct]>58||58>=cs) goto l59;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l59: if (pc[ct]>59||59>=cs) goto l60;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=60);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l60: if (pc[ct]>60||60>=cs) goto l61;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l61: if (pc[ct]>61||61>=cs) goto l62;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=62);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=62);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l62: if (pc[ct]>62||62>=cs) goto l63;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l63: if (pc[ct]>63||63>=cs) goto l64;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=64);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=64);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=64);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=64);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=64);
            
        }
        __CPROVER_assume(cs>=64);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=64);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 54;
        goto end;
    }
    __CPROVER_assume(cs>=64);
    
    //  }
l64: if (pc[ct]>64||64>=cs) goto l65;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l65: if (pc[ct]>65||65>=cs) goto l66;
    my_peer=return3[taskId];
l66: if (pc[ct]>66||66>=cs) goto l67;
    if (my_peer==taskId-1){
    l67: if (pc[ct]>67||67>=cs) goto l68;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=68);
    
    //--------------------------
l68: if (pc[ct]>68||68>=cs) goto l69;
    i=5;
l69: if (pc[ct]>69||69>=cs) goto l70;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l70: if (pc[ct]>70||70>=cs) goto l71;
    score=0;
    //  while (score!=2) {
l71: if (pc[ct]>71||71>=cs) goto l72;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l72: if (pc[ct]>72||72>=cs) goto l73;
    appel = 2;
l73: if (pc[ct]>73||73>=cs) goto l74;
    index = 1;
l74: if (pc[ct]>74||74>=cs) goto l75;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=75);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=75);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l75: if (pc[ct]>75||75>=cs) goto l76;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l76: if (pc[ct]>76||76>=cs) goto l77;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=77);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l77: if (pc[ct]>77||77>=cs) goto l78;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l78: if (pc[ct]>78||78>=cs) goto l79;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=79);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=79);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l79: if (pc[ct]>79||79>=cs) goto l80;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l80: if (pc[ct]>80||80>=cs) goto l81;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=81);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=81);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=81);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=81);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=81);
            
        }
        __CPROVER_assume(cs>=81);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=81);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 71;
        goto end;
    }
    __CPROVER_assume(cs>=81);
    
    //  }
l81: if (pc[ct]>81||81>=cs) goto l82;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l82: if (pc[ct]>82||82>=cs) goto l83;
    my_peer=return3[taskId];
l83: if (pc[ct]>83||83>=cs) goto l84;
    if (my_peer==taskId-1){
    l84: if (pc[ct]>84||84>=cs) goto l85;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=85);
    
    //--------------------------
l85: if (pc[ct]>85||85>=cs) goto l86;
    i=6;
l86: if (pc[ct]>86||86>=cs) goto l87;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l87: if (pc[ct]>87||87>=cs) goto l88;
    score=0;
    //  while (score!=2) {
l88: if (pc[ct]>88||88>=cs) goto l89;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l89: if (pc[ct]>89||89>=cs) goto l90;
    appel = 2;
l90: if (pc[ct]>90||90>=cs) goto l91;
    index = 1;
l91: if (pc[ct]>91||91>=cs) goto l92;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=92);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=92);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l92: if (pc[ct]>92||92>=cs) goto l93;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l93: if (pc[ct]>93||93>=cs) goto l94;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=94);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l94: if (pc[ct]>94||94>=cs) goto l95;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l95: if (pc[ct]>95||95>=cs) goto l96;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=96);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=96);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l96: if (pc[ct]>96||96>=cs) goto l97;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l97: if (pc[ct]>97||97>=cs) goto l98;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=98);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=98);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=98);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=98);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=98);
            
        }
        __CPROVER_assume(cs>=98);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=98);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 88;
        goto end;
    }
    __CPROVER_assume(cs>=98);
    
    //  }
l98: if (pc[ct]>98||98>=cs) goto l99;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l99: if (pc[ct]>99||99>=cs) goto l100;
    my_peer=return3[taskId];
l100: if (pc[ct]>100||100>=cs) goto l101;
    if (my_peer==taskId-1){
    l101: if (pc[ct]>101||101>=cs) goto end;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=102);
    
    //--------------------------
    //  }
end:
    return;
}

void t_three(){
    static int taskId=3;
    static int taskIdEligible;
    static int my_peer = 0;
    
l0: if (pc[ct]>0||0>=cs) goto l1;
    static int i = 1;
    // for (i = 1; i<=nb_request; i++){
l1: if (pc[ct]>1||1>=cs) goto l2;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l2: if (pc[ct]>2||2>=cs) goto l3;
    static int score=0;
    //  while (score!=2) {
l3: if (pc[ct]>3||3>=cs) goto l4;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l4: if (pc[ct]>4||4>=cs) goto l5;
    static int appel = 2;
l5: if (pc[ct]>5||5>=cs) goto l6;
    static int index = 1;
l6: if (pc[ct]>6||6>=cs) goto l7;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=7);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=7);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l7: if (pc[ct]>7||7>=cs) goto l8;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l8: if (pc[ct]>8||8>=cs) goto l9;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=9);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l9: if (pc[ct]>9||9>=cs) goto l10;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l10: if (pc[ct]>10||10>=cs) goto l11;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=11);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=11);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l11: if (pc[ct]>11||11>=cs) goto l12;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l12: if (pc[ct]>12||12>=cs) goto l13;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=13);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=13);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=13);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=13);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=13);
            
        }
        __CPROVER_assume(cs>=13);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=13);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 3;
        goto end;
    }
    __CPROVER_assume(cs>=13);
    
    //  }
l13: if (pc[ct]>13||13>=cs) goto l14;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l14: if (pc[ct]>14||14>=cs) goto l15;
    my_peer=return3[taskId];
l15: if (pc[ct]>15||15>=cs) goto l16;
    if (my_peer==taskId-1){
    l16: if (pc[ct]>16||16>=cs) goto l17;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=17);
    
    //--------------------------
l17: if (pc[ct]>17||17>=cs) goto l18;
    i = 2;
l18: if (pc[ct]>18||18>=cs) goto l19;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l19: if (pc[ct]>19||19>=cs) goto l20;
    score=0;
    //  while (score!=2) {
l20: if (pc[ct]>20||20>=cs) goto l21;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l21: if (pc[ct]>21||21>=cs) goto l22;
    appel = 2;
l22: if (pc[ct]>22||22>=cs) goto l23;
    index = 1;
l23: if (pc[ct]>23||23>=cs) goto l24;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=24);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=24);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l24: if (pc[ct]>24||24>=cs) goto l25;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l25: if (pc[ct]>25||25>=cs) goto l26;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=26);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l26: if (pc[ct]>26||26>=cs) goto l27;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l27: if (pc[ct]>27||27>=cs) goto l28;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=28);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=28);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l28: if (pc[ct]>28||28>=cs) goto l29;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l29: if (pc[ct]>29||29>=cs) goto l30;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=30);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=30);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=30);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=30);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=30);
            
        }
        __CPROVER_assume(cs>=30);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=30);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 20;
        goto end;
    }
    __CPROVER_assume(cs>=30);
    
    //  }
l30: if (pc[ct]>30||30>=cs) goto l31;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l31: if (pc[ct]>31||31>=cs) goto l32;
    my_peer=return3[taskId];
l32: if (pc[ct]>32||32>=cs) goto l33;
    if (my_peer==taskId-1){
    l33: if (pc[ct]>33||33>=cs) goto l34;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=34);
    
    //--------------------------
l34: if (pc[ct]>34||34>=cs) goto l35;
    i=3;
l35: if (pc[ct]>35||35>=cs) goto l36;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l36: if (pc[ct]>36||36>=cs) goto l37;
    score=0;
    //  while (score!=2) {
l37: if (pc[ct]>37||37>=cs) goto l38;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l38: if (pc[ct]>38||38>=cs) goto l39;
    appel = 2;
l39: if (pc[ct]>39||39>=cs) goto l40;
    index = 1;
l40: if (pc[ct]>40||40>=cs) goto l41;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=41);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=41);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l41: if (pc[ct]>41||41>=cs) goto l42;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l42: if (pc[ct]>42||42>=cs) goto l43;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=43);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l43: if (pc[ct]>43||43>=cs) goto l44;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l44: if (pc[ct]>44||44>=cs) goto l45;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=45);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=45);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l45: if (pc[ct]>45||45>=cs) goto l46;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l46: if (pc[ct]>46||46>=cs) goto l47;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=47);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=47);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=47);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=47);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=47);
            
        }
        __CPROVER_assume(cs>=47);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=47);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 37;
        goto end;
    }
    __CPROVER_assume(cs>=47);
    
    //  }
l47: if (pc[ct]>47||47>=cs) goto l48;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l48: if (pc[ct]>48||48>=cs) goto l49;
    my_peer=return3[taskId];
l49: if (pc[ct]>49||49>=cs) goto l50;
    if (my_peer==taskId-1){
    l50: if (pc[ct]>50||50>=cs) goto l51;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=51);
    
    //--------------------------
l51: if (pc[ct]>51||51>=cs) goto l52;
    i=4;
l52: if (pc[ct]>52||52>=cs) goto l53;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l53: if (pc[ct]>53||53>=cs) goto l54;
    score=0;
    //  while (score!=2) {
l54: if (pc[ct]>54||54>=cs) goto l55;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l55: if (pc[ct]>55||55>=cs) goto l56;
    appel = 2;
l56: if (pc[ct]>56||56>=cs) goto l57;
    index = 1;
l57: if (pc[ct]>57||57>=cs) goto l58;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=58);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=58);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l58: if (pc[ct]>58||58>=cs) goto l59;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l59: if (pc[ct]>59||59>=cs) goto l60;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=60);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l60: if (pc[ct]>60||60>=cs) goto l61;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l61: if (pc[ct]>61||61>=cs) goto l62;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=62);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=62);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l62: if (pc[ct]>62||62>=cs) goto l63;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l63: if (pc[ct]>63||63>=cs) goto l64;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=64);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=64);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=64);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=64);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=64);
            
        }
        __CPROVER_assume(cs>=64);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=64);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 54;
        goto end;
    }
    __CPROVER_assume(cs>=64);
    
    //  }
l64: if (pc[ct]>64||64>=cs) goto l65;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l65: if (pc[ct]>65||65>=cs) goto l66;
    my_peer=return3[taskId];
l66: if (pc[ct]>66||66>=cs) goto l67;
    if (my_peer==taskId-1){
    l67: if (pc[ct]>67||67>=cs) goto l68;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=68);
    
    //--------------------------
l68: if (pc[ct]>68||68>=cs) goto l69;
    i=5;
l69: if (pc[ct]>69||69>=cs) goto l70;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l70: if (pc[ct]>70||70>=cs) goto l71;
    score=0;
    //  while (score!=2) {
l71: if (pc[ct]>71||71>=cs) goto l72;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l72: if (pc[ct]>72||72>=cs) goto l73;
    appel = 2;
l73: if (pc[ct]>73||73>=cs) goto l74;
    index = 1;
l74: if (pc[ct]>74||74>=cs) goto l75;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=75);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=75);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l75: if (pc[ct]>75||75>=cs) goto l76;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l76: if (pc[ct]>76||76>=cs) goto l77;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=77);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l77: if (pc[ct]>77||77>=cs) goto l78;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l78: if (pc[ct]>78||78>=cs) goto l79;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=79);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=79);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l79: if (pc[ct]>79||79>=cs) goto l80;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l80: if (pc[ct]>80||80>=cs) goto l81;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=81);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=81);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=81);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=81);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=81);
            
        }
        __CPROVER_assume(cs>=81);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=81);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 71;
        goto end;
    }
    __CPROVER_assume(cs>=81);
    
    //  }
l81: if (pc[ct]>81||81>=cs) goto l82;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l82: if (pc[ct]>82||82>=cs) goto l83;
    my_peer=return3[taskId];
l83: if (pc[ct]>83||83>=cs) goto l84;
    if (my_peer==taskId-1){
    l84: if (pc[ct]>84||84>=cs) goto l85;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=85);
    
    //--------------------------
l85: if (pc[ct]>85||85>=cs) goto l86;
    i=6;
l86: if (pc[ct]>86||86>=cs) goto l87;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l87: if (pc[ct]>87||87>=cs) goto l88;
    score=0;
    //  while (score!=2) {
l88: if (pc[ct]>88||88>=cs) goto l89;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l89: if (pc[ct]>89||89>=cs) goto l90;
    appel = 2;
l90: if (pc[ct]>90||90>=cs) goto l91;
    index = 1;
l91: if (pc[ct]>91||91>=cs) goto l92;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=92);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=92);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l92: if (pc[ct]>92||92>=cs) goto l93;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l93: if (pc[ct]>93||93>=cs) goto l94;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=94);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l94: if (pc[ct]>94||94>=cs) goto l95;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l95: if (pc[ct]>95||95>=cs) goto l96;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=96);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=96);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l96: if (pc[ct]>96||96>=cs) goto l97;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l97: if (pc[ct]>97||97>=cs) goto l98;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=98);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=98);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=98);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=98);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=98);
            
        }
        __CPROVER_assume(cs>=98);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=98);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 88;
        goto end;
    }
    __CPROVER_assume(cs>=98);
    
    //  }
l98: if (pc[ct]>98||98>=cs) goto l99;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l99: if (pc[ct]>99||99>=cs) goto l100;
    my_peer=return3[taskId];
l100: if (pc[ct]>100||100>=cs) goto l101;
    if (my_peer==taskId-1){
    l101: if (pc[ct]>101||101>=cs) goto end;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=102);
    
    //--------------------------
    //  }
end:
    return;
}


void t_four(){
    static int taskId=4;
    static int taskIdEligible;
    static int my_peer = 0;
    
l0: if (pc[ct]>0||0>=cs) goto l1;
    static int i = 1;
    // for (i = 1; i<=nb_request; i++){
l1: if (pc[ct]>1||1>=cs) goto l2;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l2: if (pc[ct]>2||2>=cs) goto l3;
    static int score=0;
    //  while (score!=2) {
l3: if (pc[ct]>3||3>=cs) goto l4;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l4: if (pc[ct]>4||4>=cs) goto l5;
    static int appel = 2;
l5: if (pc[ct]>5||5>=cs) goto l6;
    static int index = 1;
l6: if (pc[ct]>6||6>=cs) goto l7;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=7);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=7);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l7: if (pc[ct]>7||7>=cs) goto l8;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l8: if (pc[ct]>8||8>=cs) goto l9;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=9);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l9: if (pc[ct]>9||9>=cs) goto l10;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l10: if (pc[ct]>10||10>=cs) goto l11;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=11);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=11);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l11: if (pc[ct]>11||11>=cs) goto l12;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l12: if (pc[ct]>12||12>=cs) goto l13;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=13);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=13);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=13);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=13);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=13);
            
        }
        __CPROVER_assume(cs>=13);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=13);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 3;
        goto end;
    }
    __CPROVER_assume(cs>=13);
    
    //  }
l13: if (pc[ct]>13||13>=cs) goto l14;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l14: if (pc[ct]>14||14>=cs) goto l15;
    my_peer=return3[taskId];
l15: if (pc[ct]>15||15>=cs) goto l16;
    if (my_peer==taskId-1){
    l16: if (pc[ct]>16||16>=cs) goto l17;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=17);
    
    //--------------------------
l17: if (pc[ct]>17||17>=cs) goto l18;
    i = 2;
l18: if (pc[ct]>18||18>=cs) goto l19;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l19: if (pc[ct]>19||19>=cs) goto l20;
    score=0;
    //  while (score!=2) {
l20: if (pc[ct]>20||20>=cs) goto l21;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l21: if (pc[ct]>21||21>=cs) goto l22;
    appel = 2;
l22: if (pc[ct]>22||22>=cs) goto l23;
    index = 1;
l23: if (pc[ct]>23||23>=cs) goto l24;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=24);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=24);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l24: if (pc[ct]>24||24>=cs) goto l25;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l25: if (pc[ct]>25||25>=cs) goto l26;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=26);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l26: if (pc[ct]>26||26>=cs) goto l27;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l27: if (pc[ct]>27||27>=cs) goto l28;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=28);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=28);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l28: if (pc[ct]>28||28>=cs) goto l29;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l29: if (pc[ct]>29||29>=cs) goto l30;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=30);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=30);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=30);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=30);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=30);
            
        }
        __CPROVER_assume(cs>=30);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=30);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 20;
        goto end;
    }
    __CPROVER_assume(cs>=30);
    
    //  }
l30: if (pc[ct]>30||30>=cs) goto l31;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l31: if (pc[ct]>31||31>=cs) goto l32;
    my_peer=return3[taskId];
l32: if (pc[ct]>32||32>=cs) goto l33;
    if (my_peer==taskId-1){
    l33: if (pc[ct]>33||33>=cs) goto l34;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=34);
    
    //--------------------------
l34: if (pc[ct]>34||34>=cs) goto l35;
    i=3;
l35: if (pc[ct]>35||35>=cs) goto l36;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l36: if (pc[ct]>36||36>=cs) goto l37;
    score=0;
    //  while (score!=2) {
l37: if (pc[ct]>37||37>=cs) goto l38;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l38: if (pc[ct]>38||38>=cs) goto l39;
    appel = 2;
l39: if (pc[ct]>39||39>=cs) goto l40;
    index = 1;
l40: if (pc[ct]>40||40>=cs) goto l41;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=41);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=41);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l41: if (pc[ct]>41||41>=cs) goto l42;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l42: if (pc[ct]>42||42>=cs) goto l43;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=43);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l43: if (pc[ct]>43||43>=cs) goto l44;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l44: if (pc[ct]>44||44>=cs) goto l45;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=45);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=45);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l45: if (pc[ct]>45||45>=cs) goto l46;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l46: if (pc[ct]>46||46>=cs) goto l47;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=47);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=47);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=47);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=47);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=47);
            
        }
        __CPROVER_assume(cs>=47);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=47);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 37;
        goto end;
    }
    __CPROVER_assume(cs>=47);
    
    //  }
l47: if (pc[ct]>47||47>=cs) goto l48;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l48: if (pc[ct]>48||48>=cs) goto l49;
    my_peer=return3[taskId];
l49: if (pc[ct]>49||49>=cs) goto l50;
    if (my_peer==taskId-1){
    l50: if (pc[ct]>50||50>=cs) goto l51;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=51);
    
    //--------------------------
l51: if (pc[ct]>51||51>=cs) goto l52;
    i=4;
l52: if (pc[ct]>52||52>=cs) goto l53;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l53: if (pc[ct]>53||53>=cs) goto l54;
    score=0;
    //  while (score!=2) {
l54: if (pc[ct]>54||54>=cs) goto l55;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l55: if (pc[ct]>55||55>=cs) goto l56;
    appel = 2;
l56: if (pc[ct]>56||56>=cs) goto l57;
    index = 1;
l57: if (pc[ct]>57||57>=cs) goto l58;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=58);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=58);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l58: if (pc[ct]>58||58>=cs) goto l59;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l59: if (pc[ct]>59||59>=cs) goto l60;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=60);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l60: if (pc[ct]>60||60>=cs) goto l61;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l61: if (pc[ct]>61||61>=cs) goto l62;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=62);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=62);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l62: if (pc[ct]>62||62>=cs) goto l63;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l63: if (pc[ct]>63||63>=cs) goto l64;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=64);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=64);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=64);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=64);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=64);
            
        }
        __CPROVER_assume(cs>=64);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=64);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 54;
        goto end;
    }
    __CPROVER_assume(cs>=64);
    
    //  }
l64: if (pc[ct]>64||64>=cs) goto l65;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l65: if (pc[ct]>65||65>=cs) goto l66;
    my_peer=return3[taskId];
l66: if (pc[ct]>66||66>=cs) goto l67;
    if (my_peer==taskId-1){
    l67: if (pc[ct]>67||67>=cs) goto l68;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=68);
    
    //--------------------------
l68: if (pc[ct]>68||68>=cs) goto l69;
    i=5;
l69: if (pc[ct]>69||69>=cs) goto l70;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l70: if (pc[ct]>70||70>=cs) goto l71;
    score=0;
    //  while (score!=2) {
l71: if (pc[ct]>71||71>=cs) goto l72;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l72: if (pc[ct]>72||72>=cs) goto l73;
    appel = 2;
l73: if (pc[ct]>73||73>=cs) goto l74;
    index = 1;
l74: if (pc[ct]>74||74>=cs) goto l75;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=75);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=75);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l75: if (pc[ct]>75||75>=cs) goto l76;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l76: if (pc[ct]>76||76>=cs) goto l77;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=77);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l77: if (pc[ct]>77||77>=cs) goto l78;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l78: if (pc[ct]>78||78>=cs) goto l79;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=79);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=79);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l79: if (pc[ct]>79||79>=cs) goto l80;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l80: if (pc[ct]>80||80>=cs) goto l81;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=81);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=81);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=81);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=81);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=81);
            
        }
        __CPROVER_assume(cs>=81);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=81);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 71;
        goto end;
    }
    __CPROVER_assume(cs>=81);
    
    //  }
l81: if (pc[ct]>81||81>=cs) goto l82;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l82: if (pc[ct]>82||82>=cs) goto l83;
    my_peer=return3[taskId];
l83: if (pc[ct]>83||83>=cs) goto l84;
    if (my_peer==taskId-1){
    l84: if (pc[ct]>84||84>=cs) goto l85;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=85);
    
    //--------------------------
l85: if (pc[ct]>85||85>=cs) goto l86;
    i=6;
l86: if (pc[ct]>86||86>=cs) goto l87;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l87: if (pc[ct]>87||87>=cs) goto l88;
    score=0;
    //  while (score!=2) {
l88: if (pc[ct]>88||88>=cs) goto l89;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l89: if (pc[ct]>89||89>=cs) goto l90;
    appel = 2;
l90: if (pc[ct]>90||90>=cs) goto l91;
    index = 1;
l91: if (pc[ct]>91||91>=cs) goto l92;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=92);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=92);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l92: if (pc[ct]>92||92>=cs) goto l93;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l93: if (pc[ct]>93||93>=cs) goto l94;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=94);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l94: if (pc[ct]>94||94>=cs) goto l95;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l95: if (pc[ct]>95||95>=cs) goto l96;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=96);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=96);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l96: if (pc[ct]>96||96>=cs) goto l97;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l97: if (pc[ct]>97||97>=cs) goto l98;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=98);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=98);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=98);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=98);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=98);
            
        }
        __CPROVER_assume(cs>=98);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=98);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 88;
        goto end;
    }
    __CPROVER_assume(cs>=98);
    
    //  }
l98: if (pc[ct]>98||98>=cs) goto l99;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l99: if (pc[ct]>99||99>=cs) goto l100;
    my_peer=return3[taskId];
l100: if (pc[ct]>100||100>=cs) goto l101;
    if (my_peer==taskId-1){
    l101: if (pc[ct]>101||101>=cs) goto end;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=102);
    
    //--------------------------
    //  }
end:
    return;
}

/*
void t_five(){
    static int taskId=5;
    static int taskIdEligible;
    static int my_peer = 0;
    
l0: if (pc[ct]>0||0>=cs) goto l1;
    static int i = 1;
    // for (i = 1; i<=nb_request; i++){
l1: if (pc[ct]>1||1>=cs) goto l2;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l2: if (pc[ct]>2||2>=cs) goto l3;
    static int score=0;
    //  while (score!=2) {
l3: if (pc[ct]>3||3>=cs) goto l4;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l4: if (pc[ct]>4||4>=cs) goto l5;
    static int appel = 2;
l5: if (pc[ct]>5||5>=cs) goto l6;
    static int index = 1;
l6: if (pc[ct]>6||6>=cs) goto l7;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=7);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=7);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l7: if (pc[ct]>7||7>=cs) goto l8;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l8: if (pc[ct]>8||8>=cs) goto l9;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=9);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l9: if (pc[ct]>9||9>=cs) goto l10;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l10: if (pc[ct]>10||10>=cs) goto l11;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=11);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=11);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l11: if (pc[ct]>11||11>=cs) goto l12;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l12: if (pc[ct]>12||12>=cs) goto l13;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=13);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=13);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=13);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=13);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=13);
            
        }
        __CPROVER_assume(cs>=13);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=13);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 3;
        goto end;
    }
    __CPROVER_assume(cs>=13);
    
    //  }
l13: if (pc[ct]>13||13>=cs) goto l14;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l14: if (pc[ct]>14||14>=cs) goto l15;
    my_peer=return3[taskId];
l15: if (pc[ct]>15||15>=cs) goto l16;
    if (my_peer==taskId-1){
    l16: if (pc[ct]>16||16>=cs) goto l17;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=17);
    
    //--------------------------
l17: if (pc[ct]>17||17>=cs) goto l18;
    i = 2;
l18: if (pc[ct]>18||18>=cs) goto l19;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l19: if (pc[ct]>19||19>=cs) goto l20;
    score=0;
    //  while (score!=2) {
l20: if (pc[ct]>20||20>=cs) goto l21;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l21: if (pc[ct]>21||21>=cs) goto l22;
    appel = 2;
l22: if (pc[ct]>22||22>=cs) goto l23;
    index = 1;
l23: if (pc[ct]>23||23>=cs) goto l24;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=24);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=24);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l24: if (pc[ct]>24||24>=cs) goto l25;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l25: if (pc[ct]>25||25>=cs) goto l26;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=26);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l26: if (pc[ct]>26||26>=cs) goto l27;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l27: if (pc[ct]>27||27>=cs) goto l28;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=28);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=28);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l28: if (pc[ct]>28||28>=cs) goto l29;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l29: if (pc[ct]>29||29>=cs) goto l30;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=30);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=30);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=30);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=30);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=30);
            
        }
        __CPROVER_assume(cs>=30);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=30);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 20;
        goto end;
    }
    __CPROVER_assume(cs>=30);
    
    //  }
l30: if (pc[ct]>30||30>=cs) goto l31;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l31: if (pc[ct]>31||31>=cs) goto l32;
    my_peer=return3[taskId];
l32: if (pc[ct]>32||32>=cs) goto l33;
    if (my_peer==taskId-1){
    l33: if (pc[ct]>33||33>=cs) goto l34;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=34);
    
    //--------------------------
l34: if (pc[ct]>34||34>=cs) goto l35;
    i=3;
l35: if (pc[ct]>35||35>=cs) goto l36;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l36: if (pc[ct]>36||36>=cs) goto l37;
    score=0;
    //  while (score!=2) {
l37: if (pc[ct]>37||37>=cs) goto l38;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l38: if (pc[ct]>38||38>=cs) goto l39;
    appel = 2;
l39: if (pc[ct]>39||39>=cs) goto l40;
    index = 1;
l40: if (pc[ct]>40||40>=cs) goto l41;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=41);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=41);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l41: if (pc[ct]>41||41>=cs) goto l42;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l42: if (pc[ct]>42||42>=cs) goto l43;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=43);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l43: if (pc[ct]>43||43>=cs) goto l44;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l44: if (pc[ct]>44||44>=cs) goto l45;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=45);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=45);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l45: if (pc[ct]>45||45>=cs) goto l46;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l46: if (pc[ct]>46||46>=cs) goto l47;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=47);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=47);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=47);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=47);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=47);
            
        }
        __CPROVER_assume(cs>=47);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=47);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 37;
        goto end;
    }
    __CPROVER_assume(cs>=47);
    
    //  }
l47: if (pc[ct]>47||47>=cs) goto l48;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l48: if (pc[ct]>48||48>=cs) goto l49;
    my_peer=return3[taskId];
l49: if (pc[ct]>49||49>=cs) goto l50;
    if (my_peer==taskId-1){
    l50: if (pc[ct]>50||50>=cs) goto l51;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=51);
    
    //--------------------------
l51: if (pc[ct]>51||51>=cs) goto l52;
    i=4;
l52: if (pc[ct]>52||52>=cs) goto l53;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l53: if (pc[ct]>53||53>=cs) goto l54;
    score=0;
    //  while (score!=2) {
l54: if (pc[ct]>54||54>=cs) goto l55;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l55: if (pc[ct]>55||55>=cs) goto l56;
    appel = 2;
l56: if (pc[ct]>56||56>=cs) goto l57;
    index = 1;
l57: if (pc[ct]>57||57>=cs) goto l58;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=58);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=58);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l58: if (pc[ct]>58||58>=cs) goto l59;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l59: if (pc[ct]>59||59>=cs) goto l60;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=60);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l60: if (pc[ct]>60||60>=cs) goto l61;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l61: if (pc[ct]>61||61>=cs) goto l62;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=62);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=62);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l62: if (pc[ct]>62||62>=cs) goto l63;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l63: if (pc[ct]>63||63>=cs) goto l64;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=64);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=64);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=64);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=64);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=64);
            
        }
        __CPROVER_assume(cs>=64);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=64);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 54;
        goto end;
    }
    __CPROVER_assume(cs>=64);
    
    //  }
l64: if (pc[ct]>64||64>=cs) goto l65;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l65: if (pc[ct]>65||65>=cs) goto l66;
    my_peer=return3[taskId];
l66: if (pc[ct]>66||66>=cs) goto l67;
    if (my_peer==taskId-1){
    l67: if (pc[ct]>67||67>=cs) goto l68;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=68);
    
    //--------------------------
l68: if (pc[ct]>68||68>=cs) goto l69;
    i=5;
l69: if (pc[ct]>69||69>=cs) goto l70;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l70: if (pc[ct]>70||70>=cs) goto l71;
    score=0;
    //  while (score!=2) {
l71: if (pc[ct]>71||71>=cs) goto l72;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l72: if (pc[ct]>72||72>=cs) goto l73;
    appel = 2;
l73: if (pc[ct]>73||73>=cs) goto l74;
    index = 1;
l74: if (pc[ct]>74||74>=cs) goto l75;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=75);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=75);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l75: if (pc[ct]>75||75>=cs) goto l76;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l76: if (pc[ct]>76||76>=cs) goto l77;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=77);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l77: if (pc[ct]>77||77>=cs) goto l78;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l78: if (pc[ct]>78||78>=cs) goto l79;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=79);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=79);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l79: if (pc[ct]>79||79>=cs) goto l80;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l80: if (pc[ct]>80||80>=cs) goto l81;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=81);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=81);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=81);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=81);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=81);
            
        }
        __CPROVER_assume(cs>=81);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=81);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 71;
        goto end;
    }
    __CPROVER_assume(cs>=81);
    
    //  }
l81: if (pc[ct]>81||81>=cs) goto l82;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l82: if (pc[ct]>82||82>=cs) goto l83;
    my_peer=return3[taskId];
l83: if (pc[ct]>83||83>=cs) goto l84;
    if (my_peer==taskId-1){
    l84: if (pc[ct]>84||84>=cs) goto l85;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=85);
    
    //--------------------------
l85: if (pc[ct]>85||85>=cs) goto l86;
    i=6;
l86: if (pc[ct]>86||86>=cs) goto l87;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l87: if (pc[ct]>87||87>=cs) goto l88;
    score=0;
    //  while (score!=2) {
l88: if (pc[ct]>88||88>=cs) goto l89;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l89: if (pc[ct]>89||89>=cs) goto l90;
    appel = 2;
l90: if (pc[ct]>90||90>=cs) goto l91;
    index = 1;
l91: if (pc[ct]>91||91>=cs) goto l92;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=92);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=92);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l92: if (pc[ct]>92||92>=cs) goto l93;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l93: if (pc[ct]>93||93>=cs) goto l94;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=94);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l94: if (pc[ct]>94||94>=cs) goto l95;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l95: if (pc[ct]>95||95>=cs) goto l96;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=96);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=96);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l96: if (pc[ct]>96||96>=cs) goto l97;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l97: if (pc[ct]>97||97>=cs) goto l98;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=98);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=98);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=98);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=98);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=98);
            
        }
        __CPROVER_assume(cs>=98);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=98);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 88;
        goto end;
    }
    __CPROVER_assume(cs>=98);
    
    //  }
l98: if (pc[ct]>98||98>=cs) goto l99;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l99: if (pc[ct]>99||99>=cs) goto l100;
    my_peer=return3[taskId];
l100: if (pc[ct]>100||100>=cs) goto l101;
    if (my_peer==taskId-1){
    l101: if (pc[ct]>101||101>=cs) goto end;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=102);
    
    //--------------------------
    //  }
end:
    return;
}

void t_six(){
    static int taskId=6;
    static int taskIdEligible;
    static int my_peer = 0;
    
l0: if (pc[ct]>0||0>=cs) goto l1;
    static int i = 1;
    // for (i = 1; i<=nb_request; i++){
l1: if (pc[ct]>1||1>=cs) goto l2;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l2: if (pc[ct]>2||2>=cs) goto l3;
    static int score=0;
    //  while (score!=2) {
l3: if (pc[ct]>3||3>=cs) goto l4;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l4: if (pc[ct]>4||4>=cs) goto l5;
    static int appel = 2;
l5: if (pc[ct]>5||5>=cs) goto l6;
    static int index = 1;
l6: if (pc[ct]>6||6>=cs) goto l7;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=7);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=7);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l7: if (pc[ct]>7||7>=cs) goto l8;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l8: if (pc[ct]>8||8>=cs) goto l9;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=9);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l9: if (pc[ct]>9||9>=cs) goto l10;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l10: if (pc[ct]>10||10>=cs) goto l11;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=11);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=11);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l11: if (pc[ct]>11||11>=cs) goto l12;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l12: if (pc[ct]>12||12>=cs) goto l13;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=13);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=13);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=13);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=13);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=13);
            
        }
        __CPROVER_assume(cs>=13);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=13);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 3;
        goto end;
    }
    __CPROVER_assume(cs>=13);
    
    //  }
l13: if (pc[ct]>13||13>=cs) goto l14;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l14: if (pc[ct]>14||14>=cs) goto l15;
    my_peer=return3[taskId];
l15: if (pc[ct]>15||15>=cs) goto l16;
    if (my_peer==taskId-1){
    l16: if (pc[ct]>16||16>=cs) goto l17;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=17);
    
    //--------------------------
l17: if (pc[ct]>17||17>=cs) goto l18;
    i = 2;
l18: if (pc[ct]>18||18>=cs) goto l19;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l19: if (pc[ct]>19||19>=cs) goto l20;
    score=0;
    //  while (score!=2) {
l20: if (pc[ct]>20||20>=cs) goto l21;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l21: if (pc[ct]>21||21>=cs) goto l22;
    appel = 2;
l22: if (pc[ct]>22||22>=cs) goto l23;
    index = 1;
l23: if (pc[ct]>23||23>=cs) goto l24;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=24);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=24);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l24: if (pc[ct]>24||24>=cs) goto l25;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l25: if (pc[ct]>25||25>=cs) goto l26;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=26);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l26: if (pc[ct]>26||26>=cs) goto l27;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l27: if (pc[ct]>27||27>=cs) goto l28;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=28);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=28);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l28: if (pc[ct]>28||28>=cs) goto l29;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l29: if (pc[ct]>29||29>=cs) goto l30;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=30);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=30);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=30);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=30);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=30);
            
        }
        __CPROVER_assume(cs>=30);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=30);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 20;
        goto end;
    }
    __CPROVER_assume(cs>=30);
    
    //  }
l30: if (pc[ct]>30||30>=cs) goto l31;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l31: if (pc[ct]>31||31>=cs) goto l32;
    my_peer=return3[taskId];
l32: if (pc[ct]>32||32>=cs) goto l33;
    if (my_peer==taskId-1){
    l33: if (pc[ct]>33||33>=cs) goto l34;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=34);
    
    //--------------------------
l34: if (pc[ct]>34||34>=cs) goto l35;
    i=3;
l35: if (pc[ct]>35||35>=cs) goto l36;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l36: if (pc[ct]>36||36>=cs) goto l37;
    score=0;
    //  while (score!=2) {
l37: if (pc[ct]>37||37>=cs) goto l38;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l38: if (pc[ct]>38||38>=cs) goto l39;
    appel = 2;
l39: if (pc[ct]>39||39>=cs) goto l40;
    index = 1;
l40: if (pc[ct]>40||40>=cs) goto l41;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=41);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=41);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l41: if (pc[ct]>41||41>=cs) goto l42;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l42: if (pc[ct]>42||42>=cs) goto l43;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=43);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l43: if (pc[ct]>43||43>=cs) goto l44;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l44: if (pc[ct]>44||44>=cs) goto l45;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=45);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=45);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l45: if (pc[ct]>45||45>=cs) goto l46;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l46: if (pc[ct]>46||46>=cs) goto l47;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=47);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=47);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=47);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=47);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=47);
            
        }
        __CPROVER_assume(cs>=47);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=47);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 37;
        goto end;
    }
    __CPROVER_assume(cs>=47);
    
    //  }
l47: if (pc[ct]>47||47>=cs) goto l48;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l48: if (pc[ct]>48||48>=cs) goto l49;
    my_peer=return3[taskId];
l49: if (pc[ct]>49||49>=cs) goto l50;
    if (my_peer==taskId-1){
    l50: if (pc[ct]>50||50>=cs) goto l51;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=51);
    
    //--------------------------
l51: if (pc[ct]>51||51>=cs) goto l52;
    i=4;
l52: if (pc[ct]>52||52>=cs) goto l53;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l53: if (pc[ct]>53||53>=cs) goto l54;
    score=0;
    //  while (score!=2) {
l54: if (pc[ct]>54||54>=cs) goto l55;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l55: if (pc[ct]>55||55>=cs) goto l56;
    appel = 2;
l56: if (pc[ct]>56||56>=cs) goto l57;
    index = 1;
l57: if (pc[ct]>57||57>=cs) goto l58;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=58);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=58);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l58: if (pc[ct]>58||58>=cs) goto l59;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l59: if (pc[ct]>59||59>=cs) goto l60;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=60);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l60: if (pc[ct]>60||60>=cs) goto l61;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l61: if (pc[ct]>61||61>=cs) goto l62;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=62);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=62);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l62: if (pc[ct]>62||62>=cs) goto l63;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l63: if (pc[ct]>63||63>=cs) goto l64;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=64);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=64);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=64);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=64);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=64);
            
        }
        __CPROVER_assume(cs>=64);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=64);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 54;
        goto end;
    }
    __CPROVER_assume(cs>=64);
    
    //  }
l64: if (pc[ct]>64||64>=cs) goto l65;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l65: if (pc[ct]>65||65>=cs) goto l66;
    my_peer=return3[taskId];
l66: if (pc[ct]>66||66>=cs) goto l67;
    if (my_peer==taskId-1){
    l67: if (pc[ct]>67||67>=cs) goto l68;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=68);
    
    //--------------------------
l68: if (pc[ct]>68||68>=cs) goto l69;
    i=5;
l69: if (pc[ct]>69||69>=cs) goto l70;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l70: if (pc[ct]>70||70>=cs) goto l71;
    score=0;
    //  while (score!=2) {
l71: if (pc[ct]>71||71>=cs) goto l72;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l72: if (pc[ct]>72||72>=cs) goto l73;
    appel = 2;
l73: if (pc[ct]>73||73>=cs) goto l74;
    index = 1;
l74: if (pc[ct]>74||74>=cs) goto l75;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=75);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=75);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l75: if (pc[ct]>75||75>=cs) goto l76;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l76: if (pc[ct]>76||76>=cs) goto l77;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=77);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l77: if (pc[ct]>77||77>=cs) goto l78;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l78: if (pc[ct]>78||78>=cs) goto l79;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=79);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=79);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l79: if (pc[ct]>79||79>=cs) goto l80;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l80: if (pc[ct]>80||80>=cs) goto l81;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=81);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=81);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=81);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=81);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=81);
            
        }
        __CPROVER_assume(cs>=81);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=81);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 71;
        goto end;
    }
    __CPROVER_assume(cs>=81);
    
    //  }
l81: if (pc[ct]>81||81>=cs) goto l82;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l82: if (pc[ct]>82||82>=cs) goto l83;
    my_peer=return3[taskId];
l83: if (pc[ct]>83||83>=cs) goto l84;
    if (my_peer==taskId-1){
    l84: if (pc[ct]>84||84>=cs) goto l85;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=85);
    
    //--------------------------
l85: if (pc[ct]>85||85>=cs) goto l86;
    i=6;
l86: if (pc[ct]>86||86>=cs) goto l87;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l87: if (pc[ct]>87||87>=cs) goto l88;
    score=0;
    //  while (score!=2) {
l88: if (pc[ct]>88||88>=cs) goto l89;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l89: if (pc[ct]>89||89>=cs) goto l90;
    appel = 2;
l90: if (pc[ct]>90||90>=cs) goto l91;
    index = 1;
l91: if (pc[ct]>91||91>=cs) goto l92;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=92);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=92);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l92: if (pc[ct]>92||92>=cs) goto l93;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l93: if (pc[ct]>93||93>=cs) goto l94;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=94);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l94: if (pc[ct]>94||94>=cs) goto l95;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l95: if (pc[ct]>95||95>=cs) goto l96;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=96);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=96);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l96: if (pc[ct]>96||96>=cs) goto l97;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l97: if (pc[ct]>97||97>=cs) goto l98;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=98);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=98);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=98);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=98);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=98);
            
        }
        __CPROVER_assume(cs>=98);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=98);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 88;
        goto end;
    }
    __CPROVER_assume(cs>=98);
    
    //  }
l98: if (pc[ct]>98||98>=cs) goto l99;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l99: if (pc[ct]>99||99>=cs) goto l100;
    my_peer=return3[taskId];
l100: if (pc[ct]>100||100>=cs) goto l101;
    if (my_peer==taskId-1){
    l101: if (pc[ct]>101||101>=cs) goto end;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=102);
    
    //--------------------------
    //  }
end:
    return;
}

void t_seven(){
    static int taskId=7;
    static int taskIdEligible;
    static int my_peer = 0;
    
l0: if (pc[ct]>0||0>=cs) goto l1;
    static int i = 1;
    // for (i = 1; i<=nb_request; i++){
l1: if (pc[ct]>1||1>=cs) goto l2;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l2: if (pc[ct]>2||2>=cs) goto l3;
    static int score=0;
    //  while (score!=2) {
l3: if (pc[ct]>3||3>=cs) goto l4;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l4: if (pc[ct]>4||4>=cs) goto l5;
    static int appel = 2;
l5: if (pc[ct]>5||5>=cs) goto l6;
    static int index = 1;
l6: if (pc[ct]>6||6>=cs) goto l7;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=7);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=7);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l7: if (pc[ct]>7||7>=cs) goto l8;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l8: if (pc[ct]>8||8>=cs) goto l9;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=9);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l9: if (pc[ct]>9||9>=cs) goto l10;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l10: if (pc[ct]>10||10>=cs) goto l11;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=11);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=11);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l11: if (pc[ct]>11||11>=cs) goto l12;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l12: if (pc[ct]>12||12>=cs) goto l13;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=13);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=13);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=13);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=13);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=13);
            
        }
        __CPROVER_assume(cs>=13);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=13);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 3;
        goto end;
    }
    __CPROVER_assume(cs>=13);
    
    //  }
l13: if (pc[ct]>13||13>=cs) goto l14;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l14: if (pc[ct]>14||14>=cs) goto l15;
    my_peer=return3[taskId];
l15: if (pc[ct]>15||15>=cs) goto l16;
    if (my_peer==taskId-1){
    l16: if (pc[ct]>16||16>=cs) goto l17;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=17);
    
    //--------------------------
l17: if (pc[ct]>17||17>=cs) goto l18;
    i = 2;
l18: if (pc[ct]>18||18>=cs) goto l19;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l19: if (pc[ct]>19||19>=cs) goto l20;
    score=0;
    //  while (score!=2) {
l20: if (pc[ct]>20||20>=cs) goto l21;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l21: if (pc[ct]>21||21>=cs) goto l22;
    appel = 2;
l22: if (pc[ct]>22||22>=cs) goto l23;
    index = 1;
l23: if (pc[ct]>23||23>=cs) goto l24;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=24);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=24);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l24: if (pc[ct]>24||24>=cs) goto l25;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l25: if (pc[ct]>25||25>=cs) goto l26;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=26);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l26: if (pc[ct]>26||26>=cs) goto l27;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l27: if (pc[ct]>27||27>=cs) goto l28;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=28);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=28);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l28: if (pc[ct]>28||28>=cs) goto l29;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l29: if (pc[ct]>29||29>=cs) goto l30;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=30);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=30);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=30);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=30);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=30);
            
        }
        __CPROVER_assume(cs>=30);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=30);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 20;
        goto end;
    }
    __CPROVER_assume(cs>=30);
    
    //  }
l30: if (pc[ct]>30||30>=cs) goto l31;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l31: if (pc[ct]>31||31>=cs) goto l32;
    my_peer=return3[taskId];
l32: if (pc[ct]>32||32>=cs) goto l33;
    if (my_peer==taskId-1){
    l33: if (pc[ct]>33||33>=cs) goto l34;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=34);
    
    //--------------------------
l34: if (pc[ct]>34||34>=cs) goto l35;
    i=3;
l35: if (pc[ct]>35||35>=cs) goto l36;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l36: if (pc[ct]>36||36>=cs) goto l37;
    score=0;
    //  while (score!=2) {
l37: if (pc[ct]>37||37>=cs) goto l38;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l38: if (pc[ct]>38||38>=cs) goto l39;
    appel = 2;
l39: if (pc[ct]>39||39>=cs) goto l40;
    index = 1;
l40: if (pc[ct]>40||40>=cs) goto l41;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=41);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=41);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l41: if (pc[ct]>41||41>=cs) goto l42;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l42: if (pc[ct]>42||42>=cs) goto l43;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=43);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l43: if (pc[ct]>43||43>=cs) goto l44;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l44: if (pc[ct]>44||44>=cs) goto l45;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=45);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=45);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l45: if (pc[ct]>45||45>=cs) goto l46;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l46: if (pc[ct]>46||46>=cs) goto l47;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=47);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=47);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=47);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=47);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=47);
            
        }
        __CPROVER_assume(cs>=47);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=47);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 37;
        goto end;
    }
    __CPROVER_assume(cs>=47);
    
    //  }
l47: if (pc[ct]>47||47>=cs) goto l48;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l48: if (pc[ct]>48||48>=cs) goto l49;
    my_peer=return3[taskId];
l49: if (pc[ct]>49||49>=cs) goto l50;
    if (my_peer==taskId-1){
    l50: if (pc[ct]>50||50>=cs) goto l51;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=51);
    
    //--------------------------
l51: if (pc[ct]>51||51>=cs) goto l52;
    i=4;
l52: if (pc[ct]>52||52>=cs) goto l53;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l53: if (pc[ct]>53||53>=cs) goto l54;
    score=0;
    //  while (score!=2) {
l54: if (pc[ct]>54||54>=cs) goto l55;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l55: if (pc[ct]>55||55>=cs) goto l56;
    appel = 2;
l56: if (pc[ct]>56||56>=cs) goto l57;
    index = 1;
l57: if (pc[ct]>57||57>=cs) goto l58;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=58);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=58);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l58: if (pc[ct]>58||58>=cs) goto l59;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l59: if (pc[ct]>59||59>=cs) goto l60;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=60);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l60: if (pc[ct]>60||60>=cs) goto l61;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l61: if (pc[ct]>61||61>=cs) goto l62;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=62);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=62);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l62: if (pc[ct]>62||62>=cs) goto l63;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l63: if (pc[ct]>63||63>=cs) goto l64;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=64);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=64);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=64);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=64);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=64);
            
        }
        __CPROVER_assume(cs>=64);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=64);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 54;
        goto end;
    }
    __CPROVER_assume(cs>=64);
    
    //  }
l64: if (pc[ct]>64||64>=cs) goto l65;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l65: if (pc[ct]>65||65>=cs) goto l66;
    my_peer=return3[taskId];
l66: if (pc[ct]>66||66>=cs) goto l67;
    if (my_peer==taskId-1){
    l67: if (pc[ct]>67||67>=cs) goto l68;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=68);
    
    //--------------------------
l68: if (pc[ct]>68||68>=cs) goto l69;
    i=5;
l69: if (pc[ct]>69||69>=cs) goto l70;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l70: if (pc[ct]>70||70>=cs) goto l71;
    score=0;
    //  while (score!=2) {
l71: if (pc[ct]>71||71>=cs) goto l72;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l72: if (pc[ct]>72||72>=cs) goto l73;
    appel = 2;
l73: if (pc[ct]>73||73>=cs) goto l74;
    index = 1;
l74: if (pc[ct]>74||74>=cs) goto l75;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=75);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=75);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l75: if (pc[ct]>75||75>=cs) goto l76;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l76: if (pc[ct]>76||76>=cs) goto l77;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=77);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l77: if (pc[ct]>77||77>=cs) goto l78;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l78: if (pc[ct]>78||78>=cs) goto l79;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=79);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=79);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l79: if (pc[ct]>79||79>=cs) goto l80;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l80: if (pc[ct]>80||80>=cs) goto l81;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=81);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=81);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=81);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=81);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=81);
            
        }
        __CPROVER_assume(cs>=81);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=81);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 71;
        goto end;
    }
    __CPROVER_assume(cs>=81);
    
    //  }
l81: if (pc[ct]>81||81>=cs) goto l82;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l82: if (pc[ct]>82||82>=cs) goto l83;
    my_peer=return3[taskId];
l83: if (pc[ct]>83||83>=cs) goto l84;
    if (my_peer==taskId-1){
    l84: if (pc[ct]>84||84>=cs) goto l85;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=85);
    
    //--------------------------
l85: if (pc[ct]>85||85>=cs) goto l86;
    i=6;
l86: if (pc[ct]>86||86>=cs) goto l87;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l87: if (pc[ct]>87||87>=cs) goto l88;
    score=0;
    //  while (score!=2) {
l88: if (pc[ct]>88||88>=cs) goto l89;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l89: if (pc[ct]>89||89>=cs) goto l90;
    appel = 2;
l90: if (pc[ct]>90||90>=cs) goto l91;
    index = 1;
l91: if (pc[ct]>91||91>=cs) goto l92;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=92);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=92);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l92: if (pc[ct]>92||92>=cs) goto l93;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l93: if (pc[ct]>93||93>=cs) goto l94;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=94);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l94: if (pc[ct]>94||94>=cs) goto l95;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l95: if (pc[ct]>95||95>=cs) goto l96;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=96);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=96);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l96: if (pc[ct]>96||96>=cs) goto l97;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l97: if (pc[ct]>97||97>=cs) goto l98;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=98);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=98);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=98);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=98);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=98);
            
        }
        __CPROVER_assume(cs>=98);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=98);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 88;
        goto end;
    }
    __CPROVER_assume(cs>=98);
    
    //  }
l98: if (pc[ct]>98||98>=cs) goto l99;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l99: if (pc[ct]>99||99>=cs) goto l100;
    my_peer=return3[taskId];
l100: if (pc[ct]>100||100>=cs) goto l101;
    if (my_peer==taskId-1){
    l101: if (pc[ct]>101||101>=cs) goto end;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=102);
    
    //--------------------------
    //  }
end:
    return;
}

void t_eight(){
    static int taskId=8;
    static int taskIdEligible;
    static int my_peer = 0;
    
l0: if (pc[ct]>0||0>=cs) goto l1;
    static int i = 1;
    // for (i = 1; i<=nb_request; i++){
l1: if (pc[ct]>1||1>=cs) goto l2;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l2: if (pc[ct]>2||2>=cs) goto l3;
    static int score=0;
    //  while (score!=2) {
l3: if (pc[ct]>3||3>=cs) goto l4;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l4: if (pc[ct]>4||4>=cs) goto l5;
    static int appel = 2;
l5: if (pc[ct]>5||5>=cs) goto l6;
    static int index = 1;
l6: if (pc[ct]>6||6>=cs) goto l7;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=7);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=7);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l7: if (pc[ct]>7||7>=cs) goto l8;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l8: if (pc[ct]>8||8>=cs) goto l9;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=9);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l9: if (pc[ct]>9||9>=cs) goto l10;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l10: if (pc[ct]>10||10>=cs) goto l11;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=11);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=11);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l11: if (pc[ct]>11||11>=cs) goto l12;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l12: if (pc[ct]>12||12>=cs) goto l13;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=13);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=13);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=13);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=13);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=13);
            
        }
        __CPROVER_assume(cs>=13);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=13);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 3;
        goto end;
    }
    __CPROVER_assume(cs>=13);
    
    //  }
l13: if (pc[ct]>13||13>=cs) goto l14;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l14: if (pc[ct]>14||14>=cs) goto l15;
    my_peer=return3[taskId];
l15: if (pc[ct]>15||15>=cs) goto l16;
    if (my_peer==taskId-1){
    l16: if (pc[ct]>16||16>=cs) goto l17;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=17);
    
    //--------------------------
l17: if (pc[ct]>17||17>=cs) goto l18;
    i = 2;
l18: if (pc[ct]>18||18>=cs) goto l19;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l19: if (pc[ct]>19||19>=cs) goto l20;
    score=0;
    //  while (score!=2) {
l20: if (pc[ct]>20||20>=cs) goto l21;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l21: if (pc[ct]>21||21>=cs) goto l22;
    appel = 2;
l22: if (pc[ct]>22||22>=cs) goto l23;
    index = 1;
l23: if (pc[ct]>23||23>=cs) goto l24;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=24);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=24);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l24: if (pc[ct]>24||24>=cs) goto l25;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l25: if (pc[ct]>25||25>=cs) goto l26;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=26);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l26: if (pc[ct]>26||26>=cs) goto l27;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l27: if (pc[ct]>27||27>=cs) goto l28;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=28);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=28);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l28: if (pc[ct]>28||28>=cs) goto l29;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l29: if (pc[ct]>29||29>=cs) goto l30;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=30);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=30);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=30);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=30);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=30);
            
        }
        __CPROVER_assume(cs>=30);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=30);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 20;
        goto end;
    }
    __CPROVER_assume(cs>=30);
    
    //  }
l30: if (pc[ct]>30||30>=cs) goto l31;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l31: if (pc[ct]>31||31>=cs) goto l32;
    my_peer=return3[taskId];
l32: if (pc[ct]>32||32>=cs) goto l33;
    if (my_peer==taskId-1){
    l33: if (pc[ct]>33||33>=cs) goto l34;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=34);
    
    //--------------------------
l34: if (pc[ct]>34||34>=cs) goto l35;
    i=3;
l35: if (pc[ct]>35||35>=cs) goto l36;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l36: if (pc[ct]>36||36>=cs) goto l37;
    score=0;
    //  while (score!=2) {
l37: if (pc[ct]>37||37>=cs) goto l38;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l38: if (pc[ct]>38||38>=cs) goto l39;
    appel = 2;
l39: if (pc[ct]>39||39>=cs) goto l40;
    index = 1;
l40: if (pc[ct]>40||40>=cs) goto l41;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=41);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=41);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l41: if (pc[ct]>41||41>=cs) goto l42;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l42: if (pc[ct]>42||42>=cs) goto l43;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=43);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l43: if (pc[ct]>43||43>=cs) goto l44;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l44: if (pc[ct]>44||44>=cs) goto l45;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=45);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=45);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l45: if (pc[ct]>45||45>=cs) goto l46;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l46: if (pc[ct]>46||46>=cs) goto l47;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=47);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=47);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=47);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=47);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=47);
            
        }
        __CPROVER_assume(cs>=47);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=47);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 37;
        goto end;
    }
    __CPROVER_assume(cs>=47);
    
    //  }
l47: if (pc[ct]>47||47>=cs) goto l48;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l48: if (pc[ct]>48||48>=cs) goto l49;
    my_peer=return3[taskId];
l49: if (pc[ct]>49||49>=cs) goto l50;
    if (my_peer==taskId-1){
    l50: if (pc[ct]>50||50>=cs) goto l51;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=51);
    
    //--------------------------
l51: if (pc[ct]>51||51>=cs) goto l52;
    i=4;
l52: if (pc[ct]>52||52>=cs) goto l53;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l53: if (pc[ct]>53||53>=cs) goto l54;
    score=0;
    //  while (score!=2) {
l54: if (pc[ct]>54||54>=cs) goto l55;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l55: if (pc[ct]>55||55>=cs) goto l56;
    appel = 2;
l56: if (pc[ct]>56||56>=cs) goto l57;
    index = 1;
l57: if (pc[ct]>57||57>=cs) goto l58;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=58);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=58);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l58: if (pc[ct]>58||58>=cs) goto l59;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l59: if (pc[ct]>59||59>=cs) goto l60;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=60);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l60: if (pc[ct]>60||60>=cs) goto l61;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l61: if (pc[ct]>61||61>=cs) goto l62;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=62);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=62);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l62: if (pc[ct]>62||62>=cs) goto l63;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l63: if (pc[ct]>63||63>=cs) goto l64;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=64);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=64);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=64);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=64);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=64);
            
        }
        __CPROVER_assume(cs>=64);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=64);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 54;
        goto end;
    }
    __CPROVER_assume(cs>=64);
    
    //  }
l64: if (pc[ct]>64||64>=cs) goto l65;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l65: if (pc[ct]>65||65>=cs) goto l66;
    my_peer=return3[taskId];
l66: if (pc[ct]>66||66>=cs) goto l67;
    if (my_peer==taskId-1){
    l67: if (pc[ct]>67||67>=cs) goto l68;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=68);
    
    //--------------------------
l68: if (pc[ct]>68||68>=cs) goto l69;
    i=5;
l69: if (pc[ct]>69||69>=cs) goto l70;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l70: if (pc[ct]>70||70>=cs) goto l71;
    score=0;
    //  while (score!=2) {
l71: if (pc[ct]>71||71>=cs) goto l72;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l72: if (pc[ct]>72||72>=cs) goto l73;
    appel = 2;
l73: if (pc[ct]>73||73>=cs) goto l74;
    index = 1;
l74: if (pc[ct]>74||74>=cs) goto l75;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=75);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=75);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l75: if (pc[ct]>75||75>=cs) goto l76;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l76: if (pc[ct]>76||76>=cs) goto l77;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=77);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l77: if (pc[ct]>77||77>=cs) goto l78;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l78: if (pc[ct]>78||78>=cs) goto l79;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=79);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=79);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l79: if (pc[ct]>79||79>=cs) goto l80;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l80: if (pc[ct]>80||80>=cs) goto l81;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=81);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=81);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=81);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=81);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=81);
            
        }
        __CPROVER_assume(cs>=81);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=81);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 71;
        goto end;
    }
    __CPROVER_assume(cs>=81);
    
    //  }
l81: if (pc[ct]>81||81>=cs) goto l82;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l82: if (pc[ct]>82||82>=cs) goto l83;
    my_peer=return3[taskId];
l83: if (pc[ct]>83||83>=cs) goto l84;
    if (my_peer==taskId-1){
    l84: if (pc[ct]>84||84>=cs) goto l85;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=85);
    
    //--------------------------
l85: if (pc[ct]>85||85>=cs) goto l86;
    i=6;
l86: if (pc[ct]>86||86>=cs) goto l87;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l87: if (pc[ct]>87||87>=cs) goto l88;
    score=0;
    //  while (score!=2) {
l88: if (pc[ct]>88||88>=cs) goto l89;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l89: if (pc[ct]>89||89>=cs) goto l90;
    appel = 2;
l90: if (pc[ct]>90||90>=cs) goto l91;
    index = 1;
l91: if (pc[ct]>91||91>=cs) goto l92;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=92);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=92);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l92: if (pc[ct]>92||92>=cs) goto l93;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l93: if (pc[ct]>93||93>=cs) goto l94;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=94);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l94: if (pc[ct]>94||94>=cs) goto l95;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l95: if (pc[ct]>95||95>=cs) goto l96;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=96);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=96);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l96: if (pc[ct]>96||96>=cs) goto l97;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l97: if (pc[ct]>97||97>=cs) goto l98;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=98);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=98);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=98);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=98);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=98);
            
        }
        __CPROVER_assume(cs>=98);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=98);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 88;
        goto end;
    }
    __CPROVER_assume(cs>=98);
    
    //  }
l98: if (pc[ct]>98||98>=cs) goto l99;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l99: if (pc[ct]>99||99>=cs) goto l100;
    my_peer=return3[taskId];
l100: if (pc[ct]>100||100>=cs) goto l101;
    if (my_peer==taskId-1){
    l101: if (pc[ct]>101||101>=cs) goto end;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=102);
    
    //--------------------------
    //  }
end:
    return;
}

void t_nine(){
    static int taskId=9;
    static int taskIdEligible;
    static int my_peer = 0;
    
l0: if (pc[ct]>0||0>=cs) goto l1;
    static int i = 1;
    // for (i = 1; i<=nb_request; i++){
l1: if (pc[ct]>1||1>=cs) goto l2;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l2: if (pc[ct]>2||2>=cs) goto l3;
    static int score=0;
    //  while (score!=2) {
l3: if (pc[ct]>3||3>=cs) goto l4;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l4: if (pc[ct]>4||4>=cs) goto l5;
    static int appel = 2;
l5: if (pc[ct]>5||5>=cs) goto l6;
    static int index = 1;
l6: if (pc[ct]>6||6>=cs) goto l7;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=7);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=7);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l7: if (pc[ct]>7||7>=cs) goto l8;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l8: if (pc[ct]>8||8>=cs) goto l9;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=9);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l9: if (pc[ct]>9||9>=cs) goto l10;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l10: if (pc[ct]>10||10>=cs) goto l11;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=11);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=11);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l11: if (pc[ct]>11||11>=cs) goto l12;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l12: if (pc[ct]>12||12>=cs) goto l13;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=13);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=13);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=13);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=13);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=13);
            
        }
        __CPROVER_assume(cs>=13);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=13);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 3;
        goto end;
    }
    __CPROVER_assume(cs>=13);
    
    //  }
l13: if (pc[ct]>13||13>=cs) goto l14;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l14: if (pc[ct]>14||14>=cs) goto l15;
    my_peer=return3[taskId];
l15: if (pc[ct]>15||15>=cs) goto l16;
    if (my_peer==taskId-1){
    l16: if (pc[ct]>16||16>=cs) goto l17;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=17);
    
    //--------------------------
l17: if (pc[ct]>17||17>=cs) goto l18;
    i = 2;
l18: if (pc[ct]>18||18>=cs) goto l19;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l19: if (pc[ct]>19||19>=cs) goto l20;
    score=0;
    //  while (score!=2) {
l20: if (pc[ct]>20||20>=cs) goto l21;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l21: if (pc[ct]>21||21>=cs) goto l22;
    appel = 2;
l22: if (pc[ct]>22||22>=cs) goto l23;
    index = 1;
l23: if (pc[ct]>23||23>=cs) goto l24;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=24);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=24);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l24: if (pc[ct]>24||24>=cs) goto l25;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l25: if (pc[ct]>25||25>=cs) goto l26;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=26);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l26: if (pc[ct]>26||26>=cs) goto l27;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l27: if (pc[ct]>27||27>=cs) goto l28;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=28);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=28);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l28: if (pc[ct]>28||28>=cs) goto l29;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l29: if (pc[ct]>29||29>=cs) goto l30;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=30);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=30);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=30);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=30);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=30);
            
        }
        __CPROVER_assume(cs>=30);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=30);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 20;
        goto end;
    }
    __CPROVER_assume(cs>=30);
    
    //  }
l30: if (pc[ct]>30||30>=cs) goto l31;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l31: if (pc[ct]>31||31>=cs) goto l32;
    my_peer=return3[taskId];
l32: if (pc[ct]>32||32>=cs) goto l33;
    if (my_peer==taskId-1){
    l33: if (pc[ct]>33||33>=cs) goto l34;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=34);
    
    //--------------------------
l34: if (pc[ct]>34||34>=cs) goto l35;
    i=3;
l35: if (pc[ct]>35||35>=cs) goto l36;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l36: if (pc[ct]>36||36>=cs) goto l37;
    score=0;
    //  while (score!=2) {
l37: if (pc[ct]>37||37>=cs) goto l38;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l38: if (pc[ct]>38||38>=cs) goto l39;
    appel = 2;
l39: if (pc[ct]>39||39>=cs) goto l40;
    index = 1;
l40: if (pc[ct]>40||40>=cs) goto l41;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=41);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=41);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l41: if (pc[ct]>41||41>=cs) goto l42;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l42: if (pc[ct]>42||42>=cs) goto l43;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=43);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l43: if (pc[ct]>43||43>=cs) goto l44;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l44: if (pc[ct]>44||44>=cs) goto l45;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=45);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=45);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l45: if (pc[ct]>45||45>=cs) goto l46;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l46: if (pc[ct]>46||46>=cs) goto l47;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=47);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=47);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=47);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=47);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=47);
            
        }
        __CPROVER_assume(cs>=47);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=47);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 37;
        goto end;
    }
    __CPROVER_assume(cs>=47);
    
    //  }
l47: if (pc[ct]>47||47>=cs) goto l48;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l48: if (pc[ct]>48||48>=cs) goto l49;
    my_peer=return3[taskId];
l49: if (pc[ct]>49||49>=cs) goto l50;
    if (my_peer==taskId-1){
    l50: if (pc[ct]>50||50>=cs) goto l51;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=51);
    
    //--------------------------
l51: if (pc[ct]>51||51>=cs) goto l52;
    i=4;
l52: if (pc[ct]>52||52>=cs) goto l53;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l53: if (pc[ct]>53||53>=cs) goto l54;
    score=0;
    //  while (score!=2) {
l54: if (pc[ct]>54||54>=cs) goto l55;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l55: if (pc[ct]>55||55>=cs) goto l56;
    appel = 2;
l56: if (pc[ct]>56||56>=cs) goto l57;
    index = 1;
l57: if (pc[ct]>57||57>=cs) goto l58;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=58);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=58);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l58: if (pc[ct]>58||58>=cs) goto l59;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l59: if (pc[ct]>59||59>=cs) goto l60;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=60);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l60: if (pc[ct]>60||60>=cs) goto l61;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l61: if (pc[ct]>61||61>=cs) goto l62;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=62);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=62);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l62: if (pc[ct]>62||62>=cs) goto l63;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l63: if (pc[ct]>63||63>=cs) goto l64;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=64);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=64);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=64);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=64);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=64);
            
        }
        __CPROVER_assume(cs>=64);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=64);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 54;
        goto end;
    }
    __CPROVER_assume(cs>=64);
    
    //  }
l64: if (pc[ct]>64||64>=cs) goto l65;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l65: if (pc[ct]>65||65>=cs) goto l66;
    my_peer=return3[taskId];
l66: if (pc[ct]>66||66>=cs) goto l67;
    if (my_peer==taskId-1){
    l67: if (pc[ct]>67||67>=cs) goto l68;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=68);
    
    //--------------------------
l68: if (pc[ct]>68||68>=cs) goto l69;
    i=5;
l69: if (pc[ct]>69||69>=cs) goto l70;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l70: if (pc[ct]>70||70>=cs) goto l71;
    score=0;
    //  while (score!=2) {
l71: if (pc[ct]>71||71>=cs) goto l72;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l72: if (pc[ct]>72||72>=cs) goto l73;
    appel = 2;
l73: if (pc[ct]>73||73>=cs) goto l74;
    index = 1;
l74: if (pc[ct]>74||74>=cs) goto l75;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=75);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=75);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l75: if (pc[ct]>75||75>=cs) goto l76;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l76: if (pc[ct]>76||76>=cs) goto l77;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=77);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l77: if (pc[ct]>77||77>=cs) goto l78;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l78: if (pc[ct]>78||78>=cs) goto l79;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=79);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=79);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l79: if (pc[ct]>79||79>=cs) goto l80;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l80: if (pc[ct]>80||80>=cs) goto l81;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=81);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=81);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=81);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=81);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=81);
            
        }
        __CPROVER_assume(cs>=81);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=81);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 71;
        goto end;
    }
    __CPROVER_assume(cs>=81);
    
    //  }
l81: if (pc[ct]>81||81>=cs) goto l82;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l82: if (pc[ct]>82||82>=cs) goto l83;
    my_peer=return3[taskId];
l83: if (pc[ct]>83||83>=cs) goto l84;
    if (my_peer==taskId-1){
    l84: if (pc[ct]>84||84>=cs) goto l85;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=85);
    
    //--------------------------
l85: if (pc[ct]>85||85>=cs) goto l86;
    i=6;
l86: if (pc[ct]>86||86>=cs) goto l87;
    x3[taskId]=taskId-1;
    //get_a_peer3(taskId);
l87: if (pc[ct]>87||87>=cs) goto l88;
    score=0;
    //  while (score!=2) {
l88: if (pc[ct]>88||88>=cs) goto l89;
    x1[taskId]=x3[taskId];
    //protected_object_call2(taskId, 2, 1);
l89: if (pc[ct]>89||89>=cs) goto l90;
    appel = 2;
l90: if (pc[ct]>90||90>=cs) goto l91;
    index = 1;
l91: if (pc[ct]>91||91>=cs) goto l92;
    lock2(taskId);
    if (appel == 0){ //function
        run_function_call2(taskId, index);
    }else if (appel == 1){ //procedure
        __CPROVER_assume(cs>=92);
        
        run_procedure_call2(taskId, index);
    }else if (appel == 2){ //entry
        __CPROVER_assume(cs>=92);
        
        //run_entry_call2(taskId, index);
        static int taskIdEligible2;
        evaluate_barrier2(index);
        if (tabBarrier2[index]==FALSE){
            add_entry_queue2(taskId, index);
            unlock2(taskId);
        l92: if (pc[ct]>92||92>=cs) goto l93;
            __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
        l93: if (pc[ct]>93||93>=cs) goto l94;
            remove_entry_queue2(taskId, index);
        }
        __CPROVER_assume(cs>=94);
        
        //run_entry_body2(taskId,index);
        if (index == 1) { //Cooperate
            if (score1[taskId]==0 && MustWait){
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l94: if (pc[ct]>94||94>=cs) goto l95;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l95: if (pc[ct]>95||95>=cs) goto l96;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=96);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=96);
            
            if (score1[taskId]==0 && First_Call){
                a_chameneos = x1[taskId];
                First_Call = FALSE;
                score1[taskId]=1;
                NotifyAll = FALSE;
                //requeue2(taskId, 2);
                index = 2;
                evaluate_barrier2(index);
                if (tabBarrier2[index]==FALSE){
                    add_entry_queue2(taskId, index);
                    unlock2(taskId);
                l96: if (pc[ct]>96||96>=cs) goto l97;
                    __CPROVER_assume(tabStatus2[taskId]&&externalLock2==taskId);
                l97: if (pc[ct]>97||97>=cs) goto l98;
                    remove_entry_queue2(taskId, index);
                }
                __CPROVER_assume(cs>=98);
                
                //run_entry_body2(taskId,index);
                //--------------------------
                //--------------------------
            }
            __CPROVER_assume(cs>=98);
            
            if (score1[taskId]==0 && !First_Call){
                b_chameneos=x1[taskId];
                x_other1[taskId]=a_chameneos;
                First_Call=TRUE;
                MustWait=TRUE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=98);
            
            if (score1[taskId]=1 && First_Call){
                x_other1[taskId]=b_chameneos;
                MustWait=FALSE;
                score1[taskId]=2;
                NotifyAll=TRUE;
            }
            __CPROVER_assume(cs>=98);
            
        }else if (index == 2){ //Wait
            __CPROVER_assume(cs>=98);
            
        }
        __CPROVER_assume(cs>=98);
        
        //--------------------------
        taskIdEligible2 = check_and_run_any_entry2();
        transfert2(taskId, taskIdEligible2);
    }
    __CPROVER_assume(cs>=98);
    
    //--------------------------
    score=score1[taskId];
    // __CPROVER_assume(score==2);
    if (score!=2){
        cs = 88;
        goto end;
    }
    __CPROVER_assume(cs>=98);
    
    //  }
l98: if (pc[ct]>98||98>=cs) goto l99;
    return3[taskId]=x_other1[taskId];
    //--------------------------
l99: if (pc[ct]>99||99>=cs) goto l100;
    my_peer=return3[taskId];
l100: if (pc[ct]>100||100>=cs) goto l101;
    if (my_peer==taskId-1){
    l101: if (pc[ct]>101||101>=cs) goto end;
        cs = size[taskId];
        goto end;
    }
    __CPROVER_assume(cs>=102);
    
    //--------------------------
    //  }
end:
    return;
}

*/


void init(){
    pc[0] = 0;
    size[0] = 0;
    
    pc[1] = 0;
    pc[2] = 0;
    pc[3] = 0;
    pc[4] = 0;
  /*  pc[5] = 0;
    pc[6] = 0;
    pc[7] = 0;
    pc[8] = 0;
    pc[9] = 0;*/
    
    int s = 102;

    size[1] = s;
    size[2] = s;
    size[3] = s;
    size[4] = s;
  /*  size[5] = s;
    size[6] = s;
    size[7] = s;
    size[8] = s;
    size[9] = s;*/
    
    int t1, t2, t3, t4, t5, t6, t7, t8, t9;
    
    create(t1, 1);
    create(t2, 2);
    create(t3, 3);
    create(t4, 4);
  /*  create(t5, 5);
    create(t6, 6);
    create(t7, 7);
    create(t8, 8);
    create(t9, 9);*/
    
    int i = 0;
    for (i=0; i<NB_TASK+1; i++){
        x1[i] = 0;
        x_other1[i] = 0;
        score1[i] = 0;
        
        x2[i] = 0;
        x_other2[i] = 0;
        score2[i] = 0;
        
        x3[i] = 0;
        return3[i] = 0;
    }
}

int main(){
    init();
    
    for (;;) {
        ct = 1;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_one();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
     
        ct = 2;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_two();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        ct = 3;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_three();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        ct = 4;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_four();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
  /*      ct = 5;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_five();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        ct = 6;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_six();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        
        ct = 7;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_seven();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }

        ct = 8;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_eight();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }

        ct = 9;
        if (active[ct]){
            cs = nondet_uint();
            __CPROVER_assume(cs >= pc[ct] && cs <= size[ct]);
            t_nine();
            pc[ct] = cs;
            active[ct] = pc[ct] != size[ct];
            dead = active[ct] ? dead : dead+1;
        }
        */
        
        __CPROVER_assert(tabEntryCount2[2]<2, "OK tabEntryCount2[2]<2");
        
        if (dead>0 && dead<NB_TASK){
            __CPROVER_assert(dead+taskInWait<NB_TASK, "OK dead+taskInWait>=NB_TASK");
        }
        
        if (pc[1] == size[1] && pc[2] == size[2] && pc[3] == size[3] && pc[4] == size[4]){ // && pc[5] == size[5] && pc[6] == size[6]){
            __CPROVER_assert(FALSE, "OK");
        }
    }
    
    __CPROVER_assert(FALSE, "KO");
    
}
